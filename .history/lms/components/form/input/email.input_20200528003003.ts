import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

/* PIPPA */
import { EmailInput as BaseEmailInput } from '../../../../pippa';

@Component({
	selector  : '.input-component',
	template  : `<mat-form-field *ngIf="formGroup" [formGroup]="formGroup">

					<input type="hidden" formControlName="{{field.name}}" (ngModelChange)="onModelChange($event)"/>

					<input type="text"
							matInput
							autocomplete="false"
							[ngModel]="value"
							[ngModelOptions]="{standalone: true}"
							[readonly]="form?.isSetModeForm() && !field.editable"
							(input)="onInput($event)"
							[placeholder]="field.placeholder"/>

				</mat-form-field>
				<error-input [control]="formControl" [submitted]="submitted">
					<span *ngIf="hasError('email')">{{'Email inválido' | translate}}</span>
				</error-input>`,
})
export class EmailInput extends BaseEmailInput
{
}