import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

/* PIPPA */
import { EmailInput as BaseEmailInput } from '../../../../pippa';

@Component({
	selector  : '.input-component',
	template  : `<mat-form-field *ngIf="formGroup" [formGroup]="formGroup">
					<input matInput placeholder="{{formItem.field.label}}" formControlName="{{formItem.field.name}}">
				</mat-form-field>
				<error-input [control]="formControl" [submitted]="submitted">
					<span *ngIf="hasError('email')">{{'Email inválido' | translate}}</span>
				</error-input>`,
})
export class EmailInput extends BaseEmailInput
{
}