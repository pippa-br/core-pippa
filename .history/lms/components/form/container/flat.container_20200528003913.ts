import { Component, Input, ViewChild, ViewContainerRef, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ToastController } from '@ionic/angular';

/* PIPPA */
import { BaseComponent, InputValidate, InputFactory } from '../../../../pippa';

@Component({
	selector	: 'flat-container',
  	templateUrl : './flat.container.html',
	styleUrls   : ['./flat.container.scss'],
})
export class FlatContainer extends BaseComponent
{
	@ViewChild('container', { read : ViewContainerRef, static : true }) container : ViewContainerRef;

	public form    	  : any;
	public formGroup  : any;
	public _resolve   : any;
	public _submitted : any;
	public _data      : any;

	@Output() add 	 	 : any = new EventEmitter<any>();
	@Input()  title   	 : string;
	@Input()  buttonLabel : string;

	private components : any;

	constructor(
		public toastController	 : ToastController,
        public changeDetectorRef : ChangeDetectorRef
	) 
	{
		super();
	}

	async createComponents(form:any)
	{
		return new Promise(async (resolve) =>
        {
            this._resolve = resolve;

            this.changeDetectorRef.markForCheck();

			this.clear();

			this.form           = form;
			this.formGroup  	= new FormGroup({});
			this.components 	= [];
			this.form.instances = [];
			this.form.formGroup = this.formGroup;
			const items 		= this.form.getItems();
	
			for(const key in items)
			{
				const formItem = items[key];
	
				const inputComponent = await InputFactory.createComponent(formItem, this.container, this.formGroup, 
				{
					form : this.form
				});
	
				this.form.instances[formItem.field.name] = inputComponent.instance;
	
				this.components.push(inputComponent);
			}	

			const promises : Array<any> = [];
			
			/* CALCULAR RENDERER */
			for(const key in this.components)
			{
				const component = this.components[key];
				const promise   = component.instance.isRendererComplete();

				promises.push(promise);					
			}
	
			Promise.all(promises).then(() =>
			{
				this._resolve();
	
				console.log('completo form all');
			});
		});				
	}

	set data(value:boolean)
    {
        this._data = value;

        for(const key in this.components)
        {
            const component = this.components[key];

            component.instance.data = value;
        }
    }

    get data()
    {
        return this._data;
    }

	set submitted(value:boolean)
    {
        this._submitted = value;

        for(const key in this.components)
        {
            const component = this.components[key];

            component.instance.submitted = value;
        }
    }

    get submitted()
    {
        return this._submitted;
    }

	clear()
	{
		this.container.clear();
	}

	/*onSave()
	{
		console.error(this.formGroup);
		
		this.formGroup.submitted = true;

		if(this.formGroup.valid)
		{
			this.add.emit(this.formGroup.value);
		}
		else
		{
			this.toastMessage();
		}

		for(const key in this.components)
		{
			this.components[key].instance.submitted = true;
		}
	}

	async toastMessage() 
	{
		const toast = await this.toastController.create({
			header   : 'Aviso!',
			cssClass : 'toastMessage',
			message  : 'Verifique se todos os campos foram preenchidos corretamente',
			position : 'bottom',
			animated : true,
			duration : 3000,
			buttons: [
			{
				text: 'Ok',
				role: 'cancel',
			}]
		});

		toast.present();
	}*/
}
