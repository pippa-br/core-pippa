import { Component, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

/* PIPPA */
import { BasePage, FormPlugin, DocumentPlugin, DocumentCollection, Grid, FieldType } from '../../../pippa';

@Component({
	selector    : 'app-perfil',
	templateUrl : './perfil.page.html',
	styleUrls   : ['./perfil.page.scss'],
})
export class PerfilPage extends BasePage {

	@ViewChild('userForm', { static : true }) userForm : any;

	public registerForm  : FormGroup;
	public courses       : any;
	public financial     : any;	
	public gridCourses   : any;
	public gridFinancial : any;
	public selectedIndex : number;

	async initialize()
	{	
		if(this.core().queryParams.tab == 'course')
		{
			this.selectedIndex = 0;
		}
		else if(this.core().queryParams.tab == 'financial')
		{
			this.selectedIndex = 1;
		}
		else if(this.core().queryParams.tab == 'data')
		{
			this.selectedIndex = 2;
		}
		else if(this.core().queryParams.tab == 'logout')
		{
			this.selectedIndex = 3;
		}

		const formPlugin 	 = this.core().injector.get(FormPlugin);
		const documentPlugin = this.core().injector.get(DocumentPlugin);

		/* GRID COURSE */
		this.gridCourses = new Grid({
            hasSetButton    : false,
            hasDelButton    : false,
			hasViewerButton : true,
            items : [
                {
                    field : {
                        label : 'Curso',
                        name : 'name',
                        type  : FieldType.type('Text'),
                        setting : {
                            hasTime : true
                        }
                    }
                },
                {
                    field : {
                        label : 'Turma',
                        name  : 'class',
                        type  : FieldType.type('Text'),
                    }
                },
            ],
		});
		
		/* GRID FINANCIAL */
		this.gridFinancial = new Grid({
            hasSetButton    : false,
            hasDelButton    : false,
			hasViewerButton : false,
            items : [
				{ 
                    field : {
                        label : 'Data',
                        name  : 'postdate',
                        type  : FieldType.type('Date'),
                        setting : {
                            hasTime : true
                        }
                    }
                },
                { 
                    field : {
                        label : 'Curso',
                        name : 'course.name',
                        type  : FieldType.type('Text'),
                        setting : {
                            hasTime : true
                        }
                    }
                },
                {
                    field : {
                        label : 'Valor',
                        name  : 'price',
                        type  : FieldType.type('Currency'),
                    }
				},
				{
                    field : {
                        label : 'Status',
                        name  : 'this',
                        type  : FieldType.type('VerifyPayment'),
                    }
                },
            ],
        });

        /* LOAD USER FORM */
        let params = this.createParams({
            appid : this.app.appid,
        });

        const resultForm = await formPlugin.getData(params);

		if(resultForm.collection.length > 0)
		{
			this.registerForm = resultForm.collection[0];
		}
		
		/* LOAD COURSES */
		params = this.createParams({
			appid   : 'financial',
			orderBy : 'postdate',
			asc     : false,
			where : [
			{
				field : 'student',
				value : this.core().user.reference
			}]
		});
		
		/**
		 * {
				field : 'payment.status',
				value : 'paid'
			}
		 */

		const resultCources = await documentPlugin.getData(params);
		await resultCources.collection.on();
		
		const financial = new DocumentCollection();
		const courses   = new DocumentCollection();

		resultCources.collection.forEach(item => 
		{
			if(item.payment.status == 'paid')
			{
				courses.add(item.course);
			}			
			
			financial.add(item);
		});

		await courses.on();

		this.courses   = courses;
		this.financial = financial;
	}	
	
	onViewer(event:any)
	{
		this.push('course', { 'appid':  'courses',  documentPath : event.data.reference.path });
	}

	onSet(event:any)
    {
		const documentPlugin = this.core().injector.get(DocumentPlugin);

		documentPlugin.set(this.app.appid, this.data, this.form, event).then(() =>
        {
            this.showAlert('Atenção', 'Dados atualizados com sucesso!');
		
			return this.registerForm.display();
        });	
    }   
}
