import { AbstractControl, ValidatorFn, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from "rxjs/Observable";
import * as moment from 'moment';
import validator from 'validator';

//https://dzone.com/articles/how-to-create-custom-validators-in-angular
import { Core }     from '../util/core/core';
import { Params }   from '../util/params/params';
import { Document } from '../model/document/document';

//@dynamic
export class InputValidate
{
    static loadingController : any;

    static search(field:any, form:any, component:any) : AsyncValidatorFn
    {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> =>
        {
            return new Promise<ValidationErrors>(async (resolve) =>
            {
                let type :any;

                if(field.single)
                {
                    type = 'single';
                }
                else if(field.setting && field.setting.autoComplete)
                {
                    if(form.isSetModeForm())
                    {
                        component.displayControl(true);
                    }

                    type = 'autoComplete';
				}
				
				//const valueChanges = await control.valueChanges.toPromise()

				//console.error(valueChanges, control.pristine)

                if (!control.valueChanges || control.pristine || !type)
                {
                    resolve(null);
                }
                else
                {
                    if(control.pristine || control.value == '')
                    {
                        resolve(null);
                    }
                    else
                    {
                        /* LOADING */
                        component.isLoading = true;
                        //Core.get().loadingController.start();

                        /* PARA NÃO FAZER VARIAS BUSCA AO DIGITAR */
                        if(component._searchChangeSetTimeout)
                        {
                            clearTimeout(component._searchChangeSetTimeout);
                        }

                        component._searchChangeSetTimeout = setTimeout(() =>
                        {
                            const index = Core.get().algoliaClient.initIndex(form.indexDocumentPath);

                            index.search('', {
                                filters : field.name + ":'" + control.value + "'"
                            })
                            .then((content:any) =>
                            {
                                console.log('search', content, component);

                                if(type == 'single')
                                {
                                    if(content.nbHits > 0)
                                    {
                                        /* VERIFICA SE ESTÁ SENDO EDITADO */
                                        if(component.data)
                                        {
                                            let has : boolean = false;

                                            for(const key in content.hits)
                                            {
                                                const data = content.hits[key];

                                                if(component.data.id == data.objectID)
                                                {
                                                    has = true;
                                                    break;
                                                }
                                            }

                                            if(has)
                                            {
                                                resolve(null);                                                
                                            }
                                            else
                                            {
                                                resolve({single : true})
                                            }
                                        }
                                        else
                                        {
                                            resolve({single : true})
                                        }
                                    }
                                    else
                                    {
                                        resolve(null);
                                    }
                                }
                                else if(type == 'autoComplete')
                                {
                                    if(content.nbHits > 0)
                                    {
                                        const document : any = new Document(content.hits[0]);

                                        document.on().then(() =>
                                        {
                                            component.displayControl(true);

                                            /* NO CASO DE AUTO COMPLETE DOS DADOS QUE ESTAO PREENCHIDOS TEM PRIORIDADE */
                                            for(const key in form.instance._initialData)
                                            {
                                                document[key] = form.instance._initialData[key];
                                            }

                                            /* PARAMS DATA TEM PRIORIDADE */
                                            if(Core.get().queryParams && Core.get().queryParams.data)
                                            {
                                                for(const key in Core.get().queryParams.data)
                                                {
                                                    document[key] = Core.get().queryParams.data[key];
                                                }
                                            }

                                            /* RESET FLAGS */
                                            document._archive  = false;
                                            document._canceled = false;

                                            /* ATUALIZA MANUALMENTE POIST TEM O RESET NO METODO DATA */
                                            form.formGroup.patchValue(document.parseData());
                                            form.formGroup.markAsPristine();
                                            form.component.instance.data = document;
                                            form.instance.isEdit = false;
                                            form.doAddModeForm();

                                            console.log('autoComplete', document.parseData());

                                            resolve(null);
                                        });
                                    }
                                    else
                                    {
                                        component.displayControl(true);
                                        resolve(null);
                                    }
                                }
                                else
                                {
                                    resolve(null);
                                }

                                /* LOADING */
                                component.isLoading = false;
                                //Core.get().loadingController.stop();
                            });

                        }, 500);
                    }
                }
            });
        };
    }

    static compare(name): ValidatorFn
    {
        return (c: AbstractControl): {[key: string]: any} =>
        {
            if(!c.value)
            {
                return null;
            }

            // self value (e.g. retype password)
            let v = c.value;

            // control value (e.g. password)
            let e = c.root.get(name);

            // value not equal
            if (e && v !== e.value) return {
                compare : true
            }

            return null;
        }
    }

    static url(name:string): ValidatorFn
    {
        name;

        return (c: AbstractControl): {[key: string]: any} =>
        {
            let v : string = c.value;

            if(/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(v))
            {
                return null;
            }
            else
            {
                return {
                    url : true
                }
            }
        }
    }

    static date(c: AbstractControl)
    {
        let date : any;

        
        if(!c.value)
        {
            return null;
        }
        else if(c.value.seconds)
        {
            date = moment(c.value.seconds);
        }
        else
        {
            date = moment(c.value);
        }

        if(date.isValid())
        {
            return null;
        }
        else
        {
            return {
                date : true
            }
        }
    }

    static creditCard(c: AbstractControl)
    {
        if(!c.value)
        {
            return null;
		}
		
		let v: string = c.value;
		
        return validator.isCreditCard(v) ? null : { creditCard : true};
    }

    static agree(formControl : AbstractControl)
    {
        if(!formControl.value)
        {
            return {
                agree : true,
            };
        }
        else
        {
            return null;
        }
    }

    static email(formControl : AbstractControl)
    {
        if(!formControl.value)
        {
            return null;
        }

        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if(!EMAIL_REGEXP.test(formControl.value))
        {
            return {
                email : true,
            };
        }
        else
        {
            return null;
        }
    }

    static cpf(c: AbstractControl)
    {
        if(!c.value)
        {
            return null;
        }

        let cpf = c.value.replace(/[^\d]+/g,'');
        if(cpf == '') return {
            cpf : true
        };
        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
                return {
                    cpf : true
                };
        // Valida 1o digito
        let add = 0;
        for (let i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
            let rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(9)))
                return {
                    cpf : true
                };
        // Valida 2o digito
        add = 0;
        for (let i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return {
                cpf : true
            };

        return null;
    }

    static cnpj(formControl: AbstractControl)
    {
        if(!formControl.value)
        {
            return null;
        }

        let c = formControl.value;
        let b = [6,5,4,3,2,9,8,7,6,5,4,3,2];

        if((c = c.replace(/[^\d]/g,"")).length != 14)
        {
            return {
                cnpj : true
            };
        }

        if(/0{14}/.test(c))
        {
            return {
                cnpj : true
            };
        }

        for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);

        if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        {
            return {
                cnpj : true
            };
        }

        for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);

        if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        {
            return {
                cnpj : true
            };
        }

        return null;
    }

    static createResult(params:Params)
    {
        return Core.get().resultFactory.create(params);
    }

    static createParams(args)
    {
        return Core.get().paramsFactory.create(args);
    }
}
