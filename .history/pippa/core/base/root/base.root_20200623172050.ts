import { AlertController } from '@ionic/angular';
import { PushNotificationToken, KeyboardInfo, AppState, PushNotification, PushNotificationActionPerformed, StatusBarStyle } from '@capacitor/core';
  
/* PIPPA */
import { BaseComponent } from '../base.component';
import { Core }          from '../../util/core/core';
import { Notification }  from '../../model/notification/notification';
import { EventPlugin }   from '../../../core/plugin/event/event.plugin';
import { Plugins } 		 from "../../../core/util/capacitor/capacitor";

const { PushNotifications, Keyboard, Network, SplashScreen, App, StatusBar, Device } = Plugins;

export abstract class BaseRoot extends BaseComponent
{
    public notifications : any = [];

    reload()
    {
        window.location.href = this.core().baseUrl;
    }

    core()
    {
        return Core.get()
	}
	
	hasMenu()
	{
		return this.core().menu && this.core().menu.length > 0
	}

    onLogout()
    {
        const eventPlugin = this.core().injector.get(EventPlugin);
        eventPlugin.dispatch('logout', this.core().user);
        eventPlugin.dispatch(this.core().user.appid + ':logout', this.core().user);
	}

	getBuiderClass()
	{
		return 'builder-' + (this.core().isOS ? 'ios' : 'android') + '-' + this.core().builder;
	}

	getMobileStyle()
	{
		if(this.core().account && this.core().account.mobileStyle)
		{
			return this.core().account.mobileStyle;
		}
		else
		{
			return;
		}
	}
	
	async runNotification(notification)
	{
		console.log('notification', notification);

		if(typeof notification.data.action == 'string')
		{
			notification.data.action = JSON.parse(notification.data.action);
		}

		if(notification.data.params && typeof notification.data.params == 'string')
		{
			notification.data.params = JSON.parse(notification.data.params);
		}

		if(notification.data.action.value == "confirm")
		{
			const alertController = this.core().injector.get(AlertController);

			const args : any = {};

			for(const key in notification.data.params)
			{
				args[notification.data.params[key].label] = notification.data.params[key].value;
			}

			const cancelText  = (args.cancelText  ? args.cancelText  : 'Rejeitar');
			const confirmText = (args.confirmText ? args.confirmText : 'Aceitar');	

			const alert = await alertController.create({
				header   		: notification.title,
				message  		: notification.data.description,
				backdropDismiss : false,
				buttons  : [
				{
					text    : cancelText,
					role    : 'cancel',
					handler : (blah) => 
					{
						
					}
				}, 
				{
					text    : confirmText,
					handler : () => 
					{						
						this.core().push(notification.data.router, args);
					}
				}]
			});
			
			await alert.present();
		}
		else if(notification.data.action.value == "alert")
		{
			const alertController = this.core().injector.get(AlertController);

			const alert = await alertController.create({
				header   		: notification.title,
				message  		: notification.data.description,	
				backdropDismiss : false,			
				buttons  : [
					{
						text    : 'Ok',
						role    : 'cancel',
						handler : (blah) => 
						{
							
						}
					},
				]
			});
			
			await alert.present();
		}

		this.notifications.push(notification);
	}

    async initializeCapacitor()
    {
        //SplashScreen
		SplashScreen.hide();

		/* STATUS BAR */
		StatusBar.setStyle({style: StatusBarStyle.Dark});
		//StatusBar.hide();
		
		/* APP */
		App.addListener('appStateChange', (state: AppState) => 
		{
			// state.isActive contains the active state
			console.log('App state changed. Is active?' + JSON.stringify(state));
		});

		/* INFO */
		const device 	   = await Device.getInfo();
		this.core().device = device; 

		/* IS OIS */
		this.core().isOS = device.platform == 'ios'; 

		/* PUSH */
		PushNotifications.register();
		
        PushNotifications.addListener('registration', (token: PushNotificationToken) => 
        {
            this.core().notification = new Notification({
				token  : token.value,
				device : this.core().device,
			}); 
			
            console.log('token ' + token.value);
        });

        PushNotifications.addListener('registrationError', (error: any) => 
        {
            console.log('error on register ' + JSON.stringify(error));
        });

        PushNotifications.addListener('pushNotificationReceived', (notification: PushNotification) => 
        {            
			this.runNotification(notification);
        });

        PushNotifications.addListener('pushNotificationActionPerformed', (data: PushNotificationActionPerformed) => 
        {
			data.notification.title = data.notification.data.title;
			data.notification.body  = data.notification.data.body;

			delete data.notification.data.title;
			delete data.notification.data.body;

			this.runNotification(data.notification);
		});	
		
		PushNotifications.requestPermission().then((result) => 
		{
			console.log('requestPermission', result);
		}); 

        /* KEYBOAR */
        Keyboard.addListener('keyboardDidShow', (info: KeyboardInfo) =>
        {
            document.body.classList.add('keyboard-is-open');
            console.log('keyboard did show with height', info.keyboardHeight);
        });

        Keyboard.addListener('keyboardDidHide', () =>
        {
            document.body.classList.remove('keyboard-is-open');
            console.log('keyboard did hide');
        });

        /* NETWORK */
        Network.addListener('networkStatusChange', (status) =>
        {
            console.log("Network status changed", status);

            if(!status.connected)
            {
                const alertController = this.core().injector.get(AlertController);

                alertController.create({
                    header  : 'Alerta',
                    message : 'Internet não contectada',
                    buttons : ['OK']
                })
                .then((alert:any) =>
                {
                    alert.present();
                });
            }
        });

        Network.getStatus().then(status =>
        {
            console.log("Network status changed", status);

            if(!status.connected)
            {
                const alertController = this.core().injector.get(AlertController);

                alertController.create({
                    header  : 'Alerta',
                    message : 'Internet não contectada',
                    buttons : ['OK']
                })
                .then((alert:any) =>
                {
                    alert.present();
                });
            }
        });
    }
}
