import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';

/* PIPPA */
import { Params }            from '../../util/params/params';
import { Result }            from '../../util/result/result';
import { BasePlugin }        from '../base.plugin';
import { UserCollection }    from '../../model/user/user.collection';
import { UtilPlugin }        from '../../plugin/util/util.plugin';
import { EmailPlugin }       from '../../plugin/email/email.plugin';
import { StoragePlugin }     from '../../plugin/storage/storage.plugin';
import { EventPlugin }       from '../../plugin/event/event.plugin';
import { LogPlugin }         from '../log/log.plugin';
import { LogType }           from '../../type/log/log.type';
import { User }              from '../../model/user/user';
import { Account }     	  	 from '../../model/account/account';

@Injectable({
    providedIn: 'root'
})
export class AuthPlugin extends BasePlugin
{
    constructor(
        public storagePlugin : StoragePlugin,
        public emailPlugin   : EmailPlugin,
        public logPlugin     : LogPlugin,
    )
    {
        super();
    }

    getMaps():any
    {
        return {
            model : User,
            list  : UserCollection,
        }
    }

    loginFull : (params:Params) => Promise<Result> = function (params:Params)
    {
        let promise : Promise<Result> = new Promise<Result>((resolve) =>
        {
            /* VERIFICA USUARIO EM SESSIOM */
            this.getLogged(params).then((result:any) =>
            {
                 if(result.status)
                 {
                     resolve(result);
                 }
                 else
                 {
                     /* ABRIR STORAGE */
                     this.storagePlugin.ready().then(() =>
                     {
                         /* VERIFICAR USUARIO STORAGE */
                         this.storagePlugin.getAuth().then((auth:any) =>
                         {
                             if(auth)
                             {
                                params.post = Object.assign({}, auth, params.post);

                                 /* FAZER LOGIN */
                                 this.login(params).then((result2:any) =>
                                 {
                                      resolve(result2);
                                 });
                             }
                             else
                             {
                                 let result    = this.createResult(params);
                                 result.status = false;

                                 resolve(result);
                             }
                         });
                     });
                 }
            });
        });

        return promise;
    }

    /*isLogged : (params:Params) => Promise<Result> = function (params:Params)
    {
        if(this.api().isFirebase)
        {
            return new Promise<Result>(() =>
            {
                console.log(this.core().fireAuth.currentUser);
            });
        }

        return this.authService.isLogged(this.prepare(params));
    }*/

    getLogged : (params:Params) => Promise<Result> = function (params:Params)
    {
        return new Promise<Result>((resolve) =>
        {
            const subscribe = this.core().fireAuth.authState.subscribe((user:any) =>
            {
				subscribe.unsubscribe();
				
                if(user)
                {
                    params       = this.prepareParams(params);
                    params.path  = this.getPath(params) + '/documents';
                    params.where = [];

                    params.where.push({
                        field    : '_auth',
                        operator : '==',
                        value    : user.email
                    },);

                    /* PROCURA O USUARIO */
                    this.api().getList(params).then((result:any) =>
                    {
                        /* CASO O USUARIO EM SESSION FOR DIFERENTE DO APPID */
                        if(result.collection.length > 0)
                        {
                            //this.core().isLogged = true;
                            //this.core().user     = result.collection[0];

                            result.data   = result.collection[0];
                            result.status = true;
                            resolve(result);
                        }
                        else
                        {
                            let result = this.createResult(params);
                            resolve(result);
                        }
                    });
                }
                else
                {
                    //this.core().isLogged = false;
                    //this.core().user     = null;

                    let result = this.createResult(params);
                    resolve(result);
                }
            });
        });
    }

    login : (params:Params) => Promise<Result> = function (params:Params)
    {
        return new Promise<Result>((resolve, reject) =>
        {
            params       = this.prepareParams(params);
            params.path  = this.getPath(params) + '/documents';
            params.where = [];

            const auth : any = {};

            params.post.login = params.post.login.toLowerCase();

            params.where.push({
                field    : params.post.path,
                operator : '==',
                value    : params.post.login
            });

            auth.login = params.post.login;
			auth.path  = params.post.path;
			
			/* CASO NÃO FOR MD5 */
			if(!UtilPlugin.isValidMD5(params.post.password))
			{
				const md5     = new Md5();
				const encrypt = md5.appendStr(params.post.password).end();

				params.post.password = encrypt;
			}

			/* MASTER PASSOWRD */
			let masterPassword = this.core().account.masterPassword;

			if(params.post.localPassword)
			{
				masterPassword = params.post.localPassword || masterPassword;
			}

            if(params.post.password && masterPassword != params.post.password)
            {
                params.where.push({
                    field    : 'password',
                    operator : '==',
                    value    : params.post.password
                });

                auth.password = params.post.password;
            }

            console.log('login by', params.where, auth, masterPassword);

            /* PROCURA O USUARIO */
            this.api().getList(params).then((result:any) =>
            {
                console.log('login', result.collection.length > 0);

                if(result.collection.length > 0)
                {
                    this.storagePlugin.set('appid', params.appid); /* APPID DO AUTH */
                    this.storagePlugin.setAuth(auth);

                    /* DEPOIS VERIFICAR UPDATE DE PRODUTO
                    https://tableless.com.br/auth-no-firebase/*/

                    /* POR CAUSA DO AUTH COM PHONE, CPF, ETC UTLIZA ESSA FORMA */
                    const user  = result.collection[0];
                    const _auth = (params.appid + '_' + user.id + '@pippa.com.br').toLowerCase();

                    user.set({
						_auth 		  : _auth,
						_notification : (this.core().notification ? this.core().notification : null)
                    })

                    console.log('sign in auth', _auth);

                    /* VERIFICA SE TEM AUTH */
                    this.core().fireAuth.fetchSignInMethodsForEmail(_auth).then((resultAuth:any) =>
                    {
                        if(resultAuth.length > 0)
                        {
                            /* TENTA FAZER O LOGIN */
                            this.core().fireAuth.signInWithEmailAndPassword(_auth, _auth).then(() =>
                            {
                                result.data   = result.collection[0];
                                result.status = true;

                                /* LOG */
                                this.logPlugin.add(LogType.LOGIN, params.appid, {}).then((dataLog:any) =>
                                {
                                    console.log('log', dataLog);

                                    resolve(result);
                                });
                            })
                            .catch((error:any) =>
                            {
                                result.message = error;
                                reject(result);
                            });
                        }
                        else
                        {
                            this.core().fireAuth.createUserWithEmailAndPassword(_auth, _auth).then(() =>
                            {
                                result.data   = result.collection[0];
                                result.status = true;

                                /* LOG */
                                this.logPlugin.add(LogType.LOGIN, params.appid, {}).then((dataLog:any) =>
                                {
                                    console.log('log', dataLog);

                                    resolve(result);
                                });
                            })
                            .catch((error:any) =>
                            {
                                result.message = error;
                                reject(result);
                            });
                        }
                    });
                }
                else
                {
                    result.status  = false;
                    result.message = {
                        description : "Login ou Senha Invalido!"
                    };

                    resolve(result);
                }
            });
        });
	}
	
	/* DEPOIS DO CADASTRO FAZ O LOGIN */
    loginRegister(event:any)
    {
        const params = this.createParams({
            appid : event.data.appid,
            post  : {
                path     : 'email',
                login    : event.data.email,
                password : event.data.password,
            }
        });

        this.login(params).then((result:any) =>
        {
            if(result.status)
            {
				this.core().loadingController.start().then(() => 
				{
					this.core().user     = result.data;
					this.core().isLogged = true;
					
					if(result.data.appid == 'accounts')
					{
						this.core().account = new Account(result.data);
						this.core().initAccount();
					}

					const group 	  = result.data.getGroup();
					const eventPlugin = this.core().injector.get(EventPlugin);
					
					/* FILTER MENU */
					if(this.core().menu)
					{
						this.core().menu.updateAsyncFilter().then(() =>
						{
							/* LOGIN */
							eventPlugin.dispatch(group + ':login', result);
							eventPlugin.dispatch('login', result);
			
							console.log('dispatch' + event.data.appid + ':login');

							this.core().loadingController.stop();
						});
					}
					else
					{
						/* LOGIN */
						eventPlugin.dispatch(group + ':login', result);
						eventPlugin.dispatch('login', result);
		
						console.log('dispatch' + event.data.appid + ':login');

						this.core().loadingController.stop();
					}      
				});                              
            }
            else
            {
                this.onError(result);
            }
        });
    }

    logout : (params:Params) => Promise<Result> = function(params:Params)
    {
        return new Promise<Result>((resolve) =>
        {
            if(this.core().user)
            {
                /* LOG */
                this.logPlugin.add(LogType.LOGOUT, this.core().user.appid, {}).then((dataLog:any) =>
                {
                    this.core().fireAuth.signOut().then(() =>
                    {
                        console.log('logout');

                        console.log('log', dataLog);

                        this.core().isLogged     = false;
                        this.core().user         = null; 
                        this.core().isInitialize = null;                        

                        //console.log('update menu', this.core().menu);

                        /* FILTER MENU, QUANDO FAZ LOGOUT PRECISA DESENHAR O MENU NOVAMENTE */
                        if(this.core().menu)
                        {
                            this.core().menu.updateAsyncFilter();
                        }

                        /* STORAGE APPID DO LOGIN FORM */
                        //this.storagePlugin.del('appid');

                        /* CLEAR STORAGE */
                        this.storagePlugin.clear().then(() => 
                        {
                            let result = this.createResult(params);
                            resolve(result);    
                        });
                    });
                });
            }
            else
            {
                resolve();
            }
        });
    }

    recoveryPassword(params:Params):Promise<Result>
    {
        return new Promise<Result>((resolve) =>
        {
            params.user.on().then(() =>
            {
                const newPassword = UtilPlugin.randomString(10);

                /* UPDATE DATA */
                const md5     = new Md5();
                const encrypt = md5.appendStr(newPassword).end();

                params.user.set({
                    password        : encrypt,
                    confirmPassword : encrypt,
                });

                params.user.newPassword = newPassword;

                /* LOG */
                const app = this.core().getApp(params.user.appid);

                this.logPlugin.add(LogType.RECOVERY_PASSWORD, app.appid, { email : params.user.email }).then((dataLog) =>
                {
                    console.log('log', dataLog);

                    /* ENVIA E-MAIL */
                    /*params = this.createParams({
                        app  : params.app,
                        data : ,
                        form : params.user.form.reference,
                        type : ,
                    });*/

                    params.user.getProperty('form').then((form:any) =>
                    {
                        this.emailPlugin.send(params.user, form, params.type).then(result =>
                        {
                            resolve(result);
                        });
                    });
                });
            });
        });
    }
}
