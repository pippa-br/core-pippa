import { Injectable } from '@angular/core';
import { AlertController, ToastController, ModalController } from '@ionic/angular';
import cleanDeep from 'clean-deep';
import * as moment from 'moment';

/* PIPPA */
import { Document }             from '../../model/document/document';
import { DocumentCollection }   from '../../model/document/document.collection';
import { EventPlugin }          from '../../../core/plugin/event/event.plugin';
import { NotificationPlugin }   from '../../../core/plugin/notification/notification.plugin';
import { BasePlugin }           from '../base.plugin';
import { Params }               from '../../util/params/params';
import { Result }               from '../../util/result/result';
import { EmailPlugin }          from '../email/email.plugin';
import { LogPlugin }            from '../log/log.plugin';
import { GatewayPlugin }        from '../gateway/gateway.plugin';
import { StripePlugin }         from '../stripe/stripe.plugin';
import { UtilPlugin }           from '../util/util.plugin';
import { LogType }              from '../../type/log/log.type';
import { SumFormTransform }     from '../../component/transform/sum.form/sum.form.transform';
import { TransactionType } 	    from '../../type/transaction/transaction.type';

@Injectable({
    providedIn: 'root'
})
export class DocumentPlugin extends BasePlugin
{
    public emailPlugin        : EmailPlugin;
    public logPlugin          : LogPlugin;
    public gatewayPlugin      : GatewayPlugin;
    public alertController    : AlertController;
    public toastController    : ToastController;
	public eventPlugin        : EventPlugin;
	public notificationPlugin : NotificationPlugin;

    constructor(
    )
    {
        super();

		this.emailPlugin        = this.core().injector.get(EmailPlugin);
		this.notificationPlugin = this.core().injector.get(NotificationPlugin);
        this.logPlugin          = this.core().injector.get(LogPlugin);
        this.gatewayPlugin      = this.core().injector.get(GatewayPlugin);
        this.alertController    = this.core().injector.get(AlertController);
        this.toastController    = this.core().injector.get(ToastController);
        this.eventPlugin        = this.core().injector.get(EventPlugin);
    }

    getMaps():any
    {
        return {
            model : Document,
            list  : DocumentCollection,
        }
    }

    get(params:Params):Promise<Result>
    {
        params      = this.prepareParams(params);
        params.path = this.getPath(params) + '/documents/' + params.id;

        return this.api().getObject(params);
    }

    add(appid:string, form:any, event:any)
    {
        return new Promise((resolve) =>
        {
            /* LOAD */
            this.core().loadingController.start().then(() =>
            {
                if(!event.data.projectID)
                {
                    event.data.projectID = this.core().account.projectID;
                }

                if(!event.data.accid)
                {
                    event.data.accid = this.core().account.accid;
                }

                if(!event.data.appid)
                {
                    event.data.appid = appid;
                }

                if(form.reference)
                {
                    event.data.form = form.reference;

                    /* CODE FORM */
                    if(form.code != undefined)
                    {
                        event.data.codeForm  = form.code;
                    }

                    /* LEVEL FORM */
                    if(form._level != undefined && event.data._level == undefined)
                    {
                        event.data._level  = form._level;
                    }
                }

                if(!event.data.postdate)
                {
                    event.data.postdate = moment().toDate();
                }

                if(!event.data.lastdate)
                {
                    event.data.lastdate = moment().toDate();
                }

                if(!event.data.order)
                {
                    event.data.order = moment().unix();
                }

                if(!event.data._lock)
                {
                    event.data._lock = 'desktop';
                }

                if(!event.data._canceled)
                {
                    event.data._canceled = false;
                }

                if(!event.data._archive)
                {
                    event.data._archive = false;
                }    

                if(this.core().user && !this.core().user.isGuest())
                {
                    if(!event.data.owner)
                    {
                        event.data.owner = this.core().user.reference;
                    }
                    else if(typeof event.data.owner == 'string')
                    {
                        event.data.owner = this.core().fireStore.doc(event.data.owner).ref
                    }

                    if(!event.data.user)
                    {
                        event.data.user = this.core().user.reference;
                    }
                    else if(typeof event.data.user == 'string')
                    {
                        event.data.user = this.core().fireStore.doc(event.data.user).ref
                    }
                }

                /* GET APP */
                const app : any = this.core().getApp(appid);
                
                app.nextSequence().then((sequence:number) =>
                {
                    /* ID SEQUENCE */
                    event.data._sequence = sequence;

                    /* GET COLLECTION */
                    let params = this.createParams({
                        appid : appid,
                    });

                    this.getReference(params).then((result:any) =>
                    {
                        const collection = result.collection;

                        /* LIMPA OBJETOS VAZIO */
						//event.data = cleanDeep(event.data);
						
						const dataset = cleanDeep(event.data._logs);			
						delete event.data._logs;

                        collection.save(event.data).then(async (data:any) =>
                        {
							/* AFTER ADD */
							if(app.api)
							{
								const callable   = this.core().fireFunctions.httpsCallable(app.api + '/afterAdd');
								const counResult = await callable({path:params.path}).toPromise();									
							}

                            data.on().then(() =>
                            {
                                /* GENERATE NAME */
								data.updateName();
								
								//const dataset = data.parseData()

                                /* LOG */
                                this.logPlugin.add(LogType.ADD, appid, dataset, data.reference).then((dataLog) =>
                                {
                                    console.log('add log', dataLog);
                                });

                                /* SEARCH */
                                data.createIndex().then((event2:any) =>
                                {
                                    console.log('indexer', event2);
                                });

                                console.log('gatewayPlugin', form);

                                /* SUM FORM */
                                this.sumForm(form.sumForm, data).then(() =>
                                {
                                    /* GATEWAY */
                                    this.gatewayPlugin.createPayment(data, form).then((resultPayment:any) =>
                                    {
										console.log('result payment', resultPayment);

                                        /* ADICIONA O LINK */
                                        data.linkInversedBy();

										/* SEND EMAIL */
                                        this.emailPlugin.send(data, form, TransactionType.ADD_TYPE).then(() =>
                                        {
											/* SEND NOTIFICATIOON */
											this.notificationPlugin.send(data, form, TransactionType.ADD_TYPE).then(() =>
                                        	{
												/* EVENTS */
												this.eventPlugin.dispatch(appid + ':add', data);

												/* LOAD */
												//this.core().loadingController.stop();

												/* REDIRECT VIEWER PAYMENT */
												if(resultPayment)
												{
													resolve(resultPayment)
												}
												/* EXIBIR MENSAGEM */
												else if(form && form.addMessage && form.addMessage != '')
												{
													UtilPlugin.parseVariables(data, form.addMessage).then(message =>
													{
														resolve({
															message : message,	
														})
													});
												}
												/* REDIRECIONAR */
												else if(form && form.redirectRouter)
												{
													resolve({
														redirectRouter : form.redirectRouter,
														redirectParams : {
															documentPath : data.reference.path
														},
													})
												}
												/* EXIBIR NOTIFICATION */
												else if(form && form.saveFixed)
												{
													this.doToast();
													resolve(data);
												}
												else
												{
													resolve(data);
												} 
											});                                                                                   
                                        });
                                    });
                                });
                            });
                        });
                    });      
                });
            });
        });
    }

    set(appid:string, data:any, form:any, event:any)
    {
        return new Promise((resolve) =>
        {
            /* LOAD */
            this.core().loadingController.start().then(() =>
            {
                if(!event.data.projectID)
                {
                    event.data.projectID = this.core().account.projectID;
                }

                if(!event.data.accid)
                {
                    event.data.accid = this.core().account.accid;
                }

                if(!event.data.appid)
                {
                    event.data.appid = appid;
                }

                if(!event.data.lastdate)
                {
                    event.data.lastdate = moment().toDate();
                }

                if(this.core().user && !this.core().user.isGuest())
                {
                    if(!event.data.user)
                    {
                        event.data.user = this.core().user.reference;
                    }
                }

                /* LOG */
                //const oldData = data.parseData();
                //const newData = cleanDeep(event.data);
				const dataset    = cleanDeep(event.data._logs);			
				delete event.data._logs;

                /*for(const key in newData)
                {
                    if(newData[key] != oldData[key])
                    {
                        dataset[key] = {
                            from : (oldData[key] == undefined ? null : oldData[key]),
                            to   : (newData[key] == undefined ? null : newData[key]),
                        };
                    }
				}*/
				
				console.log('dataset', dataset);

                /* LOG */
                this.logPlugin.add(LogType.SET, appid, dataset, data.reference).then((dataLog) =>
                {
                    console.log('add log', dataLog);
                });

                /* REMOVE O LINK PRIMEIRO, PARA O ITEM ANTERIOR */
                data.unlinkInversedBy().then(() =>
                {
                    this.doSet(data, event).then((data2:any) =>
                    {
                        data2.on().then(() =>
                        {
                            /* GENERATE NAME */
                            data.updateName();

                            /* ADICIONA O LINK */
                            data2.linkInversedBy();

                            /* SEARCH */
                            data2.createIndex().then((event2:any) =>
                            {
                                console.log('indexer', event2);
                            });

                            /* SUM FORM */
                            this.sumForm(form.sumForm, data2).then(() =>
                            {
                                console.log('gatewayPlugin', form);

                                /* GATEWAY */
                                this.gatewayPlugin.createPayment(data2, form).then(() =>
                                {
                                    /* SEND E-MAIL */
                                    this.emailPlugin.send(data2, form, TransactionType.SET_TYPE).then(() =>
                                    {
										/* SEND NOTIFICATIOON */
										this.notificationPlugin.send(data, form, TransactionType.SET_TYPE).then(() =>
										{
											/* EVENT */
											this.eventPlugin.dispatch(appid + ':set', data2);

											/* LOAD */
											this.core().loadingController.stop();

											/* EXIBIR MENSAGEM */
											if(form && form.setMessage && form.setMessage != '')
											{
												UtilPlugin.parseVariables(data2, form.setMessage).then(message =>
												{
													this.alertController.create({
														header  : 'Atenção',
														message : message
													})
													.then(alert =>
													{ 
														alert.present();
													});
												});

												resolve(data2);
											}
											else
											{
												resolve(data2);
											}
										});                                        
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }

    /* PARA OVERRRIDER */
    doSet(data:any, event:any)
    {
        return data.set(event.data);
    }

    del(item:any):Promise<Result>
    {
        return new Promise((resolve) =>
        {
            /* LOG */
            const app = this.core().getApp(item.appid);

            this.logPlugin.add(LogType.DEL, app.appid, item.parseData()).then((dataLog) =>
            {
                console.log('log', dataLog);
            });

            /* SEARCH */
            item.delIndex([]).then((event2:any) =>
            {
                console.log('del index', event2);
            });

            item.del();

            resolve();
        });
    }

    sumForm(sum:boolean, data:any)
    {
        return new Promise((resolve:any) =>
        {
            if(sum)
            {
                SumFormTransform.transform(null, data).then((output:any) =>
                {
                    data._sumForm = {
                        amount     : output.amount,
                        total      : output.total,
                        count      : output.count,
                        percentage : output.percentage,
                    };

                    /* SAVE */
                    data.save({
                        _sumForm : data._sumForm
                    })

                    resolve(data);
                });
            }
            else
            {
                resolve(data);
            }
        });
    }

    copy(params:Params):Promise<Result>
    {
        return new Promise((resolve) =>
        {
            let result = this.createResult(params);

            if(params.post.fromPath != params.post.toPath)
            {
                let fromMode = params.post.fromPath.split('/').length % 2;
                let toMode   = params.post.toPath.split('/').length   % 2;

                console.log(params.post.fromPath, fromMode);
                console.log(params.post.toPath,   toMode);

                if(fromMode == toMode)
                {
                    /* LIST */
                    if(fromMode == 1)
                    {
                        /* FROM PARAMS */
                        params      = this.prepareParams(params);
                        params.path = params.post.fromPath;

                        this.api().getList(params).then((fromResult:any) =>
                        {
                            /* TO PARAMS */
                            params      = this.prepareParams(params);
                            params.path = params.post.toPath;

                            this.api().getList(params).then((toResult:any) =>
                            {
                                for(let key in toResult.collection)
                                {
                                    let item = toResult.collection[key];
                                    item.del();
                                }

                                for(let key in fromResult.collection)
                                {
                                    let item = fromResult.collection[key];
                                    toResult.collection.save(item._data);
                                }

                                result.collection = toResult.collection;
                                result.status     = true;
                                resolve(result);
                            });
                        });
                    }
                    /* DOC - 0 */
                    else
                    {
                        /* FROM PARAMS */
                        params      = this.prepareParams(params);
                        params.path = params.post.fromPath;

                        this.api().getObject(params).then((fromResult:any) =>
                        {
                            /* TO PARAMS */
                            params      = this.prepareParams(params);
                            params.path = params.post.toPath;

                            this.api().getObject(params).then((toResult:any) =>
                            {
                                toResult.data.set(fromResult.data.parseData());

                                result.data   = toResult.data;
                                result.status = true;
                                resolve(result);
                            });
                        });
                    }
                }
                else
                {
                    result.message = 'Caminhos não compatíveis! Collection / Docs';
                    resolve(result);
                }
            }
            else
            {
                result.message = 'Os campos De e Para não pode ser iguais.';
                resolve(result);
            }
        });
    }

    async doToast()
    {
        const toast = await this.toastController.create({
            message         : 'Formulário Enviado!',
            //showCloseButton : true,
            position        : 'top',
            duration        : 1000,
            //closeButtonText : 'X'
        });

        toast.present();
    }
}
