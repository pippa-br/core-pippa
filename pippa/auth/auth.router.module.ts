export const FormRegisterRoutes = [
    {
        path         : "auth-login",
        loadChildren : "./page/login/login.module#LoginModule"
    },
    {
        path         : "auth-logout",
        loadChildren : "./page/logout/logout.module#LogoutModule"
    },
    {
        path         : "auth-viewer-profile",
        loadChildren : "./page/viewer.profile/viewer.profile.module#ViewerProfileModule"
    },
    {
        path         : "auth-change-profile",
        loadChildren : "./page/change.profile/change.profile.module#ChangeProfileModule"
    },
    {
        path         : "auth-logout-profile",
        loadChildren : "./page/logout.profile/logout.profile.module#LogoutProfileModule"
    },
    {
        path         : "auth-recovery-password",
        loadChildren : "./page/form.recovery.password/form.recovery.password.module#FormRecoveryPasswordModule"
    },
    {
        path         : "auth-change-password",
        loadChildren : () => import("./page/form.change.password/form.change.password.module").then(m => m.FormChangePasswordModule)
    },
    {
        path         : "form-register",
        loadChildren : () => import("./page/form.register/form.register.module").then(m => m.FormRegisterModule)
    },
];
