import { Component, EventEmitter, Output, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, Input } from "@angular/core";

/* PIPPA */
import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[login-form]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             (keyup.enter)="onEnter()"
                             (add)="onLogin($event)"
                             (set)="onLogin($event)"
                             (close)="onCancel()"
                             class="login-form">
                        </form>`,
})
export class LoginForm extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    @Output("login") loginEvent    : EventEmitter<any> = new EventEmitter();

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }

    display()
    {
        return this.dynamicForm.display();
    }

    onEnter()
    {
        this.dynamicForm.save();
    }

    onLogin(event:any)
    {
        this.loginEvent.emit(event);
    }

    onCancel()
    {
        super.onCancel();
    }
}
