import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-register]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             (add)="onAdd($event)"
                             (close)="onCancel()"
                             class="register-form">
                        </form>`,
})
export class FormRegister extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    display()
    {
        this.dynamicForm.display();
    }
	
    destroy()
    {
        this.dynamicForm.destroy();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }    
}
