import { Component, EventEmitter, Output, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseForm }  from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-change-password]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             (add)="onSave($event)"
                             (close)="onCancel()"
                             class="login-form">
                      </form>`,
})
export class FormChangePassword extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @Output("save") saveEvent : EventEmitter<any> = new EventEmitter();

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }

    onSave(event:any)
    {
        this.saveEvent.emit(event);
    }

    onCancel()
    {
        super.onCancel();
    }
}
