import { Component, EventEmitter, Output, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseForm }  from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-recovery-password]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             (add)="onRecovery($event)"
                             (close)="onCancel()"
                             class="login-form">
                      </form>`,
})
export class FormRecoveryPassword extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @Output("recovery") recoveryEvent : EventEmitter<any> = new EventEmitter();

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }

    onRecovery(event:any)
    {
        this.recoveryEvent.emit(event);
    }

    onCancel()
    {
        super.onCancel();
    }
}
