import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";
import { PopoverController } from "@ionic/angular";

/* PIPPA */
import { BasePage }      from "../../../core/base/page/base.page";
import { ViewerPlugin }  from "../../../core/plugin/viewer/viewer.plugin";
import { EventPlugin }   from "../../../core/plugin/event/event.plugin";
import { DynamicViewer } from "../../../core/dynamic/viewer/dynamic.viewer";

@Component({
    selector        : "viewer-profile-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_viewer';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start" *ngIf="core().menu && core().menu.length > 0">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                </ion-buttons>
                                <ion-title>                                    
									{{'Meus Dados' | translate }}
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>

                        <ion-content class="ion-padding">

                            <div dynamic-viewer
                                 #dynamicViewer
                                 [hidden]="!this._isRendererComplete"
                                 [data]="data"
                                 [viewer]="_viewer"
                                 (rendererComplete)=onRendererComplete()>
                            </div>

                            <div class="buttons" *ngIf="this._isRendererComplete">
                                <ion-button expand="block"
                                            fill="outline"
                                            color="medium"
                                            *ifAcl="'set_perfil';acl:acl;value:true;"
                                            (click)="onSetForm()">
                                    <ion-icon name="create-outline"></ion-icon> {{'Editar Perfil' | translate }}
                                </ion-button>
                                <ion-button expand="block"
                                            fill="outline"
                                            color="medium"
                                            *ifAcl="'change_password_perfil';acl:acl;value:false;"
                                            (click)="onChangePassword()">
                                    <ion-icon name="create-outline"></ion-icon> {{'Alterar Senha' | translate }}
                                </ion-button>
                                <ion-button expand="block" color="danger" (click)="onLogout()">
                                    <ion-icon name="power-outline"></ion-icon> {{'Sair' | translate }}
                                </ion-button>
                            </div>

                        </ion-content>`,
})
export class ViewerProfilePage extends BasePage
{
    @ViewChild("dynamicViewer", { static : true }) dynamicViewer : DynamicViewer;

    public _viewer : any;
    public _isRendererComplete  = false;

    constructor(
        public viewerPlugin      : ViewerPlugin,
        public eventPlugin       : EventPlugin,
        public popoverController : PopoverController,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.dynamicViewer.destroy();
    }

    onRendererComplete()
    {
        this._isRendererComplete = true;
    }

    initialize()
    {
        this.core().user.on(true).then(() =>
        {
            this.data = this.core().user;

            /* LOAD VIEWER */
            const params = this.createParams({
                appid : this.data.appid,
            });

            this.viewerPlugin.getData(params).then((result:any) =>
            {
                /* VIEWER */
                if (result.collection.length > 0)
                {
                    this._viewer           = result.collection[0];
                    this._viewer.hasHeader = false;
                }
                else
                {
                    console.error("viewer not found");
                }

                this.changeDetectorRef.markForCheck();
            });
        });
    }

    onSetForm()
    {
        this.push("form-document",
            {
                appid    : this.data.appid,
                dataPath : this.data.reference.path
            });

        /*const unsubscribe = this.navController.viewDidLoad.subscribe(event =>
        {
            event.instance.setEvent.subscribe(event2 =>
            {
                this.data.populate(event2.data);

                console.log(this.data);
            });

            unsubscribe.unsubscribe();
        });*/
    }

    onChangePassword()
    {
        this.push("auth-change-password",
            {
                appid : this.data.appid,
            });
    }

    onLogout()
    {
        this.eventPlugin.dispatch("logout", this.core().user);
    }
}
