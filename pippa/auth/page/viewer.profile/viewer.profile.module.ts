import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule } from "../../../core";
import { ViewerProfilePage } from "./viewer.profile.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ViewerProfilePage } ])
    ],
    declarations : [
        ViewerProfilePage,
    ],
})
export class ViewerProfileModule 
{}
