import { Component } from "@angular/core";

/* PIPPA */
import { BasePage }      from "../../../core/base/page/base.page";
import { EventPlugin }   from "../../../core/plugin/event/event.plugin";
import { StoragePlugin } from "../../../core/plugin/storage/storage.plugin";

@Component({
    selector : ".change-profile",
    template : "<ng-template></ng-template>",
})
export class ChangeProfilePage extends BasePage
{
    constructor(
        public eventPlugin   : EventPlugin,
        public storagePlugin : StoragePlugin,
    )
    {
        super();
    }

    loadData()
    {
        console.log("change profile", this.data);

        this.core().currentProfile = this.core().user;
        this.core().profiles.push(this.core().user);

        this.core().user         = this.data;
        this.core().userProfile  = true;
        this.core().routeProfile = this.core().navController.routes[this.core().navController.routes.length - 2];

        /* PARA QUANDO ALTERAR O PERFIL NÃO TER HISTORICO */
        this.core().navController.routes = [];

        /* CLEAR STORAGE */
        this.storagePlugin.clear().then(() => 
        {
            /* FILTER MENU */
            if (this.core().menu)
            {
                this.core().menu.updateAsyncFilter().then(() =>
                {
                    this.eventPlugin.dispatch(this.core().user.appid + ":login", { data : this.core().user });
                    this.eventPlugin.dispatch("login", { data : this.core().user });
                })
            }
            else
            {
                this.eventPlugin.dispatch(this.core().user.appid + ":login", { data : this.core().user });
                this.eventPlugin.dispatch("login", { data : this.core().user });
            }
        });        
    }
}
