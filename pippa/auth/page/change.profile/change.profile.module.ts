import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }         from "../../../core";
import { ChangeProfilePage }  from "./change.profile.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ChangeProfilePage } ])
    ],
    declarations : [
        ChangeProfilePage,
    ],
})
export class ChangeProfileModule 
{}
