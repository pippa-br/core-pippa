import { Component, ViewChild, Directive, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage } from "../../../core/base/page/base.page";

@Directive()
export class BaseUpdate extends BasePage
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    initialize()
    {
        this.changeDetectorRef.markForCheck();
    }    
}
