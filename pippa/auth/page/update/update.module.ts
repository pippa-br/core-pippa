import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule } from "../../../core";
import { UpdatePage } from "./update.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : UpdatePage } ])
    ],
    declarations : [
        UpdatePage,
    ]
})
export class UpdateModule 
{
}
