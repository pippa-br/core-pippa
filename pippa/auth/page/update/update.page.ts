import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseUpdate } from "./base.update";

@Component({
    selector        : "update-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl     : "update.page.html",
})
export class UpdatePage extends BaseUpdate
{
}
