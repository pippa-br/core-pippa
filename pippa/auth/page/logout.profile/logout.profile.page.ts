import { Component, ElementRef } from "@angular/core";

/* PIPPA */
import { BasePage }      from "../../../core/base/page/base.page";
import { EventPlugin }   from "../../../core/plugin/event/event.plugin";
import { StoragePlugin } from "../../../core/plugin/storage/storage.plugin";

@Component({
    selector : "logout-profile",
    template : "<ng-template></ng-template>",
})
export class LogoutProfilePage extends BasePage
{
    constructor(
        public eventPlugin   : EventPlugin,
        public storagePlugin : StoragePlugin,
        public elem          : ElementRef
    )
    {
        super();

        this.core().user = this.core().profiles.pop();

        if (this.core().profiles.length == 0)
        {
            this.core().userProfile    = false;
            this.core().currentProfile = null;
        }
        else
        {
            this.core().currentProfile = this.core().profiles[this.core().profiles.length - 1 ];
        }

        /* CLEAR STORAGE */
        this.storagePlugin.clear().then(() => 
        {
            /* FILTER MENU */
            if (this.core().menu)
            {
                this.core().menu.updateAsyncFilter().then(() =>
                {
                    this.core().push(this.core().routeProfile.router, this.core().routeProfile.queryParams, this.core().routeProfile.routeParams);
                });
            }
            else
            {
                this.core().push(this.core().routeProfile.router, this.core().routeProfile.queryParams, this.core().routeProfile.routeParams);
            }
        });        
    }
}
