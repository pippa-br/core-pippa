import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }        from "../../../core";
import { LogoutProfilePage } from "./logout.profile.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : LogoutProfilePage } ])
    ],
    declarations : [
        LogoutProfilePage,
    ],
})
export class LogoutProfileModule 
{}
