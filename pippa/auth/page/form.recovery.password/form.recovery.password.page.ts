import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }             from "../../../core/base/page/base.page";
import { AuthPlugin }           from "../../../core/plugin/auth/auth.plugin";
import { FormRecoveryPassword } from "../../component/form.recovery.password/form.recovery.password";
import { FieldType }            from "../../../core/type/field/field.type";
import { Form }                 from "../../../core/model/form/form";

@Component({ 
    selector        : "form-recovery-password-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_form';acl:acl;value:true;">
                            <ion-toolbar *ngIf="core().isMobile()">
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    Login
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="form-recovery-password">
                            <ion-grid [backgroundSlider]="core()?.account?.background_login">
                                <ion-row>
								<ion-col class="form-box" size-xs="12" size-ms="12" size-md="6" size-lg="6" size-xl="4"
                                        offset-xs="0" offset-ms="0" offset-md="0" offset-lg="0" offset-xl="0">
										<div class="logo-intro ion-text-center" *ngIf="!core().hasGuestUser() && form">
											<a (click)="onClose()">
												<h1 *ngIf="!core()?.account?.logo && !core()?.account?.logoLogin">{{core()?.account?.name}}</h1>
												<img *ngIf="core()?.account?.logo && !core()?.account?.logoLogin" [src]="core()?.account?.logo?._url"/>
												<img *ngIf="core()?.account?.logoLogin" [src]="core()?.account?.logoLogin._url"/>
											</a>
										</div>
                                        <div form-recovery-password
                                             #recoveryForm 
                                             [app]="app"
                                             [acl]="acl"
                                             [form]="form"
                                             (recovery)="onRecovery($event)"
                                             (cancel)="onClose()">
                                        </div>
                                    </ion-col>
									<ion-col size-xs="12" size-ms="12" size-md="6" size-lg="6" size-xl="8">
									</ion-col>
                                </ion-row>
                            </ion-grid>
                        </ion-content>`,
})
export class FormRecoveryPasswordPage extends BasePage
{
@ViewChild("recoveryForm", { static : true }) recoveryForm : FormRecoveryPassword;

public loginField : any;
public authPlugin : any;

constructor(
public changeDetectorRef : ChangeDetectorRef
)
{
    super();

    this.authPlugin = this.core().injector.get(AuthPlugin);
}

ionViewWillLeave()
{
    if (this.recoveryForm)
    {
        this.recoveryForm.destroy();
    }			
}

/* OVERRIDER: NAO DISPARA EVENTOS DE LOGIN OU NÃO LOGIN */
async dispatchLogin()
{
    // OVERRIDER 
}

initialize()
{
    const items     = [];
    const logins    = this.acl.getKey("logins", []);
    this.loginField = this.acl.getKey("loginField", {
        type        : FieldType.type("Email"),
        name        : "login",
        label       : "Email",
        placeholder : "Email",
        path        : "email"
    });

    if (logins && logins.length > 0)
    {
        items.push({
            row   : 0,
            field : {
                type        : FieldType.type("SELECT"),
                name        : "app",
                label       : "Acesso",
                placeholder : "Acesso",
                option      : {
                    items : logins
                },
            }
        });
    }

    items.push({
        row   : 2,
        field : this.loginField
    });

    this.form = new Form({
        name      : "Recuperar Senha",            
        iconAdd   : "lock-open",
        saveFixed : true,
        items     : items,
        setting   : {
            addText    : "Recuperar",
            cancelText : "Voltar",
        }
    });

    this.changeDetectorRef.markForCheck();
}

async onRecovery(event:any)
{
/* LOGIN BY APPID */
    let appid = this.app.code;

    if (event.data.app)
    {
        appid = event.data.app.value;
    }

    event.data.loginField = this.loginField.path;

    const params = this.createParams({
        appid : appid,
        post  : event.data
    });

    const result = await this.authPlugin.recoveryPassword(params);

    if (result.status)
    {
        //this.form.isValid = false;

        this.showAlert("Atenção", result.message);

        this.onClose();
    }
    else
    {
        // ERROR
        //this.form.isValid = false;

        /*if(this.form.instance)
{
this.form.instance.createComponents();
} */   

        this.showAlert("Atenção", result.error);
    }
}

onClose()
{
    this.back();
}
}
