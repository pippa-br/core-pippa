import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }               from "../../../core";
import { FormRecoveryPasswordPage } from "./form.recovery.password.page";
import { FormRecoveryPassword }     from "../../component/form.recovery.password/form.recovery.password";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormRecoveryPasswordPage } ])
    ],
    declarations : [ 
        FormRecoveryPasswordPage,
        FormRecoveryPassword,
    ],
})
export class FormRecoveryPasswordModule 
{}
 