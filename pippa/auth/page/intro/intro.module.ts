import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { IntroPage }   from "./intro.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : IntroPage } ])
    ],
    declarations : [
        IntroPage,
    ],
})
export class IntroModule 
{}
