import { Component, OnInit } from "@angular/core";

/* PIPPA */
import { BasePage } from "../../../core/base/page/base.page";

@Component({
    selector : "intro-page",
    template : `<ion-content class="intro-page">

					<slide-block [appid]="'intro'" [isInitialize]="isInitialize"></slide-block>

					<ion-button color="light" (click)="goFirst()">
						Entrar
					</ion-button>

                </ion-content>`,
})
export class IntroPage extends BasePage
{
    public isLogged  = false;

    goFirst()
    {
        this.core().goFirstMenu();
    }

    /*onRegister(event)
    {
        this.push('auth/register',
        {
            app : this.core().getApp(event.appid)
        });
    }

    onLogin()
    {
        this.push('auth/login',
        {
            appid : this.app.code
        });
    }*/
}
