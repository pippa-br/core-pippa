import { Component, ViewChild, Directive, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }      from "../../../core/base/page/base.page";
import { User }          from "../../../core/model/user/user";
import { AuthPlugin }    from "../../../core/plugin/auth/auth.plugin";
import { EventPlugin }   from "../../../core/plugin/event/event.plugin";
import { LoginForm }     from "../../component/login/login.form";
import { FieldType }     from "../../../core/type/field/field.type";
import { Form }          from "../../../core/model/form/form";
import { Account }       from "../../../core/model/account/account";
import { StoragePlugin } from "../../../core/plugin/storage/storage.plugin";
import { ReCaptchaV3Service } from "ng-recaptcha-custom";

@Directive()
export class BaseLogin extends BasePage
{
    @ViewChild("loginForm", { static : true }) _loginForm : LoginForm;	

    public loading        = false;
    public logins        : any;
    public registers     : any;
    public loginField    : any;
    public passwordField : any;
    public extraFilters  : any;
    public eventPlugin   : EventPlugin;
    public storagePlugin : StoragePlugin;
    public isPrivateMode  = false;
    public recaptchaV3Service : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();

        this.eventPlugin        = this.core().injector.get(EventPlugin);
        this.storagePlugin      = this.core().injector.get(StoragePlugin);
        this.recaptchaV3Service = this.core().injector.get(ReCaptchaV3Service);
    }

    ionViewWillLeave()
    {
        if (this._loginForm)
        {
            this._loginForm.destroy();
        }
    }

    initialize()
    {
        if (this.core().user && !this.core().user.isGuest())
        {
            /* LOGIN  */
            //BASE PAGE FAZ ISSO linha 168 this.eventPlugin.dispatch(this.core().user.getGroup() + ':login', {staus:true, data:this.core().user});
            this.eventPlugin.dispatch("login", { staus : true, data : this.core().user });
        }
        else
        {
            /* APPS */
            this.logins       = this.acl.getKey("logins",     []);
            this.registers    = this.acl.getKey("registers",  []);
            this.extraFilters = this.acl.getKey("extraFilters", []);
            this.loginField   = this.acl.getKey("loginField", {
                type        : FieldType.type("Email"),
                name        : "login",
                label       : "Email",
                placeholder : "Email",
                path        : "email",
            });

            this.passwordField = this.acl.getKey("passwordField", {
                type        : FieldType.type("Password"),
                name        : "password",
                label       : "Senha",
                placeholder : "Senha",
                path        : "password",
                encrypt     : true,
            });

            const items = [];
 
            /*items.push({
                row   : 0,
                field : {
                    type        : FieldType.type('Avatar'),
                    name        : 'photo',
                    label       : 'Photo',
                    placeholder : 'Photo',
                    required    : false,
                    editable    : false,
                }
            });*/

            if (this.logins && this.logins.length > 0)
            {
                items.push({
                    row   : 1,
                    field : {
                        type        : FieldType.type("Select"),
                        name        : "app",
                        label       : "Acesso",
                        placeholder : "Acesso",
                        setting     : {
                            searchable : false
                        },
                        option : {
                            items : this.logins
                        },
                    }
                });
            }

            items.push({
                row   : 2,
                field : this.loginField
            });

            items.push({
                row   : 3,
                field : this.passwordField,
            });

            items.push({
                row   : 4,
                field : {
                    type        : FieldType.type("Hidden"),
                    name        : "pathLogin",
                    label       : "Path",
                    placeholder : "Path",
                    initial     : this.loginField.path,
                }
            });
			
            items.push({
                row   : 5,
                field : {
                    type        : FieldType.type("Hidden"),
                    name        : "pathPassword",
                    label       : "Path Password",
                    placeholder : "Path Password",
                    initial     : this.passwordField.path,
                }
            });
			
            items.push({
                row   : 6,
                field : {
                    type        : FieldType.type("Hidden"),
                    name        : "encrypt",
                    label       : "Encrypt",
                    placeholder : "Encrypt",
                    initial     : this.passwordField.encrypt,
                }
            });

            this.form = new Form({
                appid     : this.app.code,
                saveFixed : true,
                name      : "Login",                
                iconAdd   : "lock-open",
                items     : items,
                setting   : {
                    addText : "Login",
                    setText : "Login",
                }
            });
			
            this.changeDetectorRef.markForCheck();
        }
    }

    /* OVERRIDER: NAO DISPARA EVENTOS DE LOGIN OU NÃO LOGIN */
   	async dispatchLogin(isInitialize:boolean)
    {
        isInitialize;
    }

    async onLogin(event:any)
    {
        this.recaptchaV3Service.execute('importantAction')
        .subscribe({
            next: async (token) => 
            {
                this.loading = true;

                /* LOGIN BY APPID */
                let appid           = this.app.code;
                let groupCollection = false;

                if (event.data.app)
                {
                    appid           = event.data.app.value;
                    groupCollection = (event.data.app.groupCollection !== undefined ? event.data.app.groupCollection : false);
                }

                /* LOCAL PASSWORD */
                event.data.localPassword = this.acl.getKey("localPassword");

                const params = this.createParams({
                    appid       	: appid,
                    post     	    : event.data, 
                    acl       	    : this.acl,
                    groupCollection : groupCollection,
                    extraFilters   	: this.extraFilters,
                    token           : token,
                });

                const authPlugin = this.core().injector.get(AuthPlugin);

                const result = await authPlugin.login(params);

                /* APPID DO AUTH AQUI POIS NO PLUGIN PODE SER PASSADO OUTRO*/
                await this.storagePlugin.set("appid", this.app.code);

                if (result.status)
                {
                    // POR CONTA DO MODO ANONIMO QUE NAO PERSISTE O LOGIN
                    const resultLogged = await authPlugin.getLogged(params);

                    if (resultLogged.status)
                    {
                        const group = resultLogged.data.getGroup();

                        if (group)
                        {
                            this.core().user     = resultLogged.data;
                            this.core().isLogged = true;
                
                            // LOGIN COM A CONTA
                            if (this.core().user["appid"] == "accounts")
                            {
                                this.core().account = new Account(result.data);
                                await this.core().account.on();
                            }
                            else if (this.core().user["accid"] != this.core().account.code) // LOGIN EM CONTA DIFERENTE
                            {
                                await this.storagePlugin.set("accid", this.core().user["accid"]);
                                await this.core().accountLoad();
                            }
            
                            // ATUALIZA MENU
                            await this.core().menu.updateAsyncFilter();
            
                            this.doLogin(result);
            
                            this.loading = false;
                        }
                        else
                        {
                            console.log("user group not found", group);

                            this.loading = false;
                            this.changeDetectorRef.markForCheck();

                            this.showAlert("Atenção", "Usuário sem grupo!");
                        }				
                    }
                    else
                    {
                        this.loading       = false;
                        this.isPrivateMode = true;
                        this.changeDetectorRef.markForCheck();
                    }					
                }
                else
                {
                    console.log("user not found");

                    const user = new User(event.data)

                    this.loading = false;
                    this.data    = user;
                    this.changeDetectorRef.markForCheck();

                    this.showAlert("Atenção", result.error);
                }
            },
            error: (error) => console.error('Recaptcha error:', error)
        });                
    }

    doLogin(result:any)
    {
        const group = result.data.getGroup();

        console.log("login user", this.core().user);

        /* EVENT LOGIN */
        const eventPlugin = this.core().injector.get(EventPlugin);

        eventPlugin.dispatch(group + ":login", result);
        eventPlugin.dispatch("login", result);
    }

    onRegister(data:any)
    {
        const router = data.router || "auth-register";
        const params = data.params || { appid : data.value };

        this.push(router, params);
    }

    onRecovery()
    {
        this.push("auth-recovery-password",
            {
                appid : this.app.code
            });
    }

    async onCancel()
    {
        this.goInit();
    }
}
