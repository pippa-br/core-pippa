import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseLogin } from "./base.login";

@Component({
    selector        : "login-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl     : "login.page.html",
})
export class LoginPage extends BaseLogin 
{
}
