import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule } from "../../../core";
import { LoginPage }  from "./login.page";
import { LoginForm }  from "../../component/login/login.form";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : LoginPage } ])
    ],
    declarations : [
        LoginPage,
        LoginForm,
    ]
})
export class LoginModule 
{}
