import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseFormRegister } from "./base.form.register";

@Component({
    selector        : "form-register-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ngIf="core().isMobile()">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
									Cadastre-se
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content> 
                            <ion-grid [backgroundSlider]="core()?.account?.background_login">
                                <ion-row>
                                    <ion-col size-ms="12" size-md="12" size-lg="12" size-xl="12">
                                        <div class="logo-intro ion-text-center" *ngIf="!core().isMobile() && form">
                                            <a (click)="onClose()">
                                                <h1 *ngIf="!core()?.account?.logo && !core()?.account?.logoLogin">{{core()?.account?.name}}</h1>
                                                <img *ngIf="core()?.account?.logo && !core()?.account?.logoLogin" [src]="core()?.account?.logo?._url"/>
                                                <img *ngIf="core()?.account?.logoLogin" [src]="core()?.account?.logoLogin._url"/>
                                            </a>
                                        </div>
                                        <div form-register
                                             #formRegister
                                             [app]="app"
                                             [acl]="acl"
                                             [form]="form"
                                             (add)="onAdd($event)"
                                             (cancel)="onClose()">
                                        </div>
                                    </ion-col>
                                </ion-row>
                            </ion-grid>

                        </ion-content>`,
})
export class FormRegisterPage extends BaseFormRegister
{
   
}
