import { Component, ChangeDetectorRef, Directive, ViewChild } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { DocumentPlugin } from "../../../core/plugin/document/document.plugin";
import { FormRegister }   from "../../component/form.register/form.register";
import { AuthPlugin }     from "../../../core/plugin/auth/auth.plugin";
import { EventPlugin }    from "../../../core/plugin/event/event.plugin";
import { FormPlugin }     from "../../../core/plugin/form/form.plugin";

@Directive()
export class BaseFormRegister extends BasePage
{
    @ViewChild("formRegister", { static : true }) formRegister : FormRegister;

    public documentPlugin : DocumentPlugin;
    public formPlugin     : FormPlugin;
    public authPlugin     : AuthPlugin;
    public eventPlugin    : EventPlugin;

    constructor(        
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
		
        this.documentPlugin = this.core().injector.get(DocumentPlugin);
        this.formPlugin     = this.core().injector.get(FormPlugin);
        this.authPlugin     = this.core().injector.get(AuthPlugin);
        this.eventPlugin    = this.core().injector.get(EventPlugin);
    }

    ionViewWillLeave()
    {
        if (this.formRegister)
        {
            this.formRegister.destroy();	
        }        
    }

    /* OVERRIDER: NAO DISPARA EVENTOS DE LOGIN OU NÃO LOGIN */
    async dispatchLogin(isInitialize:boolean)
    {
        isInitialize;
    }

    initialize()
    {
        const formPlugin = this.core().injector.get(FormPlugin);

        /* LOAD FORM */
        const params = this.createParams({
            appid : this.app.code,
        });

        formPlugin.getData(params).then((result:any) =>
        {
            if (result.collection.length > 0)
            {
                this.form = result.collection[0];
            }

            this.changeDetectorRef.markForCheck();
        });
    }

    onAdd(event:any)
    {
        this.documentPlugin.add(this.app.code, this.form, event).then((result:any) =>
        {
            /*if(this.form._track)
            {
                this.core().track(this.form._track);
			}*/
			
            if (result.redirectRouter)
            {
                this.push(result.redirectRouter, result.redirectParams)
            }
            else if (result.message)
            {
                this.clearForm();
                this.showAlert("", result.message);					
            }
            else
            {
                this.authPlugin.loginRegister(result.data);
            }
        });
    }
	
    clearForm()
    {
        if (this.formRegister)
        {
            this.formRegister.display();		
        }       
    }

    onClose()
    {
        this.back();
    }
}
