import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }       from "../../../core";
import { FormRegisterPage } from "./form.register.page";
import { FormRegister }     from "../../component/form.register/form.register";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormRegisterPage } ])
    ],
    declarations : [
        FormRegisterPage,
        FormRegister,
    ],
})
export class FormRegisterModule 
{}
