import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }    from "../../../core/base/page/base.page";
import { EventPlugin } from "../../../core/plugin/event/event.plugin";

@Component({
    selector        : "loading-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-content>
							<div class="backgroud-wrapper" [ngStyle]="{'background-image' : 'url(' + core()?.account?.introBackground?._url + ')'}">
								<ul *ngIf="menus" class="menus">
									<li *ngFor="let item of menus;">
										<ion-button fill="outline" (click)="onLanguage(item)">
											{{item.label}}
										</ion-button>
									</li>
								</ul>
								<div class="login" *ngIf="isInitialize">
									<ion-button color="dark" (click)="push('auth-logout')">Ir para o Login</ion-button>
								</div>								
							</div>							
                        </ion-content>`,
})
export class LoadingPage extends BasePage
{
    public _isInitialize : boolean;
    public menus         : any;

    constructor(
        public eventPlugin       : EventPlugin,
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    async initialize(isInitialize:boolean)
    {
        this._isInitialize = isInitialize;
		
        /* COMO O LANGUAGE SALVA EM CACHE, PODEMOS RESETAR A ESCOLHA */
        if (this.queryParams.reset)
        {
            await this.core().storagePlugin.del("menu_appid_" + this.core().platform.value);
        }

        console.log("loading", isInitialize);

        const language_Key = "language_" + this.core().platform.value;

        if (this.core().account[language_Key])
        {
            const menus     = this.core().account[language_Key];
            const menuAppid = await this.core().storagePlugin.get("menu_appid_" + this.core().platform.value);

            if (menuAppid)
            {
                for (const key in menus)
                {
                    if (menus[key].menuAppid == menuAppid)
                    {
                        await this.onLanguage(menus[key]);
                        break;
                    }
                }
            }
            else
            {					
                this.menus = menus;	
            }				
        }
        else
        {
            if (this.core().account.notificationFormPath)
            {
                await this.core().storagePlugin.set("notification_form_path_" + this.core().platform.value, this.core().account.notificationFormPath);						
                await this.core().loadNotification();
				
                this.dispatchEvent();
            }
            else
            {
                this.dispatchEvent();
            }                
        } 

        this.changeDetectorRef.markForCheck();       
    }

    async onLanguage(item:any)
    {
        console.log("Language", item)

        /* LOAD MENU */
        await this.core().storagePlugin.set("menu_appid_" + this.core().platform.value, item.menuAppid);
		
        await this.core().loadMenu();

        /* LOAD NOTIFICATION */
        await this.core().storagePlugin.set("notification_form_path_" + this.core().platform.value, item.notificationFormPath);

        await this.core().loadNotification();

        /* LOAD LANGUAGE */
        await this.core().storagePlugin.set("language_" + this.core().platform.value, item.value);
		
        await this.core().loadLanguage();

        this.dispatchEvent();
    }

    dispatchEvent()
    {
        console.error("user group", this.core().user);

        if (this.core().user)
        {
            const group = this.core().user.getGroup();			

            /* LOGIN  */
            this.eventPlugin.dispatch(group + ":login", { staus : true, data : this.core().user });
            this.eventPlugin.dispatch("login", { staus : true, data : this.core().user });
        }
        else
        {
            this.eventPlugin.dispatch("no-login", { staus : true, data : this.core().user });
        }

        this.eventPlugin.dispatch("auth-loading", true);
    }
}
