import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { LoadingPage } from "./loading.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : LoadingPage } ])
    ],
    declarations : [
        LoadingPage,
    ]
})
export class LoadingModule 
{} 
