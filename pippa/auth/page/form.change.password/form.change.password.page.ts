import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }             from "../../../core/base/page/base.page";
import { EmailTemplate }        from "../../../core/model/email.template/email.template";
import { UserCollection }       from "../../../core/model/user/user.collection";
import { AuthPlugin }           from "../../../core/plugin/auth/auth.plugin";
import { DocumentPlugin }       from "../../../core/plugin/document/document.plugin";
import { FormRecoveryPassword } from "../../component/form.recovery.password/form.recovery.password";
import { FieldType }            from "../../../core/type/field/field.type";
import { Form }                 from "../../../core/model/form/form";

@Component({
    selector        : "form-change-password-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_form';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    Alterar Senha
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="form-recovery-password">
                            <ion-grid>
                                <ion-row>
                                    <ion-col size-xs="12" size-ms="12" size-md="6" size-lg="6" size-xl="4"
                                            offset-xs="0" offset-ms="0" offset-md="3" offset-lg="3" offset-xl="4">                                        
                                        <div form-change-password
                                             #changeForm 
                                             [app]="app"
                                             [acl]="acl"
                                             [form]="form"
                                             (save)="onSave($event)"
                                             (cancel)="onClose()">
                                        </div>
                                    </ion-col>
                                </ion-row>
                            </ion-grid>
                        </ion-content>`,
})
export class FormChangePasswordPage extends BasePage
{
    @ViewChild("changeForm", { static : true }) recoveryForm : FormRecoveryPassword;

    constructor(
		public documentPlugin    : DocumentPlugin,
        public changeDetectorRef : ChangeDetectorRef,

    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.recoveryForm.destroy();
    }

    /* OVERRIDER: NAO DISPARA EVENTOS DE LOGIN OU NÃO LOGIN */
    async dispatchLogin()
    {
    }

    initialize()
    {
        const items = [];
        
        items.push({
            row   : 1,
            field : {
                type        : FieldType.type("password"),
                name        : "password",
                label       : "Nova Senha",
                placeholder : "Nova Senha",
            }
        });

        items.push({
            row   : 2,
            field : {
                type        : FieldType.type("confirmPassword"),
                name        : "confirmPassword",
                label       : "Confirmar Nova Senha",
                placeholder : "Confirmar Nova Senha",
            }
        });

        this.form = new Form({
            name    : "Alterar Senha",            
            iconAdd : "lock-open",
            items   : items,
            setting : {
                addText : "Alterar",
            }
        });

        this.changeDetectorRef.markForCheck();
    }

    async onSave(event:any)
    {
        const data = await this.documentPlugin.set(this.app.code, this.core().user, this.core().user.form, event);

        this.showAlert("Alterar Senha", "Sua Senha foi alterada com sucesso!");

        this.onClose();
    }

    onClose()
    {
        this.back();
    }
}
