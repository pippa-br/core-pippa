import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }             from "../../../core";
import { FormChangePasswordPage } from "./form.change.password.page";
import { FormChangePassword }     from "../../component/form.change.password/form.change.password";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormChangePasswordPage } ])
    ],
    declarations : [
        FormChangePasswordPage,
        FormChangePassword,
    ],
})
export class FormChangePasswordModule
{
}
