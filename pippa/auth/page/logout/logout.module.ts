import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule } from "../../../core";
import { LogoutPage }  from "./logout.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : LogoutPage } ])
    ],
    declarations : [
        LogoutPage, 
    ],
})
export class LogoutModule 
{}
