import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }    from "../../../core/base/page/base.page";
import { AuthPlugin }  from "../../../core/plugin/auth/auth.plugin";
import { EventPlugin } from "../../../core/plugin/event/event.plugin";

@Component({
    selector        : "logout-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : "<ion-progress-bar type=\"indeterminate\" color=\"medium\"></ion-progress-bar>",
})
export class LogoutPage extends BasePage
{
    constructor(
        public authPlugin        : AuthPlugin,
        public eventPlugin       : EventPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    async initialize()
    {
        const params = this.createParams({});
        params.appid = await this.core().storagePlugin.get("appid");

        await this.authPlugin.logout(params);

        this.changeDetectorRef.markForCheck();

        if (this.core().queryParams.redirect)
        {
            this.push(this.core().queryParams.redirect);
        }
        else
        {
            this.goInit();
        }  
    }
}
