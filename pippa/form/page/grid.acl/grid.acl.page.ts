import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }  from "../../../core/base/page/base.page";
import { GridAcl }   from "../../component/grid.acl/grid.acl";
import { AclPlugin } from "../../../core/plugin/acl/acl.plugin";
import { FieldType } from "../../../core/type/field/field.type";
import { Grid }      from "../../../core/model/grid/grid";
import { Acl }       from "../../../core/model/acl/acl";

@Component({
    selector        : "grid-acl-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} - ACL's
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="addAcl()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div grid-acl
                                 #gridAcl
                                 [app]="app"
                                 [data]="document"
                                 [acl]="acl"
                                 [grid]="grid"
                                 [collection]="collection">
                            </div>
                        </ion-content>`,
})
export class GridAclPage extends BasePage
{
    @ViewChild("gridAcl", { static : true }) gridAcl : GridAcl;    

    constructor(
        public aclPlugin         : AclPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }
    
    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    field : {
                        label : "Grupo",
                        name  : "_group",
                        type  : FieldType.type("Select"),
                    }
                },
            ],
        });

        // GET DATA
        const params = this.createParams({
            appid : this.document.code,
            order : "order",
        });

        this.aclPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    ionViewWillLeave()
    {
        this.gridAcl.destroy();
    }

    addAcl()
    {
        this.push("form-acl",
            {
                appid : this.document.code,
            //formPath : this.document.reference.path 
            });
    }

    onClose()
    {
        this.back();
    }
}
