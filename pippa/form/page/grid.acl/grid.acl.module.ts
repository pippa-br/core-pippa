import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { GridAcl }     from "../../component/grid.acl/grid.acl";
import { GridAclPage } from "./grid.acl.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridAclPage } ])
    ],
    declarations : [
        GridAclPage,
        GridAcl,
    ],
})
export class GridAclModule
{
}
