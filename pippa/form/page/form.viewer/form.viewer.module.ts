import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }     from "../../../core";
import { FormViewer }     from "../../component/form.viewer/form.viewer";
import { FormViewerPage } from "./form.viewer.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormViewerPage } ])
    ],
    declarations : [
        FormViewerPage,
        FormViewer, 
    ],
})
export class FormViewerModule 
{}
