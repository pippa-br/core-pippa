import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { ViewerPlugin }   from "../../../core/plugin/viewer/viewer.plugin";
import { Form }           from "../../../core/model/form/form";
import { Field }          from "../../../core/model/field/field";
import { FieldType }      from "../../../core/type/field/field.type";
import { AttachmentType } from "../../../core/type/attachment/attachment.type";
import { Viewer }         from "../../../core/model/viewer/viewer";
import { FormViewer }     from "../../component/form.viewer/form.viewer";
import { FormType }   	  from "../../../core/type/form/form.type";

@Component({
    selector        : "form-viewer-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Visualização
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="copyForm()">
                                        <ion-icon name="code-download-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only (click)="pastForm()">
                                        <ion-icon name="code-working-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding">
                            <div form-viewer
                                 #formViewer
                                 [app]="app"
                                 [form]="viewerForm"
                                 [data]="data"
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormViewerPage extends BasePage
{
    @ViewChild("formViewer", { static : true }) formViewer : FormViewer;

    public viewerForm : any;
    public items 	  : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
        public viewerPlugin      : ViewerPlugin,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.formViewer.destroy();
    }


    initialize()
    {
        this.form.on(true).then(() =>
        {
            this.items = this.form.getFormItemsDefault();

            this.builderForm();
        });
    }

    builderForm()
    {
        this.viewerForm = new Form({
            type  : FormType.SEGMENT_TYPE,
            items : [
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "name",
                        label       : "Nome",
                        placeholder : "Nome",
                        initial     : "Viewer Default"
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("MultiSelect"),
                        name        : "groups",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("SELECT"),
                        name        : "type",
                        label       : "Tipo",
                        placeholder : "Tipo",
                        option      : {
                            items : [
                                Viewer.FLAT_TYPE,
                                Viewer.SEGMENT_TYPE,
                                Viewer.ACCORDION_TYPE,
                                Viewer.SLIDE_TYPE,
                                Viewer.INDEXES_TYPE,
                            ] },
                        initial : FormType.FLAT_TYPE.value
                    })
                },					
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 2,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Step"),
                        name        : "items",
                        label       : "Colunas",
                        placeholder : "Colunas",
                        setting     : {
                            formItems    : this.items,
                            editFormItem : true,
                        },
                        required : false,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "hasHeader",
                        label       : "Ativar Header",
                        placeholder : "Ativar Header",
                        initial     : false,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Image"),
                        name        : "imageHeader",
                        label       : "Image Header",
                        placeholder : "Image Header",
                        required    : false,
                        attachments : [
                            AttachmentType.COMMENT,
                        ]
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Image"),
                        name        : "imageFooter",
                        label       : "Image Footer",
                        placeholder : "Image Footer",
                        required    : false,
                        attachments : [
                            AttachmentType.COMMENT,
                        ]
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("RichText"),
                        name        : "description",
                        label       : "Descrição",
                        placeholder : "Descrição",
                        required    : false,
                    })
                },
            ],
        });

        this.changeDetectorRef.markForCheck();       
    }

    async onAdd(event:any)
    {
        const data = await this.viewerPlugin.add(this.app.code, this.form, event);

        this.onClose();
    }

    async onSet(event:any)
    {
        const data = await this.viewerPlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();        
    }

    onClose()
    {
        this.back();
    }
}
