import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }     from "../../../core";
import { GridViewer }     from "../../component/grid.viewer/grid.viewer";
import { GridViewerPage } from "./grid.viewer.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridViewerPage } ])
    ],
    declarations : [
        GridViewerPage,
        GridViewer,
    ],
})
export class GridViewerModule
{
}
