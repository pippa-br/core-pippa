import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { GridViewer }     from "../../component/grid.viewer/grid.viewer";
import { FieldType }      from "../../../core/type/field/field.type";
import { ViewerPlugin }   from "../../../core/plugin/viewer/viewer.plugin";
import { Grid }           from "../../../core/model/grid/grid";

@Component({
    selector        : "grid-viewer-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} - Viewers
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="addGrid()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div grid-viewer
                                #gridViewer
                                [app]="app"
                                [acl]="acl"
                                [grid]="grid"
                                [collection]="collection">
                            </div>
                        </ion-content>`,
})
export class GridViewerPage extends BasePage
{
    @ViewChild("gridViewer", { static : true }) gridViewer : GridViewer;

    constructor(
        public viewerPlugin      : ViewerPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    row   : 1,
                    field : {
                        label : "Nome",
                        name  : "name",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row   : 2,
                    field : {
                        label : "Path",
                        name  : "reference.path",
                        type  : FieldType.type("Text"),
                    }
                },				
                {
                    row   : 3,
                    field : {
                        label : "Grupos",
                        name  : "groups",
                        type  : FieldType.type("MultiSelect"),
                    }
                },
            ],
        });

        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "name",
        });

        params.where = [];

        params.where.push({
            field    : "form",
            operator : "==",
            value    : this.form.reference,
            type     : FieldType.type("ReferenceSelect")
        });

        this.viewerPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    addGrid()
    {
        this.push("form-viewer",
            {
                formPath : this.form.reference.path
            });
    }

    ionViewWillLeave()
    {
        this.gridViewer.destroy();
    }

    onClose()
    {
        this.back();
    }
}
