import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BasePage }            		  from "../../../core/base/page/base.page";
import { Field }               		  from "../../../core/model/field/field";
import { FieldType }          	 	  from "../../../core/type/field/field.type";
import { NotificationTemplatePlugin } from "../../../core/plugin/notification.template/notification.template.plugin";
import { Form }                		  from "../../../core/model/form/form";
import { FormNotificationTemplate }   from "../../component/form.notification.template/form.notification.template";
import { TransactionType } 			  from "../../../core/type/transaction/transaction.type";

@Component({
    selector        : "form-notification-template-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Notificações
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content >
                            <div form-notification-template
                                 #formNotificationTemplate
                                 [app]="app"
                                 [acl]="acl"
                                 [form]="emailTemplateForm"
                                 [data]="data"
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormNotificationTemplatePage extends BasePage
{
    @ViewChild("formNotificationTemplate", { static : true }) formNotificationTemplate : FormNotificationTemplate;

    public emailTemplateForm : any;

    constructor(
        public notificationTemplatePlugin : NotificationTemplatePlugin,
        public changeDetectorRef   		  : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.formNotificationTemplate.destroy();
    }

    initialize()
    {
        this.emailTemplateForm = new Form({
            name  : "Cadastro Notificação",
            items : [
                {
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "type",
                        label       : "Tipo",
                        placeholder : "Tipo",
                        option      : {
                            record : false,
                            items  : [
                                TransactionType.ADD_TYPE,
                                TransactionType.SET_TYPE,
                                TransactionType.RECOVERY_PASSWORD_TYPE,
                                TransactionType.NEW_PAYMENT_TYPE,
                                TransactionType.PAID_PAYMENT_TYPE,
                                TransactionType.SCHEDULE_TYPE,
                            ]
                        },
                    })
                },
                {
                    row   : 1,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "subject",
                        label       : "Assunto",
                        placeholder : "Assunto",
                    })
                },  
                {
                    row   : 1,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "message",
                        label       : "Mensagem",
                        placeholder : "Mensagem",
                    })
                },      
                {
                    row   : 1,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "action",
                        label       : "Ação",
                        placeholder : "Ação",
                        option      : {
                            record : false,
                            items  : [
                                {
                                    label : "Alerta",
                                    value : "alert",
                                },
                                {
                                    label : "Confirmação",
                                    value : "confirm",
                                }
                            ]
                        },
                    })
                },    
                {
                    row   : 1,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "router",
                        label       : "Rota",
                        placeholder : "Rota",      
                        required    : false,
                    })
                },
                {
                    row   : 2,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ItemsSetting"),
                        name        : "params",
                        label       : "Parametros",
                        placeholder : "Parametros",
                        required    : false,
                    })
                }, 
                /* VALIDATE */
                {
                    row   : 3,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "validate",
                        label       : "Validar",
                        placeholder : "Validar",
                        required    : false,                            
                    }),
                    items : [                       
                        {
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "field",
                                label       : "Field",
                                placeholder : "Field",
                            })
                        },
                        {
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "operator",
                                label       : "Operador",
                                placeholder : "Operador",
                                initial     : "=="
                            })
                        },
                        {
                            row   : 1,
                            col   : 4,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "value",
                                label       : "Valor",
                                placeholder : "Valor",
                                required    : false,
                                setting     : {
                                    convert : true
                                }
                            })
                        },						
                    ]
                },
                {
                    row   : 4,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "toType",
                        label       : "Para",
                        placeholder : "Para",
                        setting     : {
                            controlField : "app,list,field,filters",
                            list         : "list",
                            app          : "app,filters",
                            field     		 : "field",
                        },
                        option : {
                            record : false,
                            items  : [
                                {
                                    label : "Todos",
                                    value : "all",
                                },
                                {
                                    label : "Lista",
                                    value : "list",
                                },
                                {
                                    label : "Campo",
                                    value : "field",
                                },
                                {
                                    label : "App",
                                    value : "app",
                                } ]
                        },
                    })
                },
                {
                    row   : 5,
                    col   : 1,
                    field : {
                        type        : FieldType.type("SubForm"),
                        name        : "list",
                        label       : "Para",
                        placeholder : "Para",
                        display     : false,                        
                    },
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "token",
                                label       : "Token",
                                placeholder : "Token",
                                required    : false,
                            }
                        },
                    ]
                },
                {
                    row   : 6,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "field",
                        label       : "Campo do Notification",
                        placeholder : "Campo do Notification",
                        display     : false,
                    })
                },
                {
                    row   : 7,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ReferenceApps"),
                        name        : "app",
                        label       : "App",
                        placeholder : "App",
                        display     : false,
                    })
                },
                /* FILTERS */
                {
                    row   : 8,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "filters",
                        label       : "Filtros",
                        placeholder : "Filtros",
                        required    : false,                            
                    }),
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Select"),
                                name        : "type",
                                label       : "Tipo",
                                placeholder : "Tipo",
                                option      : {
                                    items : [
                                        FieldType.type("Text"),
                                        FieldType.type("ReferenceSelect"),
                                        FieldType.type("Integer"),
                                        FieldType.type("Number"),
                                    ]
                                },
                            })
                        },                        
                        {
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "field",
                                label       : "Field",
                                placeholder : "Field",
                            })
                        },
                        {
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "operator",
                                label       : "Operador",
                                placeholder : "Operador",
                                initial     : "=="
                            })
                        },
                        {
                            row   : 1,
                            col   : 4,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "value",
                                label       : "Valor",
                                placeholder : "Valor",
                                required    : false,
                                setting     : {
                                    convert : true
                                }
                            })
                        },
                        {
                            row   : 1,
                            col   : 5,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "context",
                                label       : "Contexto",
                                placeholder : "Contexto",
                                required    : false,
                            })
                        },							
                        {
                            row   : 1,
                            col   : 6,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "path",
                                label       : "Path",
                                placeholder : "Path",
                                required    : false,
                            })
                        },
                    ]
                },
                {
                    row   : 9,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("RichText"),
                        name        : "description",
                        label       : "Descrição",
                        placeholder : "Descrição",
                    })
                },                                                                                                         				               
            ],
        });

        this.changeDetectorRef.markForCheck(); 
    }

    async onAdd(event:any)
    {
        const data = await this.notificationTemplatePlugin.add(this.app.code, this.form, event);

        this.onClose();
    }

    async onSet(event:any)
    {
        const data = await this.notificationTemplatePlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();        
    }

    onClose()
    {
        this.back();
    }
}
