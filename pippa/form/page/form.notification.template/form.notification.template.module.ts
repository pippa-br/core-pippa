import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }           		from "../../../core";
import { FormNotificationTemplate }     from "../../component/form.notification.template/form.notification.template";
import { FormNotificationTemplatePage } from "./form.notification.template.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormNotificationTemplatePage } ])
    ],
    declarations : [
        FormNotificationTemplatePage, 
        FormNotificationTemplate,
    ],
})
export class FormNotificationTemplateModule 
{
}
