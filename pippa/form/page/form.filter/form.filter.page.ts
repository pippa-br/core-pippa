import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";

/* PIPPA */
import { BasePage }     from "../../../core/base/page/base.page";
import { Field }        from "../../../core/model/field/field";
import { FieldType }    from "../../../core/type/field/field.type";
import { FilterPlugin } from "../../../core/plugin/filter/filter.plugin";
import { Form }         from "../../../core/model/form/form";
import { DynamicForm }  from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";

@Component({
    selector        : "form-filter-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Filtros
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="copyForm()">
                                        <ion-icon name="code-download-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only (click)="pastForm()">
                                        <ion-icon name="code-working-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <form dynamic-form
                                  #dynamicForm
                                  [acl]="acl"
                                  [form]="filterForm"
                                  [data]="data"
                                  (add)="onAdd($event)"
                                  (set)="onSet($event)"
                                  (close)="onClose()"
                                  (rendererComplete)="onRendererComplete($event)"
                                  class="filter-form">
                            </form>
                        </ion-content>
                        <ion-footer>
                            <ion-toolbar #footer>		
                            </ion-toolbar>
                        </ion-footer>`,
})
export class FormFilterPage extends BasePage
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @ViewChild("footer", { read : ElementRef }) public footer : ElementRef;

    public filterForm   : any;
    public items 		: any;
    public orderOptions : any;

    constructor(
        public filterPlugin      : FilterPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    onRendererComplete(event)
    {   
        const childElements = this.footer.nativeElement.childNodes;

        for (const child of childElements) 
        {
            this.footer.nativeElement.removeChild(child);
        }
		
        this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
    }

    ionViewWillLeave()
    {
        this.dynamicForm.destroy();
    }

    initialize()
    {
        this.form.on(true).then(() =>
        {
            this.items        = this.form.getFormItemsDefault();
            this.orderOptions = this.form.getOrderOptions();

            this.builderForm();
        });
    }

    builderForm()
    {
        this.filterForm = new Form({
            items : [
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "name",
                        label       : "Nome",
                        placeholder : "Nome",
                        required    : true,
                        initial     : "Filter Default"
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("MultiSelect"),
                        name        : "groups",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },										
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 2,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Step"),
                        name        : "items",
                        label       : "Colunas",
                        placeholder : "Colunas",
                        setting     : {
                            formItems    : this.items,
                            editFormItem : true,
                        },
                    })
                },
            ],
        });

        this.changeDetectorRef.markForCheck();       
    }

    async onAdd(event:any)
    {
        const data = await this.filterPlugin.add(this.app.code, this.form, event);

        this.onClose();        
    }

    async onSet(event:any)
    {
        const data = await this.filterPlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();
    }

    onClose()
    {
        this.back();
    }
}
