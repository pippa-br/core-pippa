import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { FormFilterPage } from "./form.filter.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormFilterPage } ])
    ],
    declarations : [
        FormFilterPage,
    ],
})
export class FormFilterModule 
{}
