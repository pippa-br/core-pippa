import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";

/* PIPPA */
import { BasePage }            from "../../../core/base/page/base.page";
import { Field }               from "../../../core/model/field/field";
import { FieldType }           from "../../../core/type/field/field.type";
import { EmailTemplatePlugin } from "../../../core/plugin/email.template/email.template.plugin";
import { Form }                from "../../../core/model/form/form";
import { TransactionType }     from "../../../core/type/transaction/transaction.type";
import { StepItem }    		   from "../../../core/model/step.item/step.item";
import { FormType }    		   from "../../../core/type/form/form.type";
import { DynamicForm }         from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";

@Component({
    selector        : "form-email-template-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Template de Email
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="copyForm()">
                                        <ion-icon name="code-download-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only (click)="pastForm()">
                                        <ion-icon name="code-working-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding">
                            <form dynamic-form
                                 #dynamicForm
                                 [form]="emailTemplateForm"	
                                 [data]="data"
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (close)="onClose()"
                                 (rendererComplete)="onRendererComplete($event)">
                            </form>
                        </ion-content>
                        <ion-footer>
                            <ion-toolbar #footer>		
                            </ion-toolbar>
                        </ion-footer>`,
})
export class FormEmailTemplatePage extends BasePage
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @ViewChild("footer", { read : ElementRef }) public footer : ElementRef;

    public emailTemplateForm : any;

    constructor(
        public emailTemplatePlugin : EmailTemplatePlugin,
        public changeDetectorRef   : ChangeDetectorRef,
    )
    {
        super();
    }

    onRendererComplete(event)
    {   
        const childElements = this.footer.nativeElement.childNodes;

        for (const child of childElements) 
        {
            this.footer.nativeElement.removeChild(child);
        }
		
        this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
    }    

    ionViewWillLeave()
    {
        this.dynamicForm.destroy();
    }

    initialize()
    {
        this.builderForm();
    }

    builderForm()
    {
        const generalSetting = new StepItem({
            id    : "general",   
            label : "Geral",
            order : 1,
        });

        const mobileSetting = new StepItem({
            id    : "mobile",   
            label : "Mobile",
            order : 2,
        });

        const emailSetting = new StepItem({
            id    : "email",   
            label : "E-mail",
            order : 3,
        });

        this.emailTemplateForm = new Form({
            type  : FormType.SEGMENT_TYPE,
            name  : "Cadastro Transacionais",			
            items : [
                {
                    row   : 1,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Toggle"),
                        name        : "status",
                        label       : "Status",
                        placeholder : "Status",
                    })
                },
                {
                    row   : 1,
                    col   : 2,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Toggle"),
                        name        : "onlyContent",
                        label       : "Somente Conteúdo",
                        placeholder : "Somente Conteúdo",
                        required    : false,
                    })
                },
                {
                    row   : 1,
                    col   : 3,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "mode",
                        label       : "Modo",
                        placeholder : "Modo",
                        option      : {
                            record : false,
                            items  : TransactionType.getModes()
                        },
                        setting : {
                            //controlField : "message,messageParts",
                            controlStep  : "mobile,email",
                            push         : "mobile",
                            email        : "email",
                            // app          : "message",
                            // plugZapi     : "messageParts"
                        },
                    })
                },
                {
                    row   : 1,
                    col   : 4,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "type",
                        label       : "Tipo",
                        placeholder : "Tipo",
                        option      : {
                            record : false,
                            items  : TransactionType.getTypes()
                        },
                    })
                },
                {
                    row   : 1,
                    col   : 5,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "provider",
                        label       : "Provedor",
                        placeholder : "Provedor",
                        option      : {
                            record : false,
                            items  : TransactionType.getProvides()
                        },
                    })
                },
                {
                    row   : 2,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "name",
                        label       : "Nome",
                        placeholder : "Nome",
                    })
                },
                {
                    row   : 2,
                    col   : 2,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "subject",
                        label       : "Assunto",
                        placeholder : "Assunto",
                    })
                },
                {
                    row   : 2,
                    col   : 3,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("MultiSelect"),
                        name        : "groups",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },										
                    })
                },
                {
                    row   : 2,
                    col   : 4,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Integer"),
                        name        : "limit",
                        label       : "Limite",
                        placeholder : "Limite",
                        initial     : 1,
                    })
                },				
                /* VALIDATE */
                {
                    row   : 3,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "validate",
                        label       : "Validar",
                        placeholder : "Validar",
                        required    : false,                            
                    }),
                    items : [                      
                        {
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "field",
                                label       : "Field",
                                placeholder : "Field",
                            })
                        },
                        {
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "operator",
                                label       : "Operador",
                                placeholder : "Operador",
                                initial     : "=="
                            })
                        },
                        {
                            row   : 1,
                            col   : 4,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "value",
                                label       : "Valor",
                                placeholder : "Valor",
                                required    : false,
                                setting     : {
                                    convert : true
                                }
                            })
                        },						
                    ]
                },
                // TO                
                {
                    row   : 4,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "toType",
                        label       : "Para",
                        placeholder : "Para",
                        setting     : {
                            controlField : "app,list,fieldName,fieldEmail,fieldPhone,subList,filters,fieldSubject,fieldMensagem,fieldUser,message,messageParts",
                            list         : "list,message",
                            app          : "app,filters,fieldUser,message,fieldName,fieldEmail",
                            field        : "fieldName,fieldEmail,message",
                            subList      : "fieldName,fieldEmail,subList,message",
                            fieldPhone   : "fieldPhone,messageParts",
                            fieldUser    : "fieldUser,message",
                        },
                        option : {
                            record : false,
                            items  : [
                                {
                                    label : "Lista",
                                    value : "list",
                                },
                                {
                                    label : "App",
                                    value : "app",
                                },
                                {
                                    label : "Campos Email",
                                    value : "field",
                                },
                                {
                                    label : "Campos Telfone",
                                    value : "fieldPhone",
                                },
                                {
                                    label : "Campos de Usuário",
                                    value : "fieldUser",
                                },
                                {
                                    label : "Sub Lista",
                                    value : "subList",
                                } ]
                        },
                    })
                },
                {
                    row   : 5,
                    col   : 1,
                    step  : generalSetting,
                    field : {
                        type        : FieldType.type("SubForm"),
                        name        : "list",
                        label       : "Para",
                        placeholder : "Para",
                        display     : false,                        
                    },
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "name",
                                label       : "Nome",
                                placeholder : "Nome",
                                required    : false,
                            }
                        },
                        {
                            row   : 1,
                            col   : 2,
                            field : {
                                type        : FieldType.type("Email"),
                                name        : "email",
                                label       : "E-mail",
                                placeholder : "E-mail",
                                required    : false,
                            }
                        },
                    ]
                },
                {
                    row   : 6,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("ReferenceApps"),
                        name        : "app",
                        label       : "App",
                        placeholder : "App",
                        display     : false,
                    })
                },
                /* FILTERS */
                {
                    row   : 7,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "filters",
                        label       : "Filtros",
                        placeholder : "Filtros",
                        required    : false,                            
                    }),
                    items : FieldType.getFilterItems()
                },
                // EMAIL
                {
                    row   : 8,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldName",
                        label       : "Campo do Nome",
                        placeholder : "Campo do Nome",
                        display     : false,
                        required    : false,
                    })
                },
                {
                    row   : 8,
                    col   : 2,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldEmail",
                        label       : "Campo do E-mail",
                        placeholder : "Campo do E-mail",
                        display     : false,
                        required    : false,
                    })
                },
                // OBJECT
                {
                    row   : 8,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldSubject",
                        label       : "Campo do Assunto",
                        placeholder : "Campo do Assunto",
                        display     : false,
                    })
                },
                {
                    row   : 8,
                    col   : 2,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldMensagem",
                        label       : "Campo da Mensagem",
                        placeholder : "Campo da Mensagem",
                        display     : false,
                    })
                }, 
                {
                    row   : 8,
                    col   : 3,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldUser",
                        label       : "Campo do Usuário",
                        placeholder : "Campo do Usuário",
                        display     : false,
                        required    : false,
                    })
                },
                // PHONE
                {
                    row   : 9,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "fieldPhone",
                        label       : "Campo do Telefone",
                        placeholder : "Campo do Telefone",
                        display     : false,
                    })
                },
                {
                    row   : 10,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "subList",
                        label       : "Lista do objeto",
                        placeholder : "Lista do objeto",
                        display     : false,
                    })
                },
                // PARAMS
                {
                    row   : 11,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ObjectInput"),
                        name        : "params",
                        label       : "Params",
                        placeholder : "Params",
                        required    : false,
                    })
                },
                {
                    row   : 12,
                    col   : 1,
                    step  : generalSetting,
                    field : new Field({
                        type        : FieldType.type("RichText"),
                        name        : "message",
                        label       : "Mensagem",
                        placeholder : "Mensagem",
                    })
                },
                // {
                //     row   : 12,
                //     col   : 1,
                // 	step : generalSetting,
                //     field : new Field({
                //         type        : FieldType.type('Textarea'),
                //         name        : 'messageFlat',
                //         label       : 'Mensagem Mobile',
                //         placeholder : 'Mensagem Mobile',
                // 		required    : false,
                //     })
                // },
                {
                    row   : 13,
                    col   : 1,
                    step  : generalSetting,
                    field : {
                        type        : FieldType.type("SubForm"),
                        name        : "messageParts",
                        label       : "Mensagens",
                        placeholder : "Mensagens",
                        required    : false,   
                         setting     : {
                            orderBy : "order",
                        },                                             
                    },
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Select"),
                                name        : "type",
                                label       : "Nome",
                                placeholder : "Nome",
                                setting     : {
                                    controlField : "title,description,image,link,document,imageLink,documentLink,extension",
                                    title        : "title",
                                    description  : "description",
                                    link         : "title,description,image,link",
                                    image        : "title,image,imageLink",
                                    document     : "title,description,document,documentLink,extension",
                                },
                                option : {
                                    record : false,
                                    items  : [
                                        {
                                            label : "Título",
                                            value : "title",
                                        },
                                        {
                                            label : "Descrição",
                                            value : "description",
                                        },
                                        {
                                            label : "Imagem",
                                            value : "image",
                                        },
                                        {
                                            label : "Link",
                                            value : "link",
                                        },
                                        {
                                            label : "Documento",
                                            value : "document",
                                        }]
                                },
                            }
                        },
                        {
                            row   : 1,
                            col   : 2,
                            field : {
                                type        : FieldType.type("Integer"),
                                name        : "order",
                                label       : "Ordem",
                                placeholder : "Ordem",
                                required    : false,
                            }
                        },
                        {
                            row   : 2,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "title",
                                label       : "Título",
                                placeholder : "Título",
                                required    : false,
                            }
                        },
                        {
                            row   : 3,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Textarea"),
                                name        : "description",
                                label       : "Descrição",
                                placeholder : "Descrição",
                                required    : false,
                            }
                        },                        
                        {
                            row   : 4,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Image"),
                                name        : "image",
                                label       : "Imagem",
                                placeholder : "Imagem",
                                required    : false,
                            }
                        },
                        {
                            row   : 4,
                            col   : 2,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "imageLink",
                                label       : "Imagem Link",
                                placeholder : "Imagem Link",
                                required    : false,
                            }
                        },
                        {
                            row   : 5,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "link",
                                label       : "Link",
                                placeholder : "Link",
                                required    : false,
                            }
                        },
                        {
                            row   : 6,
                            col   : 1,
                            field : {
                                type        : FieldType.type("UploadJson"),
                                name        : "document",
                                label       : "Documento",
                                placeholder : "Documento",
                                required    : false,
                            }
                        },
                        {
                            row   : 6,
                            col   : 2,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "documentLink",
                                label       : "Documento Link",
                                placeholder : "Documento Link",
                                required    : false,
                            }
                        },
                        {
                            row   : 6,
                            col   : 3,
                            field : {
                                type        : FieldType.type("Text"),
                                name        : "extension",
                                label       : "Extensão do Arquivo",
                                placeholder : "Extensão do Arquivo",
                                required    : false,
                            }
                        },
                    ]
                },
                // MOBILE
                // {
                //     row   : 13,
                //     col   : 1,
                // 	step : mobileSetting,
                //     field : new Field({
                //         type        : FieldType.type('Text'),
                //         name        : 'Resumo',
                //         label       : 'resume',
                //         placeholder : 'resume',
                //     })
                // },      
                {
                    row   : 14,
                    col   : 2,
                    step  : mobileSetting,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "action",
                        label       : "Ação",
                        placeholder : "Ação",
                        option      : {
                            record : false,
                            items  : [
                                {
                                    label : "Alerta",
                                    value : "alert",
                                },
                                {
                                    label : "Confirmação",
                                    value : "confirm",
                                }
                            ]
                        },
                    })
                },    
                {
                    row   : 15,
                    col   : 3,
                    step  : mobileSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "router",
                        label       : "Rota",
                        placeholder : "Rota",      
                        required    : false,
                    })
                },
                // E-MAIL
                {
                    row   : 16,
                    col   : 1,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("Fieldset"),
                        name        : "from",
                        label       : "De",
                        placeholder : "De",                        
                    }),
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "name",
                                label       : "Nome",
                                placeholder : "Nome",
                                initial     : this.core().masterAccount.name,
                            })
                        },
                        {
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Email"),
                                name        : "email",
                                label       : "E-mail",
                                placeholder : "E-mail",
                                initial     : this.core().masterAccount.email,
                            })
                        },
                    ]
                },                
                {
                    row   : 18,
                    col   : 1,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("Image"),
                        name        : "imageHeader",
                        label       : "Image Header",
                        placeholder : "Image Header",
                        required    : false,
                    })
                },
                {
                    row   : 19,
                    col   : 1,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("Image"),
                        name        : "imageFooter",
                        label       : "Image Footer",
                        placeholder : "Image Footer",
                        required    : false,
                    })
                },
                {
                    row   : 19,
                    col   : 2,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "backgroudColorFooter",
                        label       : "Cor de Fundo do Footer",
                        placeholder : "Cor de Fundo do Footer",
                        required    : false,
                    })
                },
                {
                    row   : 19,
                    col   : 3,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "textColorFooter",
                        label       : "Cor do Texto do Footer",
                        placeholder : "Cor do Texto do Footer",
                        required    : false,
                    })
                },
                {
                    row   : 20,
                    col   : 1,
                    step  : emailSetting,
                    field : new Field({
                        type        : FieldType.type("RichText"),
                        name        : "textFooter",
                        label       : "Texto do Footer",
                        placeholder : "Texto do Footer",
                        required    : false,
                    })
                },
                {
                    row   : 21,
                    col   : 1,
                    step  : emailSetting,
                    field : {
                        type        : FieldType.type("SubForm"),
                        name        : "footerIcons",
                        label       : "Footer Icons",
                        placeholder : "Footer Icons",
                        required    : false,                     
                    },
                    items : [
                        {
                            row   : 1,
                            col   : 1,
                            field : {
                                type        : FieldType.type("Image"),
                                name        : "image",
                                label       : "Imagem",
                                placeholder : "Imagem",
                                required    : false,
                            }
                        },
                        {
                            row   : 1,
                            col   : 2,
                            field : {
                                type        : FieldType.type("Url"),
                                name        : "url",
                                label       : "Url",
                                placeholder : "Url",
                                required    : false,
                            }
                        },
                    ]
                },				
            ],
        });

        this.changeDetectorRef.markForCheck(); 
    }

    async onAdd(event:any)
    {
        const data = await this.emailTemplatePlugin.add(this.app.code, this.form, event);

        this.onClose();
    }

    async onSet(event:any)
    {
        const data = await this.emailTemplatePlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();        
    }
	
    onClose()
    {
        this.back();
    }
}
