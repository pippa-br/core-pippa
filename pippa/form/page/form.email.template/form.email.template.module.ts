import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }            from "../../../core";
import { FormEmailTemplatePage } from "./form.email.template.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormEmailTemplatePage } ])
    ],
    declarations : [
        FormEmailTemplatePage, 
    ],
})
export class FormEmailTemplateModule 
{}
