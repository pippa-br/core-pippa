import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }   from "../../../core/base/page/base.page";
import { FormMaster } from "../../component/form.master/form.master";
import { FormPlugin } from "../../../core/plugin/form/form.plugin";
import { StepItem }   from "../../../core/model/step.item/step.item";
import { Field }      from "../../../core/model/field/field";
import { FieldType }  from "../../../core/type/field/field.type";
import { Form }       from "../../../core/model/form/form";
import { FormType }   from "../../../core/type/form/form.type";
import { FormItem }   from "../../../core/model/form.item/form.item";
import { BaseModel } from "src/core-pippa/pippa/core";

@Component({
    selector        : "form-master-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Formulário Master
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="copyForm()">
                                        <ion-icon name="code-download-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only (click)="pastForm()">
                                        <ion-icon name="code-working-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding">
                            <div form-master
                                 #formMaster
                                 [app]="app"
                                 [form]="form"
                                 [data]="data"
                                 [acl]="acl"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormMasterPage extends BasePage
{
    @ViewChild("formMaster", { static : true }) formMaster : FormMaster;

    constructor(
        public formPlugin        : FormPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.formMaster.destroy();
    }

    initialize()
    {
        const params = this.createParams({
            appid : this.app.code,
            id    : "master",
        });

        this.formPlugin.get(params).then((result:any) =>
        {
            if(!result.data)
            {
                result.data = new BaseModel();
                result.data.exists = false;                
            }

            /* CASO O FORMS MASTER NAO EXISTA */
            if (!result.data.exists)
            {
                result.data.type        = FormType.SEGMENT_TYPE;
                result.data.typeDefault = FormType.FLAT_TYPE;
                result.data.openMode    = FormType.ROUTER_OPEN_MODE;
            }
		
            this.data = result.data;  					

            this.builderForm();
        });        
    }
    
    builderForm()
    {        
        const step0 = new StepItem({
            label : "Campos do Cadastro",
            order : 0,
        });

        const step1 = new StepItem({
            label : "Campos do Formulário",
            order : 1,
        });

        const step2 = new StepItem({
            label : "Configurações",
            order : 2,
        });

        const optionsType = [
            FormType.FLAT_TYPE,
            FormType.SEGMENT_TYPE,
            FormType.ACCORDION_TYPE,
            FormType.EXPAND_TYPE,
            FormType.WIZARD_TYPE,
            FormType.STEPS_TYPE,
        ];

        /* GENERAL FORM */
        const form = new Form({
            name : "Configurar Master",
            type : {
                label : "Segment",
                value : "segment",
            },
            items : [
                {
                    row   : 1,
                    col   : 1,
                    step  : step0,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "type",
                        label       : "Tipo do Formulário",
                        placeholder : "Tipo do Formulário",
                        clearable   : false,
                        option      : {
                            items : optionsType
                        },
                        initial  : FormType.SEGMENT_TYPE.value,
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },
                {
                    row   : 3,
                    col   : 1,
                    step  : step0,
                    field : new Field({
                        type     : FieldType.type("Fields"),
                        label    : "Propriedades",
                        //hasLabel  : false,
                        required : false,
                        name     : "fieldsForm",
                        option   : {
                            items : FieldType.getItems()
                        },
                        setting : {
                            app : this.app,
                        },
                        onChange : (() =>
                        {
                            //this.drawDisplayForm();
                        })
                    })
                },
                /*{
                    row   : 4,
                    col   : 1,
                    step  : step0,
                    field : new Field({
                        type      : FieldType.type('ReferenceFields'),
                        label     : 'Campos Personalizados',
                        //hasLabel  : false,
                        required  : false,
                        name      : 'fieldsDocument',
                        option    : {
                            items : [
                                FieldType.type('ReferenceFields'),
                            ]
                        },
                        setting : {
                            app : this.app,
                        },
                        onChange  : (() =>
                        {
                            //this.drawDisplayForm();
                        })
                    })
                },*/
                /*/{
                    row    : 5,
                    col    : 1,
                    step   : step0,
                    field  : new Field({
                        type        : FieldType.type('STEP'),
                        name        : 'steps',
                        label       : 'Tabs do Formulário',
                        placeholder : 'Tabs do Formulário',
                        record      : false,
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },*/
                {
                    row   : 6,
                    col   : 1,
                    step  : step0,
                    field : new Field({
                        type        : FieldType.type("Step"),
                        name        : "items",
                        label       : "Ordenar Campos",
                        placeholder : "Ordenar Campos",
                        setting     : {
                            //fields : 'fieldsForm,fieldsDocument',
                            fields : "fieldsForm",
                            //steps  : 'steps',
                        },
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },
                /* PRODUTO */
                {
                    row   : 7,
                    col   : 1,
                    step  : step1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "typeDefault",
                        label       : "Tipo do Formulário",
                        placeholder : "Tipo do Formulário",
                        clearable   : false,
                        option      : {
                            items : optionsType
                        },
                        initial  : FormType.FLAT_TYPE.value,
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },
                {
                    row   : 8,
                    col   : 1,
                    step  : step1,
                    field : new Field({
                        type     : FieldType.type("Fields"),
                        label    : "Propriedades",
                        //hasLabel  : false,
                        required : false,
                        name     : "fieldsDefault",
                        option   : {
                            items : FieldType.getItems()
                        },
                        setting : {
                            app : this.app,
                        },
                        onChange : (() =>
                        {
                            //this.displayForm.fields.populate(event.data);
                        })
                    })
                },
                /*{
                    row    : 9,
                    col    : 1,
                    step   : step1,
                    field  : new Field({
                        type        : FieldType.type('STEP'),
                        name        : 'stepsDefault',
                        label       : 'Tabs do Formulário',
                        placeholder : 'Tabs do Formulário',
                        record      : false,
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },*/
                {
                    row   : 10,
                    col   : 1,
                    step  : step1,
                    field : new Field({
                        type        : FieldType.type("Step"),
                        name        : "itemsDefault",
                        label       : "Configurar Campos",
                        placeholder : "Configurar Campos",
                        setting     : {
                            fields : "fieldsDefault",
                            //steps  : 'stepsDefault',
                        },
                        onChange : (() =>
                        {
                            //this.displayForm.type = event.data;
                            //this.drawDisplayForm();
                        })
                    })
                },
                {
                    row   : 11,
                    col   : 2,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "saveFixed",
                        label       : "Fixar ao Salvar",
                        placeholder : "Fixar ao Salvar",
                        initial     : false,
                    })
                },
                {
                    row   : 11,
                    col   : 2,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "stepSave",
                        label       : "Salvar Step",
                        placeholder : "Salvar Step",
                        initial     : false,
                    })
                },
                {
                    row   : 11,
                    col   : 2,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "validateForm",
                        label       : "Validar Form",
                        placeholder : "Validar Form",
                        initial     : false,
                    })
                },
                {
                    row   : 11,
                    col   : 3,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "sendTransection",
                        label       : "Enviar Transacionais",
                        placeholder : "Enviar Transacionais",
                        initial     : false,
                    })
                },
                {
                    row   : 11,
                    col   : 4,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "saveLog",
                        label       : "Salvar Logs",
                        placeholder : "Salvar Logs",
                        initial     : false,
                    })
                },
                {
                    row   : 11,
                    col   : 5,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "saveBackup",
                        label       : "Salvar Backup",
                        placeholder : "Salvar Backup",
                        initial     : false,
                    })
                },
                {
                    row   : 12,
                    col   : 2,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "sumForm",
                        label       : "Somar Formulário",
                        placeholder : "Somar Formulário",
                        initial     : false,
                    })
                },
                {
                    row   : 12,
                    col   : 3,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "orderFormItem",
                        label       : "Ordenar Campos",
                        placeholder : "Ordenar Campos",
                        initial     : true,
                    })
                },
                {
                    row   : 13,
                    col   : 1,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "hasHeader",
                        label       : "Header",
                        placeholder : "Header",
                        initial     : true,
                    })
                },
                {
                    row   : 13,
                    col   : 2,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "openMode",
                        label       : "Modo de Abertura",
                        placeholder : "Modo de Abertura",
                        option      : {
                            items : [
                                FormType.ROUTER_OPEN_MODE,
                                FormType.SIDE_OPEN_MODE,
                                FormType.MODAL_OPEN_MODE,
                            ]
                        },
                        initial : FormType.MODAL_OPEN_MODE.value
                    })
                },
                // SEARCH
                {
                    row   : 14,
                    col   : 1,
                    step  : step2,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "_search",
                        label       : "Busca",
                        placeholder : "Busca",
                        required    : false,						
                    }),
                    items : [
                        new FormItem({
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "path",
                                label       : "Path",
                                placeholder : "Path",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Checkbox"),
                                name        : "full",
                                label       : "Full",
                                placeholder : "Full",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("Checkbox"),
                                name        : "case",
                                label       : "Case Sensitive",
                                placeholder : "Case Sensitive",
                            })
                        })
                    ]
                }
            ]
        });
    
        this.form = form;    

        this.changeDetectorRef.markForCheck();
    }

    onSet(event:any)
    {
        /* LOAD */
        this.core().loadingController.start().then(async () =>
        {
            if (!event.data.accid)
            {
                event.data.accid = this.app.accid;
            }

            if (!event.data.appid)
            {
                event.data.appid = this.app.code;
            }
			
            delete event.data._logs;

            if (this.data.exists)
            {
                const data = await this.formPlugin.set(this.app.code, this.data, null, event);

                //this.data.save(event.data).then(() =>
                //{
                /* LOAD */
                //this.core().loadingController.stop().then(() => 
                //{
                this.onClose();
                //});
                //});
            }
            else
            {
                event.data.id = "master";
                const data 	  = await this.formPlugin.add(this.app.code, null, event);				

                //this.data.create(event.data).then(() =>
                //{
                /* LOAD */
                //this.core().loadingController.stop().then(() => 
                //{
                this.onClose();
                //});
                //});
            }            
        });
    }

    /* METODO PARA SER SOBSCRITO */
    getDataModel()
    {
        return Document;
    }

    onClose()
    {
        this.back();
    }
}
