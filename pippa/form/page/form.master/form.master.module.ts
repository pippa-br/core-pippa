import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }     from "../../../core";
import { FormMaster }     from "../../component/form.master/form.master";
import { FormMasterPage } from "./form.master.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormMasterPage } ])
    ],
    declarations : [
        FormMasterPage,
        FormMaster,
    ],
})
export class FormMasterModule 
{}
