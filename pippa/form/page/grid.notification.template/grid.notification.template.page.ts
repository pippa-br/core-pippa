import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }          		  from "../../../core/base/page/base.page";
import { GridNotificationTemplate }   from "../../component/grid.notification.template/grid.notification.template";
import { Grid }               		  from "../../../core/model/grid/grid";
import { FieldType }          		  from "../../../core/type/field/field.type";
import { NotificationTemplatePlugin } from "../../../core/plugin/notification.template/notification.template.plugin";

@Component({
    selector        : "grid-notification-template-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{form?.name}} - Notificações
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="goAddForm()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>

                            <div grid-notification-template
                                 #gridNotificationTemplate
                                 [app]="app"
                                 [acl]="acl"
                                 [grid]="grid"
                                 [collection]="collection">
                            </div>

                        </ion-content>`,
})
export class GridNotificationTemplatePage extends BasePage
{
    @ViewChild("gridNotificationTemplate", { static : true }) gridNotificationTemplate : GridNotificationTemplate;
    
    constructor(
        public notificationTemplatePlugin : NotificationTemplatePlugin,
        public changeDetectorRef  		  : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.gridNotificationTemplate.destroy();
    }

    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    row : 1,
                    field : {
                        label : "Assunto",
                        name  : "subject",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 2,
                    field : {
                        label : "Tipo",
                        name  : "type.label",
                        type  : FieldType.type("Text"),
                    }
                },
                /*{
                    label    : 'Label',
                    value    : 'label',
                    renderer : (item, column) =>
                    {
                        return item.label + (item.required ? ' *' : '');
                    }
                },
                {
                    label : 'Código',
                    value : 'name',
                },
                {
                    label : 'Tipo',
                    value : 'type',
                    renderer : (item, column) =>
                    {
                        return (item.type.label) ? item.type.label : item.type;
                    }
                },
                {
                    label : 'Row',
                    value : 'row',
                },
                {
                    label : 'Col',
                    value : 'col',
                }*/
            ],
            buttons : [
                /*{
                    icon : 'grid',
                    isEnabled : ((item) =>
                    {
                        return true;
                    }),
                    click : ((event) =>
                    {
                        this.setGridEvent.emit(event);
                    })
                },
                {
                    icon : 'mail',
                    isEnabled : ((item) =>
                    {
                        return true;
                    }),
                    click : ((event) =>
                    {
                        this.emailTemplateEvent.emit(event);
                    })
                }
                {
                    icon : 'add-circle-outline',
                    isEnabled : ((item) =>
                    {
                        return item.isContainer();
                    }),
                    click : ((item) =>
                    {
                        this.addEvent.emit({
                            parent : item
                        });
                    })
                }*/
            ]
        });

        const params = this.createParams({
            appid : this.app.code,
            order : "order",
        });

        params.where = [];

        params.where.push({
            field    : "form",
            operator : "==",
            value    : this.form.reference,
            type     : FieldType.type("ReferenceSelect")
        });

        this.notificationTemplatePlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    goAddForm()
    {
        this.push("form-notification-template",
            {
                formPath : this.form.reference.path
            });
    }

    onClose()
    {
        this.back();
    }
}
