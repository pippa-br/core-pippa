import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }           		from "../../../core";
import { GridNotificationTemplate }     from "../../component/grid.notification.template/grid.notification.template";
import { GridNotificationTemplatePage } from "./grid.notification.template.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridNotificationTemplatePage } ])
    ],
    declarations : [
        GridNotificationTemplatePage,
        GridNotificationTemplate,
    ],
})
export class GridNotificationTemplateModule 
{
}
