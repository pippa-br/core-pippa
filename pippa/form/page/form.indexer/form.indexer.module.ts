import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { FormIndexer }     from "../../component/form.indexer/form.indexer";
import { FormIndexerPage } from "./form.indexer.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormIndexerPage } ])
    ],
    declarations : [
        FormIndexerPage,
        FormIndexer,
    ],
})
export class FormIndexerModule 
{}
