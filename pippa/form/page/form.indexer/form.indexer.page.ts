import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }     from "../../../core/base/page/base.page";
import { FormIndexer }  from "../../component/form.indexer/form.indexer";
import { Field }        from "../../../core/model/field/field";
import { FieldType }    from "../../../core/type/field/field.type";
import { Form }         from "../../../core/model/form/form";
import { TDate } from "src/core-pippa/pippa/core/util/tdate/tdate";

@Component({
    selector        : "form-indexer-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Indexer
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div form-indexer
                                 #formIndexer
                                 [app]="app"
                                 [form]="searchForm"
                                 [data]="searchData"
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormIndexerPage extends BasePage
{
    @ViewChild("formIndexer", { static : true }) formIndexer : FormIndexer;

    public searchForm : any;
    public searchData : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    initialize()
    {
        this.form.on(true).then(() => 
        {
            const items        = this.form.getFormItemsDefault();
            const orderOptions = this.form.getOrderOptions();
    
            this.searchForm = new Form({
                items : [
                    {
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("SELECT"),
                            name        : "orderBy",
                            label       : "Ordenar",
                            placeholder : "Ordenar",
                            option      : orderOptions,
                            setting     : {
                                bindValue : "id",
                            }
                        })
                    },
                    {
                        row   : 1,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("SELECT"),
                            name        : "asc",
                            label       : "Tipo de Ordenação",
                            placeholder : "Tipo de Ordenação",
                            setting     : {
                                bindValue : "id",
                            },
                            option : {
                                record : false,
                                items  : [
                                    {
                                        id    : 0,
                                        label : "Crescente",
                                        value : true,
                                    },
                                    {
                                        id    : 1,
                                        label : "Decrescente",
                                        value : false,
                                    }
                                ]
                            },
                        })
                    },
                    {
                        row   : 2,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Step"),
                            name        : "items",
                            label       : "Colunas",
                            placeholder : "Colunas",
                            setting     : {
                                formItems    : items,
                                editFormItem : true
                            },
                            required : false,
                        })
                    },
                ],
            });
    
            /* GET DATA */
            const params = this.createParams({
                appid : this.app.code,
            });
    
            params.where = [];
    
            params.where.push({
                field    : "form",
                operator : "==",
                value    : this.form.reference,
                type     : FieldType.type("ReferenceSelect")
            });
    
            /* PROCURA SE O FORM JÁ EXISTE */
            /*this.searchPlugin.getData(params).then((result:any) =>
            {
                if(result.collection.length > 0)
                {
                    this.searchData = result.collection[0];
                }
                else
                {
                    this.collection = result.collection;
                }

                this.changeDetectorRef.markForCheck();
            });*/
        });        
    }

    onAdd(event:any)
    {
        /* LOAD */
        this.core().loadingController.start().then(() =>
        {
            if (!event.data.accid)
            {
                event.data.accid = this.app.accid;
            }

            if (!event.data.appid)
            {
                event.data.appid = this.app.code;
            }

            event.data.form     = this.form.reference;
            event.data.postdate = new TDate().toDate();
            event.data.lastdate = new TDate().toDate();

            if (this.core().user)
            {
                event.data.owner = this.core().user.reference;
            }

            this.collection.save(event.data).then(() =>
            {
                /* LOAD */
                this.core().loadingController.stop().then(() => 
                {
                    this.onClose();
                });
            });
        });
    }

    onSet(event:any)
    {
        /* LOAD */
        this.core().loadingController.start().then(() =>
        {
            if (!event.data.accid)
            {
                event.data.accid = this.app.accid;
            }

            if (!event.data.appid)
            {
                event.data.appid = this.app.code;
            }

            event.data.form     = this.form.reference;
            event.data.lastdate = new TDate().toDate();

            this.searchData.save(event.data).then(() =>
            {
                /* LOAD */
                this.core().loadingController.stop().then(() => 
                {
                    this.onClose();
                });
            });
        });
    }

    ionViewWillLeave()
    {
        this.formIndexer.destroy();
    }

    onClose()
    {
        this.back();
    }
}
