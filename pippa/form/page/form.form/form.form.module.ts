import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { FormForm }     from "../../component/form.form/form.form";
import { FormFormPage } from "./form.form.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormFormPage } ])
    ],
    declarations : [
        FormFormPage,
        FormForm,
    ],
})
export class FormFormModule 
{}
