import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef, Type } from "@angular/core";

/* PIPPA */
import { BasePage }    from "../../../core/base/page/base.page";
import { Field }       from "../../../core/model/field/field";
import { FieldType }   from "../../../core/type/field/field.type";
import { FormType }    from "../../../core/type/form/form.type";
import { Types }       from "../../../core/type/types";
import { FormPlugin }  from "../../../core/plugin/form/form.plugin";
import { EventPlugin } from "../../../core/plugin/event/event.plugin";
import { StepItem }    from "../../../core/model/step.item/step.item";
import { FormItem }    from "../../../core/model/form.item/form.item";
import { Form }        from "../../../core/model/form/form";
import { DynamicForm } from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";

@Component({
    selector        : "form-form-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl    	: "form.form.page.html",
})
export class FormFormPage extends BasePage
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @ViewChild("footer", { read : ElementRef }) public footer : ElementRef;

    constructor(
    public formPlugin        : FormPlugin,
    public eventPlugin       : EventPlugin,
    public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    onRendererComplete(event)
    {   
        const childElements = this.footer.nativeElement.childNodes;

        for (const child of childElements) 
        {
            this.footer.nativeElement.removeChild(child);
        }

        this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
    }

    /* METODO PARA SER SOBSCRITO */
    getDataModel()
    {
        return Form;
    }

    ionViewWillLeave()
    {
        if (this.dynamicForm)
        {
            this.dynamicForm.destroy();	
        }        
    }

    getPlugin()
    {
        return this.formPlugin;
    }

    initialize()
    {
        this.builderForm();
    }

    builderForm()
    {
        /* MASTER FORM */
        const params = this.createParams({
            appid : this.app.code,
            id 	  : "master",
        });

        const stepSetting = new StepItem({
            id    : "setting",   
            label : "Configurações",
            order : 999,
        });

        this.formPlugin.get(params).then((result:any) =>
        {
            const formMaster = result.data;

            formMaster.on(true).then(() =>
            {    
                let fields = "";
                let step   : any;

                for (const key in formMaster.items)
                {
                    const item = formMaster.items[key];
                    step       = item.step;

                    if (item.field.isFields())
                    {
                        fields += item.field.name + ",";
                    }
                }

                fields = fields.slice(0, -1);

                console.log("Configurar Campos", fields);

                /* STEP */
                if (formMaster.orderFormItem)
                {
                    formMaster.items.add(new FormItem({
                        row   : 10,
                        col   : 1,
                        step  : step,
                        field : new Field({
                            type        : FieldType.type("Step"),
                            name        : "items",
                            label       : "Configurar Campos",
                            placeholder : "Configurar Campos",
                            setting     : {
                                defaultItems : formMaster.itemsDefault,
                                fields       : fields,
                                steps        : "stepsForm",
                                //editFormItem : true,
                            },
                            onChange : (() =>
                            {
                                //this.displayForm.type = event.data;
                                //this.drawDisplayForm();
                            })
                        })
                    }));
                }

                /* ACTIONS */
                formMaster.items.add(new FormItem({
                    row   : 11,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "actions",
                        label       : "Ações",
                        placeholder : "Ações",
                        required    : false,						
                    }),
                    items : [						
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Select"),
                                name        : "action",
                                label       : "Ação",
                                placeholder : "Ação",
                                required    : true,
                                option      : {
                                    record : false,
                                    items  : [
                                        FormType.ALERT_ACTION,
                                        FormType.REDIRECT_ACTION,
                                        FormType.PAYMENT_ACTION,
                                    ]
                                },
                                setting : {
                                    controlField : "router,params,message",
                                    alert     		 : "message",
                                    redirect   	 : "router,params",
                                    payment    	 : "router,params",
                                }
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Select"),
                                name        : "mode",
                                label       : "Modo",
                                placeholder : "Modo",
                                required    : true,
                                option      : {
                                    record : false,
                                    items  : [
                                        FormType.ADD_MODE,
                                        FormType.SET_MODE,
                                        FormType.DEL_MODE,
                                    ]
                                },
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("MultiSelect"),
                                name        : "groups",
                                label       : "Grupo de Permissão",
                                placeholder : "Grupo de Permissão",
                                option      : {
                                    record : false,
                                    items  : this.core().optionPermission.items
                                },
                                initial : "all"
                            })
                        }),
                        {
                            row   : 1,
                            col   : 4,
                            field : new Field({
                                type        : FieldType.type("Select"),
                                name        : "callback",
                                label       : "Form",
                                placeholder : "Form",
                                required    : true,
                                option      : {
                                    record : false,
                                    items  : [
                                        FormType.CLEAR_CALLBACK,
                                        FormType.CLOSE_CALLBACK,
                                        FormType.BACK_CALLBACK,
                                    ]
                                },
                                initial : FormType.CLEAR_CALLBACK
                            })
                        },
                        {
                            row   : 2,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "router",
                                label       : "Rota",
                                placeholder : "Rota",
                            })
                        },
                        {
                            row   : 3,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("ObjectInput"),
                                name        : "params",
                                label       : "Params",
                                placeholder : "Params",
                                required    : false,
                            })
                        },
                        {
                            row   : 4,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Textarea"),
                                name        : "message",
                                label       : "Mensagem",
                                placeholder : "Mensagem",
                            })
                        },
                    ]
                }));

                /* SEARCH */
                formMaster.items.add(new FormItem({
                    row   : 12,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "_search",
                        label       : "Busca",
                        placeholder : "Busca",
                        required    : false,						
                    }),
                    items : [
                        new FormItem({
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "name",
                                label       : "Nome",
                                placeholder : "Nome",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "path",
                                label       : "Path",
                                placeholder : "Path",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 3,
                            field : new Field({
                                type        : FieldType.type("Select"),
                                name        : "type",
                                label       : "Tipo",
                                placeholder : "Tipo",
                                option      : {
                                    items : [
                                        Types.STRING_TYPE,
                                        Types.INT32_TYPE,
                                        Types.INT64_TYPE,										
                                        Types.FLOAT_TYPE,
                                        Types.DOUBLE_TYPE,
                                        Types.BOOL_TYPE,
                                        Types.GEOPOINT_TYPE,
                                        Types.ARRAY_STRING_TYPE,
                                        Types.ARRAY_INT32_TYPE,
                                        Types.ARRAY_INT64_TYPE,
                                        Types.ARRAY_FLOAT_TYPE,
                                        Types.ARRAY_DOUBLE_TYPE,
                                        Types.ARRAY_BOOL_TYPE,
                                    ]
                                }
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 4,
                            field : new Field({
                                type        : FieldType.type("Checkbox"),
                                name        : "full",
                                label       : "Full",
                                placeholder : "Full",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 5,
                            field : new Field({
                                type        : FieldType.type("Checkbox"),
                                name        : "case",
                                label       : "Case Sensitive",
                                placeholder : "Case Sensitive",
                            })
                        })
                    ] }));

                /* INDICES */
                formMaster.items.add(new FormItem({
                    row   : 13,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "indexes",
                        label       : "Índices",
                        placeholder : "Índices",
                        required    : false,						
                    }),
                    items : [
                        new FormItem({
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "key",
                                label       : "Key",
                                placeholder : "Key",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "path",
                                label       : "Path",
                                placeholder : "Path",
                            })
                        }),
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "default",
                                label       : "Default",
                                placeholder : "Default",
                                required    : false,
                                setting     : {
                                    convert : true
                                }
                            })
                        })
                    ]
                }));

                /* CHAMADAS */
                formMaster.items.add(new FormItem({
                    row   : 14,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "calls",
                        label       : "Chamadas",
                        placeholder : "Chamadas",
                        required    : false,						
                    }),
                    items : [		
                        {
                            row   : 1,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Toggle"),
                                name        : "status",
                                label       : "Status",
                                placeholder : "Status",
                            })
                        },
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("MultiSelect"),
                                name        : "types",
                                label       : "Tipos",
                                placeholder : "Tipos",
                                required    : true,
                                option      : {
                                    record : false,
                                    items  : [
                                        Types.BEFORE_ADD_CALL,
                                        Types.AFTER_ADD_CALL,
                                        Types.BEFORE_SET_CALL,										
                                        Types.AFTER_SET_CALL,
                                        Types.BEFORE_DEL_CALL,										
                                        Types.AFTER_DEL_CALL,										
                                    ]
                                }
                            })
                        }),						
                        new FormItem({
                            row   : 1,
                            col   : 2,
                            field : new Field({
                                type        : FieldType.type("MultiSelect"),
                                name        : "groups",
                                label       : "Grupo de Permissão",
                                placeholder : "Grupo de Permissão",
                                option      : {
                                    record : false,
                                    items  : this.core().optionPermission.items
                                },
                                initial : "all"
                            })
                        }),
                        {
                            row   : 2,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("Text"),
                                name        : "api",
                                label       : "API",
                                placeholder : "API",
                            })
                        },
                        {
                            row   : 3,
                            col   : 1,
                            field : new Field({
                                type        : FieldType.type("ObjectInput"),
                                name        : "params",
                                label       : "Params",
                                placeholder : "Params",
                                required    : false,
                            })
                        }
                    ]
                }));

                /* SETTINGS */
                formMaster.items.add(new FormItem({
                    row   : 15,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        id          : "setting",
                        type        : FieldType.type("ObjectInput"),
                        name        : "setting",
                        label       : "Configurações",
                        required    : false,
                        viewOptions : true,
                    })
                }));

                /* GRUPOS DE PERMISSOES */
                formMaster.items.add(new FormItem({
                    row   : 16,
                    col   : 1,
                    step  : stepSetting,
                    field : new Field({
                        type        : FieldType.type("MultiSelect"),
                        name        : "groups",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },							
                        initial : "all"			
                    })
                }));

                /* LEVEL */
                formMaster.items.add(new FormItem({
                    row   : 16,
                    col   : 2,
                    step  : stepSetting,
                    field : new Field({
                        id       : "_level",
                        type     : FieldType.type("Integer"),
                        name     : "_level",
                        label    : "Level",
                        required : false,
                        initial  : 1,
                    })
                }));				

                /* COLID */
                formMaster.items.add(new FormItem({
                    row   : 16,
                    col   : 3,
                    step  : stepSetting,
                    field : new Field({
                        id       : "_colid",
                        type     : FieldType.type("Text"),
                        name     : "_colid",
                        label    : "Colid",
                        required : false,
                    })
                }));

                formMaster.viewEdit = true;
                formMaster.name     = "Configurar Campos";
                this.form           = formMaster;

                this.changeDetectorRef.markForCheck();
            });
        });
    }

    async onAdd(event:any)
    {
        event.data.type            = this.form.typeDefault;
        event.data.openMode        = this.form.openMode;
        event.data.orderFormItem   = this.form.orderFormItem;
        event.data.saveFixed       = this.form.saveFixed;
        event.data.sendTransection = this.form.sendTransection;
        event.data.saveLog         = this.form.saveLog;
        event.data.saveBackup      = this.form.saveBackup;
        event.data.stepSave        = this.form.stepSave;
        event.data.sumForm         = this.form.sumForm;
        event.data.validateForm    = this.form.validateForm;
        event.data.hasHeader       = this.form.hasHeader;		

        const data = await this.formPlugin.add(this.app.code, this.form, event);

        this.eventPlugin.dispatch(this.app.code + ":add-form", data);     

        this.onClose();
    }

    async onSet(event:any)
    {
        event.data.type            = this.form.typeDefault;
        event.data.openMode        = this.form.openMode;
        event.data.orderFormItem   = this.form.orderFormItem;
        event.data.saveFixed       = this.form.saveFixed;
        event.data.sendTransection = this.form.sendTransection;
        event.data.saveLog         = this.form.saveLog;
        event.data.saveBackup      = this.form.saveBackup;
        event.data.stepSave        = this.form.stepSave;
        event.data.sumForm         = this.form.sumForm;
        event.data.validateForm    = this.form.validateForm;
        event.data.hasHeader       = this.form.hasHeader;

        const data = await this.formPlugin.set(this.app.code, this.data, this.form, event);

        /* EVENTS */
        this.eventPlugin.dispatch(this.app.code + ":set-form", data);

        this.onClose();
    }	

    onClose()
    {
        this.back();
    }
}
