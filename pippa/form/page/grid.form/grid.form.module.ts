import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { GridForm }     from "../../component/grid.form/grid.form";
import { GridFormPage } from "./grid.form.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridFormPage } ])
    ],
    declarations : [
        GridFormPage,
        GridForm,
    ],
})
export class GridFormModule 
{}
