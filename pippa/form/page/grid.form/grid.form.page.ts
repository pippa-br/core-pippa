import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { AlertController } from "@ionic/angular";

/* PIPPA */
import { BasePage }        from "../../../core/base/page/base.page";
import { DocumentPlugin }  from "../../../core/plugin/document/document.plugin";
import { ModalController } from "../../../core/controller/modal/modal.controller";
import { FormPlugin }      from "../../../core/plugin/form/form.plugin";
import { GridForm }        from "../../component/grid.form/grid.form";

@Component({
    selector        : "grid-form-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} / Formulários
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Buscar"
                                                (click)="onShowSearch()">
                                        <ion-icon name="search-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Adicionar"
                                                (click)="onAddForm($event)">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button *ifAcl="'form_index';acl:acl;"
                                                ion-button icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Indexar"
                                                (click)="goIndexer()">
                                        <ion-icon name="aperture-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button *ifAcl="'form_master';acl:acl;"
                                                ion-button icon-onlys
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Setup"
                                                (click)="goFormMaster()">
                                        <ion-icon name="cog-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                            <ion-toolbar class="toolbar-searchbar" [ngClass]="{'hide' : !showSearch}">
                                <ion-searchbar animated 
                                               placeholder="Buscar" 
                                               showCancelButton="never"
                                               [(ngModel)]="term"
                                               [debounce]="1000"
                                               (ionInput)="onSearch($event)">
                                </ion-searchbar>
                                <a class="arrow" (click)="onShowSearch()">
                                    <ion-icon name="caret-up-outline"></ion-icon>
                                </a>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div grid-form
                                #gridForm
                                [app]="app"
                                [acl]="acl"
                                [collection]="collection"
                                (setGrid)="onSetGridEvent($event)"
                                (setViewer)="onSetViewer($event)"
								(setFilter)="onSetFilter($event)"
								(setChart)="onSetChart($event)"
                                (setIndexer)="onSetIndexer($event)"
								(emailTemplate)="onEmailTemplate($event)"
								(notification)="onNotification($event)"
								(api)="onApi($event)"
                                (copy)="onCopy($event)"
                                (add)="onAddDocument($event)"
                                (set)="onSet($event)">
								(del)="onDel($event)"
                            </div>
                        </ion-content>
                        <ion-footer>
                            <div next-pagination [collection]="collection"></div>
                        </ion-footer>`,
})
export class GridFormPage extends BasePage
{
    @ViewChild("gridForm", { static : true }) gridForm : GridForm;

    public showSearch  = false;
    public term       : string;

    constructor(
        public formPlugin        : FormPlugin,
        public documentPlugin    : DocumentPlugin,
		public modalController   : ModalController,
		public alertController   : AlertController,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.gridForm.destroy();
    }

    loadAcl()
    {
        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "postdate",
            asc     : true,
            perPage : 100,
        });

        this.formPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;            

            this.changeDetectorRef.markForCheck();

            //this.flatCollection = result.collection.getFlat();

            //this.collection.change = () =>
            //{
            //console.log('change flatCollection');
            //this.flatCollection = this.collection.getFlat();
            //};
        });
    }

    onShowSearch()
    {
        this.showSearch = !this.showSearch;

        if (!this.showSearch)
        {
            this.clearSearch();	
            this.term = "";
        }
    }

    onSetGridEvent(event:any)
    {
        this.push("grid-grid",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    onSetViewer(event:any)
    {
        this.push("grid-viewer",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    onSetFilter(event:any)
    {
        this.push("grid-filter",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }
	
    onSetChart(event:any)
    {
        this.push("grid-chart",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    onSetIndexer(event:any)
    {
        this.push("form-indexer",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    onEmailTemplate(event:any)
    {
        this.push("grid-email-template",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    onNotification(event:any)
    {
        this.push("grid-notification-template",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    async onApi(event:any)
    {
        const data : any = {};	
		
        /* ACCOUNT */
        data.account = {
            referencePath : this.core().account.reference.path
        }

        /* APP */
        data.app = {
            referencePath : this.app.reference.path
        }
		
        /* FORM */
        data.form = {
            referencePath : event.data.reference.path
        }

        data.data = await event.data.getItemsData();

        const alert = await this.alertController.create({
            header   : "API",
            cssClass : "code-alert",
            inputs   : [
                {
                    type  : "textarea",
                    label : "Código",
                    value : JSON.stringify(data),
                },				  
            ],
            buttons : [
                {
                    text    : "Ok",
                    handler : () => 
                    {
                        // OVERRIDER
                    }
                }
            ]
        });

        await alert.present();
    }

    onAddForm(event:any)
    {
        event;

        this.push("form-form",
            {
                appid : this.app.code,
            });
    }

    onAddDocument(event:any)
    {
        event;
		
        this.push("form-document",
            {
                appid    : this.app.code,
                formPath : event.data.reference.path
            });
    }

    goFormMaster()
    {
        this.push("form-master",
            {
            //app : this.app
            //viewer : FormMasterPage,
            //master : true,
            });
    }

    onSet(event:any)
    {
        this.push("form-form",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path
            });
    }

    onSearch(event:any)
    {
        if (event.target.value == "")
        {
            this.collection.search();
        }
        else
        {
            this.collection.search(event.target.value);
        }
    }

    onCopy(event:any)
    {
        const keyFrom  = event.data.reference.id;
        const keyTo    = this.core().util.randomString(20);
        const fromPath = event.data.reference.path;
        const toPath   = fromPath.replace(keyFrom, keyTo);

        const params = this.createParams({
            app  : this.app,
            post : {
                fromPath : fromPath,
                toPath   : toPath,
            }
        });

        this.documentPlugin.copy(params).then((result:any) =>
        {
            if (result.status)
            {
                this.collection.push(result.data);
            }
        });
    }

    goIndexer()
    {
        this.push("indexer-form",
            {
            });
    }

    onClose()
    {
        this.back();
    }
}
