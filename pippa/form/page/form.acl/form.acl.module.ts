import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { FormAcl }     from "../../component/form.acl/form.acl";
import { FormAclPage } from "./form.acl.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormAclPage } ])
    ],
    declarations : [
        FormAclPage,
        FormAcl,
    ],
})
export class FormAclModule
{
}
