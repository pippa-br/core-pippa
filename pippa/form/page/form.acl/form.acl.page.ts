import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }     from "../../../core/base/page/base.page";
import { FormAcl }      from "../../component/form.acl/form.acl";
import { Field }        from "../../../core/model/field/field";
import { FieldType }    from "../../../core/type/field/field.type";
import { AclPlugin }    from "../../../core/plugin/acl/acl.plugin";
import { Form }         from "../../../core/model/form/form";
import { Acl }          from "../../../core/model/acl/acl";
 
@Component({
    selector        : "form-acl-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Acl
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div #formAcl
                                 form-acl
                                 [app]="app"
                                 [data]="data"
                                 [acl]="acl"
                                 [form]="aclForm"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormAclPage extends BasePage
{
    @ViewChild("formAcl", { static : true }) formAcl : FormAcl;

    public aclForm : any;

    constructor(
        public aclPlugin         : AclPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    /* METODO SOBSCRITO */
    getDataModel()
    {
        return Acl;
    }

    ionViewWillLeave()
    {
        this.formAcl.destroy();
    }

    initialize()
    {
        this.aclForm = new Form({
            items : [
                {
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "_group",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },
                        onInput : (event:any) =>
                        {
                            if (event.data)
                            {
                                const data : any = new Acl(event.data);

                                data.on().then(() =>
                                {
                                    const params = this.createParams({
                                        appid  : this.app.code,
                                        groups : [ data.value ]
                                    });

                                    this.aclPlugin.get(params).then(async (result:any) =>
                                    {
                                        /* CRIA SE NÃO EXISTE */
                                        if (!result.data.exists)
                                        {
                                            let data2 = {
                                                id     : data.value,
                                                accid  : this.core().masterAccount.code,
                                                appid  : this.app.code,
                                                colid  : "acls",
                                                _group : event.data,
                                            }

                                            if (event.data.value == "master")
                                            {
                                                data2 = Object.assign(data2, {
                                                    add_document   	   : true,
                                                    reload_grid  	 	   : true,
                                                    del_document   	   : true,
                                                    set_document   	   : true,
                                                    export_document    : true,
                                                    import_document    : true,
                                                    search_document    : true,
                                                    viewer_document    : true,
                                                    show_header_grid   : true,
                                                    show_header_viewer : true,
                                                    show_header_form   : true,
                                                    show_pagination    : true,
                                                    add_form      		   : true,
                                                    set_grid_grid  	   : true,
                                                    set_form_viewer    : true,
                                                    set_form_filter    : true,
                                                    set_form_email     : true,
                                                    set_form_search    : true,
                                                    set_form_form  	   : true,
                                                    del_form_form  	   : true,
                                                    acl_grid      		   : true,
                                                    log_document   	   : true,
                                                    list_document  	   : true,
                                                    clone_document 	   : false,
                                                });
                                            }

                                            result = await this.aclPlugin.add(this.app.code, this.form, 
                                                {
                                                    data : data2
                                                });
                                        }
                                        else // PARA MANTER O BIND ANTIGOS
                                        {
                                            result.data.accid  = this.core().account.code;
                                            result.data.appid  = this.app.code;
                                            result.data.colid  = "acls";
                                            result.data._group = event.data;
                                        }

                                        this.data = result.data;

                                        this.changeDetectorRef.markForCheck();
                                    });
                                });
                            }
                        }
                    })
                },
                {
                    row   : 2,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Label"),
                        name        : "label_document",
                        label       : "Permissão do Documento",
                        placeholder : "Permissão do Documento",
                    })
                },
                {
                    row   : 3,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "add_document",
                        label       : "Adicionar Documento",
                        placeholder : "Adicionar Documento",
                    })
                },
                {
                    row   : 3,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "archive_document",
                        label       : "Arquivar Documento",
                        placeholder : "Arquivar Documento",
                    })
                },
                {
                    row   : 3,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "unarchive_document",
                        label       : "Desarquivar Documento",
                        placeholder : "Desarquivar Documento",
                    })
                },
                {
                    row   : 3,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "cancel_document",
                        label       : "Caneclar Documento",
                        placeholder : "Caneclar Documento",
                    })
                },
                {
                    row   : 3,
                    col   : 6,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "reload_grid",
                        label       : "Reload Grid",
                        placeholder : "Reload Grid",
                    })
                },				
                {
                    row   : 4,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "del_document",
                        label       : "Remover Documento",
                        placeholder : "Remover Documento",
                    })
                },
                {
                    row   : 4,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_document",
                        label       : "Editar Documento",
                        placeholder : "Editar Documento",
                    })
                },
                {
                    row   : 4,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "export_document",
                        label       : "Exportar Documento",
                        placeholder : "Exportar Documento",
                    })
                },
                {
                    row   : 4,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "import_document",
                        label       : "Importar Documento",
                        placeholder : "Importar Documento",
                    })
                },
                {
                    row   : 4,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "expand_document",
                        label       : "Expandir Documento",
                        placeholder : "Expandir Documento",
                    })
                },				
                {
                    row   : 5,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "search_document",
                        label       : "Buscar Documento",
                        placeholder : "Buscar Documento",
                    })
                },
                {
                    row   : 5,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "viewer_document",
                        label       : "Visualizar Documento",
                        placeholder : "Visualizar Documento",
                    })
                },
                {
                    row   : 5,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "print_thermal_document",
                        label       : "Imprimir / Impressora Termica",
                        placeholder : "Imprimir / Impressora Termica",
                    })
                },
                {
                    row   : 5,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "print_document",
                        label       : "Imprimir",
                        placeholder : "Imprimir",
                    })
                },
                {
                    row   : 5,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "edit_viewer_document",
                        label       : "Editar ao Visualizar",
                        placeholder : "Editar ao Visualizar",
                    })
                },   
                {
                    row   : 6,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "filter_document",
                        label       : "Filtrar Document",
                        placeholder : "Filtrar Document",
                    })
                },
                {
                    row   : 6,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "show_header_grid",
                        label       : "Header Page Grid",
                        placeholder : "Header Page Grid",
                    })
                },
                {
                    row   : 6,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "show_header_viewer",
                        label       : "Header Page Viewer",
                        placeholder : "Header Page Viewer",
                    })
                },	
                {
                    row   : 6,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "show_header_form",
                        label       : "Header Page Form",
                        placeholder : "Header Page Form",
                    })
                },								
                {
                    row   : 6,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "show_pagination",
                        label       : "Visualizar Páginação",
                        placeholder : "Visualizar Páginação",
                    })
                }, 
                {
                    row   : 7,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "email_document",
                        label       : "E-mail Documento",
                        placeholder : "E-mail Documento",
                    })
                },
                {
                    row   : 7,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "clone_document",
                        label       : "Clone Documento",
                        placeholder : "Clone Documento",
                    })
                },
                {
                    row   : 7,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "list_document",
                        label       : "Listar Documento",
                        placeholder : "Listar Documento",
                    })
                },
                {
                    row   : 7,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "list_owner_document",
                        label       : "Listar Documento do Usuário",
                        placeholder : "Listar Documento do Usuário",
                    })
                },	
                /* API */
                {
                    row   : 8,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Label"),
                        name        : "label",
                        label       : "API",
                        placeholder : "API",
                    })
                },
                {
                    row   : 9,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "add_document_api",
                        label       : "Adicionar Documento via API",
                        placeholder : "Adicionar Documento via API",
                    })
                },
                {
                    row   : 9,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_document_api",
                        label       : "Editar Documento via API",
                        placeholder : "Editar Documento via API",
                    })
                },					                       
                /* PERFIL */
                {
                    row   : 10,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Label"),
                        name        : "label",
                        label       : "Perfil",
                        placeholder : "Perfil",
                    })
                },
                {
                    row   : 11,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "add_perfil",
                        label       : "Criar Perfil",
                        placeholder : "Criar Perfil",
                    })
                }, 
                {
                    row   : 11,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_perfil",
                        label       : "Editar Perfil",
                        placeholder : "Editar Perfil",
                    })
                }, 
                {
                    row   : 11,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "change_password_perfil",
                        label       : "Alterar Senha",
                        placeholder : "Alterar Senha",
                    })
                }, 
                /* CONFIGURAÇÕES */
                {
                    row   : 16,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Label"),
                        name        : "label",
                        label       : "Configurações",
                        placeholder : "Configurações",
                    })
                },
                {
                    row   : 17,
                    col   : 0,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "add_form",
                        label       : "Adicionar Formulário",
                        placeholder : "Adicionar Formulário",
                    })
                },
                {
                    row   : 17,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_grid_grid",
                        label       : "Gerenciar Grid",
                        placeholder : "Gerenciar Grid",
                    })
                },
                {
                    row   : 17,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_form_viewer",
                        label       : "Gerenciar Visualização",
                        placeholder : "Gerenciar Visualização",
                    })
                },
                {
                    row   : 17,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_form_filter",
                        label       : "Gerenciar Filtros",
                        placeholder : "Gerenciar Filtros",
                    })
                },
                {
                    row   : 17,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_form_email",
                        label       : "Gerenciar E-mails",
                        placeholder : "Gerenciar E-mails",
                    })
                },
                {
                    row   : 18,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_form_search",
                        label       : "Gerenciar Busca",
                        placeholder : "Gerenciar Busca",
                    })
                },
                {
                    row   : 18,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "set_form_form",
                        label       : "Editar Formulário",
                        placeholder : "Editar Formulário",
                    })
                },
                {
                    row   : 18,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "del_form_form",
                        label       : "Remover Formulário",
                        placeholder : "Remover Formulário",
                    })
                },
                {
                    row   : 18,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "acl_grid",
                        label       : "Acl Grid",
                        placeholder : "Acl Grid",
                    })
                },
                {
                    row   : 18,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "log_document",
                        label       : "Acl Logs",
                        placeholder : "Acl Logs",
                    })
                },				
            ],
        });

        this.changeDetectorRef.markForCheck();
    }

    async onSet(event:any)
    {
        await this.aclPlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();        
    }

    onClose()
    {
        this.back();
    }
}
