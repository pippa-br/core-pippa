import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { FormGrid }       from "../../component/form.grid/form.grid";
import { Field }          from "../../../core/model/field/field";
import { FieldType }      from "../../../core/type/field/field.type";
import { GridPlugin }     from "../../../core/plugin/grid/grid.plugin";
import { Form }           from "../../../core/model/form/form";
import { Grid }           from "../../../core/model/grid/grid";
import { PaginationType } from "../../../core/type/pagination/pagination.type";
import { FormType }       from "../../../core/type/form/form.type";

@Component({
    selector        : "form-grid-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    Configurar Grid
                                </ion-title>
								<ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="copyForm()">
                                        <ion-icon name="code-download-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only (click)="pastForm()">
                                        <ion-icon name="code-working-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div #formGrid
                                 form-grid
                                 [app]="app"
                                 [data]="data"
                                 [acl]="acl"
                                 [form]="gridForm"
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (cancel)="onClose()">
                            </div>
                        </ion-content>`,
})
export class FormGridPage extends BasePage
{
    public gridForm 	: any;
    public items 		: any;
    public orderOptions : any;

    @ViewChild("formGrid", { static : true }) formGrid : FormGrid;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
		public gridPlugin        : GridPlugin,
    )
    {
        super();
    }

    getPlugin()
    {
        return this.gridPlugin;
    }

    /* METODO SOBSCRITO */
    getDataModel()
    {
        return Grid;
    }

    initialize()
    {
        this.form.on(true).then(() =>
        {
            this.items        = this.form.getFormItemsDefault();
            this.orderOptions = this.form.getOrderOptions();

            this.builderForm();
        });
    }
    
    builderForm()
    {
        this.gridForm = new Form({
            type  : FormType.SEGMENT_TYPE,
            items : [
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "name",
                        label       : "Nome",
                        placeholder : "Nome",
                        required    : true,
                        initital    : this.app.name,
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("MultiSelect"),
                        name        : "groups",
                        label       : "Grupo de Permissão",
                        placeholder : "Grupo de Permissão",
                        option      : {
                            record : false,
                            items  : this.core().optionPermission.items
                        },										
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "type",
                        label       : "Tipo",
                        placeholder : "Tipo",
                        required    : true,
                        option      : {
                            record : false,
                            items  : [
                                Grid.TABLE_TYPE,
                                Grid.TILE_TYPE,
                                Grid.CARD_TYPE,
                                Grid.CALENDAR_TYPE,
                                Grid.SLIDE_TYPE,
                                Grid.TAB_TYPE,
                                Grid.MAP_TYPE,
                            ]
                        },
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("SELECT"),
                        name        : "orderBy",
                        label       : "Ordenar",
                        placeholder : "Ordenar",
                        option      : this.orderOptions,
                        required    : true,
                        setting     : {
                            bindValue : "id",
                        }
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "asc",
                        label       : "Tipo de Ordenação",
                        placeholder : "Tipo de Ordenação",
                        required    : true,
                        setting     : {
                            bindValue : "id",
                        },
                        option : {
                            record : false,
                            items  : [
                                {
                                    id       : 1,
                                    label    : "Crescente",
                                    value    : true,
                                    selected : true,
                                },
                                {
                                    id    : 0,
                                    label : "Decrescente",
                                    value : false,
                                }
                            ]
                        },
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 6,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "groupBy",
                        label       : "Agrupar",
                        placeholder : "Agrupar",
                        option      : this.orderOptions,
                        required    : false,
                        setting     : {
                            bindValue : "id",
                        }
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 6,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "groupCollection",
                        label       : "Consolidar",
                        placeholder : "Consolidar",
                        required    : false,
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 1,
                    col   : 7,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "map",
                        label       : "MapItems",
                        placeholder : "MapItems",
                        required    : false,
                    })
                },
                {
                    step : {
                        id    : 0,
                        label : "Geral"
                    },
                    row   : 2,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Step"),
                        name        : "items",
                        label       : "Colunas",
                        placeholder : "Colunas",
                        setting     : {
                            formItems    : this.items,
                            editFormItem : true,
                        },
                    })
                },
                /* REVER
				{
					row : 1,
					col : 3,
					field : new Field({
						type        : FieldType.type('TEXT'),
						name        : 'plugin',
						label       : 'Plugin',
						placeholder : 'Plugin',
						required    : false,
					})
				},*/
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "paginationType",
                        label       : "Tipo de Páginação",
                        placeholder : "Tipo de Páginação",
                        required    : true,
                        option      : {
                            items : [
                                PaginationType.NUMBER,
                                PaginationType.INFINITE,
                                PaginationType.DAY,
                                PaginationType.WEEK,
                                PaginationType.MONTH,
                            ]
                        },
                        initial : "number",
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("INTEGER"),
                        name        : "perPage",
                        label       : "PerPage",
                        placeholder : "PerPage",
                        required    : true,
                        initial     : 40,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "_colid",
                        label       : "Colid",
                        placeholder : "Colid",
                        required    : false,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("INTEGER"),
                        name        : "_level",
                        label       : "Level",
                        placeholder : "Level",
                        required    : true,
                        initial     : 1,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 3,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "viewerOpenMode",
                        label       : "Modo de Abertura da Viewer",
                        placeholder : "Modo de Abertura da Viewer",
                        option      : {
                            items : [
                                FormType.ROUTER_OPEN_MODE,
                                FormType.MODAL_OPEN_MODE,
                            ]
                        },
                        initial : FormType.MODAL_OPEN_MODE.value
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("INTEGER"),
                        name        : "cols",
                        label       : "Colunas",
                        placeholder : "Colunas",
                        required    : true,
                        initial     : 5,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("ReferenceSelect"),
                        name        : "filterApp",
                        label       : "Ir Para",
                        placeholder : "Ir Para",
                        required    : false,
                        option      : {
                            record : false,
                            app    : this.core().getApp("apps")
                        }
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "toRouter",
                        label       : "Rota",
                        placeholder : "Rota",
                        required    : false,
                        setting     : {
                            optionID : "routes"
                        }
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "isExport",
                        label       : "Ativar Exportação",
                        placeholder : "Ativar Exportação",
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 6,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "hasHeader",
                        label       : "Ativar Header",
                        placeholder : "Ativar Header",
                        initial     : true,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 4,
                    col   : 7,
                    field : new Field({
                        type        : FieldType.type("Checkbox"),
                        name        : "hasSetButton",
                        label       : "Editar Documento",
                        placeholder : "Editar Documento",
                        initial     : true,
                    })
                },				
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 5,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ObjectInput"),
                        name        : "params",
                        label       : "Params",
                        placeholder : "Params",
                        required    : false,
                    })
                },
                {
                    step : {
                        id    : 1,
                        label : "Geral"
                    },
                    id    : "setting",
                    row   : 6,
                    col   : 1,
                    field : new Field({
                        id          : "setting",
                        type        : FieldType.type("ObjectInput"),
                        name        : "setting",
                        label       : "Configurações",
                        required    : false,
                        viewOptions : true,
                    })
                },
                /* BUTTONS */
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 7,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "buttons",
                        label       : "Botões",
                        placeholder : "Botões",
                        required    : false,
                        setting     : {
                            orderBy : "order",
                        }                 
                    }),
                    items : [{
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "status",
                            label       : "Status",
                            placeholder : "Status",
                        })
                    },
                    {
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "dropdown",
                            label       : "Dropdown ",
                            placeholder : "Dropdown",
                        })
                    },
                    {
                        row   : 2,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Select"),
                            name        : "type",
                            label       : "Tipo",
                            placeholder : "Tipo",
                            option      : {
                                record : false,
                                items  : [
                                    {
                                        id       : "route",
                                        label    : "Rota",
                                        value    : "route",
                                        selected : true,
                                    },
                                    {
                                        id    : "url",
                                        label : "Url",
                                        value : "url",
                                    },
                                    {
                                        id    : "function",
                                        label : "Função",
                                        value : "function",
                                    },
                                    {
                                        id    : "api",
                                        label : "API",
                                        value : "api",
                                    },
                                    {
                                        id    : "edit",
                                        label : "Edit",
                                        value : "edit", 
                                    },
                                    {
                                        id    : "form",
                                        label : "Form",
                                        value : "form", 
                                    }
                                ]
                            },
                        }),
                    },
                    {
                        row   : 2,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("SelectIcon"),
                            name        : "icon",
                            label       : "Icon",
                            placeholder : "Icon",
                        })
                    },
                    {
                        row   : 2,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "label",
                            label       : "Label",
                            placeholder : "Label",
                        })
                    },
                    {
                        row   : 2,
                        col   : 4,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "router",
                            label       : "Rota",
                            placeholder : "Rota",
                        })
                    },
                    {
                        row   : 2,
                        col   : 5,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "displayPath",
                            label       : "Display Path",
                            placeholder : "Display Path",
                            required   	: false,
                        })
                    },		
                    {
                        row   : 2,
                        col   : 6,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "hidePath",
                            label       : "Hide Path",
                            placeholder : "Hide Path",
                            required   	: false,
                        })
                    },		
                    {
                        row   : 2,
                        col   : 7,
                        field : new Field({
                            type        : FieldType.type("Integer"),
                            name        : "order",
                            label       : "Ordem",
                            placeholder : "Ordem",								
                        })
                    },	
                    {
                        row   : 3,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("MultiSelect"),
                            name        : "groups",
                            label       : "Grupo de Permissão",
                            placeholder : "Grupo de Permissão",
                            option      : {
                                record : false,
                                items  : this.core().optionPermission.items
                            },										
                        })
                    },	
                    {
                        row   : 4,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("ObjectInput"),
                            name        : "params",
                            label       : "Parametros",
                            placeholder : "Parametros",
                            required    : false,
                        })
                    } ]
                },
                /* BATCH */
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 8,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "batch",
                        label       : "Ações em Lote",
                        placeholder : "Ações em Lote",
                        required    : false,
                        setting     : {
                            orderBy : "order",
                        }                 
                    }),
                    items : [ {
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "status",
                            label       : "Status",
                            placeholder : "Status",
                        })
                    },
                    {
                        row   : 2,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("SelectIcon"),
                            name        : "icon",
                            label       : "Icon",
                            placeholder : "Icon",
                        })
                    },
                    {
                        row   : 2,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "label",
                            label       : "Label",
                            placeholder : "Label",
                        })
                    },
                    {
                        row   : 2,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "url",
                            label       : "Url",
                            placeholder : "Url",
                        })
                    },
                    {
                        row   : 2,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "validate",
                            label       : "Validar",
                            placeholder : "Validar",
                            required    : false,
                        })
                    },
                    {
                        row   : 2,
                        col   : 4,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "validateMessage",
                            label       : "Mensagem da Validação",
                            placeholder : "Mensagem da Validação",
                            required    : false,
                        })
                    },		
                    {
                        row   : 2,
                        col   : 5,
                        field : new Field({
                            type        : FieldType.type("Integer"),
                            name        : "order",
                            label       : "Ordem",
                            placeholder : "Ordem",								
                        })
                    },                    
                    {
                        row   : 3,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("MultiSelect"),
                            name        : "groups",
                            label       : "Grupo de Permissão",
                            placeholder : "Grupo de Permissão",
                            option      : {
                                record : false,
                                items  : this.core().optionPermission.items
                            },										
                        })
                    },
                    {
                        row   : 3,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("Checkbox"),
                            name        : "download",
                            label       : "Download",
                            placeholder : "Download",								
                        })
                    },
                    {
                        row   : 3,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Checkbox"),
                            name        : "noSelected",
                            label       : "Sem Selecção",
                            placeholder : "Sem Selecção",								
                        })
                    },
                    {
                        row   : 4,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("ObjectInput"),
                            name        : "params",
                            label       : "Parametros",
                            placeholder : "Parametros",
                            required    : false,
                        })
                    } ]
                },
                /* DESCRIPTION */
                {
                    step : {
                        id    : 1,
                        label : "Configurações"
                    },
                    row   : 9,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("RichText"),
                        name        : "description",
                        label       : "Descrição",
                        placeholder : "Descrição",
                        required    : false,
                    })
                },					
                /* FILTERS */
                {
                    step : {
                        id    : 2,
                        label : "Filtros"
                    },
                    row   : 10,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "filters",
                        label       : "Filtros",
                        placeholder : "Filtros",
                        required    : false,                            
                    }),
                    items : FieldType.getFilterItems()
                },
                /* INNER JOIN */
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 11,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ReferenceApps"),
                        name        : "innerApp",
                        label       : "App",
                        placeholder : "App",
                        required    : false,
                    })
                }, 
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 12,
                    col   : 2,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "innerMappedBy",
                        label       : "MappedBy",
                        placeholder : "MappedBy",
                        required    : false,
                    })
                },
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 13,
                    col   : 3,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "innerMappedType",
                        label       : "MappedType",
                        placeholder : "MappedType",
                        required    : false,
                        setting     : {
                            bindValue : "id",
                        },
                        option : {
                            record : false,
                            items  : [
                                {
                                    id       : 1,
                                    label    : "Objeto",
                                    value    : "object",
                                    selected : true,
                                },
                                {
                                    id    : 0,
                                    label : "Collection",
                                    value : "collection",
                                }
                            ]
                        },
                    })
                },
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 14,
                    col   : 4,
                    field : new Field({
                        type        : FieldType.type("Text"),
                        name        : "innerOrderBy",
                        label       : "Ordenar",
                        placeholder : "Ordenar",
                        required    : false,
                        setting     : {
                            bindValue : "id",
                        }
                    })
                },
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 15,
                    col   : 5,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "innerAsc",
                        label       : "Tipo de Ordenação",
                        placeholder : "Tipo de Ordenação",
                        required    : false,
                        setting     : {
                            bindValue : "id",
                        },
                        option : {
                            record : false,
                            items  : [
                                {
                                    id       : 1,
                                    label    : "Crescente",
                                    value    : true,
                                    selected : true,
                                },
                                {
                                    id    : 0,
                                    label : "Decrescente",
                                    value : false,
                                }
                            ]
                        },
                    })
                },
                {
                    step : {
                        id    : 3,
                        label : "Inner"
                    },
                    row   : 16,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "innerFilters",
                        label       : "Inner Filtro",
                        placeholder : "Inner Filtro",
                        required    : false,                            
                    }),
                    items : FieldType.getFilterItems()
                },
            ],
        });

        this.changeDetectorRef.markForCheck();              
    }

    async onAdd(event:any)
    {
        const data = await this.gridPlugin.add(this.app.code, this.form, event);

        this.onClose();
    }

    async onSet(event:any)
    {
        const data = await this.gridPlugin.set(this.app.code, this.data, this.form, event);

        this.onClose();        
    }

    ionViewWillLeave()
    {
        this.formGrid.destroy();
    }

    onClose()
    {
        this.back();
    }
}
