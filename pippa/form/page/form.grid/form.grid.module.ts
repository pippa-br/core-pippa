import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { FormGrid }     from "../../component/form.grid/form.grid";
import { FormGridPage } from "./form.grid.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormGridPage } ])
    ],
    declarations : [
        FormGridPage,
        FormGrid,
    ],
    // entryComponents : [
    //     //FormFormPage,
    //     //FormForm,
    // ]
})
export class FormGridModule 
{}
