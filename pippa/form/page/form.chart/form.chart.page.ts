import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { FormGrid }       from "../../component/form.grid/form.grid";
import { Field }          from "../../../core/model/field/field";
import { FieldType }      from "../../../core/type/field/field.type";
import { ChartPlugin }    from "../../../core/plugin/chart/chart.plugin";
import { Form }           from "../../../core/model/form/form";
import { Chart }          from "../../../core/model/chart/chart"; 
import { FormType }       from "../../../core/type/form/form.type";
import { PaginationType } from "../../../core/type/pagination/pagination.type";
import { DynamicForm } from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";
import { Chat } from "src/core-pippa/pippa/core/model/chat/chat";

@Component({
    selector        : "form-chart-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl    	: "form.chart.page.html",
})
export class FormChartPage extends BasePage
{
    public chartForm    : any; // FORM LOCAL, POIS TEM O FORM QUE VEM POR URL
    public orderOptions : any;

@ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
@ViewChild("footer",    { read : ElementRef }) public footer : ElementRef;

constructor(
public changeDetectorRef : ChangeDetectorRef,
public chartPlugin       : ChartPlugin,
)
{
    super();
}

onRendererComplete(event)
{   
    const childElements = this.footer.nativeElement.childNodes;

    for (const child of childElements) 
    {
        this.footer.nativeElement.removeChild(child);
    }

    this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
}

initialize()
{
    this.form.on(true).then(() =>
    {
        this.orderOptions = this.form.getOrderOptions();

        this.builderForm();
    });		
}

builderForm()
{
    this.chartForm = new Form({
        type  : FormType.SEGMENT_TYPE,
        items : [
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("Text"),
                    name        : "name",
                    label       : "Nome",
                    placeholder : "Nome",
                    required    : true,
                    initital    : this.app.name,
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 2,
                field : new Field({
                    type        : FieldType.type("MultiSelect"),
                    name        : "groups",
                    label       : "Grupo de Permissão",
                    placeholder : "Grupo de Permissão",
                    option      : {
                        record : false,
                        items  : this.core().optionPermission.items
                    },										
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 3,
                field : new Field({
                    type        : FieldType.type("Select"),
                    name        : "type",
                    label       : "Tipo",
                    placeholder : "Tipo",
                    required    : true,
                    option      : {
                        record : false,
                        items  : [
                            Chart.PIE_TYPE,
                            Chart.BAR_TYPE,
                            Chart.LINE_TYPE,
                            Chart.CARD_TYPE,
                        ]
                    },
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 4,
                field : new Field({
                    type        : FieldType.type("Select"),
                    name        : "paginationType",
                    label       : "Tipo de Páginação",
                    placeholder : "Tipo de Páginação",
                    required    : true,
                    option      : {
                        items : [
                            PaginationType.DAY7D,
                            PaginationType.DAY7,
                            PaginationType.DAY15,
                            PaginationType.MONTH,
                            PaginationType.MONTH_YEAR,
                            PaginationType.YEAR,
                        ]
                    },
                    initial : "number",
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 5,
                field : new Field({
                    type        : FieldType.type("Select"),
                    name        : "orderBy",
                    label       : "Ordenar",
                    placeholder : "Ordenar",
                    option      : this.orderOptions,
                    required    : true,
                    setting     : {
                        bindValue : "id",
                    }
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 1,
                col   : 6,
                field : new Field({
                    type        : FieldType.type("Select"),
                    name        : "asc",
                    label       : "Tipo de Ordenação",
                    placeholder : "Tipo de Ordenação",
                    required    : true,
                    setting     : {
                        bindValue : "id",
                    },
                    option : {
                        record : false,
                        items  : [
                            {
                                id       : 1,
                                label    : "Crescente",
                                value    : true,
                                selected : true,
                            },
                            {
                                id    : 0,
                                label : "Decrescente",
                                value : false,
                            }
                        ]
                    },
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 2,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("MultiSelect"),
                    name        : "pagination",
                    label       : "Páginação",
                    placeholder : "Páginação",
                    required    : false,
                    option      : {
                        items : [
                            PaginationType.DAY7D,
                            PaginationType.DAY7,
                            PaginationType.DAY15,
                            PaginationType.MONTH,
                            PaginationType.MONTH_YEAR,
                            PaginationType.YEAR,
                        ]
                    },
                    initial : "number",
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 2,
                col   : 2,
                field : new Field({
                    type        : FieldType.type("Checkbox"),
                    name        : "sortX",
                    label       : "Ordenar X",
                    placeholder : "Ordenar X",
                    required    : false,
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 2,
                col   : 3,
                field : new Field({
                    type        : FieldType.type("Checkbox"),
                    name        : "sortY",
                    label       : "Ordenar Y",
                    placeholder : "Ordenar Y",
                    required    : false,
                })
            },
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 2,
                col   : 4,
                field : new Field({
                    type        : FieldType.type("Checkbox"),
                    name        : "showTotal",
                    label       : "Show Total",
                    placeholder : "Show Total",
                    required    : false,
                })
            },       
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 2,
                col   : 5,
                field : new Field({
                    type        : FieldType.type("Checkbox"),
                    name        : "showAverage",
                    label       : "Show Media",
                    placeholder : "Show Media",
                    required    : false,
                })
            },     	
            {
                step : {
                    id    : 0,
                    label : "Geral"
                },
                row   : 5,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("SubForm"),
                    name        : "y",
                    label       : "Y",
                    placeholder : "Y",
                    required    : false,
                    setting     : {
                        orderBy : "order"
                    }
                }),
                items : [
                    {
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "status",
                            label       : "Status",
                            placeholder : "Status",
                            required    : true,
                        })
                    },
                    {
                        row   : 2,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Integer"),
                            name        : "order",
                            label       : "order",
                            placeholder : "order",
                            required    : true,
                        })
                    },
                    {
                        row   : 2,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("ColorPicker"),
                            name        : "backgroundColor",
                            label       : "backgroundColor",
                            placeholder : "backgroundColor",
                            required    : true,
                        })
                    },
                    {
                        row   : 2,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("ColorPicker"),
                            name        : "color",
                            label       : "Color",
                            placeholder : "Color",
                            required    : true,
                        })
                    },
                    {
                        row   : 2,
                        col   : 4,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "projection",
                            label       : "Projeção",
                            placeholder : "Projeção",
                            required    : true,
                        })
                    },
                    {
                        row   : 2,
                        col   : 4,
                        field : new Field({
                            type        : FieldType.type("Toggle"),
                            name        : "count",
                            label       : "Contador",
                            placeholder : "Contador",
                            required    : true,
                        })
                    },
                    {
                        row   : 3,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "xPath",
                            label       : "xPath",
                            placeholder : "xPath",
                            required    : true,
                        })
                    },
                    {
                        row   : 3,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "yLabel",
                            label       : "yLabel",
                            placeholder : "yLabel",
                            required    : true,
                        })
                    },
                    {
                        row   : 3,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "yPath",
                            label       : "yPath",
                            placeholder : "yPath",
                            required    : true,
                        })
                    },
                    {
                        row   : 3,
                        col   : 4,
                        field : new Field({
                            type        : FieldType.type("Select"),
                            name        : "function",
                            label       : "Função",
                            placeholder : "Função",
                            required    : true,
                            option      : {
                                items : [
                                    Chart.COUNT_FUNCTION,
                                    Chart.SUM_FUNCTION,
                                    Chart.AVERAGE_FUNCTION,                                    
                                    Chart.VALUE_FUNCTION,
                                ]
                            },
                        })
                    },
                    {
                        row   : 4,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("Select"),
                            name        : "format",
                            label       : "Formatação",
                            placeholder : "Formatação",
                            required    : true,
                            option      : {
                                items : [
                                    Chart.VALUE_FORMAT,
                                    Chart.CURRENCY_FORMAT,
                                ]
                            },
                        })
                    },
                    /* FILTERS */
                    {
                        step : {
                            id    : 2,
                            label : "Filtros"
                        },
                        row   : 5,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("SubForm"),
                            name        : "filters",
                            label       : "Filtros",
                            placeholder : "Filtros",
                            required    : false,                            
                        }),
                        items : FieldType.getFilterItems()
                    },
                    /* PARAMS */
                    {
                        step : {
                            id    : 2,
                            label : "Params"
                        },
                        row   : 6,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("ObjectInput"),
                            name        : "params",
                            label       : "Params",
                            placeholder : "Params",
                            required    : false,
                        })
                    },
                ]
            },
            {
                step : {
                    id    : 1,
                    label : "Configurações"
                },
                row   : 7,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("ObjectInput"),
                    name        : "setting",
                    label       : "Configurações",
                    placeholder : "Configurações",
                    required    : false,
                })
            },					
            /* DESCRIPTION */
            {
                step : {
                    id    : 1,
                    label : "Configurações"
                },
                row   : 8,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("RichText"),
                    name        : "description",
                    label       : "Descrição",
                    placeholder : "Descrição",
                    required    : false,
                })
            },
            /* FILTERS */
            {
                step : {
                    id    : 2,
                    label : "Filtros"
                },
                row   : 9,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("SubForm"),
                    name        : "filters",
                    label       : "Filtros",
                    placeholder : "Filtros",
                    required    : false,                            
                }),
                items : FieldType.getFilterItems()
            },
            /* INNER JOIN */
            /*{
step : {
id    : 3,
label : 'Inner'
},
row : 9,
col : 1,
field : new Field({
type        : FieldType.type('ReferenceApps'),
name        : 'innerApp',
label       : 'App',
placeholder : 'App',
required    : false,
})
}, 
{
step : {
id    : 3,
label : 'Inner'
},
row : 9,
col : 2,
field : new Field({
type        : FieldType.type('Text'),
name        : 'innerMappedBy',
label       : 'MappedBy',
placeholder : 'MappedBy',
required    : false,
})
},
{
step : {
id    : 3,
label : 'Inner'
},
row : 9,
col : 3,
field : new Field({
type        : FieldType.type('Select'),
name        : 'innerMappedType',
label       : 'MappedType',
placeholder : 'MappedType',
required    : false,
setting     : {
bindValue : 'id',
},
option : {
record : false,
items : [
{
id       : 1,
label    : 'Objeto',
value    : 'object',
selected : true,
},
{
id    : 0,
label : 'Collection',
value : 'collection',
}
]
},
})
},
{
step : {
id    : 3,
label : 'Inner'
},
row : 9,
col : 4,
field : new Field({
type        : FieldType.type('Text'),
name        : 'innerOrderBy',
label       : 'Ordenar',
placeholder : 'Ordenar',
required    : false,
setting     : {
bindValue : 'id',
}
})
},
{
step : {
id    : 3,
label : 'Inner'
},
row : 9,
col : 5,
field : new Field({
type        : FieldType.type('Select'),
name        : 'innerAsc',
label       : 'Tipo de Ordenação',
placeholder : 'Tipo de Ordenação',
required    : false,
setting     : {
bindValue : 'id',
},
option : {
record : false,
items : [
{
id       : 1,
label    : 'Crescente',
value    : true,
selected : true,
},
{
id    : 0,
label : 'Decrescente',
value : false,
}
]
},
})
},
{
step : {
id    : 3,
label : 'Inner'
},
row : 10,
col : 1,
field : new Field({
type        : FieldType.type('SubForm'),
name        : 'innerFilters',
label       : 'Inner Filtro',
placeholder : 'Inner Filtro',
required    : false,                            
}),
items : [
{
row : 1,
col : 1,
field : new Field({
type        : FieldType.type('Select'),
name        : 'type',
label       : 'Tipo',
placeholder : 'Tipo',
option : {
items : [
FieldType.type('Text'),
FieldType.type('ReferenceSelect'),
FieldType.type('Integer'),
FieldType.type('Number'),
]
},
})
},                        
{
row : 1,
col : 2,
field : new Field({
type        : FieldType.type('Text'),
name        : 'field',
label       : 'Field',
placeholder : 'Field',
})
},
{
row : 1,
col : 3,
field : new Field({
type        : FieldType.type('Text'),
name        : 'operator',
label       : 'Operador',
placeholder : 'Operador',
initial     : '=='
})
},
{
row : 1,
col : 4,
field : new Field({
type        : FieldType.type('Text'),
name        : 'value',
label       : 'Valor',
placeholder : 'Valor',
required    : false,
setting : {
convert : true
}
})
},
{
row : 1,
col : 5,
field : new Field({
type        : FieldType.type('Text'),
name        : 'context',
label       : 'Contexto',
placeholder : 'Contexto',
required    : false,
})
},							
{
row : 1,
col : 6,
field : new Field({
type        : FieldType.type('Text'),
name        : 'path',
label       : 'Path',
placeholder : 'Path',
required    : false,
})
},
]
},*/
        ],
    });

    this.changeDetectorRef.markForCheck();	
}

async onAdd(event:any)
{
    const data = await this.chartPlugin.add(this.app.code, this.form, event);

    this.onClose();
}

async onSet(event:any)
{
    const data = await this.chartPlugin.set(this.app.code, this.data, this.form, event);

    this.onClose();        
}

ionViewWillLeave()
{
    if(this.dynamicForm)
    {
        this.dynamicForm.destroy();   
    }    
}

onClose()
{
    this.back();
}
}
