import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }    from "../../../core";
import { FormChartPage } from "./form.chart.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormChartPage } ])
    ],
    declarations : [
        FormChartPage,
    ]
})
export class FormChartModule 
{}
