import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }            from "../../../core";
import { GridEmailTemplate }     from "../../component/grid.email.template/grid.email.template";
import { GridEmailTemplatePage } from "./grid.email.template.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridEmailTemplatePage } ])
    ],
    declarations : [
        GridEmailTemplatePage,
        GridEmailTemplate,
    ],
})
export class GridEmailTemplateModule 
{}
