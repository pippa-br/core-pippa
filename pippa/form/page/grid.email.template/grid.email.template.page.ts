import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }            from "../../../core/base/page/base.page";
import { GridEmailTemplate }   from "../../component/grid.email.template/grid.email.template";
import { Grid }                from "../../../core/model/grid/grid";
import { FieldType }           from "../../../core/type/field/field.type";
import { EmailTemplatePlugin } from "../../../core/plugin/email.template/email.template.plugin";

@Component({
    selector        : "grid-email-template-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{form?.name}} - E-mail Transacional
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="goAddForm()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>

                            <div grid-email-template
                                 #gridEmailTemplate
                                 [app]="app"
                                 [acl]="acl"
                                 [grid]="grid"
                                 [collection]="collection">
                            </div>

                        </ion-content>`,
})
export class GridEmailTemplatePage extends BasePage
{
    @ViewChild("gridEmailTemplate", { static : true }) gridEmailTemplate : GridEmailTemplate;
    
    constructor(
        public emailTemplatePlugin : EmailTemplatePlugin,
        public changeDetectorRef   : ChangeDetectorRef,
    )
    {
        super();
    }

    ionViewWillLeave()
    {
        this.gridEmailTemplate.destroy();
    }

    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            groupBy         : {
                type  : FieldType.type("Select"),
                label : "Modo",
                value : "mode"
            },			
            items : [
                {
                    row : 1,
                    field : {
                        label : "Nome",
                        name  : "name",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 2,
                    field : {
                        label : "Tipo",
                        name  : "type.label",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 3,
                    field : {
                        label : "Assunto",
                        name  : "subject",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 4,
                    field : {
                        label : "Grupos",
                        name  : "groups",
                        type  : FieldType.type("MultiSelect"),
                    }
                }, 
                {
                    row : 5,
                    field : {
                        label : "Limite",
                        name  : "limit",
                        type  : FieldType.type("Number"),
                    }
                },    
                {
                    row : 6,
                    field : {
                        label : "Status",
                        name  : "status",
                        type  : FieldType.type("Toggle"),
                    }
                },            
            ],
            buttons : [
                /*{
                    icon : 'grid',
                    isEnabled : ((item) =>
                    {
                        return true;
                    }),
                    click : ((event) =>
                    {
                        this.setGridEvent.emit(event);
                    })
                },
                {
                    icon : 'mail',
                    isEnabled : ((item) =>
                    {
                        return true;
                    }),
                    click : ((event) =>
                    {
                        this.emailTemplateEvent.emit(event);
                    })
                }
                {
                    icon : 'add-circle-outline',
                    isEnabled : ((item) =>
                    {
                        return item.isContainer();
                    }),
                    click : ((item) =>
                    {
                        this.addEvent.emit({
                            parent : item
                        });
                    })
                }*/
            ]
        });

        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "mode.value",
        });

        params.where = [];

        params.where.push({
            field    : "form",
            operator : "==",
            value    : this.form.reference,
            type     : FieldType.type("ReferenceSelect")
        });

        this.emailTemplatePlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    goAddForm()
    {
        this.push("form-email-template",
            {
                formPath : this.form.reference.path
            });
    }

    onClose()
    {
        this.back();
    }
}
