import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }    from "../../../core";
import { GridChartPage } from "./grid.chart.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridChartPage } ])
    ],
    declarations : [
        GridChartPage,
    ],
})
export class GridChartModule 
{}
