import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";
import { AlertController } from "@ionic/angular";

/* PIPPA */
import { BasePage }    from "../../../core/base/page/base.page";
import { GridAcl }     from "../../component/grid.acl/grid.acl";
import { ChartPlugin } from "../../../core/plugin/chart/chart.plugin";
import { FieldType }   from "../../../core/type/field/field.type";
import { Grid }        from "../../../core/model/grid/grid";
import { Acl }         from "../../../core/model/acl/acl";

@Component({
    selector        : "grid-chart-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} - Gráficos
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="onAdd()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
							<div dynamic-list
								 #gridChart
								 [grid]="grid"
								 [acl]="acl"
								 [collection]="collection"
								 (add)="onAdd()"
								 (set)="onSet($event)"
								 (del)="onDel($event)"
								 (viewer)="onViewer($event)">
							</div>
                        </ion-content>`,
})
export class GridChartPage extends BasePage
{
    @ViewChild("gridChart", { static : true }) gridChart : GridAcl;    

    constructor(
        public chartPlugin       : ChartPlugin,
		public changeDetectorRef : ChangeDetectorRef,
		public alertController   : AlertController,
    )
    {
        super();
    }
    
    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    row : 1,
                    field : {
                        label : "Nome",
                        name  : "name",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 2,
                    field : {
                        label : "Path",
                        name  : "reference.path",
                        type  : FieldType.type("Text"),
                    }
                },
            ],
        });

        const params = this.createParams({
            appid   : this.form.appid,
            orderBy : "name",
        });

        this.chartPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    ionViewWillLeave()
    {
        this.gridChart.destroy();
    }

    onAdd()
    {
        this.push("form-chart",
            {
                formPath : this.form.reference.path 
            });
    }
	
    onSet(event:any)
    {
        this.push("form-chart",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }	

    async doDel(event:any)
    {	
        await this.chartPlugin.del(event.data);
        this.collection.reload();
    }
	
    onViewer(event:any)
    {
        this.push("viewer-chart",
            {
                appid     : this.app.code,
                chartPath : event.data.reference.path,
            });
    }

    onClose()
    {
        this.back();
    }
}
