import { Component, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }     from "../../../core/base/page/base.page";
import { GridFilter }   from "../../component/grid.filter/grid.filter";
import { FilterPlugin } from "../../../core/plugin/filter/filter.plugin";
import { FieldType }    from "../../../core/type/field/field.type";
import { Grid }         from "../../../core/model/grid/grid";
import { Acl }          from "../../../core/model/acl/acl";

@Component({
    selector        : "grid-filter-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} - Filtros
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="addAcl()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div grid-filter
                                 #gridFilter
                                 [app]="app"
                                 [data]="data"
                                 [acl]="acl"
                                 [grid]="grid"
                                 [collection]="collection">
                            </div>
                        </ion-content>`,
})
export class GridFilterPage extends BasePage
{
    @ViewChild("gridFilter", { static : true }) gridFilter : GridFilter;    

    constructor(
        public filterPlugin      : FilterPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }
    
    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    row : 1,
                    field : {
                        label : "Nome",
                        name  : "name",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row : 2,
                    field : {
                        label : "Grupos",
                        name  : "groups",
                        type  : FieldType.type("MultiSelect"),
                    }
                },
            ],
        });

        const params = this.createParams({
            appid   : this.form.appid,
            orderBy : "name",
        });

        params.where = [];

        params.where.push({
            field    : "form",
            operator : "==",
            value    : this.form.reference,
            type     : FieldType.type("ReferenceSelect")
        });

        this.filterPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    ionViewWillLeave()
    {
        this.gridFilter.destroy();
    }

    addAcl()
    {
        this.push("form-filter",
            {
                formPath : this.form.reference.path
            });
    }

    onClose()
    {
        this.back();
    }
}
