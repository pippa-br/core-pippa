import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }     from "../../../core";
import { GridFilter }     from "../../component/grid.filter/grid.filter";
import { GridFilterPage } from "./grid.filter.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridFilterPage } ])
    ],
    declarations : [
        GridFilterPage,
        GridFilter,
    ],
})
export class GridFilterModule
{
}
