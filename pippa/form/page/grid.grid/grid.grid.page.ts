import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }   from "../../../core/base/page/base.page";
import { GridGrid }   from "../../component/grid.grid/grid.grid";
import { FieldType }  from "../../../core/type/field/field.type";
import { Grid }       from "../../../core/model/grid/grid";
import { GridPlugin } from "../../../core/plugin/grid/grid.plugin";

@Component({
    selector        : "grid-grid-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header class="header-form">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon name="albums-outline"></ion-icon>
                                    {{app?.name}} - Grids
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only (click)="addGrid()">
                                        <ion-icon name="add-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content>
                            <div grid-grid
                                #gridGrid
                                [app]="app"
                                [acl]="acl"
                                [grid]="grid"
                                [collection]="collection">
                            </div>
                        </ion-content>`,
})
export class GridGridPage extends BasePage
{
    @ViewChild("gridGrid", { static : true }) gridGrid : GridGrid;

    constructor(
        public gridPlugin        : GridPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    initialize()
    {
        this.grid = new Grid({
            hasSetButton    : true,
            hasDelButton    : true,
            hasViewerButton : true,
            items           : [
                {
                    row   : 1,
                    field : {
                        label : "Nome",
                        name  : "name",
                        type  : FieldType.type("Text"),
                    }
                },               
                {
                    row   : 2,
                    field : {
                        label : "Path",
                        name  : "reference.path",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    row   : 3,
                    field : {
                        label : "Grupos",
                        name  : "groups",
                        type  : FieldType.type("MultiSelect"),
                    }
                },
            ],
        });

        const params = this.createParams({
            appid  	: this.app.code,
            orderBy : "name",
        });

        params.where = [];

        params.where.push({
            field    : "form",
            operator : "==",
            value    : this.form.reference,
            type     : FieldType.type("ReferenceSelect")
        });

        this.gridPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;
            this.changeDetectorRef.markForCheck();
        });
    }

    addGrid()
    {
        this.push("form-grid",
            {
                formPath : this.form.reference.path
            });
    }

    ionViewWillLeave()
    {
        this.gridGrid.destroy();
    }

    onClose()
    {
        this.back();
    }
}
