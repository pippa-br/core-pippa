import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }   from "../../../core";
import { GridGrid }     from "../../component/grid.grid/grid.grid";
import { GridGridPage } from "./grid.grid.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridGridPage } ])
    ],
    declarations : [
        GridGridPage,
        GridGrid,
    ],
})
export class GridGridModule
{
}
