import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }   from "../../../core/base/page/base.page";
import { FormPlugin } from "../../../core/plugin/form/form.plugin";

@Component({
    selector        : "indexer-form-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{app?.name}}
                                </ion-title>
                                <ion-buttons slot="end" (click)="onClose()">
                                <ion-button icon-only>
                                    <ion-icon name="close-outline"></ion-icon>
                                </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>

                        <ion-content>

							<div class="indexer-box" [ngClass]="{'finish' : finish}">
								<span>{{percentage}}%</span>
								<small>{{count}} / {{total}}</small>
							</div>

                        </ion-content>`,
})
export class IndexerFormPage extends BasePage
{
    public total        = 0;
    public count        = 0;
    public percentage : any     = 0;
    public finish 	   = false;
    public search     : any;

    constructor(
		public formPlugin        : FormPlugin,
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }
	
    async initialize()
    {
        this.total      = 0;
        this.count      = 0;
        this.percentage = 0;

        this.changeDetectorRef.markForCheck();		

        await this.getPage(this.formPlugin, 
            {
                perPage : 25,
                page    : 1,
            });		

        this.changeDetectorRef.markForCheck();
    }
	
    async getPage(plugin, result)
    {
        await this.core().loadingController.start();
		
        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "postdate",
            asc     : false,
            perPage : result.perPage,
            page    : result.page,
            data    : (result.data ? result.data : null),
            count   : result.count,
            total   : result.total,
            where   : [ {
                field    : "form",
                operator : "!=",
                value    : null
            } ]
        });
		
        const indexesResult = await plugin.indexes(params);

        await this.core().loadingController.stop();

        this.total      = indexesResult.total;
        this.count      = indexesResult.count;
        this.percentage = ((100 * this.count) / this.total).toFixed(0);

        this.changeDetectorRef.markForCheck();		

        if (indexesResult.data)
        {
            indexesResult.page++;
            this.getPage(plugin, indexesResult);
        }
    }

    onClose()
    {
        this.back();
    }
}
