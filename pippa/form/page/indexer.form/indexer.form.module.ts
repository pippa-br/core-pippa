import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }        from "../../../core";
import { IndexerFormPage } from "./indexer.form.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : IndexerFormPage } ])
    ],
    declarations : [
        IndexerFormPage,
    ],
})
export class IndexerFormModule 
{
}
