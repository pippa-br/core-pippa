import { Component, Output, EventEmitter, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }        from "../../../core/base/grid/base.grid";
import { FieldType }       from "../../../core/type/field/field.type";
import { SlotType }        from "../../../core/type/slot/slot.type";
import { FormCollection }  from "../../../core/model/form/form.collection";
import { DocumentPlugin }  from "../../../core/plugin/document/document.plugin";
import { FilterPlugin }    from "../../../core/plugin/filter/filter.plugin";
import { Grid }            from "../../../core/model/grid/grid";
import { Where }           from "../../../core/model/where/where";
import { WhereCollection } from "../../../core/model/where/where.collection";
import { IconType }        from "../../../core/type/icon/icon.type";
import { DynamicList }     from "../../../core/dynamic/list/dynamic.list";

@Component({
    selector        : "[grid-form]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                     #dynamicList
                     [acl]="acl"
                     [grid]="grid"
                     [collection]="collection"
                     (add)="onAdd($event)"
                     (set)="onSet($event)"
                     (del)="onDel($event)"
                     (viewer)="onViewer($event)">
                </div>`,
})
export class GridForm extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    @Output("setViewer")     setViewerEvent     : EventEmitter<any> = new EventEmitter();
	@Output("setFilter")     setFilterEvent     : EventEmitter<any> = new EventEmitter();
	@Output("setChart")      setChartEvent      : EventEmitter<any> = new EventEmitter();
    @Output("setGrid")       setGridEvent       : EventEmitter<any> = new EventEmitter();
	@Output("emailTemplate") emailTemplateEvent : EventEmitter<any> = new EventEmitter();
	@Output("notification")  notificationEvent  : EventEmitter<any> = new EventEmitter();
	@Output("api")  		 apiEvent  			: EventEmitter<any> = new EventEmitter();
    @Output("copy")          copyEvent          : EventEmitter<any> = new EventEmitter();
    @Output("setIndexer")    setIndexerEvent    : EventEmitter<any> = new EventEmitter();

    constructor(
        public documentPlugin : DocumentPlugin,
        public filterPlugin   : FilterPlugin,
    )
    {
        super();
    }

    destroy()
    {
        this.dynamicList.destroy();
    }

    async doDel(item:any)
    {		
        await this.documentPlugin.del(item);

        setTimeout(() => 
        {
            this.collection.reload();
        });
    }

    loadAcl()
    {
        this.grid = new Grid({
            type         : Grid.CARD_TYPE,
            hasSetButton : this.hasAcl("set_form_form"),
            hasDelButton : this.hasAcl("del_form_form"),
            items        : [
                {
                    hasLabel : false,
                    _slot    : SlotType.TIILE,
                    field    : {
                        label : "Nome",
                        name  : "name",                        
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    hasLabel : false,
                    _slot    : SlotType.SUBTITLE,
                    field    : {
                        label : "Data",
                        name  : "postdate",
                        type  : FieldType.type("Date"),
                    }
                },
                {
                    _slot : SlotType.AFTER_CONTENT,
                    field :
                    {
                        label : "Icon",
                        name  : "icon",
                        type  : FieldType.type("SelectIcon"),
                    },
                    onClick : (event:any) =>
                    {
                        const form = event.data;

                        /* SE TEM A OPÇÃO DE ADICIONAR, VER A LISTA */
                        if (this.hasAcl("add_document"))
                        {
 							this.push("grid-document",
                                {
                                    appid  	 : this.app.code,
                                    formPath : form.reference.path
                                });
                        }
                        else
                        {
                            const params = this.createParams({
                                app : this.app,
                            });

                            params.where = [];

                            params.where.push({
                                field    : "owner",
                                operator : "==",
                                value    : this.core().user.reference
                            });

                            /* GET DOCUMENTS */
                            this.documentPlugin.getData(params).then(result =>
                            {
                                result.collection.on("form").then(() =>
                                {
                                    let hasDocument = false;

                                    for (const key in result.collection)
                                    {
                                        const document = result.collection[key];

                                        if (form.id == document.form.id)
                                        {
                                            hasDocument = true;
                                        }
                                    }

                                    if (hasDocument)
                                    {
                                        this.alertController.create(
                                            {
                                                message : "Cadastro já realizado!"
                                            })
                                        .then(alert =>
                                        {
                                            alert.present();
                                        })
                                    }
                                    else
                                    {
                                        this.push("form-document",
                                            {
                                                appid    : this.app.code,
                                                formPath : event.data.reference.path
                                            });
                                    }
                                });
                            });
                        }
                    }
                },                
            ],
            buttons : [
                {
                    icon      : IconType.GRID,
                    label     : "Grid",
                    isEnabled : (() =>
                    {
                        return this.hasAcl("set_grid_grid");
                    }),
                    onClick : ((event:any) =>
                    {
                        this.setGridEvent.emit(event);
                    })
                },
                {
                    icon      : IconType.EYE,
                    label     : "Visualização",
                    isEnabled : (() =>
                    {
                        return this.hasAcl("set_form_viewer");
                    }),
                    onClick : ((event:any) =>
                    {
                        this.setViewerEvent.emit(event);
                    })
                },
                {
                    icon      : IconType.FUNNEL,
                    label     : "Filtro",
                    isEnabled : (() =>
                    {
                        return this.hasAcl("set_form_filter");
                    }),
                    onClick : ((event:any) =>
                    {
                        this.setFilterEvent.emit(event);
                    })
                },
                {
                    icon      : IconType.BAR_CHART,
                    label     : "Gráfico",
                    isEnabled : (() =>
                    {
                        return this.hasAcl("set_form_chart");
                    }),
                    onClick : ((event:any) =>
                    {
                        this.setChartEvent.emit(event);
                    })
                },
                {
                    icon      : IconType.MAIL,
                    label     : "Notificações",
                    isEnabled : (() =>
                    {
                        return this.hasAcl("set_form_email");
                    }),
                    onClick : ((event:any) =>
                    {
                        this.emailTemplateEvent.emit(event);
                    })
                },
                /*{
                    icon  : IconType.SEND,
                    label : 'Notificações',
                    isEnabled : (() =>
                    {
                        return this.hasAcl('set_form_notification');
                    }),
                    onClick : ((event:any) =>
                    {
                        this.notificationEvent.emit(event);
                    })
				},
				/*{
                    icon  : IconType.CODE_WORKING,
                    label : 'API',
                    isEnabled : (() =>
                    {
                        return this.hasAcl('set_form_api');
                    }),
                    onClick : ((event:any) =>
                    {
                        this.apiEvent.emit(event);
                    })
                },*/
                {
                    icon    : IconType.ADD,
                    onClick : ((event:any) =>
                    {
                        this.addEvent.emit(event);
                    })
                }
                /*{
                    icon : 'copy',
                    label : 'Copiar',
                    isEnabled : (() =>
                    {
                        return this.hasAcl('copy_document_form');
                    }),
                    click : ((event:any) =>
                    {
                        this.copyEvent.emit(event);
                    })
                }
                */
            ]
        });
    }
}
