import { Component, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }    				  from "../../../core/base/grid/base.grid";
import { DynamicList } 				  from "../../../core/dynamic/list/dynamic.list";
import { NotificationTemplatePlugin } from "../../../core/plugin/notification.template/notification.template.plugin";

@Component({
    selector        : "[grid-notification-template]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridNotificationTemplate extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    constructor(
        public notificationTemplatePlugin : NotificationTemplatePlugin,
    )
    {
        super();
    }

    destroy()
    {
        this.dynamicList.destroy();
    }

   	async doDel(item:any)
    {		
        await this.notificationTemplatePlugin.del(item);
        this.collection.reload();
    }

    onSet(event:any)
    {
        this.push("form-notification-template",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }
}
