import { Component, Input, ChangeDetectionStrategy, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseGrid }    from "../../../core/base/grid/base.grid";
import { DynamicList } from "../../../core/dynamic/list/dynamic.list";
import { GridPlugin }  from "../../../core/plugin/grid/grid.plugin";

@Component({
    selector        : "[grid-grid]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridGrid extends BaseGrid
{
	@ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

	constructor(
        public gridPlugin : GridPlugin,
	)
	{
	    super();
	}

	destroy()
	{
	    this.dynamicList.destroy();
	}

	async doDel(item:any)
	{		
	    await this.gridPlugin.del(item);
	    this.collection.reload();
	}

	onSet(event:any)
	{
	    this.push("form-grid",
	        {
	            appid    : this.app.code,
	            dataPath : event.data.reference.path,
	        });
	}
}
