import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-notification-template]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (close)="onCancel()"
                             (invalid)="onInvalid($event)"
                             class="grid-form">
                        </form>`,
})
export class FormNotificationTemplate extends BaseForm
{    
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }     
}
