import { Component, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }    		   from "../../../core/base/grid/base.grid";
import { DynamicList } 		   from "../../../core/dynamic/list/dynamic.list";
import { EmailTemplatePlugin } from "../../../core/plugin/email.template/email.template.plugin";

@Component({
    selector        : "[grid-email-template]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridEmailTemplate extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    constructor(
        public emailTemplatePlugin : EmailTemplatePlugin,
    )
    {
        super();
    }	

    destroy()
    {
        this.dynamicList.destroy();
    }

    async doDel(item:any)
    {		
        await this.emailTemplatePlugin.del(item);
        this.collection.reload();
    }

    onSet(event:any)
    {
        this.push("form-email-template",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }
}
