import { Component, Input, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }    from "../../../core/base/grid/base.grid";
import { DynamicList } from "../../../core/dynamic/list/dynamic.list";

@Component({
    selector        : "[grid-viewer]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridViewer extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    destroy()
    {
        this.dynamicList.destroy();
    }

    doDel(item:any)
    {
        item.del();
    }

    onSet(event:any)
    {
        this.push("form-viewer",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }
}
