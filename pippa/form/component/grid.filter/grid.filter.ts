import { Component, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }     from "../../../core/base/grid/base.grid";
import { DynamicList }  from "../../../core/dynamic/list/dynamic.list";
import { FilterPlugin } from "../../../core/plugin/filter/filter.plugin";

@Component({
    selector        : "[grid-filter]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridFilter extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    constructor(
        public filterPlugin : FilterPlugin,
    )
    {
        super();
    }

    destroy()
    {
        this.dynamicList.destroy();
    }

    async doDel(item:any)
    {
        await this.filterPlugin.del(item);
        this.collection.reload();
    }

    onSet(event:any)
    {
        this.push("form-filter",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }
}
