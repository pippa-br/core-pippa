import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-form]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             [acl]="acl"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (close)="onCancel()"
                             class="field-form">
                        </form>`,
})
export class FormForm extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    } 
}
