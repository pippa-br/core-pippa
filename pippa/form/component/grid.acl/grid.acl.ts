import { Component, ViewChild, ChangeDetectionStrategy } from "@angular/core";

/* PIPPA */
import { BaseGrid }    from "../../../core/base/grid/base.grid";
import { DynamicList } from "../../../core/dynamic/list/dynamic.list";
import { AclPlugin }   from "../../../core/plugin/acl/acl.plugin";

@Component({
    selector        : "[grid-acl]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div dynamic-list
                            #dynamicList
                            [grid]="grid"
                            [acl]="acl"
                            [collection]="collection"
                            (add)="onAdd($event)"
                            (set)="onSet($event)"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                        </div>`,
})
export class GridAcl extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;

    constructor(
        public aclPlugin : AclPlugin,
    )
    {
        super();
    }

    destroy()
    {
        this.dynamicList.destroy();
    }

    async doDel(item:any)
    {		
        await this.aclPlugin.del(item);
        this.collection.reload();
    }

    onSet(event:any)
    {
        this.push("form-acl",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }
}
