import { Component, Input, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef } from "@angular/core";

import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-viewer]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (close)="onCancel()"
                             (invalid)="onInvalid($event)"
                             class="viewer-form">
                        </form>`,
})
export class FormViewer extends BaseForm
{
    @Input() addText  = "Salvar";
    @Input() context  = "GRID";    

    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }    
}
