import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from "@angular/core";

import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-grid]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form dynamic-form
                             #dynamicForm
                             [form]="form"
                             [data]="data"
                             [acl]="acl"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (close)="onCancel()"
                             (invalid)="onInvalid($event)"
                             class="grid-form">
                        </form>`,
})
export class FormGrid extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    } 
}
