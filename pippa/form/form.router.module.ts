export const FormRoutes = [
    {
        path         : "grid-form",
        loadChildren : "./page/grid.form/grid.form.module#GridFormModule"
    },
    {
        path         : "form-grid",
        loadChildren : "./page/form.grid/form.grid.module#FormGridModule"
    },
    {
        path         : "form-master",
        loadChildren : "./page/form.master/form.master.module#FormMasterModule"
    },
    {
        path         : "form-form",
        loadChildren : "./page/form.form/form.form.module#FormFormModule"
    },
    {
        path         : "form-viewer",
        loadChildren : "./page/form.viewer/form.viewer.module#FormViewerModule"
    },
    {
        path         : "form-filter",
        loadChildren : "./page/form.filter/form.filter.module#FormFilterModule"
    },
    {
        path         : "grid-email-template",
        loadChildren : "./page/grid.email.template/grid.email.template.module#GridEmailTemplateModule"
    },
    {
        path         : "form-email-template",
        loadChildren : "./page/form.email.template/form.email.template.module#FormEmailTemplateModule"
    },
    {
        path         : "indexer-form",
        loadChildren : "./page/indexer.form/indexer.form.module#IndexerFormModule"
    },
    {
        path         : "form-indexer",
        loadChildren : "./page/form.indexer/form.indexer.module#FormIndexerModule"
    },
    {
        path         : "grid-acl",
        loadChildren : "./page/grid.acl/grid.acl.module#GridAclModule"
    },
    {
        path         : "form-acl",
        loadChildren : "./page/form.acl/form.acl.module#FormAclModule"
    },
];
