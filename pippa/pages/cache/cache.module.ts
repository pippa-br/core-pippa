import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CachePage } from "./cache.page";

/* PIPPA */
import { CoreModule } from "../../../../core-pippa/pippa";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : CachePage } ])
    ],
    declarations : [
        CachePage
    ]
})
export class CacheModule 
{
}
