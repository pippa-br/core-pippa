import { Component, ViewChild } from "@angular/core";

/* PIPPA */
import { ExportPlugin, ExportType, FieldType, Form, BasePage, BaseModel, AccountPlugin } from "../../../../core-pippa/pippa";

@Component({
    selector    : "cache-page",
    templateUrl : "cache.page.html",
    styleUrls   : [ "./cache.page.scss" ],
})
export class CachePage extends BasePage
{
    constructor(
    )
    {
        super();
    }

    initialize()
    {
        //
    }

    async onClearCache()
    {
        const data = { 
            "accid" : "default",
        }

        const result = await this.core().api.createTask("cron", "utilApi/purgeCache", data)
        
        this.showAlert("Task", "Limpeza de Cache Concluída!"); 
    }    
}
