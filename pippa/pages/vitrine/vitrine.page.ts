import { ChangeDetectorRef, ViewChild, Component } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { IonInfiniteScroll } from "@ionic/angular";
import { Location } from "@angular/common"; 
import { BasePage, DocumentCollection, DocumentPlugin, FieldType, FunctionsType, Types, WhereCollection } from "src/core-pippa/pippa/core";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { TDate } from "../../core/util/tdate/tdate";
@Component({
    selector    : "vitrine-page",
    templateUrl : "vitrine.page.html",
    styleUrls   : [ "./vitrine.page.scss" ],
})
export class VitrinePage extends BasePage
{
    public showSearch     	   = false;
    public isLoad     	  	   = false;
    public filters         	  : any;
    public dataFilters     	  : any;
    public where          	  : any;
    public _disabledFilter	   = false;
    public buttons        	  : Array<any>;
    public _listSubscribe  	  : any;
    public term 			  : string;
    public location		  	  : Location;
    public router		 	  : Router;
    public selecteds		  : any; 
    public addLabel            = "";
    public orderBy             = "order";
    public suffixBy            = "";
    public total               = 0;
    public items              = [];
		
@ViewChild(IonInfiniteScroll) infiniteScroll : IonInfiniteScroll;

constructor(
public documentPlugin    : DocumentPlugin,
public changeDetectorRef : ChangeDetectorRef
)
{
    super(); 

    //this.modalController   = this.core().injector.get(ModalController);	
    this.location	= this.core().injector.get(Location);
    this.router   = this.core().injector.get(Router);    
}  

//https://stackblitz.com/edit/angular-drag-drop-flex-wrap?file=src%2Fapp%2Fapp.component.html,src%2Fapp%2Fapp.component.ts
//https://stackblitz.com/edit/angular-cdk-drag-drop-sortable-flex-wrap?file=src%2Fapp%2Fapp.component.ts
async drop(event: CdkDragDrop<any>) 
{
    const fromModel = this.items[event.previousContainer.data.index];
    const fromIndex = event.previousContainer.data.index;
    let toIndex     = event.container.data.index;

    if (fromIndex > toIndex)
    {
        toIndex = toIndex + event.currentIndex;
    }
    else
    {
        toIndex = toIndex + (event.currentIndex == 0 ? -1 : 0);
    }		

    moveItemInArray(this.items, fromIndex, toIndex);

    let order = 0;

    if (toIndex == 0)
    {
        const model = this.items[toIndex + 1];
        order       = model[this.orderBy + this.suffixBy] + 30;

        console.log(model.name, model[this.orderBy + this.suffixBy]);
    }
    else if (toIndex + 1 == this.items.length)
    {
        const model = this.items[toIndex - 1];
        order       = model[this.orderBy + this.suffixBy] - 30;

        console.log(model.name, model[this.orderBy + this.suffixBy]);
    }
    else
    {
        const model1 = this.items[toIndex - 1];
        const model2 = this.items[toIndex + 1];
        order        = (model1[this.orderBy + this.suffixBy] + model2[this.orderBy + this.suffixBy]) / 2;

        console.log(model1.name, model1[this.orderBy + this.suffixBy]);
        console.log(model2.name, model2[this.orderBy + this.suffixBy]);
    }

    // ATUALIZA O OBJETO ORIGINAL
    fromModel[this.orderBy + this.suffixBy] = order;

    console.log(fromModel.name, order);

    const data : any   = {};
    data[this.orderBy + this.suffixBy] = order;

    await this.documentPlugin.set(fromModel.appid, fromModel, fromModel.form, 
        {
            data : data
        }, false);

    this.markForCheck();
}

async onSave()
{
    const fromModel = this.items[0];
    let order       = parseInt(fromModel[this.orderBy + this.suffixBy]);

    const promise = [];

    for(const item of this.items)
    {        
        const data : any   = {};
        data[this.orderBy + this.suffixBy] = order;

        promise.push(this.documentPlugin.set(item.appid, item, item.form, 
        {
            data : data
        }));

        order -= 1;
    }

    await Promise.all(promise);

    this.markForCheck();
}


markForCheck()
{
    this.changeDetectorRef.markForCheck();
}

async onShowSearch()
{
    this.showSearch = !this.showSearch;

    if (!this.showSearch)
    {
        this.clearSearch();	
        this.term = "";

        // RE COUNT
        const where = this.where.copy();

        const params = this.createParams({
            appid : this.app.code,
            colid : this.app._colid,
            where : where,
        });			

        await this.getTotal(params);
    }		

    this.markForCheck();
}

async initialize()
{
    if(this.queryParams.suffixBy)
    {
        this.suffixBy = this.queryParams.suffixBy;
    }

    this.where = new WhereCollection([ {
        name     : "Publicado",
        field  	 : "published",
        operator : "==",
        value    : true,  
    }]);

    if(this.queryParams.platform)
    {
        this.where.set({
            name     : this.queryParams.platform,
            field  	 :"indexes.platforms." + this.queryParams.platform,
            operator : "==",
            value    : true,  
        });
    }

    this.selecteds	= new DocumentCollection();

    this.buttons = [{
        name  : "Salvar",
        icon  : { value : 'save' },
        click : (event) => 
        {
            this.onSave();
        }
    }];

    await this.load();
    this.markForCheck();		
}

async onReload()
{
    await this.collection.clearCache();
    this.load();					
}

async onSort(event:any)
{
    await this.collection.clearCache();
    this.load();					
}

async disabledFilter()
{        
    for (const key in this.where)
    {
        this.where[key].enabled = false;
    }

    this._disabledFilter = true;

    await this.load();
}

clearFilter()
{
    this._disabledFilter = false;    
}

normalizeCollection(collection:any)
{
    this.items = []

    if (collection)
    {
        for (const item of collection)
        {
            if (item.indexes?.thumb)
            {
                this.items.push(item);
            }
        }
    }
}

async load()
{
    this.isLoad      = false;
    const collection = await this.getList(this.where);

    if (collection)
    {
        this.collection            = collection;
        this.collection.isInfinite = true;

        this.normalizeCollection(collection);

        if (this.total)
        {
            this.collection.total = this.total;
        }				

        if (this._listSubscribe)
        {
            this._listSubscribe.unsubscribe();
        }            

        this._listSubscribe = new Subscription();

        this._listSubscribe.add(collection.onUpdate().subscribe(() =>
        {
            console.log("collection change display");

            this.loadCollection();
        }));					

        this.loadCollection();   		
    }

    this.isLoad = true;
    this.markForCheck();
}

loadCollection()
{
}

ionViewDidLeave()
{
// SEARCH
    this.showSearch = false;
    //this.clearSearch();	
    this.term = "";

    super.ionViewDidLeave();
}

ngOnDestroy()
{
    console.log("ngOnDestroy");

    if (this._listSubscribe)
    {
        this._listSubscribe.unsubscribe();
    }

    this.clearFilter();

    super.ngOnDestroy();
}

get user()
{
    return this.core().user;
}    

async getList(where:any)
{
    if (this.app)
    {
        const params = this.createParams({
            appid   : this.app.code,
            colid  	: this.app._colid,
            where   : where,
            perPage : 100,				
            orderBy : this.orderBy + this.suffixBy,
            asc     : false,
            noCache : true,
        });

        const result = await this.plugin.getData(params);
        this.getTotal(params);

        return result.collection;
    }
    else
    {
        return;
    }
}

filter()
{
    const where  = new WhereCollection([]);
    this.orderBy = "order";
    let hasSale  = false;

    for (const item of this.where)
    {
        if (item.field == "sale")
        {
            if (item.value)
            {
                hasSale      = true;
                this.orderBy = "saleOrder";				

                where.add({
                    id       : item.id,
                    name     : "Sale",
                    field  	 : "indexes.hasSale",
                    operator : "==",
                    value    : true,  
                    type     : FieldType.type("Toggle")
                })	
            }				
        }
        else if (item.field == "categories") 
        {
            if (hasSale)
            {
                this.orderBy = "saleCategoryOrder";
            }
            else
            {
                this.orderBy = "categoryOrder";
            }		

            where.add(item);
        }
        else
        {
            where.add(item);
        }
    }

    console.error("orderBy", this.orderBy + this.suffixBy)

    this.where = where;

    this.load();
}

async getTotal(params)
{
    const result = await this.plugin.count(params);

    if (this.collection)
    {
        this.collection.total = result.total;
    }

    this.total = result.total;
    this.markForCheck();
}

onClose()
{
    this.back();
}

onNextPage()
{
    if (this.collection.hasNextPage())
    {			
        this.collection.nextPageReference().then(() =>
        {
            this.infiniteScroll.complete();
            this.normalizeCollection(this.collection);

            console.log("onNextPage");
        });
    }
    else
    {
        this.infiniteScroll.disabled = true;
    }
} 

async onSearch(event:any)
{		
    if (event.target.value)
    {			
        this.collection.search(event.target.value);

        if (event.target.value != undefined)
        {
            let term = event.target.value.toLowerCase();
            term     = this.core().util.removeAccents(term);

            const where = this.where.copy();
            where.push({
                field    : "search", 
                operator : "array-contains", 
                value    : event.target.value.toLowerCase(),
            });

            const params = this.createParams({
                appid : this.app.code,
                colid : this.app._colid,
                where : where,
            });			

            await this.getTotal(params);
        }			

        // if(this._searchTime)
        // {
        // 	clearTimeout(this._searchTime);
        // }

        // this._searchTime = setTimeout(() => 
        // {
        // 	this.collection.search(event.target.value);
        // 	this.dynamicList.display();
        // }, 500);
    }
}

// LIMPAR CACHE IMEDIATO
async delCache()
{		
    if (this.collection.keyCache)
    {
        await this.plugin.delCache(this.app.code, this.app._colid, this.collection.keyCache);	
    }
}    

getUrl(file:any)
{        
    if (file.indexes && file.indexes.thumb)
    {
        let url = file.indexes.thumb._480x480 || file.indexes.thumb._300x300 || file.indexes.thumb._url;
        url     = url.replace(".webp", ".jpeg");

        return url;    
    }
}
}
