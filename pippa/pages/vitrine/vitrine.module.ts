import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { VitrinePage } from "./vitrine.page";

/* PIPPA */
import { CoreModule } from "../../../../core-pippa/pippa";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : VitrinePage } ])
    ],
    declarations : [
        VitrinePage
    ]
})
export class VitrineModule 
{
}
