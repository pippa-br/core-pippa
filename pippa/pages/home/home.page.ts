import { ChangeDetectorRef, Component } from "@angular/core";

/* PIPPA */
import { DocumentPlugin, Params, BasePage, FieldType, WhereCollection, FilterPlugin, Acl, Types } from "../../core";
import { environment } from "src/environments/environment";
import { ChartPlugin } from "../../core/plugin/chart/chart.plugin";

@Component({
    selector    : "home-page",
    templateUrl : "home.page.html",
    styleUrls   : [ "./home.page.scss" ],
})
export class HomePage extends BasePage
{ 
    public filter     : any;
    public chart1	    : any;
    public chart2 	  : any;
    public chart3 	  : any;
    public chartData1 : any;
    public chartData2 : any;
    public chartData3 : any;

    constructor(
				public changeDetectorRef : ChangeDetectorRef,
        public documentPlugin : DocumentPlugin,
				public chartPlugin		: ChartPlugin,
				public filterPlugin   : FilterPlugin,
    )
    {
        super();
    }

    // google: latitude longitude NEPAL
    async initialize()
    {				
        this.getFilter();		
        this.loadChart1();	
        this.loadChart2();					
        this.loadChart3();		
    }

    async getFilter()
    {
        //FILTRO
        const params = new Params({
            appid : "financial",
            path  : environment.filterPath,
        });

        const result = await this.filterPlugin.getObject(params);

        // FILTER
        this.filter = result.data;
    }

    clear()
    {
        super.clear();
			
        this.chart1     = null;
        this.chart2     = null;
        this.chart3     = null;
        this.chartData1 = null;
        this.chartData2 = null;
        this.chartData3 = null;
    }

    async loadChart1()
    {
        if (environment.chartHome1)
        {
            const params2 = this.createParams({
                accid : "default",
                appid : "financial",
                colid : "chart",
                path  : environment.chartHome1,
            });

            const resultData = await this.chartPlugin.getObject(params2);

            this.chart1 = resultData.data;
								
            const params = this.createParams({
                accid : "default",
                appid : "financial",
                colid : "documents",
                chart : resultData.data,
            });

            const result    = await this.chartPlugin.getDataChart(params);
            this.chartData1 = result.data
        }			
    }

    async loadChart2()
    {
        if (environment.chartHome2)
        {
            const params2 = this.createParams({
                accid : "default",
                appid : "financial",
                colid : "chart",
                path  : environment.chartHome2,
            });

            const resultData = await this.chartPlugin.getObject(params2);

            this.chart2 = resultData.data;
								
            const params = this.createParams({
                accid : "default",
                appid : "financial",
                colid : "documents",
                chart : resultData.data,
            });

            const result    = await this.chartPlugin.getDataChart(params);
            this.chartData2 = result.data
        }			
    }	


    async loadChart3()
    {
        if (environment.chartHome3)
        {
            const params2 = this.createParams({
                accid : "default",
                appid : "payable",
                colid : "chart",
                path  : environment.chartHome3,
            });

            const resultData = await this.chartPlugin.getObject(params2);

            this.chart3 = resultData.data;
								
            const params = this.createParams({
                accid : "default",
                appid : "payable",
                colid : "documents",
                chart : resultData.data,
            });

            const result    = await this.chartPlugin.getDataChart(params);
            this.chartData3 = result.data
        }			
    }

    async onFilter(event)
    {
        const params = this.core().paramsFactory.create({
            accid : this.chart1.accid,
            appid : this.chart1.appid,
            colid : this.chart1.colid,
            chart : this.chart1,
            ...event
        });

        const result = await this.chartPlugin.getDataChart(params);

        this.chartData1 = result.data;	

        const params2 = this.core().paramsFactory.create({
            accid : this.chart2.accid,
            appid : this.chart2.appid,
            colid : this.chart2.colid,
            chart : this.chart2,
            ...event
        });

        const result2 = await this.chartPlugin.getDataChart(params2);

        this.chartData2 = result2.data;	
			
        this.changeDetectorRef.markForCheck();
    }
}
