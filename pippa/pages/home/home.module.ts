import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HomePage } from "./home.page";

// PIPPA
import { CoreModule } from "src/core-pippa/pippa";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : HomePage } ])
    ],
    declarations : [
        HomePage
    ]
})
export class HomePageModule 
{ }
