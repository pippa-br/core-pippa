import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { TimezonePage } from "./timezone.page";

/* PIPPA */
import { CoreModule } from "../../../../core-pippa/pippa";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : TimezonePage } ])
    ],
    declarations : [
        TimezonePage
    ]
})
export class TimezoneModule 
{
}
