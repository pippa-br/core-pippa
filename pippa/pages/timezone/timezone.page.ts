import { Component, ViewChild } from "@angular/core";

/* PIPPA */
import { ExportPlugin, ExportType, FieldType, Form, BasePage, BaseModel, AccountPlugin } from "../../../../core-pippa/pippa";

@Component({
    selector    : "timezone-page",
    templateUrl : "timezone.page.html",
    styleUrls   : [ "./timezone.page.scss" ],
})
export class TimezonePage extends BasePage
{
    public exportForm : any;

    constructor(
        public accountPlugin : AccountPlugin,
    )
    {
        super();
    }

    initialize()
    {
        const items = [];	

        items.push({
            row   : 3,
            col   : 1,
            field : {
                type        : FieldType.type("Select"),
                name        : "timezone",
                label       : "Timezone",
                placeholder : "Timezone",
                fixed     		: true,			
                setting     : {
                    optionID : "timezone"
                }
            }
        });

        this.data = new BaseModel({
            timezone : this.core().timezone
        });

        delete this.data.colid;

        this.exportForm = new Form({
            name      : "Timezone",            
            iconAdd   : "lock-open",
            items     : items,
            saveFixed : true,
            setting   : {
                setText : "Salvar",
            }
        });
    }

    async onSave(event)
    {
        let accid = await this.core().storagePlugin.get("accid");

        if (!accid)
        {
            accid = this.core().account.code;
        }

        /* LOAD ACCOUNT */ 
        const params = this.core().paramsFactory.create({
            accid : accid,
        });

        params.data = event.data;

        await this.accountPlugin.setTimezone(params);

        this.core().timezone = event.data.timezone;

        this.data = new BaseModel({
            timezone : this.core().timezone
        });
    }	
}
