import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { BlingPage } from "./bling.page";

/* PIPPA */
import { CoreModule } from "../../../../core-pippa/pippa";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : BlingPage } ])
    ],
    declarations : [
        BlingPage
    ]
})
export class BlingModule 
{
}
