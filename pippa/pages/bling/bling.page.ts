import { Component, ViewChild } from "@angular/core";

/* PIPPA */
import { ExportPlugin, ExportType, FieldType, Form, BasePage, BaseModel, AccountPlugin } from "../../../../core-pippa/pippa";

@Component({
    selector    : "bling-page",
    templateUrl : "bling.page.html",
    styleUrls   : [ "./bling.page.scss" ],
})
export class BlingPage extends BasePage
{
    public exportForm : any;
 
    constructor(
        public accountPlugin : AccountPlugin,
    )
    {
        super();
    }

    initialize()
    {        
    }

    async onSyncProducts()
    {
        const data = {
            "accid": "default",
            "appid": "product",
            "colid": "documents",
            "mapItems": {
                "referencePath": "/default/accounts/grids/c9t4wXAHeeMTmYmj0JBy"
            },
            "form": {
                "referencePath": "default/product/forms/SoLXfheZ4Tre4cNmkF9t"
            },
            "box" : {
                "referencePath" : "default/box/documents/seWIPL3kSn4EUjA17ScC"
            }
        }

        const result = await this.core().api.createTask('cron', 'blingApi/produtos/sync', data)

        console.log('task', result);
        
        this.showAlert('Task', 'Produtos enviados para sincronização');        
    }
    
    async onSyncStock()
    {
        const data = {
            "accid": "default",
            "appid": "stock",
            "colid": "documents",
            "mapItems": {
                "referencePath": "/default/accounts/grids/c9t4wXAHeeMTmYmj0JBy"
            },
            "form": {
                "referencePath": "default/stock/forms/MevJTYP9tkKgcnfecIsM"
            },
            "stockName": {
                "referencePath": "default/stockName/documents/H7ny9BhRSF6jj1KsfeDW"
            }
        }

        const result = await this.core().api.createTask('cron', 'blingApi/estoques/sync', data)

        console.log('task', result);
        
        this.showAlert('Task', 'Estoques enviados para sincronização');        
    }
}
