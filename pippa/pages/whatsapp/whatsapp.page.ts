import { Component } from "@angular/core";

/* PIPPA */
import { BasePage } from "../../core";

@Component({
    selector    : "whatsapp-page",
    templateUrl : "whatsapp.page.html",
    styleUrls   : [ "./whatsapp.page.scss" ],
})
export class WhatsappPage extends BasePage
{
    public intervalTime   : any;
    public intervalQRCode : any;
    public image          : any;
    public time             = 20;
    public status          = false;

    constructor(
    )
    {
        super();
    }

    async initialize()
    {
        await this.loadStaus();

        if (!this.status)
        {
            this.loadQrCdoe();

            this.intervalQRCode = setInterval(async() => 
            {
                this.loadStaus();
                this.loadQrCdoe();	
                this.time = 21;
            }, 1000 * 20);

            this.intervalTime = setInterval(async() => 
            {	
                if (this.time > 0)
                {
                    this.time -= 1;
                }		

            }, 1000);
        }
    }

    async loadStaus()
    {
        const data = {
            accid : "default",
            appid : "appd",
            colid : "documents",
        }

        const result = await this.core().api.callable("plugZapiApi/status", data);

        this.status = result.status;
    }

    async loadQrCdoe()
    {
        const data = {
            accid : "default",
            appid : "appd",
            colid : "documents",
        }

        const result = await this.core().api.callable("plugZapiApi/qr-code", data);

        this.image = result.data;
    }

    ngOnDestroy()
    {
        super.ngOnDestroy();

        if (this.intervalQRCode)
        {
            clearInterval(this.intervalQRCode);
        }		
    }
}
