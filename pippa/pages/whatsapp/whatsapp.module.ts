import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WhatsappPage } from './whatsapp.page';

/* PIPPA */
import { CoreModule } from '../../core';

@NgModule({
	imports: [
		CoreModule,
		RouterModule.forChild([{ path : '', component : WhatsappPage }])
	],
	declarations: [
		WhatsappPage
	]
})
export class WhatsappModule 
{
}
