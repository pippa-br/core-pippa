const path = require('path');
const fs   = require('fs');
const util = require('util');

// get application version from package.json
//const appVersion = require('../../package.json').version;
const appVersion = new Date().getFullYear() + '.' + new Date().getMonth() + '.' + new Date().getDate() + '.' + new Date().getHours() + '.' + new Date().getMinutes();

// promisify core API's
const readDir = util.promisify(fs.readdir);
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);

console.log('\nRunning post-build tasks');

// our version.json will be in the dist folder
const versionFilePath = path.join(__dirname + '/www/version.json');

let mainHash = '';
let mainBundleFile = '';

// RegExp to find main.bundle.js, even if it doesn't include a hash in it's name (dev build)
let mainBundleRegexp = /.js/;

// read the dist folder files and find the one we're looking for
readDir(path.join(__dirname, '/www/')).then(files => 
{	
	for(let key in files)
	{
		if(files[key].indexOf('.js') > -1)
		{
			const mainFilepath = path.join(__dirname, '/www/', files[key]);

			console.log(mainFilepath);
	
			readFile(mainFilepath, 'utf8')
			.then(mainFileData => {
				let replacedFile = mainFileData.replace(/{{APP_BUILD_VERSION}}/gi, appVersion);
				return writeFile(mainFilepath, replacedFile);
			})
			.catch(() => 
			{
	
			});
		}
		else if(files[key].indexOf('.html') > -1)
		{
			const mainFilepath = path.join(__dirname, '/www/', files[key]);

			console.log(mainFilepath);
	
			readFile(mainFilepath, 'utf8')
			.then(mainFileData => {
				let replacedFile = mainFileData.replace(/type="module"/gi, 'type="text/javascript"');
				return writeFile(mainFilepath, replacedFile);
			})
			.catch(() => 
			{
	
			});
		}		
	}

    console.log(`Writing version and hash to ${versionFilePath}`);

    // write current version and hash into the version.json file
    const json = {
		version : appVersion
	};

	writeFile(versionFilePath, JSON.stringify(json), 'utf8');
})
.then(() => 
{
	console.log('done');
})