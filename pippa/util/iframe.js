var _iframe = {
	registerIframe(args)
	{
		if(window._iframes == undefined)
		{
			window._iframes = [];
		}
	
		if(args._offset == undefined)
		{
			args._offset = 0;	
		}	
	
		if(args._scrollTop == undefined)
		{
			args._scrollTop = true;	
		}
	
		window._iframes.push(args);
	},
	getArgs(target)
	{
		for(const key in window._iframes)
		{
			const args = window._iframes[key];

			if(args.targets && args.targets.includes(target))
			{
				args.embed = document.getElementById(args.id);
				
				return args;
			}			
		}
	}
}

function _handleMessage(e) 
{		
	const args = _iframe.getArgs(e.data.target)

	if(args)
	{		
		if(e.data.type == 'resize')
		{		
			args.embed.height = e.data.height + args._offset;
		}
		else if(e.data.type == 'adjust')
		{
			args._lastHeight  = args.embed.height;
			args.embed.height = document.documentElement.clientHeight;
		}
		else if(e.data.type == 'back')
		{
			args.embed.height = args._lastHeight;
		}
	
		if(args._scrollTop)
		{
			window.scrollTo(0,0);
		}	
		//embed.scrollIntoView();
	}
	else
	{
		console.error('args not defined', e.data);
	}
}

if ( window.addEventListener ) {
	window.addEventListener('message', _handleMessage, false);
} else if ( window.attachEvent ) { // ie8
	window.attachEvent('onmessage', _handleMessage);
}