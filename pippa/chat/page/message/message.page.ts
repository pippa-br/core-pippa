import { Component, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { NavController, IonContent } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import { serverTimestamp } from "firebase/firestore";
import { Keyboard } from "@capacitor/keyboard";

/* PIPPA */
import { BasePage }    		  from "../../../core/base/page/base.page";
import { Message }     		  from "../../../core/model/message/message";
import { File }        		  from "../../../core/model/file/file";
import { ChatPlugin }   	  from "../../../core/plugin/chat/chat.plugin";
import { MessagePlugin }	  from "../../../core/plugin/message/message.plugin";
import { UploadPlugin }  	  from "../../../core/plugin/upload/upload.plugin";
import { FormPlugin }  	  	  from "../../../core/plugin/form/form.plugin";
import { NotificationPlugin } from "../../../core/plugin/notification/notification.plugin";
import { TransactionType } 	  from "../../../core/type/transaction/transaction.type";
import { AlertType } 	  	  from "../../../core/type/alert/alert.type";
import { TDate } from "src/core-pippa/pippa/core/util/tdate/tdate";

declare let window : any;

@Component({
    selector      		: "message-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<ion-header>
							<ion-toolbar>
								<ion-buttons slot="start">
									<ion-menu-toggle>
										<ion-menu-button></ion-menu-button>
									</ion-menu-toggle>
									<ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
										<ion-icon name="arrow-back-outline"></ion-icon>
									</ion-button>
								</ion-buttons>
								<ion-title>{{title}}</ion-title>
								<ion-buttons slot="end">
									<ion-button ion-button icon-only (click)="onClose()">
										<ion-icon name="close-outline"></ion-icon>
									</ion-button>
									<ion-button *ifAcl="'add_form';acl:acl;"
												icon-only
												hideDelay="0" showDelay="0" 
												matTooltip="Formulários"
												(click)="goGridForm()">
										<ion-icon name="albums-outline"></ion-icon>
									</ion-button>   
								</ion-buttons>
							</ion-toolbar>
						</ion-header>
						<ion-content class="message-view ion-padding" #contentMessage>
							<div class="messages">
								<div class="message" *ngFor="let item of collection; let l = last; let f = first; trackBy : trackById"
									[ngClass]="{'me': document?.sender?.id == item?.sender?.id}"
									[lastElement]="l" (last)="onPosition()">

									<div class="message-header">
										<ion-thumbnail item-start>
											<img *ngIf="item.sender.photo" [src]="item?.sender?.photo?._url">
											<img *ngIf="!item.sender.photo" src="./assets/img/avatar.png">
										</ion-thumbnail>
										<p class="name">{{item?.sender?.name}}</p>
										<p class="time">{{item.postdate | dateFormat:'dd/MM/yyyy HH:mm'}}</p>
										<a *ngIf="item.isOwner()" class="close" tappable (click)="del(item)">
											<ion-icon name="close-outline"></ion-icon>
										</a>
									</div>

									<div class="message-content">
										<p class="text" *ngIf="item.type == 'text'" [innerHTML]="item.text | breakLine"></p>
										<div file-viewer-block *ngIf="item.file" [file]="item.file" [photo]="item.sender.photo"></div>										
									</div>

								</div>
								<div class="messages-last"></div>
							</div>
							<input #fileInput type="file" (change)="onUpload($event)" style="display: none;"/>
						</ion-content>
						<ion-footer>
							<ion-toolbar *ifAcl="'add_document';acl:acl;" [ngClass]="{'open-mic' : time > 0, 'close-mic' : time == 0}">
								<ion-buttons class="actions-start" slot="start">
									<ion-spinner item-start [hidden]="collection"></ion-spinner>
									<a [hidden]="!collection" class="add" tappable (click)="openOptions()">
										<ion-icon name="add-outline"></ion-icon>
									</a>
								</ion-buttons>
								<ion-textarea rows="1" item-content (keyup)="change()"
											placeholder="Digite sua mensagem..."
											id="messageInputBox"
											[(ngModel)]="text">
								</ion-textarea>
								<ion-buttons class="actions-end" slot="end">
									<a [hidden]="!collection || text == ''" class="send" tappable (click)="sendText()">
										<ion-icon name="send-outline"></ion-icon>
									</a>
									<div *ngIf="this.core().isApp" [hidden]="!collection || text != ''" class="mic">
										<a *ngIf="false" class="mic-icon" (press)="pressed()" (pressup)="released()" (panend)="onPressEnd($event)">
											<ion-icon name="mic-outline"></ion-icon>
										</a>
										<a class="time">
											{{formatTime()}} <span class="label">Deslize para Fechar <ion-icon class="arrow-outline" name="arrow-back-outline"></ion-icon></span>
										</a>
									</div>
								</ion-buttons>
							</ion-toolbar>
						</ion-footer>`,
})
export class MessagePage extends BasePage
{
    /*
     <!---
    <p class="date" *ngIf="viewDate(f, item.date)" ion-affix [content]="contentMessage">
        <span>{{item.date | dateFormat:'DD/MMM/Y'}}</span>
    </p>
    ---->
    */

    @ViewChild(IonContent,  { static : false })  content  : IonContent;
    @ViewChild("fileInput", { static : false }) fileInput : ElementRef;

    public uploader:any;

    public title           = "Chat";
    public text            = "";
    public mediaObject 	  : any;
    public time        	   = 0;
    public int         	  : any;
    public element     	  : any;
    public textarea       : any;
    public scrollContent  : any;
    public lastDate  	  : any;
    public subscribe	  : any;

    constructor(		
		public changeDetectorRef  : ChangeDetectorRef,
        public navController      : NavController,
        public elementRef         : ElementRef,
        public alertController    : AlertController,
        public chatPlugin         : ChatPlugin,
        public messagePlugin      : MessagePlugin,
		public uploadPlugin       : UploadPlugin,
		public formPlugin         : FormPlugin,
		public notificationPlugin : NotificationPlugin,		
    )
    {
        super();
    }

    async initialize()
    {
        await this.document.on(true);

        this.title = this.document.receiver.name;

        console.log("open messages");
        console.log("sender",   this.document.sender);
        console.log("receiver", this.document.receiver);
        console.log("target",   this.document.target);
        console.log("path",     this.document.reference.path);

        /* GET MESSAGES */
        let params = this.createParams({
            appid  	   : this.app.code,
            chat   	   : this.document,
            autoReload : true,
        });

        const result = await this.messagePlugin.getData(params);		
        await result.collection.on(true);

        this.collection = result.collection;
        this.subscribe  = this.collection.onUpdate().subscribe(async () =>
        {
            await result.collection.on(true);
            this.changeDetectorRef.markForCheck();
        });

        /* FORM */
        params = this.createParams({
            appid : this.app.code,
        });

        this.form = await this.formPlugin.getOne(params);

        setTimeout(() =>
        {
            /* REMOVE BADGE */
            //this.badge.decrease(this.document.count);

            if (this.document)
            {
                /* UPDATE COUNT CHAT  */
                this.document.set({
                    count : 0
                });
            }
			
            this.changeDetectorRef.markForCheck();
        });
    }

    getPlugin()
    {
        return this.messagePlugin;
    }

    //ionViewDidEnter()
    //{
    // super.ionViewDidEnter();

    //this.scrollContent = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    //this.element       = document.getElementById('messageInputBox');

    //if(this.element)
    //{
    //this.textarea = this.element.getElementsByTagName('textarea')[0];
    //}

    //this.onPosition();
    //}

    goGridForm()
    {
        this.push("grid-form",
            {
                appid : this.app.code
            });
    }

    viewDate(first:any, date:any)
    {
        date = new TDate({ value : date });

        if (first || !this.lastDate)
        {
            this.lastDate = date;

            return true;
        }
        else
        {
            const days    = date.format("DD") - this.lastDate.format("DD");
            this.lastDate = date;
            return days > 0;
        }
    }

    onPosition()
    {
        setTimeout(() =>
        {
            this.content.scrollToBottom(300);
        }, 1000);
    }

    sendText()
    {
        console.log("this.text", this.text);

        if (this.text != "")
        {
            /* POST MESSAGE */
            const message = new Message(
                {
                    sender   : this.document.sender,
                    receiver : this.document.receiver,
                    target   : this.document.target,
                    text     : this.text,
                    type     : Message.TEXT
                });

            this.sendMessage(message);

            this.text = "";
            //let scroll_height = 38;

            // reset style
            //this.element.style.height     = (scroll_height + 10) + "px";
            //this.textarea.style.minHeight = scroll_height + "px";
            //this.textarea.style.height    = scroll_height + "px";
            //this.scrollContent.style.marginBottom = (41) + "px"
        }
    }

    sendImage(file:File)
    {
        if (file)
        {
            /* POST MESSAGE */
            const message = new Message(
                {
                    receiver : this.document.receiver,
                    sender   : this.document.sender,
                    target   : this.document.target,
                    file     : file,
                    text     : "Imagem",
                    type     : Message.IMAGE
                });

            this.sendMessage(message);
        }
    }

    sendAudio(file:File)
    {
        if (file)
        {
            /* POST MESSAGE */
            const message = new Message(
                {
                    receiver : this.document.receiver,
                    sender   : this.document.sender,
                    target   : this.document.target,
                    file     : file,
                    text     : "Audio",
                    type     : Message.AUDIO
                });

            this.sendMessage(message);
        }
    }

    sendDocument(file:File)
    {
        if (file)
        {
            /* POST MESSAGE */
            const message = new Message(
                {
                    receiver : this.document.receiver,
                    sender   : this.document.sender,
                    target   : this.document.target,
                    file     : file,
                    text     : "Document",
                    type     : Message.DOCUMENT
                });

            this.sendMessage(message);
        }
    }

    async sendMessage(message:any)
    {
        /* ADD MESSAGE */
        message.accid     = this.app.accid;
        message.appid     = this.app.code;
        message.form      = this.form.reference;
        message.postdate  = serverTimestamp();
        message.projectID = this.core().account.projectID;
		
        const data = message.parseData();
        await this.collection.save(data);

        /* UPDATE CHAT  */
        const params = this.createParams({
            appid    : this.app.code,
            sender   : this.document.sender,
            receiver : this.document.receiver,
            target   : this.document.target,
            data     : data,
        });

        this.chatPlugin.setChat(params);
		
        /* SEND NOTIFICATIOON */
        await this.notificationPlugin.send(message, this.form, TransactionType.ADD_TYPE);

        /* ALERTS */
        if (this.core().isMobile())
        {
            let alerts = this.core().account.alerts;

            if (!alerts)
            {
                alerts = [];
            }

            alerts.push({
                type : AlertType.CHAT
            });

            this.core().account.set({
                alerts : alerts
            });	
        }

        /* CLOSE */
        //if(Capacitor.isPluginAvailable)
        //{
        Keyboard.hide();
        //} 
		
        //this.changeDetectorRef.markForCheck();
    }

    formatTime()
    {
        let minutes = this.time / 60
        minutes     = parseInt("" + minutes);

        const seconds = this.time % 60;

        return (minutes > 9 ? minutes : "0" + minutes) + ":" + (seconds > 9 ? seconds : "0" + seconds);
    }

    onPressEnd($event:any)
    {
        console.log("onPan audio", $event);

        if (this.time > 0)
        {
            if ($event && $event.distance > 20)
            {
                this.cancel();
            }
            else
            {
                this.released();
            }
        }
    }

    pressed()
    {
        this.cancel();
        this.time = 1;

        /*this.mediaNative.create().then(data =>
        {
            this.mediaObject = data;

            window.AVAudioSession.setCategoryWithOptions(
                window.AVAudioSession.Categories.PLAY_AND_RECORD,
                window.AVAudioSession.CategoryOptions.MIX_WITH_OTHERS,
                () =>
                {
                    console.log('successful window.AVAudioSession');
                },
                () =>
                {
                    console.log('error window.AVAudioSession');
                }
            );

            this.mediaObject.media.release();
            this.mediaObject.media.startRecord();

            this.int = setInterval(() => {
                this.time++;
            }, 1000);
        });*/
    }

    cancel()
    {
        clearInterval(this.int);
        this.time = 0;

        if (this.mediaObject)
        {
            this.mediaObject.media.stopRecord();
            this.mediaObject.media.release();

            /* BUG IOS */
            window.AVAudioSession.setCategoryWithOptions(
                window.AVAudioSession.Categories.MULTI_ROUTE,
                window.AVAudioSession.CategoryOptions.MIX_WITH_OTHERS,
                () =>
                {
                    console.log("successful window.AVAudioSession");
                },
                () =>
                {
                    console.log("error window.AVAudioSession");
                }
            );

            //this.mediaNative.remove(this.mediaObject.file);

            this.mediaObject.media = null;
            this.mediaObject       = null;

            console.log("cancel", this.mediaObject);
        }
    }

    released()
    {
        clearInterval(this.int);
        this.time = 0;

        this.mediaObject.media.stopRecord();
        this.mediaObject.media.release();

        /* BUG IOS */
        window.AVAudioSession.setCategoryWithOptions(
            window.AVAudioSession.Categories.MULTI_ROUTE,
            window.AVAudioSession.CategoryOptions.MIX_WITH_OTHERS,
            () =>
            {
                console.log("successful window.AVAudioSession");
            },
            () =>
            {
                console.log("error window.AVAudioSession");
            }
        );

        /*this.uploadPlugin.upload(this.mediaObject.file).then(file =>
        {
            this.mediaNative.remove(this.mediaObject.file);

            console.log('released', file);

            this.sendAudio(file);

            this.mediaObject.media = null;
            this.mediaObject = null;
        });*/
    }

    openOptions()
    {
        /*let buttons = []

        if(this.core().isApp)
        {
            buttons.push({
                text    : 'Camera',
                icon    : 'camera',
                handler : () =>
                {
                    this.uploadPlugin.uploadCamera().then(file =>
                    {
                        console.log(file);
                        this.sendImage(file);
                    });
                }
            });
        }

        buttons.push({
            text    : 'Fotos',
            icon    : 'image',
            handler : () =>
            {
                /* BROWSER */
        /*if(!this.core().isApp)
                {
                    this.fileInput.nativeElement.click();
                }
                else
                {
                    this.uploadPlugin.uploadLibrary().then(file =>
                    {
                        console.log(file);
                        this.sendImage(file);
                    });
                }
            }
        });

        if(!this.core().isApp)
        {
            buttons.push({
                text    : 'Documentos',
                icon    : 'document',
                handler : () =>
                {
                    this.fileInput.nativeElement.click();
                }
            });
        }

        buttons.push({
            text: 'Fechar',
            role: 'cancel',
            handler: () => {
                console.log('Cancel clicked');
            }
        });

        let actionSheet = this.actionSheetController.create({
            buttons : buttons
        });

        actionSheet.present();*/

        this.fileInput.nativeElement.click();
    }

    onUpload(event:any)
    {
        const fileInput = event.target.files[0];

        this.uploadPlugin.uploadFromInput(fileInput).then(file =>
        {
            this.sendImage(file);
        });
    }

    del(item:any)
    {
        this.alertController.create({
            message : "Você deseja remover essa mensagem?",
            buttons : [
                {
                    text : "Fechar",
                    role : "cancel",
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        item.del();
                    }
                },				
            ]
        })
        .then(alert =>
        {
            alert.present();
        })
    }

    change()
    {
        // set default style for textarea
        //this.textarea.style.minHeight = '0';
        //this.textarea.style.height    = '0';

        // limit size to 96 pixels (6 lines of text)
        //var scroll_height = this.textarea.scrollHeight;

        //if(scroll_height > 96)
        //{
        //scroll_height = 96;
        //}

        // apply new style
        /*this.element.style.height     = (scroll_height + 10) + "px";
        this.textarea.style.minHeight = scroll_height + "px";
        this.textarea.style.height    = scroll_height + "px";

        this.scrollContent.style.marginBottom = (scroll_height + 41 - 28) + "px"

        this.onPosition();*/
    }

    /*ionViewWillLeave()
    {
        console.log('modal ionViewWillLeave');
        this.cancel();
    }*/

    onClose()
    {
        this.back();
    }
	
    ngOnDestroy()
    {
        if (this.subscribe)
        {
            this.subscribe.unsubscribe();
        }		
    }
}
