import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { MessagePage } from "./message.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : MessagePage } ])
    ],
    declarations : [
        MessagePage,
    ],
})
export class MessageModule 
{}
