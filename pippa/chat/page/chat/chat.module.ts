import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule } from "../../../core";
import { ChatPage }   from "./chat.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ChatPage } ])
    ],
    declarations : [
        ChatPage,
    ],
})
export class ChatModule 
{}
