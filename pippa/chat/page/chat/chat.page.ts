import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage }    from "../../../core/base/page/base.page";
import { ChatPlugin }  from "../../../core/plugin/chat/chat.plugin";

@Component({
    selector      		: "chat-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<ion-header>
							<ion-toolbar>
								<ion-buttons slot="start">
									<ion-menu-toggle>
										<ion-menu-button></ion-menu-button>
									</ion-menu-toggle>
								</ion-buttons>
								<ion-title>{{title}}</ion-title>
							</ion-toolbar>
						</ion-header>
						<ion-content class="chats ion-padding">
							<ion-list>
								<ion-item class="chat" *ngFor="let chat of collection; trackBy : trackById" (click)="openMessages(chat)">
									<ion-thumbnail item-start *ngIf="chat.receiver && chat.receiver.photo">
										<img [src]="chat?.receiver?.photo?._url">
									</ion-thumbnail>
									<div class="content-text">
										<h2 class="name">{{chat?.receiver?.name}}</h2>
										<p class="text" *ngIf="chat.lastdate">{{chat.lastdate | dateFormat : 'dd/MM/yyyy HH:MM'}}</p>
										<p class="text" *ngIf="chat.target">#{{chat.target._sequence}}</p>
										<p class="text" *ngIf="chat.lastMessage">{{chat.lastMessage.text}}</p>
									</div>
									<ion-badge color="danger" slot="end" *ngIf="chat.count && chat.count > 0">{{chat.count}}</ion-badge>
								</ion-item>
							</ion-list> 
						</ion-content>`,
})
export class ChatPage extends BasePage
{
    public title       = "Chat";
    public text       : string;
    public subscribe  : any;

    constructor(
		public chatPlugin 		 : ChatPlugin,
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }

    async initialize()
    {
        if (!this.document)
        {
            this.document = this.core().user;

            if (this.app.chatType == "account")
            {
                this.document = this.core().account;
            }			
        }

        if (this.document)
        {
            this.title = this.document.name;

            /* GET CHATS */
            const params = this.createParams({
                appid  : this.app.code,
                sender : this.document,
            });

            const result = await this.chatPlugin.getData(params);		
            await result.collection.on(true);			

            this.collection = result.collection;
            this.subscribe  = this.collection.onUpdate().subscribe(async () =>
            {
                await result.collection.on(true);
                this.changeDetectorRef.markForCheck();
            });				
			
            this.changeDetectorRef.markForCheck();
        }
    }

    openMessages(chat:any)
    {
        this.push("chat-message",
            {
                appid        : this.app.code,
                documentPath : chat.reference.path
            },
            {
                chat : chat.id,
                menu : this.core().routeParams.menu + "-messagens",
            });
    }

    onClose()
    {
        this.back();
    }
	
    ngOnDestroy()
    {
        if (this.subscribe)
        {
            this.subscribe.unsubscribe();
        }		
    }
}
