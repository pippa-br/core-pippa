export const AuthRoutes = [
    {
        path         : "chat",
        loadChildren : "./page/chat/chat.module#ChatModule"
    },
    {
        path         : "chat-message",
        loadChildren : "./page/message/message.module#MessageModule"
    }
];
