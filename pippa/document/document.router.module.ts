export const DocumentRoutes = [
    {
        path      		 : "form-document",
        loadChildren : () => import("./page/form.document/form.document.module").then(m => m.FormDocumentModule)
    },
    {
        path      		 : "grid-document",
        loadChildren : () => import("./page/grid.document/grid.document.module").then(m => m.GridDocumentModule)
    },
    {
        path      		 : "viewer-document",
        loadChildren : () => import("./page/viewer.document/viewer.document.module").then(m => m.ViewerDocumentModule)
    },
    {
        path      		 : "indexer-document",
        loadChildren : () => import("./page/indexer.document/indexer.document.module").then(m => m.IndexerDocumentModule)
    },
    {
        path      		 : "import-document",
        loadChildren : () => import("./page/import.document/import.document.module").then(m => m.ImportDocumentModule)
    },
];
