import { Component } from "@angular/core";

/* PIPPA */
import { BaseGrid } from "../../../core/base/grid/base.grid";

@Component({
    selector : "[grid-log]",
    template : `<div dynamic-list
                     [grid]="grid"
                     [collection]="collection"
                     (add)="onAdd($event)"
                     (set)="onSet($event)"
                     (del)="onDel($event)"
                     (viewer)="onViewer($event)">
                </div>`,
})
export class GridLog extends BaseGrid
{
}
