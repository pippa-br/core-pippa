import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseForm }    from "../../../core/base/form/base.form";
import { DynamicForm } from "../../../core/dynamic/form/dynamic.form";

@Component({
    selector        : "[form-document]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<form #dynamicForm 
                             dynamic-form
                             [form]="form"
                             [data]="data"
                             [acl]="acl"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (close)="onCancel()"
                             (invalid)="onInvalid($event)">
                        </form>`,
})
export class FormDocument extends BaseForm
{
    @ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    destroy()
    {
        this.dynamicForm.destroy();
    }
	
    display()
    {
        this.dynamicForm.display();
    }

    loadForm()
    {
        this.changeDetectorRef.markForCheck();
    }

    onAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    onSet(event:any)
    {
        this.setEvent.emit(event);
    }
}
