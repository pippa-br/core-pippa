import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseViewer }    from "../../../core/base/viewer/base.viewer";
import { DynamicViewer } from "../../../core/dynamic/viewer/dynamic.viewer";

@Component({
    selector        : "[viewer-log]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div #dynamicViewer
                            dynamic-viewer
                            [viewer]="viewer"
                            [data]="data"
                            (close)="onClose()">
                        </div>`
})
export class ViewerLog extends BaseViewer
{
    @ViewChild("dynamicViewer", { static : true }) dynamicViewer : DynamicViewer;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    onClose()
    {
        this.back();
    }

    destroy()
    {
        this.dynamicViewer.destroy();
    }
}
