import { Component, Input, OnInit } from "@angular/core";

/* PIPPA */
import { BaseForm }        from "../../../core/base/form/base.form";
import { FieldType }       from "../../../core/type/field/field.type";
import { DocumentPlugin }  from "../../../core/plugin/document/document.plugin";
import { Form }            from "../../../core/model/form/form";

@Component({
    selector : "[copy-document]",
    template : `<form dynamic-form
                      [form]="form"
                      (add)="onAdd($event)"
                      class="login-form">
                </form>`,
})
export class CopyDocument extends BaseForm implements OnInit
{
    constructor(
        public documentPlugin : DocumentPlugin,
    )
    {
        super();
    }

    @Input() addText   = "Copiar";

    ngOnInit()
    {
        const items = [];

        items.push({
            row   : 0,
            col   : 0,
            field : {
                type        : FieldType.type("TEXT"),
                name        : "fromPath",
                label       : "De",
                placeholder : "De",
            }
        });

        items.push({
            row   : 0,
            col   : 1,
            field : {
                type        : FieldType.type("TEXT"),
                name        : "toPath",
                label       : "Para",
                placeholder : "Para",
            }
        });

        this.form = new Form({
            items : items
        });
    }

    onAdd(event:any)
    {
        const params = this.createParams({
            app  : this.app,
            post : event.data
        });

        this.documentPlugin.copy(params).then((result:any) =>
        {
            if (result.status)
            {
                this.addEvent.emit(result);
            }
            else
            {
                this.onError(result);
            }

            this.form.reset();
        });
    }

    onError(result:any)
    {
        this.alertController.create({
            header  : "Alerta",
            message : result.message,
            buttons : [ "OK" ]
        })
         .then((alert:any) =>
         {
             alert.present();
         });
    }
}
