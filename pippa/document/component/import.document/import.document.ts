import { Component, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseGrid }     from "../../../core/base/grid/base.grid";
import { ImportPlugin } from "../../../core/plugin/import/import.plugin";
import { DynamicList }  from "../../../core/dynamic/list/dynamic.list";

@Component({
    selector        : "[import-document]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<input type="file"
                              #fileInput
                              style="display: none;"
                              name="Upload"
                              id="txtFileUpload"
                              (change)="fileChangeListener($event)" accept=".xlsx"/>

                        <ion-button [hidden]="grid" class="import-button" slot="icon-only" color="medium" (click)="openInput()">
                            <ion-icon name="cloud-upload-outline"></ion-icon>&nbsp;&nbsp;&nbsp;Importar XLSX
                        </ion-button>

                        <div dynamic-list
                             [hidden]="!grid"
                             #dynamicList
                             [grid]="grid"
                             [acl]="acl"
                             [collection]="collection"
                             (add)="onAdd($event)"
                             (set)="onSet($event)"
                             (del)="onDel($event)"
                             (viewer)="onViewer($event)"
                             (rendererComplete)="onRendererComplete()"
                             (rendererClear)="onRendererClear()">
                        </div>`,
})
export class ImportDocument extends BaseGrid
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList;
    @ViewChild("fileInput", { static : false }) fileInput: ElementRef;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
        public importPlugin      : ImportPlugin
    )
    {
        super();    
    }

    openInput()
    {
        this.fileInput.nativeElement.click();
    }

    destroy()
    {
        this.dynamicList.destroy();
    }

    async fileChangeListener(event: any)
    {
        if (event.srcElement.files.length)
        {
            const result = await this.importPlugin.import(this.form, event.srcElement.files);
			
            this.grid       = result.data;
            this.collection = result.collection;

            this.grid.hasSetButton = false;
            //this.grid.hasDelButton 	  = false;
            this.grid.hasViewerButton = false;

            //console.error(result);

            this.changeDetectorRef.markForCheck();			
        }        
    }

    onDel(event:any)
    {
        this.collection.del(event.data);
    }

    onRendererComplete()
    {
        this.core().loadingController.stop();
    }

    onRendererClear()
    {
        this.core().loadingController.stop();
    }

    async clear()
    {
        await this.core().loadingController.start();
       
        this.fileInput.nativeElement.value = ""
        this.collection                    = null;
        this.grid                          = null;

        this.dynamicList.destroy();
    }    
}
