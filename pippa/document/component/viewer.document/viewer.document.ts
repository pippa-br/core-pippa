import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';

/* PIPPA */
import { BaseViewer }    from '../../../core/base/viewer/base.viewer';
import { DynamicViewer } from '../../../core/dynamic/viewer/dynamic.viewer';

@Component({
    selector        : '[viewer-document]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div #dynamicvViewer
                            dynamic-viewer
                            [viewer]="viewer"
                            [data]="data"
                            (close)="onClose()">
                        </div>`
})
export class ViewerDocument extends BaseViewer
{
    @ViewChild('dynamicvViewer', {static:true}) dynamicViewer : DynamicViewer;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    onClose()
    {
        this.back();
    }

    destroy()
    {
        this.dynamicViewer.destroy();
    }
}
