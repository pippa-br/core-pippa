import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }      from "../../../core";
import { ViewerChartPage } from "./viewer.chart.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ViewerChartPage } ])
    ],
    declarations : [
        ViewerChartPage,
    ],
})
export class ViewerChartModule 
{}
