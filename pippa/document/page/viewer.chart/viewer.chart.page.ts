import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import html2canvas from "html2canvas";

/* PIPPA */
import { BaseViewerChartPage } from "./base.viewer.chart.page";

@Component({
    selector        : "viewer-chart-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_viewer';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" class="back-button" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{getTitle()}}
								</ion-title>
								
								<filter-header-block [app]="app" 
												[acl]="acl" 
												[where]="where" 
												(change)="onReload()">
								</filter-header-block> 
								
								<ion-buttons slot="end">
                                    <ion-button ion-button icon-only
                                                (click)="downloadChart()">
                                        <ion-icon name="download-outline"></ion-icon>
                                    </ion-button>                                
									<!--- <ion-button ion-button icon-only
                                    //             (click)="onPrint()">
                                    //     <ion-icon name="print-outline"></ion-icon>
									// </ion-button> --->
									<ion-button ion-button icon-only
                                                *ngIf="hasSetButton()"
                                                (click)="onSet()">
                                        <ion-icon name="create-outline"></ion-icon>
                                    </ion-button>
									<ion-button ion-button icon-only                                                
                                                (click)="onReload()">
										<ion-icon name="reload-outline"></ion-icon>
									</ion-button>
								</ion-buttons>								
                            </ion-toolbar>
						</ion-header>						
                        <ion-content class="ion-padding" shadowCss=".scroll-x, .scroll-y{overscroll-behavior: auto !important;}">
                            
                            <div class="chart-content">

                                <filter-chart-block [chart]="chart" (change)="onFilter($event)"/> 

                                <div #dynamicChart
                                        dynamic-chart
                                        [chart]="chart"
                                        [data]="data">
                                </div>

                            </div>
                            
                        </ion-content>`, 
})
export class ViewerChartPage extends BaseViewerChartPage
{
    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);		
    }

    async onFilter(event)
	{
		const params = this.core().paramsFactory.create({
                accid : this.chart.accid,
                appid : this.chart.appid,
                colid : this.chart.colid,
                chart : this.chart,
                ...event,
        });

        const result = await this.chartPlugin.getDataChart(params);

        this.data = result.data;	
        
        this.markForCheck();
	}

    markForCheck()
	{
		this.changeDetectorRef.markForCheck();
	}	

    downloadChart() 
    {
        // Use html2canvas para converter o gráfico em uma imagem
        html2canvas(document.getElementById("chartContainer")).then(function (canvas) 
        {
            // Crie um link de download para a imagem
            const downloadLink    = document.createElement("a");
            downloadLink.href     = canvas.toDataURL("image/png");
            downloadLink.download = "grafic.png";	
            downloadLink.click();		
        });
    }
}
