import { Component, ViewChild, Directive, ChangeDetectorRef, HostBinding } from "@angular/core";
import { IonContent } from "@ionic/angular";

/* PIPPA */
import { BasePage }        from "../../../core/base/page/base.page";
import { ChartPlugin }     from "../../../core/plugin/chart/chart.plugin";
import { PrintPlugin }     from "../../../core/plugin/print/print.plugin";
import { FilterPlugin }    from "../../../core/plugin/filter/filter.plugin";
import { FieldType }       from "../../../core/type/field/field.type";
import { PaginationType }  from "../../../core/type/pagination/pagination.type";
import { WhereCollection } from "../../../core/model/where/where.collection";
import { TDate } from "src/core-pippa/pippa/core/util/tdate/tdate";

@Directive()
export class BaseViewerChartPage extends BasePage
{
    @ViewChild(IonContent, { static : false }) content : IonContent;
    @ViewChild("dynamicChart", { static : true }) dynamicChart : any;

    public chartPlugin  : any;
    public printPlugin  : any;
    public filterPlugin : any;
    public where        : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();

        this.chartPlugin  = this.core().injector.get(ChartPlugin);
        this.printPlugin  = this.core().injector.get(PrintPlugin);
        this.filterPlugin = this.core().injector.get(FilterPlugin);
    }

    getTitle()
    {
        if (this.document)
        {
            return this.document.name;
        }
        else if (this.chart)
        {
            return this.chart.name;
        }
        else
        {
            return this.app.name;
        }
    }

    initialize()
    {
        this.where = new WhereCollection();

        if (this.chart)
        {
            this._loadData();
        }
        else
        {
            this.core().currentContent = this.content;

            const appid = this.app.code;

            /* LOAD CHART */
            const params = this.createParams({
                appid   : appid,
                /*where : [{
field : 'form',
value :  this.form.reference,
type  : FieldType.type('ReferenceSelect'),
}],*/
                orderBy : "name",
            });

            const groups = [ {
                id    : "all",
                label : "Todos",
                value : "all",
            } ];

            if (this.core().user)
            {
                groups.push(this.core().user.getGroup());
            }

            params.where.set({
                field    : "groups",
                operator : "array-contains-any",
                value    : groups,
                type   	 : FieldType.type("MultiSelect"),
            });

            this.chartPlugin.getData(params).then((result:any) =>
            {
            /* VIEWER */
                if (result.collection.length > 0)
                {
                    const chart = result.collection[0];                

                    chart.on(true).then(() =>
                    {
                        this.chart = chart;
                        this.where.setItems(this.chart.filters);

                        this._loadData();
                    });
                }
                else
                {
                    console.error("chart not found");
                }
            });	
        }        
    }

    onReload()
    {
        this._loadData();
    }

    _loadData()
    {
        this.doPaginationFilter().then(async () =>
        {
            const params = this.createParams({
                appid   : this.app.code,
                colid   : this.app._colid,
                where   : this.where,
                chart   : this.chart,
                orderBy : this.chart.orderBy.value,
                asc     : this.chart.asc.value,
                perPage : 9999                
            });

            const result = await this.chartPlugin.getDataChart(params);

            this.data = result.data;
            this.markForCheck();
        });
    }

    markForCheck()
	{
		this.changeDetectorRef.markForCheck();
	}	

    doPaginationFilter(args?:any)
    {
        return new Promise<void>((resolve) =>
        {
            if (!args)
            {
                args = {};
            }

            if (this.isPaginationType(PaginationType.WEEK.value))
            {
                const startDate = new TDate({ value : args.date }).startOfWeek().toDate();
                const endDate   = new TDate({ value : args.date }).endOfWeek().toDate();

                const itemStart = {
                    id       : "_start",
                    field    : this.chart.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week start", itemStart);

                const itemEnd = {
                    id       : "_end",
                    field    : this.chart.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else if (this.isPaginationType(PaginationType.MONTH_YEAR.value))
            {
                const startDate = new TDate({ value : args.date }).startOfYear().toDate();
                const endDate   = new TDate({ value : args.date }).endOfYear().toDate();

                const itemStart = {
                    id       : "_start",
                    field    : this.chart.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter year start", itemStart);

                const itemEnd = {
                    id       : "_end",
                    field    : this.chart.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter year end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else if (this.isPaginationType(PaginationType.MONTH.value))
            {
                const startDate = new TDate({ value : args.date }).startOfMonth().toDate();
                const endDate   = new TDate({ value : args.date }).endOfMonth().toDate();

                const itemStart = {
                    id       : "_start",
                    field    : this.chart.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter month start", itemStart);

                const itemEnd = {
                    id       : "_end",
                    field    : this.chart.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter month end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else if (this.isPaginationType(PaginationType.YEAR.value))
            {
                const startDate = new TDate({ value : args.date }).startOfYear().toDate();
                const endDate   = new TDate({ value : args.date }).endOfYear().toDate();

                const itemStart = {
                    id       : "_start",
                    field    : this.chart.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter year start", itemStart);

                const itemEnd = {
                    id       : "_end",
                    field    : this.chart.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter year end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else if (this.isPaginationType(PaginationType.DAY.value))
            {
                const startDate = new TDate({ value : args.date }).startOfDay().toDate();
                const endDate   = new TDate({ value : args.date }).endOfDay().toDate();

                const itemStart = {
                    id       : "_start",
                    field    : this.chart.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week start", itemStart);

                const itemEnd = {
                    id       : "_end",
                    field    : this.chart.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else
            {
                resolve();
            }
        });
    }

    hasSetButton()
    {
        return this.hasAcl("add_form");
    }

    onSet()
    {
        this.push("form-chart",
            {
                dataPath : this.chart.reference.path,
            });
    }

    isPaginationType(value:string)
    {
        return this.chart && this.chart.paginationType.value == value;
    }

    onPrint()
    {
        window.print();       
    }

    ngOnDestroy()
    {
        if (this.dynamicChart)
        {
            this.dynamicChart.clear();
        }

        super.ngOnDestroy();
    }
}
