import { Component } from "@angular/core";

/* APP */
import { BasePage } from "../../../core/base/page/base.page";

@Component({
    selector    : "app-home",
    templateUrl : "home.page.html",
    styleUrls   : [ "home.page.scss" ],
})
export class HomePage extends BasePage
{
    initialize()
    {
        // OVERRIDER
    }
}
