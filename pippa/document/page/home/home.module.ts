import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { HomePage } from "./home.page";

/* PIPPA */
import { CoreModule } from "../../../core/core.module"; 

@NgModule({
    imports : [ 
        CoreModule,
        RouterModule.forChild([ { path : "", component : HomePage } ])
    ],
    declarations : [ HomePage ]
})
export class HomeModule 
{}
