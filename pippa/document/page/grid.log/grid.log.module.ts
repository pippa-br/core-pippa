import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }  from "../../../core";
import { GridLog }     from "../../component/grid.log/grid.log";
import { GridLogPage } from "./grid.log.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridLogPage } ])
    ],
    declarations : [
        GridLogPage,
        GridLog,
    ],
})
export class GridLogModule 
{
}
