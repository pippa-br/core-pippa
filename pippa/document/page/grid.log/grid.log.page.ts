import { Component } from "@angular/core";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { FieldType }      from "../../../core/type/field/field.type";
import { LogPlugin }      from "../../../core/plugin/log/log.plugin";
import { Grid }           from "../../../core/model/grid/grid";
import { PaginationType } from "../../../core/type/pagination/pagination.type";

@Component({
    selector : "grid-log-page",
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-buttons slot="start">
                            <ion-menu-toggle>
                                <ion-menu-button></ion-menu-button>
                            </ion-menu-toggle>
                            <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                <ion-icon name="arrow-back-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                        <ion-title>
                            <ion-icon [name]="app?.icon?.value"></ion-icon>
                            {{app?.name}}
                        </ion-title>
                        <ion-buttons slot="end" (click)="onClose()">
                          <ion-button icon-only>
                              <ion-icon name="close-outline"></ion-icon>
                          </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>
                    <div dynamic-list
                            [grid]="grid"
                            [collection]="collection"
                            (del)="onDel($event)"
                            (viewer)="onViewer($event)">
                    </div>    
                </ion-content>
                <ion-footer *ngIf="acl && acl.hasKey('show_pagination') && isPaginationType(PaginationType.NUMBER.value)">
                    <div only-next-pagination [collection]="collection" [total]="total"></div>
                </ion-footer>`,
})
export class GridLogPage extends BasePage
{
    public total           = 0;
    public PaginationType : any = PaginationType;

    constructor(
        public logPlugin : LogPlugin,
    )
    {
        super();
    }

    initialize()
    {
        const params = this.createParams({
            appid : this.app.code
        });

        if (this.document)
        {
            params.where =  [ {
                field : "document",
                value : this.document.reference,
            } ];
        }

        this.logPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;

            this.getTotal(params);

            //this.flatCollection = result.collection.getFlat();

            //this.collection.change = () =>
            //{
            //console.log('change flatCollection');
            //this.flatCollection = this.collection.getFlat();
            //};
        });        

        this.grid = new Grid({
            hasSetButton    : false,
            hasDelButton    : false, //this.core().user.isMaster(),
            hasViewerButton : true,
            items           : [
                {
                    field : {
                        label   : "Data da Criação",
                        name    : "postdate",
                        type    : FieldType.type("Date"),
                        setting : {
                            hasTime : true
                        }
                    }
                },
                {
                    field : {
                        label : "Tipo",
                        name  : "type",
                        type  : FieldType.type("Select"),
                    }
                },
                {
                    field : {
                        label : "Usuário",
                        name  : "owner",
                        type  : FieldType.type("ReferenceSelect"),
                    }
                },
                {
                    field : {
                        label : "ID",
                        name  : "document._sequence",
                        type  : FieldType.type("Text"),
                    }
                },
                {
                    field : {
                        label : "Documento",
                        name  : "document",
                        type  : FieldType.type("ReferenceSelect"),
                    }
                },
            ],
        });
    }

    async getTotal(params)
    {
        const result = await this.plugin.count(params);

        if (this.collection)
        {
            this.collection.total = result.total;
        }

        this.total = result.total;
    }
	
    onViewer(event:any)
    {
        this.core().push("viewer-log",
            {
                documentPath : event.data.reference.path,
            });
    }

    async onDel(event:any)
    {
        this.core().loadingController.start(true).then(() =>
        {
            this.plugin.del(event.data).then(() => 
            {
                this.core().loadingController.stop();
				
                this.collection.reload();				
            });
        });
    }

    isPaginationType(value:string)
    {
        return this.grid && this.grid.paginationType.value == value;
    }

    onClose()
    {
        this.back();
    }
}
