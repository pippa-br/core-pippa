import { Component } from "@angular/core";

/* PIPPA */
import { BasePage } from "../../../core/base/page/base.page";

@Component({
    selector : "copy-document-page",
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-buttons slot="start">
                            <ion-menu-toggle>
                                <ion-menu-button></ion-menu-button>
                            </ion-menu-toggle>
                            <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                <ion-icon name="arrow-back-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                        <ion-title>
                            {{app?.name}}
                        </ion-title>
                        <ion-buttons slot="end" (click)="onClose()">
                          <ion-button icon-only>
                              <ion-icon name="close-outline"></ion-icon>
                          </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>

                <ion-content >

                    <div copy-document
                         [app]="app">
                    </div>

                </ion-content>`,
})
export class CopyDocumentPage extends BasePage
{
    constructor(
    )
    {
        super();
    }

    onClose()
    {
        this.back();
    }
}
