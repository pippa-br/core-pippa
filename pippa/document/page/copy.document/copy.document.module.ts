import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }       from "../../../core";
import { CopyDocument }     from "../../component/copy.document/copy.document";
import { CopyDocumentPage } from "./copy.document.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : CopyDocumentPage } ])
    ],
    declarations : [
        CopyDocumentPage,
        CopyDocument,
    ],
})
export class CopyDocumentModule 
{}
