import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }       from "../../../core";
import { GridDocumentPage } from "./grid.document.page"; 

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridDocumentPage } ])
    ],
    declarations : [
        GridDocumentPage,
    ],
})
export class GridDocumentModule
{
}
