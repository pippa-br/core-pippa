import { Directive, ChangeDetectorRef, ViewChild, OnDestroy } from "@angular/core";
import { PopoverController } from "@ionic/angular";
import { Router } from "@angular/router";
import { MbscEventcalendarOptions } from "@mobiscroll/angular";
import { IonContent } from "@ionic/angular";
import { Subscription } from "rxjs";
import { IonInfiniteScroll } from "@ionic/angular";
import { Location } from "@angular/common"; 
import * as FileSaver from "file-saver";

/* PIPPA */
import { BasePage }           from "../../../core/base/page/base.page";
import { ListPopover }        from "../../../core/popover/list/list.popover";
import { ExportPopover }      from "../../../core/popover/export/export.popover";
import { EmailPopover }       from "../../../core/popover/email/email.popover";
import { FormFormPopover }    from "../../../core/popover/form.form/form.form.popover";
import { ExportPlugin }       from "../../../core/plugin/export/export.plugin";
import { FormPlugin }         from "../../../core/plugin/form/form.plugin";
import { GridPlugin }         from "../../../core/plugin/grid/grid.plugin";
import { ViewerPlugin }       from "../../../core/plugin/viewer/viewer.plugin";
import { FunctionsType }      from "../../../core/type/functions/functions.type";
import { PaginationType }     from "../../../core/type/pagination/pagination.type";
import { ExportType }         from "../../../core/type/export/export.type";
import { WhereCollection }    from "../../../core/model/where/where.collection";
import { LogType }            from "../../../core/type/log/log.type";
import { FieldType }          from "../../../core/type/field/field.type";
import { FormType }           from "../../../core/type/form/form.type";
import { IframeModal }        from "../../../core/modal/iframe/iframe.modal";
import { DynamicList } 		  from "../../../core/dynamic/list/dynamic.list";
import { GatewayPlugin } 	  from "../../../core/plugin/gateway/gateway.plugin";
import { ViewerModal } 		  from "../../../core/modal/viewer/viewer.modal";
import { DocumentCollection } from "../../../core/model/document/document.collection";
import { BaseModel, EventPlugin, Types, Viewer } from "src/core-pippa/pippa/core";
import { environment }   from "src/environments/environment";
import { StoragePlugin } from "src/core-pippa/pippa/core/plugin/storage/storage.plugin";
import { TDate } from "src/core-pippa/pippa/core/util/tdate/tdate";

@Directive()
export class BaseGridDocumentPage extends BasePage implements OnDestroy
{
    @ViewChild("content",        { static : true }) content        : IonContent; 
    @ViewChild("dynamicList",    { static : true }) dynamicList    : DynamicList;
    @ViewChild("viewerDocument", { static : true }) viewerDocument : any;
    @ViewChild("pagination", 	 { static : false }) pagination    : any;
    @ViewChild(IonInfiniteScroll) infiniteScroll		   	   	   : IonInfiniteScroll;

    public showSearch     	   = false;
    public forms           	  : any; 
    public filters         	  : any;
    public dataFilters     	  : any;
    public where          	  : any;
    public _disabledFilter	   = false;
    public buttons        	  : Array<any>;
    public events         	  : Array<any>;
    public calendarPagination  = false;    	
    public _listSubscribe  	  : any;
    public term 			  : string;
    public gridPlugin   	  : any;
    public formPlugin   	  : any;
    public exportPlugin 	  : any;
    public viewerPlugin 	  : any;
    public popoverController  : any;
    public storagePlugin      : any;
    //public modalController    : any;
    public PaginationType 	  : any = PaginationType;
    public mobiscrollInstance : any;
    public eventSettings 	  : MbscEventcalendarOptions;
    public location		  	  : Location;
    public router		 	  : Router;
    public selecteds		  : any; 
    public addLabel            = "";
    public total               = 0;
    public currentPage         = 1;

    constructor(
        public changeDetectorRef : ChangeDetectorRef)
    {
        super(); 

        this.gridPlugin        = this.core().injector.get(GridPlugin);
        this.formPlugin        = this.core().injector.get(FormPlugin);
        this.viewerPlugin      = this.core().injector.get(ViewerPlugin);
        this.exportPlugin      = this.core().injector.get(ExportPlugin);
        this.popoverController = this.core().injector.get(PopoverController);
        //this.modalController   = this.core().injector.get(ModalController);	
        this.location      = this.core().injector.get(Location);
        this.router        = this.core().injector.get(Router);
        this.storagePlugin = this.core().injector.get(StoragePlugin);
    }    
	
    onNextPage()
    {
        console.error("onNextPage");

        if (this.collection.hasNextPage())
        {
            this.collection.nextPage().then(() =>
            {
                this.infiniteScroll.complete();
                console.log("onNextPage");
            });
        }
        else
        {
            this.infiniteScroll.disabled = true;
        }
    }    
	
    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }

    async onShowSearch()
    {
        this.showSearch = !this.showSearch;

        if (!this.showSearch)
        {
            this.clearSearch();	
            this.term = "";

            // RE COUNT
            const where = this.where.copy();

            const params = this.createParams({
                appid : this.app.code,
                colid : this.app._colid,
                where : where,
            });			

            await this.getTotal(params);
        }		

        this.markForCheck();
    }

    isPaginationType(value:string)
    {
        return this.grid && this.grid.paginationType.value == value;
    }

    initialize()
    {
        this.where                 = new WhereCollection();
        this.selecteds             = new DocumentCollection();
        this.core().currentContent = this.content;

        /* LOAD GRID*/
        this.getGrid().then(() =>
        {
            if (this.grid)
            {
                const filters = [];

                if (this.grid.filters && this.grid.filters.length > 0)
                {
                    for (const item of this.grid.filters)
                    {
                        if (this.core().user.hasGroups(item._groups))
                        {
                            filters.push(item);
                        }
                    }
                }

                this.where.setItems(filters);

                this.setPermissions();

                if (this.isPaginationType(PaginationType.WEEK.value) || this.isPaginationType(PaginationType.DAY.value))
                {
                    this.settingCalendar();
                    this.calendarPagination = true;

                    if (this.mobiscrollInstance)
                    {
                    //this.mobiscrollInstance.redraw();						
                    //this.mobiscrollInstance.setDate(new Date());
                        this.mobiscrollInstance.refresh();
                    }

                    this.markForCheck();
                }
                else
                {
                    this.doPaginationFilter().then(async () =>
                    {
                        await this.load();
                    });
                }
            }
            else
            {
                this.markForCheck();
                console.error("grid not found");
            }
        });

        /* BUTTONS */
        this.buttons = [];

        if (this.hasAcl("search_document"))
        {
            this.buttons.push({
                name : "Buscar",
                icon : {
                    value : "search-outline"
                },
                click : (event:any) => 
                {
                    this.onShowSearch();
                },
            });
        }

        if (this.hasAcl("export_document"))
        {
            this.buttons.push({
                name : "Exporta",
                icon : {
                    value : "cloud-download-outline"
                },
                click : (event:any) => 
                {
                    this.export();
                },
            });
        }

        if (this.hasAcl("import_document"))
        {
            this.buttons.push({
                name : "Importar",
                icon : {
                    value : "cloud-upload-outline"
                },
                click : (event:any) => 
                {
                    this.import();
                },
            });
        }

        if (this.hasAcl("log_document"))
        {
            this.buttons.push({
                name : "Logs",
                icon : {
                    value : "server-outline"
                },
                click : (event:any) => 
                {
                    this.goLog();
                },
            });
        }

        if (this.hasAcl("indexer_document"))
        {
            this.buttons.push({
                name : "Indexar",
                icon : {
                    value : "aperture-outline"
                },
                click : (event:any) => 
                {
                    this.goIndexer();
                },
            });
        }

        if (this.hasAcl("acl_grid"))
        {
            this.buttons.push({
                name : "Acl",
                icon : {
                    value : "lock-closed-outline"
                },
                click : (event:any) => 
                {
                    this.goGridAcl();
                },
            });
        }

        if (this.hasAcl("add_form"))
        {           			
            this.buttons.push({
                name : "Formulários",
                icon : {
                    value : "albums-outline"
                },
                click : (event:any) => 
                {
                    this.goGridForm();
                },
            });
        }

        if (this.hasAcl("reload_grid"))
        {           			
            this.buttons.push({
                name : "Atualizar Dados",
                icon : {
                    value : "reload-outline"
                },
                click : async (event:any) => 
                {
                    this.onReload();
                },
            });
        }
    }

    async onReload()
    {
        await this.collection.clearCache();
        this.load();					
    }

    async onSort(event:any)
    {
        await this.collection.clearCache();
        this.load();					
    }

    setPermissions()
    {
        this.grid.hasAddLevelButton  = this.hasAcl("add_document");
        this.grid.hasSetButton       = this.grid.hasSetButton === undefined ? this.hasAcl("set_document") : this.grid.hasSetButton && this.hasAcl("set_document");
        this.grid.hasDelButton       = this.hasAcl("del_document");
        this.grid.hasOrderButton     = this.hasAcl("order_grid");
        this.grid.hasCancelButton    = this.hasAcl("cancel_document",    false);
        this.grid.hasCloneButton     = this.hasAcl("clone_document",     false);
        this.grid.hasArchiveButton   = this.hasAcl("archive_document",   false);
        this.grid.hasUnarchiveButton = this.hasAcl("unarchive_document", false);
        this.grid.hasEmailButton     = this.hasAcl("email_document",     false);
        this.grid.hasExpandButton    = this.hasAcl("expand_document",    false);
        this.grid.hasViewerButton    = this.hasAcl("viewer_document");
    }

    settingCalendar()
    {
        const theme : any = this.core().account.theme ? "dark" : "ligth"

        console.log("init calendar");

        this.eventSettings =
{
    view : {
        calendar : {
            type : "week",
        },
    },
    themeVariant : theme,
    onInit       : (event, inst) => 
    {
        this.mobiscrollInstance = inst;

        console.log("onInit", event);

        this.doPaginationFilter({
            date : event.firstDay,
        }).then(async () => 
        {
            await this.load();
        });
    },
    onPageChange : (event, inst) => 
    {
        console.log("onPageChange", event);

        this.doPaginationFilter({
            date : event.firstDay,
        }).then(async () => 
        {
            await this.load();
        });
    },        
    /*/onPageLoading: (event, inst) => 
{
console.log('onPageLoading', event);;

this.doPaginationFilter({
date : event.firstDay,
}).then(async () => 
{
await this.load();
});
},*/
    onCellClick : (event, inst) => 
    {            
        const key           = new TDate({ value : event.date }).format("yyyy-MM-dd");
        const element : any = document.getElementsByClassName(key);

        if (element.length > 0)
        {
            const yOffset = element[0].offsetTop - 40;
            this.content.scrollToPoint(0, yOffset, 1000);
        }

        console.log("onDayChange", element, key);

        this.doPaginationFilter({
            date : event.date,
        }).then(async () => 
        {
            await this.load();
        });
    },
    onEventClick : (event) =>
    {
        /*if(event.domEvent.target.classList.contains('set-button'))
{
this.emitSet(event.event);
}
else if(event.domEvent.target.classList.contains('del-button'))
{
this.emitDel(event.event);
}*/
    }
};
    }

    async disabledFilter()
    {        
        for (const key in this.where)
        {
            this.where[key].enabled = false;
        }

        this._disabledFilter = true;

        await this.load();
    }

    clearFilter()
    {
        this._disabledFilter = false;    
    }

    async load()
    {
        const collection = await this.getList(this.where);

        if (collection)
        {
        /* LOAD FILTER DOCUMENT */
            this.setFunctionFilter(collection).then((collection:any) =>
            {
                if (this.core().queryParams.autoClick && collection.length > 0)
                {
                    this.onViewer({
                        data : collection[0]
                    });
                }
                else
                {
                /* EVENTS CALENDAR */
                    if (this.calendarPagination && this.grid.groupBy)
                    {
                        const events = [];

                        for (const key in collection)
                        {
                            const item = collection[key];
                            events.push({
                                d : item[this.grid.groupBy.value],
                            });
                        }

                        this.events = events;
                    }

                    this.collection            = collection;
                    this.collection.isInfinite = this.isPaginationType(PaginationType.INFINITE.value);

                    if (this._listSubscribe)
                    {
                        this._listSubscribe.unsubscribe();
                    }            

                    this._listSubscribe = new Subscription();

                    this._listSubscribe.add(collection.onUpdate().subscribe(() =>
                    {
                        console.log("collection change display");

                        this.loadCollection();
                    }));					

                    this.loadCollection();
                }                
            });			
        }		

        this.markForCheck();
    }

    loadCollection()
    {
    // OVERRIDER
    }

    ionViewDidLeave()
    {
        if (this.dynamicList)
        {
            this.dynamicList.destroy();
        }			

        if (this.viewerDocument)
        {
            this.viewerDocument.destroy();
        }			

        /* AQUI VOLTA A PAGINAÇÃO INICIAL */
        if (this.mobiscrollInstance)
        {
            this.mobiscrollInstance.setDate(new Date());
            this.mobiscrollInstance.refresh();
        }

        // SEARCH
        this.showSearch = false;
        //this.clearSearch();	
        this.term = "";

        super.ionViewDidLeave();
    }

    ngOnDestroy()
    {
        console.log("ngOnDestroy");

        if (this._listSubscribe)
        {
            this._listSubscribe.unsubscribe();
        }

        this.clearFilter();

        super.ngOnDestroy();
    }

    getGrid()
    {
        return new Promise<void>(async (resolve) =>  
        {
            if (this.grid)
            {
                await this.getViewer(); 

                resolve(); 
            }
            else
            {
            /* LOAD GRID */
                const params = this.createParams({
                    appid  	: this.app.code,
                    orderBy : "name",
                });				

                const values = [ {
                    id    : "all",
                    label : "Todos",
                    value : "all",
                } ];

                this.core().user.group = null;
                const group            = this.core().user.getGroup();

                // POR SEGURANCA NÃO PERMITIR USUARIO SEM GRUPO
                ///&& group.value != 'guest' removi pois no campolim precisa de grid guest
                if (group && group.value)
                {
                    values.push(group);

                    params.where.set({
                        field    : "groups",
                        operator : "array-contains-any",
                        value    : values,
                        type   	 : FieldType.type("MultiSelect"),
                    });

                    this.gridPlugin.getData(params).then(async (result:any) =>
                    {
                    /* GRID */
                        if (result.collection.length > 0)
                        {
                            const grid = result.collection[0];

                            // REMOVE BATCH DESATIVADOS
                            if (grid.batch && grid.batch.length > 0)
                            {
                                grid.batch = grid.batch.filter(item => 
                                {									
                                    return item.status;
                                })
                            }

                            this.grid = grid;

                            console.log("grid", this.grid);

                            this.nameClass +=  this.grid.type.value + "-type ";

                            await this.getViewer();

                            resolve();
                        }
                        else
                        {
                            resolve();
                        }
                    });
                }		
                else
                {
                    const eventPlugin = this.core().injector.get(EventPlugin);
                    eventPlugin.dispatch("logout", this.core().user);
                    eventPlugin.dispatch(this.core().user.appid + ":logout", this.core().user);	
                }		
            }
        });
    }

    async getViewer()
    {
        if (this.grid.setting)
        {
            if (this.grid.setting.viewerPath)
            {
                const paths = this.grid.setting.viewerPath.split("/");

                const params = this.createParams({
                    getEndPoint : Types.GET_DOCUMENT_API,
                    accid       : paths[0],
                    appid       : paths[1],
                    colid       : paths[2],
                    path        : this.grid.setting.viewerPath,
                    model       : Viewer,
                    mapItems    : {
                        referencePath : environment.defaultMapItems
                    }
                })

                const result = await this.viewerPlugin.getObject(params);
                this.viewer  = result.data;					
            }

            if (this.grid.setting.addLabel)
            {
                this.addLabel = this.grid.setting.addLabel;
            }
        }
    }

    async getForm(level:number)
    {
        if (!this.forms)
        {			
            await this.getForms();			
        }

        let selected: any;

        for (const key in this.forms)
        {
            const form = this.forms[key];

            if (form.id != "master")
            {
                if (form._level == level)
                {
                    selected = form
                    break;
                }
                else
                {
                    selected = form;
                }
            }
        }

        if (!selected)
        { 
            selected = this.forms[0];
        }

        return selected;
    }    

    get user()
    {
        return this.core().user;
    }

    doPaginationFilter(args?:any)
    {
        return new Promise<void>((resolve) =>
        {
            if (!args)
            {
                args = {};
            }

            if (this.isPaginationType(PaginationType.WEEK.value))
            {
                const startDate = new TDate({ value : args.date }).startOfWeek().toDate();
                const endDate   = new TDate({ value : args.date }).endOfWeek().toDate();

                const itemStart = {
                    id       : "startOfWeek",
                    field    : this.grid.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week start", itemStart);

                const itemEnd = {
                    id       : "endOfWeek",
                    field    : this.grid.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else if (this.isPaginationType(PaginationType.DAY.value))
            {
                const startDate = new TDate({ value : args.date }).startOfDay().toDate();
                const endDate   = new TDate({ value : args.date }).endOfDay().toDate();

                const itemStart = {
                    id       : "startOfWeek",
                    field    : this.grid.orderBy.value,
                    operator : ">=",
                    value    : startDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week start", itemStart);

                const itemEnd = {
                    id       : "endOfWeek",
                    field    : this.grid.orderBy.value,
                    operator : "<=",
                    value    : endDate,
                    type     : FieldType.type("Date"),
                };

                console.log("loadPaginationFilter week end", itemEnd);

                this.where.set(itemStart);
                this.where.set(itemEnd);

                resolve();
            }
            else
            {
                resolve();
            }
        });
    }

    getPerPage()
    {
        if (this.grid)
        {
            if (this.isPaginationType(PaginationType.NUMBER.value))
            {
                return this.grid.perPage;
            }
            else if (this.isPaginationType(PaginationType.WEEK.value) || this.isPaginationType(PaginationType.DAY.value))
            {
                return 1000;
            }
        }
        else
        {
            return 25;
        }
    }

    async getList(where:any)
    {
        if (this.app)
        {
            const params = this.createParams({
                appid   : this.app.code,
                colid  	: this.app._colid,
                where   : where,
                perPage : this.getPerPage(),	
                page    : this.currentPage,		
            });

            if (this.grid)
            {
                const orderBy = await this.storagePlugin.get(this.grid.id + "_orderBy");
                const asc     = await this.storagePlugin.get(this.grid.id + "_asc");

                // orderBy == this.grid.orderBy pois fica deixando sempre da grid como default
                if (orderBy)
                {
                    params.orderBy = orderBy;
                }
                else if (this.grid.orderBy)
                {
                    params.orderBy = this.grid.orderBy.value;

                    // CASO DE 2 ORDENAÇÕES
                    if (this.grid.setting && this.grid.setting.orderBy)
                    {
                        params.orderBy += "," + this.grid.setting.orderBy
                    }

                    await this.storagePlugin.set(this.grid.id + "_orderBy", params.orderBy);
                }

                //&& asc == this.grid.asc pois fica deixando sempre da grid como default
                if (typeof asc == "boolean")
                {
                    params.asc = asc;
                }
                else if (this.grid.asc)
                {
                    params.asc = this.grid.asc.value;

                    await this.storagePlugin.set(this.grid.id + "_asc", params.asc);
                }

                if (this.grid.map)
                {
                    params.map      = this.grid.map;
                    params.mapItems = this.grid.reference;
                }

                if (this.grid.groupCollection)
                {
                    params.groupCollection = this.grid.groupCollection;

                    /* CADASTROS ANTIGOS */
                    if (this.app.colid == "documents")
                    {
                        params.where.push({
                            field  	 : "appid",
                            operator : "==",
                            value  	 : this.app.code,
                        });
                    }
                }
            }			

            const result = await this.plugin.getData(params);
            this.getTotal(params);

            return result.collection;
        }
        else
        {
            return;
        }
    }

    async getTotal(params)
    {
        const result = await this.plugin.count(params);

        if (this.collection)
        {
            this.collection.total = result.total;
        }

        this.total = result.total;
        this.markForCheck();
    }

    setFunctionFilter(collection:any)
    {
        return new Promise<any>((resolve) =>
        {
        /* FILTER DOCUMENT */
            const item = this.getAcl("function_filter_grid",
                {
                    name : null
                });

            console.log("set function_filter_grid", item);

            if (item && item.name)
            {
                collection.setFilter(FunctionsType.get().getFunction(item.name));

                resolve(collection);
            }
            else
            {
                resolve(collection);
            }
        });
    }

    onClose()
    {
        this.back();
    }  

    async onViewer(event:any)
    {
        console.log("go viewer");

        /* GO TO APP */
        if (this.grid.filterApp)
        {
            this.grid.filterApp.on().then((app:any) =>
            {
                const router     = this.grid.toRouter.value;
                const data : any = { 
                    appid     		 : app.code,					
                    documentPath : event.data.reference.path
                };

                this.core().push(router, data,
                    {
                        app : app.id,
                    });
            })
        }
        else if (this.grid.toRouter && this.grid.toRouter != "") /* ROTA DEFINADA NO GRID */
        {
            console.log("grid router", this.grid.toRouter, this.grid.params);

            let filterField = null;

            if (this.grid.params && this.grid.params.filterField)
            {
                filterField = this.grid.params.filterField;
                delete this.grid.params.filterField;
            /*for(const key in this.grid.params)
{
if(key == 'filterField')
{
filterField = this.grid.params[key]
delete this.grid.params.filterField;
break;
}
}*/	
            }	

            if (filterField)
            {
                event.data.getProperty(filterField).then((data2:any) => 
                {
                    const params : any = this.grid.params || {};

                    params.appid        = data2.appid,
                    params.documentPath = data2.reference.path,

                    this.core().push(this.grid.toRouter.value, params,
                        {
                            app : data2.appid,
                        });
                });
            }
            else
            {

                const app = this.core().getApp(event.data.appid).copy();
                //app.viewer = this.getViewer(this.grid.viewer);

                //this.goApp(app, event); /* USA GO APP NÃO PUSH */

                //event.app = this.app;/
                this.core().push(this.grid.toRouter.value,
                    {
                        appid        : app.code,
                        documentPath : event.data.reference.path,
                    },
                    {
                        app : app.code,
                    });				
            }
        }
        else if (event.data.toUrl && event.data.toUrl != "") /* URL DEFINADA NO GRID */
        {
            console.log("data toUrl", event.data.toUrl);

            this.modalController.create({
                component       : IframeModal,
                backdropDismiss : false,
                cssClass        : "full",
                componentProps  : {
                    url     : event.data.toUrl,
                    title   : event.data.name,
                    onClose : () =>
                    {
                        this.modalController.dismiss();
                    }
                }
            })
            .then((modal:any) =>
            {
                modal.present();
            });
        }
        else if (event.data.router) /* ROTA DEFINADA NO DATA, CASO MENU */
        {
            event.data.app.on().then(() =>
            {
            //const app = this.getApp(event.data.appid).copy();

                const router : string = event.data.router.value;

                console.log("menu router", router, event.data.app.code);

                this.core().push(router, {
                    appid : event.data.app.code
                },
                {
                    app : event.data.app.id,
                });
            });
        }
        // else if(event.data.link != undefined)
        // {
        // 	if(event.data.link)
        // 	{
        // 		window.open(event.data.link, '_blank');
        // 	}            
        // }
        else if (this.grid.viewerOpenMode && this.grid.viewerOpenMode.value == FormType.MODAL_OPEN_MODE.value)
        {
            console.log("viewerOpenMode", this.grid.viewerOpenMode);

            const url = this.router.url;
            this.location.replaceState("viewer-document;" + this.app.id + "?documentPath=" + event.data.reference.path + "&appid=" + event.data.appid);

            this.modalController.create(
                {
                    component      : ViewerModal,
                    componentProps : {
                        document : event.data,
                        onReload : () => 
                        {
                            this.onReload();
                        },
                        onClose : () => 
                        {
                            this.location.replaceState(url);
                            this.modalController.dismiss();
                        }
                    },
                    showBackdrop    : true,
                    backdropDismiss : false,
                    cssClass        : "full",
                })
            .then(popover =>
            {
                popover.present();
            });				                	
        }
        else if (event.data.reference)
        {
            this.core().push("viewer-document",
                {
                    documentPath : event.data.reference.path,
                });
        }

        this.resetTimeout();

        //this.onViewer(event);
    }	    

    openButtons(event:any)
    {
        this.popoverController.create(
            {
                component : ListPopover,
                event     : event,
                componentProps :
{
    title    : "Ações",
    items    : this.buttons,
    onSelect : ((item:any) =>
    {
        item.click(event);
        this.popoverController.dismiss();
    })
},
                showBackdrop : false,
            })
        .then(popover =>
        {
            popover.present();
        });
    }

    async getForms()
    {
        /* GET FORM */
        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "name"
        });

        const values = [ {
            id    : "all",
            label : "Todos",
            value : "all",
        } ];

        this.core().user.group = null;
        const group            = this.core().user.getGroup();

        values.push(group);

        params.where.set({
            field    : "groups",
            operator : "array-contains-any",
            value    : values,
            type   	 : FieldType.type("MultiSelect"),
        });        

        const result = await this.formPlugin.getData(params);			
        this.forms   = result.collection;	
    }

    async goListForm(event:any) 
    {
        if (!this.forms)
        {			
            await this.getForms();			
        }

        if (this.forms.length == 1)
        {
            this.openForm(this.forms[0]);
        }
        else
        {
            const popover = await this.popoverController.create(
                {
                    component      : ListPopover,
                    event          : event,
                    componentProps : {
                        title    : "Formulários",
                        items    : this.forms,
                        onSelect : ((form:any) =>
                        {
                            this.popoverController.dismiss();
                            this.openForm(form);
                        })
                    },
                    showBackdrop : false,
                });

            popover.present();
        }
    }	

    onEmail(event:any) 
    {
        this.popoverController.create(
            {
                component : EmailPopover,
                componentProps :
{
    title   : "E-mail",
    data    : event.data,
    onClose : ((form:any) =>
    {
        this.popoverController.dismiss();
    })
},
                showBackdrop : true,
                cssClass   	 : "right-popover",
            })
        .then(popover =>
        {
            popover.present();
        });
    }

    onSelect(event:any) 
    {
        if (event.selected)
        {
            this.selecteds.set(event.data);
        }
        else
        {
            this.selecteds.del(event.data);
        }

        if (this.pagination)
        {
            this.pagination.markForCheck();
        }
    }

    async onBatch(event:any) 
    {
        if ((this.selecteds && this.selecteds.length > 0) || event.noSelected)
        {
            event.params.platform = this.core().platform;

            if (event.validate)
            {
                const noValids = [];

                for (const item of this.selecteds)
                {
                    const validate = await item.getProperty(event.validate);

                    //console.error('validate', event.validate, validate);

                    if (!validate)
                    {						
                        noValids.push(item._sequence);
                    }
                }

                if (noValids.length == 0)
                {
                    event.params.data  = this.selecteds;
                    event.params.where = this.where.copy();
                    const result : any = await this.callBatch(event);

                    if (event.download)
                    {
                        FileSaver.saveAs(result.data._url);
                    }

                    this.selecteds = new DocumentCollection();
                    this.load();
                }
                else if (noValids.length > 0)
                {
                    let message = "O iten(s)" + noValids.join(", ") + " não é valido para ação";
                    
                    if (event.validateMessage)
                    {
                        message = event.validateMessage.replace("${_sequence}", noValids.join(", "));
                    }                                      

                    this.showAlert("Atenção", message);
                }
            }
            else
            {
                event.params.data  = this.selecteds;
                event.params.where = this.where.copy();

                const result : any = await this.callBatch(event);

                if (event.download)
                {
                    FileSaver.saveAs(result.data._url);
                }

                this.selecteds = new DocumentCollection();
                this.load();
            }
        }
        else
        {
            this.showAlert("Atenção", "Selecione os items para a ação");
        }		
    }

    async callBatch(event:any)
    {
        const result : any = await this.callableApi(event.url, event.params, { model : BaseModel });

        if (result.status && event.params.openForm)
        {
            const form : any = new BaseModel(event.params.openForm);
            await form.on();

            this.modalController.create({
                component : FormFormPopover,
                componentProps :
                {
                    form  : form,
                    data  : new BaseModel(result.data),
                    onSet : (async (event:any) =>
                    {
                        this.modalController.dismiss();

                        const result = await this.plugin.add(form.appid, form, event)

                        setTimeout(() => 
                        {
                            this.collection.reload();
                        });
                    }),					
                    onClose : () =>
                    {
                        this.modalController.dismiss();
                    }
                },
                cssClass        : "full",
                showBackdrop    : true,
                backdropDismiss : false,
            })
            .then(modal =>
            {
                modal.present();

                modal.onDidDismiss().then(() =>
                {
                    this.core().isModal = false;
                });
            })
        }		

        return result;
    }

    async callableApi(url, params, maps?:any)
    {
        await this.core().loadingController.start();
        const result = await this.core().api.callable(url, params, maps);
        await this.collection.reload();
        this.core().loadingController.stop();

        if (!result.status)
        {
            this.alertController.create({
                header  : "Ops! Aconteceu algo inesperado!",
                message : result.error,
                buttons : [ "OK" ]
            })
            .then((alert:any) =>
            {
                alert.present();
            });
        }

        return result;
    }

    async onAdd(event:any)
    {
        const form = await this.getForm(event.level);
        this.openForm(form, event.data, event.parent, event.level);
        this.resetTimeout();
    }

    async onSet(event:any)
    {
        const params = this.createParams({
            path       	: event.data.reference ? event.data.reference.path : event.data.referencePath,
            appid       : this.app.code,
            includeForm : true,
        });

        const resultData = await this.plugin.getObject(params);	
        event.data       = resultData.data; 

        if (event.form)
        {
            this.openForm(event.form, event.data, event.parent, event.level);
        }
        else
        {
            const form = await event.data.getProperty("form");
            this.openForm(form, event.data, event.parent, event.level);
        }

        this.resetTimeout();
    }

    async doClone(event:any)
    {
        await this.core().loadingController.start(true);	
        await this.plugin.clone(event.data);
        await this.core().loadingController.stop();	

        setTimeout(() => 
        {
            this.collection.reload();
        }, 300);
    }

    async openForm(form:any, data?:any, parent?:any, level?:any)
    {
        if (form)
        {
            if (form.openMode.value == FormType.SIDE_OPEN_MODE.value && !this.core().isMobile())
            {
                console.log("openMode", form.openMode, data);

                const url = this.router.url;		

                if (data)
                {
                    this.location.replaceState("form-document;" + this.app.id + "?dataPath=" + data.reference.path + "&appid=" + data.appid);					
                }

                this.popoverController.create(
                    {
                        component : FormFormPopover,
                        componentProps :
                    {
                        form  : form,
                        data  : data,
                        onAdd : (async (event:any) =>
                        {
                            if (!form.saveFixed)
                            {
                                this.popoverController.dismiss();
                            }

                            event.data._parent = (parent != undefined ? parent.reference : null);
                            event.data._level  = (level  != undefined ? level : 1);
                            event.data._isLeaf = true;

                            await this.delCache();

                            const result = await this.plugin.add(this.app.code, form, event);

                            /* SAVE PARENT */
                            if (parent)
                            {
                                const orderBy = this.grid.orderBy.value || "order";

                                parent._children.set(result.data);
                                parent._children.sort((a:any, b:any) => 
                                {									
                                    if (a[orderBy] < b[orderBy])
                                    {
                                        return -1;
                                    }
                                    else if (a[orderBy] > b[orderBy])
                                    {
                                        return 1;
                                    }										
                                    else
                                    {
                                        return 0;
                                    }																	
                                });

                                const parseData = parent.parseData();

                                await this.delCache();

                                await this.plugin.set(this.app.code, parent, form, 
                                    {
                                        data : {
                                            _children : parseData._children
                                        }
                                    });
                            }                                

                            await this.doAction(form, result.data, FormType.ADD_MODE);

                            setTimeout(() => 
                            {
                                this.collection.reload();
                            });						
                        }),
                        onSet : (async (event:any) =>
                        {
                            this.popoverController.dismiss();

                            event.data._level = (level != undefined  ? level : 1);
                            event.data.form   = form,

                            await this.delCache();

                            const result = await this.plugin.set(this.app.code, data, form, event);

                            /* SAVE PARENT */
                            if (parent)
                            {
                                const orderBy = this.grid.orderBy.value || "order";

                                parent._children.set(result.data);
                                parent._children.sort((a:any, b:any) => 
                                {									
                                    if (a[orderBy] < b[orderBy])
                                    {
                                        return -1;
                                    }
                                    else if (a[orderBy] > b[orderBy])
                                    {
                                        return 1;
                                    }										
                                    else
                                    {
                                        return 0;
                                    }																	
                                });

                                const parseData = parent.parseData();
                                await this.delCache();

                                await this.plugin.set(this.app.code, parent, form, 
                                    {
                                        data : {
                                            _children : parseData._children,
                                            _isLeaf   : parseData._children.length == 0,
                                        }
                                    });
                            }                      

                            await this.doAction(form, result.data, FormType.SET_MODE);

                            setTimeout(() => 
                            {
                                this.collection.reload();
                            });       						
                        }),
                        onClose : () =>
                        {
                            this.popoverController.dismiss();
                        }
                    },
                        showBackdrop : false,
                        cssClass     : "right-popover",
                    })
                .then(popover =>
                {
                    popover.present();

                    popover.onDidDismiss().then(() =>
                    {
                        this.location.replaceState(url);
                    });
                });
            }
            else if (form.openMode.value == FormType.MODAL_OPEN_MODE.value && !this.core().isMobile())
            {
                this.core().isModal = true;

                const url = this.router.url;

                if (data)
                {
                    this.location.replaceState("form-document;" + this.app.id + "?dataPath=" + data.reference.path + "&appid=" + data.appid);
                }

                this.modalController.create({
                    component      : FormFormPopover,
                    componentProps : {
                        form  : form,
                        data  : data,
                        onAdd : (async (event:any) =>
                        {
                            this.modalController.dismiss();

                            event.data._parent = (parent != undefined ? parent.reference : null);
                            event.data._level  = (level  != undefined ? level : 1);
                            event.data._isLeaf = true;

                            await this.delCache();

                            const result = await this.plugin.add(this.app.code, form, event)

                            /* SAVE PARENT */
                            if (parent)
                            {
                                const orderBy = this.grid.orderBy.value || "order";

                                parent._children.set(result.data);
                                parent._children.sort((a:any, b:any) => 
                                {									
                                    if (a[orderBy] < b[orderBy])
                                    {
                                        return -1;
                                    }
                                    else if (a[orderBy] > b[orderBy])
                                    {
                                        return 1;
                                    }										
                                    else
                                    {
                                        return 0;
                                    }																	
                                });

                                await this.plugin.set(this.app.code, parent, parent.form, 
                                    {
                                        data : {
                                            _children : parent._children.parseData()
                                        }
                                    });
                            }

                            await this.doAction(form, result.data, FormType.ADD_MODE);

                            setTimeout(() => 
                            {
                                this.collection.reload();
                            });
                        }),
                        onSet : (async (event:any) =>
                        {
                            this.modalController.dismiss();

                            event.data._level = (level  ? level : 1);
                            event.data.form   = form,

                            await this.delCache();

                            const result = await this.plugin.set(this.app.code, data, form, event);

                            /* SAVE PARENT */
                            if (parent)
                            {
                                const orderBy = this.grid.orderBy.value || "order";

                                parent._children.set(result.data);
                                parent._children.sort((a:any, b:any) => 
                                {									
                                    if (a[orderBy] < b[orderBy])
                                    {
                                        return -1;
                                    }
                                    else if (a[orderBy] > b[orderBy])
                                    {
                                        return 1;
                                    }										
                                    else
                                    {
                                        return 0;
                                    }																	
                                });

                                const parseData = parent.parseData();

                                await this.plugin.set(this.app.code, parent, form, 
                                    {
                                        data : {
                                            _children : parseData._children,
                                            _isLeaf   : parseData._children.length == 0,
                                        }
                                    });
                            }

                            await this.doAction(form, result.data, FormType.SET_MODE);

                            setTimeout(() => 
                            {
                                this.collection.reload();
                            });
                        }),
                        onClose : () =>
                        {
                            this.modalController.dismiss();
                        }
                    },
                    cssClass        : "full",
                    showBackdrop    : true,
                    backdropDismiss : false,
                })
                .then(modal =>
                {
                    modal.present();

                    modal.onDidDismiss().then(() =>
                    {
                        this.location.replaceState(url);
                        this.core().isModal = false;
                    });
                })
            }
            else
            {
                if (data)
                {
                    this.push("form-document",
                        {
                            dataPath : data.reference.path,
                        });
                }
                else
                {
                    this.push("form-document",
                        {
                            formPath : form.reference.path,
                        });
                }
            }
        }
        else
        {
            console.log("form not found");
        }
    }

    async doAction(form:any, data:any, mode:any)
    {
        console.log("actions", form.actions);

        if (form.actions && form.actions.length > 0)
        {
            for (const key in form.actions)
            {
                const item = form.actions[key];

                if (item.mode.value == mode.value && this.core().user && this.core().user.hasGroups(item.groups))
                {
                    if (item.action.value == FormType.ALERT_ACTION.value)
                    {
                        this.showAlert("", item.message);
                    }
                    else if (item.action.value == FormType.PAYMENT_ACTION.value)
                    {
                        const gatewayPlugin = this.core().injector.get(GatewayPlugin);
                        await gatewayPlugin.createPayment(data);
                    }
                }
            }
        }
    }

    async onSearch(event:any)
    {		
        if (event.target.value)
        {			
            this.collection.search(event.target.value);
            this.dynamicList.display();

            if (event.target.value != undefined)
            {
                let term = event.target.value.toLowerCase();
                term     = this.core().util.removeAccents(term);

                const where = this.where.copy();
                where.push({
                    field    : "search", 
                    operator : "array-contains", 
                    value    : event.target.value.toLowerCase(),
                });

                const params = this.createParams({
                    appid        : this.app.code,
                    colid        : this.app._colid,
                    where        : where,
                    perPage      : this.getPerPage(),
                    searchVector : term,
                });

                await this.getTotal(params);
            }			

            // if(this._searchTime)
            // {
            // 	clearTimeout(this._searchTime);
            // }

        // this._searchTime = setTimeout(() => 
        // {
        // 	this.collection.search(event.target.value);
        // 	this.dynamicList.display();
        // }, 500);
        }
    }

    onPaging(event:any)
    {
        this.currentPage = event;
        this.dynamicList.display();
    }

    // LIMPAR CACHE IMEDIATO
    async delCache()
    {		
        if (this.collection.keyCache)
        {
            await this.plugin.delCache(this.app.code, this.app._colid, this.collection.keyCache);	
        }
    }

    async doDel(event:any)
    {		
        await this.delCache();

        await this.core().loadingController.start(true);		
        await this.plugin.del(event.data);

        if (event.parent)
        {
            event.parent._children.del(event.data);

            const parseData = event.parent.parseData();

            await this.plugin.set(this.app.code, event.parent, event.parent.form, 
                {
                    data : {                        
                        _children : parseData._children,
                        _isLeaf   : parseData._children.length == 0,
                    }
                });
        }

        this.core().loadingController.stop();

        setTimeout(() => 
        {
            this.collection.reload();
        });
    }

    async doArchive(event:any)
    {
        let type : any;

        if (event.data._archive)
        {
            type = LogType.UNARCHIVE;
        }
        else
        {
            type = LogType.ARCHIVE;
        }

        await this.delCache();

        await this.plugin.set(event.data.appid, event.data, event.data.form, 
            {
                data : {
                    _archive : !event.data._archive
                }
            }, true, type);	

        setTimeout(() => 
        {
            this.collection.reload();
        }, 300);
    }

    async doCancel(event:any)
    {
        let type : any;

        if (event.data._canceled)
        {
            type = LogType.UNCANCEL;
        }
        else
        {
            type = LogType.CANCEL;
        }

        await this.delCache();

        await this.plugin.set(event.data.appid, event.data, event.data.form, 
            {
                data : 
{
    _canceled : !event.data._canceled
}
            }, true, type);	

        setTimeout(() => 
        {
            this.collection.reload();
        });
    }

    export()
    {
        this.popoverController.create(
            {
                component : ExportPopover,
                componentProps :
{
    app      : this.app,
    onExport : ((event:any) =>
    {
        if (this.collection.length > 0)
        {
            const where = new WhereCollection(this.where);

            // FILTRO DE DATAS
            if (event.data.documents.value == ExportType.PERIOD_DOCUMENTS.value)
            {
                event.data.endDate.setHours(23, 59, 59);

                where.set({
                    field  	 : "postdate",
                    operator : ">=",
                    value    : event.data.startDate,
                });

                where.set({
                    field  	 : "postdate",
                    operator : "<=",
                    value    : event.data.endDate,
                });							
            }

            // EXPORT
            this.exportPlugin.export(this.app.code, {}, event.data.gridPath, event.data.type, this.grid, null, where, "postdate", false).then(() =>
            {
                this.popoverController.dismiss();
            });	
        }
    }),
    onCancel : () =>
    {
        this.popoverController.dismiss();
    }
},
                showBackdrop : false,
                cssClass     : "right-popover",
            }).then(popover =>
        {
            popover.present();
        });
    }

    async import()
    {
        if (!this.forms)
        {			
            await this.getForms();			
        }

        if (this.forms)
        {
            if (this.forms.length == 1)
            {
                this.push("import-document",
                    {
                        appid    : this.app.code,
                        formPath : this.forms[0].reference.path,
                    });
            }
            else
            {
                const popover = await this.popoverController.create(
                    {
                        component : ListPopover,
                        event     : event,
                        componentProps :
{
    items    : this.forms,
    onSelect : ((form:any) =>
    {
        this.push("import-document",
            {
                appid    : this.app.code,
                formPath : form.reference.path,
            });

        this.popoverController.dismiss();
    })
},
                        showBackdrop : false,
                    });

                popover.present();                
            }
        }
    }

    hasBatch()
    {
        return this.grid.batch && this.grid.batch.length > 0;
    }

    onAllSelected(value:boolean)
    {
        for (const item of this.dynamicList.listContainer.instance.selectedComponents)
        {
            item.instance.onInput({ checked : value });
        }

        if (this.pagination)
        {
            this.pagination.markForCheck();
        }
    }

    goGridForm()
    {
        this.push("grid-form",
            {
                appid : this.app.code
            });
    }

    goGridAcl()
    {
        this.push("grid-acl",
            {
                documentPath : this.app.reference.path
            });
    }

    goIndexer()
    {
        this.push("indexer-document",
            {
                appid : this.app.code
            });
    }

    goLog()
    {
        this.push("grid-log",
            {
                appid : this.app.code
            });
    }
}
