import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, OnDestroy } from "@angular/core";

/* PIPPA */
import { BaseGridDocumentPage } from "./base.grid.document.page";

@Component({
    selector        : "grid-document-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_grid';acl:acl;value:true;">
                            <ion-toolbar class="toolbar-buttons">
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
								
								<ion-title>
									<ion-icon [name]="core()?.currentMenu?.icon?.value"></ion-icon>
                                    <label>{{grid?.name}}</label>
                                </ion-title>

								<filter-header-block [app]="app" 
                                        [acl]="acl" 
										[where]="where" 
										(change)="load()">
								</filter-header-block> 
                                 
                                <!--- POPOVER --->

                                <ion-buttons slot="end" class="forms-popover" *ifAcl="'add_document';acl:acl;">
                                    <ion-button icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Adicionar"
                                                (click)="goListForm($event)">
                                        <ion-icon name="add-outline"></ion-icon> {{addLabel}}
                                    </ion-button>
                                </ion-buttons>         
                                <ion-buttons slot="end" class="buttons-popover" *ngIf="buttons && buttons.length > 0" >
                                    <ion-button icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Ações"
                                                (click)="openButtons($event)">
                                        <ion-icon name="caret-down-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                
                                <!--- BUTTONS --->

                                <ion-buttons *ngIf="buttons && buttons.length > 0" slot="end" class="other-buttons">    
                                    <ion-button *ngFor="let item of buttons"
                                                icon-only
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="{{item.name}}"
                                                (click)="item.click($event)">
                                        <ion-icon name="{{item.icon.value}}"></ion-icon>
                                    </ion-button>                                       
                                </ion-buttons>
                            </ion-toolbar>
                            <ion-toolbar class="toolbar-searchbar" *ifAcl="'search_document';acl:acl;" [ngClass]="{'hide' : !showSearch}">
                                <ion-searchbar animated 
                                               placeholder="Buscar" 
                                               showCancelButton="never"
                                               [debounce]="1000"
                                               [(ngModel)]="term"
                                               (ionInput)="onSearch($event)">
                                </ion-searchbar>
                                <a class="arrow" (click)="onShowSearch()">
                                    <ion-icon name="caret-up-outline"></ion-icon>
                                </a>
                            </ion-toolbar>
                        </ion-header>						
                        <ion-header class="pagination-bar" *ngIf="calendarPagination">
                            <mbsc-eventcalendar [data]="events" [options]="eventSettings"></mbsc-eventcalendar>
						</ion-header>
						<!--- SCROLL PARA NAVEGAÇÃO HORIZONTAL scroll-x="true" scroll-events="true" --->
                        <ion-content #content>
							
							<div #viewerDocument
                                 *ngIf="viewer"
                                 dynamic-viewer
                                 [viewer]="viewer"
                                 [data]="document">
							</div>
							
							<div #dynamicList
                                 dynamic-list
                                 [acl]="acl"
                                 [grid]="grid"
                                 [collection]="collection"                                 
                                 (add)="onAdd($event)"
                                 (set)="onSet($event)"
                                 (clone)="onClone($event)"
                                 (archive)="onArchive($event)"
                                 (viewer)="onViewer($event)"
                                 (email)="onEmail($event)"
                                 (batch)="onBatch($event)"
                                 (cancel)="onCancel($event)"
                                 (select)="onSelect($event)"
                                 (sort)="onSort($event)"
                                 (reload)="onReload()"
                                 (del)="onDel($event)"> 
							</div>
							
							<ion-infinite-scroll *ngIf="acl && acl.hasKey('show_pagination') && isPaginationType(PaginationType.INFINITE.value)" 
                                                 threshold="100px" 
                                                 (ionInfinite)="onNextPage()">
								<ion-infinite-scroll-content loadingText="Carregando..." loadingSpinner="dots"></ion-infinite-scroll-content>
							</ion-infinite-scroll>

                        </ion-content>
                        <ion-footer *ngIf="acl && acl.hasKey('show_pagination') && isPaginationType(PaginationType.NUMBER.value)">
                            <div number-pagination 
                                 #pagination
                                 [hasBatch]="hasBatch()"
                                 [selecteds]="selecteds" 
                                 [collection]="collection" 
                                 [total]="total" 
                                 (allSelected)="onAllSelected($event)"
                                 (paging)="onPaging($event)">
                            </div>
                        </ion-footer>`,
})
export class GridDocumentPage extends BaseGridDocumentPage
{
    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);		
    }
}
