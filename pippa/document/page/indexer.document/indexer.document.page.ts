import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BasePage } 	from "../../../core/base/page/base.page";

@Component({
    selector        : "indexer-document-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{app?.name}}
                                </ion-title>
                                <ion-buttons slot="end" (click)="onClose()">
                                <ion-button icon-only>
                                    <ion-icon name="close-outline"></ion-icon>
                                </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>

                        <ion-content>

							<div class="indexer-box" [ngClass]="{'finish' : finish}">
								<span>{{percentage}}%</span>
								<small>{{count}} / {{total}}</small>
							</div>

                        </ion-content>`,
})
export class IndexerDocumentPage extends BasePage
{
    public total         = 0;
    public count         = 0;
    public currentPage   = 1;
    public perPage 	     = 100;
    public totalPage     = 25;
    public percentage  : any     = 0;
    public finish 	    = false;
    public search      : any;

    constructor(
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super();
    }
	
    async initialize()
    {
        this.total      = 0;
        this.count      = 0;
        this.percentage = 0;

        this.changeDetectorRef.markForCheck();		

        const plugin = this.app.getPlugin();

        await this.getTotal(plugin);
        await this.getPage(plugin, {});		

        this.changeDetectorRef.markForCheck();
    }

    async getTotal(plugin)
    {
        await this.core().loadingController.start();
		
        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "postdate",
            asc     : false
        });
		
        const countResult = await plugin.count(params);

        this.total     = countResult.count;
        this.totalPage = Math.ceil(this.total / this.perPage);

        await this.core().loadingController.stop();
    }
	
    async getPage(plugin, result)
    {
        await this.core().loadingController.start();
		
        const params = this.createParams({
            appid   : this.app.code,
            orderBy : "postdate",
            asc     : false,
            perPage : this.perPage,
            data    : (result.data ? result.data : null),
        });		
		
        const indexesResult = await plugin.indexes(params);

        await this.core().loadingController.stop();

        this.percentage = ((100 * this.currentPage) / this.totalPage).toFixed(0);

        this.changeDetectorRef.markForCheck();		

        if (this.currentPage < this.totalPage)
        {
            this.getPage(plugin, indexesResult);
        }

        this.currentPage++;
    }

    /*async initialize2()
    {
		/* COUNT */
    /*let params = this.createParams({
            appid : this.app.code,
        });

		const plugin 	  = this.app.getPlugin();
		const countResult = await plugin.count(params);
		
		this.total      = countResult.count;
		this.count      = 0;
		this.percentage = 0;
		this.finish     = false;
		
		this.changeDetectorRef.markForCheck();

		/* DADAS */
    /*params = this.createParams({
            appid      : this.app.code,
			perPage    : 50,
			orderBy    : 'postdate', // SO FAZ PAGINAÇÃO COM ORDERBY
			autoReload : false,
		});
		
        plugin.getData(params).then((result:any) =>
        {
			result.collection.on().then(() => 
			{
				/* ITERATOR */
    /*result.collection.iterator().subscribe((item:any) =>
				{
					/* CREATE INDEXER */
    /*item.createIndexes().then(() =>
					{
						result.collection.nextIterator().then((status:boolean) =>
						{
							if(!status)
							{
								this.finish = true;
								result.collection.reset();
							}
							
							this.count = this.count + 1;

							this.percentage = ((100 * this.count) / this.total).toFixed(0);

							this.changeDetectorRef.markForCheck();
						});
					});
				});
			})			            
        });
    }*/

    onClose()
    {
        this.back();
    }
}