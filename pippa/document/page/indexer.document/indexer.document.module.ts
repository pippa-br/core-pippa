import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }          from "../../../core";
import { IndexerDocumentPage } from "./indexer.document.page";

@NgModule({
    imports : [ 
        CoreModule,
        RouterModule.forChild([ { path : "", component : IndexerDocumentPage } ])
    ],
    declarations : [
        IndexerDocumentPage,
    ],
})
export class IndexerDocumentModule 
{
}
