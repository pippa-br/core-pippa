import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef } from "@angular/core";

/* PIPPA */
import { BaseFormDocumentPage } from "./base.form.document.page";
import { DocumentPlugin }	    from "../../../core/plugin/document/document.plugin";
import { DynamicForm } from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";
import { FormType } from "src/core-pippa/pippa/core";

@Component({
    selector        : "form-document-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_form';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{getTitle()}}
                                </ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding" shadowCss=".scroll-x, .scroll-y{overscroll-behavior: auto !important;}">

                            <form #dynamicForm 
                                    dynamic-form
                                    [form]="form"
                                    [data]="data"
                                    [acl]="acl"
                                    (add)="onAdd($event)"
                                    (set)="onSet($event)"
                                    (close)="onCancel()"
                                    (rendererComplete)="onRendererComplete($event)">
                            </form>

                        </ion-content>
                        <ion-footer *ngIf="hasFooter()">
                            <ion-toolbar #footer>		
                            </ion-toolbar>
                        </ion-footer>`,
})
export class FormDocumentPage extends BaseFormDocumentPage
{        
    constructor(
        public documentPlugin    : DocumentPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(documentPlugin, changeDetectorRef);
    }    

    hasFooter()
    {
        return this.form && this.form.type.value != FormType.EXPAND_TYPE.value;
    }
}
