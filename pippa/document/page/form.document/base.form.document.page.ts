import { Component, ChangeDetectorRef, Directive, ChangeDetectionStrategy, ViewChild, ElementRef } from "@angular/core";
import { AlertController } from "@ionic/angular";
import { IonContent } from "@ionic/angular";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { FormPlugin }     from "../../../core/plugin/form/form.plugin";
import { DocumentPlugin } from "../../../core/plugin/document/document.plugin";
import { Document }       from "../../../core/model/document/document";
import { FormDocument }   from "../../component/form.document/form.document";
import { FormType }    	  from "../../../core/type/form/form.type";
import { GatewayPlugin }  from "../../../core/plugin/gateway/gateway.plugin";
import { DynamicForm } from "src/core-pippa/pippa/core/dynamic/form/dynamic.form";

@Directive()
export class BaseFormDocumentPage extends BasePage
{
    @ViewChild("content",      { static : true }) content    : IonContent;
	@ViewChild("dynamicForm", { static : true }) dynamicForm : DynamicForm;
    @ViewChild("footer", 	  { read : ElementRef }) public footer : ElementRef;

    constructor(
        public documentPlugin    : DocumentPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }
	
    getTitle()
    {
        if (this.form)
        {
            return this.form.name;
        }		
    }

    /* METODO PARA SER SOBSCRITO */
    getDataModel()
    {
        return Document;
    }

    async initialize(isInitialize:boolean)
    {
        isInitialize;
		
        /* VERIFICAR SE O USUARIO PODE ADICIONAR DOCUMENTO PELO MAXIMO */
        const maxUserDocuments = this.acl.getKey("maxUserDocuments", 0);
        const showResult       = this.acl.getKey("showResult", false);
        const editResult       = this.acl.getKey("editResult", false);

        /*if(showResult)
		{
			showResult = await new Where(showResult).on();
			showResult = await showResult.parseData();			
		}*/

        if (this.core().user && maxUserDocuments > 0)
        {
            const params = this.createParams({
                appid : this.app.code,
                where : [ {
                    field    : "owner",
                    operator : "==",
                    value    : this.core().user.reference,
                } ]
            });

            const result = await this.documentPlugin.getData(params);
			
            if (maxUserDocuments <= result.collection.length)
            {				
                //if(showResult && showResult.value)? PQ
                if (showResult)
                {
                    this.push("viewer-document", {
                        documentPath : result.collection[0].reference.path,
                        appid     		 : this.app.code,
                    });
                }
                else if (editResult)
                {
                    await this.getForm();

                    this.push("form-document", {
                        formPath : result.collection[0].form.path,
                        dataPath : result.collection[0].reference.path,
                        appid  	 : this.app.code,
                    });
                }
                else
                {
                    this.destroyForm();
                    this.showAlert("Atenção", "Você já preencheu esse formulário! Em breve você terá acesso ao resultado!");
                }                    
            }
            else
            {
                await this.getForm();
            }
        }
        else
        {
            await this.getForm();
        }
		
        /* TRACK */
        /*if(this.form && this.form._initTrack)
		{
			this.core().track(this.form._initTrack);
		}*/
    }

    ionViewDidLeave()
    {
        if (this.dynamicForm)
        {
            this.dynamicForm.destroy();	
        }   
		
        super.ionViewDidLeave();
    }

    async getForm()
    {
        /* SE O FORM NÃO FOR PASSO POR PARAMETRO */
        if (!this.form)
        {
            const formPlugin = this.core().injector.get(FormPlugin);

            /* LOAD FORM */
            const params = this.createParams({
                appid : this.app.code,
            });

            const result = await formPlugin.getData(params);
        
            if (result.collection.length > 0)
            {
                this.form = result.collection[0];
            }

            this.changeDetectorRef.markForCheck();
        }
        else
        {
            this.changeDetectorRef.markForCheck();
        }
    }

    async onAdd(event:any)
    {
        const result = await this.plugin.add(this.app.code, this.form, event);

        /* TRACK */
        /*if(this.form._track)
		{
			this.core().track(this.form._track);
		}*/

        if (this.form.stepSave)
        {
            this.data = result.data;

            this.core().navController.router.navigate([], 
                {
                    relativeTo : this.activatedRoute,
                    queryParams : 
				{
				    dataPath : this.data.reference.path
				},
                    queryParamsHandling : "merge",
                });
        }
        else
        {
            await this.doAction(result.data, FormType.ADD_MODE, this.form);
        }  
    }
	
    displayForm()
    {
        if (this.dynamicForm)
        {
            this.dynamicForm.display();		
        }       
    }

    destroyForm()
    {
        if (this.dynamicForm)
        {
            this.dynamicForm.destroy();		
        }       
    }

    onRendererComplete(event)
    {   
        const childElements = this.footer.nativeElement.childNodes;

        for (const child of childElements) 
        {
            this.footer.nativeElement.removeChild(child);
        }
		
        if (event.footerViewer)
        {
            this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
        }        
    }

    async onSet(event:any)
    {
        const result = await this.plugin.set(this.app.code, this.data, this.form, event);
		
        if (this.form.stepSave)
        {
            if ((this.core().user && this.core().user.isGuest()) || (result.data._steps && result.data._steps.completeAll ))
            {
                await this.doAction(result.data, FormType.SET_MODE, this.form);
            }
        }
        else
        {
            await this.doAction(result.data, FormType.SET_MODE, this.form);
        } 				
    }

    async doAction(data:any, mode:any, form:any)
    {
        console.log("actions", form.actions);

        let hasAction = false;

        if (form.actions && form.actions.length > 0)
        {
            for (const key in form.actions)
            {
                const item = form.actions[key];

                if (item.mode.value == mode.value && this.core().user && this.core().user.hasGroups(item.groups))
                {
                    hasAction = true;

                    if (item.action.value == FormType.ALERT_ACTION.value)
                    {
                        if (!item.callback || item.callback.value == FormType.CLOSE_CALLBACK.value)
                        {
                            this.destroyForm();
							
                        }
                        else if (item.callback.value == FormType.CLEAR_CALLBACK.value)
                        {
                            this.displayForm();
                        }						
                        else if (item.callback.value == FormType.BACK_CALLBACK.value)
                        {
                            this.onCancel();
                        }

                        this.showAlert("", item.message);
                    }
                    else if (item.action.value == FormType.REDIRECT_ACTION.value)
                    {
                        item.params.documentPath = data.reference.path;
                        this.push(item.router, item.params);
                    }
                    else if (item.action.value == FormType.PAYMENT_ACTION.value)
                    {
                        const gatewayPlugin = this.core().injector.get(GatewayPlugin);
                        await gatewayPlugin.createPayment(data);

                        item.params.documentPath = data.reference.path;
                        this.push(item.router, item.params);
                    }
                }
            }
        }

        if (!hasAction)
        {
            this.onCancel();
        }
    }

    async onCancel()
    {
        this.back();
    }
}
