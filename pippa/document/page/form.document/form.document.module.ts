import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }       from "../../../core";
import { FormDocument }     from "../../component/form.document/form.document";
import { FormDocumentPage } from "./form.document.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : FormDocumentPage } ])
    ],
    declarations : [
        FormDocumentPage,
        FormDocument,
    ],
})
export class FormDocumentModule 
{}
