import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { IonContent } from "@ionic/angular";

/* PIPPA */
import { BasePage }        from "../../../core/base/page/base.page";
import { ViewerPlugin }    from "../../../core/plugin/viewer/viewer.plugin";
import { PrintPlugin }     from "../../../core/plugin/print/print.plugin";
import { FieldType }       from "../../../core/type/field/field.type";
import { Document } 	   from "../../../core/model/document/document";

@Component({
    selector        : "viewer-log-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_viewer';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" class="back-button" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    <ion-icon [name]="app?.icon?.value"></ion-icon>
                                    {{document?.name}}
                                </ion-title>
                                <ion-buttons slot="end">
                                    <ion-button ion-button icon-only
                                                *ngIf="hasSetButton()"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Editar" hide-delay="0" placement="bottom"
                                                (click)="onSet()">
                                        <ion-icon name="create-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only
                                                *ifAcl="'print_thermal_document';acl:acl;value:false;"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Imprimir" hide-delay="0" placement="bottom"
                                                (click)="onPrintThermal()">
                                        <ion-icon name="print-outline"></ion-icon>
									</ion-button>
									<ion-button ion-button icon-only
                                                *ifAcl="'print_document';acl:acl;"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Imprimir" hide-delay="0" placement="bottom"
                                                (click)="onPrint()">
                                        <ion-icon name="print-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding" shadowCss=".scroll-x, .scroll-y{overscroll-behavior: auto !important;}">
                            <div viewer-log
                                 id="vieweLog"
                                 [app]="app"
                                 [acl]="acl"
                                 [viewer]="viewer"
                                 [data]="dataset">
                            </div>
                        </ion-content>`,
})
export class ViewerLogPage extends BasePage
{
    @ViewChild(IonContent, { static : false }) content : IonContent;

    public viewerPlugin : any;
    public printPlugin  : any;
    public original 	: any;
    public dataset 		: any;

    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
		
        this.viewerPlugin = this.core().injector.get(ViewerPlugin);
        this.printPlugin  = this.core().injector.get(PrintPlugin);
    }

    async initialize()
    {
        if (this.viewer)
        {
            this._loadDocument();
        }
        else
        {
            this.core().currentContent = this.content;

            let appid = this.app.code;
	
            /* O APPID DO DATA ALTERA O APP */
            if (this.document && this.document.appid)
            {
                appid = this.document.appid;
            }			

            this.original = new Document(this.document.document);
            await this.original.on();
	
            /* LOAD VIEWER */
            const params = this.createParams({
                appid : appid,
                where : [ {
                    field : "form",
                    value : this.original.form.reference,
                } ],
                orderBy : "name",
            });
	
            const groups = [ {
                id    : "all",
                label : "Todos",
                value : "all",
            } ];

            if (this.core().user)
            {
                groups.push(this.core().user.getGroup());
            }

            params.where.set({
                field    : "groups",
                operator : "array-contains-any",
                value    : groups,
                type   	 : FieldType.type("MultiSelect"),
            });

            this.viewerPlugin.getData(params).then((result:any) =>
            {
                /* VIEWER */
                if (result.collection.length > 0)
                {
                    const viewer = result.collection[0];                
	
                    viewer.on(true).then(() =>
                    {
                        this.viewer = viewer;
	
                        this._loadDocument();
                    });
                }
                else
                {
                    console.error("viewer not found");
                }
            });	
        }        
    }

    async _loadDocument()
    {
        if (this.document)
        {
            this.dataset      = new Document(this.document.dataset)
            this.dataset.form = this.original.form
            await this.dataset.on();

            this.changeDetectorRef.markForCheck();
        }
        else
        {
            this.changeDetectorRef.markForCheck();
        }
    }

    hasSetButton()
    {
        return this.document && !this.document.isLock() && !this.document.isArchive() && !this.document.isCanceled()
        && (
            (this.hasAcl("owner_edit_viewer_document") && this.document ? this.document && this.document.isOwner() : false) ||
            this.hasAcl("edit_viewer_document")
        );
    }

    onSet()
    {
        this.push("form-document",
            {
                dataPath : this.document.reference.path,
            });
    }

    onPrintThermal()
    {
        if (this.viewer && this.document)
        {
            this.printPlugin.printThermalViewer(this.viewer, this.document);
        }
    }
	
    onPrint()
    {
        window.print();

        /*var node = document.getElementById('viewerDocument');

		const data : any = node;
		const html = sanitizeHtml(data.innerHTML, {
			allowedTags: [ 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
			'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
			'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe'],
			allowedAttributes: {
			a: [ 'href', 'name', 'target' ],
			// We don't currently allow img itself by default, but this
			// would make sense if we did. You could add srcset here,
			// and if you do the URL is checked for safety
			img: [ 'src' ]
			},
			// Lots of these won't come up by default because we don't allow them
			selfClosing: [ 'img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta' ],
			// URL schemes we permit
			allowedSchemes: [ 'http', 'https', 'ftp', 'mailto' ],
			allowedSchemesByTag: {},
			allowedSchemesAppliedToAttributes: [ 'href', 'src', 'cite' ],
			allowProtocolRelative: true
		 });

		 console.log(html);

		printJS({
			type: "raw-html",
			printable: html
		})*/
    }
}
