import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }    from "../../../core";
import { ViewerLog }     from "../../component/viewer.log/viewer.log";
import { ViewerLogPage } from "./viewer.log.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ViewerLogPage } ])
    ],
    declarations : [
        ViewerLogPage,
        ViewerLog,
    ],
})
export class ViewerLogModule 
{}
