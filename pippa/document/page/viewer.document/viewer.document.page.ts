import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseViewerDocumentPage } from "./base.viewer.document.page";

@Component({
    selector        : "viewer-document-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header *ifAcl="'show_header_viewer';acl:acl;value:true;">
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" class="back-button" ion-button icon-only (click)="back()">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{getTitle()}}
                                </ion-title>
								<ion-buttons slot="end">
									<ion-button ion-button icon-only
												*ifAcl="'log_document';acl:acl;"
												hideDelay="0" showDelay="0" 
												matTooltip="Logs" hide-delay="0" placement="bottom"
												(click)="onLogs()">
										<ion-icon name="server-outline"></ion-icon>
									</ion-button>
                                    <ion-button ion-button icon-only
                                                *ngIf="hasSetButton()"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Editar" hide-delay="0" placement="bottom"
                                                (click)="onSet()">
                                        <ion-icon name="create-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button ion-button icon-only
                                                *ifAcl="'print_thermal_document';acl:acl;value:false;"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Imprimir" hide-delay="0" placement="bottom"
                                                (click)="onPrintThermal()">
                                        <ion-icon name="print-outline"></ion-icon>
									</ion-button>
									<ion-button ion-button icon-only
                                                *ifAcl="'print_document';acl:acl;"
                                                hideDelay="0" showDelay="0" 
                                                matTooltip="Imprimir" hide-delay="0" placement="bottom"
                                                (click)="onPrint()">
                                        <ion-icon name="print-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding" shadowCss=".scroll-x, .scroll-y{overscroll-behavior: auto !important;}">
                            <div viewer-document
                                 #viewerDocument
                                 [app]="app"
                                 [acl]="acl"
                                 [viewer]="viewer"
                                 [data]="document">
                            </div>
                        </ion-content>`,
})
export class ViewerDocumentPage extends BaseViewerDocumentPage
{
    constructor(        
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);		
    }
}
