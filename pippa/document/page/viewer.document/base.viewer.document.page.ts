import { Component, ViewChild, Directive, ChangeDetectorRef, HostBinding } from "@angular/core";
import { IonContent } from "@ionic/angular";
import { PopoverController, ModalController } from "@ionic/angular";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { ViewerPlugin }   from "../../../core/plugin/viewer/viewer.plugin";
import { GridPopover }    from "../../../core/popover/grid/grid.popover";
import { ViewerModal }    from "../../../core/modal/viewer/viewer.modal";
import { PrintPlugin }    from "../../../core/plugin/print/print.plugin";
import { LogPlugin }      from "../../../core/plugin/log/log.plugin";
import { FieldType }      from "../../../core/type/field/field.type";
import { Grid }      	  from "../../../core/model/grid/grid";
import { Document } 	  from "../../../core/model/document/document";
import { ViewerDocument } from "../../component/viewer.document/viewer.document";

@Directive()
export class BaseViewerDocumentPage extends BasePage
{
@ViewChild(IonContent, { static : false }) content : IonContent;
@ViewChild("viewerDocument", { static : true }) viewerDocument : ViewerDocument;

public viewerPlugin : any;
public printPlugin  : any;
public logPlugin    : any;

constructor(        
public changeDetectorRef : ChangeDetectorRef
)
{
    super();

    this.viewerPlugin = this.core().injector.get(ViewerPlugin);
    this.printPlugin  = this.core().injector.get(PrintPlugin);
    this.logPlugin    = this.core().injector.get(LogPlugin);
}

getTitle()
{
    return this.viewer.name;
}

initialize()
{
    if (this.viewer)
    {
        this._loadDocument();
    }
    else
    {
        this.core().currentContent = this.content;

        let appid = this.app.code;

        /* O APPID DO DATA ALTERA O APP */
        if (this.document && this.document.appid)
        {
            appid = this.document.appid;
        }

        /* LOAD VIEWER */
        const params = this.createParams({
            appid : appid,
            where : [ {
                field : "form",
                value : this.document.form,
                type  : FieldType.type("ReferenceSelect"),
            } ],
            orderBy : "name",
        });

        const groups = [ {
            id    : "all",
            label : "Todos",
            value : "all",
        } ];

        if (this.core().user)
        {
            groups.push(this.core().user.getGroup());
        }

        params.where.set({
            field    : "groups",
            operator : "array-contains-any",
            value    : groups,
            type   	 : FieldType.type("MultiSelect"),
        });

        this.viewerPlugin.getData(params).then((result:any) =>
        {
            /* VIEWER */
            if (result.collection.length > 0)
            {
                const viewer = result.collection[0];                

                viewer.on(true).then(() =>
                {
                    this.viewer = viewer;

                    this._loadDocument();
                });
            }
            else
            {
                console.error("viewer not found");
            }
        });	
    }        
}

_loadDocument()
{
    if (!this.document)
    {
        const params = this.createParams({
            appid : this.app.code,
        });

        this.plugin.getData(params).then((result:any) =>
        {
            if (result.collection.length > 0)
            {
                this.document = result.collection[0];
                this.changeDetectorRef.markForCheck();
            }
        });
    }
    else
    {
        this.changeDetectorRef.markForCheck();
    }
}

hasSetButton()
{
    return this.document && !this.document.isLock() && !this.document.isArchive() && !this.document.isCanceled()
&& (
    (this.hasAcl("owner_edit_viewer_document") && this.document ? this.document && this.document.isOwner() : false) ||
this.hasAcl("edit_viewer_document")
);
}

onSet()
{
    this.push("form-document",
        {
            dataPath : this.document.reference.path,
        });
}

onPrintThermal()
{
    if (this.viewer && this.document)
    {
        this.printPlugin.printThermalViewer(this.viewer, this.document);
    }
}

async onLogs()
{
    const popoverController = this.core().injector.get(PopoverController);

    const params = this.createParams({
        appid : this.app.code,
        where : [ {
            field : "document",
            value : this.document.reference,
        } ]
    });

    const result = await this.logPlugin.getData(params);

    const grid = new Grid({
        hasSetButton    : false,
        hasDelButton    : false,
        hasViewerButton : true,
        items           : [
            {
                field : {
                    label   : "Data da Criação",
                    name    : "postdate",
                    type    : FieldType.type("Date"),
                    setting : {
                        hasTime : true
                    }
                }
            },
            {
                field : {
                    label : "Tipo",
                    name  : "type",
                    type  : FieldType.type("Select"),
                }
            },
            {
                field : {
                    label : "Usuário",
                    name  : "owner",
                    type  : FieldType.type("ReferenceSelect"),
                }
            },
        ],
    });

    popoverController.create(
        {
            component : GridPopover,
            componentProps :
{
    title      : "Logs",
    grid     		: grid,
    collection : result.collection,
    onViewer  	: ((event:any) =>
    {
        this.onViewer(event.data);				
    }),
    onClose : () =>
    {
        popoverController.dismiss();
    }
},
            showBackdrop : false,
            cssClass     : "right-popover",
        })
.then(popover =>
{
    popover.present();
});
}

async onViewer(document:any)
{
    const original : any = new Document(document.document);
    await original.on();

    const dataset : any = new Document(document.dataset)
    dataset.appid       = original.appid;
    dataset.form        = original.form;
    await dataset.on();

    console.log("dataset", dataset)

    const modalController = this.core().injector.get(ModalController);

    //let viewer = result.collection[0];                
    //await viewer.on(true);

    modalController.create(
        {
            component : ViewerModal,
            componentProps :
{
    title    : "Logs",
    //viewer   : viewer,
    document : dataset,
    onClose  : () =>
    {
        modalController.dismiss();
    }
},
        })
.then(modal =>
{
    modal.present();
});

    /* LOAD VIEWER */
    /*const params = this.createParams({
appid : this.app.code,
where : [{
field : 'form',
value :  original.form.reference,
}],
orderBy : 'name',
});

const groups = [{
id    : 'all',
label : 'Todos',
value : 'all',
}];

if(this.core().user)
{
groups.push(this.core().user.getGroup());
}

params.where.set({
field    : 'groups',
operator : 'array-contains-any',
value    : groups,
type 	 : FieldType.type('MultiSelect'),
});

this.viewerPlugin.getData(params).then(async (result:any) =>
{
/* VIEWER */
/*if(result.collection.length > 0)
{

}
else
{
console.error('viewer not found');
}
});	*/
}

ionViewWillLeave()
{
    if (this.viewerDocument)
    {
        this.viewerDocument.destroy();
    }			

    this.ngOnDestroy();
}

onPrint()
{
    window.print();

/*var node = document.getElementById('viewerDocument');

const data : any = node;
const html = sanitizeHtml(data.innerHTML, {
allowedTags: [ 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe'],
allowedAttributes: {
a: [ 'href', 'name', 'target' ],
// We don't currently allow img itself by default, but this
// would make sense if we did. You could add srcset here,
// and if you do the URL is checked for safety
img: [ 'src' ]
},
// Lots of these won't come up by default because we don't allow them
selfClosing: [ 'img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta' ],
// URL schemes we permit
allowedSchemes: [ 'http', 'https', 'ftp', 'mailto' ],
allowedSchemesByTag: {},
allowedSchemesAppliedToAttributes: [ 'href', 'src', 'cite' ],
allowProtocolRelative: true
});

console.log(html);

printJS({
type: "raw-html",
printable: html
})*/
}
}
