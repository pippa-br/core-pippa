import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }         from "../../../core";
import { ViewerDocument }     from "../../component/viewer.document/viewer.document";
import { ViewerDocumentPage } from "./viewer.document.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ViewerDocumentPage } ])
    ],
    declarations : [
        ViewerDocumentPage,
        ViewerDocument,
    ],
})
export class ViewerDocumentModule 
{}
