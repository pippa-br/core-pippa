import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { AlertController } from "@ionic/angular";

/* PIPPA */
import { BasePage }       from "../../../core/base/page/base.page";
import { Document }       from "../../../core/model/document/document";
import { ImportDocument } from "../../component/import.document/import.document";

@Component({
    selector        : "indexer-document-page",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-header>
                            <ion-toolbar>
                                <ion-buttons slot="start">
                                    <ion-menu-toggle>
                                        <ion-menu-button></ion-menu-button>
                                    </ion-menu-toggle>
                                    <ion-button *ngIf="hasBack()" ion-button icon-only (click)="back()" class="my-style-for-modal">
                                        <ion-icon name="arrow-back-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
                                <ion-title>
                                    {{app?.name}}
                                </ion-title>
                                <ion-buttons slot="end" (click)="onClose()">
                                <ion-button icon-only>
                                    <ion-icon name="close-outline"></ion-icon>
                                </ion-button>
                                </ion-buttons>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content scroll-x="true" scroll-events="true">

                            <div import-document
                                 #importDocument
                                 [app]="app"
                                 [form]="form">
                            </div>

                        </ion-content>
						<ion-footer>
							<ion-toolbar [ngClass]="{'hide' : !importDocument || !importDocument.grid}">
								<ion-button type="button"
											class="cancel-button" ion-button outline
											(click)="onClear()">
									<ion-icon name="close-circle-outline"></ion-icon>&nbsp;&nbsp;Limpar
								</ion-button>
								<ion-button type="button"
											class="save-button" ion-button outline
											(click)="onSave()">
									<ion-icon name="add-circle-outline"></ion-icon>&nbsp;&nbsp;Importar ({{count}} / {{this.importDocument?.collection?.length}})
								</ion-button>
							</ion-toolbar>
                        </ion-footer>`,
})
export class ImportDocumentPage extends BasePage
{
    @ViewChild("importDocument", { static : true }) importDocument : ImportDocument;

    public alertController : AlertController;
    public count             = 0;
    public subscription    : any;

    constructor(
		public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();

        this.alertController = this.core().injector.get(AlertController);
    }

    onSave()
    {
        this.alertController.create({
            header  : "Alerta",
            message : ("Deseja importar esses dados agora?"),
            buttons : [
                {
                    text    : "Não",
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text    : "Sim",
                    handler : () =>
                    {
                        this.importCollection();
                        this.alertController.dismiss();
                        return false;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        });
    }

    importCollection()
    {
        this.core().loadingController.start().then(async () =>
        {
            this.count       = 0;
            const perPage    = 100;
            const totalPages = Math.ceil(this.importDocument.collection.length / perPage);

            for (let page = 1; page <= totalPages; page++)
            {
                const items = this.importDocument.collection.slice((page - 1) * perPage, (page * perPage));
                //console.error(page, items);

                await this.plugin.adds(this.app.code, this.form, { data : items })
                this.count += perPage;
                this.changeDetectorRef.markForCheck();
                //break;
            }

            this.core().loadingController.stop()
            this.onClear();
        });
    }

    importCollection2()
    {
        this.core().loadingController.start().then(() =>
        {
            this.count = 0;

            /* ITERATOR */
            this.subscription = this.importDocument.collection.iterator().subscribe(async (item:any) =>
            {
                // MODO SET
                if (item.key)
                {
                    const document = new Document(item.key.reference);
                    delete item.key;

                    const data = item.parseData();

                    await document.set(data);

                    console.log("iterator set", document);	
                }
                else // MODO ADD
                {
                    await this.plugin.add(this.app.code, this.form, { data : item.parseData() })
                
                    console.log("iterator add", item);	
                }

                this.importDocument.collection.nextIterator().then((status:boolean) =>
                {
                    this.count++;
                    this.changeDetectorRef.markForCheck();

                    if (!status)
                    {
                        this.core().loadingController.stop()
                        this.onClear();
                    }
                });                
            });
        });
    }
	
    onClear()
    {
        this.count = 0;

        if (this.subscription)
        {
            this.subscription.unsubscribe();
        }

        this.importDocument.clear();
    }


    onClose()
    {
        this.back();
    }
}
