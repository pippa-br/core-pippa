import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }         from "../../../core";
import { ImportDocument }     from "../../component/import.document/import.document";
import { ImportDocumentPage } from "./import.document.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ImportDocumentPage } ])
    ],
    declarations : [
        ImportDocumentPage,
        ImportDocument,
    ],
})
export class ImportDocumentModule
{
}
