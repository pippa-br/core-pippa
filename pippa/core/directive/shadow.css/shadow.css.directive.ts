import { Directive, ElementRef, Input, OnChanges, OnInit } from '@angular/core';

/* PIPPA */
import { BaseDirective } from '../../../core/directive/base.directive';

@Directive({
    selector : '[shadowCss]'
})
export class ShadowCssDirective extends BaseDirective implements OnChanges, OnInit
{
	@Input() shadowCss: string;
	
	constructor(
        public el: ElementRef
    )
    {
        super();
    }

    ngOnChanges()
    {
		const shadow = this.el.nativeElement.shadowRoot || this.el.nativeElement.attachShadow({ mode: 'open' });
		
        if(shadow)
        {
            let innerHTML 	  = '';
            innerHTML		 += '<style>';
            innerHTML 		 += this.shadowCss;
            innerHTML 		 += '</style>';
            shadow.innerHTML += innerHTML;
		}
	}

	ngOnInit()
	{
		this.ngOnChanges();
	}
}
