import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/* APPP */
import { Core } from '../util/core/core';

export class BaseDirective
{
    constructor()
    {
    }

    core()
    {
        return Core.get();
    }

    api()
    {
        return Core.get().api;
    }
}
