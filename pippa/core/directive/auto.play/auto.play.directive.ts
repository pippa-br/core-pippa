import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector : '[auto-play]'
})
export class AutoPlayDirective implements OnInit
{
    constructor(public element: ElementRef)
    {
    }

    public ngOnInit(): void
    {
        console.log('video autoplay');

        let vid   = this.element.nativeElement;
        vid.muted = true;
        vid.play();
    }
}
