import {Directive, ElementRef, Input, OnChanges, Renderer2} from "@angular/core";

@Directive({
    selector : '[hidden]'
})
export class HiddenDirective implements OnChanges
{
    @Input() hidden: boolean;

    constructor(private renderer: Renderer2, private elRef: ElementRef)
    {
    }

    ngOnChanges()
    {
        if(this.hidden)
        {
            this.renderer.setStyle(this.elRef.nativeElement, 'display', 'none');
        }
        else
        {
            this.renderer.setStyle(this.elRef.nativeElement, 'display', 'block');
        }
    }
}
