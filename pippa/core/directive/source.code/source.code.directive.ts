import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';

/* PIPPA */
import { BaseDirective } from '../../../core/directive/base.directive';

@Directive({
    selector : '[source-code]'
})
export class SourceCodeDirective extends BaseDirective
{
    constructor(
		public elementRef: ElementRef
	)
    {
		super();
        this.setTimer(); //somehow lastElem value is available on next tick		
    }

    setTimer()
    {
        setTimeout(() =>
        {
            this.core().util.makeClickHtml(this.elementRef);			
        }, 50);
    }
}
