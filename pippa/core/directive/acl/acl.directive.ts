import { Directive, Input, TemplateRef, ViewContainerRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

/* PIPPA */
import { BaseDirective } from '../../../core/directive/base.directive';

@Directive({
    selector : '[ifAcl]'
})
export class AclDirective extends BaseDirective implements OnDestroy
{
    private subscriptions : Subscription = new Subscription();
    private _name         : string;
    private _acl          : any;
    private _value        : any;

    constructor(
        public templateRef   : TemplateRef<any>,
        public viewContainer : ViewContainerRef,
    )
    {
        super();

        this.enabled = false;
    }

    set enabled(value:boolean)
    {
        this.viewContainer.clear();

        if(value)
        {
            this.viewContainer.createEmbeddedView(this.templateRef);
        }
    }

    @Input()
    set ifAcl(name:string)
    {
        this._name = name;
        this.display();
    }

    @Input()
    set ifAclAcl(acl:any)
    {
        this._acl = acl;

        this.display();
    }

    @Input()
    set ifAclValue(value:any)
    {
        this._value = value;

        this.display();
    }

    display()
    {
        if(this._acl && this._value != undefined)
        {
            this.enabled = this._acl.hasKey(this._name, this._value);
        }
        else if(this._acl)
        {
            this.enabled = this._acl.hasKey(this._name);
        }
        else
        {
            this.enabled = false;
        }
    }

    ngOnDestroy()
    {
        if(this.subscriptions)
        {
            this.subscriptions.unsubscribe();
        }
    }
}
