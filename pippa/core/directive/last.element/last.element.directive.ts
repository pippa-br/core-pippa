import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';

/*
<p class="my-element"
  *ngFor="let i of stuff; let l = last"
  [lastElement]="l" (onLast)="test()">
    {{i}}
</p>*/

@Directive({
    selector : '[lastElement]'
})
export class LastElementDirective
{
    @Input()  lastElement : any;
    @Output() last = new EventEmitter();

    constructor(private el: ElementRef)
    {
        this.setTimer(); //somehow lastElem value is available on next tick
    }

    setTimer()
    {
        setTimeout(() =>
        {
            if(this.lastElement) this.last.emit();
        }, 50);
    }
}
