import { Directive, ElementRef, Input, HostBinding, OnDestroy } from '@angular/core';

@Directive({
	selector: '[backgroundSlider]'
})
export class BackgroundSliderDirective implements OnDestroy
{
	@HostBinding("style.position") position: String = "relative";
	@HostBinding("style.zIndex") zIndex: String = "0";
	
	public index: number = 0;
	public _slides: any;
	public proc;
	public mobile;

	constructor(public elementRef: ElementRef) 
	{
	}

	@Input()
	set backgroundSlider(slides: any) 
	{
		this._slides = slides || [];

		//CREATE ALL ELEMENTS     
		this.createElements();

		this.display();
	}

	get backgroundSlider() 
	{
		return this._slides;
	}

	display() 
	{
		this.mobile = window.matchMedia("(max-width:745px)").matches;

		if(this.backgroundSlider && this.backgroundSlider.length > 0) 
		{
			let currentSlide = this.backgroundSlider[this.index];
			const slideType = currentSlide.type || 'image';

			//CASES
			if (slideType == 'image') 
			{
				if(this.mobile) 
				{
					/* a viewport tem pelo menos 800 pixels de largura */
					currentSlide.element.style.maxWidth = "none";
					currentSlide.element.style.height = "-webkit-fill-available";
					currentSlide.element.style.width = "auto";
				}

				currentSlide.element.style.display = "block";
				currentSlide.element.style.animation = "fade " + currentSlide.time + "s 1";

				if (this.proc) 
				{
					clearTimeout(this.proc);
				}

				// Run function every x seconds  
				this.proc = setTimeout(() => 
				{
					currentSlide.element.style.display = "none";
					this.display()
				}, currentSlide.time * 1000);

				// Check If Index Is Under Max
				if (this.index < this.backgroundSlider.length - 1) 
				{
					// Add 1 to Index       
					this.index++;
				}
				else 
				{
					// Reset Back To O        
					this.index = 0;
				}
			}
			else if (slideType == 'video') 
			{
				if(this.mobile) 
				{
					/* a viewport tem pelo menos 800 pixels de largura */
					currentSlide.element.style.width = "auto";
					currentSlide.element.style.height = "-webkit-fill-available";
				}

				currentSlide.element.style.display = "block";				
				currentSlide.element.style.animation = "fade 3s 1";

				setTimeout(() => 
				{
					currentSlide.video.play();
				});

				if (this.proc) {
					clearTimeout(this.proc);
				}

				// Run function every x seconds  
				this.proc = setTimeout(() => {
					currentSlide.element.style.display = "none";
					this.display()
				}, currentSlide.time * 1000);

				// Check If Index Is Under Max
				if (this.index < this.backgroundSlider.length - 1) {
					// Add 1 to Index       
					this.index++;
				}
				else {
					// Reset Back To O        
					this.index = 0;
				}
			}
			else if (slideType == 'iframe') 
			{
				if(this.mobile) 
				{
					/* a viewport tem pelo menos 800 pixels de largura */
					currentSlide.element.style.width = "auto";
					currentSlide.element.style.height = "-webkit-fill-available";
				}

				currentSlide.element.style.display = "block";
				currentSlide.element.style.animation = "fade 3s 1";

				if (this.proc) {
					clearTimeout(this.proc);
				}

				// Run function every x seconds  
				this.proc = setTimeout(() => {
					currentSlide.element.style.display = "none";
					this.display()
				}, currentSlide.time * 1000);

				// Check If Index Is Under Max
				if (this.index < this.backgroundSlider.length - 1) {
					// Add 1 to Index       
					this.index++;
				}
				else {
					// Reset Back To O        
					this.index = 0;
				}
			}
		}
	}

	createElements() 
	{
		for (let i = 0; i < this.backgroundSlider.length; i++) 
		{
			const item = this.backgroundSlider[i];
			const type = item.type || 'image';			
			const filterColor = item.filterColor || 'white';
			const filterOpacity = item.filterOpacity || 0;
			item.time = item.time || 3;

			//CREATE A PARENT DIV TO INSERT THE MEDIA
			let div = document.createElement('div');
			div.style.position = "absolute";
			div.style.top = "0",
			div.style.left = "0",
			div.style.zIndex = "-1";
			div.style.width = "100%",
			div.style.height = "100%",        
			div.style.content = "",
			div.className = "background-login",
			this.elementRef.nativeElement.appendChild(div);
			this.backgroundSlider[i].element = div;      

			if (type == 'image') 
			{
				//CREATE THE FILTER
				let filter = document.createElement('div');
				filter.style.zIndex = "0";
				filter.style.width = "100vw";
				filter.style.height = "100vh";
				filter.style.background = filterColor;
				filter.style.filter = "opacity(" + filterOpacity/100  + ")";

				let image = document.createElement('div');
				image.style.backgroundImage = 'url("' + (this.mobile ? this.backgroundSlider[i]._mobileUrl : this.backgroundSlider[i]._url) + '")';
				image.style.backgroundSize = "cover";
				image.style.position = "fixed";				
				image.style.top = "0";
				image.style.left = "0";
				image.style.zIndex = "-1";
				image.style.width = "100vw";
				image.style.height = "100vh";
				//image.style.width = "-webkit-fill-available";
				div.appendChild(filter);
        		div.appendChild(image);        
				div.style.display = "none";
				
				console.log("time", item.time);
			}
			else if (type == 'video') 
			{
				//CREATE THE FILTER
				let filter = document.createElement('div');
				filter.style.zIndex = "0";
				filter.style.width = "100%";
				filter.style.height = "100%";
				filter.style.background = filterColor;
				filter.style.filter = "opacity(" + filterOpacity/100  + ")";

				let video = document.createElement('video');
				video.src = this.backgroundSlider[i]._url;
				video.style.position = "fixed";
				video.style.top = "0";
				video.style.left = "0";
				video.style.zIndex = "-1";
				video.style.width = "100%"
				video.setAttribute("type", "video/mp4")
				video.setAttribute("muted", "true");
				div.appendChild(filter);
       			div.appendChild(video); 
				div.style.display = "none"; 
				
				this.backgroundSlider[i].video = video;
			}
			else if (type == 'iframe') 
			{
				//CREATE THE FILTER
				let filter = document.createElement('div');
				filter.style.zIndex = "0";
				filter.style.width = "100%";
				filter.style.height = "100%";
				filter.style.background = filterColor;
				filter.style.filter = "opacity(" + filterOpacity/100  + ")";

				let iframe = document.createElement('iframe');
				iframe.src = 'https://www.youtube.com/embed/'  + this.backgroundSlider[i]._url + '?version=3&autoplay=1&showinfo=0&controls=0&rel=0&&mute=1&loop=1';
				iframe.style.position = "fixed";
				iframe.style.top = "0";
				iframe.style.left = "0";
				iframe.style.zIndex = "-1";
				iframe.style.width = "100%",
				iframe.style.height = "100%",
				iframe.setAttribute("frameborder", "0");
				iframe.setAttribute("muted", "true");
				iframe.setAttribute("allow", "autoplay");
				iframe.setAttribute("allowfullscreen", "true");
				div.appendChild(filter);
				div.appendChild(iframe); 
				div.style.display = "none";
			}
		}
	}

	ngOnDestroy()
    {
        console.log('ngOnDestroy');
        
		if(this.proc) 
		{
			clearTimeout(this.proc);
		}	
    }
}




