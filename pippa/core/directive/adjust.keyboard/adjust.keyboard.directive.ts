import { Directive, ElementRef } from '@angular/core';

/* PIPPA */
import { Core } from '../../util/core/core';

@Directive({
    selector: '[adjust-keyboard]' // Attribute selector
})
export class AdjustKeyboardDirective
{
    constructor(
        public elementRef: ElementRef
    )
    {

    }

    ngAfterViewInit()
    {
        if(Core.get().isApp)
        {
            window.addEventListener('keyboardDidShow', (e) =>
            {
                this.elementRef.nativeElement.style.marginBottom = (<CustomEvent>e).detail.keyboardHeight + 'px';

            });

            window.addEventListener('keyboardDidHide', () =>
            {
                this.elementRef.nativeElement.style.marginBottom = '0px';
            });
        }
    }
}
