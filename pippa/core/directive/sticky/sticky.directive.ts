import { Directive, ElementRef, Input, HostListener, Renderer2, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Directive({
  selector: '[sticky]'
})
export class StickyDirective implements OnInit
{
  	private sticked: boolean = true;
  	private selectedOffset: number = 0;
  	private windowOffsetTop: number = 0;

  	@Input() addClass : string = 'sticky';
	@Input() offSet   : number = 0;
	@Input() content  : any;

	constructor(private el: ElementRef, private render:Renderer2) 
	{
		this.selectedOffset = this.el.nativeElement.offsetTop;			
	}

	public ngOnInit(): void	
    {
		this.content.ionScroll.subscribe((event)=>
		{
			this.onWindowScroll()
		});
    }	  

	private addSticky() 
	{
    	this.sticked = true;
    	this.el.nativeElement.style.position = 'sticky';
    	this.el.nativeElement.style.top = this.offSet + 'px';
    	this.render.addClass(this.el.nativeElement, this.addClass);
  	}

	private removeSticky() 
	{
    	this.sticked = false;
    	this.el.nativeElement.style.position = '';
    	this.render.removeClass(this.el.nativeElement, this.addClass);
  	}

	async onWindowScroll() 
	{
		const scroll 		 = await this.content.getScrollElement();
      	let offset: number   = this.el.nativeElement.offsetTop;
		this.windowOffsetTop = scroll.scrollTop || 0;
		  
		if(this.selectedOffset === 0) 
		{
        	this.selectedOffset = offset;
      	}

		if (this.sticked === false) 
		{
        	this.selectedOffset = offset;
      	}

		if((this.windowOffsetTop + this.offSet) > this.selectedOffset) 
		{
        	this.addSticky();
		} 
		else 
		{
        	this.removeSticky();
      	}
    }
}