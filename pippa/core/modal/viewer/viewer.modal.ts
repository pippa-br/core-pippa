import { Component } from "@angular/core";

/* PIPPA */
import { BaseModal } 	   from "../../base/modal/base.modal";
import { ViewerPlugin }    from "../../../core/plugin/viewer/viewer.plugin";
import { FieldType } 	   from "../../type/field/field.type";
import { WhereCollection } from "../../model/where/where.collection";

@Component({
    selector : "viewer-modal",
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            {{viewer?.name || title}}
                        </ion-title>
                        <ion-buttons slot="end">
                            <ion-button ion-button icon-only (click)="onClose()">
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content class="ion-padding">	
									<div dynamic-viewer
										[viewer]="viewer"						 
										[data]="document"
										(reload)="onReload()">
									</div>
                </ion-content>`
})
export class ViewerModal extends BaseModal
{
    public viewer : any;

    loadDocument()
    {
        if (!this.viewer)
        {
            /* LOAD VIEWER */
            const params = this.createParams({
                appid   : this.document.appid,
                orderBy : "name",
            });

            const app = this.getApp(this.document.appid);

            if (!app.uniqueViewer)
            {
                params.where = new WhereCollection([ {
                    field : "form",
                    value : {
                        referencePath : this.document.form.referencePath ? this.document.form.referencePath : (this.document.form.reference ? this.document.form.reference.path : this.document.form.path)
                    },
                } ]);
            }

            const groups = [ {
                id    : "all",
                label : "Todos",
                value : "all",
            } ];

            if (this.core().user)
            {
                groups.push(this.core().user.getGroup());
            }

            params.where.set({
                field    : "groups",
                operator : "array-contains-any",
                value    : groups,
                type   	 : FieldType.type("MultiSelect"),
            });

            const viewerPlugin = this.core().injector.get(ViewerPlugin);

            viewerPlugin.getData(params).then((result:any) =>
            {
                /* VIEWER */
                if (result.collection.length > 0)
                {
                    const viewer = result.collection[0];                

                    viewer.on(true).then(() =>
                    {
                        this.viewer = viewer;
                    });
                }
                else
                {
                    console.error("viewer not found");
                }
            });
        }		
    }
}
