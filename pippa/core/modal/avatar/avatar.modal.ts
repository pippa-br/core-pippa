import { Component, AfterContentInit } from '@angular/core';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';

@Component({
    selector : 'avatar-modal',
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            Avatar
                        </ion-title>
                        <ion-buttons slot="end" (click)="onClose()">
                            <ion-button ion-button icon-only>
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>

                    <image-cropper [imageChangedEvent]="imageEvent"
								   [imageBase64]="imageBase64"
								   [maintainAspectRatio]="true"
                                   [aspectRatio]="aspectRatio"
                                   [resizeToWidth]="300"
								   [roundCropper]="roundCropper"
                                   [imageQuality]="50"
                                   format="png"
                                   (imageCropped)="imageCropped($event)">
                    </image-cropper>

                </ion-content>
                <ion-footer>
                    <ion-toolbar>
                        <ion-buttons slot="end" (click)="onSave(imageCroppedEvent)">
                            <ion-button ion-button>
                                <ion-icon name="checkmark-outline"></ion-icon>&nbsp;&nbsp;Salvar
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-footer>`
})
export class AvatarModal extends BaseModal implements AfterContentInit
{
    public imageEvent        : any;
	public imageBase64       : any;
    public imageCroppedEvent : any;
	public aspectRatio       : number = 1;
    public onSave            : any;
	public roundCropper      : boolean = false;

    constructor()
    {
        super();
		
        this.core().loadingController.start();
    }

    ngAfterContentInit()
    {
        this.core().loadingController.stop();
    }

    imageCropped(event: any)
    {
        console.log('imageCroppedEvent', event);

        this.imageCroppedEvent = event;
    }
}
