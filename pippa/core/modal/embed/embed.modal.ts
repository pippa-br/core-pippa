import { Component } from '@angular/core';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';

@Component({
    selector : 'embed-modal',
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-buttons slot="start">
                            <ion-button ion-button icon-only (click)="onClose()">
                                <ion-icon name="arrow-back-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                        <ion-title>
                            {{title}}
                        </ion-title>
                        <ion-buttons slot="end">
                            <ion-button ion-button icon-only (click)="onClose()">
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content >
                    <div *ngIf="embed" [innerHTML]="embed | safe"></div>
                </ion-content>`
})
export class EmbedModal extends BaseModal
{
    embed : string;
}
