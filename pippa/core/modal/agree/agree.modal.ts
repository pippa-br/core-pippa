import { Component, AfterContentInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthPlugin, EventPlugin } from '../..';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';

@Component({
    selector : 'agree-modal',
    template : ` <ion-content class="agree-page">

                    <ngx-extended-pdf-viewer
                                    [src]="url" 
                                    useBrowserLocale="true" 
                                    [mobileFriendlyZoom]="zoom"
                                    [language]="language"
                                    [showBorders]="false"
                                    [showToolbar]="false"
                                    [scrollMode]="0"
                                    textLayer="true">
                    </ngx-extended-pdf-viewer>	

                </ion-content>
                <ion-footer>
                    <div class="ion-toolbar">         
                        <ion-buttons slot="secondary">
                            <input id="agree"
                                class="checkbox-agree"
                                type="checkbox"
                                [(ngModel)]="selected">

                            <label for="agree">Li e concordo plenamente com os termos apresentados</label>                            
                        </ion-buttons>       
                        <ion-buttons slot="primary">
                            <ion-button class="agree-button" (click)="yesAgree()">
                                Aceitar Termos 
                            </ion-button>
                            <ion-button (click)="noAgree()">
                                Recursar Termos 
                            </ion-button>
                        </ion-buttons>                        
                    </div>
                </ion-footer>`
})
export class AgreeModal extends BaseModal implements AfterContentInit
{
    public authPlugin : any;
    public user       : any ;
    public selected   : boolean = false;
    public zoom       : string  = '100%';
	public language   : string  = 'pt-PT';
    public url        : string;

    ngAfterContentInit()
    {
        this.authPlugin = this.core().injector.get(AuthPlugin);
    }

    async yesAgree()
    {
        if(this.selected)
        {
            const params = this.createParams({
                accid : this.user.accid,
                appid : this.user.appid,
                colid : this.user.colid,
                document : {
                    referencePath : this.user.referencePath
                },
                // form : { USER ESTAVA SEM FORM POR CONTA DA MUNDA DE ACL
                //     referencePath : this.user.form.reference.path
                // },
                data  : {
                    agreeTerm : true
                },
            });

            await this.authPlugin.setUser(params);

            this.onClose();
        }
        else
        {
            this.showAlert('Atenção', 'Você precisa aceitar o termo para continuar!');   
        }
    }

    noAgree()
    {
		const eventPlugin = this.core().injector.get(EventPlugin);
        eventPlugin.dispatch('logout', this.core().user);
        eventPlugin.dispatch(this.core().user.appid + ':logout', this.core().user);	
    }
}
