import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';

@Component({
    selector : 'pdf-modal',
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            {{title}}
                        </ion-title>
                        <ion-buttons slot="end">
                            <ion-button ion-button icon-only (click)="onClose()">
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>
					<ngx-extended-pdf-viewer [src]="url" 
											 useBrowserLocale="true" 
											 [mobileFriendlyZoom]="zoom"
											 [language]="language"
											 textLayer="true"
											 (pdfLoaded)="onLoad()">
					</ngx-extended-pdf-viewer>
                </ion-content>`
})
export class PDFModal extends BaseModal
{
	public _url     : string;
    public loading  : any;
    public times    : number = 0;
	public zoom     : string = '100%';
	public language : string = 'pt-PT';

    constructor(
		public loadingController : LoadingController,
    )
    {
        super();
	}
	
	set url(value:string)
	{
		this._url = value;

		console.log('pdf', value);

		if(value)
		{
			this.presentModal();
		}
	}

	get url()
	{
		return this._url;
	}

	async presentModal()
	{
		this.loading = await this.loadingController.create({
			spinner         : 'dots',
			backdropDismiss : true,
		})
		
		this.loading.present();		
	}

	onLoad()
    {
        this.loading.dismiss();
    }
}
