import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';

/* PIPPA */
import { BaseModal }  from '../../base/modal/base.modal';

@Component({
    selector : 'gallery-modal',
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            {{title}}
                        </ion-title>
                        <ion-buttons slot="end" (click)="clickClose()">
                            <ion-button (click)="download()" *ngIf="hasDownload">
                                <ion-icon name="cloud-download-outline"></ion-icon>
                            </ion-button>
                            <ion-button ion-button icon-only>
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>

                    <swiper-container #slides pager="true" [options]="slideOpts" 
                                (ionSlideWillChange)="onIonSlideWillChange()"
                                (ionSlidesDidLoad)="onIonSlidesDidLoad()">
                        <swiper-slide *ngFor="let image of images;">
                            <img [src]="image._url"/>
                        </swiper-slide>
                    </swiper-container>

                    <a class="swiper-button-prev swiper-button-black" (click)="prevSlide()" *ngIf="!isBeginning"></a>
                    <a class="swiper-button-next swiper-button-black" (click)="nextSlide()" *ngIf="!isEnd"></a> 

                </ion-content>`
})
export class GalleryModal extends BaseModal implements OnInit
{
    @ViewChild('slides') slides : ElementRef | undefined;

	public slideID      : number  = 0;
    public hasDownload  : boolean = false;
    public isBeginning  : boolean = true;
    public isEnd        : boolean = true;
    public initialSlide : number  = 0;
    public _images      : any;
    public slideOpts    : any = {
        speed      : 400,
		autoHeight : true,
		autoplay: {
			delay: 3000,
		},
    };

    ngOnInit()
    {
        /* IFRAME */
        if(window.parent)
        {                    
            window.parent.postMessage({
				type   : 'adjust',
				target : this.slideID,
			}, '*');
        } 
    }

    set images(value:any)
    {
        this._images = value;

        if(value)
        {
            this.slides?.nativeElement.swiper.update();
        }
    }

    get images()
    {
        return this._images;
    }

    nextSlide() 
    {
        this.slides?.nativeElement.swiper.slideNext();
    }
    
    prevSlide() 
    {
        this.slides?.nativeElement.swiper.slidePrev();
    }

    onIonSlideWillChange()
    {
        this.slides?.nativeElement.swiper.isBeginning().then((value) => 
        {
            this.isBeginning = value;
        })

        this.slides?.nativeElement.swiper.isEnd().then((value) => 
        {
            this.isEnd = value;
        })
    }

    onIonSlidesDidLoad()
    {
        if(this.slides)
        {
            this.slides?.nativeElement.swiper.slideTo(this.initialSlide);
        }
    }

    download()
    {
        this.core().util.download(this.images);
    }

    clickClose()
    {
        /* IFRAME */
        if(window.parent)
        {                    
            window.parent.postMessage({type : 'back' }, '*');
        } 

        this.onClose();
    }
}
