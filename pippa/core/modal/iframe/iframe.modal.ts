import { Component } from '@angular/core';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';

@Component({
    selector : 'iframe-modal',
    template : `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            {{title}}
                        </ion-title>
                        <ion-buttons slot="end">
                            <ion-button ion-button icon-only (click)="onClose()">
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>
                    <iframe class="e2e-iframe-untrusted-src" [src]="url | safe : 'resourceUrl'"></iframe>
                </ion-content>`
})
export class IframeModal extends BaseModal
{
    public url : string;

    constructor(
    )
    {
        super();
    }
}
