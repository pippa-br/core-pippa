import { Component, AfterContentInit } from '@angular/core';

/* PIPPA */
import { BaseModal } from '../../base/modal/base.modal';
import { File } from '../../model/file/file';

@Component({
    selector: 'coment-image-modal',
    template: `<ion-header>
                    <ion-toolbar>
                        <ion-title>
                            Adicionar Comentário
                        </ion-title>
                        <ion-buttons slot="end" (click)="_onClose()">
                            <ion-button ion-button icon-only>
                                <ion-icon name="close-outline"></ion-icon>
                            </ion-button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <div class="bg-dark">
                    <div class="ion-text-center">
                        <img class="img-coment" [src]="file.url" [alt]="file.name">
                    </div>
                    <div class="commentary-fixed">
                        <input type="text" focus [(ngModel)]="file.commentary" placeholder="Comentário" name="commentary" />
                            <button ion-button icon-only  (click)="_onSave()">
                                <ion-icon name="checkmark-outline"></ion-icon>
                            </button>
                    </div>
                </div>`
})
export class ComentImageModal extends BaseModal
{
    public _file   : any;

	public onSave  : any;
	public onClose : any;

	set file(value:any)
	{
		if(!(value instanceof File))
		{
			value = new File(value);
		}

		this._file = value;
	}

	get file()
	{
		return this._file;
	}

	_onSave() 
	{
		this.onSave(this.file);
    }

	_onClose() 
	{
		if (this.onClose) 
		{
            this.onClose();
        }
    }
}
