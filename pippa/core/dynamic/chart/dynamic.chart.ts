import { Component, Input, ElementRef, Renderer2, ViewContainerRef, ChangeDetectionStrategy, ViewChild, EventEmitter, Output, ChangeDetectorRef } from "@angular/core";
import { Subscription } from "rxjs";
import * as Unbind from "element-resize-event";

/* PIPPA */
import { Chart } 		     from "../../model/chart/chart";
import { PieChartContainer } from "./container/pie/pie.chart.container";
import { BarChartContainer } from "./container/bar/bar.chart.container";
import { Core }              from "../../util/core/core";
import { LineChartContainer } from "./container/line/line.chart.container";
import { PaginationType } from "../../type/pagination/pagination.type";
import { ChartPlugin } from "../../plugin/chart/chart.plugin";
import { CardChartContainer } from "./container/card/card.chart.container";

@Component({
    selector      		: "[dynamic-chart]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div [hidden]="_isRendererComplete" class="progress-bar">
												<ion-progress-bar color="medium" type="indeterminate"></ion-progress-bar>
												<span>Carregando...</span>
												</div>
												<div [hidden]="!_isRendererComplete">
												<div *ngIf="chart && chart.description" class="info" [innerHtml]="chart.description"></div>
												<div id='containerChartTable_{{_chartId}}' class="containerChartTable"></div>
												<div class="chart-container">									
												<ng-template #template></ng-template>
												</div>
												</div>`
})
export class DynamicChart
{
    public _chart     	       : any;
    public _data	 	           : any;
    public chartContainer	     : any;
    public _subscriptions	     : any;
    public _isRendererComplete  = false;
    public _runDisplay          = false;
    public _chartId 	         : number  = Math.random();

@ViewChild("template", { read : ViewContainerRef, static : true }) template : ViewContainerRef;
@Output("rendererClear") 	  rendererClearEvent    : EventEmitter<any> = new EventEmitter();
@Output("rendererComplete") rendererCompleteEvent : EventEmitter<any> = new EventEmitter();
@Output("viewer")           viewerEvent           : EventEmitter<any> = new EventEmitter();

constructor(
public elementRef 		   : ElementRef,
public renderer   		   : Renderer2,
public changeDetectorRef : ChangeDetectorRef,
public chartPlugin		   : ChartPlugin,
)
{
}

@Input()
public chartWidth   = "100%";
@Input()
public chartHeight  = "100%";

@Input()
set chart(chart:any)
{
    this._chart = chart;		

    if (chart && chart.showAverage)
    {
        chart.y.push({ status : true, yLabel : "Media", format : chart.y[0].format, datas : [], backgroundColor : "orangered", color : "orangered", average : true });
    }

    this.display();
}

get chart():any
{
    return this._chart;
}

@Input()
set data(data:any)
{
    if (data)
    {
        const data2   = [];
        const labels2 = [];
        let i         = 0;

        for (const item of data)
        {
            item.data  = [];
            item.count = [];

            for (const item2 of item.values)
            {
                if (item2.active || item2.current)
                {
                    item.data.push(item2.value)
                    item.count.push(item2.count)

                    if (i == 0)
                    {
                        labels2.push(item2.label);
                    }										
                }
            }			

            i++;
        }

        this._data = {
            data   : data,
            labels : labels2
        };
    }

    this.display();
}

get data():any
{
    return this._data;
}

async display() 
{
    if (this.chart && this.data)
    {
        console.log("display chart");

        let container : any;

        if (this.chart.type.value == Chart.PIE_TYPE.value)
        {
            container = PieChartContainer;
        }
        else if (this.chart.type.value == Chart.BAR_TYPE.value)
        {
            container = BarChartContainer;
        }
        else if (this.chart.type.value == Chart.LINE_TYPE.value)
        {
            container = LineChartContainer;
        }
        else if (this.chart.type.value == Chart.CARD_TYPE.value)
        {
            container = CardChartContainer;
        }

        this.clear();

        /* COMPOENT GRID */
        this.chartContainer = await this.core().componentFactory.create(this.template, container);

        this.template.insert(this.chartContainer.hostView);

        //this.chartContainer.instance.acl = this.acl;

        this.chartContainer.instance.chartWidth  = this.chartWidth;
        this.chartContainer.instance.chartHeight = this.chartHeight;
        this.chartContainer.instance.createComponents(this.chart, this.data).then(() =>
        {                                    
            this.drawTable();

            /* ISSO GARANTI OS OBJETOS SERAM DESTRUIDOS */
            setTimeout(() => 
            {
                /* VIEWER EVENT */
                this._subscriptions.add(this.chartContainer.instance.viewerEvent.subscribe((event:any) =>
                {
                    this.emitViewer(event);                            
                }));

                this.changeDetectorRef.markForCheck();

                this._isRendererComplete = true;
                this.rendererCompleteEvent.emit();

                this._runDisplay = false;

                console.log("dynamic-chart hostView");   					
            });     					               
        });
    }
}    

//https://jsfiddle.net/tara5/cjvuphkL/
drawTable()
{
    if (this.chart.type.value == Chart.BAR_TYPE.value || this.chart.type.value == Chart.LINE_TYPE.value)
    {
        const tableContainer = document.getElementById("containerChartTable_" + this._chartId);

        const tableHeader = `<tr>${
            this.data.labels.reduce((memo, entry) => 
            {
                memo += `<th>${entry}</th>`; return memo;
            }, "<th></th>")
        }</tr>`;

        const tableBody = this.data.data.reduce((memo, entry) => 
        {
            if (entry.status && !entry.average)
            {
                const rows = entry.data.reduce((memo, value, index) => 
                {				
                    if (entry.values[index].active || entry.values[index].current)
                    {
                        const count       = entry.count[index];
                        const trendDatas  = entry.values[index].trendValue  == 0 ? "" : entry.values[index].trendValue  == 1 ? "↑" : "↓";
                        const trendCounts = entry.values[index].trendCount == 0 ? "" : entry.values[index].trendCount == 1 ? "↑" : "↓";
                        const KData       = entry.values[index].trendValue  == 1 ? "positive" : "negative";
                        const KCount      = entry.values[index].trendCount == 1  ? "positive" : "negative";

                        if (entry.format.value == Chart.CURRENCY_FORMAT.value)
                        {
                            memo += `<td><p><small class="${KData}">${trendDatas}</small>${new Intl.NumberFormat("pt-BR", { style : "currency", currency : "BRL" }).format(value.toFixed(2))} (<small class="${KCount}">${trendCounts}</small>${count})</p></td>`;
                        }
                        else
                        {
                            memo += `<td>${value} ( ${count} )</td>`;
                        }																	
                    }						

                    return memo;

                }, "");

                memo += `<tr><td>${entry.label}</td>${rows}</tr>`;										
            }				

            return memo;

        }, "");

        let tableFooter = "";

        if (this.chart.showTotal)
        {
            tableFooter = `<tr><th>Total</th>${
                this.data.labels.reduce((memo, entry, indexes) => 
                {
                    let total : any = 0;

                    for (const item of this.data.data)
                    {
                        if (!item.average)
                        {
                            if (item.format.value == Chart.CURRENCY_FORMAT.value)
                            {
                                total += item.values[indexes].value;
                            }													
                        }
                    }		

                    memo += `<td>${new Intl.NumberFormat("pt-BR", { style : "currency", currency : "BRL" }).format(total.toFixed(2))}</td>`;

                    return memo;

                }, "")
            }</tr>`;
        }			

        const table = `<table class="striped">${tableHeader}${tableBody}${tableFooter}</table>`;

        tableContainer.innerHTML = table;
    }
}

markForCheck()
{
    this.changeDetectorRef.markForCheck();
}	

clear()
{
/* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
    if (this.template)
    {
        this.template.clear();
    }

    if (this.chartContainer)
    {
        this.chartContainer.destroy();
    }

    //if(this._subscriptions)
    //{
    //this._subscriptions.unsubscribe();                 
    //}

    /* NEW FAZ unsubscribe */
    this._subscriptions = new Subscription();

    this._isRendererComplete = false;

    console.log("clear list", (this.chart && this.chart.items ? this.chart.items.length : 0));            

    if (this.chartContainer)
    {
        Unbind.unbind(this.chartContainer.location.nativeElement);
    }

    this.markForCheck();

    this.rendererClearEvent.emit();
}

core()
{
    return Core.get();
}

emitViewer(event:any)
{
    this.viewerEvent.emit(event);
}
}
