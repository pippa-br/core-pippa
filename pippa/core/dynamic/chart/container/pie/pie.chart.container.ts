import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

/* PIPPA */
import { BaseChartContainer } from '../base.chart.container';
import { Chart }         	  from '../../../../model/chart/chart';

@Component({
    selector        : `[pie-chart-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<div class="canvas-wrapper" [style.width]="chartWidth" [style.height]="chartHeight">							
							<canvas baseChart
									id="chartContainer" 
									*ngIf="chartData"
									[datasets]="chartData"
									[labels]="chartLabels"
									[options]="chartOptions"
									[plugins]="chartPlugins"
									[legend]="chartLegend"
									[type]="chartType">
							</canvas> 
						</div>`
})
export class PieChartContainer extends BaseChartContainer
{
	public chartData    : any;
	public chartLabels  : any;	
	public chartPlugins : any = [pluginDataLabels];
	public chartLegend  : any = true;
	public chartType    : any =  'pie';

	// , 
	// {
	// 	afterLayout : (chart) => 
	// 	{
	// 		chart.legend.legendItems.forEach(
	// 		  	(label) => 
	// 		  	{
	// 				  let value = chart.data.datasets[0].data[label.index] || 0;
	// 				const yItem = this.chart.y[0]; /// DEPOIS PRECISA REVER QUANDO TIVER MAIS DE UM NIVEL

	// 				if(yItem.format.value == Chart.CURRENCY_FORMAT.value)
	// 				{
	// 					value = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
	// 				}

	// 				label.text += ': ' + value;

	// 				return label;
	// 		  	}
	// 		)			
	// 	}
	// }

	public chartOptions: any = 
	{
		responsive: true,
		//aspectRatio : 1,
		maintainAspectRatio : false,
		legend: 
		{
		  	position: 'top',
			labels : 
			{
				fontSize : 20,
			}
		},
		tooltips : 
		{
			callbacks : 
			{
			  	label : (tooltipItem, data) =>
			  	{
					const datasetLabel = data.labels[tooltipItem.datasetIndex] || '';
					const yItem 	   = this.chart.y[tooltipItem.datasetIndex];
					  let value        = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

					if(yItem.format.value == Chart.CURRENCY_FORMAT.value)
					{
						value = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
					}

					return datasetLabel + ': ' + value;
			  	}
			}
		},
		plugins: 
		{
		  	datalabels: 
			{
				formatter : (value, context) =>
				{
					const label = context.chart.data.labels[context.dataIndex];
					const yItem = this.chart.y[context.datasetIndex];

					if(yItem && yItem.format.value == Chart.CURRENCY_FORMAT.value)
					{
						return label + ': ' + new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
					}
					else 
					{
						return label + ': ' + value;
					}
                }
		  	},		  
			labels: 
			{
				render: 'percentage',
				fontColor: '#000',
				position: 'outside',
				fontSize: 20,
			}
		}
	};

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }    

    drawChart()
    {
        return new Promise<void>(async (resolve) =>
        {
			console.log('drawChart', this.data);
			
			const chartLabels = this.data.labels;
			const chartData   = [{data:this.data.data[0]}];

			/* DEFAULT: PREENCHE TODAS AS OPCAOES */
			if(this.setting.contextDefault)
			{
				const datas = await this.core()[this.setting.contextDefault].getProperty(this.setting.pathDefault);

				for(const key in datas)
				{
					const data = datas[key];

					if(!chartLabels.includes(data.name))
					{
						chartLabels.push(data.name);
						chartData[0].data.push(0);
					}
				}
			}

			this.chartLabels = chartLabels;
			this.chartData   = chartData;

            resolve();
        });
    }
}
