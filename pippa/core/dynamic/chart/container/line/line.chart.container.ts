import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import DataLabelsPlugin from 'chartjs-plugin-datalabels';

/* PIPPA */
import { BaseChartContainer } from '../base.chart.container';
import { Chart }         	  from '../../../../model/chart/chart';

@Component({
    selector        : `[line-chart-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
		template        : `<div class="canvas-wrapper" [style.width]="chartWidth" [style.height]="chartHeight">							
													<canvas baseChart
															*ngIf="chartData"
															[datasets]="chartData"
															[labels]="chartLabels"
															[options]="chartOptions"
															[plugins]="chartPlugins"
															[legend]="chartLegend"
															[type]="chartType">
													</canvas> 
											</div>`
})
export class LineChartContainer extends BaseChartContainer
{
	public chartData    : any;
	public chartLabels  : any;	
	public chartLegend  : any = true;
	public chartType    : any =  'line';
	public chartPlugins : any = [DataLabelsPlugin, 
	{
		id: 'customPlugin',
		beforeInit : (chart, options) => 
		{
			this.core().loadingController.start();
			chart.legend.afterFit = function() 
			{
				this.height += 50;
			};
		},
		afterInit : (chart, options) => 
		{
			this.core().loadingController.stop();
		},
	}];
	
	public chartOptions: any = 
	{
		responsive: true,
		maintainAspectRatio : false,
		layout: {
        padding: {
            top: 30,    // Padding no topo
            right: 50,  // Padding à direita
            bottom: 30, // Padding na parte inferior
            left: 0    // Padding à esquerda
        }
    },
		options: {
        scales: {
            y: {
                stacked: true
            }
        }
    },
		scales: {
        x: {
            ticks: {
                padding: 10, // Espaçamento entre os rótulos e o eixo
            },
            grid: {
                borderColor: '#333', // Cor da linha do eixo
                borderWidth: 1, // Largura da linha do eixo
                tickLength: 50, // Comprimento dos ticks no eixo
                lineWidth: 1 // Largura da linha da grade
            }
        },
        y: {
            ticks: {
                padding: 10, // Espaçamento entre os rótulos e o eixo
            },
            grid: {
                borderColor: '#333', // Cor da linha do eixo
                borderWidth: 1, // Largura da linha do eixo
                tickLength: 50, // Comprimento dos ticks no eixo
                lineWidth: 1 // Largura da linha da grade
            }
        }
    },
		// scales :
    //     {
    //         xAxes : [{
		// 					stacked: false,
    //         }],
    //         yAxes : [{
		// 					stacked: false,
		// 					ticks: {
		// 						//stepSize	: 2,
		// 						beginAtZero : true,
		// 					}
    //         }]
		// },
		tooltips : 
		{
			callbacks : 
			{
			  	label : (tooltipItem, data) =>
			  	{
							const datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
							const yItem 	   = this.chart.y[tooltipItem.datasetIndex];
								let value        = tooltipItem.yLabel;

							if(yItem.format.value == Chart.CURRENCY_FORMAT.value)
							{
									value = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
							}

							return datasetLabel + ': ' + value;
			  	}
			}
		},
		plugins : 
		{
			datalabels : 
			{
					anchor: 'start',
					align: 'top', // Change to 'top', 'bottom', 'center', etc.
					//padding: 5, // Add padding to increase distance
					offset: -30, // Adjust the offset if needed
					formatter : (value, context) =>
					{
							const yItem = this.chart.y[context.datasetIndex];
							const count = context.dataset.count[context.dataIndex];

							if(yItem)
							{
									if(yItem.average)
									{
											return '';
									}

									let valueFormat = ''

									if(yItem.format.value == Chart.CURRENCY_FORMAT.value)
									{
											valueFormat += new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
									}
									else 
									{
											valueFormat += value
									}

									console.error(yItem)

									if(yItem.count)
									{
											valueFormat += ' ( ' + count + ' )';
									}

									return valueFormat;
							}							
					}
			},	
			legend: {
				display: true,
			},		
			labels : true
		}
	};

    constructor(
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }    

    drawChart()
    {
       for(const item of this.data.data)
				{
						item.borderColor = item.backgroundColor;
				}

				this.chartLabels = this.data.labels;
				this.chartData   = this.data.data;	
    }	
}
