import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseChartContainer } from "../base.chart.container";

@Component({
    selector        : "[card-chart-container]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div *ngIf="chartData" class="canvas-wrapper" [style.width]="chartWidth" [style.height]="chartHeight">							
													<section class="flex">
															<div class="flex-item" *ngFor="let item of chartData" 
																	[style.background]="item.backgroundColor" 
																	[style.color]="item.color" 
																	[ngClass]="{'noValue' : !item?.count }">
																<label>{{item.label}} / {{item.filterLabel}}</label>
																<small *ngIf="item.projectionCounts > 0 && item.showProjection && item.format.value == 'currency'">{{item.projectionDatas|currencyFormat}} ({{item.projectionCounts|intFormat}})</small>
																<p>
																	<span *ngIf="item.format.value == 'currency'">{{sum(item)|currencyFormat}} <small *ngIf="item.showCount">({{count(item)|intFormat}})</small></span>
																	<span *ngIf="item.format.value == 'value'">{{sum(item)}} <small *ngIf="item.showCount">({{count(item)|intFormat}})</small></span>
																</p>
															</div>													
												</section>
											</div>`
})
export class CardChartContainer extends BaseChartContainer
{
    public chartData   : any;

    constructor( 
				public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }    

    drawChart()
    {
        for (const item of this.data.data)
        {
            item.borderColor = item.backgroundColor;
        }

        this.chartData = this.data.data;		
    }	

    sum(item:any)
    {
			  let total = 0;
        let i     = 0;

        if (item.values)
        {
            for (const data of item.values)
            {
                if (data.value)
                {
                    total += data.value;
                    i++
                }								
            }

            if (item.function.value == "average")
            {
                total = total / i;
            }
        }

        return total;
    }

    count(item:any)
    {
			  let total = 0;
        let i     = 0;

        if (item.values)
        {
            for (const data of item.values)
            {
                if (data.count)
                {
                    total += data.count;
                    i++
                }
            }

            if (item.function.value == "average")
            {
                total = total / i;
            }
        }

        return total;
    }
}
