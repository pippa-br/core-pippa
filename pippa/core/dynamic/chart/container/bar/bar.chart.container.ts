import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import DataLabelsPlugin from 'chartjs-plugin-datalabels';

/* PIPPA */
import { BaseChartContainer } from '../base.chart.container';
import { Chart }         	  from '../../../../model/chart/chart';

@Component({
    selector        : `[bar-chart-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<div class="canvas-wrapper" [style.width]="chartWidth" [style.height]="chartHeight">							
							<canvas baseChart
									*ngIf="chartData"
									[datasets]="chartData"
									[labels]="chartLabels"
									[options]="chartOptions"
									[plugins]="chartPlugins"
									[legend]="chartLegend"
									[type]="chartType">
							</canvas> 
					   </div>`
})
export class BarChartContainer extends BaseChartContainer
{
	public chartData    : any;
	public chartLabels  : any;	
	public chartLegend  : any = true;
	public chartType    : any =  'bar';
	public chartPlugins : any = [DataLabelsPlugin, 
	{
		id: 'customPlugin',
		beforeInit : (chart, options) => 
		{
			this.core().loadingController.start();
			chart.legend.afterFit = function() 
			{
				this.height += 50;
			};
		},
		afterInit : (chart, options) => 
		{
			this.core().loadingController.stop();
		},
	}];
	
	public chartOptions: any = 
	{
		responsive: true,
		maintainAspectRatio : false,
		scales : {
			// x: {
			// 	stacked: true
			// },
			// y: {
			// 	stacked: true,
			// 	beginAtZero: true
			// },
		},
		tooltips : 
		{
			callbacks : 
			{
			  	label : (tooltipItem, data) =>
			  	{
					const datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
					const yItem 	   = this.chart.y[tooltipItem.datasetIndex];
					  let value        = tooltipItem.yLabel;

					if(yItem.format.value == Chart.CURRENCY_FORMAT.value)
					{
						value = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
					}

					return datasetLabel + ': ' + value;
			  	}
			}
		},
		plugins : 
		{
			datalabels : 
			{
			  	anchor    : 'end',
				align     : 'end',
				formatter : (value, context) =>
				{
					const yItem = this.chart.y[context.datasetIndex];

					if(yItem && yItem.format.value == Chart.CURRENCY_FORMAT.value)
					{
						return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value.toFixed(2));
					}
					else 
					{
						return value;
					}
                }
			},	
			legend: {
				display: true,
			},		
			labels : true
		}
	};

    constructor(
		public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }    

    drawChart()
    {
				console.log('drawChart', this.data);

				// if(this.data.data.length > 0)
				// {
				// 		const w = this.data.data[0].data.length * 5 * this.data.data.length;

				// 		if(w < 100)
				// 		{
				// 			this.chartWidth = 100;
				// 		}				
				// 		else
				// 		{
				// 			this.chartWidth = w;
				// 		}
				// }

				this.chartLabels = this.data.labels;
				this.chartData   = this.data.data;
    }	
}
