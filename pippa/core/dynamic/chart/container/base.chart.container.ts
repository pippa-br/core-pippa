import { Input, Output, EventEmitter, OnDestroy, ViewContainerRef, ViewChild, HostBinding, ChangeDetectorRef, Directive } from "@angular/core";
import { Subscription } from "rxjs";
import { BaseChartDirective } from "ng2-charts";

/* PIPPA */
import { Core } from "../../../util/core/core";

@Directive()
export abstract class BaseChartContainer implements OnDestroy
{
    public _chart      	   : any;
    public _data 	   	   : any;
    public _subscriptions  : any;
    public _listSubscribe  : any;
    public _chartSubscribe : any;
    public _resolve        : any;
    public runDisplay      : any;
    public setting         : any;
    public chartType       : any;
    public chartColors 	   : any = [];
    @Input()
    public chartWidth    = "100%";
    @Input()
    public chartHeight   = "100%";

    @Input() public acl  : any;

    @Output("viewer")  viewerEvent  : EventEmitter<any>;

    @ViewChild(BaseChartDirective) public baseChart : BaseChartDirective;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        this.viewerEvent = new EventEmitter();
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }	

    createComponents(chart:any, data:any)
    {
        return new Promise((resolve) =>
        {
            this.chart = chart;
            this.data  = data;

            this._resolve = resolve;

            this.display();
        });
    }

    @Input()
    set data(data:any)
    {
        if (data)
        {
            /*if(this._listSubscribe)
            {
                this._listSubscribe.unsubscribe();
            }            

            this._listSubscribe = new Subscription();

            this._listSubscribe.add(data.onUpdate().subscribe(() =>
            {
                console.log('data change display');

                this.display();
            }));*/
			
            data._instance = this;
        }

        this._data = data;
    }

    get data():any
    {
        return this._data;
    }

    @Input()
    set chart(chart:any)
    {
        if (chart)
        {
            this.setting = chart.setting || {};

            if (this.setting.chartType)
            {
                this.chartType = this.setting.chartType;
            }

            this._chartSubscribe = chart.onUpdate().subscribe(() =>
            {
                console.log("chart change display");

                this.display();
            });					

            this._chart = chart; 
        }
    }

    get chart()
    {
        return this._chart;
    }

    display()
    {
        if (this.chart && this.data)
        {   
            if (!this.runDisplay)
            {
                this.runDisplay = true;

                if (this.data && this.chart.y.length > 0)
                {
                    this.drawChart();

                    this.core().loadingController.stop().then(() => 
                    {                          
                        this.markForCheck();   

                        this._resolve();
                                                            
                        console.log("display chart", (this.data ? this.data.length : 0));    
                    }); 
                    
                    this.runDisplay = false;
                }
                else
                {                
                    console.log("display table", (this.data ? this.data.length : 0)); 
                    
                    this.drawEmpty();

                    this.markForCheck();
                    this.runDisplay = false;
                }
            }
        }
        else
        {            
            this.markForCheck();
            this._resolve();
        }
    }	

    drawChart()
    {
        // OVERRIDER       
    }
	
    drawEmpty()
    {
        if (this.chart.showNoItems) 
        {
            /*const component = await PartialFactory.createComponent(PartialType.type('Label'), this.template, 
            {
                label   : 'Nenhum item adicionado!',
                classes : ['note-alert'],
            });*/	
        }
    }    

    ngOnDestroy()
    {
        this.destroy();
    }
	
    destroy()
    {
        console.log("ngOnDestroy");

        if (this._chartSubscribe)
        {
            this._chartSubscribe.unsubscribe();
        }
        
        if (this._listSubscribe)
        {
            this._listSubscribe.unsubscribe();
        }

        this.clear();
    }

    clear(force = false)
    {
        /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
        if (this.baseChart)
        {
            this.baseChart.update(); 
        }

        console.log("chart clear");

        if (this._subscriptions)
        {
            this._subscriptions.unsubscribe();                 
        }

        this._subscriptions = new Subscription();

        this.markForCheck();
    }

    hasAcl(name:string, value?:any)
    {
        return this.acl && this.acl.hasKey(name, value);
    }

    emitViewer(event:any)
    {
        this.viewerEvent.emit(event);
    }    

    core()
    {
        return Core.get()
    }
}
