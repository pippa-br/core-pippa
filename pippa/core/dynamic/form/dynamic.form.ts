import { Component, Input, Output, ViewChild, ViewContainerRef, OnDestroy, HostBinding, EventEmitter,
    ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { ModalController, PopoverController } from "@ionic/angular";
import { Subscription } from "rxjs";
import * as elementResizeEvent from "element-resize-event";
import * as Unbind from "element-resize-event";
import * as _ from "lodash" 

/* PIPPA */
import { BaseComponent }          from "../../base/base.component"; /* NÃO USAR O BASEFORM */
import { BaseModel }              from "../../model/base.model";
import { Form }                   from "../../model/form/form";
import { Field }                  from "../../model/field/field";
import { FieldType }              from "./../../type/field/field.type";
import { FormType }               from "./../../type/form/form.type";
import { Core }                   from "../../util/core/core";

@Component({
    selector        : "form[dynamic-form]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="no-available" *ngIf="!_available">
                       Documento não Disponível!
                   </div>
										<div class="loading-container" [hidden]="!_isSend && _isRendererComplete">
												<ion-progress-bar color="medium" type="indeterminate"></ion-progress-bar>
										</div>
                   <div class="form-container" [hidden]="!_isRendererComplete || !_available">
                       <ng-template #template></ng-template>
                   </div>`,
})
export class DynamicForm extends BaseComponent implements OnDestroy
{
    @HostBinding("class") nameClass  = "ng-load";

    @Output("add")              addEvent         : EventEmitter<any> = new EventEmitter();
    @Output("set")              setEvent         : EventEmitter<any> = new EventEmitter();
    @Output("close")            closeEvent       : EventEmitter<any> = new EventEmitter();
    @Output("invalid")          invalidEvent     : EventEmitter<any> = new EventEmitter();
    @Output("isValidChange")    isValidChange    : EventEmitter<any> = new EventEmitter();
    @Output("rendererComplete") rendererComplete : EventEmitter<any> = new EventEmitter();

    @Input() id  : string;
    @Input() acl : any;

    @ViewChild("template", { read : ViewContainerRef, static : true }) template : ViewContainerRef;

    public unItems       		: any;
    public unData        		: any;
    public subscriptions 		: Subscription = new Subscription();
    public autoSaveTime  		: any;
    public subscribeAutoSave 	: any;
    public setTimeoutUpdateData : any;

    public formContainer       : any;
    //public isInitialValue      : boolean = false;
    public isCreateComponents   = false;
    public _updateData          = false;	
    public _form               : Form;
    public _data               : any;
    public _initialData        : any;
    public _paramsData         : any;
    public _isRendererComplete  = false;
    public _available           = true;
    public _isSend              = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super();
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }	

    @Input()
    get form():any
    {
        return this._form;
    }

    set form(form:any)
    {
        if (form)
        {
            console.log("set form", form);

            if (!(form instanceof Form))
            {
                form = new Form(form);
            }

            this._form = form;

            form.on(true).then(() =>
            {
                //form.items.doSort();
				
                /* INIT INVERSED BY */
                form.items._set({
                    id    : "initInversedBy",
                    //step  : this.form.items[0].step,
                    row   : 999,
                    field : new Field({
                        id       : "initInversedBy",
                        name     : "initInversedBy",
                        type     : FieldType.type("Hidden"),
                        required : false,
                    })
                });

                /* INVERSED BY */
                form.items._set({
                    id    : "inversedBy",
                    //step  : this.form.items[0].step,
                    row   : 999,
                    field : new Field({
                        id       : "inversedBy",
                        name     : "inversedBy",
                        type     : FieldType.type("Hidden"),
                        required : false,
                    })
                });

                /* ATTACHMENTS */
                form.items._set({
                    id    : "attachments",
                    //step  : this.form.items[0].step,
                    row   : 998,
                    field : new Field({
                        id       : "attachments",
                        name     : "attachments",
                        type     : FieldType.type("Hidden"),
                        required : false,
                    })
                });

                /* STEPS */
                form.items._set({
                    id    : "_steps",
                    //step  : this.form.items[0].step,
                    row   : 998,
                    field : new Field({
                        id       : "_steps",
                        name     : "_steps",
                        type     : FieldType.type("Hidden"),
                        required : false,
                    })
                });
				
                /* LOGS */
                form.items._set({
                    id    : "_logs",
                    //step  : this.form.items[0].step,
                    row   : 998,
                    field : new Field({
                        id       : "_logs",
                        name     : "_logs",
                        type     : FieldType.type("Hidden"),
                        required : false,
                    })
                });

                form.instance = this;

                /* BIND CHANGE */
                if (this.unItems)
                {
                    this.unItems.unsubscribe();
                }               
				
                /* REMOVI PARA DEIXAR APENAS NO STEP PRECISA VER SE ISSO EH UTIL PARA ALGUM OUTRO CESSNARIO. FORM AUTO SAVE */
                /*if(this.form.autoSave)
				{
					if(this.subscribeAutoSave)
					{
						this.subscribeAutoSave.unsubscribe();
					}

					this.subscribeAutoSave = this.form.formGroup.valueChanges.subscribe(val => 
					{
						if(this._isRendererComplete && this.form.isSetModeForm())
						{
							if(this.autoSaveTime)	
							{
							   clearTimeout(this.autoSaveTime);
							}
   
						   this.autoSaveTime = setTimeout(() => 
						   {							
								if(this.data)
								{
									const data = _.merge({}, this._initialData, this.form.formGroup.value);

									this.data.set(data)	 
								}
   
							   clearTimeout(this.autoSaveTime);
						   }, 300);
						}						 
					});
				}*/

                this.display().then(() =>
                {
                    // PARA CARREGA TODOS OS OPTIONS JUNTOS
                    Promise.all(form.loadOptionPromises).then(() => 
                    {
                        form.getInitialData().then((data:any) =>
                        {
                            /* PARAMS DATA */
                            if (this.core().queryParams && this.core().queryParams.data)
                            {
                                this._paramsData = this.core().queryParams.data;
                            }
	
                            console.log("paramsData", this._paramsData);
	
                            this._initialData = _.merge({}, this._initialData, data, this._paramsData);
	
                            console.log("getInitialData", this._initialData);
	
                            /* BIND PARA NOVAS ALTERAÇOES*/
                            this.unItems = form.items.onUpdate().subscribe(() =>
                            {
                                /* SE O FORMULARIO ALTERAR O DATA INICIAL TAMBÉM ALTERA */
                                form.getInitialData().then((initialData:any) =>
                                {
                                    console.log("change items form");
	
                                    this._initialData = initialData;
	
                                    console.log("getInitialData", this._initialData);
	
                                    /* NÃO LIMPAR COM CEAR POIS O VALUE PODE TER O NULL COMO DEFAULT */
                                    const data = _.merge({}, this.getData(), this._paramsData, this.form.formGroup.value);
	
                                    this.display().then(() =>
                                    {
                                        if (!this._data)
                                        {
                                            this._data = new BaseModel();
                                        }
	
                                        this.data.populate(data);
	
                                        this._updateData = false;
                                        this.updateData();
                                    });
                                });
                            });
	
                            this.updateData();						
                        });
                    });											
                });                
            });
        }
        else
        {
            this._form = null;
        }

        this.doModeForm();
        // /this.doBind();
    }

    /* DATA */
    @Input()
    get data():any
    {
        return this._data;
    }

    set data(data:any)
    {
        if (data)
        {
            console.log("set data", data);
            //console.log('-----xxxyww', data);

            /* PQ ???? GARANTE O BIND QUANDO HOVER ALTERACAO NO DATA */
            //data.doSnapshot().then(() =>
            //{
            /* BIND CHANGE */				
            if (this.unData)
            {
                this.unData.unsubscribe();
            }                    
				
            /*this.unData = data.onUpdate().subscribe(() =>
				{
					if(!this.form.autoSave)
					{
						this._updateData = false;
	
						this.checkAvaliable();
						this.updateData();
	
						this.markForCheck();
					}
				});*/

            this._data       = data;
            this._updateData = false;

            this.checkAvaliable();
            this.updateData();
            //});
        }
        else
        {
            this._data = null;
        }

        this.doModeForm();
    }

    doModeForm()
    {
        if (this.form)
        {
            if (this.data)
            {
                this.form.doSetModeForm();
            }
            else
            {
                this.form.doAddModeForm();
            }            
        }
    }

    checkAvaliable()
    {
        if (this.data)
        {
            this._available = !this.data._archive && !this.data._canceled && this.data._lock != "mobile";

            /* QUANDO AVAILABLE FECHA TODAS JANELAS */
            if (!this._available)
            {
                const modalController   = this.core().injector.get(ModalController);
                const popoverController = this.core().injector.get(PopoverController);
	
                modalController.dismiss();
                popoverController.dismiss();
            }
        }        
    }

    getData()
    {
        if (this._data)
        {
            return this.data.parseData();
        }
    }

    getFullData()
    {
        console.log("fulldata", this._initialData, this._paramsData, this.data);

        return _.merge({}, this._initialData, this._paramsData, this.getData());
    }

    updateData()
    {
        if (this.isCreateComponents && !this._updateData && this.form)
        {            
            this._updateData = true;
            const data       = this.getFullData();

            /* RESET */
            this.reset();
			
            /* OBJECT VAZIO*/
            if (Object.keys(data).length == 0)
            {
                this.formContainer.instance.submitted = false;
            }
            else
            {
                /* PASSA O MODEL PARA OS INPUTS TER ACESSO, PRINCIPALMETE NO ADD POIS NÃO TEM DATA */
                // AUTO SAVE PRECISA DO THIS.DATA 				
                if (this.form.isAddModeForm())
                {
                    const model = new BaseModel();
                    model.populate(data);

                    this.formContainer.instance.data = model;
                }
                else
                {
                    this.formContainer.instance.data      = this.data;
                    this.formContainer.instance.submitted = true;
                }	
				
                if (this.setTimeoutUpdateData)
                {
                    clearTimeout(this.setTimeoutUpdateData);	
                }				
                
                // BUG DE NÃO ATUALIZAR O SIDE FORM
                this.setTimeoutUpdateData = setTimeout(() => 
                {
                    //this.form.formGroup.patchValue(cleanDeep(data)); /* PATCH NÃO PERMITE NULL PARA OBJETOR COM FILDSET */
                    this.form.formGroup.patchValue(data); /* PRECISO ENVIAR NULLS */
                    this.form.formGroup.markAsPristine();					
					
                    console.log("update data form", data, this.form.formGroup.value, this.form.formGroup);
                }, 100);
            }
        }
    }
	
    save()
    {
        console.log("save formGroup", this.form.formGroup, this.form.formGroup.value, "add", this.form.isAddModeForm());
        
        /* STEPS TEM SEU PROPRIO SUBMIT */
        if (!this.form.stepSave && !this.formContainer.instance.submitted)
        {
            this.formContainer.instance.submitted = true;
            console.log("submitted", this.formContainer.instance.submitted);
        }

        if (this.form.stepSave || this.form.formGroup.valid)
        {
            /* FAZ COPIA E MERGE COM O INITIAL DATA, POIS TEM CAMPOS QUE NÃO PODEM ESTA NO FORM POR PERMISSAO */
            const data = _.merge({}, this._initialData, this.form.formGroup.value);
            //const data = Object.assign({}, this._initialData, cleanDeep(this.form.formGroup.value));
            console.log("data", data);

            this.form.error                     = "";
            this.formContainer.instance.isValid = true;

            if (this.form.isAddModeForm())
            {
                console.log("add data", data);

                this.addEvent.emit({
                    data : data
                });
            }
            else
            {
                console.log("set data",  data);

                this.setEvent.emit({
                    data : data
                });
            }

            if (this.form.saveFixed)
            {
                if (this.form.isAddModeForm())
                {
                    this._updateData = false;
                    this.updateData(); /* FAZ RESET */	
                }
                else
                {
                    this._data   = null;
                    this._isSend = true;

                    this.clear();					
                    this.display();

                    this.form.doAddModeForm();
                }	
            }
            else if (this.form.stepSave)
            {
                this.form.doSetModeForm();
            }
            else
            {
                this._data   = null;
                this._isSend = true;

                this.clear();
            }

            this.form.submitEvent.emit({
                data : data
            });			
        }
        else
        {
            this.formContainer.instance.isValid = false;
            this.form.error                     = "Verificar os campos requeridos!";
            this.invalidEvent.emit(this.form.formGroup);
            this.formContainer.instance.goStepInvalidStep();
        }
    }

    display()
    {
        this.isCreateComponents = false;

        return new Promise<void>(async (resolve) =>
        {
            if (this.form)
            {
                /* LOAD */
                //await this.core().loadingController.start();

                this.clear();

                console.log("Create components by dynamic form", this.form.items);

                console.log("this.form.mode", this.form.mode);

                const container : any = FormType.container(this.form.type.value);                    

                this.formContainer = await this.core().componentFactory.create(this.template, container);

                /* COMPONENT E DIFERENTE DE INSTACE, COMPONENT PARA USAR O detectChanges */
                this.form.component = this.formContainer; 
                this.form.component = this.formContainer;
                this.template.insert(this.formContainer.hostView);

                /* ACL */
                this.formContainer.instance.acl = this.acl;

                /* RESIZE IFRAME: QUANDO RESIZE */
                if (window.parent)
                {
                    elementResizeEvent(this.formContainer.location.nativeElement, () => 
                    {
                        this.dispatchPostMessage();
                    });
                } 

                this.formContainer.instance.createComponents(this.form).then(() =>
                {                        
                    this.markForCheck();

                    /* EVENT VALIDATE: ADD / SET */
                    this.subscriptions.add(this.form.saveEvent.subscribe(() =>
                    {
                        console.log("save click");

                        this.save();
                    }));

                    /* EVENT CANCEL */
                    this.subscriptions.add(this.form.cancelEvent.subscribe(() =>
                    {
                        console.log("close click");

                        this._data   = null;
                        this._isSend = true;
	
                        this.clear();
                        this.form.reset();
                        this.closeEvent.emit();
						
                        if (this.form.saveFixed)
                        {
                            this.display();
                        }

                        this.form.doAddModeForm();
                    }));

                    this.isCreateComponents = true;

                    this.nameClass = "ng-complete";

                    //this.updateData();

                    this._isSend             = false;
                    this._isRendererComplete = true;
                    this.rendererComplete.emit({
                        footerViewer : this.formContainer.instance.footer
                    });

                    /* LOAD */
                    //this.core().loadingController.stop();
					
                    /* RESIZE IFRAME: QUANDO LOAD */
                    if (window.parent)
                    {
                        this.dispatchPostMessage();
                    } 
					
                    console.log("dynamic-form hostView");

                    resolve();
                });
            }
        });
    }
	
    dispatchPostMessage()
    {
        setTimeout(() => 
        {   
            window.parent.postMessage({
                type   : "resize", 
                height : this.formContainer.location.nativeElement.scrollHeight,
                target : this.form.id,
            }, 
            "*");
        }, 500);
    }

    destroy()
    {
        this.ngOnDestroy();
    }
	
    ngOnDestroy()
    {
        this.reset();
        this.clear();

        if (this.unItems)
        {
            this.unItems.unsubscribe();
        }
		
        if (this.unData)
        {
            this.unData.unsubscribe();
        }
		
        if (this.subscribeAutoSave)
        {
            this.subscribeAutoSave.unsubscribe();
        }

        this._isSend             = false; // PARA NÃO APARECER O LOADBAR
        this._isRendererComplete = true; // PARA NÃO APARECER O LOADBAR

        console.log("ngOnDestroy form");
    }

    reset()
    {
        if (this.formContainer)
        {
            this.formContainer.instance.submitted = false;
            this.formContainer.instance.isValid   = false;
        }

        if (this.form)
        {
            this.form.reset();

            console.log("reset form", this.form.id);
        }
    }

    clear()
    {
        /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
        if (this.template)
        {
            this.template.clear();
        }

        if (this.formContainer)
        {
            this.formContainer.destroy();
            this.formContainer.instance.submitted = false;
            this.formContainer.instance.isValid   = false;
        }

        this.subscriptions.unsubscribe();
        this.subscriptions = new Subscription();

        this._isRendererComplete = false;
        this._updateData         = false; /* ADICIONEI DEPOIS VAMOS VER!!! */

        if (this.formContainer)
        {
            Unbind.unbind(this.formContainer.location.nativeElement);
        }

        this.markForCheck();
    }

    core()
    {
        return Core.get()
    }
}
