import { Component, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseFormContainer }  from '../base.form.container';

@Component({
    selector        : '[expand-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        :  `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid }" 
							[formGroup]="form.formGroup">
							
							<mat-toolbar class="header-container" *ngIf="form.hasHeader">                            
								<h4 *ngIf="form.name">{{form.name}} <span *ngIf="data && data.name">- {{data.name}}</span></h4>                            
							</mat-toolbar>
							<mat-toolbar class="subheader-container" *ngIf="form.description">                            
								<div [innerHtml]="form.getDescription()"></div>                            
							</mat-toolbar>
	
							<mat-accordion #stepper>
								<mat-expansion-panel *ngFor="let step of steps; let i = index;" 
													 class="wrapper-body-container" 
													 [ngClass]="{hide : !step.display}">

									<mat-expansion-panel-header class="title-container" [ngClass]="{'complete' : step.complete}">
										<mat-panel-title>
											<span class="icon">{{i + 1}}</span> <label>{{step.label}}</label> <ion-icon *ngIf="step.complete" name="checkmark-outline"></ion-icon>
										</mat-panel-title>
									</mat-expansion-panel-header>
									<div class="body-container">
										<ng-template #templates></ng-template>										
									</div>
									<div class="error-container">{{form.error}}</div>
									<mat-toolbar class="footer-container" *ngIf="form.hasButtons">
										<div class="buttons">
											<ion-button *ngIf="(form.hasSubmitButton) || (core().user && core().user.isAdmin())"
													class="save-button"
													outline
													(click)="onSave(step)">
												{{(form.isAddModeForm() ? form.addText : form.setText) | translate}}
											</ion-button>
											<ion-button *ngIf="form.hasCancelButton"
														class="cancel-button" ion-button outline
														[disabled]="step?.isValid"
														(click)="form.onCancel($event)">
												{{form.cancelText}}
											</ion-button>									
										</div>
									</mat-toolbar>
								</mat-expansion-panel>								
							</mat-accordion>		
							
						</div>`,
})
export class ExpandFormContainer extends BaseFormContainer
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
	}
	
	/* OVERRIDE */
	/*displaySteps()
	{
		if(this.stepper)
		{
			for(const key in this.steps)
			{
				const step = this.steps[key];
	
				if(step.display)
				{
					this.renderer.removeClass(this.stepper._elementRef.nativeElement.children[key], 'hide');
				}
				else
				{
					this.renderer.addClass(this.stepper._elementRef.nativeElement.children[key], 'hide');
				}
			};
	
			this.markForCheck();
		}		
	}*/

	/* OVERRIDE */
	/*completeStep()
	{
		if(this.stepper)
		{
			for(const key in this.steps)
			{
				const step = this.steps[key];
	
				if(step.complete)
				{
					this.renderer.addClass(this.stepper._elementRef.nativeElement.children[key], 'complete');				
				}
				else
				{
					this.renderer.removeClass(this.stepper._elementRef.nativeElement.children[key], 'complete');
				}
			};
	
			this.markForCheck();
		}		
	}*/
}
