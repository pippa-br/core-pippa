import { ViewChildren, ViewChild, ViewContainerRef, QueryList, Renderer2, OnDestroy, ChangeDetectorRef, AfterViewInit, Directive, ElementRef } from "@angular/core";
import { Subscription } from "rxjs";
import { MatStepper } from "@angular/material/stepper";

/* CORE */
import { Core }           from "../../../util/core/core";
import { Params }         from "../../../util/params/params";
import { InputFactory }   from "../../../factory/input/input.factory";
import { PartialType }    from "../../../type/partial/partial.type";
import { PartialFactory } from "../../../factory/partial/partial.factory";
import { BaseModel } from "../../../model/base.model";

@Directive()
export abstract class BaseFormContainer implements AfterViewInit, OnDestroy
{
    @ViewChildren("templates", { read : ViewContainerRef }) templates : QueryList<ViewContainerRef>;
    @ViewChild("stepper",      { static : false })     public stepper : any;
    @ViewChild("footer",       { read : ElementRef }) public footer   : ElementRef;

    //@ViewChild('stepper', { read: MatStepper, static:false }) public stepper : any;

    public horizontal 	  = false;
    public isLinear  	  = true;
    public isFirst   	  = true;
    public isLast     	  = false;
    public container     : any;
    public steps         : any = [];
    public form          : any;
    public _data         : any;    
    public _resolve      : any;
    public _submitted     = false;
    public isValid        = false;
    public items         : any /* ITENS FILTRADOS POR PERMISSOES */
    public _updateData   : any;
    public subscriptions : any = new Subscription();

    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
    }

    createComponents(form:any)
    {
        return new Promise((resolve) =>
        {
            this.form     = form;
            this.items    = form.getItems();
            this.steps    = this.items.getSteps(null, form);
            this._resolve = resolve;
			
            for (const key in this.steps)
            {
                const step = this.steps[key];
                this.subscriptions.add(step.onUpdate().subscribe(() =>
                {
                    this.displaySteps();
                }));
            }

            /* SETTING */
            if (this.form.setting)
            {
                /* DEPOIS REMOVER */
                this.isLinear = this.form.setting.isLinear;
            }
			
            if (this.form.setting && this.form.setting.horizontal !== undefined)
            {
                /* DEPOIS REMOVER */
                this.horizontal = this.form.setting.horizontal;
            }					
	
            /* DEIXA OS ADMINISTRADORES VER TODOS OS STEPS */
            if (this.core().user && this.core().user.isAdmin())
            {
                this.isLinear = false;	
            }
            
            /* GET INFOS PARA OS STEPS */
            for (const key in this.steps)
            {
                this.steps[key].infos = this.getInfos(this.steps[key]);
            }

            this.initialize();
        });
    }

    getInfos(step:any)
    {	
        const infos = [];

        if (step.setting && step.setting.infoLabel && step.setting.infoPath)
        {
            const infoLabel = step.setting.infoLabel.split(",");
            const infoPath  = step.setting.infoPath.split(",");

            for (const key in infoLabel)
            {
                infos.push({
                    label : infoLabel[key],
                    path  : infoPath[key],
                })
            }
        }

        return infos;
    }

    initialize()
    {
        // OVERRIDER
    }

    onSave(step?:any)
    {
        //&& this.form.stepSave
        if (step)
        {
            if (this.validateSave(step) && this.validateStep(step))
            {
                this.setStepValue(step);
                this.form.onSave();
            }
	
            this.submittedStep(step, true);	
        }
        else
        {
            this.form.onSave();
        }
    }

    displaySteps()
    {	
        /* OVERRIDE */
    }
    
    completeStep()
    {	
        /* OVERRIDE */
    }
    
    getCurrentStep()
    {        
        /* OVERRIDE */
    }

    setStepValue(step:any)
    {
        step.complete = true;	
        const control = this.form.formGroup.get("_steps");
        let value     = control.value;

        if (!value || value == "")
        {
            value = {};
        }

        const key = step.name || step.id;

        // SET VALUE STEP
        value[step.id] = true;

        console.log("setStepValue", value);

        // SET ALL STEP
        let total = 0;
        let count = 0;

        for (const step2 of this.steps)
        {
            // SO CONSIDERAR OS STEP DISPLAY
            if (step.display)
            {
                if (value && value._steps && value._steps[step2.id])
                {
                    count++;
                }
				
                total++;
            }
        }

        if (total == count)
        {
            value["completeAll"] = true;
        }

        control.setValue(value);			
    }

    onNextStep(step:any)
    {
        /* POR CAUSA DA VALIDACAO EM BANCO */	
        if (this.form.formGroup.status == "PENDING")
        {
            setTimeout(() =>
            {
                this.onNextStep(step);
            }, 2000);
        }
        else
        {
            console.log("onNextStep formGroup", this.form.formGroup.value);

            // DADOS UTILIZADO NO VISUAL DOS STEPS
            this.updateData = new BaseModel(this.form.formGroup.value);

            if (this.validateSave(step) && this.validateStep(step))
            {				
                step.complete = true;
                this.stepper.next();
                this.checkButtons();
				
                this.setStepValue(step);
				
                if (this.form.stepSave)
                {
                    this.form.onSave();
                }				

                this.completeStep();
                this.markForCheck();
            }
            else
            {
                step.complete = false;
                this.submittedStep(step, true);
            }            
        }
    }

    onPrevStep(step:any)
    {
        if (this.stepper)
        {
            this.stepper.previous();
        }
		
        this.checkButtons();
    }

    goStep(index:number)
    {
        if (this.stepper)
        {
            this.stepper.selectedIndex = index;
        }
        
        this.checkButtons();    
    }

    goStepInvalidStep()
    {
        for (const key in this.steps)
        {
            const step = this.steps[key];
            this.validateSave(step);
            this.validateStep(step);

            if (!step.isValid)
            {
                this.goStep(Number(key));
                break;
            }
        }
    }

    submittedStep(step:any, value:boolean)
    {
        for (const key in step.rows)
        {
            step.rows[key].component.instance.submitted = value;
        }
    }

    validateSave(step:any)
    {
        step.isValid = true;

        for (const key in step.rows)
        {
            if (!step.rows[key].component.instance.validateSave())
            {
                step.isValid = false;
                break;
            }
        }

        return step.isValid;
    }

    validateStep(step:any)
    {
        step.isValid = true;

        for (const key in step.rows)
        {
            if (!step.rows[key].component.instance.validate())
            {
                step.isValid = false;
                break;
            }
        }

        return step.isValid;
    }

    getStep(step:any)
    {
        if (step)
        {
            for (const key in this.steps)
            {
                if (step.id == this.steps[key].id)
                {
                    return this.steps[key];
                }
            }
			
            //return this.steps[0];
        }
        else
        {
            return this.steps[0];
        }
    }
    
    checkButtons()
    {
        /* OVERRIDE */
    }

    getTitle()
    {
        if (this.data && this.data._sequence)
        {
            return "Nº " + this.data._sequence + " - " + this.form.name;
        }
        else
        {
            return this.form.name;
        }		
    }

    async ngAfterViewInit()
    {
        const templates = this.templates.toArray();

        for (const key in this.items)
        {
            const formItem = this.items[key];
            const step 	   = this.getStep(formItem.step);

            if (step && !formItem.field.blocked)
            {				
                const row          = formItem.row;                
                const stepTemplate = templates[step.position];

                if (!step.template)
                {
                    step.template = stepTemplate;
                    step.rows     = [];
                    step.hiddens  = [];
                    step.items    = []; // PARA DISPLAYS COM CONTROLSTEPS
                }

                if (formItem.field.isHidden())
                {
                    const classes = [];
                    //classes.push(formItem.field.type.value.toLowerCase() + '-col');
					
                    const inputComponent = await this.addInput(formItem, templates[0], {
                        classes : classes
                    });
					
                    step.hiddens.push(inputComponent);
                }
                else
                {
                    step.items.push(formItem);
					
                    if (!step.rows[row])
                    {
                        const classes = [];
                        classes.push(formItem.field.type.value.toLowerCase() + "-row");

                        if (key == "0")
                        {
                            classes.push("first-row");
                        }

                        const rowComponent = await this.addPartial(PartialType.type("Row"), step.template, { classes : classes });
					
                        step.rows[row] = {
                            component : rowComponent,
                            total     : 0,
                            columns   : [],
                        };
                    }

                    /* INPUT */
                    const classes = [];
                    classes.push(formItem.field.type.value.toLowerCase() + "-col");

                    const colComponent = await step.rows[row].component.instance.addPartial(PartialType.type("Col"),
                        {
                            classes : classes
                        });

                    const inputComponent = await colComponent.instance.addInput(formItem, this.form.formGroup,
                        {
                            form  : this.form,
                            steps : this.steps,
                        });

                    colComponent.formItem = formItem;

                    /* ATTACHMENTS */
                    if (formItem.field.hasAttachments())
                    {
                        step.rows[row].component.instance.addPartial(PartialType.type("Attachments"),
                            {
                                formItem       : formItem,
                                inputComponent : inputComponent,
                            });
                    }

                    step.rows[row].columns.push(colComponent);
                    step.rows[row].total++;
                }
            }
        }

        const promises : Array<any> = [];

        /* CALCULAR RENDERER */
        for (const key in this.steps)
        {
            const step = this.steps[key];

            /* ROWS */
            for (const key2 in step.rows)
            {
                const row     = step.rows[key2];
                const promise = row.component.instance.isRendererComplete();

                promises.push(promise);

                // CALCULO DE COLUNAS
                for (const key3 in row.columns)
                {
                    const column = row.columns[key3];
                    const col    = 12 / row.total;
										
                    column.instance.attributes = [ { 
                        name  : "size",
                        value : 12
                    },
                    {
                        name  : "size-sm",
                        value : col,
                    } ];

                    if (column.formItem.field.instance)
                    {
                        column.formItem.field.instance.nameClass += "size-" + col + " ";
                    }
                }
            }

            /* HIDDENS */
            for (const key2 in step.hiddens)
            {
                const hidden  = step.hiddens[key2];
                const promise = hidden.instance.isRendererComplete();

                promises.push(promise);
            }
        }

        Promise.all(promises).then(() =>
        {
            this._resolve();

            /* DEPOIS */
            this.data = this._data;
			
            this.markForCheck();
            this.checkButtons();			

            this.rendererComplete();

            console.log("completo form all");
        }); 
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }
	
    rendererComplete()
    {
        /* OVERRIDE */
    }

    set submitted(value:boolean)
    {
        this._submitted = value;

        for (const key in this.steps)
        {
            const step = this.steps[key];

            /* INPUTS */
            for (const key2 in step.rows)
            {
                const row                        = step.rows[key2];
                row.component.instance.submitted = value;
            }
			
            /* HIDDENS */
            for (const key2 in step.hiddens)
            {
                const hidden              = step.hiddens[key2];
                hidden.instance.submitted = value;
            }            
        }        
    }

    get submitted()
    {
        return this._submitted;
    }

    set data(value:any)
    {
        this._data      = value;
        this.updateData = value;

        /* SET DATA AOS FILHOS */
        for (const key in this.steps)
        {
            const step = this.steps[key];

            /* INPUTS */
            for (const key2 in step.rows)
            {
                const row                   = step.rows[key2];
                row.component.instance.data = value;
            }
			
            /* HIDDENS */
            for (const key2 in step.hiddens)
            {
                const hidden         = step.hiddens[key2];
                hidden.instance.data = value;
            }
			
            /* SET COMPLETE AO STEP */
            if (value && value._steps && value._steps[step.id])
            {
                step.complete = true;
            }
        }	

        this.completeStep();
    }

    get data()
    {
        return this._data;
    }

    set updateData(value:any)
    {
        this._updateData = value;

        /* SET DATA AOS FILHOS */
        for (const key in this.steps)
        {
            const step = this.steps[key];

            /* INPUTS */
            for (const key2 in step.rows)
            {
                const row                         = step.rows[key2];
                row.component.instance.updateData = value;
            }
			
            /* HIDDENS */
            for (const key2 in step.hiddens)
            {
                const hidden               = step.hiddens[key2];
                hidden.instance.updateData = value;
            }                    
        }	
    }

    get updateData()
    {
        return this._updateData;
    }   

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    async addInput(formItem:any, template:any, args?:any)
    {
        args      = args || {};
        args.form = this.form;

        const inputComponent = await InputFactory.createComponent(formItem, template, this.form.formGroup, args);

        return inputComponent;
    }

    createParams : (args:any) => Params = function(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    core()
    {
        return Core.get()
    }

    clear()
    {
        for (const key in this.steps)
        {
            const step = this.steps[key];

            /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
            if (step.template)
            {
                step.template.clear();
            }

            /* INPUTS */
            for (const key2 in step.rows)
            {
                const row = step.rows[key2];
                row.component.destroy();
            }

            /* HIDDENS */
            for (const key2 in step.hiddens)
            {
                const hidden = step.hiddens[key2];
                hidden.destroy();
            }

            step.template = null;
            step.row      = null;
            step.hiddens  = null;
			
            if (this.subscriptions)
            {
                this.subscriptions.unsubscribe();
            }		
        }
    }
	
    ngOnDestroy()
    {		
        this.clear();
    }

    trackById(index:any, item:any) 
    {
        index; return item.id; 
    }
}
