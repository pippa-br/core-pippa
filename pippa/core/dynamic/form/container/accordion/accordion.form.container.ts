import { Component, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseFormContainer }  from '../base.form.container';

@Component({
    selector        : '[accordion-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        :  `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid }" 
							[formGroup]="form.formGroup">
							
							<mat-toolbar class="header-container" *ngIf="form.hasHeader">                            
								<h4 *ngIf="form.name">{{form.name}} <span *ngIf="data && data.name">- {{data.name}}</span></h4>                            
							</mat-toolbar>
							<mat-toolbar class="subheader-container" *ngIf="form.description">                            
								<div [innerHtml]="form.getDescription()"></div>                            
							</mat-toolbar>
							<mat-vertical-stepper #stepper [linear]="isLinear" labelPosition="bottom">

								<ng-template matStepperIcon="edit">
									<ion-icon name="checkmark-outline"></ion-icon>
								</ng-template>
							
								<!-- Define the number of the step -->                  
								<ng-template matStepperIcon="number" let-index="index">
									{{index+1}}
								</ng-template>
				
								<mat-step *ngFor="let step of steps" 
										  label="{{step.label}}" 	
										  [completed]="this.validateStep(step)"
										  class="wrapper-body-container">

									<div class="body-container">
										<ng-template #templates></ng-template>
									</div>

									<div class="error-container">
										{{form.error}}
									</div>									
										
								</mat-step>

							</mat-vertical-stepper>

							<mat-toolbar #footer class="footer-container" *ngIf="form.hasButtons">
								<div class="buttons">
									<ion-button *ngIf="form.hasCancelButton && isFirst && false"
												class="cancel-button" ion-button outline
												[disabled]="getCurrentStep()?.isValid"
												(click)="form.onCancel($event)">
										{{form.cancelText}}
									</ion-button>
									<ion-button *ngIf="!isFirst"
												class="prev prev-button" color="tertiary" outline
												(click)="onPrevStep(getCurrentStep())">
										Voltar
									</ion-button>                                                    
									<ion-button class="next
												next-button"
												color="tertiary"
												*ngIf="!isLast"
												(click)="onNextStep(getCurrentStep())">
										Avançar
									</ion-button>
									<ion-button *ngIf="(form.hasSubmitButton && isLast) || (core().user && core().user.isAdmin())"
											class="save-button"
											outline
											(click)="onSave(getCurrentStep())">
										{{(form.isAddModeForm() ? form.addText : form.setText) | translate}}
									</ion-button>
								</div>
							</mat-toolbar>
						</div>`,
})
export class AccordionFormContainer extends BaseFormContainer
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
	}
	
	/* OVERRIDE */
	displaySteps()
	{
		if(this.stepper)
		{
			for(const key in this.steps)
			{
				const step = this.steps[key];
	
				if(step.display)
				{
					this.renderer.removeClass(this.stepper._elementRef.nativeElement.children[key], 'hide');
				}
				else
				{
					this.renderer.addClass(this.stepper._elementRef.nativeElement.children[key], 'hide');
				}
			};
	
			this.markForCheck();
		}		
	}

	/* OVERRIDE */
	completeStep()
	{
		if(this.stepper)
		{
			for(const key in this.steps)
			{
				const step = this.steps[key];
	
				if(step.complete)
				{
					this.renderer.addClass(this.stepper._elementRef.nativeElement.children[key], 'complete');				
				}
				else
				{
					this.renderer.removeClass(this.stepper._elementRef.nativeElement.children[key], 'complete');
				}
			};
	
			this.markForCheck();
		}		
	}

	/* OVERRIDE */
	checkButtons()
	{
		if(this.stepper)
		{
			this.isFirst = this.stepper.selectedIndex === 0 && this.stepper._steps.length > 0;
			this.isLast  = this.stepper.selectedIndex === this.stepper._steps.length - 1;			
		}
	}
}
