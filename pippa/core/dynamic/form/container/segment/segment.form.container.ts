import { Component, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseFormContainer }  from '../base.form.container';

@Component({
    selector        : '[segment-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid }"
                            [formGroup]="form.formGroup">
                                                    
                            <mat-toolbar class="subheader-container" *ngIf="form.getDescription()">
                                <div [innerHtml]="form.getDescription()"></div>
                            </mat-toolbar>
                            <div class="wrapper-body-container">
                                <mat-tab-group class="steps" #stepper (selectedIndexChange)="onSelectedIndexChange($event)">
                                    <mat-tab *ngFor="let step of steps; let isFirst = first; let isLast = last;trackBy : trackById"                                         
											 label="{{step.label}}"
											 [ngClass]="{ 'hidden' : step.hidden }">
                                        <div class="body-container">
                                            <ng-template #templates></ng-template>
                                        </div>
                                    </mat-tab>
                                </mat-tab-group>
                            </div>
                            <div class="error-container" *ngIf="submitted">
								{{form.error | translate}}
                            </div>
                            <mat-toolbar #footer class="footer-container" *ngIf="form.hasButtons && !isValid">
                                <div class="buttons">  
                                    <ion-button [disabled]="isFirst"
                                                class="prev-button" outline
                                                (click)="goStep(stepper.selectedIndex - 1)">
                                        <ion-icon name="chevron-back-outline"></ion-icon>
                                    </ion-button>
                                    <ion-button *ngIf="form.hasCancelButton"
                                            type="button"
                                            class="cancel-button" ion-button outline
                                            [disabled]="isValid"
                                            (click)="form.onCancel($event)">
                                        <ion-icon name="{{form.iconCancel}}"></ion-icon>&nbsp;{{form.cancelText | translate}}
                                    </ion-button>
                                    <ion-button *ngIf="form.hasSubmitButton"
                                            type="button"
                                            class="save-button" ion-button outline
                                            [disabled]="isValid"
                                            (click)="form.onSave()">

                                        <ion-icon *ngIf="form.isAddModeForm()" name="{{form.iconAdd}}"></ion-icon>
                                        <ion-icon *ngIf="form.isSetModeForm()" name="{{form.iconSet}}"></ion-icon>

                                        &nbsp;{{(form.isAddModeForm() ? form.addText : form.setText ) | translate}}

                                    </ion-button>
                                    <ion-button class="next-button"
                                                [disabled]="isLast" 
                                                outline
												(click)="goStep(stepper.selectedIndex + 1)">
                                        <ion-icon name="chevron-forward-outline"></ion-icon>
									</ion-button>
                                </div>                                
                            </mat-toolbar>

                        </div>`,
})
export class SegmentFormContainer extends BaseFormContainer
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
    }

	/* OVERRIDE */
	displaySteps()
	{
		if(this.stepper)
		{
			for(const key in this.steps)
			{
				const step = this.steps[key];

				if(step.display)
				{
					this.renderer.removeClass(this.stepper._tabHeader._items._results[key].elementRef.nativeElement, 'hide');
				}
				else
				{
					this.renderer.addClass(this.stepper._tabHeader._items._results[key].elementRef.nativeElement, 'hide');
				}
			};

			this.markForCheck();
		}		
	}

    /* OVERRIDE */
	checkButtons()
	{
		if(this.stepper)
		{
			this.isFirst = this.stepper.selectedIndex === 0 && this.steps.length > 0;
			this.isLast  = this.stepper.selectedIndex === this.steps.length - 1;                    
		}
	}

    /* OVERRIDE */
	getCurrentStep()
    {        
		if(this.steps && this.steps[this.stepper.selectedIndex])
		{
			return this.steps[this.stepper.selectedIndex];
		}

		return;
    }

    onSelectedIndexChange(event:any)
    {
        this.checkButtons();
    }
}
