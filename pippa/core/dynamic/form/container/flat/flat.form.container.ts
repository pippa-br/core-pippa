import { Component, ChangeDetectorRef, Renderer2, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';

/* PIPPA */
import { BaseFormContainer } from '../base.form.container';

@Component({
    selector        : '[flat-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid, 'horizontal-steps' : horizontal}"
                            [formGroup]="form.formGroup">

                            <mat-toolbar class="header-container" *ngIf="form.hasHeader">
                                <h4 *ngIf="form.name">{{getTitle()}}</h4>
                            </mat-toolbar>
                            <mat-toolbar class="subheader-container" *ngIf="form.getDescription()">
                                <div [innerHtml]="form.getDescription()"></div>
                            </mat-toolbar>
                            <div class="wrapper-body-container">
                                <div class="steps" *ngFor="let step of steps; trackBy : trackById; let i = index;">
                                    <div class="title-container" *ngIf="step.label && steps.length > 1">
                                        <span class="icon">{{i + 1}}</span> <label>{{step.label}}</label>
                                    </div>
                                    <div class="body-container">
                                        <ng-template #templates></ng-template>
                                    </div>
                                </div>   
                            </div>               
                            <div class="error-container">{{form.error | translate}}</div>                       
                            <mat-toolbar #footer class="footer-container" *ngIf="form.hasButtons && !isValid">
                                <div class="buttons">  
                                    <ion-button *ngIf="form.isSetModeForm() || form.hasCancelButton"
                                            type="button"
                                            class="cancel-button"
                                            [disabled]="isValid"
                                            (click)="form.onCancel($event)">
                                        <ion-icon name="{{form.iconCancel}}"></ion-icon>&nbsp;{{form.cancelText | translate}}
                                    </ion-button>
                                    <ion-button *ngIf="form.hasSubmitButton"
											id="{{form.saveId}}"
                                            type="button"
                                            class="save-button"
                                            [disabled]="isValid"
                                            (click)="onSave()">

                                        <ion-icon *ngIf="form.isAddModeForm()" name="{{form.iconAdd}}"></ion-icon>
                                        <ion-icon *ngIf="form.isSetModeForm()" name="{{form.iconSet}}"></ion-icon>

                                        &nbsp;{{(form.isAddModeForm() ? form.addText : form.setText) | translate}}

                                    </ion-button>
                                </div>                        
                            </mat-toolbar>

                    </div>`,
        })
export class FlatFormContainer extends BaseFormContainer implements AfterViewInit
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
    }
}
