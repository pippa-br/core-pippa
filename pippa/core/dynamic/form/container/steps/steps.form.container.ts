import { Component, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseFormContainer } from '../base.form.container';

@Component({
    selector        : '[steps-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid }" 
                            [formGroup]="form.formGroup">
                            
                            <mat-toolbar class="header-container" *ngIf="form.hasHeader">                            
                                <h4 *ngIf="form.name">{{form.name}} <span *ngIf="data && data.name">- {{data.name}}</span></h4>                            
                            </mat-toolbar>
                            <mat-toolbar class="subheader-container" *ngIf="form.description">                            
                                <div [innerHtml]="form.getDescription()"></div>                            
                            </mat-toolbar>

							<ion-grid>
 
								<ion-row>
									<ion-col size="12" size-sm="3">
										<div class="cards-container">
											<mat-card *ngFor="let step of steps; let i = index" 
													  class="step-card" 
													  [ngClass]="step.complete || form.isSetModeForm() ? 'complete' : (getCurrentStep()?.id == step.id ? 'current' : '')">

												<mat-card-title>
													{{step.label}} 
													<a *ngIf="step.complete || form.isSetModeForm()" (click)="goStep(i)">
														<ion-icon name="create-outline"></ion-icon>
													</a>
												</mat-card-title>	
												
												<mat-card-content *ngIf="updateData && (step.complete || form.isSetModeForm())">
													<p *ngFor="let info of step.infos">
														{{info.label}}: <span [innerHtml]="updateData | property : info.path | async"></span>
													</p>													
												</mat-card-content>
													
											</mat-card>	
										</div>										
									</ion-col>
									<ion-col size="12" size-sm="9">
										<mat-horizontal-stepper #stepper [linear]="isLinear" labelPosition="bottom">

											<ng-template matStepperIcon="edit">
												<ion-icon name="checkmark-outline"></ion-icon>
											</ng-template>
										
											<!-- Define the number of the step -->                  
											<ng-template matStepperIcon="number" let-index="index">
												{{index+1}}
											</ng-template>

											<mat-step *ngFor="let step of steps" 
													label="{{step.label}}" 	
													[completed]="validateStep(step)"											  
													class="wrapper-body-container">

												<div class="body-container">
													<ng-template #templates></ng-template>
												</div>

												<div class="error-container">
													{{form.error | translate}}
												</div>                       										
													
											</mat-step>

										</mat-horizontal-stepper>
									</ion-col>
								</ion-row>

							</ion-grid>							

							<mat-toolbar #footer class="footer-container" *ngIf="form.hasButtons">
								<div class="buttons">
									<div class="start">
										<ion-button *ngIf="!isFirst"
													class="cancel-button" outline
													(click)="onPrevStep(getCurrentStep())">
											{{form.prevText | translate}}
										</ion-button>                                                    									
									</div>
									<div class="end">
										<ion-button class="next
													next-button"
													class="cancel-button" outline
													*ngIf="!isLast"
													(click)="onNextStep(getCurrentStep())">
											{{form.nextText | translate}}
										</ion-button>
										<ion-button *ngIf="form.hasCancelButton && isFirst && false"
													class="cancel-button" ion-button outline
													[disabled]="getCurrentStep()?.isValid"
													(click)="form.onCancel($event)">
											{{form.cancelText | translate}}
										</ion-button>						
										<ion-button *ngIf="(form.hasSubmitButton && isLast)"
												class="save-button"
												outline
												(click)="onSave(getCurrentStep())">
											{{(form.isAddModeForm() ? form.addText : form.setText) | translate}}
										</ion-button>
									</div>									
								</div>
							</mat-toolbar>
							
                        </div>`,
})
export class StepsFormContainer extends BaseFormContainer
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
    }

	/* OVERRIDE */
	checkButtons()
	{
		if(this.stepper)
		{
			this.isFirst = this.stepper.selectedIndex === 0 && this.stepper._steps.length > 0;
			this.isLast  = this.stepper.selectedIndex === this.stepper._steps.length - 1;					
		}
	}

	/* OVERRIDE */
	getCurrentStep()
    {        
		if(this.steps && this.stepper && this.steps[this.stepper.selectedIndex])
		{
			return this.steps[this.stepper.selectedIndex];
		}

		return;
    }
}
