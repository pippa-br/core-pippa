import { Component, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseFormContainer } from '../base.form.container';

@Component({
    selector        : '[wizard-form-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : isValid, 'ng-invalid' : !isValid }" 
                            [formGroup]="form.formGroup">
                            
                            <mat-toolbar class="header-container" *ngIf="form.hasHeader">                            
                                <h4 *ngIf="form.name">{{form.name}} <span *ngIf="data && data.name">- {{data.name}}</span></h4>                            
                            </mat-toolbar>
                            <mat-toolbar class="subheader-container" *ngIf="form.description">                            
                                <div [innerHtml]="form.getDescription()"></div>                            
                            </mat-toolbar>
							<mat-horizontal-stepper #stepper [linear]="isLinear" labelPosition="bottom">

									<ng-template matStepperIcon="edit">
										<ion-icon name="checkmark-outline"></ion-icon>
									</ng-template>
								
									<!-- Define the number of the step -->                  
									<ng-template matStepperIcon="number" let-index="index">
										{{index+1}}
									</ng-template>

									<mat-step *ngFor="let step of steps" 
											  label="{{step.label}}" 	
											  [completed]="validateStep(step)"											  
											  class="wrapper-body-container">

										<div class="body-container">
											<ng-template #templates></ng-template>
										</div>

										<div class="error-container">
                                			{{form.error | translate}}
                            			</div>                       										
											
									</mat-step>

							</mat-horizontal-stepper>

							<mat-toolbar #footer class="footer-container" *ngIf="form.hasButtons">
								<div class="buttons">
									<ion-button *ngIf="form.hasCancelButton && isFirst && false"
												class="cancel-button" ion-button outline
												[disabled]="getCurrentStep()?.isValid"
												(click)="form.onCancel($event)">
										{{form.cancelText | translate}}
									</ion-button>
									<ion-button *ngIf="!isFirst"
												class="cancel-button" outline
												(click)="onPrevStep(getCurrentStep())">
										{{form.prevText | translate}}
									</ion-button>
									<ion-button class="next
												next-button"
												class="cancel-button" outline
												*ngIf="!isLast"
												(click)="onNextStep(getCurrentStep())">
										{{form.nextText | translate}}
									</ion-button>
									<ion-button *ngIf="(form.hasSubmitButton && isLast)"
											class="save-button"
											outline
											(click)="onSave(getCurrentStep())">
										{{(form.isAddModeForm() ? form.addText : form.setText) | translate}}
									</ion-button>
								</div>
							</mat-toolbar>
							
                        </div>`,
})
export class WizardFormContainer extends BaseFormContainer
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public renderer          : Renderer2,
    )
    {
        super(changeDetectorRef, renderer);
    }

	/* OVERRIDE */
	checkButtons()
	{
		if(this.stepper)
		{
			this.isFirst = this.stepper.selectedIndex === 0 && this.stepper._steps.length > 0;
			this.isLast  = this.stepper.selectedIndex === this.stepper._steps.length - 1;					
		}
	}

	/* OVERRIDE */
	getCurrentStep()
    {        
		if(this.steps && this.steps[this.stepper.selectedIndex])
		{
			return this.steps[this.stepper.selectedIndex];
		}

		return;
    }
}
