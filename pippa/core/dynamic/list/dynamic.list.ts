import { Component, Input, ViewContainerRef, OnDestroy, Output, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter } from "@angular/core";
import { Subscription } from "rxjs";
import { PopoverController, AlertController, ModalController } from "@ionic/angular";
import * as elementResizeEvent from "element-resize-event";
import * as Unbind from "element-resize-event";

/* PIPPA */
import { Grid }                  from "../../../core/model/grid/grid";
import { Core }                  from "../../util/core/core";
import { TableListContainer }    from "./container/table/table.list.container";
import { TileListContainer }     from "./container/tile/tile.list.container";
import { CardListContainer }     from "./container/card/card.list.container";
import { TabListContainer }      from "./container/tab/tab.list.container";
import { MapListContainer } 	 from "./container/map/map.list.container";
import { CalendarListContainer } from "./container/calendar/calendar.list.container";
import { SlideListContainer }    from "./container/slide/slide.list.container";
import { Form }                  from "../../../core/model/form/form";
import { FilterPlugin }          from "../../../core/plugin/filter/filter.plugin";

@Component({ 
    selector        : "[dynamic-list]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [hidden]="!grid || !collection || _isRendererComplete">
                            <ion-progress-bar color="medium" type="indeterminate"></ion-progress-bar>
                       </div>
                       <div [hidden]="!_isRendererComplete">
                            <div *ngIf="grid && grid.description" class="info" [innerHtml]="grid.description"></div>
                            <ng-template #template></ng-template>
                       </div>`,
})
export class DynamicList implements OnDestroy
{
    public _gridSubscribe      : any;
    public forms               : Array<Form>;
    public listContainer       : any;
    public overlayForm         : any;
    public _subscriptions      : Subscription;
    public _grid               : any;
    public _collection         : any;
    public _plugin             : any;
    public _isRendererComplete  = false;
    public _runDisplay          = false;
    public setting             : any;

    @Input() fixedHeader  = true;
    @Input() acl         : any;

    @Output("add")              addEvent              : EventEmitter<any> = new EventEmitter();
    @Output("set")              setEvent              : EventEmitter<any> = new EventEmitter();
    @Output("clone")            cloneEvent            : EventEmitter<any> = new EventEmitter();
    @Output("del")              delEvent              : EventEmitter<any> = new EventEmitter();
    @Output("cancel")           cancelEvent           : EventEmitter<any> = new EventEmitter();
    @Output("select")           selectEvent           : EventEmitter<any> = new EventEmitter();	
    @Output("archive")          archiveEvent          : EventEmitter<any> = new EventEmitter();
	@Output("batch")            batchEvent            : EventEmitter<any> = new EventEmitter();
	@Output("viewer")           viewerEvent           : EventEmitter<any> = new EventEmitter();
	@Output("email")            emailEvent            : EventEmitter<any> = new EventEmitter();
    @Output("sort")             sortEvent             : EventEmitter<any> = new EventEmitter();
    @Output("reload")           reloadEvent           : EventEmitter<any> = new EventEmitter();
	@Output("rendererComplete") rendererCompleteEvent : EventEmitter<any> = new EventEmitter();
	@Output("collectionChange") collectionChangeEvent : EventEmitter<any> = new EventEmitter();
    @Output("rendererClear")    rendererClearEvent    : EventEmitter<any> = new EventEmitter();

    @ViewChild("template", { read : ViewContainerRef, static : true }) template : ViewContainerRef;

    constructor(
        public popoverController : PopoverController,
        public alertController   : AlertController,
        public modalController   : ModalController,
        public filterPlugin      : FilterPlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
    }

    @Input()
    set grid(data:any)
    {
        if (data)
        {
            data.on(true).then(() =>
            {
                this._grid         = data;
                this.grid.instance = this;
                this.setting       = data.setting || {};

                if (this.grid.plugin)
                {
                    this._plugin = this.core().getPlugin(this._grid.plugin);
                }

                this._gridSubscribe = this._grid.items.onUpdate().subscribe(() =>
                {
                    console.log("grid change display");

                    this.display();
                });

                this.display();
            });
        }
    }

    get grid():any
    {
        return this._grid;
    }

    @Input()
    set collection(data:any)
    {
        this._collection = data;

        if (data)
        {
            this.display();
        }
    }

    get collection()
    {
        return this._collection;
    }

    display() 
    {
        return new Promise((resolve:any) =>
        {
            if (this.grid && this.collection && !this._runDisplay)
            {
                console.log("display list", this.collection.length);

                this._runDisplay = true;
	
                let container : any;
	
                if (this.grid.type.value == Grid.TILE_TYPE.value)
                {
                    container = TileListContainer;
                }
                else if (this.grid.type.value == Grid.CARD_TYPE.value)
                {
                    container = CardListContainer;
                }
                else if (this.grid.type.value == Grid.SLIDE_TYPE.value)
                {
                    container = SlideListContainer;
                }
                else if (this.grid.type.value == Grid.TAB_TYPE.value)
                {
                    container = TabListContainer;
                }
                else if (this.grid.type.value == Grid.MAP_TYPE.value)
                {
                    container = MapListContainer;
                }
                else if (this.grid.type.value == Grid.CALENDAR_TYPE.value)
                {
                    container = CalendarListContainer;
                }
                else
                {
                    container = TableListContainer;
                }
	
                this.clear().then(async () =>
                {
                    /* COMPOENT GRID */
                    this.listContainer = await this.core().componentFactory.create(this.template, container);
						
                    this.template.insert(this.listContainer.hostView);
	
                    this.listContainer.instance.fixedHeader = this.fixedHeader;
                    this.listContainer.instance.acl         = this.acl;
					
                    /* RESIZE IFRAME: QUANDO RESIZE */
                    if (window.parent)
                    {
                        elementResizeEvent(this.listContainer.location.nativeElement, () => 
                        {
                            this.dispatchPostMessage();
                        });
                    } 
	
                    this.listContainer.instance.createComponents(this.grid, this.collection).then(() =>
                    {                                    
                        /* ISSO GARANTI OS OBJETOS SERAM DESTRUIDOS */
                        setTimeout(() => 
                        {
                            /* ADD EVENT */
                            this._subscriptions.add(this.listContainer.instance.addEvent.subscribe((event:any) =>
                            {
                                this.emitAdd(event);
                            }));
	
                            /* SET EVENT */
                            this._subscriptions.add(this.listContainer.instance.setEvent.subscribe((event:any) =>
                            {
                                this.emitSet(event);
                            }));

                            /* CLONE EVENT */
                            this._subscriptions.add(this.listContainer.instance.cloneEvent.subscribe((event:any) =>
                            {
                                this.emitClone(event);
                            }));
	
                            /* DEL EVENT */
                            this._subscriptions.add(this.listContainer.instance.delEvent.subscribe((event:any) =>
                            {
                                this.emitDel(event);
                            }));                            
	
                            /* CANCEL EVENT */
                            this._subscriptions.add(this.listContainer.instance.cancelEvent.subscribe((event:any) =>
                            {
                                this.emitCancel(event);
                            }));

                            /* SELECT EVENT */
                            this._subscriptions.add(this.listContainer.instance.selectEvent.subscribe((event:any) =>
                            {
                                this.emitSelect(event);
                            }));

                            /* BATCH EVENT */
                            this._subscriptions.add(this.listContainer.instance.batchEvent.subscribe((event:any) =>
                            {
                                this.emitBatch(event);
                            }));
	
                            /* DEL EVENT */
                            this._subscriptions.add(this.listContainer.instance.archiveEvent.subscribe((event:any) =>
                            {
                                this.emitArchive(event);
                            }));
	
                            /* VIEWER EVENT */
                            this._subscriptions.add(this.listContainer.instance.viewerEvent.subscribe((event:any) =>
                            {
                                this.emitViewer(event);                            
                            }));

                            /* EMAIL EVENT */
                            this._subscriptions.add(this.listContainer.instance.emailEvent.subscribe((event:any) =>
                            {
                                this.emitEmail(event);                            
                            }));

                            /* SORT EVENT */
                            this._subscriptions.add(this.listContainer.instance.sortEvent.subscribe((event:any) =>
                            {
                                this.emitSort(event);                            
                            }));
							
                            this.changeDetectorRef.markForCheck();
	
                            this._isRendererComplete = true;
                            this.rendererCompleteEvent.emit();
	
                            this._runDisplay = false;
							
                            /* RESIZE IFRAME: QUANDO LOAD */
                            if (window.parent)
                            {
                                this.dispatchPostMessage();
                            } 
	
                            console.log("dynamic-list hostView");   
							
                            resolve();
                        });                    
                    });
                });                         
            }
            else
            {
                resolve();
            }     
        });           
    }    
	
    dispatchPostMessage()
    {
        setTimeout(() => 
        {   
            window.parent.postMessage({
                type   : "resize", 
                height : this.listContainer.location.nativeElement.scrollHeight,
                target : this.grid.id,
            }, 
            "*");
        }, 500);
    }

    clear()
    {
        return new Promise((resolve:any) =>
        {
            /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
            if (this.template)
            {
                this.template.clear();
            }

            if (this.listContainer)
            {
                this.listContainer.destroy();
            }

            //if(this._subscriptions)
            //{
            //this._subscriptions.unsubscribe();                 
            //}

            /* NEW FAZ unsubscribe */
            this._subscriptions = new Subscription();

            this._isRendererComplete = false;

            console.log("clear list", (this.grid && this.grid.items ? this.grid.items.length : 0));            

            if (this.listContainer)
            {
                Unbind.unbind(this.listContainer.location.nativeElement);
            }

            this.rendererClearEvent.emit();

            resolve();            
        });
    }

    hasAcl(name:string, value?:any)
    {
        return this.acl && this.acl.hasKey(name, value);
    }

    core()
    {
        return Core.get();
    }

    emitViewer(event:any)
    {
        this.viewerEvent.emit(event);
    }
	
    emitEmail(event:any)
    {
        this.emailEvent.emit(event);
    }

    emitSort(event:any)
    {
        this.sortEvent.emit(event);
    }

    emitReload()
    {
        this.reloadEvent.emit();
    }

    emitAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    emitSet(event:any)
    {
        this.setEvent.emit(event);
    }

    emitArchive(event:any)
    {
        this.archiveEvent.emit(event);
    }

    emitBatch(event:any)
    {
        this.batchEvent.emit(event);
    }

    emitCancel(event:any)
    {
        this.cancelEvent.emit(event);
    }

    emitSelect(event:any)
    {
        this.selectEvent.emit(event);
    }

    emitClone(event:any)
    {
        this.cloneEvent.emit(event);
    }

    emitDel(event:any)
    {
        this.delEvent.emit(event);
    }

    destroy()
    {
        this.ngOnDestroy();
    }

    ngOnDestroy()
    {
        console.log("ngOnDestroy DynamicList");
        this.clear();

        if (this._gridSubscribe)
        {
            this._gridSubscribe.unsubscribe();
        }
    }
}
