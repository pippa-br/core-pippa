import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { PartialType }       from "../../../../type/partial/partial.type";
import { PartialFactory }    from "../../../../factory/partial/partial.factory";
import { BaseListContainer } from "../base.list.container";
import { IconType }          from "../../../../type/icon/icon.type";

@Component({
    selector        : "[tile-list-container]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-grid>
                            <ng-template #template></ng-template>
                       </ion-grid>`
})
export class TileListContainer extends BaseListContainer
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    getCols()
    {
        if (this.grid)
        {
            if (this.grid.cols == 6)
            {
                return [ {
                    name  : "size",
                    value : 12
                },
                {
                    name  : "size-sm",
                    value : 12,
                },
                {
                    name  : "size-sm",
                    value : 12
                },
                {
                    name  : "size-md",
                    value : 3,
                },
                {
                    name  : "size-lg",
                    value : 2
                },
                {
                    name  : "size-xl",
                    value : 2,
                } ];
            }
            else if (this.grid.cols == 5)
            {
                return [ {
                    name  : "size",
                    value : 12
                },
                {
                    name  : "size-sm",
                    value : 12,
                },
                {
                    name  : "size-sm",
                    value : 12
                },
                {
                    name  : "size-md",
                    value : 3,
                },
                {
                    name  : "size-lg",
                    value : 2.4
                },
                {
                    name  : "size-xl",
                    value : 2.4,
                } ];
            }
            else if (this.grid.cols == 4)
            {
                return [ {
                    name  : "size",
                    value : 12
                },
                {
                    name  : "size-sm",
                    value : 12,
                },
                {
                    name  : "size-sm",
                    value : 12
                },
                {
                    name  : "size-md",
                    value : 4,
                },
                {
                    name  : "size-lg",
                    value : 3
                },
                {
                    name  : "size-xl",
                    value : 3,
                } ];
            }
            else if (this.grid.cols == 3)
            {
                return [
                    {
                        name  : "size",
                        value : 12
                    },
                    {
                        name  : "size-sm",
                        value : 12
                    },
                    {
                        name  : "size-md",
                        value : 6
                    },
                    {
                        name  : "size-lg",
                        value : 4
                    },
                    {
                        name  : "size-xl",
                        value : 4,
                    } ];
            }
            else if (this.grid.cols == 2)
            {
                return [
                    {
                        name  : "size",
                        value : 12
                    },
                    {
                        name  : "size-sm",
                        value : 12
                    },
                    {
                        name  : "size-md",
                        value : 6
                    },
                    {
                        name  : "size-lg",
                        value : 6
                    },
                    {
                        name  : "size-xl",
                        value : 6
                    } ];
            }
        }
    }

    drawList()
    {
        return new Promise<void>(async (resolve) =>
        {
            const rowComponent = await this.addPartial(PartialType.type("Row"), this.template,
                {
                    classes : [ "tile-box" ],
                });

            this.components.push(rowComponent);

            /* COLLECTION */
            for (const key in this.collection)
            {
                if (!this.collection[key]._isRenderer)
                {
                    this.drawItem(this.collection[key], rowComponent);
                    this.collection[key]._isRenderer = this.collection.isInfinite;
                }
            }

            /* RENDERER */
            const promises : any = [];

            for (const key in this.components)
            {
                const component = this.components[key];
                const promise   = component.instance.isRendererComplete();

                promises.push(promise);
            }

            Promise.all(promises).then(() =>
            {
                resolve();

                console.log("completo tile all");
            });
        });
    }

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    drawItem(data:any, rowComponent:any)
    {
        return new Promise(async () =>
        {
            const classes = [];

            if (data.active != undefined && !data.active)
            {
                classes.push("no-active"); 
            }

            const colComponent = await rowComponent.instance.addPartial(PartialType.type("Col"),
                {
                    attributes : this.getCols(),
                    classes    : classes
                });

            const tableComponent = await colComponent.instance.addPartial(PartialType.type("Table"), 
                {
                    classes : [ "striped" ]
                });

            const trComponent = await tableComponent.instance.addPartial(PartialType.type("Tr"));
            const tdComponent = await trComponent.instance.addPartial(PartialType.type("Td"));

            const buttonsComponent = await tdComponent.instance.addPartial(PartialType.type("Div"),
                {
                    classes : [ "output", "buttons" ]
                });

            /* GRID BUTTONS */
            for (const key in this.grid.buttons)
            {
                const button  = this.grid.buttons[key];
                let isEnabled = true;
				
                if (typeof button.isEnabled == "function")
                {
                    isEnabled = button.isEnabled(data);									
                }

                if (isEnabled)
                {
                    const buttonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                        {
                            icon    : button.icon,
                            data    : data,
                            tooltip : button.label,
                        });

                    if (buttonComponent.instance.click && button.click)
                    {
                        buttonComponent.instance.click.subscribe((event:any) =>
                        {
                            button.click(event);
                        });
                    }
                }
            }

            /* SET BUTTON */
            this.displaySetButton(buttonsComponent, data);

            /* DEL BUTTON */
            this.displayDelButton(buttonsComponent, data);

            /* COLUMNS */
            let j = 0;

            /* ITEMS */
            for (const key in this.grid.items)
            {
                const formItem = this.grid.items[key];

                const trComponent = await tableComponent.instance.addPartial(PartialType.type("Tr"));
                const tdComponent = await trComponent.instance.addPartial(PartialType.type("Td"), 
                    {
                        classes : [ "col-" + formItem.field.type.value ]
                    });

                if (j == 0)
                {
                    /* O ONCLICK PODE SER PASSADO POR PARAMETRO EM CODIGO */
                    if (!formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            console.log("viewer click");

                            if (event.data.active == undefined || event.data.active)
                            {
                                this.emitViewer(event);
                            }
                        };
                    }
                }

                formItem.onAdd = (event:any) =>
                {
                    this.emitAdd(event);
                };

                /* LABEL */
                if (formItem.hasLabel)
                {
                    await tdComponent.instance.addPartial(PartialType.type("Label"),
                        {
                            label : formItem.label,
                        });
                }

                /* ADD OUTPUT */
                await  tdComponent.instance.addOutput(formItem, data);

                j++;
            }
        });
    }

    async displaySetButton(buttonsComponent : any, data : any)
    {
        if (this.hasSetButton(data))
        {
            const setButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.CREATE,
                    data    : data,
                    classes : [ "set-button" ],
                    tooltip : "Editar",
                });

            if (setButtonComponent.instance.click)
            {
                setButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitSet(event);
                });
            }
        }
    }

    async displayDelButton(buttonsComponent : any, data : any)
    {
        if (this.hasDelButton(data))
        {
            const delButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.TRASH,
                    data    : data,
                    classes : [ "del-button" ],
                    tooltip : "Remover",
                });

            if (delButtonComponent.instance.click)
            {
                delButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitDel(event);
                });
            }
        }
    }
}
