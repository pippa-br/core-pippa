import { Component, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { AlertController, ModalController, PopoverController } from "@ionic/angular";
import * as FileSaver from "file-saver";
import { Browser } from "@capacitor/browser";

/* PIPPA */
import { BaseListContainer }  from "../base.list.container";
import { PartialType }        from "../../../../type/partial/partial.type";
import { PartialFactory }     from "../../../../factory/partial/partial.factory";
import { ColumnOrderPopover } from "../../../../popover/column.order/column.order.popover";
import { PrintPlugin }        from "../../../../plugin/print/print.plugin";
import { DynamicList }        from "../../../../dynamic/list/dynamic.list";
import { ViewerPlugin }       from "../../../../plugin/viewer/viewer.plugin";
import { FieldType }          from "../../../..//type/field/field.type";
import { FormItem }           from "../../../..//model/form.item/form.item";
import { Field }              from "../../../../model/field/field";
import { Grid }               from "../../../../model/grid/grid";
import { IconType }           from "../../../../type/icon/icon.type";
import { DocumentPlugin }     from "../../../../plugin/document/document.plugin";
import { FunctionsType }	    from "../../../../type/functions/functions.type";
import { TranslatePipe } 	    from "../../../../pipe/translate/translate.pipe";
import { ListPopover } 		    from "../../../../popover/list/list.popover";
import { BaseModel } 				  from "src/core-pippa/pippa/core/model/base.model";
import { FormFormPopover } 	  from "src/core-pippa/pippa/core/popover/form.form/form.form.popover";
import { StoragePlugin }		  from "src/core-pippa/pippa/core/plugin/storage/storage.plugin";
import { environment } from "src/environments/environment";

@Component({
    selector        : "[table-list-container]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="tabela-box">
                            <table id="dynamicTable" class="striped" [ngClass]="'table-level-' + _level">
                                <ng-template #template></ng-template>
                            </table>
                      </div>`,
})
export class TableListContainer extends BaseListContainer implements OnDestroy
{
    public _level  			   = 0;
    public _viewer 			  : any; /* PARA IMPRESSÃO */
    public _mapsGroupBy;
    public isRendererHeader    = false;
    public translatePipe 	  : any;
    public alertController    : any
    public storagePlugin      : any
    public selectedComponents : any

    constructor(
        public popoverController : PopoverController,
        public changeDetectorRef : ChangeDetectorRef,		
    )
    {
        super(changeDetectorRef);

        this.translatePipe   = new TranslatePipe();
        this.alertController = this.core().injector.get(AlertController);
        this.storagePlugin   = this.core().injector.get(StoragePlugin);
    }

    drawList()
    {
        return new Promise<void>(async (resolve) =>
        {			
            this.selectedComponents = [];

            /* HEADER */
            if (this.grid.hasHeader)
            {
                await this.displayHeader();
            }       

            /* BODY */
            await this.displayBody();			

            /* RENDERER */
            const promises : any = [];

            for (const key in this.components)
            {
                const component = this.components[key];
                const promise   = component.instance.isRendererComplete();								

                promises.push(promise);
            }

            // BUG: SUBFORM OUTPUT VERIFCAR POIS NAO DAR DISPATCH
            Promise.all(promises).then(() =>
            {												
                resolve();

                console.log("completo table all", this.collection.length);		
            });
        });
    }	

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    async displayHeader()
    {
        if (!this.collection.isInfinite || !this.isRendererHeader)
        {
            this.isRendererHeader = true; 

            let classes = [];

            const theadComponent = await this.addPartial(PartialType.type("Thead"), this.template,
                {
                    classes : classes,
                });

            this.components.push(theadComponent);

            this._level = this.grid._level;

            classes = [];

            const rowComponent = await theadComponent.instance.addPartial(PartialType.type("Tr"), this.template,
                {
                    classes : classes,
                });

            /* ORDER */
            await this.displayBatch(rowComponent);

            let i = 0;
            let lastRow;
            let colComponent;

            const orderBy = await this.storagePlugin.get(this.grid.id + "_orderBy");
            const asc     = await this.storagePlugin.get(this.grid.id + "_asc");

            for (const key in this.grid.items)
            {
                const formItem = this.grid.items[key];

                if (formItem.enabled && lastRow != formItem.row)
                {
                    formItem.col   = i;
                    formItem.first = (i == 0);
                    formItem.last  = (this.grid.items.length - 1 == i);

                    const classes = [];

                    classes.push((i > 1 ? "hide-md" : ""));
                    classes.push(formItem.field.type.value.toLowerCase() + "-col");

                    colComponent = await rowComponent.instance.addPartial(PartialType.type("Th"),
                        {
                            classes : classes,
                        });	

                    const divComponent = await colComponent.instance.addPartial(PartialType.type("Div"),
                        {
                            classes : [ "buttons-div" ],
                        });

                    await divComponent.instance.addPartial(PartialType.type("Label"),
                        {
                            label  	: formItem.label,
                            tooltip : formItem.setting && formItem.setting.tooltip ? formItem.setting.tooltip : "",
                        });

                    if (formItem._sort)
                    {
                        const classes = [ "sort" ];
                        let icon      = IconType.ARROW_DOWN;

                        if (orderBy == formItem._name)
                        {
                            classes.push("selected");
                        }

                        if (asc == true)
                        {
                            icon = IconType.ARROW_UP;
                        }

                        const orderButtonComponent = await divComponent.instance.addPartial(PartialType.type("Button"),
                            {
                                classes : classes,
                                icon    : icon,
                                tooltip : "Ordernar",
                                data    : formItem,
                            });

                        this._subscriptions.add(orderButtonComponent.instance.click.subscribe(async (event:any) =>
                        {
                            await this.storagePlugin.set(this.grid.id + "_orderBy", event.data._name);
                            const asc = await this.storagePlugin.get(this.grid.id + "_asc");

                            if (orderBy == event.data._name)
                            {
                                await this.storagePlugin.set(this.grid.id + "_asc", !asc);
                            }

                            this.emitSort(event.data);
                        }));
                    }

                    i++;
                }

                lastRow = formItem.row;
            }

            if (!this.grid.disabledButtons)
            {
                /* BUTTONS */
                const colComponent = await rowComponent.instance.addPartial(PartialType.type("Th"),
                    {
                        classes : [ "buttons-col" ],
                    });

                const divComponent = await colComponent.instance.addPartial(PartialType.type("Div"),
                    {
                        classes : [ "buttons-div" ],
                    });

                /* ADD */
                await this.displayAddButton(divComponent);

                /* ORDER */
                //await this.displayOrderButton(divComponent);
            } 	
        }        
    }    

    async displayGroup(tbodyComponent, data)
    {
        if (this.grid.groupBy)
        {            
            let key : string;

            if (this.grid.groupBy.type.value == FieldType.type("Date").value)
            {
                key = await data.getProperty(this.grid.groupBy.value + ".format");
            }
            else if (this.grid.groupBy.type.value == FieldType.type("Select").value)
            {
                const item = await data.getProperty(this.grid.groupBy.value);

                key = item.value;
            }
            else if (this.grid.groupBy.type.value == FieldType.type("Text").value)
            {
                key = await data.getProperty(this.grid.groupBy.value);
            }

            if (!this._mapsGroupBy[key])
            {
                /* TR */
                let classes = [ "groupBy" ];
                classes.push(key);

                const rowComponent = await tbodyComponent.instance.addPartial(PartialType.type("Tr"),
                    {
                        classes : classes,                            
                    });                        

                /* OUTPUT */
                classes = [];                    

                const attributes = [ {
                    name  : "colspan",
                    value : this.grid.items.length + 1 + (this.hasBatch() ? 1 : 0),
                } ]

                classes.push(this.grid.groupBy.type.value.toLowerCase() + "-groupBy");

                const colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                    {
                        classes    : classes,
                        attributes : attributes,
                    });

                await colComponent.instance.addOutput(new FormItem({
                    field : new Field(this.grid.groupBy),
                    path  : this.grid.groupBy.value,
                }), data);                

                this._mapsGroupBy[key] = rowComponent;                            
            } 
        }
    }

    async displayBody()
    {
        this._mapsGroupBy = {};

        const classes = [];
        classes.push("body-level-" + this.grid._level);

        const tbodyComponent = await this.addPartial(PartialType.type("Tbody"), this.template,
            {
                classes : classes,
                grid    : this.grid
            });

        this.components.push(tbodyComponent);        

        /* DOCUMENTS */
        for (const key in this.collection)
        {
            const data = this.collection[key];

            // && data.exists talvez não precise mais
            if (!data._isRenderer)
            {				
                this.collection[key]._isRenderer = this.collection.isInfinite;			

                await this.displayGroup(tbodyComponent, data);

                /* TR */
                const classes = [];
                classes.push("tr-level-1");

                if (data._archive)
                {
                    classes.push("archive");
                }

                if (data._canceled)
                {
                    classes.push("canceled");
                }

                if (this.setting.trClasses)
                {
                    const trClasses = this.setting.trClasses.split(",")

                    for (const name of trClasses)
                    {
                        if (data[name])
                        {
                            classes.push(name);
                        }
                    }
                }

                const rowComponent = await tbodyComponent.instance.addPartial(PartialType.type("Tr"),
                    {
                        classes : classes,
                    });

                await this.displaySelect(rowComponent, data);

                let lastRow;
                let colComponent;

                /* ITEMS */
                for (const key in this.grid.items)
                {
                    const formItem = this.grid.items[key];

                    if (formItem.enabled)
                    {
                        /* OUTPUT */
                        const classes    = [];
                        const attributes = [];

                        /*if(this.hasViewerButton() && (
                        formItem.field.type.value == FieldType.type('Text').value ||
                        formItem.field.type.value == FieldType.type('Number').value ||
                        formItem.field.type.value == FieldType.type('Select').value ||
                        formItem.field.type.value == FieldType.type('Date').value ||
                        formItem.field.type.value == FieldType.type('Avatar').value
                        ))
                        {
                        /* O ONCLICK PODE SER PASSADO POR PARAMETRO EM CODIGO */
                        /*if(!formItem.onClick)
                        {
                        formItem.onClick = (event:any) =>
                        {
                        console.log('viewer click');

                        if(event.data.active == undefined || event.data.active)
                        {
                        this.emitViewer(event);
                        }
                        };
                        }

                        classes.push('hover');
                        }*/

                        classes.push(formItem.field.type.value.toLowerCase() + "-col");
                        attributes.push({
                            name  : "data-label",
                            value : formItem.label
                        });

                        if (lastRow != formItem.row)
                        {
                            colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                                {
                                    classes    : classes,
                                    attributes : attributes
                                });
                        }                       

                        // SUB FORM NAO APARECE NA GRID COMO TABLE
                        if (formItem.field.type.value == FieldType.type("SubForm").value)
                        {
                            if (!formItem.setting)
                            {
                                formItem.setting = {};
                            }

                            if (formItem.setting.isModal == undefined)
                            {
                                formItem.setting.isModal = true;
                            }							
                        }

                        // NÃO FAZ PARSE DE TRANSFORM
                        formItem.map = this.grid.map;
                        lastRow      = formItem.row;

                        colComponent.instance.addOutput(formItem, data);
                    }
                }

                await this.displayButtons(data, rowComponent, tbodyComponent);                 

                this.markForCheck();

                await this.childrenDisplay(2, data, tbodyComponent);
            }			            
        }  
    }

    async callableApi(router, params)
    {
        await this.core().loadingController.start();
        const result = await this.core().api.callable(router, params);

        await this.collection.reload();
        this.core().loadingController.stop();

        this.markForCheck();

        if (!result.status)
        {
            this.alertController.create({
                header  : "Ops! Aconteceu algo inesperado!",
                message : result.error,
                buttons : [ "OK" ]
            }).then((alert:any) =>
            {
                alert.present();
            });
        }

        return result;
    }

    async getExtraButtons(data)
    {
        const dropdownButtons = [];
        const buttons         = [];

        /* EXTRA BUTTONS */
        for (const key in this.grid.buttons)
        {
            const button  = this.grid.buttons[key];
            let   display = true;

            if (button.groups)
            {
                display = this.core().user.hasGroups(button.groups);
            }

            if (display && button.displayPath)
            {
                display = await data.getProperty(button.displayPath);

            }						

            if (display && button.hidePath)
            {
                const hide = await data.getProperty(button.hidePath);

                if (hide !== "")
                {
                    display = !hide;
                }						
            }
                    
            if (display && button.status)
            {
                const btn = {
                    icon    : button.icon,
                    label   : button.label,
                    data    : data,
                    tooltip : button.label,
                    onClick : async () => 
                    {
                        if (button.onClick)
                        {
                            button.onClick(event);
                        }
                        else if (button.type && button.type.value == "function")
                        {
                            const fc = FunctionsType.get().getFunction(button.router + ":click");

                            if (fc)
                            {
                                fc({
                                    data : data
                                });
                            }
                            else
                            {
                                console.log("this.core().functions not found", button.router);
                            }																	
                        }
                        else if (button.type && button.type.value == "edit")
                        {
                            const form : any = new BaseModel(button.params.form);
                            await form.on();

                            this.emitSet({ data : data, form : form });
                        }
                        else if (button.type && button.type.value == "form")
                        {
                            let params : any =  {
                                platform : this.core().platform,
                                mapItems : environment.defaultMapItems,
                                document : {
                                    referencePath : data.reference.path
                                },
                            };

                            params       = Object.assign({}, params, button.params);
                            const result = await this.callableApi(button.router, params);

                            const form : any = new BaseModel(button.params.openForm);
                            await form.on();

                            const dataModel = new BaseModel(result.data);
                            await dataModel.on();

                            const plugin          = this.core().injector.get(DocumentPlugin);
                            const modalController = this.core().injector.get(ModalController);

                            modalController.create({
                                component      : FormFormPopover,
                                componentProps : {
                                    form  : form,
                                    data  : dataModel,
                                    onSet : (async (event:any) =>
                                    {
                                        modalController.dismiss();

                                        if (button.params.edit)
                                        {
                                            await plugin.set(form.appid, data, form, event);
                                        }   
                                        else
                                        {
                                            await plugin.add(form.appid, form, event)
                                        }                                     

                                        setTimeout(() => 
                                        {
                                            this.collection.reload();
                                        });
                                    }),					
                                    onClose : () =>
                                    {
                                        modalController.dismiss();
                                    }
                                },
                                cssClass        : "full",
                                showBackdrop    : true,
                                backdropDismiss : false,
                            }).then(modal =>
                            {
                                modal.present();

                                modal.onDidDismiss().then(() =>
                                {
                                    this.core().isModal = false;
                                });
                            })
                        }
                        else if (button.type && button.type.value == "route")
                        {
                            let params : any = {
                                documentPath : data.reference.path
                            };

                            params = Object.assign({}, params, button.params);

                            this.core().push(button.router, params);	
                        }
                        else if (button.type && button.type.value == "url")
                        {
                            let url = await data.getProperty(button.router);

                            await this.core().loadingController.start();

                            if (url.indexOf("?") > -1)
                            {
                                url += "&v=" + new Date().getTime()
                            }
                            else
                            {
                                url += "?v=" + new Date().getTime()
                            }

                            await Browser.open({ url : url });

                            this.core().loadingController.stop();	
                        }
                        else if (button.type && button.type.value == "api")
                        {
                            let params : any = {
                                platform : this.core().platform,
                                document : {
                                    referencePath : data.reference.path
                                },
                            };

                            params = Object.assign({}, params, button.params);

                            if (button.params.hasAlert)
                            {
                                this.alertController.create({
                                    header  : "Alerta",
                                    message : "Deseja Prosseguir com " + button.label,
                                    buttons : [
                                        {
                                            text    : "Não",
                                            handler : () =>
                                            {
                                                this.alertController.dismiss();
                                                return false;
                                            }
                                        },
                                        {
                                            text    : "Sim",
                                            handler : async () =>
                                            {
                                                const result = await this.callableApi(button.router, params);
                                                this.alertController.dismiss();

                                                if (result.data && result.data._url)
                                                {
                                                    if (button.params.hasDownload)
                                                    {
                                                        FileSaver.saveAs(result.data._url);
                                                    }
                                                    else
                                                    {
                                                        window.open(result.data._url);
                                                    }
                                                }											

                                                return false;
                                            }
                                        }
                                    ]
                                }).then(alert =>
                                {
                                    alert.present(); 
                                });
                            }
                            else
                            {
                                const result = await this.callableApi(button.router, params);

                                if (result.data && result.data._url)
                                {
                                    if (button.params.hasDownload)
                                    {
                                        FileSaver.saveAs(result.data._url);
                                    }
                                    else
                                    {
                                        window.open(result.data._url);
                                    }
                                }							
                            }														
                        }
                    }
                };

                if (button.dropdown)
                {
                    dropdownButtons.push(btn);
                }
                else
                {
                    buttons.push(btn);
                }                
            }
        }

        return [ buttons, dropdownButtons ];
    }

    async childrenDisplay(level:number, data, tbodyComponent)
    {
        if (level <= this.grid._level)
        {
            const _children = await data.getProperty("_children");

            /* CHILDREN */
            if (_children && _children.length > 0)
            {
                /* CRIA PRIMEIRAMENTE AS LINAS POR CAUSA DO LOAD DO CHILDREN */
                const trs = [];

                for (const key in _children)
                {
                    const classes = [];
                    const data2   = _children[key];

                    classes.push("tr-level-" + level);

                    if (data._archive)
                    {
                        classes.push("archive");
                    }

                    if (data._canceled)
                    {
                        classes.push("canceled");
                    }

                    const rowComponent = await tbodyComponent.instance.addPartial(PartialType.type("Tr"),
                        {
                            classes : classes,
                        });

                    await this.displaySelect(rowComponent, data2);

                    await this.childrenDisplay(level + 1, data2, tbodyComponent);

                    trs[key] = rowComponent;
                }

                for (const key in _children)
                {
                    const rowComponent = trs[key];

                    const data2 = _children[key];
                    let colComponent;

                    let i = 0;
                    let lastRow;                    

                    for (const key in this.grid.items)
                    {
                        const formItem = this.grid.items[key];

                        if (formItem.enabled)
                        {
                            const classes = [];

                            classes.push((i > 1 ? "hide-md" : ""));
                            classes.push(formItem.field.type.value.toLowerCase() + "-col");

                            if (lastRow != formItem.row)
                            {
                                colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                                    {
                                        classes : classes,
                                    });
                            }

                            // NÃO FAZ PARSE DE TRANSFORM
                            formItem.map = this.grid.map;

                            colComponent.instance.addOutput(formItem, data2);

                            i++;
                            lastRow = formItem.row;
                        }
                    }

                    if (!this.grid.disabledButtons)
                    {
                        /* BUTTONS */
                        const colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                            {
                                classes : [ "buttons-col" ],
                            });

                        const divComponent = await colComponent.instance.addPartial(PartialType.type("Div"),
                            {
                                classes : [ "buttons-div" ],
                            });

                        let [ buttons, dropdownButtons ] = await this.getExtraButtons(data2);       
                        dropdownButtons                  = this.displaySetButton(dropdownButtons, data2, data, level);
                        dropdownButtons                  = this.displayArchiveButton(dropdownButtons, data2);
                        dropdownButtons                  = this.displayCancelButton(dropdownButtons, data2);
                        dropdownButtons                  = this.displayDelButton(dropdownButtons, data2, data);

                        //colComponent.instance.addPartial(PartialType.type('Label'));

                        /* VIEWER BUTTON */
                        //this.displayViewerButton(divComponent, data2);

                        /* PRINT */
                        //this.displayPrintButton(divComponent, data2);

                        /* ADD BUTTON */
                        await this.displayAddLevelButton(divComponent, data2, level + 1);

                        // EXTRA BUTTONS
                        await this.displayExtraButtons(buttons, data2, divComponent);

                        /* VIEWER BUTTON */
                        await this.displayViewerButton(divComponent, data2);

                        /* DEPOIS VER COMO FAZER POIS NÃO ESTOU NA PÁGINA PRINT */
                        //this.displayPrintButton(divComponent, data);

                        /* PRINT THERMAL */
                        //countButtons += await this.displayPrintThermalButton(divComponent, data) ? 1 : 0;

                        /* E-MAIL */
                        //countButtons += await this.displayEmailButton(divComponent, data) ? 1 : 0;

                        /* CLONE BUTTON */
                        //countButtons += await this.displayCloneButton(divComponent, data) ? 1 : 0;

                        // DROPDOWN BUTTONS
                        await this.displayDropdownButtons(dropdownButtons, divComponent, rowComponent);

                        /* PRINT THERMAL */
                        //await this.displayPrintThermalButton(divComponent, data2);

                        /* SET BUTTON */
                        //await this.displaySetButton(divComponent, data2, data, level);

                        /* EMAIL BUTTON */
                        //await this.displayEmailButton(divComponent, data2);
                    }					
                }				
            }
        }
    }

    async displaySelect(rowComponent:any, data:any)
    {
        if (this.hasBatch())
        {			
            const colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                {
                    classes : [ "select-col" ],
                });

            const selectComponent = await colComponent.instance.addPartial(PartialType.type("Select"),
                {
                    labelId : this.core().util.randomString(10),
                    data    : data
                });

            if (selectComponent.instance.click)
            {
                this._subscriptions.add(selectComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitSelect(event);
                }));
            }

            this.selectedComponents.push(selectComponent);

            return true;			
        }

        return false;
    }

    async displayAddButton(buttonsComponent:any)
    {
        if (this.hasAddButton())
        {
            const addButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.ADD,
                    tooltip : "Adicionar",
                });

            if (addButtonComponent.instance.click)
            {
                this._subscriptions.add(addButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitAdd(event);
                }));
            }

            return true;
        }

        return false;
    }

    async displayAddLevelButton(buttonsComponent:any, data:any, level:number)
    {
        if (this.hasAddLevelButton() && level <= this.grid._level)
        {
            const addButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.ADD,
                    data    : data,
                    tooltip : "Adicionar",
                });

            if (addButtonComponent.instance.click)
            {
                this._subscriptions.add(addButtonComponent.instance.click.subscribe((event:any) =>
                {
                    event.parent = event.data;
                    event.data   = null;
                    event.level  = level;
                    this.emitAdd(event);
                }));
            }

            return true;
        }

        return false;
    }

    async displayExpandButton(buttonsComponent:any, colExpandComponent:any, data:any)
    {
        if (this.hasExpandButton())
        {
            const expandButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon : IconType.CHEVRON_DOWN,
                    data : {
                        data         			   : data,
                        colExpandComponent : colExpandComponent,
                    },
                    tooltip : this.translatePipe.transform(this.setting.expandLabel || "Expandir"),
                });

            expandButtonComponent.isClose = true;

            if (expandButtonComponent.instance.click)
            {
                this._subscriptions.add(expandButtonComponent.instance.click.subscribe(async (event:any) =>
                {
                    event.data.colExpandComponent.instance.clear();					

                    expandButtonComponent.isClose = !expandButtonComponent.isClose;

                    if (!expandButtonComponent.isClose)
                    {
                        expandButtonComponent.instance.icon = IconType.CHEVRON_UP;

                        const plugin = this.core().injector.get(DocumentPlugin);

                        /* GRID */		
                        let params = this.core().createParams({
                            model : Grid,
                            appid : this.grid.params.appid,
                            path  : this.grid.params.gridPath
                        });

                        let result = await plugin.getObject(params);
                        const grid = result.data;

                        /* PARA O FILTER TER ACESSO */
                        this.core().expandDocument = event.data.data;

                        /* COLLECTION */
                        params = this.core().createParams({
                            appid     		: this.grid.params.appid,
                            where     		: grid.filters,
                            perPage     : grid.perPage,
                            searchFirst : true,
                        });

                        if (grid.map)
                        {
                            params.map      = grid.map;
                            params.mapItems = grid.reference;
                        }

                        result           = await plugin.getData(params);		
                        const collection = result.collection;			

                        /* COMPONENT */
                        const gridComponent	= await this.core().componentFactory.create(event.data.colExpandComponent.instance.template, DynamicList);					

                        gridComponent.instance.grid       = grid;
                        gridComponent.instance.collection = collection;	

                        event.data.colExpandComponent.instance.parent.addClasses([ "open" ]);
                        event.data.colExpandComponent.instance.parent.delClasses([ "close" ]);
                    }
                    else
                    {
                        event.data.colExpandComponent.instance.parent.addClasses([ "close" ]);
                        event.data.colExpandComponent.instance.parent.delClasses([ "open" ]);
                        expandButtonComponent.instance.icon = IconType.CHEVRON_DOWN;
                    }									
                }));
            }

            return true;
        }

        return false;
    }

    async displayViewerButton(buttonsComponent:any, data:any)
    {
        if (this.hasViewerButton(data))
        {
            const viewerButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.EYE,
                    data    : data,
                    tooltip : "Visualizar",
                });

            if (viewerButtonComponent.instance.click)
            {
                this._subscriptions.add(viewerButtonComponent.instance.click.subscribe((event:any) =>
                {
                    if (this.grid.viewerClick)
                    {
                        this.grid.viewerClick(event);
                    }
                    else
                    {
                        this.emitViewer(event);
                    }
                }));
            }

            return true;
        }

        return false;
    }

    async displayPrintButton(buttonsComponent:any, data:any)
    {
        if (this.hasPrintButton(data))
        {
            const printButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.PRINT,
                    data    : data,
                    tooltip : "Imprimir",
                });

            if (printButtonComponent.instance.click)
            {
                this._subscriptions.add(printButtonComponent.instance.click.subscribe((event:any) =>
                {
                    window.print();
                }));
            }

            return true;
        }

        return false;
    }

    async displayPrintThermalButton(buttonsComponent:any, data:any)
    {
        if (this.hasPrintThermalButton(data))
        {
            const printThermalButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.PRINT,
                    data    : data,
                    tooltip : "Imprimir",
                });

            if (printThermalButtonComponent.instance.click)
            {
                this._subscriptions.add(printThermalButtonComponent.instance.click.subscribe((event:any) =>
                {
                    const printPlugin = this.core().injector.get(PrintPlugin);

                    if (this._viewer)
                    {
                        printPlugin.printThermalViewer(this._viewer, event.data);
                    }
                    else
                    {
                        const viewerPlugin = this.core().injector.get(ViewerPlugin);

                        /* LOAD VIEWER */
                        const params = this.core().createParams({
                            appid : this.grid.appid,
                            where : [ {
                                field    : "form",
                                operator : "==",
                                value    : data.form
                            } ]
                        });

                        viewerPlugin.getData(params).then((result:any) =>
                        {
                            /* VIEWER */
                            if (result.collection.length > 0)
                            {
                                this._viewer = result.collection[0];

                                this._viewer.on(true).then(() =>
                                {
                                    printPlugin.printThermalViewer(this._viewer, event.data);
                                });
                            }
                        });
                    }
                }));
            }

            return true;
        }

        return false;
    }

    displayEmailButton(buttons:any, data:any)
    {
        if (this.hasEmailButton(data))
        {
            buttons.push({
                name    : this.translatePipe.transform(this.setting.sendLabel || "Enviar"),
                icon    : IconType.SEND,
                onClick : () => 
                {
                    console.log("click email");

                    this.emitEmail({ data : data });
                }
            });

            // const emailButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
            //     {
            //         icon    : IconType.SEND,
            //         data    : data,
            //         tooltip : "Enviar",
            //     });

            // if (emailButtonComponent.instance.click)
            // {
            //     this._subscriptions.add(emailButtonComponent.instance.click.subscribe((event:any) =>
            //     {
            //         console.log("click email");
            //         this.emitEmail(event);
            //     }));
            // }

            // return true;
        }

        return buttons;
    }

    displaySetButton(buttons, data:any, parent:any, level:number)
    {
        if (this.hasSetButton(data))
        {
            buttons.push({
                name    : this.translatePipe.transform(this.setting.editLabel || "Editar"),
                icon    : IconType.CREATE,
                onClick : () => 
                {
                    console.log("click set");
                    
                    this.emitSet({ data : data, parent : parent, level : level });
                }
            });        
        }

        return buttons;
    }

    async displayCloneButton(buttonsComponent:any, data:any, parent?:any)
    {
        if (this.hasCloneButton(data))
        {
            const cloneButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : IconType.DUPLICATE,
                    data    : data,
                    tooltip : "Duplicar",
                });

            if (cloneButtonComponent.instance.click)
            {
                this._subscriptions.add(cloneButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitClone(event);
                }));
            }

            return true;
        }

        return false;
    }

    displayDelButton(buttons, data:any, parent?:any)
    {
        if (this.hasDelButton(data))
        {
            buttons.push({
                name    : this.translatePipe.transform(this.setting.delLabel || "Remover"),
                icon    : IconType.TRASH,
                onClick : () => 
                {
                    console.log("click del",);
                    
                    this.emitDel({ data : data, parent : parent });
                }
            });
        }

        return buttons;
    }

    displayCancelButton(buttons, data:any, parent?:any)
    {
        if (this.hasCancelButton(data) && !data.isCanceled())
        {
            buttons.push({
                name    : "Cancelar",
                icon    : IconType.CLOSE,
                onClick : () => 
                {
                    this.emitCancel({ data : data, parent : parent });
                }
            });
        }
        else if (this.hasCancelButton(data) && data.isCanceled())
        {
            buttons.push({
                name    : "Descancelar",
                icon    : IconType.CLOSE_CIRCLE,
                onClick : () => 
                {
                    this.emitCancel({ data : data, parent : parent });
                }
            });
        }

        return buttons;
    }

    displayArchiveButton(buttons, data:any, parent?:any)
    {
        if (this.hasArchiveButton(data) && !data.isArchive())
        {
            buttons.push({
                name    : "Arquivar",
                icon    : IconType.UNLOCK,
                onClick : () => 
                {
                    this.emitArchive({ data : data, parent : parent });
                }
            });
        }
        else if (this.hasUnarchiveButton(data) && data.isArchive())
        {
            buttons.push({
                name    : "Desarquivar",
                icon    : IconType.LOCK,
                onClick : () => 
                {
                    this.emitArchive({ data : data, parent : parent });
                }
            });
        }

        return buttons;
    }

    async displayOrderButton(buttonsComponent:any)
    {
        if (this.hasOrderButton())
        {
            const orderButtonComponent = await buttonsComponent.instance.addPartial(PartialType.type("Button"),
                {
                    classes : [ "header" ],
                    icon    : IconType.CODE_WORKING,
                    tooltip : "Ordernar",
                });

            this._subscriptions.add(orderButtonComponent.instance.click.subscribe((event:any) =>
            {
                this.popoverController.create(
                    {
                        component      : ColumnOrderPopover,
                        event          : event,
                        componentProps : {
                            grid    : this.grid,
                            onClose : () =>
                            {
                                this.popoverController.dismiss();
                            },
                        },
                        showBackdrop : false,
                        cssClass     : "column-order-popover"
                    }).then(popover =>
                {
                    popover.present();
                });
            }));
        }        
    }

    async displayButtons(data, rowComponent:any, tbodyComponent:any)
    {
        if (!this.grid.disabledButtons)
        {
            let countButtons = 0;

            /* BUTTONS */
            const colComponent = await rowComponent.instance.addPartial(PartialType.type("Td"),
                {
                    classes : [ "buttons-col" ],
                });

            const divComponent = await colComponent.instance.addPartial(PartialType.type("Div"),
                {
                    classes : [ "buttons-div" ],
                });

            /* EXTRA TR EXPAND */
            let colExpandComponent : any;

            if (this.grid.hasExpandButton)
            {
                const rowExpandComponent = await tbodyComponent.instance.addPartial(PartialType.type("Tr"),
                    {
                        classes : [ "tr-expand", "close" ],
                    });

                colExpandComponent = await rowExpandComponent.instance.addPartial(PartialType.type("Td"),
                    {
                        attributes : [ {
                            name  : "colspan",
                            value : this.grid.items.length + 1
                        } ],
                    });
            }		
            
            let [ buttons, dropdownButtons ] = await this.getExtraButtons(data);                   
            dropdownButtons                  = this.displayEmailButton(dropdownButtons, data);
            dropdownButtons                  = this.displayArchiveButton(dropdownButtons, data);
            dropdownButtons                  = this.displayCancelButton(dropdownButtons, data);
            dropdownButtons                  = this.displaySetButton(dropdownButtons, data, null, 1);
            dropdownButtons                  = this.displayDelButton(dropdownButtons, data);
            
            /* ADD BUTTON */
            countButtons += await this.displayAddLevelButton(divComponent, data, 2) ? 1 : 0;

            /* EXPAND BUTTON */
            countButtons += await this.displayExpandButton(divComponent, colExpandComponent, data) ? 1 : 0;

            // EXTRA BUTTONS
            countButtons += await this.displayExtraButtons(buttons, data, divComponent);

            /* VIEWER BUTTON */
            countButtons += await this.displayViewerButton(divComponent, data) ? 1 : 0;

            /* DEPOIS VER COMO FAZER POIS NÃO ESTOU NA PÁGINA PRINT */
            //this.displayPrintButton(divComponent, data);

            /* PRINT THERMAL */
            //countButtons += await this.displayPrintThermalButton(divComponent, data) ? 1 : 0;

            /* CLONE BUTTON */
            //countButtons += await this.displayCloneButton(divComponent, data) ? 1 : 0;

            /* SET BUTTON */
            //countButtons += await this.displaySetButton(divComponent, data, null, 1) ? 1 : 0;

            // DROPDOWN BUTTONS
            await this.displayDropdownButtons(dropdownButtons, divComponent, rowComponent);
            
            // CLASS COUNT BUTTON
            colComponent.instance.addClasses([ "buttons-" + countButtons ]);

            divComponent.instance.markForCheck();
        }        
    }

    async displayExtraButtons(buttons, data, divComponent:any)
    {
        let count = 0;

        for (const button of buttons)
        {
            const buttonComponent = await divComponent.instance.addPartial(PartialType.type("Button"),
                {
                    icon    : button.icon,
                    label   : button.label,
                    data    : data,
                    tooltip : button.label,
                });

            if (buttonComponent.instance.click)
            {
                this._subscriptions.add(buttonComponent.instance.click.subscribe((event:any) =>
                {
                    button.onClick();
                }));
            }

            count++;
        }   

        return count;
    }

    async displayDropdownButtons(dropdownButtons, divComponent:any, rowComponent)
    {
        if (dropdownButtons.length > 0)
        {
            const dropdownButtonComponent = await divComponent.instance.addPartial(PartialType.type("Button"),
                {
                    classes : [ "header" ],
                    icon    : IconType.ELLIPSIS_HORIZONTAL,
                    tooltip : "Ações",
                });

            this._subscriptions.add(dropdownButtonComponent.instance.click.subscribe(async (event:any) =>
            {
                rowComponent.instance.addClasses([ "release" ]);                

                const popover = await this.popoverController.create(
                    {
                        component      : ListPopover,
                        event          : event,
                        componentProps : {
                            items    : dropdownButtons,
                            onSelect : (event:any) =>
                            {
                                event.onClick();

                                this.popoverController.dismiss();
                            },
                        },
                        cssClass     : "listPopover",
                        showBackdrop : true,
                        arrow        : false,
                    });

                popover.onDidDismiss().then((detail) => 
                {
                    rowComponent.instance.delClasses([ "release" ]);
                });

                return await popover.present();

            }));
        } 
    }
    
    async displayBatch(rowComponent:any)
    {
        if (this.hasBatch())
        {
            const colComponent = await rowComponent.instance.addPartial(PartialType.type("Th"),
                {
                    classes : [ "buttons-col" ],
                });

            const orderButtonComponent = await colComponent.instance.addPartial(PartialType.type("Button"),
                {
                    classes : [ "header" ],
                    icon    : IconType.ELLIPSIS_HORIZONTAL,
                    tooltip : "Ação em Lote",
                });

            this._subscriptions.add(orderButtonComponent.instance.click.subscribe((event:any) =>
            {
                this.popoverController.create(
                    {
                        component      : ListPopover,
                        event          : event,
                        cssClass       : "batchPopover",
                        componentProps : {
                            title  	 : "Ação em Lote",
                            items    : this.getBatch(),
                            onSelect : (item:any) =>
                            {
                                this.alertController.create({
                                    header  : "Alerta",
                                    message : "Deseja Prosseguir com a ação " + item.label + "?",
                                    buttons : [
                                        {
                                            text    : "Não",
                                            handler : () =>
                                            {
                                                this.alertController.dismiss();
                                                return false;
                                            }
                                        },
                                        {
                                            text    : "Sim",
                                            handler : async () =>
                                            {
                                                //await this.callableApi(item.url, params);
                                                this.emitBatch(item);
                                                this.alertController.dismiss();

                                                return false;
                                            }
                                        }
                                    ]
                                }).then(alert =>
                                {
                                    alert.present();
                                });

                                this.popoverController.dismiss();
                            },
                        },
                        showBackdrop : true,
                    }).then(popover =>
                {
                    popover.present();
                });
            }));
        }        
    }
}
