import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { PartialType }       from '../../../../type/partial/partial.type';
import { PartialFactory }    from '../../../../factory/partial/partial.factory';
import { BaseListContainer } from '../base.list.container';
import { SlotType }          from '../../../../type/slot/slot.type';
import { IconType }          from '../../../../type/icon/icon.type';

@Component({
    selector        : `[card-list-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-grid>
                            <ng-template #template></ng-template>
                       </ion-grid>`
})
export class CardListContainer extends BaseListContainer
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    getCols()
    {
        if(this.grid)
        {
            if(this.grid.cols == 6)
            {
                return [{
                    name  : 'size',
                    value : 12
                },
                {
                    name  : 'size-sm',
                    value : 6,
                },
                {
                    name  : 'size-sm',
                    value : 4
                },
                {
                    name  : 'size-md',
                    value : 3,
                },
                {
                    name  : 'size-lg',
                    value : 2
                },
                {
                    name  : 'size-xl',
                    value : 2,
                }];
            }
            else if(this.grid.cols == 5)
            {
                return [{
                    name  : 'size',
                    value : 12
                },
                {
                    name  : 'size-sm',
                    value : 6,
                },
                {
                    name  : 'size-sm',
                    value : 4
                },
                {
                    name  : 'size-md',
                    value : 4,
                },
                {
                    name  : 'size-lg',
                    value : 2.4
                },
                {
                    name  : 'size-xl',
                    value : 2.4,
                }];
            }
            else if(this.grid.cols == 4)
            {
                return [{
                    name  : 'size',
                    value : 12
                },
                {
                    name  : 'size-sm',
                    value : 6,
                },
                {
                    name  : 'size-sm',
                    value : 4
                },
                {
                    name  : 'size-md',
                    value : 4,
                },
                {
                    name  : 'size-lg',
                    value : 3
                },
                {
                    name  : 'size-xl',
                    value : 3,
                }];
            }
            else if(this.grid.cols == 3)
            {
                return [
                {
                    name  : 'size',
                    value : 12
                },
                {
                    name  : 'size-sm',
                    value : 6
                },
                {
                    name  : 'size-md',
                    value : 6
                },
                {
                    name  : 'size-lg',
                    value : 4
                },
                {
                    name  : 'size-xl',
                    value : 4,
                }];
            }
            else if(this.grid.cols == 2)
            {
                return [
                {
                    name : "size",
                    value : 12
                },
                {
                    name  : 'size-sm',
                    value : 12
                },
                {
                    name  : 'size-md',
                    value : 6
                },
                {
                    name  : 'size-lg',
                    value : 6
                },
                {
                    name  : 'size-xl',
                    value : 6
                }];
            }
        }
    }

    drawList()
    {
        return new Promise<void>(async (resolve) =>
        {
            const rowComponent = await this.addPartial(PartialType.type('Row'), this.template,
            {
                classes : ['tile-box'],
            });

            this.components.push(rowComponent);

			/* ADD */
			this.displayAddButton(rowComponent);

            for(const key in this.collection)
            {
                if(!this.collection[key]._isRenderer)
                {
                    this.drawItem(this.collection[key], rowComponent);
                    this.collection[key]._isRenderer = this.collection.isInfinite;
                }
            }

            /* RENDERER */
            const promises : any = [];

            for(const key in this.components)
            {
                const component = this.components[key];
                const promise   = component.instance.isRendererComplete();

                promises.push(promise);
            }

            Promise.all(promises).then(() =>
            {
                resolve();

                console.log('completo tile all');
            });
        });
    }

	async displayAddButton(rowComponent:any)
    {
        if(this.hasAddButton())
        {
            /* COL */
            const colComponent = await rowComponent.instance.addPartial(PartialType.type('Col'),
            {
                attributes : this.getCols(),
                classes    : ['add-button']
            });

            const addButtonComponent = await colComponent.instance.addPartial(PartialType.type('Button'),
            {
                icon    : IconType.ADD,
                tooltip : 'Adicionar',
            });

            if(addButtonComponent.instance.click)
            {
                this._subscriptions.add(addButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitAdd(event);
                }));
            }
        }
	}

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    drawItem(data:any, rowComponent:any)
    {
        return new Promise(async () =>
        {
            const classes = [];

            if(data.active != undefined && !data.active)
            {
                classes.push('no-active');
            }

            /* COL */
            const colComponent = await rowComponent.instance.addPartial(PartialType.type('Col'),
            {
                attributes : this.getCols(),
                classes    : classes
            });

            /* CARD */
            const cardComponent = await colComponent.instance.addPartial(PartialType.type('Card'),
            {
                classes : []
            });

            /* GRID BUTTONS */
            for(const key in this.grid.buttons)
            {
				const button = this.grid.buttons[key];
				let display  = true;
				
                if(typeof button.isEnabled == 'function')
                {
					display = button.isEnabled(data);									
                }

				if(button.displayPath)
				{
					display = await data.getProperty(button.displayPath);
				}

                if(display)
                {
					const buttonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
					{
						icon    : button.icon,
						data    : data,
						label   : button.label,
						tooltip : button.label,
					});
	
					buttonComponent.instance.click.subscribe((event:any) =>
					{
						event;
	
						/* BUTTON IN CODE */
						if(button.onClick)
						{
							button.onClick(event);
						}
						else
						{
							const params = {};
	
							for(const key in button.params)
							{
								const item = button.params[key];
		
								if(item.label == 'dataReference')
								{
									const data       = {};
									data[item.value] = event.data.reference.path
									params['data']   = JSON.stringify(data);
								}
								else
								{                            
									params[item.label] = item.value;
								}
							}
		
							this.core().push(button.router, params);
		
							//{
								
								//appid        : data.appid,
								//documentPath : data.reference.path
							//})   
						}                    
					});
				}                
            }

            /* SET BUTTON */
            this.displaySetButton(cardComponent, data);

            /* DEL BUTTON */
            this.displayDelButton(cardComponent, data);

            /* ITEMS */
            for(const key in this.grid.items)
            {
                const formItem = this.grid.items[key];

                formItem.onAdd = (event:any) =>
                {
                    this.emitAdd(event);
                };

                const hasViewerButton = this.grid.hasViewerButton && (this.hasAcl('viewer_document') || (this.core().user && this.core().user.isMaster()));

                if(formItem._slot && SlotType.TIILE.value == formItem._slot.value)
                {
                    cardComponent.instance.addTitleOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.SUBTITLE.value == formItem._slot.value)
                {
                    cardComponent.instance.addSubTitleOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.FOOTER.value == formItem._slot.value)
                {
                    cardComponent.instance.addFooterOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.BEFORE_CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addBeforeOutput(formItem, data);

                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
                else if(formItem._slot && SlotType.CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addOutput(formItem, data);                    

                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
                else if(formItem._slot && SlotType.AFTER_CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addAfterOutput(formItem, data);
                    
                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
            }
        });
    }

    async displaySetButton(cardComponent : any, data : any)
    {
        const hasSetButton = this.grid.hasSetButton && (this.hasAcl('set_document') || (this.core().user && this.core().user.isMaster()));

        if(hasSetButton)
        {
            let setButtonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
            {
                icon    : IconType.CREATE,
                data    : data,
                classes : ['set-button'],
                tooltip : 'Editar',
            });

            if(setButtonComponent.instance.click)
            {
                setButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitSet(event);
                });
            }
        }
    }

    async displayDelButton(cardComponent : any, data : any)
    {
        const hasDelButton = this.grid.hasDelButton && (this.hasAcl('del_document') || this.core().user && this.core().user.isMaster());

        if(hasDelButton)
        {
            const delButtonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
            {
                icon    : IconType.TRASH,
                data    : data,
                classes : ['del-button'],
                tooltip : 'Remover',
            });

            if(delButtonComponent.instance.click)
            {
                delButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitDel(event);
                });
            }
        }
    }
}
