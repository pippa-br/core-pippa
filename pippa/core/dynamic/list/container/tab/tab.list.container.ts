import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { PartialType }       from '../../../../type/partial/partial.type';
import { PartialFactory }    from '../../../../factory/partial/partial.factory';
import { BaseListContainer } from '../base.list.container';
import { SlotType }          from '../../../../type/slot/slot.type';
import { IconType }          from '../../../../type/icon/icon.type';

@Component({
    selector        : `[tab-list-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ng-template #template></ng-template>`
})
export class TabListContainer extends BaseListContainer
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }    

    drawList()
    {
        return new Promise<void>(async (resolve) =>
        {
			const tabComponent = await this.addPartial(PartialType.type('Tab'), this.template);
			await tabComponent.instance.createSlides();

			this.components.push(tabComponent);
			
            for(const key in this.collection)
            {
                if(!this.collection[key]._isRenderer)
                {
                    this.drawItem(this.collection[key], tabComponent);
                    this.collection[key]._isRenderer = this.collection.isInfinite;
                }
            }

            /* RENDERER */
            const promises : any = [];

            for(const key in this.components)
            {
                const component = this.components[key];
                const promise   = component.instance.isRendererComplete();

                promises.push(promise);
            }

            Promise.all(promises).then(() =>
            {
				/* SELECIONA O PRIMEIRO */
				tabComponent.instance.markForCheck();
				tabComponent.instance.selected = 'tab0'; 				

                resolve();

                console.log('completo tile all');
            });
        });
    }

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    drawItem(data:any, tabComponent:any)
    {
        return new Promise(async () =>
        {
            const classes = [];

            /* TAB */            
            const segmentButton = await tabComponent.instance.createSegmentButtonPartial();
            const slidePartial  = await tabComponent.instance.createSlidePartial();                        

            /* GRID BUTTONS */
            for(const key in this.grid.buttons)
            {
                const button = this.grid.buttons[key];

                const buttonComponent = await slidePartial.instance.addButtonsPartial(PartialType.type('Button'),
                {
                    icon    : button.icon,
                    data    : data,
                    tooltip : button.label,
                });

                buttonComponent.instance.click.subscribe((event:any) =>
                {
                    event;

                    const params = {};

                    for(const key in button.params)
                    {
                        const item = button.params[key];

                        if(item.label == 'dataReference')
                        {
                            const data       = {};
                            data[item.value] = event.data.reference.path
                            params['data']   = JSON.stringify(data);
                        }
                        else
                        {                            
                            params[item.label] = item.value;
                        }
                    }

                    this.core().push(button.router, params);

                    //{                        
                        //appid        : data.appid,
                        //documentPath : data.reference.path
                    //})
                });
			}
			
			/* SET BUTTON */
			this.displaySetButton(slidePartial, data);

			/* DEL BUTTON */
			this.displayDelButton(slidePartial, data);            			

            /* ITEMS */
            for(const key in this.grid.items)
            {
                const formItem = this.grid.items[key];

                formItem.onAdd = (event:any) =>
                {
                    this.emitAdd(event);
                };

                if(formItem._slot && SlotType.TIILE.value == formItem._slot.value)
                {
                    segmentButton.instance.addOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.SUBTITLE.value == formItem._slot.value)
                {
                    segmentButton.instance.addOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.FOOTER.value == formItem._slot.value)
                {
                    slidePartial.instance.addOutput(formItem, data);
                }
                else
                {
                    slidePartial.instance.addOutput(formItem, data);

                    const hasViewerButton = this.grid.hasViewerButton && (this.hasAcl('viewer_document') || (this.core().user && this.core().user.isMaster()));

                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
            }
        });
    }

    async displaySetButton(cardComponent : any, data : any)
    {
        const hasSetButton = this.grid.hasSetButton && (this.hasAcl('set_document') || (this.core().user && this.core().user.isMaster()));

        if(hasSetButton)
        {
            let setButtonComponent = await cardComponent.instance.addButtonsPartial(PartialType.type('Button'),
            {
                icon    : IconType.CREATE,
                data    : data,
                classes : ['set-button'],
                tooltip : 'Editar',
            });

            if(setButtonComponent.instance.click)
            {
                setButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitSet(event);
                });
            }
        }
    }

    async displayDelButton(cardComponent : any, data : any)
    {
        const hasDelButton = this.grid.hasDelButton && (this.hasAcl('del_document') || this.core().user && this.core().user.isMaster());

        if(hasDelButton)
        {
            const delButtonComponent = await cardComponent.instance.addButtonsPartial(PartialType.type('Button'),
            {
                icon    : IconType.TRASH,
                data    : data,
                classes : ['del-button'],
                tooltip : 'Remover',
            });

            if(delButtonComponent.instance.click)
            {
                delButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitDel(event);
                });
            }
        }
    }
}
