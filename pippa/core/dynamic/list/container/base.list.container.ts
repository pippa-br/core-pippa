import { Input, Output, EventEmitter, OnDestroy, ViewContainerRef, ViewChild, HostBinding, ChangeDetectorRef, Directive } from "@angular/core";
import { Subscription } from "rxjs";

/* PIPPA */
import { Core } 	      from "../../../util/core/core";
import { PartialType }    from "../../../type/partial/partial.type";
import { PartialFactory } from "../../../factory/partial/partial.factory";
import { IconType }       from "../../../type/icon/icon.type";

@Directive()
export abstract class BaseListContainer implements OnDestroy
{
    //public _gridSubscribe : any;
    public _listSubscribe : any;
    public _grid          : any;
    public _collection    : any;
    public _subscriptions : Subscription;
    public pageChanges     = false;
    public components     : any = [];
    public outputs        : any;
    public _resolve       : any;
    //public runDisplay     : boolean = false;
    public setting        : any;

    @HostBinding("class.fixed-header")
    @Input() public fixedHeader  = true;
    @Input() public acl         : any;

    @Output("add")     addEvent     : EventEmitter<any>;
    @Output("set")     setEvent     : EventEmitter<any>;
	@Output("select")  selectEvent  : EventEmitter<any>;
    @Output("clone")   cloneEvent   : EventEmitter<any>;
    @Output("del")     delEvent     : EventEmitter<any>;
    @Output("cancel")  cancelEvent  : EventEmitter<any>;
    @Output("archive") archiveEvent : EventEmitter<any>;
	@Output("batch")   batchEvent 	: EventEmitter<any>;
	@Output("viewer")  viewerEvent  : EventEmitter<any>;
	@Output("email")   emailEvent   : EventEmitter<any>;
    @Output("sort")    sortEvent    : EventEmitter<any>;

    @ViewChild("template", { read : ViewContainerRef, static : true }) template : ViewContainerRef;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        this.addEvent     = new EventEmitter();
        this.setEvent     = new EventEmitter();
        this.selectEvent  = new EventEmitter();
        this.cloneEvent   = new EventEmitter();
        this.delEvent     = new EventEmitter();
        this.cancelEvent  = new EventEmitter();
        this.archiveEvent = new EventEmitter();
        this.batchEvent   = new EventEmitter();
        this.emailEvent   = new EventEmitter();
        this.viewerEvent  = new EventEmitter();
        this.sortEvent    = new EventEmitter();
    }

    createComponents(grid:any, collection:any)
    {
        return new Promise(async (resolve) =>
        {
            this.grid       = grid;
            this.collection = collection;

            this._resolve = resolve;

            await this.display();
        });
    }

    @Input()
    set collection(collection:any)
    {
        if (collection)
        {
            if (this._listSubscribe)
            {
                this._listSubscribe.unsubscribe();
            }            

            this._listSubscribe = new Subscription();

            this._listSubscribe.add(collection.onUpdate().subscribe(async () =>
            {
                console.log("collection change display");

                await this.display();
            }));

            this._listSubscribe.add(collection.onSearch().subscribe(() =>
            {
                //this.clear(true);
            }));

            this._listSubscribe.add(collection.onNextPage().subscribe(() =>
            {
                this.pageChanges = true;
                //this.clear();
            }));

            this._listSubscribe.add(collection.onPrevPage().subscribe(() =>
            {
                this.pageChanges = true;
                //this.clear();
            }));
			
            collection._instance = this;
        }

        this._collection = collection;
    }

    get collection():any
    {
        return this._collection;
    }

    @Input()
    set grid(grid:any)
    {
        if (grid)
        {
            /*this._gridSubscribe = grid.onUpdate().subscribe(() =>
            {
                console.log('grid change display');

                this.display();
            });*/

            this.fixedHeader = grid.fixedHeader
            this._grid       = grid; 
            this.setting     = grid.setting || {};
        }
    }

    get grid()
    {
        return this._grid;
    }

    async display()
    {
        if (this.grid && this.collection)
        {   
            //console.error(this.grid, this.collection);

            //if(!this.runDisplay)
            //{
            //this.runDisplay = true;

            setTimeout(async () => 
            {
                await this.clear();

                /* FOI FEITO ISSO POR CAUDA DA GRID DO SUBFORM */
                if (!this.collection.isEmpty() && this.grid.items.length > 0)
                {
                    await this.drawList();
													
                    if (this.core().currentContent)
                    {
                        if (this.pageChanges)
                        {
                            if (this.collection.isInfinite)
                            {
                                // ...
                            }
                            else
                            {
                                this.core().currentContent.scrollToTop();																			
                            }
	
                            this.pageChanges = false;
                        }
                    }
	
                    await this.core().loadingController.stop();
						
                    this.markForCheck();
                    //this.runDisplay = false;
	
                    this._resolve();
	
                    console.log("display table", (this.collection ? this.collection.length : 0));    										      
                }
                else
                {                
                    console.log("display empty table", (this.collection ? this.collection.length : 0)); 
						
                    await this.drawEmpty()
	
                    this.markForCheck();
                    //this.runDisplay = false;
	
                    // TA ENTRANDOO EM LOOP
                    // SE AO FINALIZAR O DRAW EMPTY FOI ATUALIZADO O ARRAY
                    /*if(!this.collection.isEmpty())
						{
							await this.display();
						}*/
						
                    this._resolve();
                }
            });				
            //}
            /*else
			{
				this.markForCheck();
            	this._resolve();
			}*/			           
        }
        else
        {            
            this.markForCheck();
            this._resolve();
        }
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }	
	
    // JA FAZ NO BACK
    /*innerFilters()
	{
		return new Promise<void>(async (resolve) =>
		{
			if(this.grid.innerApp)
			{
				for(const key in this.collection)
				{
					const item = this.collection[key];
					
					this.core().innerDocument = item;

					/* COLLECTION */
    /*const params = this.core().createParams({
						appid   : this.grid.innerApp.appid,
						where   : this.grid.innerFilters,
						orderBy : this.grid.innerOrderBy,
						asc 	: this.grid.innerAsc.value,
					});
		
					const plugin 	 = this.grid.innerApp.getPlugin();
					const result 	 = await plugin.getData(params);		
					const collection = result.collection;

					if(this.grid.innerMappedType.value == 'object')
					{
						item[this.grid.innerMappedBy] = collection[0];
					}
					else
					{
						item[this.grid.innerMappedBy] = collection;
					}
				}
			}
			
			resolve();
		});		
	}*/

    drawMock()
    {
        return new Promise<void>(resolve =>
        {
            resolve();
        });
    }

    async drawList()
    {        
    }
	
    drawEmpty()
    {
        return new Promise<void>(async (resolve) =>
        {
            if (this.grid.showNoItems) 
            {
                const rowComponent = await PartialFactory.createComponent(PartialType.type("Tr"), this.template, 
                    {
                        classes : [ "empty-row" ],
                    });

                // LABEL
                const colComponent   = await rowComponent.instance.addPartial(PartialType.type("Td"));
                const labelComponent = await colComponent.instance.addPartial(PartialType.type("Label"), 
                    {
                        label : "Nenhum item cadastrado",
                    });

                // BUTON
                if (this.hasAddButton())
                {
                    if (labelComponent.instance.click)
                    {
                        this._subscriptions.add(labelComponent.instance.click.subscribe((event:any) =>
                        {
                            this.emitAdd(event);
                        }));
                    }
				
                    const col2Component      = await rowComponent.instance.addPartial(PartialType.type("Td"), {
                        classes : [ "buttons-col" ],
                    });
                    const divComponent       = await col2Component.instance.addPartial(PartialType.type("Div"),
                        {
                            classes : [ "buttons-div" ],
                        });
                    const addButtonComponent = await divComponent.instance.addPartial(PartialType.type("Button"),
                        {
                            icon    : IconType.ADD,
                            tooltip : "Adicionar",
                        });
	
                    if (addButtonComponent.instance.click)
                    {
                        this._subscriptions.add(addButtonComponent.instance.click.subscribe((event:any) =>
                        {
                            this.emitAdd(event);
                        }));
                    }
                }				
            }
	
            resolve();
        });
    }

    hasOrderButton()
    {
        return this.grid.hasOrderButton
    }

    hasAddLevelButton()
    {
        return this.grid.hasAddLevelButton;
    }

    hasAddButton()
    {
        return this.grid.hasAddButton;
    }

    hasBatch()
    {
        const values = [];
       
        if (this.grid.batch && this.grid.batch.length > 0)
        {
            for (const item of this.grid.batch)
            {
                if (this.core().user.hasGroups(item.groups))
                {
                    values.push(item);
                }
            }
        }

        return values.length > 0;
    }

    getBatch()
    {
        const values = [];
       
        if (this.grid.batch && this.grid.batch.length > 0)
        {
            for (const item of this.grid.batch)
            {
                if (item.status && this.core().user.hasGroups(item.groups))
                {
                    values.push(item);
                }
            }
        }

        return values;
    }

    hasSetButton(data?:any)
    {
        return this.grid.hasSetButton && (
            !data || !data.isLock() && !data.isArchive() && !data.isCanceled()
        ) && (
            (this.hasAcl("owner_set_document") && data ? data && data.isOwner() : true)
        );
    }

    hasCloneButton(data?:any)
    {
        return this.grid.hasCloneButton && (
            !data || !data.isLock() && !data.isArchive() && !data.isCanceled()
        ) && (
            (this.hasAcl("owner_clone_document") && data ? data && data.isOwner() : true)
        );
    }

    hasDelButton(data?:any)
    {
        return this.grid.hasDelButton && (
            !data || !data.isLock() && !data.isArchive() //&& !data.isCanceled()
        ) && (
            (this.hasAcl("owner_del_document") && data ? data && data.isOwner() : true)
        );
    }

    hasCancelButton(data?:any)
    {
        return this.grid.hasCancelButton && (
            !data || !data.isLock() && !data.isArchive()
        ) && (
            (this.hasAcl("owner_cancel_document", false) && data ? data && data.isOwner() : true)
        );
    }

    hasArchiveButton(data?:any)
    {
        return this.grid.hasArchiveButton && (
            !data || !data.isLock() && !data.isCanceled()
        ) && (
            (this.hasAcl("owner_archive_document", false) && data ? data && data.isOwner() : true)
        );
    }
	
    hasUnarchiveButton(data?:any)
    {
        return this.grid.hasUnarchiveButton && (
            !data || !data.isLock() && !data.isCanceled()
        ) && (
            (this.hasAcl("owner_unarchive_document", false) && data ? data && data.isOwner() : true)
        );
    }

    hasEmailButton(data?:any)
    {
        return this.grid.hasEmailButton
    }

    hasPrintThermalButton(data?:any)
    {
        data;
        return this.hasAcl("print_thermal_document", false)
    }
	
    hasPrintButton(data?:any)
    {
        data;
        return this.hasAcl("print_document", false)
    }

    hasViewerButton(data?:any)
    {
        if (data.hasViewerButton !== undefined)
        {
            return data.hasViewerButton;
        }
        else
        {
            return this.grid.hasViewerButton;
        }        
    }
	
    hasExpandButton()
    {
        return this.grid.hasExpandButton;
    }

    ngOnDestroy()
    {
        console.log("ngOnDestroy");

        /*if(this._gridSubscribe)
        {
            this._gridSubscribe.unsubscribe();
        }*/
        
        if (this._listSubscribe)
        {
            this._listSubscribe.unsubscribe();
        }

        this.clear();
    }

    clear(force = false)
    {
        return new Promise<void>(resolve =>
        {            
            if (!this.collection || !this.collection.isInfinite || force)
            {
                /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
                if (this.template)
                {
                    this.template.clear();
                }

                for (const key in this.components)
                {
                    this.components[key].destroy();
                }

                console.log("grid clear", (this.collection ? this.collection.length : 0));
            }

            if (this._subscriptions)
            {
                this._subscriptions.unsubscribe();                 
            }

            this._subscriptions = new Subscription();

            resolve();

            /* ISSO GARANTI OS OBJETOS SERAM DESTRUIDOS */
            /*setTimeout(() => 
            {							
            });*/			
        });
    }

    hasAcl(name:string, value?:any)
    {
        return this.acl && this.acl.hasKey(name, value);
    }

    emitViewer(event:any)
    {
        this.viewerEvent.emit(event);
    }

    emitSort(event:any)
    {
        this.sortEvent.emit(event);
    }

    emitAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    emitSelect(event:any)
    {
        this.selectEvent.emit(event);
    }

    emitSet(event:any)
    {
        this.setEvent.emit(event);
    }

    emitArchive(event:any)
    {
        this.archiveEvent.emit(event);
    }

    emitBatch(event:any)
    {
        this.batchEvent.emit(event);
    }
	
    emitEmail(event:any)
    {
        this.emailEvent.emit(event);
    }

    emitCancel(event:any)
    {
        this.cancelEvent.emit(event);
    }

    emitClone(event:any)
    {
        this.cloneEvent.emit(event);
    }

    emitDel(event:any)
    {
        this.delEvent.emit(event);
    }

    core()
    {
        return Core.get()
    }
}
