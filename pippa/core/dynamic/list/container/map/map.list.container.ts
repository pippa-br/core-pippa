import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import {Loader, LoaderOptions } from 'google-maps';


/* PIPPA */
import { BaseListContainer } from '../base.list.container';
import { MarkerManager, CircleManager } from '@agm/core';
// import { google } from '@agm/core/services/google-maps-types';

@Component({ 
    selector        : `[map-list-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div id="maps"></div>`
})
export class MapListContainer extends BaseListContainer
{
    
    constructor(
        public changeDetectorRef : ChangeDetectorRef,        
    )
    {
        super( changeDetectorRef );
    }    

    drawList()
    {
        return new Promise<void>(resolve =>
        {       
            //MAP SET UP
            const options: LoaderOptions = 
            {
                language    : 'pt-BR',
                region      : 'Brazil',
                // version?: string,
                // client?: string,
                // channel?: string,
                // language?: string,
                // libraries?: Array<string>,
            };

            const loader = new Loader('AIzaSyDV9ir369fjp7UhStfgOGVhRT-w-n_yyjQ', options);
            
            //INIT MAP
            loader.load().then (
                // (google) =>
                // {
                //     //MAP OPTIONS
                //     const mapOptions = {                
                //         center: {lat: -23.5478381, lng: -46.7093233},
                //         zoom: 5,                
                //     };

                //     const maps = new google.maps.Map(document.getElementById('maps'), mapOptions); 
                //     let members = [];
                    
                //     for (let i = 0; i < this.collection.length; i++) {
                //         const element = this.collection[i];

                //         if(this.collection[i].address) {
                //             members[i] = {
                //                 name    : this.collection[i].name,
                //                 lat     : this.collection[i].address.lat,
                //                 lng     : this.collection[i].address.lng,
                //                 photo   : this.collection[i].photo._url || null,
                //                 email   : this.collection[i].email

                //             }
                //         }                           
                //     }  
                //     //CLEAR EMPTY FIELDS
                //     members = members.filter(Object);                    
                    
                    
                //     console.log(members, 'Data'); 
                //     let bounds = new google.maps.LatLngBounds();

                //     //MAP MARKERS
                //     for (let i = 0; i < members.length; i++) {
                              
                //         //MARKER SET UP
                //         let marker : any = new google.maps.Marker({
                //             position: new google.maps.LatLng(members[i].lat, members[i].lng),
                //             map: maps,
                                                        
                //         }); 

                        
                //         marker.setIcon({
                //             url: members[i].photo || '/assets/icons/maps-icon.png',
                //             size: new google.maps.Size(71, 71),
                //             origin: new google.maps.Point(0, 0),
                //             anchor: new google.maps.Point(17, 34),
                //             scaledSize: new google.maps.Size(50, 50)
                //         })
                         
                //         bounds.extend(marker.position);

                //         //INFO WINDOW
                //         let infoWindow = new google.maps.InfoWindow({
                //             content:'<div id="infoWindow">'+
                //             '<img src="' + members[i].photo + '">'+
                //             '<div class="info">'+
                //             '<p class="name">' + members[i].name + '</p>' +
                //             '<p class="address">'+ members[i].email + '</p>'+
                //             '</div>'+
                //             '</div>'
                //         })
                //         marker.addListener('mouseover', ()=> {
                //             infoWindow.open(maps, marker);
                //         })
                //         marker.addListener('mouseout', ()=> {
                //             infoWindow.close();
                //         })
                                             
                //     } 

                //     maps.fitBounds(bounds);  
                    
                //     var listener = google.maps.event.addListener(maps, "idle", function () {
                //         maps.setZoom(12);
                //         google.maps.event.removeListener(listener);
                //     })
                // }
                (google)=>{
                    //adapted from http://gmaps-samples-v3.googlecode.com/svn/trunk/overlayview/custommarker.html
                    function CustomMarker(latlng, map, imageSrc) {
                        this.latlng_ = latlng;
                        this.imageSrc = imageSrc;
                        // Once the LatLng and text are set, add the overlay to the map.  This will
                        // trigger a call to panes_changed which should in turn call draw.
                        this.setMap(map);
                    }

                    const overlay : any = new google.maps.OverlayView();

                    CustomMarker.prototype = overlay;
                    console.log(CustomMarker.prototype, 'data');

                    CustomMarker.prototype.draw = function () {
                        // Check if the div has been created.
                        var div = this.div_;
                        if (!div) {
                            // Create a overlay text DIV
                            div = this.div_ = document.createElement('div');
                            // Create the DIV representing our CustomMarker
                            div.className = "customMarker";
                            div.setAttribute("id","customImage");


                            var img = document.createElement("img");
                            img.src = this.imageSrc;
                            div.appendChild(img);
                            google.maps.event.addDomListener(div, "click", function (event) {
                                // google.maps.event.trigger(me, "click");
                            });

                            // Then add the overlay to the DOM
                            var panes = this.getPanes();
                            panes.overlayImage.appendChild(div);
                        }

                        // Position the overlay 
                        var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
                        if (point) {
                            div.style.left = point.x + 'px';
                            div.style.top = point.y + 'px';
                        }
                    };                   
                      
                    CustomMarker.prototype.remove = function () {
                        // Check if the overlay was on the map and needs to be removed.
                        if (this.div_) {
                            this.div_.parentNode.removeChild(this.div_);
                            this.div_ = null;
                        }
                    };

                    CustomMarker.prototype.getPosition = function () {
                        return this.latlng_;
                    };   
                    
                    CustomMarker.prototype.setPosition = function(position) {
                        this.latlng_ = position;
                          // Position the overlay 
                        var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
                        if (point) {
                          this.div_.style.left = point.x + 'px';
                          this.div_.style.top = point.y + 'px';
                        }
                    };

                    var map = new google.maps.Map(document.getElementById("maps"), {
                        zoom: 17,
                        center: new google.maps.LatLng(-23.5478381, -46.7093233),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    let members = [];
                    
                    for (let i = 0; i < this.collection.length; i++) {
                        const element = this.collection[i];

                        if(this.collection[i].address) {
                            members[i] = {
                                name    : this.collection[i].name,
                                lat     : this.collection[i].address.lat,
                                lng     : this.collection[i].address.lng,
                                photo   : this.collection[i].photo._url || null,
                                email   : this.collection[i].email
                            }
                        }                          
                    }  

                    //CLEAR EMPTY FIELDS
                    members = members.filter(Object);    

                    for(var i=0;i<members.length;i++){
                    new CustomMarker(new google.maps.LatLng(members[i].lat,members[i].lng), map,  members[i].photo);
                    }
                }
            );     

            super.drawList();

            resolve();
        });
    }
}
