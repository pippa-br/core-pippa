	import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MbscEventcalendarOptions } from '@mobiscroll/angular';
import { PopoverController } from '@ionic/angular';

/* PIPPA */
import { TableListContainer } from '../table/table.list.container';

@Component({ 
    selector        : `[calendar-list-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="segment">
                            <ion-segment (ionChange)="changeView()" [(ngModel)]="viewOptions">
                                <ion-segment-button value="month">
                                    <ion-label>Mês</ion-label>
                                </ion-segment-button>
                                <ion-segment-button value="week">
                                    <ion-label>Semana</ion-label>
                                </ion-segment-button>
                                <ion-segment-button value="day">
                                    <ion-label>Dia</ion-label>
                                </ion-segment-button>
                            </ion-segment>
                        </div>
                        <div class="calendar-box" [ngClass]="{'month': viewOptions == 'month'}">
                            <mbsc-eventcalendar [data]="events" [options]="eventSettings" [view]="eventSettings.view"></mbsc-eventcalendar>
                            <div class="tabela-box">
                                    <table id="dynamicTable" class="striped" [ngClass]="'level-' + _level">
                                        <ng-template #template></ng-template>
                                    </table>
                            </div>
                        </div>
                       `
})
export class CalendarListContainer extends TableListContainer
{
    public events    : any;
    public setButton : string = '<ion-button clear class="set-button"><ion-icon name="create-outline"></ion-icon></ion-button>';
    public delButton : string = '<ion-button clear class="del-button"><ion-icon name="trash-outline"></ion-icon></ion-button>';
    viewOptions = 'month';
    public eventSettings : MbscEventcalendarOptions =
    {		
        view    : {
            calendar : {
				type : 'week',
				//weekCounter : true,
            },
            /*eventList  : {
                type : 'week',
                scrollable: true
            }*/
        },
        onEventClick : (event) =>
        {
            if(event.domEvent.target.classList.contains('set-button'))
            {
                this.emitSet(event.event);
            }
            else if(event.domEvent.target.classList.contains('del-button'))
            {
                this.emitDel(event.event);
            }
        }
    };

    constructor(
        public popoverController : PopoverController,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(popoverController, changeDetectorRef);
    }
    
    changeView() {
            switch (this.viewOptions) {
                case 'month':
                    this.eventSettings.view = {
                        calendar: { type: 'month' },
                    };
                    break;
                case 'week':
                    this.eventSettings.view = {
                        calendar: { type: 'week' },
                    };
                    break;
                case 'day':                    
                    this.eventSettings.view = {
                        schedule: { type: 'day', }
                    };
                    break;
            }
       
    }

    /* NÃO DESENHA HEADER */
    async displayHeader()
    {

    }

    drawList()
    {
        return new Promise<void>(resolve =>
        {
            const events = [];

            for(const key in this.collection)
            {
                if(this.collection[key].startDate)
                {
                    const _data : any = {};

                    /* DATES */
                    if(this.collection[key].startDate && this.collection[key].endDate)
                    {
                        _data.start = this.collection[key].startDate.toDate();
                        _data.end   = this.collection[key].endDate.toDate();
                    }
                    else
                    {
                        _data.d = this.collection[key].startDate.toDate();
                    }

                    _data.data = this.collection[key];

                    events.push(_data);
                }
            }

            console.log('events', events);
            
            this.events = events;

            super.drawList();

            resolve();
        });
    }
}
