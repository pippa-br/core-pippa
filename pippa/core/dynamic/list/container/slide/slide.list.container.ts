import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';


/* PIPPA */
import { PartialType }       from '../../../../type/partial/partial.type';
import { PartialFactory }    from '../../../../factory/partial/partial.factory';
import { BaseListContainer } from '../base.list.container';
import { SlotType }          from '../../../../type/slot/slot.type';

@Component({
    selector        : `[slide-list-container]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<swiper-container #slides pager="true"
								   (slidechange)="onIonSlideWillChange()"
								   [options]="slideOpts">
                            <ng-template #template></ng-template>
					   </swiper-container>					   
					   <a class="swiper-button-prev swiper-button-black" (click)="prevSlide()" *ngIf="!isBeginning"></a>
                       <a class="swiper-button-next swiper-button-black" (click)="nextSlide()" *ngIf="!isEnd"></a> `
})
export class SlideListContainer extends BaseListContainer
{
	@ViewChild('slides') slides : ElementRef | undefined;
	
	public isBeginning  : boolean = true;
    public isEnd        : boolean = true;
    public slideOpts : any = {
        initialSlide : 0,
        speed        : 400,
		loop         : true,
		autoplay: {
			delay: 5000,
		},
    };
    
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
	}
	
	nextSlide() 
    {
        this.slides?.nativeElement.swiper.slideNext();
    }
    
    prevSlide() 
    {
        this.slides?.nativeElement.swiper.slidePrev();
    }

    onIonSlideWillChange()
    {
        this.slides?.nativeElement.swiper.isBeginning().then((value) => 
        {
            this.isBeginning = value;
        })

        this.slides?.nativeElement.swiper.isEnd().then((value) => 
        {
            this.isEnd = value;
        })
    }
    
    drawList()
    {
        return new Promise<void>(resolve =>
        {
            for(const key in this.collection)
            {
                if(!this.collection[key]._isRenderer)
                {
                    this.drawItem(this.collection[key]);
                    this.collection[key]._isRenderer = this.collection.isInfinite;
                }
            }

            /* RENDERER */
            const promises : any = [];

            for(const key in this.components)
            {
                const component = this.components[key];
                const promise   = component.instance.isRendererComplete();

                promises.push(promise);
            }

            Promise.all(promises).then(() =>
            {
                resolve();

                console.log('completo tile all');
            });
        });
    }

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    drawItem(data:any)
    {
        return new Promise(async () =>
        {
            const classes = [];

            /* SLIDE */
            const slideComponent = await this.addPartial(PartialType.type('Slide'), this.template,
            {
                classes : classes
            });

            /* CARD */
            const cardComponent = await slideComponent.instance.addPartial(PartialType.type('Card'),
            {
                classes : []
            });

            /* GRID BUTTONS */
            for(const key in this.grid.buttons)
            {
                let button    = this.grid.buttons[key];
                let isEnabled = true;

                if(button.isEnabled)
                {
                    isEnabled = button.isEnabled(data);
                }

                if(isEnabled)
                {
                    let buttonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
                    {
                        icon    : button.icon,
                        data    : data,
                        tooltip : button.label,
                    });

                    if(buttonComponent.instance.click && button.click)
                    {
                        buttonComponent.instance.click.subscribe((event:any) =>
                        {
                            button.click(event);
                        });
                    }
                }
            }

            /* SET BUTTON */
            this.displaySetButton(cardComponent, data);

            /* DEL BUTTON */
            this.displayDelButton(cardComponent, data);

            /* ITEMS */
            for(const key in this.grid.items)
            {
                const formItem = this.grid.items[key];

                formItem.onAdd = (event:any) =>
                {
                    this.emitAdd(event);
				};
				
				const hasViewerButton = this.grid.hasViewerButton && (this.hasAcl('viewer_document') || (this.core().user && this.core().user.isMaster()));

                if(formItem._slot && SlotType.TIILE.value == formItem._slot.value)
                {
                    cardComponent.instance.addTitleOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.SUBTITLE.value == formItem._slot.value)
                {
                    cardComponent.instance.addSubTitleOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.FOOTER.value == formItem._slot.value)
                {
                    cardComponent.instance.addFooterOutput(formItem, data);
                }
                else if(formItem._slot && SlotType.BEFORE_CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addBeforeOutput(formItem, data);

                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
                else if(formItem._slot && SlotType.CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addOutput(formItem, data);                    

                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
                else if(formItem._slot && SlotType.AFTER_CONTENT.value == formItem._slot.value)
                {
                    cardComponent.instance.addAfterOutput(formItem, data);
                    
                    if(hasViewerButton && !formItem.onClick)
                    {
                        formItem.onClick = (event:any) =>
                        {
                            if(event.data.active == undefined || event.data.active)
                            {
                                console.log('onClick');
                                this.emitViewer(event);
                            }
                        };
                    }
                }
            }
        });
    }

    async displaySetButton(cardComponent : any, data : any)
    {
        const hasSetButton = this.hasSetButton(data);

        if(hasSetButton)
        {
            let setButtonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
            {
                icon    : 'create',
                data    : data,
                classes : ['set-button'],
                tooltip : 'Editar',
            });

            if(setButtonComponent.instance.click)
            {
                setButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitSet(event);
                });
            }
        }
    }

    async displayDelButton(cardComponent : any, data : any)
    {
        const hasDelButton = this.hasDelButton(data);

        if(hasDelButton)
        {
            const delButtonComponent = await cardComponent.instance.addFooterPartial(PartialType.type('Button'),
            {
                icon    : 'trash',
                data    : data,
                classes : ['del-button'],
                tooltip : 'Remover',
            });

            if(delButtonComponent.instance.click)
            {
                delButtonComponent.instance.click.subscribe((event:any) =>
                {
                    this.emitDel(event);
                });
            }
        }
    }
}
