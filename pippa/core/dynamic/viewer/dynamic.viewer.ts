import { Component, Input, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy,
         ViewContainerRef, ViewChild, HostBinding, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import * as elementResizeEvent from 'element-resize-event';
import * as Unbind from 'element-resize-event';
import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { IModel }                   from '../../interface/model/i.model';
import { Core }                     from '../../util/core/core';
import { Viewer }                   from '../../model/viewer/viewer';
import { FlatViewerContainer }      from './container/flat/flat.viewer.container';
import { SegmentViewerContainer }   from './container/segment/segment.viewer.container';
import { SlideViewerContainer }     from './container/slide/slide.viewer.container';
import { WizardViewerContainer }    from './container/wizard/wizard.viewer.container';
import { AccordionViewerContainer } from './container/accordion/accordion.viewer.container';
import { Params }                   from '../../../core/util/params/params';
import { BaseModel } from '../../model/base.model';
import { Form } from '../../model/form/form';

@Component({
    selector        : `[dynamic-viewer]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div [hidden]="!viewer || !data || _isRendererComplete">
                            <ion-progress-bar color="medium" type="indeterminate"></ion-progress-bar>
                       </div>
                       <div class="dynamic-viewer-wrapper" [hidden]="!_isRendererComplete">
                            <ng-template #template></ng-template>
                       </div>`,
})
export class DynamicViewer implements OnDestroy
{
    public changeSubscribe     : any;
    public _subscriptions      : Subscription = new Subscription();
    public _data               : any;
    public _viewer             : any;
    public viewerContainer     : any;
    public _isRendererComplete : boolean = false;

    @Output('close') 			closeEvent 			  : EventEmitter<any> = new EventEmitter();
	@Output('changeStep') 		changeStepEvent 	  : EventEmitter<any> = new EventEmitter();
    @Output('reload') 		    reloadEvent 	      : EventEmitter<any> = new EventEmitter();
    @Output('rendererComplete') rendererCompleteEvent : EventEmitter<any> = new EventEmitter();

    @ViewChild('template', { read: ViewContainerRef, static:true }) template : ViewContainerRef;

    @HostBinding('class') classes : string = 'dynamic-viewer ';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
    }        

    @Input()
    set viewer(viewer:any)
    {
        if(viewer)
        {
            viewer.on(true).then(() =>
            {
                this._viewer = viewer;

                // PQ ISSO? SE NECESSARIO TEM QUE ALTERAR A POSICAO POIS A VIEWER PODE TER ITEMS VAZIO QUE Ë NORMALIZADO COM O FORM 
                /*this.changeSubscribe = this._viewer.items.onChange().subscribe(() =>
                {
                    console.log('viewer change display');

                    this.display();
                });*/

                this.normalizeItems().then(() =>
                {
                    this.display();
                });
            });
        }
    }

    get viewer():any
    {
        return this._viewer;
    }

    @Input()
    set data(data:any)
    {
		this._data = data;

        if(data)
        {
            this.normalizeItems().then(() =>
            {
                this.display();
            });
        }
        else
        {
            this.clear();
        }
    }

    get data()
    {
        return this._data;
    }

    async normalizeItems()
    {
        if(this.data && this.viewer)
		{
			if(this.data instanceof DocumentReference)
			{
				this._data = new BaseModel(this._data);
				await this._data.on();
			}            

			/* SE NÃO HOUVER COLUNA, CRIA TODAS DOS FIELDS */
			if(!this.viewer.items || this.viewer.items.length == 0)
			{
                if(!(this.data.form instanceof Form))
                {
                    this.data.form = new Form(this.data.form);                
                }

				await this.data.form.reload();                

                this.data.form.mode = Params.VIEWER_MODE_FORM;
				this.data.form.changePermission();

				this.viewer.items = this.data.form.items.copy();
			}
		}
    }

    display()
    {
        if(this.data && this.viewer && this.viewer.items.length > 0)
        {
            this.clear().then(async () => 
            {
                console.log('display viewer');

                this.classes += this.viewer.type.value + '-type ';
    
                let container : any = FlatViewerContainer;
    
                if(this.viewer.type.value == Viewer.SEGMENT_TYPE.value)
                {
                    container = SegmentViewerContainer;
                }
                else if(this.viewer.type.value == Viewer.SLIDE_TYPE.value)
                {
                    container = SlideViewerContainer;
                }
                else if(this.viewer.type.value == Viewer.INDEXES_TYPE.value)
                {
                    container = WizardViewerContainer;
                }
                else if(this.viewer.type.value == Viewer.ACCORDION_TYPE.value)
                {
                    container = AccordionViewerContainer;
                }
    
                this.viewerContainer = await this.core().componentFactory.create(this.template, container);
    
                /* COMPONENT E DIFERENTE DE INSTACE, COMPONENT PARA USAR O detectChanges */
                this.viewer.component = this.viewerContainer;
                this.template.insert(this.viewerContainer.hostView);
    
                /* RESIZE IFRAME: QUANDO RESIZE */
                if(window.parent)
                {                    
                    elementResizeEvent(this.viewerContainer.location.nativeElement, () => 
                    {
                        this.dispatchPostMessage();
                    });
                } 
    
                this.viewerContainer.instance.createComponents(this.viewer, this.data).then(() =>
                {                                                        
                    /* ISSO GARANTI OS OBJETOS SERAM DESTRUIDOS */
                    setTimeout(() => 
                    {
						// EVENTO DE FECHAMENTO DA VIEWER
                        this._subscriptions.add(this.viewerContainer.instance.closeEvent.subscribe(() =>
                        {
                            this.closeEvent.emit();
                        }));

                        // EVENTO DE RELOAD
                        this._subscriptions.add(this.viewerContainer.instance.reloadEvent.subscribe(() =>
                        {
                            this.reloadEvent.emit();
                        }));
						
						// EVENT DO MUDANCA DE STEP
						this._subscriptions.add(this.viewerContainer.instance.changeStepEvent.subscribe((event) =>
                        {
                            this.changeStepEvent.emit(event);
                        }));
        
                        this.rendererCompleteEvent.emit();
        
                        this._isRendererComplete = true;
    
                        this.changeDetectorRef.markForCheck();
        
                        /* RESIZE IFRAME: QUANDO LOAD */
                        if(window.parent)
                        {
                            this.dispatchPostMessage();
                        } 
        
                        //console.log('dynamic-viewer hostView', this.viewer);
                    })                    
                });
            });            
        }
	}
	
	dispatchPostMessage()
	{
		setTimeout(() => 
		{
			window.parent.postMessage({
				type : 'resize', 
				height : this.viewerContainer.location.nativeElement.scrollHeight,
				target : this.viewer.id,
			}, 
			'*');
		}, 500);
	}

    core()
    {
        return Core.get();
    }

    clear()
    {
        return new Promise((resolve:any) =>
        {
            /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
            if(this.template)
            {
                this.template.clear();
            }

            if(this.viewerContainer)
            {
                this.viewerContainer.destroy();
            }

            this._subscriptions.unsubscribe();
            this._subscriptions = new Subscription();

            this._isRendererComplete = false;

            if(this.viewerContainer)
            {
                Unbind.unbind(this.viewerContainer.location.nativeElement);
            }

            console.log('clear viewer', (this.viewer && this.viewer.items ? this.viewer.items.length : 0));

            resolve();
        });        
    }

    destroy()
    {
        this.ngOnDestroy();
    }

    ngOnDestroy()
    {
        console.log('ngOnDestroy');
        this.clear();

        if(this.changeSubscribe)
        {
            this.changeSubscribe.unsubscribe();
        }
    }
}
