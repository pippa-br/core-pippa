import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseViewerContainer } from '../base.viewer.container';

@Component({
    selector        : '[accordion-viewer-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="wrapper-body-container">
                            <mat-vertical-stepper class="steps">
                                <mat-step *ngFor="let step of steps; let isFirst = first; let isLast = last;trackBy : trackById"
                                        [completed]="false">
                                    <ng-template matStepLabel>
                                        <ion-label>{{step.label}}</ion-label>
                                    </ng-template>
                                    <div class="body-container">
                                        <ng-template #templates></ng-template>
                                    </div>
                                </mat-step>                                    
                            </mat-vertical-stepper>
                        </div>`,
})
export class AccordionViewerContainer extends BaseViewerContainer
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }
}
