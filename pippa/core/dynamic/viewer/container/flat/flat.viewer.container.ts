import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseViewerContainer } from '../base.viewer.container';

@Component({
    selector        : '[flat-viewer-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<mat-toolbar class="header-container" *ngIf="viewer.hasHeader && !viewer.imageHeader">
                            <h4>{{getTitle()}}</h4>
						</mat-toolbar>
						<mat-toolbar class="subheader-container image" *ngIf="viewer.hasHeader && viewer.imageHeader">
                            <img [src]="viewer.imageHeader._url"/>
                        </mat-toolbar>
						<mat-toolbar class="subheader-container" *ngIf="viewer.description">
							<div [innerHtml]="viewer.description"></div>
						</mat-toolbar>                        
                        <div class="wrapper-body-container steps">
                            <div *ngFor="let step of steps; trackBy : trackById; let i = index;">
                                <div class="title-container" *ngIf="step.label && steps.length > 1">
                                    <span class="icon">{{i + 1}}</span> <label>{{step.label}}</label>
                                </div>
                                <div class="body-container">
                                    <ng-template #templates></ng-template>
                                </div>
                            </div>
                        </div>                        
                        <mat-toolbar class="footer-container has-image" *ngIf="viewer.imageFooter">
                            <ion-img [src]="viewer.imageFooter._url"></ion-img>
                        </mat-toolbar>`,
})
export class FlatViewerContainer extends BaseViewerContainer implements AfterViewInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }
}
