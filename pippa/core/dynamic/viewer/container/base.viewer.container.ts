import { EventEmitter, Output, AfterViewInit, ViewContainerRef, OnDestroy,
         QueryList, ViewChildren, ChangeDetectorRef, Directive } from '@angular/core';

/* CORE */
import { Core }             from '../../../util/core/core';
import { Params }           from '../../../util/params/params';
import { PartialFactory }   from '../../../factory/partial/partial.factory';
import { PartialType }      from '../../../type/partial/partial.type';
import { Viewer }       	from '../../../model/viewer/viewer';

@Directive()
export abstract class BaseViewerContainer implements AfterViewInit, OnDestroy
{
    @ViewChildren('templates', { read: ViewContainerRef }) templates : QueryList<ViewContainerRef>;
    @Output('close') 	  closeEvent 	  : EventEmitter<any> = new EventEmitter();
    @Output('reload') 	  reloadEvent 	  : EventEmitter<any> = new EventEmitter();
	@Output('changeStep') changeStepEvent : EventEmitter<any> = new EventEmitter();

    public container : any;
    public steps     : any = [];
    public _viewer   : any;
    public _data     : any;
	public _resolve  : any;
	public submitted : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
	}
	
	getTitle()
	{
		if(this.data)
		{
			return (this.data._sequence ? 'Nº ' + this.data._sequence + ' - ' : '') + (this.data.name ? this.data.name : '');
		}
	}

    createComponents(viewer:any, data:any)
    {
        return new Promise((resolve) =>
        {
            this.viewer = viewer;
            this.data   = data;
            this.steps  = (viewer.items ? viewer.items.getSteps(data) : null);

            this._resolve = resolve;

            this.markForCheck();
        });
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }

    ngAfterViewInit()
    {
        this.display();
    }

    async display()
    {
        if(this.viewer && this.templates)
        {
            const templates = this.templates.toArray();

            for(const key in this.viewer.items)
            {
                const formItem = this.viewer.items[key];

                if(formItem.field && !formItem.field.blocked)
                {
                    if(formItem.field.isHidden())
                    {
                        //this.addFormItem(item, containers[0]);
                    }
                    else
                    {
                        const row  = formItem.row;
						const step = this.getStep(formItem.step);
						
						/* POR CAUSA DA PERMISSÕA DE VISUALIZAR STEP */
						if(step)
						{
							const stepTemplate = templates[step.position];

							if(!step.template)
							{
								step.template = stepTemplate;
								step.rows     = [];
							}
	
							if(!step.rows[row])
							{
								const rowComponent = await this.addPartial(PartialType.type('Row'), step.template);
	
								step.rows[row] = {
									component : rowComponent,
									total     : 0,
									columns   : []
								};
							}
	
							const classes = [];
							classes.push(formItem.field.type.value.toLowerCase() + '-col');
							classes.push('id-' + formItem.id);
	
							const colComponent = await step.rows[row].component.instance.addPartial(PartialType.type('Col'),
							{
								classes : classes
							});
	
							colComponent.formItem = formItem;
	
							/* LABEL */
							if(formItem.hasLabel)
							{
								await colComponent.instance.addPartial(PartialType.type('Label'),
								{
									label : formItem.label,
								});
							}
	
							/* DATA */
							await colComponent.instance.addOutput(formItem, this.data,
							{
								classes : ['output-partial']
							});
	
							/* ATTACHMENTS */
							if(formItem.field.hasAttachments())
							{
								await step.rows[row].component.instance.addPartial(PartialType.type('Attachments'),
								{
									formItem : formItem
								});
							}
	
							step.rows[row].columns.push(colComponent);
							step.rows[row].total++;
						}                        
                    }
                }
            }

            const promises : any = [];

            /* CALCULAR COLS */
            for(const key in this.steps)
            {
                const step = this.steps[key];

                for(const key2 in step.rows)
                {
                    const row     = step.rows[key2];
                    const promise = row.component.instance.isRendererComplete();

                    promises.push(promise);

					// CALCULO DE COLUNAS
                    for(const key3 in row.columns)
                    {
                        const column = row.columns[key3];
                        const col    = 12 / row.total;

                        column.instance.attributes = [{
                            name  : 'size',
                            value : 12
                        },
                        {
                            name  : 'size-sm',
                            value : col,
                        }];

                        if(column.formItem.field.instance)
                        {
                            column.formItem.field.instance.nameClass += 'size-' + col + ' ';
                        }
                    }
                }
            }

            Promise.all(promises).then(() =>
            {
                this._resolve();

                /* DEPOIS */
                this.data   = this._data;
				this.viewer = this._viewer;

				// PARA OUTPUS QUE PRECISA SABER SE O STEP ESTÁ VISÍVEL. EX: PDF
				if(this.steps.length > 0)
				{
					/* QUANDO É FLAT TODOS OS STEPS ESTAO ATIVOS */
					if(this.viewer.type.value == Viewer.FLAT_TYPE.value)
					{
						for(const step of this.steps)
						{
							this.onChangeStep(step);
						}						
					}
					else
					{
						this.onChangeStep(this.steps[0]);
					}					
				}				

                console.log('completo viewer all');
            });
        }
    }

    async addPartial(partial:any, template:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, template, args);

        return component;
    }

    set data(value:any)
    {
        this._data = value;

        for(const key in this.steps)
        {
            const step = this.steps[key];

            for(const key2 in step.rows)
            {
                const row = step.rows[key2];
                row.component.instance.data = value;
            }
        }
    }

    get viewer()
    {
        return this._viewer;
    }

	set viewer(value:any)
    {
        this._viewer = value;

        for(const key in this.steps)
        {
            const step = this.steps[key];

            for(const key2 in step.rows)
            {
                const row = step.rows[key2];
                row.component.instance.viewer = value;
            }
        }
    }

    get data()
    {
        return this._data;
    }

    ngOnDestroy()
    {
        console.log('ngOnDestroy');
        this.clear();
    }

    getStep(step:any)
    {
        if(step)
        {
            for(const key in this.steps)
            {
                if(step.id == this.steps[key].id)
                {
                    return this.steps[key];
                }
            }
        }
        else
        {
            return this.steps[0];
        }
    }

    createParams : (args:any) => Params = function(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    onClose()
    {
        this.closeEvent.emit();
    }

    onReload()
    {
        this.reloadEvent.emit();
    }

	onChangeStep(step:any)
	{
		this.changeStepEvent.emit({
			step : step
		});
	}

    core()
    {
        return Core.get()
    }

    clear()
    {
        for(const key in this.steps)
        {
            const step = this.steps[key];

            /* ESSE DEVE VIM PRIMEIRO QUE O DESTROY */
            if(step.template)
            {
                step.template.clear();
            }

            for(const key2 in step.rows)
            {
                const row = step.rows[key2];
                row.component.destroy();
            }

            step.template = null;
            step.row      = null;
        }
    }

    trackById(index:any, item:any) { index; return item.id; }
}
