import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseViewerContainer } from '../base.viewer.container';

@Component({
    selector        : '[segment-viewer-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="wrapper-body-container" [ngClass]="{'onlyOne' : steps.length == 1}">
                            <mat-tab-group class="steps" [selectedIndex]="0" (selectedTabChange)="onTabChanged($event);">
                                <mat-tab *ngFor="let step of steps; let isFirst = first; let isLast = last;trackBy : trackById"                                         
                                        label="{{step.label}}">
                                    <div class="body-container">
                                        <ng-template #templates></ng-template>
                                    </div>
                                </mat-tab>
                            </mat-tab-group>
                       </div>`,
})
export class SegmentViewerContainer extends BaseViewerContainer
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

	onTabChanged(event)
	{
		if(this.steps.length >= event.index)
		{
			this.onChangeStep(this.steps[event.index]);
		}		
	}
}
