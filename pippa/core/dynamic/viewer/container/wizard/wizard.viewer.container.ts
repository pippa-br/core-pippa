import { Component, Input, AfterViewInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

/* PIPPA */
import { BaseViewerContainer } from '../base.viewer.container';

@Component({
    selector        : '[wizard-viewer-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<mat-horizontal-stepper #stepper [linear]="isLinear" labelPosition="bottom">
	
							<ng-template matStepperIcon="edit">
								<ion-icon name="checkmark-outline"></ion-icon>
							</ng-template>

							<!-- Define the number of the step -->                  
							<ng-template matStepperIcon="number" let-index="index">
								{{index+1}}
							</ng-template>

							<mat-step *ngFor="let step of steps" 
									label="{{step.label}}" 	
									class="wrapper-body-container">

								<ion-grid class="body-container">
                        		    <ng-template #templates></ng-template>
								</ion-grid>
						
							</mat-step>

                	</mat-horizontal-stepper>`,
})
export class WizardViewerContainer extends BaseViewerContainer implements AfterViewInit
{
    @ViewChild('stepper', { read: MatStepper, static:false }) public stepper : MatStepper;

	public isLinear : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    createComponents(viewer:any, data:any)
    {
        const promise = super.createComponents(viewer, data);

        /* SETTING */
        if(this.viewer.setting)
        {
            /* DEPOIS REMOVER */
            this.isLinear = this.viewer.setting.isLinear;
		}	

        return promise;
    }

    stepEnter(event:any)
    {
        event;

        if(this.core().currentContent)
        {
            this.core().currentContent.scrollToTop();
        }
    }
}
