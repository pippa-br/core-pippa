import { Component, ViewChild, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy, ElementRef } from '@angular/core';

/* PIPPA */
import { BaseViewerContainer } from '../base.viewer.container';

@Component({
    selector        : '[slide-viewer-container]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<mat-toolbar class="header-container">
                            <h4>{{title}}</h4>
                        </mat-toolbar>
                        <div class="wrapper-body-container steps">
                            <swiper-container #slides pager="true" [options]="slideOpts" (slidechange)="onIonSlideWillChange()">
                                <swiper-slide *ngFor="let step of steps; let isFirst = first; let isLast = last;">
                                    <div class="body-container">
                                        <ng-template #templates></ng-template>
                                    </div>
                                </swiper-slide>
                            </swiper-container>
                        </div>`,
})
export class SlideViewerContainer extends BaseViewerContainer implements OnDestroy
{
    @ViewChild('slides') slides : ElementRef | undefined;

    //public unsubscribe : any;
    public title        : string = '';
    public initialSlide : number = 0;
    public slideOpts : any = {
        initialSlide : 0,
        speed : 400,
        autoHeight: true,
    };

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    createComponents(viewer:any, data:any)
    {
        const promise = super.createComponents(viewer, data);
        this.setTitle(this.initialSlide);

        /*this.unsubscribe = this.core().navController.onChange().subscribe((params:any) =>
        {
            console.log('--------', params);
        });*/

        return promise;
    }

    onIonSlideWillChange()
    {
        this.slides?.nativeElement.swiper.getActiveIndex().then((value:number) =>
        {
            this.setTitle(value);
            this.core().currentContent.scrollToTop();
        })
    }

    setTitle(value:number)
    {
        if(value < this.steps.length)
        {
            this.title = this.steps[value].label;
            this.markForCheck();
        }
    }

    ngOnDestroy()
    {
        super.ngOnDestroy();
        //this.unsubscribe.unsubscriber();
    }
}
