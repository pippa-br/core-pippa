import { Pipe, PipeTransform } from '@angular/core'

/*
  <div *ng-for="#value of arrayOfObjects | array"> </div>
*/

@Pipe({ name: 'array' })
export class ArrayPipe
{
    transform (value:any, args:any)
    {
        if(value == undefined)
        {
            return [];
        }

        return Object.keys(value).map(key => value[key]);
    }
}
