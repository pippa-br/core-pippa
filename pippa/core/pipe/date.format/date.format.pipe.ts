import { Pipe, PipeTransform } from '@angular/core'
import { TDate } from '../../util/tdate/tdate';

@Pipe({
    name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform
{    
    transform(date:any, format?:string, parse?:string):any
    {
		if(date)
		{
			if(!format)
			{
				format = 'dd/MM/yyyy';
			}
	
			if(!parse)
			{
				parse = 'yyyy-MM-dd HH:mm:ss';
			}
	
			if(date && typeof date.toDate == 'function')
			{
				date = date.toDate();
			}
			
			if(format == 'fromNow')
			{
				return new TDate({ value : date, mask : parse}).toDate();
			}			
			else
			{
				return new TDate({ value : date, mask : parse}).format(format);
			}
		}		
    }
}
