import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'addressFormat'
})
export class AddressFormatPipe implements PipeTransform
{    
    transform(address:any):any
    {
		let formart : string = '';

		if(address)
		{
			formart = address.street + ', ' + address.housenumber + ' ' + address.complement
					  + ', ' + address.district + ' - ' + address.city + ' / ' + address.state + ' - '
					  + address.zipcode;
		}

        return formart;
    }
}
