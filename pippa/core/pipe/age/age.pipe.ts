import { Pipe, PipeTransform } from '@angular/core'
import { TDate } from '../../util/tdate/tdate';

@Pipe({
    name: 'age'
})
export class AgePipe implements PipeTransform
{
    transform(date:any):any
    {
        if(date && typeof date.toDate == 'function')
        {
            date = date.toDate();
        }

        return new TDate().diff(new TDate({ value : date}), 'years');
    }
}
