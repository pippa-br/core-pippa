import { Pipe, PipeTransform } from '@angular/core'
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name: 'nbspRemove'
})
export class NbspRemovePipe implements PipeTransform
{
    constructor(
		public sanitizer: DomSanitizer)
    {
    }

    transform(value: string): any
    {
        let result : any;

        if (value)
        {
            result = value.replace(/<p>&nbsp;<\/p>/g, '');
            result = this.sanitizer.bypassSecurityTrustHtml(result);
        }

        return result ? result : value;
    }
}
