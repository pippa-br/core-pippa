import { Pipe, PipeTransform } from '@angular/core'
import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { Document } from '../../model/document/document' 

@Pipe({
    name : 'property'
})
export class PropertyPipe implements PipeTransform
{    
    async transform(data:any, path:string, _default?:string)
    {
		if(data)
		{
			if(data instanceof DocumentReference)
			{
				data = new Document(data);
				data = await data.on();
			}
			else if(typeof data.on == 'function' && !data.fetch)
			{
				await data.reload();
				data.fetch = true;
			}

			const value = await data.getProperty(path);

			return value || _default;	
		}
    }
}
