import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name : 'currencyFormat'
})
export class CurrencyFormatPipe implements PipeTransform
{    
    transform(value:any):any
    {
        if(value != undefined)
        {
            return 'R$ ' + value.toLocaleString('pt-BR', { minimumFractionDigits : 2, maximumFractionDigits: 2})
        } 
        
        return 'R$ 0';
    }
}
