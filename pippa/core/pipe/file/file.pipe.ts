import { Pipe } from '@angular/core'

/* FILE */
import { File } from '../../model/file/file';

@Pipe({
    name : 'file'
})
export class FilePipe
{
    transform(value, key)
    {
        let file = new File(value);

        if(key)
        {
            return file[key];
        }

        return file;
    }
}
