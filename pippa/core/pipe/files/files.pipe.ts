import { Pipe } from '@angular/core'

import { FileCollection } from '../../model/file/file.collection';

@Pipe({
    name : 'files'
})
export class FilesPipe
{
    transform(values)
    {
        return new FileCollection(values);
    }
}
