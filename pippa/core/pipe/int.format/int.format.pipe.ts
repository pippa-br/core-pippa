import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name : 'intFormat'
})
export class IntFormatPipe implements PipeTransform
{    
    transform(value:any):any
    {
        if(value != undefined)
        {
            return parseInt(value)
        } 
        
        return 0;
    }
}
