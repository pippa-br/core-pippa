import { Pipe, PipeTransform } from '@angular/core'
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name: 'breakLine'
})
export class BreakLinePipe implements PipeTransform
{
    constructor(
		public sanitizer: DomSanitizer)
    {
    }

    transform(value: string): any
    {
        let result : any;

        if (value)
        {
            result = value.replace(/(?:\r\n|\r|\n)/g, '<br>');
            result = this.sanitizer.bypassSecurityTrustHtml(result);
        }

        return result ? result : value;
    }
}
