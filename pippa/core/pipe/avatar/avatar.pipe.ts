import { Pipe, PipeTransform } from '@angular/core'
import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { Document } from '../../model/document/document' 

@Pipe({
    name : 'avatar'
})
export class AvatarPipe implements PipeTransform
{    
    async transform(data:any, path:string)
    {
		if(data)
		{
			if(data instanceof DocumentReference)
			{
				data = new Document(data);
				data = await data.on();
			}
			else if(typeof data.on == 'function' && !data.fetch)
			{
				await data.reload();
				data.fetch = true;
			}

			const value = await data.getProperty(path);

			if(value)
			{
				if(typeof value == 'string')
				{
					return value;
				}
				else
				{
					return value._url;
				}
			}
			else
			{   
				return '/assets/img/avatar.png';
			}
		}
    }
}
