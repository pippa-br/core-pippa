import { Pipe, PipeTransform } from '@angular/core'

import { Core } from '../../util/core/core';

@Pipe({ name: 'notificationCount'})
export class NotificationCountPipe implements PipeTransform
{
    constructor()
    {

    }

    transform(data, type):any
    {
        if(data[type])
        {
            return data[type];
        }

        return '';
    }

    ionic()
    {
        return Core.get().ionic
    }
}
