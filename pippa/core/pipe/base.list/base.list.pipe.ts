import { Pipe, PipeTransform } from '@angular/core'
import { BaseList } from '../../model/base.list';

/*
  <div *ng-for="#value of arrayOfObjects | array"> </div>
*/

@Pipe({ name: 'baseList' })
export class BaseListPipe
{
    transform (value:any, args:any)
    {
        if(value == undefined)
        {
            return new BaseList();
        }

        return new BaseList(value);
    }
}
