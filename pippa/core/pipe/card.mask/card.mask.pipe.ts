import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'cardMask'
})
export class CardMaskPipe implements PipeTransform
{
    transform(value:any):any
    {
		if(value)
		{
			const r = /\b(?:\d{4}[ -]?){3}(?=\d{4}\b)/gm;
			const s = `**** **** **** `;
			return value.replace(r, s);
		}     
    }
}
