import { Pipe } from '@angular/core'

@Pipe({ name: 'unique' })
export class UniquePipe
{
    transform (values:any)
    {
        if(values == undefined)
        {
            return [];
        }

		const items = [];
		const keys  = [];

        for(const item of values)
		{
			if(keys.indexOf(item.id) == -1)
			{
				items.push(item);
				keys.push(item.id);
			}
		}

		return items;
    }
}
