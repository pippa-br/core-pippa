import { Pipe } from '@angular/core'

/* PIPPA */
import { Core } from '../../util/core/core';

@Pipe({
    name: 'translate'
})
export class TranslatePipe
{
    transform(value:any)
    {
		if(Core.get().languageMaps)
		{
			return Core.get().languageMaps[value] || value;
		}

        return value;
    }
}
