import { Pipe, PipeTransform } from '@angular/core'
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
    name : 'resume'
})
export class ResumePipe implements PipeTransform
{
	constructor(
		public sanitizer : DomSanitizer)
    {
    }

    transform(value: string, count:number): any
    {
        if(value)
        {
            value = value.replace(/<.*?>/g, '').trim().slice(0, count); // replace tags
			return this.sanitizer.bypassSecurityTrustHtml(value);
		}
    }
}
