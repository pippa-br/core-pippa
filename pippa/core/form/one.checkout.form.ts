/* PIPPA */
import { Form } 	 from '../model/form/form';
import { Field } 	 from '../model/field/field';
import { FieldType } from '../type/field/field.type';
import { Core }  	 from '../util/core/core';
import { Document }  from '../model/document/document';
import { TDate } from '../util/tdate/tdate';

export class OneCheckoutForm
{
    static async create(data:any, setting:any)
    {
		/* FACEBOOK */
		/*if(Core.get().account.facebook && Core.get().account.facebook.selectPayment)
		{
			Core.get().track(Core.get().account.facebook.selectPayment);
		}*/
		
		for(const key in data.prices)
		{
			const item 	  = data.prices[key];
			item.selected = key == '0';

			if(item.discountPercentage)
			{
				item.amount = item.value - (item.discountPercentage * item.value) / 100;
			}
			else
			{
				item.amount = item.value;
			}
			
			item.label  = item.paymentMethod.label;
			//item.label += await Core.get().util.currencyFormat(item.value) + ' ';			
			item.label += (item.note ? ' - ' + item.note : '');
		}

		let finishDay : number = 0;

		if(data.finishDay)
		{
			finishDay = data.finishDay;
		}

		const installments = async () =>
		{
			const data = form.formGroup.value.type;

			if(data)
			{
				const installments = [];
				let amount 		   = data.amount;

				// COUPON
				if(form.instances.coupon.selected)
				{
					amount = data.value; // SE TIVER CUPOM PEGA O VALOR ORIGINAL
					let coupon : any = new Document(form.instances.coupon.selected);				

					await coupon.on();

					if(coupon._percentage)
					{
						amount = amount - (coupon._percentage * amount) / 100;
					}
					else if(coupon._value)
					{
						amount = amount - coupon._value;
					}
				}				

				// PARCELAMENTO
				let amountInstallments = amount;
	
				for(let i = 1; i <= data.maxInstallments; i++)
				{			
					if(i > data.freeInstallments && data.interestRate)
					{
						amountInstallments = amountInstallments + (amountInstallments * data.interestRate)  / 100;
					}

					const installmentAmount = amountInstallments / i;
	
					installments.push({
						id       : i + 'x',
						label    : i + 'x - R$ ' + installmentAmount.toLocaleString('pt-BR', { minimumFractionDigits : 2, maximumFractionDigits : 2}) + (data.interestRate == 0 ? ' S/ Juros' : ''),
						value    : i,
						amount   : amountInstallments,
						selected : i == 0
					});
				}
	
				if(installments.length > 0)
				{									
					form.instances.installments.items   = installments;
					form.instances.installments.initial = installments[0];
					form.instances.installments.populate(installments[0]);					
				}				

				form.instances.totalLabel.populate('Total: R$' + amount.toLocaleString('pt-BR', { minimumFractionDigits : 2, maximumFractionDigits : 2}));
			}			
		}

		const items = [
			{
				id  : 'productLabel',
				row : 1,
				col : 1,
				field : new Field({
					id   		: 'productLabel',
					type 		: FieldType.type('Note'),	
					name 		: 'productLabel',
					description : data.name,
					setting     : {
						type : 'warning',
					}
				})
			},
			{
				id  : 'type',
				row : 2,
				col : 1,
				field : new Field({
					id          : 'type',
					type        : FieldType.type('Select'),
					name        : 'type',
					label       : 'Forma de Pagamento',
					placeholder : 'Forma de Pagamento',
					option      : {
						items : data.prices
					},
					setting : {
						controlField : 'creditCard',
						controlPath  : 'paymentMethod.value',
						credit_card  : 'creditCard',						
					},
					onChange : async (event:any) => 
					{					
						if(event.data)
						{
							installments();																	
						}							
					},
				}),
			},
			{
				id  : 'coupon',
				row : 3,
				col : 1,
				field : new Field({
					id          : 'coupon',
					type        : FieldType.type('ReferenceCoupon'),
					name        : 'coupon',
					label       : 'Cupom',
					placeholder : 'Cupom',
					required    : false,
					option : {
						app : setting.couponAppid ? Core.get().getApp(setting.couponAppid) : Core.get().getApp('coupon')
					},
					onChange : async (event:any) => 
					{					
						//if(event.data)
						//{
							installments();			
						//}							
					}
				}),
			},
			{
				id  : 'totalLabel',
				row : 4,
				col : 1,
				field : new Field({
					id          : 'totalLabel',
					type        : FieldType.type('Note'),	
					name        : 'totalLabel',
					setting     : {
						type : 'warning',
					}
				})
			},
			{
				id  : 'creditCard',
				row : 5,
				col : 1,
				field : new Field({
					id          : 'creditCard',
					type        : FieldType.type('CreditCard'),
					name        : 'creditCard',
					label       : 'Dados do Cartão',
					placeholder : 'Dados do Cartão',
					setting     : {
						installments : [],
					}
				}),
			}
		];

		if(finishDay)
		{
			items.push({	
				id  : 'finishDate',
				row : 6,
				col : 1,
				field : new Field({
					id      : 'finishDate',
					type    : FieldType.type('Hidden'),	
					name    : 'finishDate',
					initial : new TDate().add(finishDay, 'days').toDate(),
				})
			});
		}

        const form : any = new Form({
			name    : 'Comprar Agora!',							
			items   : items,
			setting : {
				addText : 'Confirmar'
			},			
		});

		return form;
	}	
}
