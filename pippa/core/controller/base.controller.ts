/* PIPPA */
import { Core } from '../util/core/core';

export class BaseController
{
    constructor(
    )
    {
    }

    createParams(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    createResult(params:any)
    {
        return this.core().resultFactory.create(params);
    }

    core()
    {
        return Core.get();
    }
}
