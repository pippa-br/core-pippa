import { Injectable } from '@angular/core';
import { LoadingController as LoadingIonicController} from '@ionic/angular';

/* APP */
import { BaseController } from '../../../core/controller/base.controller';

@Injectable({
    providedIn: 'root'
})
export class LoadingController extends BaseController
{
    public loading : any;
	public count   : number = 0;
	public time    : any;

    constructor(
        public loadingIonicController : LoadingIonicController,
    )
    {
        super();
    }

    start(show?:boolean)
    {
        return new Promise<void>(async (resolve:any) =>
        {
			if(show === undefined)
			{
				show = true;
			}
			
            /* DEIXAR COM TIME */
            if(this.time)
            {
                clearTimeout(this.time);
			} 	
			
			this.count++
			
            if(!this.core().isLoading && show)
            {
				this.core().isLoading = true;				
				
                this.loading = await this.loadingIonicController.create({
                    spinner      : 'dots',                    
					showBackdrop : false,
					backdropDismiss: true,
                    //spinner: null,
                    cssClass: 'bg-loading'
                    //duration : 1000,
                });
                
				this.loading.present();

				// setTimeout(() => 
				// {
				// 	this.stop();
				// }, 3000)

                resolve();
            }
            else
            {
                resolve();
            }
        });
    }

    stop()
    {
        return new Promise<void>(resolve =>
        {	
			this.count--;

            this.time = setTimeout(() =>
            {                
				if(this.core().isLoading && this.count <= 0)
				{
					this.loading.dismiss();
					this.loading = null;				
					this.core().isLoading = false;
					this.count = 0;
				}		   
            }, 300);  /* PARA EVITAR FICAR PISCANDO AO ABRIR LOADING SEGUINDOS */  

            /* DEIXAR FORA POIS SE LIMPAR O CLEAR TIME NÃO EXECUTA */
            resolve();
        });
    }
}
