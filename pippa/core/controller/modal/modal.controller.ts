/* APP */
import { Injectable } from '@angular/core';
import { ModalController as _ModalController} from '@ionic/angular'

/* PIPPA */
import { BaseController } from '../base.controller';

@Injectable({
    providedIn: 'root'
})
export class ModalController extends BaseController
{
    public modals : Array<any> = [];

    constructor(
        public modalController : _ModalController,
    )
    {
        super();
    }

    /* SE DEIXAR ISSO NO BASE APP ELE NÃO CARREGA O COMPONENT */
    async create(ModalPage:any, event?:any, isFull?:boolean)
    {
        this.core().isModal = true;

        const modal = await this.modalController.create({
            component       : ModalPage,
            componentProps  : event,
            cssClass        : (isFull ? 'full' : 'normal'),
            showBackdrop    : true,
            backdropDismiss : false,
        });

        /* ALTERAR O CURRENT APP */
        if(event && event.app)
        {
            this.core().currentApp = event.app;
        }

        modal.onDidDismiss().then(() =>
        {
            this.modals.pop();

            if(this.modals.length == 0)
            {
                this.core().isModal = false;

                console.log('close all modal');
            }
        });

        this.modals.push(modal);

        await modal.present();

        return modal;
    }

    dismiss()
    {
        return new Promise<void>(resolve =>
        {
            this.modalController.getTop().then(data =>
            {
                if(data)
                {
                    this.modalController.dismiss().then(() =>
                    {
                        setTimeout(() =>
                        {
                            resolve();
                        })
                    });
                }
                else
                {
                    resolve();
                }
            });
        });
    }
}
