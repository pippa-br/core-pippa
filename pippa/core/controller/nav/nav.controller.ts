import { Subject }         from 'rxjs';
import { Injectable }      from '@angular/core';
import { Router, UrlSerializer, UrlTree }          from "@angular/router";
import { MenuController, Platform }  from '@ionic/angular'
import { Browser } 		   from '@capacitor/browser';
import { Event as NavigationEvent, NavigationStart } from "@angular/router";
import { filter } from "rxjs/operators";
import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { BaseController }  from '../base.controller';
import { FilterPlugin }    from '../../plugin/filter/filter.plugin';
import { IframeModal }     from '../../modal/iframe/iframe.modal';
import { ModalController } from '../../controller/modal/modal.controller';
import { Menu } 		   from '../../model/menu/menu';
import {Location} 			from '@angular/common';
//import { AclPlugin }           from '../../plugin/acl/acl.plugin';

/* UTILIZAR ESSA CLASS SOMENTE PARA ROOT NAVEGATION */
@Injectable({
    providedIn: 'root'
})
export class NavController extends BaseController
{
    public _change 	    : any = new Subject();
	//public routes  	    : any = [];
	public menuSelected : any;
	public countHistory : number = 0;

    constructor(
        public router          : Router,
        public filterPlugin    : FilterPlugin, 
        public modalController : ModalController,
        public menuController  : MenuController,
		public location		   : Location,
		public platform		   : Platform,
        private urlSerializer  : UrlSerializer
    )
    {
        super();

		//menuController.enable(false);

		// PARA RODAR NO ANDROID
		if(this.platform.is('capacitor'))
		{
			this.platform.backButton.subscribeWithPriority(10, (processNextHandler:any) => 
			{
				this.back();	

				// REMOVE BACK NORMAL DO ANDROID
				//processNextHandler();				
			});					
		}

		this.router.events
		.pipe(
			filter(
				( event: NavigationEvent ) => 
				{
					return( event instanceof NavigationStart );
				}
			)
		)
		.subscribe(
			( event: NavigationStart ) => 
			{
				if(event.navigationTrigger == 'popstate') // ANDROID NÃO FUNCIONADA POPSTATE NO BACK
				{
					this.countHistory--;					
				}
				else if(event.navigationTrigger == 'imperative')
				{
					const exlude = ['/', '/auth-loading'];

					if(!exlude.includes(event.url))
					{
						this.countHistory++;
					}
				}	

				console.log( "countHistory:", this.countHistory);	
			}
		);
    }

    change()
    {
        return this._change.asObservable();
    }

    push(router:string, queryParams?:any, routeParams?:any, newTab=false)
    {
        this.modalController.dismiss().then(() =>
        {
            if(!routeParams)
            {
                routeParams = {}
            }

            queryParams = queryParams ? queryParams : {};

            if(this.core().currentApp)
            {
                if(!routeParams['app'])
                {
                    routeParams['app'] = this.core().currentApp.id;
                }

                if(!queryParams['appid'])
                {
                    queryParams['appid'] = this.core().currentApp.code;
                }

                if(queryParams['data'] && (typeof queryParams['data'] == 'object'))
                {
                    queryParams['data'] = JSON.stringify(queryParams['data']);
                }
            }

			// PASSA A QUERY ACCESS PARA DEMAIS PAGINAS
			if(this.core().access)
			{
				queryParams['access'] = true;
			}

            this.core().currentRouter = router;
            this.core().queryParams   = queryParams;
            this.core().routeParams   = routeParams;

            /*this.routes.push({
                router      : router,
                queryParams : queryParams,
                routeParams : routeParams,
            });*/

            console.log('push router', router, routeParams, queryParams);

            if(newTab)
            {
                let baseUrl = router;

                for(const key in routeParams)
                {
                    baseUrl += ';' + key + '=' + routeParams[key];
                }
                
                const urlTree: UrlTree = this.router.createUrlTree([baseUrl], { queryParams });            
                const url: string = this.urlSerializer.serialize(urlTree);

                window.open(url, '_blank');
            }
            else
            {                
                this.router.navigate([router, routeParams], { queryParams : queryParams});            
            }

            this._change.next(queryParams);

            this.menuController.close();
        });
    }

    back()
    {
        this.modalController.dismiss().then(() =>
        {
            if(this.hasBack())
            {
                /*this.routes.pop();

                const item = this.routes[this.routes.length - 1];

				console.log('back router', item);
				
				/* DEPOIS REVER */
				if(this.menuSelected)
				{
					this.menuSelected.selected = false;

					if(this.menuSelected.parent)
					{
						this.menuSelected.parent.selected = true;
					}
				}

                //this.router.navigate([item.router, item.routeParams], {queryParams:item.queryParams});
				this.location.back();
            }
            else
            {
                this.router.navigate(['']);
            }

            this.menuController.close();
        });
    }

    hasBack()
    {
        return this.countHistory > 0;
    }

    async goMenu(menu:any)
    {
		console.log('menu', menu);
		
		if(menu instanceof DocumentReference)
		{
			menu = new Menu(menu);
			menu = await menu.on(true);
		}
		else if(typeof menu == 'string')
		{
			menu = this.core().getMenuByName(menu);
		}	

        const queryParams : any = {};
        let   toUrl       : string;

        if(menu.params)
        {
            console.log('params', menu.params);

            for(let key in menu.params)
            {
                queryParams[menu.params[key].label] = menu.params[key].value;

                if(menu.params[key].label == 'toUrl')
                {
                    toUrl = menu.params[key].value;
				}
				if(menu.params[key].label == 'document' && menu.params[key].value == 'notification')
                {
					delete queryParams[menu.params[key].label];
					queryParams['dataPath'] = this.core().notification.reference.path;
                }
            }
		}

		if(this.menuSelected)
		{
			this.menuSelected.selected = false;

			if(this.menuSelected.parent)
			{
				this.menuSelected.parent.selected = true;
			}
		}

		if(menu.parent)
		{
			menu.parent.selected = true;
		}

		menu.selected 	  = true;
		this.menuSelected = menu;

        if(toUrl) /* OPEN URL */
        {
            console.log('params toUrl', toUrl);

            await Browser.open({ url: toUrl });
        }
        else if(menu.router)
        {
            if(menu.app)
            {
				// BUG: QUANDO FICA MUITO TEMPO SEM USAR A TELA
				if(!menu.app._isPopulate)
				{
					console.error('NOT POPULATE');
					this.core().isInitialize = false;
					await menu.app.reload();
				}

                queryParams.appid = menu.app.code;

                this.push(menu.router.value, queryParams,
                {
                    menu : menu.id
                },
                menu.newTab);
            }
            else
            {
                this.push(menu.router.value, queryParams,
                {
                    menu : menu.id
                },
                menu.newTab);
            }

            this.menuController.close();
        }
    }

    goFirstMenu()
    {
		const menu = this.getFirstMenu();

        if(menu)
        {
			this.goMenu(menu);
			
			history.pushState(null, null, window.location.href);
        }
    }

	getFirstMenu()
	{
		if(this.core().menu)
        {
			console.log('getFirstMenu', this.core().menu, this.core().menu.length);
			
			/* LENGTH DEPOIS DE ABRIR */
			if(this.core().menu.length > 0)
			{
				if(this.core().menu[0].router)
				{
					return this.core().menu[0];
				}
				else if(this.core().menu[0]._children && this.core().menu[0]._children.length > 0)
				{
					return this.core().menu[0]._children[0];
				}
			}	
        }

		return;
	}

    goInit()
    {
        this.router.navigate(['/']);
        this.menuController.close();
    }
}
