import { Directive } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Token, PushNotifications, PushNotificationSchema, ActionPerformed } from '@capacitor/push-notifications';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import { App, AppState } from '@capacitor/app';
import { Device } from '@capacitor/device';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar, Style } from '@capacitor/status-bar';
import { Keyboard, KeyboardInfo } from '@capacitor/keyboard';
import { Network } from '@capacitor/network';
import { register } from 'swiper/element/bundle';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

register();

/* PIPPA */
import { BaseComponent } from '../base.component';
import { Core }          from '../../util/core/core';
import { Notification }  from '../../model/notification/notification';
import { EventPlugin }   from '../../../core/plugin/event/event.plugin';

@Directive()
export abstract class BaseRoot extends BaseComponent
{
    public notifications : any = [];

	constructor(
        public breakpointObserver : BreakpointObserver
    )
    {
        super();

		this.breakpointObserver.observe([Breakpoints.Handset]).subscribe(result => 
		{
			this.core()._isMobile = result.matches;

			console.log('Is mobile:', this.core().isMobile(), result.matches);
		});
    }

    reload()
    {
        window.location.href = this.core().baseUrl;
    }

    core()
    {
        return Core.get()
	}
	
	hasMenu()
	{
		return this.core().menu && this.core().menu.length > 0
	}

    onLogout()
    {
		if(this.core().accountProfile)
		{
			this.push('logout-account');
		}
		else
		{
			const eventPlugin = this.core().injector.get(EventPlugin);
			eventPlugin.dispatch('logout', this.core().user);
			eventPlugin.dispatch(this.core().user.appid + ':logout', this.core().user);	
		}
	}

	getBuiderClass()
	{
		if(this.core().device)
		{
			return 'builder-' + this.core().device.platform + '-' + this.core().builder;
		}
	}

	getMenuClass()
	{
		if(this.core().routeParams && this.core().routeParams.menu)
		{
			return 'menu-' + this.core().routeParams.menu;
		}
	}

	getMobileStyle()
	{
		if(this.core().account && this.core().account.mobileStyle)
		{
			return this.core().account.mobileStyle;
		}
		else
		{
			return;
		}
	}
	
	async runNotification(notification)
	{
		console.log('notification', notification);

		Haptics.vibrate();
		Haptics.impact({ style: ImpactStyle.Heavy });

		let args : any = {};

		if(typeof notification.data.action == 'string')
		{
			notification.data.action = JSON.parse(notification.data.action);
		}

		if(notification.data.params && typeof notification.data.params == 'string')
		{
			notification.data.params = JSON.parse(notification.data.params);

			for(const key in notification.data.params)
			{
				args[notification.data.params[key].label] = notification.data.params[key].value;
			}
		}

		if(notification.data.action.value == "confirm")
		{
			const alertController = this.core().injector.get(AlertController);
			
			const cancelText  = (args.cancelText  ? args.cancelText  : 'Rejeitar');
			const confirmText = (args.confirmText ? args.confirmText : 'Aceitar');	

			const alert = await alertController.create({
				header   		: notification.title,
				message  		: notification.data.description,
				backdropDismiss : false,
				buttons  : [
				{
					text    : cancelText,
					role    : 'cancel',
					handler : (blah) => 
					{
						
					}
				}, 
				{
					text    : confirmText,
					handler : () => 
					{					
						if(notification.data.router)
						{
							this.core().push(notification.data.router, args);
						}							
					}
				}]
			});
			
			await alert.present();
		}
		else if(notification.data.action.value == "alert")
		{
			const alertController = this.core().injector.get(AlertController);

			const alert = await alertController.create({
				header   		: notification.title,
				message  		: notification.data.description,	
				backdropDismiss : false,			
				buttons  : [
					{
						text    : 'Ok',
						role    : 'cancel',
						handler : (blah) => 
						{
							if(notification.data.router)
							{
								this.core().push(notification.data.router, args);
							}
						}
					},
				]
			});
			
			await alert.present();
		}

		this.notifications.push(notification);		
	}

    async initialize()
    {						
		/* APP */
		App.addListener('appStateChange', (state: AppState) => 
		{
			const eventPlugin = this.core().injector.get(EventPlugin);
        	eventPlugin.dispatch('appStateChange', state);
		});

		/* INFO */
		const device 	   = await Device.getInfo();
		this.core().device = device; 

		console.log('device', device);

		if(this.core().isIOS || this.core().isAndroid)
		{
			this.initializeMobile();
		}
	}
	
	async initializeMobile()
	{
		//SplashScreen
		SplashScreen.hide();

		/* STATUS BAR */
		StatusBar.setStyle({style: Style.Dark});
		//StatusBar.hide();
		
		/* PUSH */		
		PushNotifications.addListener('registration', (token: Token) => 
		{
			this.core().notification = new Notification({
				token  : token.value,
				device : this.core().device,
			}); 
			
			console.log('token ' + token.value);
		});

		PushNotifications.addListener('registrationError', (error: any) => 
		{
			console.log('error on register ' + JSON.stringify(error));
		});

		PushNotifications.addListener('pushNotificationReceived', (notification: PushNotificationSchema) => 
		{            
			this.runNotification(notification);
		});

		PushNotifications.addListener('pushNotificationActionPerformed', (data: ActionPerformed) => 
		{
			data.notification.title = data.notification.data.title;
			data.notification.body  = data.notification.data.body;

			delete data.notification.data.title;
			delete data.notification.data.body;

			this.runNotification(data.notification);
		});	

		PushNotifications.requestPermissions().then((result) => 
		{
			if (result.receive === 'granted') 
			{
				// Register with Apple / Google to receive push via APNS/FCM
				PushNotifications.register();
			} else {
				// Show some error
			}

			console.log('requestPermission', result);
		});

		/* KEYBOAR */
		Keyboard.addListener('keyboardDidShow', (info: KeyboardInfo) =>
		{
			document.body.classList.add('keyboard-is-open');
			console.log('keyboard did show with height', info.keyboardHeight);
		});

		Keyboard.addListener('keyboardDidHide', () =>
		{
			document.body.classList.remove('keyboard-is-open');
			console.log('keyboard did hide');
		});

		/* NETWORK */
		Network.addListener('networkStatusChange', (status) =>
		{
			console.log("Network status changed", status);

			if(!status.connected)
			{
				const alertController = this.core().injector.get(AlertController);

				alertController.create({
					header  : 'Alerta',
					message : 'Internet não contectada',
					buttons : ['OK']
				})
				.then((alert:any) =>
				{
					alert.present();
				});
			}
		});

		Network.getStatus().then(status =>
		{
			console.log("Network status changed", status);

			if(!status.connected)
			{
				const alertController = this.core().injector.get(AlertController);

				alertController.create({
					header  : 'Alerta',
					message : 'Internet não contectada',
					buttons : ['OK']
				})
				.then((alert:any) =>
				{
					alert.present();
				});
			}
		});

		//Exiting the Appv
		App.addListener('backButton', event => 
		{
			if(!this.core().hasBack())
			{
				App.exitApp();
			}
		});
	}
}
