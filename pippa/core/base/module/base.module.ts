import { Directive } from '@angular/core';

/* APP */
import { Core } from '../../util/core/core';

@Directive()
export class BaseModule
{
    core()
    {
        return Core.get()
    }
}
