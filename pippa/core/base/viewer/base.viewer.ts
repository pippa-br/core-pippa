import { Input, Output, OnInit, EventEmitter, Directive } from '@angular/core';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { BaseApp }         from '../app/base.app';
import { Core }            from '../../util/core/core';

@Directive()
export abstract class BaseViewer extends BaseApp
{
    public alertController : AlertController;

    @Input() viewer : any;
    @Input() plugin : any;

    @Output('add')  addEvent  : EventEmitter<any> = new EventEmitter();
    @Output('set')  setEvent  : EventEmitter<any> = new EventEmitter();
    @Output('del')  delEvent  : EventEmitter<any> = new EventEmitter();

    constructor(
    )
    {
        super();

        /* INJECTOR */
        this.alertController = this.core().injector.get(AlertController);
    }

    onAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    onSet(event:any)
    {
        this.setEvent.emit(event);
    }

    onDel(event:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja realmente esse item?',
            buttons : [
                {
                    text: 'Fechar',
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text: 'Remover',
                    handler : () =>
                    {
                        this.doDel(event.data)

                        this.alertController.dismiss();
                        this.delEvent.emit(event);

                        return false;
                    }
                }
            ]
        })
        .then((alert:any) =>
        {
            alert.present();
        });
    }

    doDel(data:any)
    {
        data.del();
    }

    core()
    {
        return Core.get();
    }
}
