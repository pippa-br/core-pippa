import { Input, Output, EventEmitter, Directive } from '@angular/core';

/* PIPPA */
import { BaseComponent } from '../base.component';
import { App }           from '../../model/app/app';

@Directive()
export abstract class BaseApp extends BaseComponent
{
    @Output('add')     addEvent     : EventEmitter<any> = new EventEmitter();
    @Output('set')     setEvent     : EventEmitter<any> = new EventEmitter();
    @Output('archive') archiveEvent : EventEmitter<any> = new EventEmitter();
    @Output('del')     delEvent     : EventEmitter<any> = new EventEmitter();
    @Output('cancel')  cancelEvent  : EventEmitter<any> = new EventEmitter();
    @Output('close')   closeEvent   : EventEmitter<any> = new EventEmitter();
    @Output('invalid') invalidEvent : EventEmitter<any> = new EventEmitter();
    @Output('error')   errorEvent   : EventEmitter<any> = new EventEmitter();
    @Output('viewer')  viewerEvent  : EventEmitter<any> = new EventEmitter();
    @Output('rendererComplete') rendererComplete  : EventEmitter<any> = new EventEmitter();

    protected _form : any;
    protected _data : any;
    protected _acl : any;
    protected _app : any;
    protected _collection : any;

    public plugin : any;

    constructor(
    )
    {
        super();
    }

    @Input()
    get collection():any
    {
        return this._collection;
    }

    /* INPUT E OUTPUT - [(filter)] */
    //@Output('collectionChange') collectionChange : EventEmitter<any> = new EventEmitter();
    set collection(value:any)
    {
        this._collection = value;
        //this.collectionChange.emit(value);

        this.loadCollection();
    }

    loadCollection()
    {
    }

    get hasPagination()
    {
        return this.collection && (this.collection.hasPrevPage() || this.collection.hasNextPage())
    }

    @Input()
    set data(data:any)
    {
        this._data = data;

        if(this._data)
        {
            this.loadData();
        }
    }

    get data():any
    {
        return this._data;
    }

    loadData()
    {
    }

    @Input()
    set form(form:any)
    {
        this._form = form;

        if(this._form)
        {
            this.loadForm();
        }
    }

    get form():any
    {
        return this._form;
    }

    loadForm()
    {
    }

    @Input()
    set acl(acl:any)
    {
        this._acl = acl;

        if(this._acl)
        {
            this.loadAcl();
        }
    }

    get acl():any
    {
        return this._acl;
    }

    loadAcl()
    {
    }

    @Input()
    set app(app:any)
    {
        this._app = app;

        if(this._app)
        {
            this.plugin = this.app.getPlugin();

            this.loadApp();
        }
    }

    get app():any
    {
        return this._app;
    }

    loadApp()
    {
    }

    emitRendererComplete(event:any)
    {
        this.rendererComplete.emit(event);
    }

    emitAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    emitSet(event:any)
    {
        this.setEvent.emit(event);
    }

    emitClose()
    {
        this.closeEvent.emit();
    }

    emitDel(event:any)
    {
        this.delEvent.emit(event);
    }

    emitArchive(event:any)
    {
        this.archiveEvent.emit(event);
    }

    emitCancel(event:any)
    {
        this.cancelEvent.emit(event);
    }

    onViewer(event:any)
    {
        event;
    }

    onSet(event:any)
    {
        this.emitSet(event);
    }

    onAdd(event:any)
    {
        this.emitAdd(event);
    }

    onDel(event:any)
    {
        this.emitDel(event);
    }

    createParams(args?:any)
    {
        return this.core().paramsFactory.create(args);
    }

    hasAcl(name:string, value?:any)
    {
        return this.acl && this.acl.hasKey(name, value);
    }

    getAcl(name:string, value?:any)
    {
        return this.acl && this.acl.getKey(name, value);
    }

    createResult(params:any)
    {
        return this.core().resultFactory.create(params);
    }

    goInit()
    {
        this.core().goInit();
    }

    push(router:any, queryParams?:any, routeParams?:any)
    {
        this.core().push(router, queryParams, routeParams);
    }

    back()
    {
        this.core().back();
    }

    hasBack()
    {
        return this.core().hasBack() || this.core().isModal;
    }
}
