import { Input, Directive } from '@angular/core';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { Core }   from '../util/core/core';
import { Result } from '../util/result/result';

@Directive()
export abstract class BaseComponent
{
    @Input() modeForm    : string  = 'add';
	public _isInitialize : boolean = false;
	
	public adoptedStyleSheets = [];

    createParams(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    @Input()
    set isInitialize(value:boolean) 
    {
        this._isInitialize = value;

        if(value)
        {
            this.initialize(value);
        }        
    }

    get isInitialize()
    {
        return this._isInitialize;
    }

    initialize(isInitialize:boolean)
    {        
    }

    createResult(params:any)
    {
        return this.core().resultFactory.create(params);
    }

    getApp(name:string)
    {
        return this.core().getApp(name);
    }

    hasBack()
    {
        return this.core().hasBack() || this.core().isModal;
    }

    goMenu(menu:any)
    {
        this.core().goMenu(menu);
	}

	goMenuByName(name:string)
    {
        this.core().goMenuByName(name);
	}

	getMenuByName(name:string)
    {
        this.core().getMenuByName(name);
	}
	
    push(router:string, queryParams?:any, routeParams?:any)
    {
        this.core().push(router, queryParams, routeParams);
    }

    back()
    {
        this.core().back();
    }

    goInit()
    {
        return this.core().goInit();
    }

    goFirstMenu()
    {
        return this.core().goFirstMenu();
    }

    getPlugin(name:string)
    {
        return this.core().getPlugin(name);
    }

    getViewer(name:string)
    {
        return this.core().getViewer(name)
    }

    api()
    {
        return this.core().api;
    }

    core()
    {
        return Core.get()
    }

	async openUrl(url:string) 
	{
		this.core().util.openUrl(url);	
	}

    trackById(index:any, item:any)
    {
        index;
        return item.id;
	}
	
	showAlert(title:string, message:string)
    {
        const alertController = this.core().injector.get(AlertController);

        alertController.create({
            header  : title,
            message : message,
            buttons : ['OK']
        })
        .then((alert:any) =>
        {
            alert.present();
        });
    }

    onError(result:Result)
    {
        let message : string = '';

        if(result.form && result.form.messages)
        {
            for(let key in result.form.messages)
            {
                message = this.getMessage(result.form.messages[key]);
            }

            const alertController = this.core().injector.get(AlertController);

            alertController.create({
                header  : 'Alerta',
                message : message,
                buttons : ['OK']
            })
            .then((alert:any) =>
            {
                alert.present();
            });
        }

        console.log('error', message);

        //this.errorEvent.emit(result);
	}

    getMessage(message:any)
    {
        if(typeof message == 'string')
        {
            return message;
        }
        else
        {
            for(let key in message)
            {
                let m = this.getMessage(message[key]);

                if(typeof m == 'string')
                {
                    return m;
                }
            }
        }
    }
}
