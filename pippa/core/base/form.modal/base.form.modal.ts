import { Directive, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseForm } from "../form/base.form";

@Directive()
export abstract class BaseFormModal extends BaseForm
{
    public title    : string;
    public onClose  : any = null;
    public onCancel : any = null;
    public onAdd    : any = null;
    public onSet    : any = null;
    public onSave   : any = null;

    _onAdd(event)
    {
        if (this.onAdd)
        {
            this.onAdd(event);
        }

        if (this.onSave)
        {
            this.onSave(event);
        }
    }

    _onSet(event)
    {
        if (this.onSet)
        {
            this.onSet(event);
        }

        if (this.onSave)
        {
            this.onSave(event);
        }
    }

    _onCancel()
    {
        if (this.onCancel)
        {
            this.onCancel();
        }
        else if (this.onClose)
        {
            this.onClose();
        }
    }

    _onClose()
    {
        if (this.onClose)
        {
            this.onClose();
        }
    }
}
