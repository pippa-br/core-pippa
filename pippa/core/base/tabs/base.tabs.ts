import { OnChanges, AfterViewInit, OnDestroy, Directive } from '@angular/core';

/* PIPPA */
import { BaseApp }  from '../app/base.app';

@Directive()
export abstract class BaseTabs extends BaseApp implements OnChanges, AfterViewInit, OnDestroy
{
    tabs : Array<any> = [];

    constructor(
    )
    {
        super();
    }

    ngOnChanges()
    {
    }

    push(page)
    {
        this.tabs.push(page.component);
    }

    ngAfterViewInit()
    {

    }

    ngOnDestroy()
    {
    }
}
