import { Directive } from '@angular/core';

/* PIPPA */
import { BaseApp } 		  from '../app/base.app';
import { DocumentPlugin } from '../../../core/plugin/document/document.plugin';

@Directive()
export abstract class BaseModal extends BaseApp
{
    public title   		 : string;
	public onClose 	  	 : any;
	public onReload 		 : any;
	public _documentPath : string;
	public _appid        : string;
	public _document     : any;

	set documentPath(path:string)
	{
		this._documentPath = path;				

		if(this._documentPath)
		{
			const documentPlugin = this.core().injector.get(DocumentPlugin);

			const params2 = this.createParams({
				path  : this._documentPath,
			});

			documentPlugin.getObject(params2).then((result:any) =>
			{
				this.document = result.data;							
			});
		}		
	}

	get documentPath()
	{
		return this._documentPath;
	}

	set document(document:any)
	{
		this._document = document;	
		
		if(this._document)
		{
			console.log('document', this._document.referencePath);

			this.loadDocument();
		}
	}

	get document()
	{
		return this._document;
	}

	set appid(appid:string)
	{
		this._appid = appid;	
		
		if(this._appid)
		{
			this.loadApp();
		}
	}

	get appid()
	{
		return this._appid;
	}

	loadDocument()
	{

	}

	loadApp()
	{

	}
}
