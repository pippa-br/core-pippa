import { Input, Directive } from '@angular/core';

/* PIPPA */
import { BaseApp } from '../app/base.app';
import { Form }    from '../../model/form/form';
import { Types } from '../../type/types';

@Directive()
export abstract class BaseForm extends BaseApp
{
    public alertController : any;

    @Input()
    set form(form:any)
    {
        if(form)
        {
            if(!form.reference && form.referencePath)
			{
				this.initializeForm(form.referencePath);
			}
			else
			{
				form.on(true).then(() =>
				{				
					this._form 			 = form;
					this._form.hasHeader = false;
					this.loadForm();
				})	
			}			
        }
        else
        {
            this._form = form;
        }
    }

    get form():any
    {
        return this._form;
    }

	async initializeForm(formPath:any)
    {
        const paths = formPath.split('/');

        const params = this.createParams({
            getEndPoint : Types.GET_DOCUMENT_API,
            accid       : paths[0],
            appid       : paths[1],
            colid       : paths[2],
            path        : formPath,
            model       : Form,
        })
        
		const result = await this.api().getObject(params)
		
		console.log('load form', result.data);

		this._form 			 = result.data;
		this._form.hasHeader = false;
		this.loadForm();
    };

    async copyForm()
	{
        this.core().util.copyForm(this.data);            
	}

	async pastForm()
	{
        const data = await this.core().util.pastForm(this.data);

		if(data)
		{
            const referencePath = this.form.reference.path;

            // RELOAD FORM
            this.form = null;
			this.form = {
                referencePath : referencePath
            }
        }
	}

    clear()
    {
        this.data = {};
    }

    onAdd(data:any)
    {
        //this.display = false;
        this.addEvent.emit(data);
    }

    onSet(data:any)
    {
        //this.display = false;
        this.setEvent.emit(data);
    }

    onCancel()
    {
        //this.display = false;

        this.cancelEvent.emit({
            data : this.data,
            form : this.form,
        });
    }

	onRendererComplete(event?:any)
    {
        //this.display = false;
        this.rendererComplete.emit();
    }

    onInvalid(data:any)
    {
        this.invalidEvent.emit(data);
    }    
}
