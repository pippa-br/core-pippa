import { HostBinding, Input, OnDestroy, Directive } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Title } from '@angular/platform-browser';

/* PIPPA */
import { BaseComponent } from '../base.component';
import { User }          from '../../model/user/user';
import { Form }          from '../../model/form/form';
import { Viewer }        from '../../model/viewer/viewer';
import { Grid }          from '../../model/grid/grid';
import { Chart }         from '../../model/chart/chart';
import { AclPlugin }     from '../../plugin/acl/acl.plugin';
import { BaseModel } 	 from '../../../core/model/base.model';
import { AgreeModal }    from '../../modal/agree/agree.modal';
@Directive()
export abstract class BasePage extends BaseComponent
{
    @HostBinding('class') nameClass : string  = ' '; 

    public currentArgs     : any;
    public _isLoaded       : any;
    public _form           : any;
	public _data           : any;
	public _document       : any;
    public _viewer         : any;
    public _grid           : any;
    public _acl            : any;
	public _app            : any;
	public _chart          : any;
    public _collection     : any;
	public _documentPath   : string;
	public _dataPath       : string;
    public _formPath       : string;
	public _gridPath       : string;
	public _viewerPath     : string;
	public _chartPath      : string;
    public activatedRoute  : any;
    public routeParams     : any;
    public queryParams     : any;
    public aclPlugin       : any;
    public titleService    : any;
	public modalController : any;
	public alertController : any;
	public plugin          : any;
	public _searchTime     : any;
	public _timeout		   : any;

    constructor(
    )
    {
        super();

        this.activatedRoute  = this.core().injector.get(ActivatedRoute);
        this.aclPlugin       = this.core().injector.get(AclPlugin);
		this.modalController = this.core().injector.get(ModalController);
		this.alertController = this.core().injector.get(AlertController);
        this.titleService    = this.core().injector.get(Title)

		// REMOVE PAGE
		const removeElements = (elms) => elms.forEach(el => el.remove());
		removeElements( document.querySelectorAll(".current-page") );
		//this.clear();		
	}
	
    ionViewDidEnter()
    {
		console.log('ionViewDidEnter');
		
        this.core().initialize().then(async (isInitialize:boolean) =>
        {
            this.currentArgs = this.core().currentArgs;
            this.queryParams = this.core().queryParams;
            this.routeParams = this.core().routeParams;

            if(!this.queryParams)
            {
                this.queryParams = {};
            }

            if(!this.routeParams)
            {
                this.routeParams = {};
			}
			
			this.queryParams = new BaseModel(Object.assign({}, this.core().util.queryStringToJSON(window.location.href)));
			this.routeParams = new BaseModel(Object.assign({}, this.core().util.routeParamsToJSON(window.location.href)));

			/* ATUALIZA ACCOUNT */
			this.core().masterAccount.reload();

			/* VERIFICCA A VERSÃO DO APP */
			const published_Key = 'published_' + this.core().device.platform.value;

			if(this.core().builder != undefined && this.core().masterAccount[published_Key])
			{	
				const builder_Key = 'builder_' + this.core().device.platform.value;			

				if(this.core().masterAccount[builder_Key] != undefined && this.core().masterAccount[builder_Key] != this.core().builder)
				{
					this.push('auth-update', {}, { menu : 'auth-update' });
					this.initialize(isInitialize);
					return;
				}
			}

			/* PARAMS OPTION  */
			if(this.queryParams.option)
			{
				this.queryParams.option = JSON.parse(this.queryParams.option);
			}
			
			await this.queryParams.on();
			await this.routeParams.on();
			
			this.core().queryParams = this.queryParams;
			this.core().routeParams = this.routeParams;

			console.log('queryParams', this.queryParams, window.location.href);
			console.log('routeParams', this.routeParams, window.location.href);

			const promises = [];
			
			/*  ISFRAME  */
			this.core().iframe = window.self !== window.top;

			// MENU
			if(this.routeParams.menu)
			{
				this.core().currentMenu = this.core().getMenuById(this.routeParams.menu);
			}

            /* QUERY PARAMS */
            if(this.queryParams)
            {
                /* SE HOUVER ACCESS TRUE NÃO PERMITIR FAZER LOGIN */
                if(this.queryParams.access)
                {
                    this.core().user = new User({
                        name    : 'Login',
                        group   : 'guest'
                    });
                    this.core().isLogged = false;
					this.core().menu     = null;
					this.core().access   = true;
                }
                                
                /* APPID */
                if(this.queryParams.appid)
                {
                    this.app               = this.getApp(this.queryParams.appid);
                    this.core().currentApp = this.app;

                    this.titleService.setTitle(this.app.name);

                    promises.push(this.initializeAcl(this.app));
				}
				
				/* APPID */
                if(this.queryParams.language)
                {
                    promises.push(this.initializeLanguage(this.queryParams.language));
                }

                /* PARAMS DATA  */
                if(this.queryParams.data)
                {
                    promises.push(this.initializeParams(this.queryParams.data));
				}			

                /* PATH FORM VIA QUERY */
                if(this.queryParams.formPath)
                {
                    promises.push(this.initializeForm(this.queryParams.formPath));
                }

                /* PATH GRID VIA QUERY */
                if(this.queryParams.gridPath)
                {
                    promises.push(this.initializeGrid(this.queryParams.gridPath));
				}

				/* PATH CHART VIA QUERY */
                if(this.queryParams.chartPath)
                {
                    promises.push(this.initializeChart(this.queryParams.chartPath));
				}
				
				/* PATH VIEWER VIA QUERY */
                if(this.queryParams.viewerPath)
                {
                    promises.push(this.initializeViewer(this.queryParams.viewerPath));
				}
				
				/* PATH DOCUMENT VIA QUERY */
				if(this.queryParams.documentPath)
				{
					promises.push(this.initializeDocument(this.queryParams.documentPath));
				}

				/* PATH DOCUMENT VIA QUERY PARA FORMULARIOS */
				if(this.queryParams.dataPath)
				{
					promises.push(this.initializeData(this.queryParams.dataPath));
				}
				
                /* QUANDO DAR RELOAD NA PAGINA */
                if(this.core().navController.routes && this.core().navController.routes.length == 0)
                {
                    const pathname = window.location.pathname.substring(1).split(';');
                    this.core().navController.routes.push({
                        router      : pathname[0],
                        queryParams : this.queryParams,
                        routeParams : {
                            app : (this.core().currentApp ? this.core().currentApp.id : 0)
                        }
                    });
                }
            }

            Promise.all(promises).then(async () =>
            {
				this._isLoaded = true;

				// TRACK FACEBOOK
				this.core().trackFacebook('pageView');

				this.canActivate();
                await this.dispatchLogin(isInitialize);
                this.initialize(isInitialize);

                /* BUG: REMOVE TOOLTIPS */
                while(document.getElementsByTagName('mat-tooltip-component').length > 0) 
                { 
                    document.getElementsByTagName('mat-tooltip-component')[0].remove(); 
                }

                /* LOAD USER */
                if(this.core().user)
                {
                    this.loadUser();

                    if(this.core().account.userTermAppid && this.core().account.userTermAppid == this.core().user.appid && !this.core().user.agreeTerm)
                    {
                        this.modalController.create({
                            component       : AgreeModal,
                            backdropDismiss : false,
                            cssClass        : 'full',                        
                            componentProps  : {
                                user    : this.core().user,
                                url     : this.core().account.userTermUrl._url,
                                onClose : () =>
                                {
                                    this.modalController.dismiss();
                                }
                            }
                        })
                        .then((modal:any) =>
                        {
                            modal.present();
                        }); 
                    }                                       
                }   

				this.core().eventPlugin.dispatch('page-loading', {staus:true});

				//TIME PARA RELOAD INITAIVO
				this.resetTimeout();
            });
        });
	}

    getResetTime()
    {
        return 1000 * 60 * 60;
    }

	resetTimeout()
	{
		if(this._timeout)
		{
			clearTimeout(this._timeout);	
		}		
		
		this._timeout = setTimeout(() => 
		{
			document.location.reload();
		}, this.getResetTime());
	}

	showAlert(title:string, message:string, buttons?:any, inputs?:any)
    {
        this.alertController.create({
            header  : title,
			message : message,
			inputs  : inputs || [],
            buttons : buttons || ['OK']
        })
        .then((alert:any) =>
        {
            alert.present();
        });
	}

    changeParams(params:any)
    {
        params;
	}
	
	/* VERIFICA SE TEM PERMISSÕA PRA LOAD A PAGINA */
	canActivate()
    {
		if(this.core().canActivate)
		{
			this.core().canActivate();
		}
    }

    async dispatchLogin(isInitialize:boolean)
    {
		if(this.core().access)
		{
			// ACESSO SEM LOGIN
		}
		else if(this.core().user && this.core().user.isGuest())
		{
			// USER GUEST
		}
		else
		{
			/* SOMENTE NA PRIMEIRA VEZ POIS O USUARIO JA FOI PUXADO NA CORE */
			if(isInitialize)
			{
				if(this.core().user)
				{
					const group = this.core().user.getGroup();
	
					/* POR CAUSA DO RELOAD SEMPRE USAR AUTO-LOGIN*/
					this.core().eventPlugin.dispatch(group.value.toLowerCase() + ':auto-login', { status : true, data : this.core().user});
					this.core().eventPlugin.dispatch('auto-login', { status : true, data : this.core().user});
	
					console.log('dispatch ' + group.value.toLowerCase() + ':auto-login');	
				}
				else
				{					
					this.core().user     = null;
					this.core().isLogged = false;
					
					this.core().eventPlugin.dispatch('no-login');
					console.log('usuário não logado');
				}	
			}
			else
			{
				/* LOGIN BY APPID */
				const appid = await this.core().storagePlugin.get('appid');
				
				console.log('verify auth', appid);

				if(appid)
				{
					const params = this.core().paramsFactory.create({
						appid : appid
					});
			
					const result = await this.core().authPlugin.getLogged(params);
	
					// SE A SESSION ACABOU REMOVE O USUARIO
					if(!result.status)
					{
						this.core().user     = null;
						this.core().isLogged = false;
						
						this.core().eventPlugin.dispatch('no-login');
						console.log('usuário não logado');
					}
				}				
			}						
		}							
    }

    loadUser()
    {
    }

    get isInitialize()
    {
        return this.core().isInitialize;
    }

    initialize(isInitialize:boolean)
    {
        isInitialize;
	}
	
	getPlugin()
	{
		return this.app.getPlugin();
	}

    @Input()
    set app(app:any)
    {
        this._app = app;

        if(this._app)
        {
            this.nameClass = this._app.accid + ' ' + this._app.code + '-app ion-page current-page ';

			this.plugin = this.getPlugin();
			
            if(this.app._track)
            {
				// TRACK FACEBOOK
                this.core().trackFacebook(this.app._track);
            }

            this.loadApp();
        }
    }

    get app():any
    {
        return this._app;
    }

    @Input()
    set form(form:any)
    {
        this._form = form;
    }

    get form():any
    {
        return this._form;
    }

    @Input()
    set documentPath(value:any)
    {
        this._documentPath = value;
    }

    get documentPath():any
    {
        return this._documentPath;
	}
	
	@Input()
    set dataPath(value:any)
    {
        this._dataPath = value;
    }

    get dataPath():any
    {
        return this._dataPath;
    }

    @Input()
    set formPath(value:any)
    {
        this._formPath = value;
    }

    get formPath():any
    {
        return this._formPath;
    }

    @Input()
    set data(data:any)
    {
		this._data = data;
		
		if(data)
		{
			this.nameClass += 'data-' + this.data.id + ' ';
			this.loadData();  
		}		      
    }

    get data():any
    {
        return this._data;
	}
	
	@Input()
    set document(document:any)
    {
		this._document = document;
		
		if(document)
		{
			this.nameClass += 'document-' + this.document.id + ' ';
			this.loadDocument();	
		}
    }

    get document():any
    {
        return this._document;
    }

    @Input()
    set acl(acl:any)
    {
        this._acl = acl;

        if(this._acl)
        {
            this.loadAcl();
        }
    }

    get acl():any
    {
        return this._acl;
    }

    @Input()
    set viewer(viewer:any)
    {
        this._viewer = viewer;

        if(this._viewer)
        {
            this.loadViewer();
        }
    }

    get viewer():any
    {
        return this._viewer;
    }

    @Input()
    set grid(grid:any)
    {
        this._grid = grid;

        if(this._grid)
        {
            this.loadGrid();
        }
    }

    get grid():any
    {
        return this._grid;
	}
	
	@Input()
    set chart(chart:any)
    {
        this._chart = chart;

        if(this._chart)
        {
            this.loadChart();
        }
    }

    get chart():any
    {
        return this._chart;
    }

    loadGrid()
    {

	}
	
	loadChart()
    {

    }

    loadViewer()
    {

    }

    loadData()
    {

	}
	
	loadDocument()
    {

    }

    loadAcl()
    {

    }

    loadApp()
    {
    }

    initializeAcl(app:any)
    {
        return new Promise<void>((resolve) =>
        {
            /* GET ACL */
            const params = this.createParams({
                appid  : app.code,
                groups : (this.core().user ? this.core().user.getGroups() : ['guest'])
			});
			
            this.aclPlugin.get(params).then((result:any) =>
            {
                this.acl 		= result.data;
				this.core().acl = this.acl;

				resolve();
            });
        });
	}
	
	initializeLanguage(value:string)
	{
		return new Promise<void>(async (resolve) =>
        {
			/* LOAD LANGUAGE */
			await this.core().storagePlugin.set('language_' + this.core().platform.value, value);

			await this.core().loadLanguage();

			resolve();
		});
	}

    initializeParams(data:any)
    {
        data = JSON.parse(data);

        return new Promise<void>((resolve) =>
        {
            const promises = [];
            const keys     = [];

            for(const key in data)
            {
                const value = data[key];

                /* VERIFICA SE É PATH */
                if(typeof value == 'string' && (value.match(/\//g) || []).length > 2)
                {
                    const paths = value.split('/');

                    if(!paths[0])
                    {
                        paths.shift();
                    }

                    const params = this.createParams({
                        accid       : paths[0],
                        appid       : paths[1],
                        colid       : paths[2],
                        path        : value,
                    });

                    promises.push(this.plugin.getObject(params));
                    keys.push(key);
                }
            }

            Promise.all(promises).then((results:any) =>
            {
                for(const i in keys)
                {
                    const key = keys[i];

                    // VERIFICA REFERENCEIAS BASEADAS COM STATUS
                    if(results[i].data.status !== undefined)
                    {
                        if(results[i].data.status === true)
                        {
                            data[key] = results[i].data.reference;
                        }             
                        else
                        {
                            delete data[key];
                        }           
                    }
                    else
                    {
                        data[key] = results[i].data.reference;
                    }                    
                }

				this.queryParams.data   = data;
				this.core().queryParams = this.queryParams;

                console.log('queryParams', this.queryParams.data);

                resolve();
            })
        });
    };

    initializeData(dataPath:any)
    {
        return new Promise<void>((resolve) =>
        {
            this._dataPath = dataPath;

            if(this._dataPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : this.app._colid,
                    path  : this._dataPath,
                    includeForm : true
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
                    this.data		 = result.data;
					this.form 		 = result.data.form;					
					this.core().data = this.data;

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
	};
	
	initializeDocument(documentPath:any)
    {
        return new Promise<void>((resolve) =>
        {
			this._documentPath = documentPath;					

            if(this._documentPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : this.app._colid,
                    path  : this._documentPath,
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
                    this.document        = result.data;
					this.core().document = this.document;

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    };

    initializeForm(formPath:any)
    {
        return new Promise<void>((resolve) =>
        {
            this._formPath = formPath;

            console.log('formPath', formPath);

            if(this._formPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : 'forms',
                    path  : this._formPath,
                    model : Form,   
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
                    console.log('load form', result.data);

					this.form 		 = result.data;
					this.core().form = this.form;

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    };

    initializeGrid(gridPath:any)
    {
        return new Promise<void>((resolve) =>
        {
            this._gridPath = gridPath;

            console.log('gridPath', gridPath);

            if(this._gridPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : 'grids',
                    path  : this._gridPath,
                    model : Grid,
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
					this.grid 		 = result.data;
					this.core().grid = this.grid;

                    console.log('load grid', this.grid);

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
	};

	initializeChart(chartPath:any)
    {
        return new Promise<void>((resolve) =>
        {
            this._chartPath = chartPath;

            console.log('chartPath', chartPath);

            if(this._chartPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : 'chart',
                    path  : this._chartPath,
                    model : Chart,
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
					this.chart 		 = result.data;
					this.core().chart = this.chart;

                    console.log('load chart', this.chart);

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
	};
	
	initializeViewer(viewerPath:any)
    {
        return new Promise<void>((resolve) =>
        {
            this._viewerPath = viewerPath;

            console.log('viewerPath', viewerPath);

            if(this._viewerPath)
            {
                const params2 = this.createParams({
                    accid : this.core().account.code,
                    appid : this.app.code,
                    colid : 'viewer',
                    path  : this._viewerPath,
                    model : Viewer,
                });

                this.plugin.getObject(params2).then((result:any) =>
                {
					this.viewer 	   = result.data;
					this.core().viewer = this.viewer;

                    console.log('load viewer', this.viewer);

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
	};
	
	onSearch(event:any)
    {
        if(event.target.value)
		{
            if(this._searchTime)
            {
                clearTimeout(this._searchTime);
            }
    
            this._searchTime = setTimeout(() => 
            {
                this.collection.search(event.target.value);
            }, 500);    
        }
	}

	clearSearch()
    {
        if(this.collection)
		{
			this.collection.search();
		}		
	}
	
	async onArchive(event:any)
    {
        const alert = await this.alertController.create({
            header  : 'Alerta',
            message : (event.data._archive ? 'Deseja Abrir Esse Item?' : 'Deseja Arquivar Esse Item?'),
            buttons : [
                {
                    text    : 'Não',
					role    : "no",
                    handler : () =>
                    {
                    }
                },
                {
                    text    : 'Sim',
					role 	: "yes",
                    handler : () =>
                    {                        
                    }
                }
            ]
        });

		await alert.present();

		const { role } = await alert.onDidDismiss();

		if(role == 'yes')
		{
			this.doArchive(event);
		}
	}
	
	doArchive(event:any)
	{		
	}

    async onCancel(event:any)
    {
        const alert = await this.alertController.create({
            header  : 'Alerta',
            message : (event.data._canceled ? 'Deseja Abrir Esse Item?' : 'Deseja Fechar Esse Item?'),
            buttons : [
                {
                    text: 'Não',
					role : "no",
                    handler : () =>
                    {
                    }
                },
                {
                    text: 'Sim',
					role : "yes",
                    handler : () =>
                    {                        
                    }
                }
            ]
        });

		await alert.present();

		const { role } = await alert.onDidDismiss();

		if(role == 'yes')
		{
			this.doCancel(event);
		}
	}
	
	doCancel(event:any)
	{		
	}

    async onClone(event:any)
    {
        const alert = await this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja duplicar esse item?',
            buttons : [
                {
                    text	: 'Não',
					role    : "no",
                    handler : () =>
                    {
                    }
                },
                {
                    text	: 'Sim',
					role    : "yes",
                    handler : () =>
                    {                        
                    }
                }
            ]
        });

		await alert.present();

		const { role } = await alert.onDidDismiss();

		if(role == 'yes')
		{
			this.doClone(event);
		}
	}
	
	doClone(event:any)
	{		
	}

    async onDel(event:any)
    {
        const alert = await this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text	: 'Não',
					role    : "no",
                    handler : () =>
                    {
                    }
                },
                {
                    text	: 'Sim',
					role    : "yes",
                    handler : () =>
                    {                        
                    }
                }
            ]
        });

		await alert.present();

		const { role } = await alert.onDidDismiss();

		if(role == 'yes')
		{
			this.doDel(event);
		}
	}
	
	doDel(event:any)
	{		
	}

    set collection(value:any)
    {
        this._collection = value;
    }

    get collection():any
    {
        return this._collection;
    }

    hasAcl(name:string, value?:any)
    {
        return this.acl && this.acl.hasKey(name, value);
    }

    getAcl(name:string, data?:any)
    {
        return this.acl && this.acl.getKey(name, data);
    }

    get hasPagination()
    {
        return this.collection && (this.collection.hasPrevPage() || this.collection.hasNextPage())
    }

    goInit()
    {
        this.core().goInit();
    }

    ionViewDidLeave()
    {
		console.log('ionViewDidLeave');

		clearTimeout(this._timeout);
		
		this.ngOnDestroy()

        this.clear();
    }

	ngOnDestroy()
	{
		this.clear();
	}  

    builderForm()
    {
        
    }
    
    async copyForm()
	{
        this.core().util.copyForm(this.data);            
	}

	async pastForm()
	{
        const data = await this.core().util.pastForm(this.data);

		if(data)
		{
            this.form = null;
			this.builderForm()
        }
	}

    clear()
    {
		this._isLoaded    = false;
        this.form         = null;
        this.viewer       = null;
        this.grid         = null;
        this.chart        = null;
        this.data         = null;
        this.acl          = null;
        this.app          = null;
        this.collection   = null;
		this.documentPath = null;
		this.dataPath 	  = null;
		this.document 	  = null;
        this.formPath     = null;
    }
}
