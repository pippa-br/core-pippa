import { Input, EventEmitter, Output, Directive } from '@angular/core';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { BaseApp }      from '../app/base.app';
import { Core }         from '../../../core/util/core/core';
import { FilterPlugin } from '../../../core/plugin/filter/filter.plugin';

@Directive()
export abstract class BaseGrid extends BaseApp
{
    public alertController : AlertController;
    public filterPlugin    : FilterPlugin;
	public _grid      	   : any;

    @Input() plugin     : any;    
    @Input() reference  : any;

    @Output('add')     addEvent     : EventEmitter<any> = new EventEmitter();
    @Output('set')     setEvent     : EventEmitter<any> = new EventEmitter();
    @Output('del')     delEvent     : EventEmitter<any> = new EventEmitter();
    @Output('cancel')  cancelEvent  : EventEmitter<any> = new EventEmitter();
    @Output('archive') archiveEvent : EventEmitter<any> = new EventEmitter();
	@Output('viewer')  viewerEvent  : EventEmitter<any> = new EventEmitter();
	@Output('email')   emailEvent   : EventEmitter<any> = new EventEmitter();

    constructor(
    )
    {
        super();

        this.alertController = this.core().injector.get(AlertController);
    }

    get grid():any
    {
        return this._grid;
    }

	@Input()
    set grid(value:any)
    {
        this._grid = value;

        if(this._grid)
        {
            this.loadGrid();
        }
    }

    loadGrid()
    {
    }

    onAdd(event:any)
    {
        this.emitAdd(event);
    }

    onSet(event:any)
    {
        this.emitSet(event);
    }

    onViewer(event:any)
    {
        this.emitViewer(event);
	}
	
	onEmail(event:any)
    {
        this.emitEmail(event);
    }

    onArchive(event:any)
    {
        this.emitArchive(event);
    }

    onCancel(event:any)
    {
        this.emitCancel(event);
    }

    emitViewer(event:any)
    {
        this.viewerEvent.emit(event);
	}
	
	emitEmail(event:any)
    {
        this.emailEvent.emit(event);
    }

    emitAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    emitSet(event:any)
    {
        this.setEvent.emit(event);
    }

    emitArchive(event:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : (event.data._archive ? 'Deseja Abrir Esse Item?' : 'Deseja Arquivar Esse Item?'),
            buttons : [
                {
                    text    : 'Não',
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text    : 'Sim',
                    handler : () =>
                    {
                        this.doArchive(event);

                        this.alertController.dismiss();
                        super.emitArchive(event);

                        return false;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        });
    }

    emitCancel(event:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : (event.data._canceled ? 'Deseja Abrir Esse Item?' : 'Deseja Fechar Esse Item?'),
            buttons : [
                {
                    text: 'Não',
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text: 'Sim',
                    handler : () =>
                    {
                        this.doCancel(event);

                        this.alertController.dismiss();
                        super.emitCancel(event);

                        return false;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        })
    }

    emitDel(event:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text: 'Não',
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text: 'Sim',
                    handler : () =>
                    {
                        this.doDel(event.data);

                        this.alertController.dismiss();
                        super.emitDel(event);

                        return false;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        })
    }

    doDel(item:any)
    {
        item;
    }

    doCancel(item:any)
    {
        item;
    }

    doArchive(item:any)
    {
        item;
    }

    core()
    {
        return Core.get();
    }
}
