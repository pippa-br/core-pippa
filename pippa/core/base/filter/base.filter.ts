import { Input, HostBinding, EventEmitter, Output, Directive } from '@angular/core';

/* PIPPA */
import { BaseApp } from '../app/base.app';

@Directive()
export abstract class BaseFilter extends BaseApp
{
    @HostBinding('class') nameClass : string = '';

    @Input() filter  : any;

    @Output('change') changeEvent : EventEmitter<any> = new EventEmitter();

    protected route;

    constructor(
        //public navParams     : NavParams,
        //public navController : NavController,
    )
    {
        super();
    }

    onChange(event)
    {
        /* UPDATE QUERY */
        let query = this.collection.getQuery();

        console.log('filter change', event);

        if(event)
        {
            query.where = event.data;
        }
        else
        {
            query.where = null;
        }

        this.collection.setQuery(query);
        this.collection.reload();

        /* EMIT EVENT */
        this.changeEvent.emit(event);
    }
}
