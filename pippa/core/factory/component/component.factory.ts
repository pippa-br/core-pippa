import { Injectable } from '@angular/core';
import { ComponentFactoryResolver} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ComponentFactory
{
    constructor(
        public resolver : ComponentFactoryResolver,
    )
    {
    }

    /* https://brianflove.com/2019/12/13/lazy-load-angular-v9-components/ */
    async create(container:any, component: any)
    {		
		if(typeof component == 'string')
		{	
			console.log(component);

			//const { component2 } : any = await import('/pippa/core/component/input/text/text.input');
			//component = component2;
		}

        const factory = this.resolver.resolveComponentFactory(component);
        const ref     = container.createComponent(factory);

        //ref.instance.changeDetectorRef = ref.changeDetectorRef;

        return ref;

        // Inputs need to be in the following format to be resolved properly
        ////let inputProviders = Object.keys(inputs).map((inputName) => {return {provide: inputName, useValue: inputs[inputName]};});
        ////let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

        // We create an injector out of the data we want to pass down and this components injector
        ////let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, container.parentInjector);

        // We create a factory out of the component we want to create
        ////let factory = this.resolver.resolveComponentFactory(component);

        // We create the component using the factory and the injector
        /////return factory.create(injector);

        // We insert the component into the dom container
        //this.container.insert(component.hostView);

        // Destroy the previously created component
        //if (this.currentComponent) {
          //this.currentComponent.destroy();
        //}

         //this.currentComponent = component;
      }
}
