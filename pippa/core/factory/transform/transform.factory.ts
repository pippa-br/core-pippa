/* FIELD */
import { FieldType }             from '../../type/field/field.type';
import { FormItem }              from '../../model/form.item/form.item';
import { Output }                from '../../model/output/output';
import { OutputCollection }      from '../../model/output/output.collection';
import { IModel }                from '../../interface/model/i.model';
import { Core }                  from '../../util/core/core';
import { FunctionsType } from '../../type/functions/functions.type';

//@dynamic
export class TransformFactory
{
    static transformCollection(items:any, collection:any):any
    {
        return new Promise<any>((resolve:any) =>
        {
            const outputCollection = [];

            for(const key in collection)
            {
                const data = collection[key];

                outputCollection.push(TransformFactory.transformData(items, data));
            }

            Promise.all(outputCollection).then(values =>
            {
                resolve(new OutputCollection(values));
            });
        })
    }

    static transformData(items:any, data:any):any
    {
        return new Promise<any>((resolve:any) =>
        {
            const outputs = [];

            for(const key in items)
            {
                const formItem = items[key];

                outputs.push(TransformFactory.transform(formItem, data));
            }

            Promise.all(outputs).then(values =>
            {
                const outputCollection = new OutputCollection(values)
                outputCollection._data = data;

                resolve(outputCollection);
            });
        })
    }

    static transform(formItem:any, data:any):any
    {
        let transformClass : any;

        /* DEFINED TRANFORM */
        if(formItem.setting && formItem.setting.transformClass)
        {
			transformClass = FieldType.output(formItem.setting.transformClass);

            if(!transformClass)
            {
                console.error('transform not found', formItem.setting.transformClass);
            }
        }        
        else if(formItem.map)// NÃO FAZ PARSE DE TRANSFORM
        {
			return new Promise<any>((resolve:any) =>
            {
                resolve(new Output({
                    formItem : formItem,
                    map      : formItem.map,
                    label    : formItem.label,
                    value    : data[formItem.name],
                    data     : data,
                    count    : 1,
                    total    : 1,
                }));
            });
        }
        else if(formItem.setting && formItem.setting.transformFunction)
        {
            return new Promise<any>((resolve:any) =>
            {
                data.getPathValue(formItem.path).then(async (value:any) =>
                {
                    const format = await FunctionsType.get().getFunction(formItem.setting.transformFunction)(data, value);

                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        format   : format,
                        data     : data,
                        count    : 1,
                        total    : 1,
                    }));
                });
            });
        }
        else
        {
            transformClass = FieldType.transform(formItem.field.type.value);

            /* VERIFICAR SE O COMPONENT EXISTE */
            if(transformClass == undefined)
            {
                transformClass = FieldType.transform('TEXT');
            }
        }

        const promise : Promise<any> = transformClass.transform(formItem, data);

        if(formItem.field.onTransform)
        {
            formItem.field.onTransform(promise);
        }

        return promise;
    }
}
