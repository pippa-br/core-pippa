/* PIPPA */
import { Core }      from '../../../core/util/core/core';
import { FieldType } from '../../type/field/field.type';
import { FormItem }  from '../../model/form.item/form.item';

//@dynamic
export class OutputFactory
{
    static async createComponent(formItem:FormItem, container:any, args?:any)
    {
        let componentClass : any;

        /* DEFINED OUTPUT VIEW */
        if(formItem.setting && formItem.setting.outputClass)
        {
			componentClass = FieldType.output(formItem.setting.outputClass);

            if(!componentClass)
            {
                console.error('outputOutput not found', formItem.setting.outputClass);
            }
        }
        else
        {
            componentClass = FieldType.output(formItem.field.type.value);

            /* VERIFICAR SE O COMPONENT EXISTE */
            if(componentClass == undefined)
            {
                componentClass = FieldType.output('Text');
            }
        }

        let component = await Core.get().componentFactory.create(container, componentClass);

        container.insert(component.hostView);

        if(args)
        {
            for(const key in args)
            {
                component.instance[key] = args[key];
            }
        }

        return component;
    }
}
