/* FIELD */
import { Core }             from '../../../core/util/core/core';
import { PartialType }      from '../../type/partial/partial.type';

//@dynamic
export class PartialFactory
{
    static async createComponent(type:any, container:any, args?:any)
    {
        const componentClass = PartialType.partial(type.value);

        if(componentClass)
        {
            const component = await Core.get().componentFactory.create(container, componentClass);

            container.insert(component.hostView);

            if(args) 
            {
                for(const key in args)
                {
                    if(args[key])
                    {
                        component.instance[key] = args[key];
                    }
                }
            }

            /* CHAMADA PARA MANUAL */
            //component.instance.markForCheck();

            return component;
        }
        else
        {
            console.error('no partial', type.value);
        }
    }
}
