/* PIPPA */
import { Result } from '../../util/result/result';
import { Params } from '../../util/params/params';

export class ResultFactory
{
    create : (params?:Params) => Result = function(params?:Params)
    {
        return new Result(params);
    }
}
