/* FIELD */
import { FieldType } from '../../type/field/field.type';
import { FormItem }  from '../../model/form.item/form.item';
import { Core }      from '../../util/core/core';

//@dynamic
export class InputFactory
{
    static async createComponent(formItem:FormItem, container:any, formGroup:any, args={})
    {
        const componentPath = FieldType.input(formItem.field.type.value);

        /* VERIFICAR SE O COMPONENT EXISTE */
        if(componentPath == undefined)
        {
            console.log('not found componentClass',formItem.field);

            return formItem;
        }

        const component = await Core.get().componentFactory.create(container, componentPath);

        /* COMPONENT E DIFERENTE DE INSTACE, COMPONENT PARA USAR O detectChanges */
        formItem.field.component = component;

        /* SET ARGS */
        for(const key in args)
        {
            component.instance[key] = args[key];
        }

        await component.instance.createControls(formItem, formGroup);

        if(component)
        {
            component.instance.component = component;
            container.insert(component.hostView);
        }
        else
        {
            console.error('no input', formItem.field.type.value);
        }

        return component;
    }

    core()
    {
        return Core.get();
    }
}
