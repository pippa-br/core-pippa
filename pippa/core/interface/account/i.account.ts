export interface IAccount
{
    id                 : string;
    projectID          : string;
    accid              : string;
    name               : string;
    description        : string;
    _count             : number;
    populate(data:any) : any;
    getMenuId()        : string;
}
