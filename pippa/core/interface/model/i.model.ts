export interface IModel
{
    id           : string;
    accid        : string;
    appid        : string;
    reference    : any;
    owner        : any;
    form         : any;
    getPathValue : any;
    active       : boolean; /* UTILIZADO PARA FLAG DE VISUALIZAÇÃO OU ACESSO */
    on()         : any;
    isOwner()    : boolean;
    set(data:any)   : any;
    getAttachments(name:string) : any;
}
