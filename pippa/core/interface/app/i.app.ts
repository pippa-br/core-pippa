export interface IApp
{
    id             : string;
    name           : string;
    projectID      : string;
    accid          : string;
    appid          : string;
    active         : boolean;
    plugin         : any;
    basePath       : string;
    default        : boolean;
    copy()
}
