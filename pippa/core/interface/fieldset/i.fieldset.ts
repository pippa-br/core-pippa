export interface IFieldset
{
    validate():boolean;
    form : any;
    data : any;
}
