/* PIPPA */
import { BaseList } from '../base.list';
import { User } from './user';

export class UserCollection extends BaseList<User>
{
    getMaps()
    {
        return {
            model : { klass : User },
            list  : { klass : UserCollection },
        };
    }
}
