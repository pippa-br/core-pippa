/* PIPPA */
import { BaseModel } from '../../../core/model/base.model';
import { Form }      from '../../../core/model/form/form';
import { Payment }   from '../../../core/model/payment/payment';
import { Document }  from '../../../core/model/document/document';

export class User extends BaseModel
{
    /*public email     : string;
    public group     : any;
    public groups    : Array<any>;
    public password  : string;*/

    getMaps()
    {
        return {
            model   : { klass : User },
            form    : { klass : Form },
			owner   : { klass : User },
			user    : { klass : User },
            payment : { klass : Payment },
        };
    }

    getGroups()
    {
		let keyGroups = 'groups';
		let keyGroup  = 'group';
		let value 	  = this['appid'] ? this['appid'] : 'guest';

		/* USUARIO EM CONTA DIFERENTE */
		if(this.core().accountProfile)
		{
			keyGroups = 'accountGroups';
			keyGroup  = 'accountGroup';
			//value     = 'guest';
		}

		if(typeof this[keyGroups] == 'string') /* CASOS DE EXPORTAÇÃO */
		{
			this[keyGroups] = [];
		}
        else if(this[keyGroups] && this[keyGroups].length > 0)
        {
			/* NESSE CASO OS GRUPOS JA ESTA CERTO! NAO FAZ NADA */
			value = null;			
		}		
        else if(this[keyGroup])
        {				
            if(typeof this[keyGroup] == 'string')
            {
				value = this[keyGroup];
			}	
			else
			{
				value = this[keyGroup].value;
			}	
			
			//delete this[keyGroup];
        }
		
		if(this.core().optionPermission && value)
		{
			for(const key in this.core().optionPermission.items)
			{
				const item = this.core().optionPermission.items[key];					

				if(item.value == value)
				{
					this[keyGroups] = [item];
					//delete this[keyGroup];
					break;						
				}
			}
		}

        return this[keyGroups];
    }

    getGroup()
    {
        const groups = this.getGroups();
        return groups[0]
	}

	hasGroups(values:any)
	{
		let has : boolean = false;

		const _groups = this.getGroups();

		for(const key in values)
		{
			for(const key2 in _groups)
			{
				let value : string;

				if(values[key].value)
				{
					value = values[key].value;
				}
				else
				{
					value = values[key];
				}

				if(value === _groups[key2].value || value == 'all')
				{
					has = true;
					break;
				}
			}			
		}

		return has;
	}
	
	hasGroup(value:any)
	{
		let has : boolean = false;

		const _groups = this.getGroups();

		for(const key2 in _groups)
		{
			if(value === _groups[key2].value || value == 'all')
			{
				has = true;
				break;
			}
		}

		return has;
	}

    isGuest()
    {
        const groups = this.getGroups();

        for(const key in groups)
        {
            return (groups[key] && groups[key].value.toLowerCase() == 'guest');
        }
    }

    isAdmin()
    {
        const groups = this.getGroups();

        for(const key in groups)
        {
            return (groups[key] && groups[key].value.toLowerCase() == 'admin') || this.isMaster();
        }
    }

    isMaster()
    {
        const groups = this.getGroups();

        for(const key in groups)
        {
            return (groups[key] && groups[key].value.toLowerCase() == 'master');
        }
    }
}
