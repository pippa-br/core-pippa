import { BaseList }    from '../base.list';
import { Message } from './message';

export class MessageCollection extends BaseList<Message>
{
    getMaps():any
    {
        return {
			model : { klass : Message },
			list  : { klass : MessageCollection },
        };
    }
}
