/* PIPPA */
import { BaseModel } from '../../model/base.model';
import { User }      from '../../model/user/user';
import { File }      from '../../model/file/file';
import { Document }  from '../../model/document/document';

export class Message extends BaseModel
{
    static TEXT     : string = 'text';
    static IMAGE    : string = 'image';
    static AUDIO    : string = 'audio';
    static DOCUMENT : string = 'document';

    getMaps()
    {
        return {
			model    : { klass : Message },
            file     : { klass : File },
            sender   : { klass : User },
            receiver : { klass : User },
			target   : { klass : Document },
        };
    }

    initialize()
    {
        this['text'] = "";
	}
	
	on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* SENDER */
                    if(this['sender'])
                    {                        
                        promises.push(this.getProperty('sender', fetch));
                    }

                    /* RECEIVER */
                    if(this['receiver'])
                    {
                        promises.push(this.getProperty('receiver', fetch));
                    }

					/* TARGET */
                    if(this['target'])
                    {
                        promises.push(this.getProperty('target', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    });
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }    

    isOwner()
    {
        if(this['sender'] && this.core().user)
        {
            /* NÃO ABRIR OWNER, POIS PRECISA DO RETORNO IMEDIATO */
            if(this.core().user.reference.id == this['sender'].reference.id || this.core().user.isMaster())
            {
                return true;
            }
        }

        return false;
    }
}
