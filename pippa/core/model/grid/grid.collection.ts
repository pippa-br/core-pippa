/* APP */
import { BaseList } from '../base.list';
import { Grid } from './grid';

export class GridCollection extends BaseList<Grid>
{
    getMaps()
    {
        return {
            model : { klass : Grid },
            list  : { klass : GridCollection },
        };
    }
}
