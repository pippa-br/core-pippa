/* PIPPA */
import { BaseModel }          from "../base.model";
import { App }                from "../app/app";
import { Form }               from "../form/form";
import { FormItemCollection } from "../form.item/form.item.collection"
import { PaginationType }     from "../../type/pagination/pagination.type"

export class Grid extends BaseModel
{
    static TABLE_TYPE     = { value : "table",     label : "Tabela" };
    static TILE_TYPE      = { value : "tile",      label : "Tile" };
    static CARD_TYPE      = { value : "card",      label : "Card" };
    static SLIDE_TYPE     = { value : "slide",     label : "Slide" };
    static TAB_TYPE       = { value : "tab",       label : "Tab" };
    static MAP_TYPE       = { value : "map",       label : "Map" };
    static CALENDAR_TYPE  = { value : "calendar",  label : "Calendário" };
     
    /*public name             : string;
    public description      : string;
    public items            : FormItemCollection;
    public component        : any;
    public orderBy          : any;
    public groupBy          : any;
    public asc              : any;
    public buttons          : any;    
    public platform         : string;
    public cols             : number;    
    public type             : any;
    public filterApp        : App;
    public toRouter         : string;
    public toUrl            : string;
    public plugin           : any;
    public perPage          : number;
    public _level           : number;
    public filterField      : string;    
    public paginationType   : any;*/

    /* ONLY VIEWER */
    public trClass            : any;
    public hasAddButton       : boolean;
    public hasAddLevelButton  : boolean;
    public hasViewerButton    : boolean;
    public hasExpandButton    : boolean;
    public hasSetButton       : boolean;
    public hasCloneButton     : boolean;    
    public hasDelButton       : boolean;
    public hasArchiveButton   : boolean;
    public hasUnarchiveButton : boolean;
    public hasEmailButton     : boolean;
    public hasCancelButton    : boolean;
    public hasOrderButton     : boolean;
    public fixedHeader        : boolean;
    public viewerClick        : false;
    public disabledButtons    : boolean;
    public hasHeader  		  : boolean;
    public showNoItems        : boolean;

    constructor(args?:any, fetch = false)
    {
        super(args, fetch);

        Object.defineProperty(this, "trClass",            { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasAddButton",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasAddLevelButton",  { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasViewerButton",    { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasExpandButton",    { writable : true, enumerable : false, configurable : true });  
        //Object.defineProperty(this, "hasSetButton",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasCloneButton",     { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "hasDelButton",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasArchiveButton",   { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasUnarchiveButton", { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasEmailButton", 	  { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasCancelButton",    { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasOrderButton",     { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "fixedHeader",        { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "viewerClick",        { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "disabledButtons",    { writable : true, enumerable : false, configurable : true });
        //Object.defineProperty(this, "hasHeader",  	  { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "showNoItems",  	  { writable : true, enumerable : false, configurable : true });
    }

    getMaps()
    {
        return {
            model     : { klass : Grid },
            form      : { klass : Form },
            filterApp : { klass : App },
            innerApp  : { klass : App },
            items     : { klass : FormItemCollection },            
        };
    }

    collectionName()
    {
        return "grids";
    }

    initialize()
    {
        this.hasAddButton       = false;
        this.hasViewerButton    = false;
        this.hasExpandButton    = false;
        this.hasSetButton       = undefined;
        this.hasCloneButton     = false;
        this.hasDelButton       = false;
        this.hasArchiveButton   = false;
        this.hasUnarchiveButton = false;
        this.hasEmailButton     = false;
        this.hasOrderButton     = false;
        this.hasCancelButton    = false;
        this.disabledButtons    = false; 
        this.fixedHeader        = true;
        this.hasHeader          = true;
        this.showNoItems        = true;

        this.trClass = () =>
        {
            return;
        };

        this["cols"]           = 4;
        this["perPage"]        = 24;
        this["type"]           = Grid.TABLE_TYPE;
        this["paginationType"] = PaginationType.NUMBER;
        this["items"]          = new FormItemCollection();
        //this['platform']          = Params.DESKTOP_PLATFORM;        
    }

    isTable()
    {
        return this["type"] && this["type"].value == Grid.TABLE_TYPE.value;
    }

    isTile()
    {
        return this["type"] && this["type"].value == Grid.TILE_TYPE.value;
    }

    on(fetch = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() => 
            {
                if (fetch) 
                { 
                    const promises = []

                    /* ITEMS */
                    if (this["items"] && this["items"].length > 0)
                    {
                        promises.push(this.getProperty("items", fetch));
                    }
					
                    if (this["innerApp"])
                    {
                        promises.push(this.getProperty("innerApp", fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {                
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
