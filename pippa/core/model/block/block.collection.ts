/* PIPPA */
import { BaseList }            from '../base.list';
import { Block }               from './block';
import { StepBlockCollection } from '../step.block/step.block.collection';
import { StepBlock }           from '../step.block/step.block';

export class BlockCollection extends BaseList<Block>
{
    getMaps()
    {
        return {
            model : { klass : Block },
        };
    }

    getSteps()
    {
        const steps = new StepBlockCollection();
        const ids   = [];

        for (const key in this)
        {
            const item : any = this[key];

            if(item.step && ids.indexOf(item.step.id) == -1)
            {
                steps.set(item.step);
                ids.push(item.step.id);
            }
        }

        if(steps.length == 0)
        {
            steps.push(new StepBlock({
                id : 0,
            }))
        }
        else
        {
            steps.doSort();

            let i = 0;

            for (const key in steps)
            {
                steps[key].position = i;
                i++;
            }
        }

        return steps;
    }
}
