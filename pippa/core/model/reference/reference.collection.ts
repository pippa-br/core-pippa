import { BaseList } from '../base.list';
import { Form } from '../form/form';

export class ReferenceCollection extends BaseList<Form>
{
    getMaps()
    {
        return {
			model : { klass : Form },
			list  : { klass : ReferenceCollection },
        };
    }
}
