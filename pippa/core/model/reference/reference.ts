import { BaseModel } from '../base.model';
import { Form }      from '../form/form';
import { User }      from '../user/user';

export class Reference extends BaseModel
{
    getMaps()
    {
        return {
            model : { klass : Reference },
            form  : { klass : Form },
            owner : { klass : User },
        };
    }
}
