/* PIPPA */
import { BaseModel }  from '../../model/base.model';
import { Form }       from '../../model/form/form';
import { PluginType } from '../../../core/type/plugin/plugin.type';

export class App extends BaseModel
{
    /* ONLY VIEWER */
    public route : string;
 
    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        Object.defineProperty(this, "route", { writable: true, enumerable: false, configurable: true });              
    }

    getMaps()
    {
        return {
            model  : { klass : App },
            form   : { klass : Form },
        };
    }

    initialize()
    {
	}	

    getPlugin()
    {
        return PluginType.get().getInstance(this['plugin'].value);
    }

    getClassPlugin()
    {
        return PluginType.get().getClass(this['plugin'].value);
	}
	
	appidName()
	{
		return 'apps';
	}

	collectionName()
	{
		return 'documents';	
	}

    nextSequence()
    {
        return new Promise((resolve:any) =>
        {
            return this.core().fireStore.firestore.runTransaction((transaction:any) =>
            {
                return transaction.get(this.reference).then((sfDoc:any) =>
                {
                    if (!sfDoc.exists)
                    {
                        throw "Document does not exist!";
					}

					let key : string;

					if(this.core().isAccountMaster)
					{
						key = '_sequence';
					}
					else
					{
						key = '_sequence_' + this.core().account.code;
					}

                    const _sequence = (sfDoc.data()[key] ? sfDoc.data()[key] + 1 : 1);
					
					const data = {};
					data[key]  = _sequence;
					
					transaction.update(this.reference, data);
					
                    return _sequence;
                });
            })
            .then(function(event:any)
            {
                resolve(event);
            })
            .catch(function(error:any)
            {
                console.error("Transaction failed: ", error);
            });
        });
    }
}
