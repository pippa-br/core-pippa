/* APP */
import { BaseList } from '../base.list';
import { App } from './app';

export class AppCollection extends BaseList<App>
{
    getMaps()
    {
        return {
            model : { klass : App },
            list  : { klass : AppCollection },
        };
    }

    get indexPath():string
    {
        return this.core().account.projectID + '-' + this.core().account.code + '-apps';
    }
}
