import { BaseModel } from '../../model/base.model';
import { App }       from '../../model/app/app';
import { Form }      from '../../model/form/form';
import { FieldType } from '../../type/field/field.type';

export class Option extends BaseModel
{
    /*public id         : any;
    public accid      : string;
    public appid      : string;
    public name       : string;
    public code       : string;
    public label      : string;
    public value      : string;
    public items      : any;
    public rows       : any;
    public documents  : any;
    public columns    : any;
    public app        : any;    
    public categories : any;
    ;*/

    /* ONLY VIEWER */
    //public record : boolean

    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        //Object.defineProperty(this, "record", { writable: true, enumerable: false, configurable: true });                                         
    }

    getMaps()
    {
        return {
            model : { klass : Option },
            app   : { klass : App },            
            form  : { klass : Form },
        };
    }

    initialize()
    {
        //this.record   = false;

        //this['name']  = '';
        //this['code']  = '';
        this['items'] = [];
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = []

                    /* APP */
                    if(this['app'])
                    {
                        promises.push(this.getProperty('app', fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
	}   
	
	getByValue(value:string)
	{
		for(const key in this['items'])
		{
			if(this['items'][key].value == value)
			{
				return this['items'][key];
			}
		}
	}
}
