/* PIPPA */
import { BaseList }   from '../base.list';
import { Option } from './option';

export class OptionCollection extends BaseList<Option>
{
    getMaps()
    {
        return {
            model : { klass : Option },
            list  : { klass : OptionCollection },
        };
    }
}
