/* APP */
import { BaseList } from '../base.list';
import { Acl } from './acl';

export class AclCollection extends BaseList<Acl>
{
    getMaps()
    {
        return {
            model : { klass : Acl },
            list  : { klass : AclCollection },
        };
    }
}
