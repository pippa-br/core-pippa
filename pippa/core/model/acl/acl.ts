/* PIPPA */
import { BaseModel } from '../base.model';
import { Form } 	 from '../form/form';
import { FieldType } from '../../type/field/field.type';

export class Acl extends BaseModel
{
    //public _group : any;

    getMaps()
    {
        return {
            model  : { klass : Acl },
			form   : { klass : Form },
        };
    }

    initialize()
    {
        //this._group = null;
	}

	collectionName()
	{
		return 'acls';
	}

    /* VERIFICA SE EXISTE E SEMPRE RETORNA UM DADO */
    getKey(name:string, value?:any)
    {
        //name = this.core().currentApp.code + '_' + name;

        if(this.exists && this[name] == undefined)
        {
            /* PEGAR APENAS A INFORMAÇÃO */
            if(value == undefined)
            {
                return;
            }

            this[name] = value;

            const data : any = {};
            data[name] = value;

            //this.save(data);
        }

        return this[name];
    }

    /* VERIFICA SE EXISTE E SEMPRE RETORNA TRUE OU FALSE */
    hasKey(name:string, value?:any)
    {
        if(value === undefined)
        {
            value = (this.core().user && this.core().user.isMaster() ? true : false);
        }

        return this.getKey(name, value); /* RETORNA FALSE, NULL OR OBJECT PARA IFS */
    }

    merge(data:any)
    {
		// POR CAUSA DO ACL PLUGIN QUE CRIA ACL VAZIO
		this.exists = this.exists || data.exists;

        for(const key in data)
        {			
            if(typeof data[key] == 'boolean')
            {
                this[key] = this[key] || data[key];
            }
			else if(data[key] === undefined || data[key] == null)
			{
				this[key] = false;
			}
			else
			{
				// SEMPRE COLOCAR TODAS AS CHAVES POR CAUSA DO GETKEY QUE CRIA QUANDO NÃO TEM
				this[key] = data[key];
			}
        }
    }
}
