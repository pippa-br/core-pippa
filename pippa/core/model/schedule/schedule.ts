import later from '@breejs/later';

export class Schedule
{
	public data     : any;
	public schedule : any;

	constructor(data:any)
	{ 
		this.data = data;

		// set later to use local time
		later.date.localTime();

		/*const schedule = {
			schedules :
			  [
				{h:[19], m: [41,42,43]},
				{h:[19], m: [44,45]}
			  ],
		};*/

		this.schedule = {
			schedules : [
				{
				}
			]
		};

		if(data instanceof Object)
		{
			/* ano */
			if(data.Y)
			{
				this.schedule.schedules[0].Y = [];

				for(const key in data.Y)
				{
					this.schedule.schedules[0].Y.push(data.Y[key].value);
				}
			}

			/* mês */
			if(data.M)
			{
				this.schedule.schedules[0].M = [];

				for(const key in data.M)
				{
					this.schedule.schedules[0].M.push(data.M[key].value);
				}
			}

			/* dia da semana */
			if(data.dw)
			{
				this.schedule.schedules[0].dw = [];

				for(const key in data.dw)
				{
					this.schedule.schedules[0].dw.push(data.dw[key].value);
				}
			}

			/* dias */
			if(data.D)
			{
				this.schedule.schedules[0].D = [];

				for(const key in data.D)
				{
					this.schedule.schedules[0].D.push(data.D[key].value);
				}
			}

			/* horas */
			if(data.h)
			{
				this.schedule.schedules[0].h = [];

				for(const key in data.h)
				{
					this.schedule.schedules[0].h.push(data.h[key].value);
				}
			}

			/* minutos */
			if(data.m && data.m.length > 0)
			{
				this.schedule.schedules[0].m = [];

				for(const key in data.m)
				{
					this.schedule.schedules[0].m.push(data.m[key].value);
				}
			}

			//console.log(data, this.schedule.schedules);
		}
	}

	get isValid()
	{		
		return this.data !== undefined && later.schedule(this.schedule).isValid(new Date());
	}
}