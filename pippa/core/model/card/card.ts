export class Card extends Object
{
    id         : number;
    accid      : number;
    appid      : number;
    postdate   : string;
    cardname   : string;
    owner      : string;
    cardnumber : string;
    expirydate : string;
    cpf        : string;
    cvv        : string;
    user       : any;
}
