/* PIPPA */
import { BaseModel } from '../../../core/model/base.model';
import { Form }      from '../../../core/model/form/form';
import { Payment }   from '../../../core/model/payment/payment';
import { User }      from '../user/user';

export class Invoice extends BaseModel
{
    //public payment : Payment;

    getMaps()
    {
        return {
            model   : { klass : Invoice },
            form    : { klass : Form },
            onwer   : { klass : User },
            payment : { klass : Payment },
        };
    }
}
