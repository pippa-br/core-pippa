/* PIPPA */
import { BaseList }    from '../base.list';
import { Invoice } from './invoice';

export class InvoiceCollection extends BaseList<Invoice>
{
    getMaps()
    {
        return {
            model : { klass : Invoice },
        };
    }
}
