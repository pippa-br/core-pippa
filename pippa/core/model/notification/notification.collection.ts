/* PIPPA */
import { BaseList } 	from '../base.list';
import { Notification } from './notification';

export class NotificationCollection extends BaseList<Notification>
{
    getMaps()
    {
        return {
            model : { klass : Notification },
            list  : { klass : NotificationCollection },
        };
    }    
}
