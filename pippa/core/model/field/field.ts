import { Input, Directive } from '@angular/core';
import { FormGroup } from '@angular/forms';

/* FIELD */
import { FieldType } 		from '../../type/field/field.type';
import { BaseModel } 		from '../../model/base.model';
import { Option }    		from '../../model/option/option';
import { OptionCollection } from '../../model/option/option.collection';
import { Grid }      		from '../../model/grid/grid';
import { IconType }  		from '../../type/icon/icon.type';

@Directive()
export class Field extends BaseModel
{
    /* ONLY VIEWER */
    public subFormGroup : FormGroup; /* UTILIZADO EM SUBFORM PARA REFERENCIA O FORMGROUP DO FORM */
    public formGroup    : FormGroup; /* DEVE SER CRIADO EXTERNAMENTE */
    public disable      : boolean; /* VISUALIZADO MAS NÃO ACESSIVEL */
	public display      : boolean; /* UTILIZADO PARA REGRAS NO FRONT */
	public hidden       : boolean; // USADO EM PERMISSOES DE ACESSO
	public uppercase    : boolean;
    public hasAdd       : boolean;
    public first        : boolean;
    public component    : any; /* COMPONENT E DIFERENTE DE INSTACE, COMPONENT PARA USAR O detectChanges */
    public _instance    : any;
    public onClick      : any;
    public onChange     : any;
    public onReset      : any;
    public onSelect     : any;
    public onComplete   : any;
    public onSubmit     : any;
    public onInit       : any;
    public onAdd        : any;
    public onSet        : any;
    public onEdit       : any;
    public onDel        : any;
    public onCancel     : any;
    public onTransform  : any;

    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        Object.defineProperty(this, "subFormGroup", { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "formGroup",    { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "disable",      { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "display",      { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "hasAdd",       { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "first",        { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "component",    { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "_instance",    { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onClick",      { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onChange",     { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onReset",      { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onSelect",     { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onComplete",   { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onSubmit",     { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onInit",       { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onAdd",        { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onSet",        { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onEdit",       { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onDel",        { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onCancel",     { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "onTransform",  { writable: true, enumerable: false, configurable: true });         
    }

    getMaps()
    {
        return {
            model   : { klass : Field },
			option  : { klass : Option },
			options : { klass : OptionCollection },
            grid    : { klass : Grid, },            
        };
    }    

    initialize()
    {    
        this.hasAdd = false;
        this.first  = false;

        this['required']    = true;
        this['editable']    = true;
        this['record']      = true;
        this['initial']     = '';
        this['name']        = '';
        this['label']       = '';
        this['placeholder'] = '';
        this['description'] = '';
        this['information'] = '';
        this['viewColummn'] = true;
        this['single']      = false;
        this['fixed']       = false;
        this['disable']     = false;
        this['readonly']    = false;
        this['display']     = true;
        this['hasLabel']    = true;        
        this['blocked']     = false;
        this['_groups']     = null;
        this['onlyAdd']     = false;
		this['onlySet']     = false;
		this['hidden']      = false;
		this['uppercase']   = false;		
        this['type']        = FieldType.type('Text');
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = []

                    /* OPTION */
                    if(this['option'])
                    {
                        promises.push(this.getProperty('option', fetch));
					}
					
					/* OPTIONS */
                    if(this['options'])
                    {
                        promises.push(this.getProperty('options', fetch));
                    }

                    /* GRID */
                    if(this['grid'])
                    {
                        promises.push(this.getProperty('grid', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }

    isContainer()
    {
        return this['type'] && (this['type'].value == FieldType.type('Fieldset').value ||
               this['type'].value == FieldType.type('SubForm').value ||
               this['type'].value == FieldType.type('DataForm').value ||
			   this['type'].value == FieldType.type('Matrix').value ||
               this['type'].value == FieldType.type('ReferenceArrange').value);
    }

    isFields()
    {
        return this['type'].value == FieldType.type('ReferenceFields').value ||
               this['type'].value == FieldType.type('Fields').value;
    }

    isHidden()
    {
        return this['type'].value == FieldType.type('Hidden').value || this.hidden;
    }

    getIcon()
    {
        const icons = {};

        icons['Email']                = IconType.MAIL;
        icons['Phone']                = IconType.CALL;
        icons['CPF']                  = IconType.DOCUMENT;
        icons['Url']                  = IconType.LINK;
        icons['Zipcode']              = IconType.PIN;
        icons['Password']             = IconType.EYE_OFF;
        icons['ConfirmPassword']      = IconType.EYE_OFF;
        icons['GeneratePassword']     = IconType.SYNC;
		icons['ReferenceSelect']      = IconType.ADD;
		icons['ReferenceText']        = IconType.ADD;
        icons['ReferenceMultiSelect'] = IconType.ADD;

        if(this['icon'])
        {
            return this['icon'];
        }
        else if(icons[this['type'].value])
        {
            return icons[this['type'].value];
        }
        else
        {
            return null;
        }
    }

    getBindValue()
    {
        const bindValues            = {};
		bindValues['Select']        = "value";
        bindValues['MultiSelect']   = "value";
		bindValues['Radio']         = "value";
		bindValues['Matrix']        = "value";
		bindValues['PaymentStatus'] = "value";
        bindValues['ColumnGrid']    = "field.type.value";        

        /* BIND VALUE */
        if(this['setting'] && this['setting'].bindValue)
        {
            return this['setting'].bindValue;
        }
        else if(bindValues[this['type'].value])
        {
            return bindValues[this['type'].value];
        }
        else
        {
            return "id";
        }
    }

    getBindLabel()
    {
        const bindLabels                     = [];
        bindLabels['ReferenceMultiCheckbox'] = 'name';
        bindLabels['ReferenceApps']          = 'name';
		bindLabels['ReferenceForm']	         = 'name';
        bindLabels['ColumnGrid']             = '_label';
        bindLabels['ReferenceSelect']        = '_label';
        bindLabels['ReferenceOptions']       = '_label';
		bindLabels['ReferenceMultiSelect']   = '_label';
        bindLabels['ProductMultiSelect']     = '_label';
		bindLabels['Menu']   				 = '_label';

        /* BIND VALUE */
        if(this['setting'] && this['setting'].bindLabel)
        {
            return this['setting'].bindLabel;
        }
        else if(bindLabels[this['type'].value])
        {
            return bindLabels[this['type'].value];
        }
        else
        {
            return "label"
        }
    }

    @Input()
    set instance(value:any)
    {
        this._instance = value;
    }

    get instance():any
    {
        return this._instance;
    }

    del()
    {
        if(this.reference)
        {
            this.reference.delete();
        }
    }

    isDisplay()
    {
        if( this['type'].value == FieldType.type('Password').value ||
            this['type'].value == FieldType.type('ConfirmPassword').value)
        {
            return false;
        }

        return true;
    }

    hasAttachments()
    {
        return this['typeAttachments'] && this['typeAttachments'].length > 0
    }

    /*hasParent()
    {
        if(this.parent)
        {
            return true;
        }

        return false;
    }*/

    /*rootParent()
    {
        return this._rootParent(this);
    }

    _rootParent(field)
    {
        if(field.parent)
        {
            return this._rootParent(field.parent);
        }
        else
        {
            return field;
        }
    }*/

    /*getDeep():number
    {
        return this.getPath().length - 1;
    }

    getPath():Array<string>
    {
        let paths = [];

        this._getPath(paths, this);

        paths.reverse();

        return paths;
    }

    _getPath(paths, field)
    {
        let items = field.name.split('.');
        items.reverse();

        for(let key in items)
        {
            paths.push(items[key]);
        }

        if(field.hasParent())
        {
            this._getPath(paths, field.parent);
        }
    }*/
}
