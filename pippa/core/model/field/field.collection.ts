/* PIPPA */
import { BaseList } from '../base.list';
import { Field }    from './field';

export class FieldCollection extends BaseList<Field>
{
    getMaps()
    {
        return {
            model : { klass : Field },
            list  : { klass : FieldCollection }
        };
    }

    getFlat():any
    {
        const items = [];
        this._getFlat(items, this);

        const collection = this.copy();
        collection.setSort(null);
        collection.setItems(items);

        return collection;
    }

    _getFlat(items:any, fields:any):void
    {
        for(const key in fields)
        {
            items.push(fields[key]);

            if(fields[key].fields)
            {
                this._getFlat(items, fields[key].fields);
            }
        }
    }

    getById(id:any)
    {
        for(const key in this)
        {
            const item : any = this[key];

            if(item.id == id)
            {
                return item;
            }
        }
    }

    /*delAll():void
    {
        this.delAllComponent();

        /* NÃO USAR LOOP POIS HAVERA VARIOS UPDATE NO VIEW */
        /*this.splice(0, this.length);
    }*/

    /*_set(item):void
    {
        const data     = this.parseItem(item);
        const position = this.findPosition(data);

        if(position > -1)
        {
            const field = this[position];
            field.delInstance();

            this[position] = data;
        }
        else
        {
            /* SE NÃO HOUVER ADICIONA */
            /*this.push(data);
        }
    }

    _del(item):boolean
    {
        const position = this.findPosition(item);

        if(position > -1)
        {
            const field = this[position];
            field.delInstance();
        }

        return super._del(item);
    }

    delAllComponent()
    {
        for(const key in this)
        {
            const field = this[key];
            field.delInstance();
        }
    }*/
}
