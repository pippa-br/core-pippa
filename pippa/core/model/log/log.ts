/* PIPPA */
import { BaseModel } from '../../model/base.model';
import { LogCollection } from './log.collection';

export class Log extends BaseModel
{
    /*public dataset  : any;
    public type     : any;
    public document : any;*/

    getMaps()
    {
        return {
			model : { klass : Log },
        };
    }

    initialize()
    {
        //this.dataset  = {};
        //this.document = null;
	}

	collectionName()
	{
		return 'logs';
	}
}
