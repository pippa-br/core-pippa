/* APP */
import { BaseList } from '../base.list';
import { Log } from './log';

export class LogCollection extends BaseList<Log>
{
    getMaps()
    {
        return {
			model : { klass : Log },
			list  : { klass : LogCollection },
        };
    }
}
