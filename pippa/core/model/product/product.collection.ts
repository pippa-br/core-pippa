import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList }    from '../base.list';
import { Product } from './product';

export class ProductCollection extends BaseList<Product>
{
    getMaps()
    {
        return {
            model : { klass : Product },
        };
    }
}
