/* PIPPA */
import { BaseList }  from "../base.list";
import { Where }     from "./where";
import { FieldType } from "../../type/field/field.type";

export class WhereCollection extends BaseList<Where>
{
    getMaps()
    {
        return {
            model : { klass : Where },
            list  : { klass : WhereCollection }
        };
    }

    async parseFilter()
    {
        await this.on();

        const _where = [];

        this.sort(( a:any, b:any ) => 
        {
            if (a.order < b.order)
            {
				  return -1;
            }
            else if (a.order > b.order)
            {
				  return 1;
            }

            return 0;
        });

        for (const key in this)
        {
            const item : any = await this[key].parseFilter();

            if (item.enabled)
            {								
                _where.push({
                    name     : item["name"],
                    field    : item["field"],
                    operator : item["operator"],
                    order    : item["order"],
                    value    : item["value"],
                    type     : item["type"],
                }); 
            }                            
        }

        return _where;
    }

    async getLabels()
    {
        await this.on();

        const _labes = [];

        for (const item of this)
        {
            if (item.enabled && !item.isEmpty())
            {								
                _labes.push(await item.getLabel());
            }                            
        }

        return _labes;
    }

    add(item:any):void
    {
        if (!item.type)
        {
            throw "Type no found";
        }

        super.add(item);        
    }

    /* VERIFICA SE TEM O FILTRO DE VALOR */
    _set(item:any, index?:number):void
    {		
        /* SEMPRE O OBJETO QUE VAI SER SETADO TEM QUE TEM O ENABLED */
        if (item.enabled === undefined)
        {
            item.enabled = true;
        }		

        const similar = this.getSimilar(item);

        if (similar)
        {
            similar["enabled"] = false;

            console.error("filtro removido por ser similiar", item); 
        }
		
        super._set(item, index);
    }
	
    getSimilar(item:any)
    {
        for (const key in this)
        {
            const item2 : any = this[key];

            if (item.id == item2.id || (item.name && item2.name && item.name == item2.name)) /* O SIMILAR DE UM ITEM É ELE MESMO */
            {
                return this[key];
            }
            else if (item.field == item2.field && item.operator == item2.operator && item.operator != "combine") // considerar operador EX: postdate >= e postdate <=
            {
                return this[key];
            }
        }
    }

    enabledAll()
    {
        for (const key in this)
        {
            const item      = this[key];
            item["enabled"] = true;
        }
    }

    /*getSearchOperator(item:any)
	{
		const operators = {
			'==' : ':',
			'!=' : ':'
		}

		if(item.operator == '')
		{

		}
	}
	
	async parseSearch()
	{
		let filters : string = '';

		for(let key in this)
		{
			const item : any = await this[key].parseSearch();

			if(item.enabled)
			{
				if(item.type.value == FieldType.type('ReferenceSelect').value)
				{
					filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + '.objectID:"' + (item.value ? item.value.id : null) + '" AND '
				}
				else if(item.type.value == FieldType.type('ReferenceMultiSelect').value)
				{
					let i = 0;

					filters += '( ';

					for(const key2 in item.value)
					{
						filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + '.objectID:"' + item.value[key2].id + '" ';

						if(item.value.length - 1 !== i)
						{
							filters += ' OR ';
						}

						i++;
					}

					filters += ') AND ';
				}
				else if(item.type.value == FieldType.type('MultiSelect').value)
				{
					let i = 0;

					filters += '( ';

					for(const key2 in item.value)
					{
						filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + '.id:"' + item.value[key2].id + '" ';

						if(item.value.length - 1 !== i)
						{
							filters += ' OR ';
						}

						i++;
					}

					filters += ') AND ';
				}					
				else if(item.type.value == FieldType.type('Select').value)
				{
					filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + '.id:"' + item.value.id + '" AND ';
				}
				else if(item.type.value == FieldType.type('Toggle').value)
				{
					filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + ':' + item.value + ' AND '; // DEFEITO DO ALGOLIA
				}
				else if(item.type.value == FieldType.type('Number').value)
				{
					// ALGOLIA USA = COMO ==
					if(item.operator == '==')
					{
						item.operator = '=';
					}

					filters += item.field + ' ' + item.operator + ' ' + item.value + ' AND ';
				}
				else if(item.type.value == FieldType.type('Date').value)
				{
					if(typeof item.value.toDate == 'function')
					{						
						filters += item.field + ' ' + item.operator + ' ' + item.value.toDate().getTime() + ' AND ';
					}
					else if(typeof item.value.getTime == 'function')
					{
						filters += item.field + ' ' + item.operator + ' ' + item.value.getTime() + ' AND ';
					}									
				}
				else
				{
					filters += (item.operator == '!=' ? 'NOT ' : '') + item.field + ':"' + item.value + '" AND ';
				}
			}
		}

		const k = filters.lastIndexOf(' AND ');
		filters = filters.substring(0, k);
		filters = filters.replace(/this./gi, '');

		return filters;
	}*/
}
