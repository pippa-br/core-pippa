import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { BaseModel } from '../../../core/model/base.model';
import { FieldType } from '../../../core/type/field/field.type';
import { TDate } from '../../util/tdate/tdate';

export class Where extends BaseModel
{
	public label   : string;
	public name    : string; 
	public enabled : boolean; 
	public value   : any; 
	public setting : any;

    getMaps()
    {
        return {
            model : { klass : Where },
        }
    }

    initialize()
    {
		this.label       = '';
		this.name        = '';
		this.enabled     = true;
        this.value  	 = null;
        this['operator'] = '==';
		this['order']    = 0;
		this['groups']   = null;
        this['type']     = FieldType.type('Text');
	}

	async populate(data:any)
	{
		super.populate(data);

		await this.getLabel();
	}

	async getLabel()
	{
		const data : any = {
			label   : this.label,
			enabled : this.enabled
		};

		if(this['value'] === undefined)
		{	
			// NÃO ALTERAR O DATA
		}
		else if(this['value'] === null)
		{	
			data['value'] = 'null';
		}
		else if(this['type'].value == FieldType.type('Date').value)
		{
			if(this['value'] == 'now')
			{				
				data['value'] = new TDate().format('dd/MM/yyyy');
			}
			else if(typeof this['value'] == 'string')
			{
				data['value'] = new TDate({ value : this['value']}).format('dd/MM/yyyy');
			}
			else
			{
				data['value'] = new TDate({ value : this['value']}).format('dd/MM/yyyy');
			}
		}
		else if(this['type'].value == FieldType.type('ReferenceSelect').value)
		{
			const model : any = new BaseModel(this['value']);
			await model.on();

			data['value'] = model.name;
		}
		else if(this['type'].value == FieldType.type('Select').value)
		{
			data['value'] = this['value'].label;
		}
		else if(this['type'].value == FieldType.type('Toggle').value)
		{
			data['value'] = this['value'];
		}
		else if(this['type'].value == FieldType.type('ReferenceMultiSelect').value ||
				this['type'].value == FieldType.type('ProductMultiSelect').value)
		{
			let value = '';

			for(const item of this['value'])
			{
				const model : any = new BaseModel(item);
				await model.on();

				value += model.name + ', ';
			}

			value = value.slice(0, -2);

			data['value'] = value;
		}
 
		return data;
	}

	isEmpty()
	{
		/* POR CONTA DE BUSCAS OR */
		if(this.value instanceof Array && this.value.length == 0)
		{
			return true;
		}
		else if(this.value == null)
		{
			return true;
		}
		else if(this.value === '')
		{
			return true;
		}

		return false;
	}

    async parseFilter()
    {
		let field   = this['field'];
		let value   = this['value'];
		let context = this['context'];
		let path    = this['path'];

		/* PEGAR SOMENTE OS FILTROS COM O USUARIO*/
		if(this['_groups'] && this['_groups'].length > 0)
		{
			if(this.core().user)
			{
				if(!this.core().user.hasGroups(this['_groups']))
				{
					return {
						enabled : false
					};
				}				
			}	
		}

		/* IMPORTANT: ISSO PATH E NÃO A REFERENCIA É STRING */
		if(context)
		{
			const dataContext = this.core()[context] || this[context];

			if(dataContext)
			{
				value = await dataContext.getProperty(path);

				if(value === undefined)
				{
					console.error('value context undefined', context, path);

					// NUNCA DESABILITAR UM FRITRO POIS USUARIO PODE PERDER SESSION
					this['value'] = null;

					// return {
					// 	enabled : false,
					// };
				}
				else
				{
					this['value'] = value;
				}
			}
			else
			{
				console.error('not found context', context, path);
				
				// NUNCA DESABILITAR UM FRITRO POIS USUARIO PODE PERDER SESSION
				this['value'] = null;

				// return {
				// 	enabled : false
				// };
			}			
		}		

		if(value && typeof value == 'object')
        {
            if(this['type'].value == FieldType.type('ReferenceSelect').value)
            {
                if(this['value'] instanceof DocumentReference)
                {
                    value = this['value'];
				}
				else if(this['value'] instanceof Array)
				{
					value = this['value'];
				}
				else if(this['value'].referencePath)
                {
                    value = this['value'];
                }
                else
                {
                    value = this['value'].reference;
                }
            }
            else if(this['type'].value == FieldType.type('ReferenceMultiSelect').value ||
					this['type'].value == FieldType.type('ProductMultiSelect').value)
            {
				const values = [];
				
				/* CASO NÃO SEJA ARRAY */
				if(!(this['value'] instanceof Array))
				{
					this['value'] = [this['value']];
				}
    
                for(const key2 in this['value'])
                {
                    if(this['value'][key2] instanceof DocumentReference)
                    {
                        values.push(this['value'][key2]);
                    }
                    else if(this['value'][key2] && this['value'][key2].reference)
                    {
                        values.push(this['value'][key2].reference);
					}
					else
					{
						values.push(this['value'][key2]);
					}
                }

                value = values;
			}
			else if(this['type'].value == FieldType.type('Select').value)
			{				
						let fieldKey = 'id';

						if(this['operator'] == '==')
						{								
								if(this.setting && this.setting.fieldKey)
								{
									fieldKey = this.setting.fieldKey;
								}

								value = this['value'][fieldKey];
						}						

						// CASO DO NULLITEM
						if(value === 'null')
						{
							value = null;
						}

						if(this['operator'] == '==')
						{
								if(value !== null) // PARA FAZER FILTROS COM INDEXES NULL
								{
										field = field + '.' + fieldKey;
								}
						}
			}
			else if(this['type'].value == FieldType.type('PaymentStatus').value)
			{				
				value = this['value'].status;

				// CASO DO NULLITEM
				if(value === 'null')
				{
					value = null;
				}

				if(value !== null) // PARA FAZER FILTROS COM INDEXES NULL
				{
					field = 'payment.status';
				}				
			}			
		}
		else if(this['type'].value == FieldType.type('Date').value)
		{
			if(!value)
			{
				value = null;
				
				// return {
				// 	enabled : false
				// };
			}
			else if(this['value'] == 'now')
			{
				value = new TDate().toDate();
			}
			else if(this['value'] == 'nowFirstTime')
			{
				const date = new TDate().toDate();
				
				date.setHours(0);
				date.setMinutes(0);
				date.setSeconds(0);

				value = date;
			}
			else if(typeof this['value'] == 'string')
			{
				value = new TDate({ value : this['value'] });

				if(!value.isValid())
				{
					value = null;

					// return {
					// 	enabled : false
					// };
				}
				else
				{
					value = value.toDate();
				}
			}		
		}
		else if(this['type'].value == FieldType.type('Integer').value || this['type'].value == FieldType.type('Number').value)
		{
			value = parseInt(this['value']);
		}
		else if(this['type'].value == FieldType.type('Toggle').value)
		{
			value = this['value'];
		}				

		//else if(this['type'].value == FieldType.type('Text').value)
		//{
			//value = '' + this['value'];
		//}
		
		//this['value'] = value;		

        return {
            id       : this['id'],
            name     : this['name'],
            field    : field,
			operator : this['operator'],
			order    : this['order'],
            value    : value,
            type     : this['type'],
            enabled  : this['enabled'],
        }
	}	
}
