import { Cart } 	from './cart';
import { BaseList } from '../base.list';

export class CartCollection extends BaseList<Cart>
{
    getMaps():any
    {
        return {
            model : { klass : Cart },
            list  : { klass : CartCollection },
        };
    }
}
