import { BaseModel }      	  from '../base.model';
import { App }            	  from '../app/app';
import { Form }           	  from '../form/form';
import { DocumentCollection } from '../document/document.collection';

export class Cart extends BaseModel
{
    getMaps()
    {
        return {
            model  : { klass : Cart },
            form   : { klass : Form },
            items  : { klass : DocumentCollection }
        };
    }

    initialize()
    {
        this['items'] = new DocumentCollection();
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* ITEMS */
                    if(this['items'] && this['items'].length > 0)
                    {
                        promises.push(this.getProperty('items', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    });
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
