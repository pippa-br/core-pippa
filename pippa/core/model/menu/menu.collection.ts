import { Menu } from '../menu/menu';
import { BaseList } from '../base.list';

export class MenuCollection extends BaseList<Menu>
{
    getMaps():any
    {
        return {
            model : { klass : Menu },
            list  : { klass : MenuCollection },
        };
    }

    toFlat()
    {
        const menuCollection = new MenuCollection();

        for(const key in this)
        {
            const menu : any = this[key];

            if(menu._children && menu._children.length > 0)
            {
                for(const key2 in menu._children)
                {
                    menuCollection.add(menu._children[key2]);
                }
            }
            else
            {
                menuCollection.add(menu);
            }            
        }

        return menuCollection;
	}
	
	getData()
	{
		const children = [];

		for(const key in this)
		{
			children.push(this[key].reference);
		}

		return children;
	}
}
