import { BaseModel }      from '../base.model';
import { App }            from '../app/app';
import { User }           from '../user/user';
import { Form }           from '../form/form';
import { MenuCollection } from './menu.collection';

export class Menu extends BaseModel
{
    getMaps()
    {
        return {
			model     : { klass : Menu },
			owner     : { klass : User },
            user      : { klass : User },
            app       : { klass : App },
            form      : { klass : Form },
            _children : { klass : MenuCollection }
        };
    }

    initialize()
    {
        this['_parent']   = null;
        this['_children'] = new MenuCollection();
        this['_level']    = 1;
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* APP */
                    if(this['app'])
                    {                        
                        promises.push(this.getProperty('app', fetch));
                    }

                    /* CHILDREN */
                    if(this['_children'] && this['_children'].length > 0)
                    {
                        promises.push(this.getProperty('_children', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    });
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
