//import { DocumentReference } from 'firebase/firestore';

// PIPPA
import { BaseModel }  from "./base.model";
import { Subject }    from "rxjs";
import { Core }       from "../util/core/core";
import { Types }      from "../type/types";

export class BaseList<T> extends Array<T>
{
    protected id              : any;
    protected _reference      : any;
    protected _query          : any;
    protected _sort           : any;
    protected _isPopulate      = false;
    protected _onUpdate       : any;    /* USADO PARA VARIAS CHAMDAS SIMUTANIAS DO CHANGE */
    protected _onNextPage     : any;
    protected _onPrevPage     : any;
    protected _onSearch       : any;
    protected _onNextPageLoad : any;
    protected _onPrevPageLoad : any;
    protected _onIterator     : any;
    protected isMock           = false;
    protected _index          : any;
    protected _appid          : any;
    protected _activeFilter    = false;
    protected _filterAsync    : any;
    protected _filter         : any;
    protected _filterItems    : any;
    protected fetch           : boolean;
    protected unsubscribe     : any;
    public    iteratorItem     = 0;
    public    streamTimeOut   : any;
    public    isInfinite       = false;
    public    _instance       : any;
    public    _maps           : any;
    public    onlyCount        = false;
    public    autoReload       = false;
    public    _total          : number;
    public    _count          : number;
    public    _percentage     : number;
    public 	  keyCache		  : string;
    //public    searchFirst     : boolean = false;
    //public    _data           : any;

    constructor(items?:any, fetch = false)
    {
        super();

        this.fetch = fetch;

        this.initialize(items);
    }

    getMaps():any
    {
        return {
            model : { klass : BaseModel },
            list  : { klass : BaseList },
        };
    }
	
    isEmpty()
    {
        return this.length == 0;
    }

    get total()
    {
        return this._query._total
    }

    set total(value:number)
    {
        this._query._total = value;
    }

    get perPage()
    {
        return this._query.perPage
    }

    set perPage(value:number)
    {
        this._query.perPage = value;
    }

    /* METODO UTILIZADO PARA INICIALIZAR VARIABLES */
    initialize(items:Array<any>)
    {
        this.id              = this.core().util.randomString(20);
        this._onNextPage     = new Subject();
        this._onPrevPage     = new Subject();
        this._onNextPageLoad = new Subject();
        this._onPrevPageLoad = new Subject();
        this._onSearch       = new Subject();
        this._onIterator     = new Subject();
        this._onUpdate       = new Subject();

        Object.defineProperty(this, "id",                { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_reference",        { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onUpdate",         { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_isPopulate",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_query",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_sort",             { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onNextPage",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onPrevPage",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onNextPageLoad",   { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onPrevPageLoad",   { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onIterator",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_onSearch",         { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_index",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "angularFirestore",  { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_appid",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_activeFilter",     { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_filter",           { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_filterAsync",      { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_filterItems",      { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "fetch",             { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "unsubscribe",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "iteratorItem",      { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "streamTimeOut",     { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "isInfinite",        { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_instance",         { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_maps",             { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_total",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_count",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_percentage",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "searchFirst",       { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "onlyCount",         { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "autoReload",        { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "keyCache",          { writable : true, enumerable : false, configurable : true });
        //Object.defineProperty(this, "_data",             {writable: true, enumerable: false, configurable: true});

        Object.getOwnPropertyNames(this).forEach((prop) =>
        {
            if (prop == "constructor" || prop == "length")
            {
                return;
            }

            Object.defineProperty(this, prop,  { writable : true, enumerable : false, configurable : true });
        });

        if (items)
        {
            this.populate(items);
        }
    }

    set appid(id)
    {
        this._appid = id;
    }

    get appid()
    {
        return this._appid;
    }
	
    set reference(value:any)
    {
        this._reference = value;

        // this.doUnsubscribe(); 
            
        // /* AUTO RELOAD  */
        // if(this.autoReload && this.reference)
        // {
        // 	this.unsubscribe = this.reference.stateChanges().subscribe((docs) => 
        // 	{
        // 		this.reload();
        // 	});
        // }
    }

    get reference()
    {
        return this._reference;
    }

    setFilter : (callbackfn: (value: any, index?: number, array?: any) => boolean, thisArg?: any) => any = function(callbackfn: (value: any, index?: number, array?: any) => boolean, thisArg?: any) : any
    {
        if (callbackfn)
        {
            /* SO ADICIONAR O FILTRO DE POIS QUE OS ELEMENTOS FORAM CARREGADOS */
            this.on().then(() =>
            {
                if (!this._activeFilter)
                {
                    this._filterItems  = this.slice();
                    this._activeFilter = true;
                }

                const items = this._filterItems.filter(callbackfn, thisArg);
                this._updateItems(items);

                this._filter = callbackfn;
            });
        }
    }
	
    async localFilter(where:any)
    {
        const list = new BaseList();
		
        for (const key in this)
        {
            const item  : any = this[key];
			  let valid         = true;

            for (const key2 in where)
            {
                const item2 = where[key2];
                const value = await item.getProperty(item2.field);

                valid = valid && value == item2.value;
            }

            if (valid)
            {
                list.add(item);
            }
        }

        return list;
    }

    sortAsync(predicate:any)
    {
        return new Promise<void>((resolve:any) =>
        {
            const promises = [];

            for (const key in this)
            {
                promises.push(predicate(this[key]));
            }

            Promise.all(promises).then((result:any) =>
            {
                const items = [];                

                for (const key in result)
                {
                    this[key]._sort_key = result[key];
                }

                this.setSort((a:any, b:any) =>
                {   
                    if (a._sort_key > b._sort_key) 
                    {
                        return 1;
                    }
                      
                    if (a._sort_key < b._sort_key) 
                    {
                        return -1;
                    }
                    
                    return 0;
                });

                this.dispatchUpdate();   

                resolve();
            })
        });
    }

    filterAsync(predicate:any)
    {
        return new Promise<any>((resolve:any) =>
        {
            if (!this._activeFilter)
            {
                this._filterItems  = this.slice();
                this._activeFilter = true;
            }

            this._filterAsync = predicate;
            const promises    = [];

            for (const key in this._filterItems)
            {
                promises.push(predicate(this._filterItems[key]));
            }

            Promise.all(promises).then((result:any) =>
            {
                const items = [];

                for (const key in result)
                {
                    if (result[key])
                    {
                        items.push(this._filterItems[key]);
                    }
                }

                this._updateItems(items).then(() => 
                {
                    this.dispatchUpdate();   
                
                    resolve(items);    
                });
            })
        });
    }

    get term()
    {
        return this._query ? this._query.term : "";
    }

    get filters()
    {
        return this._query ? this._query.filters : "";
    }

    setQuery(query:any)
    {
        //this.runQuery(query);
        this._query = query;
    }

    getQuery():any
    {
        return this._query;
    }

    onUpdate()
    {
        return this._onUpdate.asObservable();
    }

    dispatchUpdate()
    {
        this._onUpdate.next(this);
    }

    dispatchSearch()
    {
        this._onSearch.next(this);
    }

    doUnsubscribe()
    {
        if (this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
    }

    reload():Promise<any>
    {
        return new Promise<void>((resolve) =>
        {
            console.log("reload init", this.appid);            

            if (this._query)
            {                
                /* SET QUERY */
                this._query.reload().then(async (_query:any) =>
                {
                    //this.reference.query = _query;

                    if (!_query.term && _query.loading)
                    {
                        await this.core().loadingController.start();
                    }					

                    /* LOAD DATA */
                    const result = await this.core().api.callable(_query.listEndPoint, {
                        maccid        		: _query.maccid,
                        accid         		: _query.accid, 
                        appid         		: _query.appid,
                        colid         		: _query.colid, 
                        groupCollection : _query.groupCollection,
                        onlyMaster    		: _query.onlyMaster, 
                        perPage       		: _query.perPage,
                        asc       	   		: _query.asc,
                        orderBy       		: _query.orderBy,
                        startAfter    		: _query.startAfter,
                        page      		    : _query.page,
                        startAt       		: _query.startAt,
                        endAt         		: _query.endAt,
                        total         		: _query.total,
                        where         		: _query._where, 
                        map           		: _query.map,
                        mapItems       	: _query.mapItems,
                        platform        : _query.platform,
                        searchVector    : _query.searchVector
                    });

                    this.core().loadingController.stop();

                    //const app          = this.core().getApp(this.appid);
                    //this._query._total  = result.total;
                    this._query.perPage = result.perPage;
                    this.keyCache       = result.keyCache;

                    if (result.error)
                    {
                        this._isPopulate = true;
                        this.isMock      = false;

                        resolve();
                    }
                    else
                    {
                        console.log("reload onSnapshot " + this.appid, this.length, this._query._total, result.collection);

                        this.iteratorItem = 0;                        
	
                        this.populate(result.collection).then(async () => 
                        {						
                            await this.doReloadData();

                            if (this._isPopulate)
                            {
                                /* DISPATCH */
                                this.dispatchUpdate();
                            }
	
                            this._isPopulate = true;
                            this.isMock      = false;

                            /* CASO FOR UMA LISTA FETCH */
                            if (this.fetch)
                            {
                                this.on(this.fetch, this._maps).then(() =>
                                {
                                    resolve();
                                });
                            }
                            else
                            {
                                resolve();
                            }
                        });
                    }					

                    //console.error('------', this.reference.ref.path, counResult);
					
                    /*this.unsubscribe = this.reference.snapshotChanges()
                    .pipe(
                        map(
                        (documents:any) =>
                        {
                            return documents.map((a:any) =>
                            {
								const data = a.payload.doc.data();
								data.id    = a.payload.doc.id;								

                                return {...data,
                                	reference         : a.payload.doc.ref,
                                	exists            : a.payload.doc.exists,
                                	_documentSnapshot : a.payload.doc,
                                };
                            });
                        }
                    )).subscribe((docs:any) =>
                    {						
                        
                    });*/
                });   
            }   
            else
            {
                this.on(this.fetch, this._maps).then(() =>
                {
                    resolve();
                });			
            }
        });
    }

    async clearCache()
    {
        await this.core().loadingController.start();

        const _query = await this._query.reload();

        /* LOAD DATA */
        const result = await this.core().api.callable(Types.CLEAR_CACHE_DOCUMENT_API, {
            maccid          : _query.maccid,
            accid           : _query.accid, 
            appid           : _query.appid,
            colid           : _query.colid, 
            groupCollection : _query.groupCollection,
            onlyMaster      : _query.onlyMaster, 
            perPage         : _query.perPage,
            asc         	   : _query.asc,
            orderBy         : _query.orderBy,
            startAfter      : _query.startAfter,
            page            : _query.page,
            startAt         : _query.startAt,
            endAt           : _query.endAt,
            total           : _query.total,
            where           : _query._where,
            mapItems        : _query.mapItems,
            platform        : _query.platform, 
        });

        this.core().loadingController.stop();
    }

    search(term?:string, lowerCase?:boolean)
    {
        console.log("init search " + this.appid, term, this._query); 

        this.iteratorItem = 0;

        return new Promise<void>((resolve) =>
        {            
            this.clear(); /* POR CAUSA QUANDO UTILIZA INFINITE */

            this._query.reset();

            if (term != undefined)
            {
                if (lowerCase || lowerCase === undefined)
                {
                    term = term.toLowerCase();
                }

                term = this.core().util.removeAccents(term);

                this._query.term = term;
                this._query.searchVector = term;
            }

            this.reload().then(() =>
            {
                this.dispatchSearch();
                resolve();
            });            
        });
    }
	
    reset()
    {
        if (this._query)
        {
            this._query.reset();
        }
    }

    nextPageOffset(page = 1)
    {
        return new Promise<void>(resolve =>
        {
            this._query.nextPage(page);

            this.reload().then(() =>
            {
                this._onNextPageLoad.next();

                resolve();
            });
        });
    }
    
    nextPageReference()
    {
        return new Promise<void>(resolve =>
        {
            const item : any = this.lastItem();

            if (item)
            {                
                this._onNextPage.next();

                this._query.nextPageReference(item.referencePath);
				
                this.iteratorItem = 0;

                this.reload().then(() =>
                {
                    this._onNextPageLoad.next();

                    resolve();
                });
            }
        });
    }

    prevPage()
    {
        return new Promise<void>(resolve =>
        {
            this._onPrevPage.next();

            this._query.prevPage();

            this.iteratorItem = 0;

            this.reload().then(() =>
            {
                this._onPrevPageLoad.next();

                resolve();
            });
        });
    }

    hasPrevPage()
    {
        return this._query && this._query.hasPrevPage();
    }

    hasNextPage()
    {
        return this._query && this._query.hasNextPage();
    }

    firstItem()
    {
        if (this.length > 0)
        {
            return this[0];
        }
    }

    lastItem()
    {
        if (this.length > 0)
        {
            return this[this.length - 1];
        }
    }

    setSort(s:any):void
    {
        this._sort = s;

        if (this._sort)
        {
            this.sort(this._sort);
        }
        else
        {
            this.sort(function()
            {
                return 0;
            });
        }
    }

    getSort():any
    {
        return this._sort;
    }

    doSort():any
    {
        this.setSort(this.getSort());
    }

    updateFilter():void
    {
        const filter = this._filter;

        console.log("updateFilter", filter);

        this.clearFilter();
        this.setFilter(filter);
    }

    clearFilter()
    {
        if (this._activeFilter)
        {
            this._updateItems(this._filterItems);
            this._filterItems  = null;
            this._activeFilter = false;
            this._filter       = null
            this._filterAsync  = null;
        }
    }

    updateAsyncFilter()
    {
        return new Promise<void>((resolve) =>
        {
            const filter = this._filterAsync;

            console.log("updateAsyncFilter", filter);

            this.clearFilter();
            this.filterAsync(filter).then(() =>
            {
                resolve();
            });
        });
    }

    populate(items:any)
    {
        return new Promise<void>(resolve =>
        {
            if (!this.isInfinite)
            {
                this.clear();
            }

            this._setItems(items);

            //this._data = items;

            /* SORT */
            if (this._sort)
            {
                this.sort(this._sort);
            }

            /* FILTER */
            if (this._filter)
            {
                this.updateFilter();
            }

            /* FILTER ASYNC */
            if (this._filterAsync)
            {
                this.updateAsyncFilter();
            }  

            resolve();                    
        });
    }

    on(fetch = false, maps?:any)
    {
        return new Promise<any>((resolve) =>
        {
            this.fetch = fetch;
            this._maps = maps;

            if (this.length == 0)
            {
                resolve(this);
            }
            else
            {
                const promises = [];

                for (const key in this)
                {
                    const data : any = this[key];
                    promises.push(data.on(fetch, maps));
                }

                Promise.all(promises).then(() =>
                {
                    resolve(this);
                });
            }
        });
    }

    async doReloadData()
    {
        const promisses = [];

        for (const key in this)
        {
            const item : any = this[key];
            promisses.push(item.reload());
            promisses.push(item.on());
            //await item.reload();
            //await item.on();
        }

        await Promise.all(promisses);
    }

    parseItem(item:any):any
    {
        const maps        = this.getMaps();
        const model : any = maps.model.klass;
        const list  : any = maps.list.klass;
        let   data  : any;

        if (item instanceof model)
        {
            data = item;
        }
        else if (item instanceof list)
        {
            data = item;
        }
        else
        {
            data = new model(item);
        }

        return data;
    }

    setItems(items:any):void
    {
        this._setItems(items);

        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    set(item:any, index?:number):void
    {
        this._set(item, index);

        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    /* NO DISPATCHNEXT */
    _setItems(items:any):void
    {
        for (const key in items)
        {
            if (key != "_dataReferences") // QUANDO O OBJECT NÃO É ARRAY E SIM UM OBJECT
            {
                const item = items[key];
                this._set(item);	
            }
        }
    }

    /* NO DISPATCHNEXT */
    _set(item:any, index?:number):void
    {
        let item2 = this.findItem(item);   
        
        if (item2)
        {
            item2.populate(item);
        }
        else
        {            
            item2 = this.parseItem(item);
            this.push(item2);
        }

        /* MOVE */
        if (index != undefined && index != -1)
        {
            const position = this.findPosition(item2);
            this._move(position, index);
        }
    }

    add(item:any):void
    {
        this._add(item);
        
        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    _add(item:any):void
    {
        const data = this.parseItem(item);
        this.push(data);
    }

    addItems(items:any):void
    {
        for (const key in items)
        {
            this._add(items[key])
        }

        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    updateItems(items:any)
    {
        return new Promise<void>(resolve =>
        {
            this._updateItems(items).then(() =>
            {
                /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
                if (!this.unsubscribe)
                {
                    this.dispatchUpdate();   
                }
                
                resolve();
            });
        });
    }

    _updateItems(items:any)
    {
        return new Promise<void>(resolve =>
        {
            this.splice(0, this.length);

            for (const key in items)
            {
                this._add(items[key])
            }

            this.on(this.fetch, this._maps).then(() =>
            {
                resolve();
            });
        });
    }

    getById(id:number)
    {
        for (const key in this)
        {
            const item : any = this[key];

            if (item.id == id)
            {
                return this[key];
            }
        }
    }

    save(data:any):Promise<any>
    {
        return new Promise((resolve) =>
        {
            this.reference.add(data).then(async (ref:any) =>
            {
                const maps   = this.getMaps();
                const model  = maps.model.klass;
                const model2 = new model(ref);

                model2.on(true).then(() =>
                {
                    console.log("save", model2);

                    resolve(model2);
                });					
            }); 
        });
    }

    setItem(item:any):void
    {
        this.set(item);
    }
	
    delItem(item:any):void
    {
        this.del(item);
    }

    delItems(items:any):void
    {
        for (const key in items)
        {
            const item = items[key];
            this._del(item);
        }

        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    del(item:any):boolean
    {
        const value = this._del(item);
        
        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }

        return value;
    }

    /* PARA QUANDO UTILIZA O DELITEMS NÃO DISPARA VARIOS DISPATCHNEXT */
    _del(item:any):boolean
    {
        const position = this.findPosition(item);

        if (position > -1)
        {
            this.splice(position, 1);

            console.log("del", item.id, position, this.length);
            return true;
        }
        else
        {
            console.log("no del: no find", item.id, position, this.length);

            return false;
        }
    }

    move(from:any, to:any)
    {
        this._move(from, to);
        
        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    _move(from:any, to:any)
    {
        if (from >= 0 && to >= 0 && from <= this.length - 1 && to <= this.length - 1)
        {
            this.splice(to, 0, this.splice(from, 1)[0]);
            console.log("move", from, to);
        }
    }

    /* PQ O LENGTH NÃO ESTAVA ATUALIZANDO */
    hasItems()
    {
        return this.length > 0;
    }

    delAll():void
    {
        /* NÃO USAR LOOP POIS HAVERA VARIOS UPDATE NO VIEW */
        this.splice(0, this.length);

        /* CASO TENHA SUBSCRIBER O RELOAD FICA POR CANTA DA ATUALIZACO */
        if (!this.unsubscribe)
        {
            this.dispatchUpdate();   
        }
    }

    has(item:any):boolean
    {
        return this.findPosition(item) != -1;
    }

    get(item:any):any
    {
        for (const key in this)
        {
            const item2 : any = this[key];

            if (item.id == item2.id)
            {
                return item2;
            }
        }

        return;
    }

    findPosition(item:any):number
    {
        let i = 0;

        for (const key in this)
        {
            const item2 : any = this[key];

            if (item.id == item2.id)
            {
                return i;
            }

            i++;
        }

        return -1;
    }

    findItem(item:any):any
    {
        for (const key in this)
        {
            const item2 : any = this[key];

            if (item && item2 && item.id == item2.id)
            {
                return item2;
            }
        }
    }

    copy()
    {
        const maps : any = this.getMaps();
        const list : any = new maps.list.klass;

        for (const key in this)
        {
            const item : any = this[key];
            list.push(item.copy());
        }

        return list;
    }

    parseData():any
    {
        const items = [];

        for (const key in this)
        {
            const item : any = this[key];

            if (item.reference)
            {
                items.push(item.reference);
            }            
            else
            {
                items.push(item.parseData());
            }
        }

        return items;
    }

    getData():any
    {
        const items = [];

        for (const key in this)
        {
            const item : any = this[key];

            /*if(item instanceof DocumentReference)
			{
				const maps = this.getMaps();
				items.push(new maps.model.klass(item));
			}
			else
			{
				
			}   */
			
            items.push(item.getData());
        }

        return items;
    }

    clear():void
    {
        for (const key in this)
        {
            const item : any = this[key];
			
            item.doUnsubscribe();
        }

        this.splice(0, this.length);
    }

    /* REMOVE ITEMS CRIADOS VAZIOS */
    /*delEmpties():Promise<any>
    {
        return new Promise<any>(resolve =>
        {
            const empties = [];

            for(const key in this)
            {
                const data : any = this[key];

                if(!data.exists)
                {
                    empties.push(data);
                }
            }

            const promise = [];

            for(const key2 in empties)
            {
                console.log('----- delEmpties', empties[key2]);
                //promise.push(this.del(empties[key2]));
            }

            Promise.all(promise).then(() =>
            {
                resolve();
            });
        });
    }*/

    iterator()
    {
        this.onNextPageLoad().subscribe(() =>
        {
            if (this.length > 0)
            {
                this.nextIterator();
            }
        });

        /* ISSO PARA DAR TEMPO DE EXECUTAR O RETURN */
        setTimeout(() =>
        {
            console.log("iterator init", this.length);						

            if (this.length > 0)
            {
                this.nextIterator();
            }
        }, 1000);

        return this._onIterator.asObservable();
    }

    nextIterator()
    {
        return new Promise((resolve) =>
        {
            if (this.length == this.iteratorItem)
            {
                if (this.hasNextPage())
                {
                    console.log("iterator NextPage");
					
                    this.nextPageReference().then(() => 
                    {
                        if (this.length > 0)
                        {
                            resolve(true);
                        }
                        else
                        {
                            resolve(false);
                        }						
                    })
                }
                else
                {
                    resolve(false);
                    console.log("iterator finish");
                }
            }
            else
            {
                const data = this.nextIteratorItem();

                console.log("iterator", this.iteratorItem, this.length, data);

                if (data)
                {
                    this._onIterator.next(data);
                    resolve(true);
                    console.log("iterator next");
                }
                else
                {
                    resolve(false);
                    console.log("iterator finish")
                }
            }
        });
    }

    nextIteratorItem()
    {
        if (this.iteratorItem < this.length)
        {
            const data = this[this.iteratorItem];
            this.iteratorItem++;

            return data;
        }
        else
        {
            /* QUANDO FINALIZAR RESET */
            this.iteratorItem = 0;

            return;
        }
    }
	
    async groupBy(path:string)
    {
        const datas : any = {};

        for (const key in this)
        {
            const item : any = this[key];
            const key2 : any = await item.getProperty(path);

            if (!datas[key2])
            {
                datas[key2] = new BaseList();
            }

            datas[key2].push(item);
        }

        /* CONVERT ARRAY */
        const list = new BaseList();

        for (const key in datas)
        {
            list.push({
                group  	   : key,
                collection : datas[key],
            });	
        }

        return list;
    }

    onSearch()
    {
        return this._onSearch.asObservable();
    }

    onNextPage()
    {
        return this._onNextPage.asObservable();
    }

    onPrevPage()
    {
        return this._onPrevPage.asObservable();
    }

    onNextPageLoad()
    {
        return this._onNextPageLoad.asObservable();
    }

    onPrevPageLoad()
    {
        return this._onPrevPageLoad.asObservable();
    }

    core()
    {
        return Core.get();
    }
}
