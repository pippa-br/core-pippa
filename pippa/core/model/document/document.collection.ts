import { Document } from '../document/document';
import { BaseList }     from '../base.list';

export class DocumentCollection extends BaseList<Document>
{
    getMaps():any
    {
        return {
            model : { klass : Document },
            list  : { klass : DocumentCollection }
        };
    }
}
