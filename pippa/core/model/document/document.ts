import { BaseModel }          from "../base.model";
import { User }               from "../user/user";
import { Form }               from "../form/form";

export class Document extends BaseModel
{
    getMaps()
    {
        return {
            model : { klass : Document },
            form  : { klass : Form },
            owner : { klass : User },
            user  : { klass : User },
        }
    }	
}
