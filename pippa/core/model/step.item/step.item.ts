/* PIPPA */
import { BaseModel }          from '../base.model';
import { FormItemCollection } from '../form.item/form.item.collection';

export class StepItem extends BaseModel
{
	public items    : FormItemCollection;
	public position : number;
	public display  : boolean;
	public complete : boolean;
	public matrix   : any;

    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        Object.defineProperty(this, "items",     { writable: true, enumerable: false, configurable: true }); 
        Object.defineProperty(this, "position",  { writable: true, enumerable: false, configurable: true }); 
		Object.defineProperty(this, "display",   { writable: true, enumerable: false, configurable: true }); 
		Object.defineProperty(this, "complete",  { writable: true, enumerable: false, configurable: true }); 
        Object.defineProperty(this, "matrix",    { writable: true, enumerable: false, configurable: true }); 
    }

    getMaps()
    {
        return {
            model : { klass : StepItem },
        };
    }

    initialize()
    {
        /* DEFAULT */
        this['label']   = "";  
		this['onlyAdd'] = false;
		this['onlySet'] = false;

        /* CONTROL */
		this['position'] = 0;
		this['display']  = true;
		this['complete'] = false;
        this['matrix']   = new FormItemCollection();
        this['items']    = new FormItemCollection();  
    }

	hasGroups(values:any)
	{
		let has : boolean = false;

		const _groups = this['_groups'];		

		if(_groups && _groups.length > 0)
		{
			for(const key in values)
			{
				for(const key2 in _groups)
				{
					let value : string;
	
					if(values[key].value)
					{
						value = values[key].value;
					}
					else
					{
						value = values[key];
					}
	
					if(value === _groups[key2].value || value == 'all')
					{
						has = true;
						break;
					}
				}			
			}
		}
		else
		{
			has = true; 
		}

		return has;
	}

	isComplete(data:any)
	{
		if(data._steps && data._steps[this['id']])
		{
			return true
		}

		return false;
	}

    async doMatrix()
    {
        const matrix : any = new FormItemCollection();

        for(const key in this['items'])
        {
            const item : any = this['items'][key];

            if(item)
            {
                if(!matrix[item.row])
                {
					matrix[item.row] = new FormItemCollection();
				}
				
				if(!matrix[item.row][item.col])
                {
					matrix[item.row][item.col] = new FormItemCollection();
				}

                matrix[item.row][item.col].set(item);
            }
		}
		
		this['matrix'] = matrix;     
		
		await this.normalizeMatrix();
	}

	async normalizeMatrix()
	{		
		const _matrix : any = new FormItemCollection();
		let i = 0;		

		for(const key in this['matrix'])
		{
			if(this['matrix'][key] && this['matrix'][key].length > 0)
			{			
				_matrix.set(new FormItemCollection());

				let j = 0;
				
				for(const key2 in this['matrix'][key])
				{
					if(this['matrix'][key][key2] && this['matrix'][key][key2].length > 0)
					{												
						let k = 0;
						_matrix[i].set(new FormItemCollection());

						for(const key3 in this['matrix'][key][key2])
						{
							const data = this['matrix'][key][key2][key3];

							if(data)
							{	
								_matrix[i][j].set(data);	

								k++;
							}							
						}

						_matrix[i][j].doSort();

						await _matrix[i][j].on(true);

						j++;
					}
				}				

				i++;
			}
		}

		console.log('normalizeMatrix', _matrix);

		this['matrix'] = _matrix;
	}
}
