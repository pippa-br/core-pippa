/* APP */
import { BaseList } from '../base.list';
import { StepItem } from './step.item';
import { FormItemCollection } from '../form.item/form.item.collection';

export class StepItemCollection extends BaseList<StepItem>
{
    getMaps()
    {
        return {
            model : { klass : StepItem },
            list  : { klass : StepItemCollection } 
        };
    }

    getSort()
    {
        this._sort = function(a:any, b:any)
        {
            if(a.stepOrder < b.stepOrder) return -1;
            if(a.stepOrder > b.stepOrder) return 1;

            return 0;
        };

        return this._sort;
    }

    async doMatrix()
    {
        for(const key in this)
        {
            await this[key].doMatrix();
        }
	}
	
	async normalizeMatrix()
    {
        for(const key in this)
        {
            await this[key].normalizeMatrix();
        }
	}
	
	getItems()
	{
		const formsItems = new FormItemCollection();

		for(const key in this)
        {
			const item : any = this[key];
            formsItems.setItems(item.items);
		}
		
		return formsItems;
	}
}
