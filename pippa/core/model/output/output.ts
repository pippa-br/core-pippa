/* FIELD */
import { BaseModel } from '../base.model';
import { FormItem }  from '../../model/form.item/form.item';

export class Output extends BaseModel
{
    /*public label         : string;    
    public format        : string; /* DADO FORMATADO */
    /*public total         : number;
    public amount        : number;
    public count         : number;
    public percentage    : number;    
    public parseAmount   : any;
    public formItem      : FormItem;
    public data          : any;*/

    /* ONLY VIEWER */
    public form          : any;
    public grid          : any;
    public viewer        : any;
    public component     : any;
    public value         : any;
    public map           : any;

    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        Object.defineProperty(this, "form",          { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "grid",          { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "viewer",        { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "component",     { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "value",         { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "map",           { writable: true, enumerable: false, configurable: true });
    }

    getMaps()
    {
        return {
            model : { klass : Output },
        };
    }

    initialize()
    {
		this['percentage'] = 0;		
        //this.total         = 0;
        //this.amount        = 0;
        //this.count         = 0;
        //this.format        = '';        
    }

    isSimpleValue()
    {
        if(this.value)
        {
            return (typeof this.value == 'string');
        }

        return false;
    }
}
