/* PIPPA */
import { Output } from './output';
import { BaseList }   from '../base.list';

export class OutputCollection extends BaseList<Promise<Output>>
{
    public _data : any;

    getMaps()
    {
        return {
            model : { klass : Output},
        };
    }

    constructor(args?:any)
    {
        super(args);

        Object.defineProperty(this, "sum", {writable: true, enumerable: false, configurable: true});
    }

    /*sum : () => Promise<any> = function() : Promise<any>
    {
        return new Promise<any>((resolve, reject) =>
        {
            Promise.all(this).then(function(items:any)
            {
                let labels : Array<string> = [];
                let values : Array<any> = [];
                let total  : number = 0;
                let count  : number = 0;

                for(let key in items)
                {
                    total += items[key].total;
                    count += items[key].count;
                    labels.push(items[key].label);
                    values.push(items[key].value);
                }

                resolve({
                    label : labels,
                    value : values,
                    total : total,
                    count : count
                });
            });
        });
    }*/
}
