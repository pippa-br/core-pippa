/* PIPPA */
import { BaseModel } from '../../model/base.model';
import { Form }      from '../form/form';
import { App }       from '../app/app';

export class EmailTemplate extends BaseModel
{
    getMaps()
    {
        return {
            model : { klass : EmailTemplate },
			form  : { klass : Form },
			app   : { klass : App },
        };
	}
	
	collectionName()
	{
		return 'email.template';
	}
}
