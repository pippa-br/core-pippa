import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList } from '../base.list';
import { EmailTemplate } from './email.template';

export class EmailTemplateCollection extends BaseList<EmailTemplate>
{
    getMaps()
    {
        return {
            model : { klass : EmailTemplate },
            list  : { klass : EmailTemplateCollection },
        };
    }

    constructor(args?:any)
    {
        super(args);
    }
}
