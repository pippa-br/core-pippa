/* PIPPA */
import { Core }      from '../../util/core/core';
import { BaseModel } from '../../../core/model/base.model';

export class File extends BaseModel
{
    static FILE     : string = 'file';
    static IMAGE    : string = 'image';
    static AUDIO    : string = 'audio';
    static VIDEO    : string = 'video';
	static DOCUMENT : string = 'document';
	static SAMPLE   : string = 'sample';
	static PDF 		: string = 'pdf';
    static ICON     : string = 'icon';

    /*public type       : string;
    public path       : string;
    public name       : string;
    public value      : string;
    public commentary : string;
    public width      : number;
    public height     : number;
    private _url      : string;*/

    getMaps()
    {
        return {
            type : { value : File.FILE }
        }
    }

    populate(data:any)
    {
        super.populate(data);

        /* CORRIGI NOME DO ARQUIVO */
        if(this['name'])
        {
            if(this['name'].indexOf('?') > 0)
            {
                this['name'] = this['name'].substring(0, this['name'].indexOf('?'));
            }    

            this['name'] = decodeURI(this['name']);
        }
    }

    set url(url:string)
    {
        if(url)
        {
            url = decodeURI(url);

            /* REMOVE QUERY STRING PARA ENCONTRAR O TYPE, MAS NÃO REMOVER DA URL POR CAUSA DO FIREBASE */
            let __url = url.toLowerCase().split(/[?#]/)[0];

            if(__url.match(/\.(jpeg|jpg|gif|png)$/) !== null)
            {
                this['type'] = File.IMAGE;
            }
            else if(__url.match(/\.(doc|docx|xls|xlsx)$/) !== null)
            {
                this['type'] = File.DOCUMENT;
			}
			else if(__url.match(/\.(pdf)$/) !== null)
            {
                this['type'] = File.PDF;
            }
            else if(__url.match(/\.(mp4|m4a|m4v|f4v|f4a|m4b|m4r|f4b|mov|3gp|3gp2|3g2|3gpp|3gpp2|ogg|ogavogv|ogx|wmv|wma|asf|webm|flv)$/) !== null)
            {
                this['type'] = File.VIDEO;
            }

            let isIOS     = Core.get().isIOS;
            let isAndroid = Core.get().isAndroid;

            if(isIOS)
            {
                /* RESIZE PRECISOU DO FILE */
                //url = url.replace(/^file:\/\//, '');
            }
            else if(isAndroid)
            {
                /* ERROR ANDROID - URI SEM FILE:// */
                if(url.indexOf('://') == -1)
                {
                    url = 'file://' + url;
                }
            }

            /* SET NAME */
            if(!this['name'])
            {
                this['name'] = url;
                this['name'] = this['name'].substring(this['name'].lastIndexOf('/') + 1);

                if(this['name'].indexOf('?') > 0)
                {
                    this['name'] = this['name'].substring(0, this['name'].indexOf('?'));
                }
            }
        }

        this['_url'] = url;
    }

    get url():string
    {
        return this['_url'];
    }

    isImage()
    {
        return this['type'] == File.IMAGE;
    }

	isSample()
    {
        return this['type'] == File.SAMPLE;
    }

    isAudio()
    {
        return this['type'] == File.AUDIO;
	}
	
	isPDF()
    {
        return this['type'] == File.PDF;
    }

    isDocument()
    {
        return this['type'] == File.DOCUMENT;
    }

    isVideo()
    {
        return this['type'] == File.VIDEO;
    }
}
