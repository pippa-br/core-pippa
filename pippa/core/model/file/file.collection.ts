/* CORE */
import { BaseList } from '../base.list';
import { File } from './file';

export class FileCollection extends BaseList<any>
{
    getMaps()
    {
        return {
            model : { klass : File },
            list  : { klass : FileCollection },
        };
    }

	hasSample(name:string)
	{
		for(const item of this)
		{
			if(item.sample == name)
			{
				return true;
			}			
		}

		return false;
	}

	delAllSamples()
	{
		for(const item of this)
		{
			if(item.isSample())
			{
				this.del(item);
			}			
		}
	}
}
