/* PIPPA */
import { BaseModel } from '../../model/base.model';

export class Payment extends BaseModel
{
    /*public id          : string;
    public url         : string;
    public orders_paid : number;*/

    isPaid()
    {
        return this['orders_paid'] >= 1;
    }
}
