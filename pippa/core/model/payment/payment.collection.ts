/* PIPPA */
import { BaseList }   from '../base.list';
import { Payment } from './payment';

export class PaymentCollection extends BaseList<Payment>
{
    getMaps()
    {
        return {
            model : { klass : Payment },
        };
    }
}
