// PIPPA
import { BaseModel2 } from './base.model2';
import { Core }       from '../util/core/core';

export class BaseList2<T> extends Array<T>
{
	public id : string;

    constructor(items?:any)
    {
        super();

		Object.defineProperty(this, "id", {writable: true, enumerable: false, configurable: true});

        Object.getOwnPropertyNames(this).forEach((prop) =>
        {
            if(prop == 'constructor' || prop == 'length')
            {
                return;
            }

            Object.defineProperty(this, prop,  { writable: true, enumerable: false, configurable: true });
        });

        this.initialize();

		this.populate(items);
    }

    getMaps():any
    {
        return {
            model : { klass : BaseModel2 },
            list  : { klass : BaseList2 },
        };
	}
	
	isEmpty()
	{
		return this.length == 0;
	}

    /* METODO UTILIZADO PARA INICIALIZAR VARIABLES */
    initialize()
    {
        this.id = this.core().util.randomString(20);        
    }

    populate(items:any)
    {
		this._setItems(items);        
    }

    parseItem(item:any):any
    {
        const maps = this.getMaps();
        const model : any = maps.model.klass;
        const list  : any = maps.list.klass;
        let   data  : any;

        if(item instanceof model)
        {
            data = item;
        }
        else if(item instanceof list)
        {
            data = item;
        }
        else
        {
            data = new model(item);
        }

        return data;
    }

    setItems(items:any):void
    {
        this._setItems(items);
    }

    set(item:any, index?:number):void
    {
        this._set(item, index);
    }

    _setItems(items:any):void
    {
        for(const key in items)
        {
            const item = items[key];
            this._set(item);
        }
    }

    _set(item:any, index?:number):void
    {
        let item2 = this.findItem(item);   
        
        if(item2)
        {
            item2.populate(item);
        }
        else
        {            
            item2 = this.parseItem(item);
            this.push(item2);
        }

        /* MOVE */
        if(index != undefined && index != -1)
        {
            let position = this.findPosition(item2);
            this._move(position, index);
        }
    }

    add(item:any):void
    {
        this._add(item);        
    }

    _add(item:any):void
    {
        const data = this.parseItem(item);
        this.push(data);
    }

    addItems(items:any):void
    {
        for(const key in items)
        {
            this._add(items[key])
        }
    }

    updateItems(items:any)
    {
        this._updateItems(items);
    }

    _updateItems(items:any)
    {
        this.splice(0, this.length);

		for(const key in items)
		{
			this._add(items[key])
		}
    }

    getById(id:number)
    {
        for(const key in this)
        {
            const item : any = this[key];

            if(item.id == id)
            {
                return this[key];
            }
        }
	}

	setItem(item:any):void
    {
        this.set(item);
    }
	
	delItem(item:any):void
    {
        this.del(item);
    }

    delItems(items:any):void
    {
        for(const key in items)
        {
            const item = items[key];
            this._del(item);
        }
    }

    del(item:any):boolean
    {
        const value = this._del(item);        
        return value;
    }

    /* PARA QUANDO UTILIZA O DELITEMS NÃO DISPARA VARIOS DISPATCHNEXT */
    _del(item:any):boolean
    {
        const position = this.findPosition(item);

        if(position > -1)
        {
            this.splice(position, 1);

            console.log('del', item.id, position, this.length);
            return true;
        }
        else
        {
            return false;
        }
    }

    move(from:any, to:any)
    {
        this._move(from, to);
    }

    _move(from:any, to:any)
    {
        if(from >= 0 && to >= 0 && from <= this.length - 1 && to <= this.length - 1)
        {
            this.splice(to, 0, this.splice(from, 1)[0]);
            console.log('move', from, to);
        }
    }

    /* PQ O LENGTH NÃO ESTAVA ATUALIZANDO */
    hasItems()
    {
        return this.length > 0;
    }

    has(item:any):boolean
    {
        return this.findPosition(item) != -1;
    }

    get(item:any):any
    {
        for(const key in this)
        {
            const item2 : any = this[key];

            if(item.id == item2.id)
            {
                return item2;
            }
        }

        return;
    }

    findPosition(item:any):number
    {
        let i = 0;

        for(const key in this)
        {
            const item2 : any = this[key];

            if(item.id == item2.id)
            {
                return i;
            }

            i++;
        }

        return -1;
    }

    findItem(item:any):any
    {
        for(const key in this)
        {
            const item2 : any = this[key];

            if(item.id == item2.id)
            {
                return item2;
            }
        }
    }

    copy()
    {
        const maps : any = this.getMaps();
        const list : any = new maps.list.klass;

        for(const key in this)
        {
            const item : any = this[key];
            list.push(item.copy());
        }

        return list;
    }

    getData():any
    {
        const items = [];

        for(const key in this)
        {
            const item : any = this[key];
			
			items.push(item.getData());
        }

        return items;
    }   

    async doReloadData()
    {
        // const promisses = [];

        // for(const key in this)
        // {
        //     const item : any = this[key];
        //     //promisses.push(item.reload());
        //     //promisses.push(item.on());
        //     //await item.reload();
        //     //await item.on();
        // }

        // await Promise.all(promisses);
    }

    core()
    {
        return Core.get();
    }
}
