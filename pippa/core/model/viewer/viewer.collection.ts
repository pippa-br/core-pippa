import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList }   from '../base.list';
import { Viewer } from './viewer';

export class ViewerCollection extends BaseList<Viewer>
{
    getMaps()
    {
        return {
            model : { klass : Viewer },
            list  : { klass : ViewerCollection },
        };
    }
}
