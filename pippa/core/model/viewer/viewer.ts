/* PIPPA */
import { BaseModel } from "../base.model";
import { FormItemCollection } from "../../model/form.item/form.item.collection";

export class Viewer extends BaseModel
{
    static FLAT_TYPE      = { value : "flat",      label : "Flat" };
    static SEGMENT_TYPE   = { value : "segment",   label : "Segmento" };
    static ACCORDION_TYPE = { value : "accordion", label : "Accordion" };
    static SLIDE_TYPE     = { value : "slide",     label : "Slide" };
    static INDEXES_TYPE   = { value : "Indexes",   label : "indexes", navBarLocation : "right", navigationMode : "allow" };

    //static TABLE_MODE = { value : 'table', label : 'Tabela'};
    //static LINE_MODE  = { value : 'line',  label : 'Linha'};

    /*public name        : string;
    public description : string;
    public items       : FormItemCollection;
    public type        : any;
    public mode        : any;
    public imageHeader : boolean;
    public imageFooter : boolean;
    public hasHeader   : boolean;*/
    
    /* VIEWER */
    public component : any;

    constructor(args?:any, fetch = false)
    {
        super(args, fetch);

        Object.defineProperty(this, "component", { writable : true, enumerable : false, configurable : true }); 
    }

    getMaps()
    {
        return {
            model : { klass : Viewer },
            items : { klass : FormItemCollection },
        };
    }
	
    collectionName()
    {
        return "viewer";
    }

    initialize()
    {
        this["type"] = Viewer.FLAT_TYPE;

        /*this.imageHeader = null;
        this.imageFooter = null;        
        this.hasHeader   = true;*/
    }

    on(fetch = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if (fetch)
                {
                    const promises = []

                    /* ITEMS */
                    if (this["items"] && this["items"].length > 0)
                    {
                        promises.push(this.getProperty("items", fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
