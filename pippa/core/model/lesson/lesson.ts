import { BaseModel2 } 		from '../base.model2';
import { User }      		from '../user/user';
import { Form }      		from '../form/form';
import { LessonCollection } from './lesson.collection';
import { Core }      		from '../../util/core/core';

export class Lesson extends BaseModel2
{	
	public minPercentage : number;
	public isFirst       : boolean;
	public isLast        : boolean;
	public hasSum        : boolean;
	public prev          : any;
	public next          : any;
	public _children     : any;

	constructor(args:any)
    {
        super(args);

		Object.defineProperty(this, "minPercentage", { writable: true, enumerable: false, configurable: true });
		Object.defineProperty(this, "isFirst", 		 { writable: true, enumerable: false, configurable: true });
		Object.defineProperty(this, "isLast", 		 { writable: true, enumerable: false, configurable: true });
		Object.defineProperty(this, "prev", 		 { writable: true, enumerable: false, configurable: true });
		Object.defineProperty(this, "next", 		 { writable: true, enumerable: false, configurable: true });
		Object.defineProperty(this, "_children",  	 { writable: true, enumerable: false, configurable: true });
    }

    getMaps()
    {
        return {
            form      : { klass : Form },
			owner     : { klass : User },
			model     : { klass : Lesson },
			_children : { klass : LessonCollection },
			prev 	  : { klass : Lesson },
			next 	  : { klass : Lesson }
        };
    }

    initialize()
    {
		this.minPercentage = ( this.core().account.minPercentage != undefined ? this.core().account.minPercentage : 80);
		this['percentage'] = -1;
		this['isFirst']    = false;
		this['isLast']     = false;
		this['hasSum']     = false;
	}

	get actived()
	{
		return this['percentage'] >= this.minPercentage || !this['hasSum'];
	}

	get current()
	{
		return (this['percentage'] < this.minPercentage && this.isFirst) || (this['percentage'] < this.minPercentage && this.prev && this.prev.actived);
	}
}
