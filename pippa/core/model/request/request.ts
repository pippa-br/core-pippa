import { BaseModel }      	 from '../base.model';
import { User }            	 from '../user/user';
import { Form }           	 from '../form/form';
import { RequestCollection } from '../request/request.collection';

export class Request extends BaseModel
{
    getMaps()
    {
        return {
            model  		: { klass : Request },
            form   		: { klass : Form },
			items  		: { klass : RequestCollection },
			deliveryman : { klass : User },
        };
    }

    initialize()
    {
        this['items'] = new RequestCollection();
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* ITEMS */
                    if(this['items'] && this['items'].length > 0)
                    {
                        promises.push(this.getProperty('items', fetch));
					}
					
					/* deliveryman */
                    if(this['deliveryman'])
                    {
                        promises.push(this.getProperty('deliveryman', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    });
                }
                else
                {
                    resolve(this);
                }
            });
        });
	}
	
	getPaymentStatusLabel()
	{
		if(this['payment'])
		{
			if(this['payment'].status == 'paid')
			{
				return 'Pago'
			}
			else if(this['payment'].status == 'refused')
			{
				return 'Recusado';
			}
			else
			{
				return 'Aguardando';
			}
		}
	}

	getPaymentStatus()
	{
		if(this['payment'])
		{
			return this['payment'].status;
		}
	}

	getDeliveryStatusLabel()
	{
		if(this['deliveryStatus'])
		{
			if(this['deliveryStatus'].value == 'delivered')
			{
				return 'Entregue'
			}
			else if(this['deliveryStatus'].value == 'way')
			{
				return 'A Caminho';
			}
			else if(this['deliveryStatus'].value == 'preparation')
			{
				return 'Pedido Liberado';
			}
			else
			{
				return 'Novo';
			}
		}
	}

	getDeliveryStatus()
	{
		if(this['deliveryStatus'])
		{
			return this['deliveryStatus'].value;
		}
	}

	isNew()
	{
		if(this['deliveryStatus'])
		{
			if(this['deliveryStatus'].value == 'new')
			{
				return true;
			}			
		}

		return false;
	}

	isPreparation()
	{
		if(this['deliveryStatus'])
		{
			if(this['deliveryStatus'].value == 'preparation')
			{
				return true;
			}			
		}

		return false;
	}

	isWay()
	{
		if(this['deliveryStatus'])
		{
			if(this['deliveryStatus'].value == 'way')
			{
				return true;
			}			
		}

		return false;
	}

	isDelivered()
	{
		if(this['deliveryStatus'])
		{
			if(this['deliveryStatus'].value == 'delivered')
			{
				return true;
			}			
		}

		return false;
	}

	isPaid()
	{
		if(this['payment'])
		{
			if(this['payment'].status == 'paid')
			{
				return true;
			}			
		}

		return false;
	}

	isRefused()
	{
		if(this['payment'])
		{
			if(this['payment'].status == 'refused')
			{
				return true;
			}			
		}

		return false;
	}
}
