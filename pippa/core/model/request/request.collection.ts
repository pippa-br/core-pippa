import { Request } 	from './request';
import { BaseList } from '../base.list';

export class RequestCollection extends BaseList<Request>
{
    getMaps():any
    {
        return {
            model : { klass : Request },
            list  : { klass : RequestCollection },
        };
    }
}
