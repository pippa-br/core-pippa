import { Directive } from "@angular/core";
import { DocumentReference, doc, updateDoc, setDoc, getDoc } from "firebase/firestore";
import { Subject } from "rxjs";

/* PIPPA */
import { Core } from "../util/core/core";
import { FunctionsType } from "../type/functions/functions.type";
import { TDate } from "../util/tdate/tdate";
@Directive()
export class BaseModel extends Object
{
    //public id : string;

    /* CONTROL  */
    //public _data             : any;
    public unsubscribe       : any;
    public inversedBy        : any;
    public initInversedBy    : any;
    public _onResolver       : any; /* EVENTO PARA ABRIR DOCUMENT_REFERENCE */
    public _promise          : any;    /* USADO PARA VARIAS CHAMDAS SIMUTANIAS DO ON, RETORNA O MESMO PROMISE */
    public _isPopulate       : boolean;
    public _reference        : any;
    public _active           : boolean;
    public exists            : boolean;
    public _documentSnapshot : any; // ISSO SERVER PARA PAGINACAO, POIS NAO EH FEITO POR REFERENCE
    public _onUpdate         : any = new Subject();
    public fetch             : boolean;
    public _isRenderer       : boolean;    
    public _maps             : any;
    public _onPropertyUpdate : any;
    public referencePath     : any;

    constructor(args?:any, fetch = false)
    {
        super();
        
        //Object.defineProperty(this, "_data",             { writable: true, enumerable: false, configurable: true });         
        Object.defineProperty(this, "unsubscribe",       { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "inversedBy",        { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "initInversedBy",    { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_onResolver",       { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_promise",          { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_isPopulate",       { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_reference",        { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_active",           { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "exists",            { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_documentSnapshot", { writable : true, enumerable : false, configurable : true });  
        Object.defineProperty(this, "_onUpdate",         { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "fetch",             { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_isRenderer",       { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_maps",             { writable : true, enumerable : false, configurable : true });    
        Object.defineProperty(this, "_onPropertyUpdate", { writable : true, enumerable : false, configurable : true });    

        this.initialize();

        this["id"]             = this.core().util.randomString(20);
        this["colid"]          = "documents";
        this.exists            = true;         
        this.fetch             = fetch;
        this._maps             = {};
        this._onPropertyUpdate = [];

        if (args && Object.keys(args).length == 1 && args.referencePath)
        {
            this.reference = doc(this.core().fireStore, args.referencePath);
            //this.exists 	= this.reference.exists();
        }
        else
        {
            this.populate(args);
        }
    }

    initialize()
    {         
        // OVERRIDER
    }

    onPropertyUpdate(value:string)
    {
        if (!this._onPropertyUpdate[value])
        {
            this._onPropertyUpdate[value] = new Subject();
        }

        return this._onPropertyUpdate[value].asObservable();
    }

    dispatchPropertyUpdate(value:string)
    {
        if (this._onPropertyUpdate[value])
        {
            this._onPropertyUpdate[value].next(this);
        }		
    }

    onUpdate()
    {
        return this._onUpdate.asObservable();
    }

    dispatchUpdate()
    {
        this._onUpdate.next(this);
    }

    getMaps()
    {
        const maps : any = Object.assign({}, {
            model : { 
                klass : BaseModel
            }
        }, this._maps);

        return maps;
    }

    core()
    {
        return Core.get();
    }

    set reference(ref:any)
    {
        this._reference = ref;

        if (ref && !this["id"])
        {
            this["id"] = ref.id;
        }
    }

    get reference()
    {
        if (!this._reference && this.referencePath)
        {
            this._reference = doc(this.core().fireStore, this.referencePath);

            if (!this["id"])
            {
                this["id"] = this._reference.id;
            }            
        }		

        return this._reference;
    }

    on(fetch = false, maps?:any):Promise<BaseModel>
    {
        this.fetch = fetch;

        if (this._promise)
        {
            return this._promise;
        }

        this._promise = new Promise<BaseModel>((resolve) =>
        {
            this._onResolver = resolve;

            if (this._isPopulate)
            {
                this.onMaps(maps, fetch).then(() => 
                {
                    resolve(this);
                })
            }
            else
            {                
                if (this.reference)
                {
                    this.doSnapshot().then(() =>
                    {											
                        this.onMaps(maps, fetch).then(() => 
                        {
                            resolve(this);
                        })
                    });
                }
                else
                {
                    resolve(this);
                }
            }
        });

        return this._promise;
    }

    onMaps(maps:any, fetch:boolean)
    {
        return new Promise<void>((resolve) => 
        {
            if (maps)
            {
                this._maps     = maps;
                const promises = [];

                for (const key in maps)
                {
                    promises.push(this.getProperty(key, fetch));
                }
    
                Promise.all(promises).then(() => 
                {
                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    }

    async reload()
    {
        this._isPopulate = false;
        await this.doSnapshot();
        await this.on();
    }

    doSnapshot()
    {
        return new Promise(async (resolve:any) =>
        {
            if (this.reference && !this._isPopulate)
            {
                this._isPopulate = true;

                //var startTime = new TDate();

                const docSnap = await getDoc(this.reference);
                this.exists   = docSnap.exists();

                if (this.exists) 
                {
                    //console.log("Document data:", docSnap.data());

                    this.doUnsubscribe();

                    this._documentSnapshot = docSnap;

                    const data : any = docSnap.data();                    

                    /* QUANDO TEM UMA REFERENCIA MAS O OBJETO FOI REMOVIDO */
                    if (data)
                    {
                        data.id = docSnap.id;
                        //console.log('reload subscribe', data);
                        this.populate(data);
                    }
                    else
                    {
                        this.populate({});
                    }

                    resolve();
                } 
                else 
                {
                    resolve();
                }

                // this.unsubscribe = docRx(this.reference).subscribe((doc:any) =>
                // {
                //     //var endTime = new TDate();

                //     // var msElapsedTime = endTime.diff(startTime, 'milliseconds');

                // 	// console.log('XXX', msElapsedTime, this.reference.path);
					
                // 	this.doUnsubscribe();

                //     this._documentSnapshot = doc;

                //     const data  = doc.data();
                //     this.exists = doc.exists();

                //     /* QUANDO TEM UMA REFERENCIA MAS O OBJETO FOI REMOVIDO */
                //     if(data)
                //     {
                //         data.id = doc.id;
                //         //console.log('reload subscribe', data);
                //         this.populate(data);
                //     }
                //     else
                //     {
                //         this.populate({});
                //     }

                //     resolve();
                // });
            }
            else
            {
                resolve();
            }
        });
    }

    populate(data:any)
    {
        if (data)
        {
            if (data instanceof DocumentReference)
            {
                this._isPopulate = false;
                this.reference   = data;
            }
            else
            {
                this._isPopulate = true;
                const maps       = this.getMaps();

                /* CASO TENTE POPULAR COM O MESMO TIPO DE OBJETO */
                if (typeof data.getData == "function")
                {   
                    this.reference = data.reference; // COPIA TAMBÉM A REFERENCIA
                    data           = data.getData();
                }

                for (const key in data)
                {
                    const value : any = data[key];
                    const map   : any = maps[key];

                    if (value != undefined && map && map.klass && typeof value == "object" && !(value instanceof map.klass))
                    {
                        this[key] = new map.klass(value);
                    }
                    else if (value != undefined && map && map.klass && typeof value == "object" && (this[key] instanceof map.klass))
                    {                    
                        this[key].populate(value);                    
                    }
                    else
                    {
                        this[key] = value;
                    }                                        
                }                 
                
                if (this._onResolver)
                {
                    this._onResolver(this);

                    /* PARA NÃO SER UM SUBSCRIBER */
                    this._onResolver = null;
                    this._promise    = null;
                }

                if (this._active == undefined)
                {
                    this._active = true;
                }

                this.dispatchUpdate();
            }
        }                
    }

    parseData()
    {
        const data : any = {};
        const maps : any = this.getMaps();
        //const keys = Array.from(new Set(Object.keys(this._data || {}).concat(Object.keys(maps))))

        /* UTILIZAR O _DATA PARA PEGAR OS KEYS */
        for (const key in this)
        { 
            const field = key.replace("$", "");
            data[field] = this[field];            

            if (data[field] === undefined)
            {
                delete data[field];
            }
            else if (data[field] === null)
            {
                //DEIXA OS CAMPO NULL
            }
            else if (data[field].reference)
            {
                data[field] = data[field].reference;
            }
            else if (typeof data[field].parseData == "function")
            {
                data[field] = data[field].parseData();
            }
            else if (Array.isArray(data[field]))
            {
                const _data = [];

                for (const key2 in data[field])
                {
                    if (!data[field][key2])
                    {
                        // NAO FAZ NADA
                    }
                    else if (data[field][key2].reference)
                    {
                        _data.push(data[field][key2].reference);
                    }
                    else if (typeof data[field].parseData == "function")
                    {
                        _data.push(data[field].parseData());
                    }
                    else
                    {
                        _data.push(data[field][key2]);
                    }
                }

                data[field] = _data;
            }
        }
		
        delete data._documentSnapshot;

        return data;
    }

    /* PEGA TODOS OS CAMPOS SEM TER QUE ABRIR NOVAMENTE */
    getData()
    {
        const data : any = {};
        const maps : any = this.getMaps();
        //const keys = Array.from(new Set(Object.keys(this._data || {}).concat(Object.keys(maps))))

        for (const key in this)
        {
            const field = key.replace("$", "");
            data[field] = this[field];

            if (data[field] === undefined)
            {
                delete data[field];
            }
            else if (data[field] === null)
            {
                //DEIXA OS CAMPO NULL
            }
            else if (typeof data[field].getData == "function")
            {
                data[field] = data[field].getData();
            }
            else if (Array.isArray(data[field]))
            {
                const _data = [];

                for (const key2 in data[field])
                {
                    if (typeof data[field].getData == "function")
                    {
                        _data.push(data[field].getData());
                    }
                    else
                    {
                        _data.push(data[field][key2]);
                    }
                }

                data[field] = _data;
            }
        }    
        
        /* NO SELECT REFERENE PRECISA */
        if (this.reference)
        {
            data.reference = this.reference
        }

        return data;
    }

    getProperty(name:string, fetch = false):Promise<any>
    {
        return new Promise((resolve) =>
        {
            this.getPathValue(name, fetch).then((data:any) =>
            {
                //this[name] = data;

                resolve(data);
            });
        });
    }

    getPathValue(path:string, fetch = false)
    {
        return new Promise<any>((resolve) =>
        {
            if (!path)
            {
                resolve("");
            }
            else
            {
                if (path == "this")
                {
                    resolve(this);
                }
                else
                {
                    //path = path.replace(/\s+/g, ''); /* REMOVE ALL SPACES */

                    const promises = [];

                    let variables : any = path.match(/\${([^{][^}]+)}/g);
                    const funcs         = path.match(/([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/g);
                    let isVariables     = false;

                    if (!variables)
                    {
                        variables = path.split(",");
                    }
                    else
                    {
                        isVariables = true;
                    }

                    for (const key in variables)
                    {
                        let subPath = variables[key];
                        let item    = this;

                        if (subPath.indexOf("loggedUser") > -1)
                        {
                            subPath = subPath.replace("loggedUser.", "");
                            item    = this.core().user;
                        }

                        subPath = subPath.replace("${", "").replace("}", "");
                        //subPath = subPath.replace(/\s+/g, ''); /* REMOVE ESPACOS EM BRANCO */
                        const paths = subPath.split(".");

                        promises.push(this._getPathValue(item, paths, fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {
                        if (!isVariables)
                        {
                            const pipes = path.split("|"); /* PIPE EXTERNO */

                            if (values && values.length > 0)
                            {
                                if (values.length == 1)
                                {
                                    resolve(values[0]);
                                }
                                else
                                {
                                    resolve(values);
                                }
                            }
                            else
                            {
                                resolve("");
                            }
                        }
                        else
                        {
                            path         = path.replace(/\|\|/ig, "[OR]"); // PARA NAO CONFUNDIR COM PIPE
                            const pipes  = path.split("|"); /* PIPE EXTERNO */
                            path         = pipes[0];
                            const pipe   = (pipes[1] ? pipes[1] : null);
                            const isCalc = /(?!^-)[+*\/-](\s?-)?/.test(path);
                            let hasFunc  = false;		
                            path         = path.replace(/\[OR\]/ig, "||"); // PARA NAO CONFUNDIR COM PIPE	
                            
                            // EXC
                            const exc : any = {
                                eval : (expression:any, scope:any) =>
                                {
                                    try
                                    {
                                        return Function("\"use strict\";return (" + expression + ")").bind(scope)();
                                    }
                                    catch (e)
                                    {
                                        console.error(expression, variables, scope);
                                        console.error(e);
                                        
                                        return (isCalc ? 0 : "")
                                    }
                                },
                                calc : (...args:any) => 
                                {
                                    let value = eval(args[0]);

                                    if (typeof value == "boolean")
                                    {
                                        return value;
                                    }

                                    value = +Number(value).toFixed(2);

                                    return value;
                                },
                                object : async (...args:any) => 
                                {
                                    return args[0];
                                },
                                notNull : (...args:any) => 
                                {
                                    for (const value of args)
                                    {
                                        if (value !== "" && value !== undefined && value !== null)
                                        {
                                            return value
                                        }
                                    }
                                },
                                concat : (...args:any) => 
                                {
                                    let value       = "";
                                    const separator = " - ";
                                    
                                    for (const key in args)
                                    {
                                        if (args[key])
                                        {
                                            value += args[key] + (parseInt(key) < args.length - 1 && args[key] ? separator : "");
                                        }
                                    }

                                    return value;
                                },
                                currency : (...args:any) => 
                                {
                                    let precision = 2;

                                    if (args.length == 2)
                                    {
                                        precision = args[1]
                                    }

                                    return "R$ " + args[0].toLocaleString("pt-BR", { minimumFractionDigits : precision })
                                },
                                address : (...args:any) => 
                                {
                                    if (args[0])
                                    {
                                        return args[0].street + ", " + args[0].housenumber + " " + args[0].complement
                                        + ", " + args[0].district + " - " + args[0].city + " / " + args[0].state + " - "
                                        + args[0].zipcode;
                                    }

                                    return "";
                                },
                                join : (...args:any) => 
                                {                                    
                                    const separator = args.pop();
                                    const value     = args.join(separator);

                                    return value;
                                },
                                whatsapp : (...args:any) => 
                                {
                                    let phone = args[0];
                                    phone     = "https://api.whatsapp.com/send?phone=55" + phone.replace(/[\(\)\.\s-]+/g, "").replace("+55", "");

                                    return "<a href=\"" + phone + "\" target=\"_blank\">" + args[0] + "</a>";
                                },
                                whatsappUrl : (...args:any) => 
                                {
                                    let phone = args[0];
                                    phone     = "https://api.whatsapp.com/send?phone=55" + phone.replace(/[\(\)\.\s-]+/g, "").replace("+55", "");

                                    return phone;
                                },
                                func : (...args:any) => 
                                {
                                    if (args && args.length > 0)
                                    {
                                        const func = FunctionsType.get().getFunction(args[0]);

                                        return func(args[1]);
                                    }                                    
                                },
                            }
                            
                            // FUNCTIONS
                            if (funcs)
                            {
                                for (const func of funcs)
                                {
                                    if (exc[func])
                                    {
                                        //path    = path.replaceAll(func + "(", "this." + func + "(");
                                        path    = path.replaceAll(func + "(", "this." + func + "(").replace(new RegExp("~", "g"), ",");
                                        hasFunc = true;
                                    }				
                                }
                            }

                            for (const key in values)
                            {
                                const value    = values[key];
                                const variable = variables[key];
				                const subPath  = variable.replace("${", "").replace("}", "").replace(/\./g, "_");

                                if (hasFunc)
                                {                                    
                                    //path = path.replace(variable, value ? value : 0);
                                    path = path.replaceAll(variable, "this._" + subPath + "");                                    
                                }
                                else
                                {
                                    path = path.replaceAll(variables[key], value != undefined ? value : "");
                                }

                                exc["_" + subPath] = value != undefined ? value : "";
                            }

                            if (hasFunc)
                            {                                  
                                resolve(exc.eval(path, exc));
                            }
                            else if (pipe == "calc") // TROCAR POR FUNC POIS TEM controlPath com ('${id}' != '')|calc
                            {
                                resolve(eval(path));
                            }
                            else if (pipe == "currency") // TROCAR POR FUNC
                            {
                                resolve(this.core().util.currencyFormat(path));
                            }
                            else if (pipe == "addressFormat") // TROCAR POR FUNC
                            {
                                resolve(this.core().util.addressFormat(values));
                            }
                            else
                            {
                                resolve(path);
                            }
                        }
                    });
                }
            }
        });
    }

    _getPathValue(data:any, paths:Array<string>, fetch = false):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if (paths.length == 0)
            {
                resolve(data);
            }
            else
            {
                const maps    = this.getMaps();
                const keyPath = paths.shift();

                if (data == undefined)
                {
                    resolve("");
                }
                else if (keyPath.indexOf("->") >= 0) // REFERENCIA AND COLLECTION
                {
                    const subPath   = data.reference.path + "/" + keyPath.replace(/->/ig, "/");
                    let value : any = doc(this.core().fireStore, subPath);

                    const classMap = maps.model.klass;        
                    value          = new classMap(value);
                    
                    value.on(fetch).then((value2:any) =>
                    {
                        this._getPathValue(value2, paths).then(data2 =>
                        {
                            resolve(data2);
                        });
                    });
                }
                else
                {
                    let value = data[keyPath];
					
                    /* CONVERT TIMESTAP DATA */
                    if (value && typeof value.toDate == "function")
                    {
                        value = value.toDate();
                    }
					
                    if (value === undefined)
                    {
                        resolve("");
                    }				
                    /* IS PRIMITIVE */	
                    else if (typeof value != "object" && typeof value != "function")
                    {
                        resolve(value);
                    }
                    /* CLASS MAP, INSTANCIA */
                    else if (maps[keyPath] && value instanceof maps[keyPath].klass)
                    {
                        value.on(fetch).then(value2 =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
                    }
                    /* CLASS MAP E NÃO INSTANCIA */
                    else if (maps[keyPath] && !(value instanceof maps[keyPath].klass))
                    {
                        const classMap = maps[keyPath].klass;        
                        value          = new classMap(value);
                        
                        value.on(fetch).then((value2:any) =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
                    }
                    /* DOCUMENT REFERENCE */
                    else if (value instanceof DocumentReference && keyPath != "reference")
                    {
                        const classMap = maps.model.klass;        
                        value          = new classMap(value);
                        
                        value.on(fetch).then((value2:any) =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
                    }
                    /* ARRAY */
                    else if (value instanceof Array)
                    {
                        const promises = [];
						  let value2;						
						
                        for (const key in value)
                        {
                            if (value[key] instanceof DocumentReference)
                            {
                                const classMap = maps.model.klass;        
                                value2         = new classMap(value[key]);
                                value2         = value2.on();
                            }
                            else if (value[key].referencePath)
                            {
                                const classMap = maps.model.klass;        
                                value2         = new classMap(value[key]);
                                value2         = value2.on();
                            }
                            else
                            {
                                value2 = new Promise((resolve) =>
                                {
                                    resolve(value[key]);
                                })
                            }	
							
                            promises.push(value2);
                        }
                        
                        Promise.all(promises).then(async (values) => 
                        {							
                            const _paths = paths.slice(); // COPIA PARA PODER VERIFICAR A PROXIMA CHAVE
                            const key    = _paths.shift();

                            if (key == "last")
                            {
                                if (values.length > 0)
                                {
                                    const item = values[values.length - 1];

                                    this._getPathValue(item, _paths.slice()).then((data2) => 
                                    {
                                        resolve(data2);
                                    })
                                }
                                else
                                {
                                    resolve(null);
                                }
                            }
                            else if (key == "first")
                            {
                                if (values.length > 0)
                                {
                                    const item = values[0];

                                    this._getPathValue(item, _paths.slice()).then((data2) => 
                                    {
                                        resolve(data2);
                                    })
                                }
                                else
                                {
                                    resolve(null);
                                }
                            }
                            else if (key == "find")
                            {
                                if (values.length > 0)
                                {
							        let key2 = _paths.shift();
                                    key2     = key2.replace(/->/ig, ".");   
                                    let has  = false;
                                    
                                    for (const item2 of values)
                                    {   
                                        const model = new BaseModel(item2);
                                        const value = await model.getProperty(key2)

                                        if (value)
                                        {
                                            resolve(item2);
                                            has = true;
                                            break;
                                        }
                                    }

                                    if (!has)
                                    {
                                        resolve(null);
                                    }                                    
                                }
                                else
                                {
                                    resolve(null);
                                }
                            }
                            else if (key == "length")
                            {
                                resolve(values.length);
                            }
                            else
                            {
                                const promises = [];

                                for (const key in values)
                                {
                                    // SLICE COPIA O ARRAY POR CAUSA DA REFERENCIA
                                    promises.push(this._getPathValue(values[key], paths.slice()));
                                }
	
                                Promise.all(promises).then((values2) => 
                                {
                                    resolve(values2);
                                });
                            }
                        });
                    }
                    /* DATE */
                    else if (value && value instanceof Date && paths.length > 0)
                    {
                        const key      = paths.pop();
						  let date : any = value;
												
                        date = new TDate({ value : date });

                        if (key == "day")
                        {
                            resolve(date.format("dd"));
                        }
                        else if (key == "month")
                        {
                            resolve(date.format("MM"));
                        }
                        else if (key == "year")
                        {
                            resolve(date.format("yyyy"));
                        }
                        else if (key == "date")
                        {
                            resolve(date.format("dd/MM/yyyy"));
                        }
                        else if (key == "datetime")
                        {
                            resolve(date.format("dd/MM/yyyy HH:mm"));
                        }
                        else if (key == "week")
                        {
                            resolve(date.format("dddd"));
                        }
                        else
                        {
                            resolve(date.format("yyyy-MM-dd"));
                        }
                    }
                    else
                    {                      
                        this._getPathValue(value, paths).then(data2 =>
                        {
                            resolve(data2);
                        });
                    }
                }
            }
        });
    }

    doUnsubscribe()
    {
        if (this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
    }

    collectionName()
    {		
        return this["colid"];
    }

    appidName()
    {
        return this["appid"];
    }

    accidName()
    {
        return this["accid"];
    }

    /* CRIA OU RESET DATA */
    create(data:any)
    {	
        return new Promise((resolve) =>
        {
            if (!this.reference)
            {
                const referencePath = this.accidName() + "/" + this.appidName() + "/" + this.collectionName() + "/" + this["id"];
                this.reference      = doc(this.core().fireStore, referencePath);		
            }

            /* SAVE ID */
            data.id = this["id"];
			
            setDoc(this.reference, data);
            this.populate(data);

            console.log("create", data);

            this.exists = true;

            this.createIndexes().then(() =>
            {
                resolve(this);				
            });
        });
    }

    /* ONLY SAVE */
    save(data:any)
    {
        return new Promise((resolve) =>
        {
            /* CRIAR CASO O OBJETO NAO EXISTA */
            if (this.reference && data)
            {
                /* SAVE ID */
                data.id = this["id"];
				
                updateDoc(this.reference, data).then(() =>
                {
                    this.populate(data);

                    console.log("save", data);
					
                    this.createIndexes().then((event) =>
                    {
                        resolve(this);	
                    });
                });
            }
            else
            {
                this.populate(data);
				
                console.error("object not save");

                resolve(this);

                //this.create(data).then(() =>
                //{
                    
                //});
            }
        });
    }

    set(data:any)
    {
        return new Promise((resolve) =>
        {
            console.log("set", data);

            if (this.reference)
            {
                this.save(data).then(() =>
                {            
                    /* DISPATCH EVENT */
                    for (const key in data)
                    {
                        this.dispatchPropertyUpdate(key);
                    }			

                    resolve(this);
                });
            }
            else
            {
                this.populate(data);
				
                /* DISPATCH EVENT */
                for (const key in data)
                {
                    this.dispatchPropertyUpdate(key);
                }			
				
                resolve(this);
            }					
        });
    }

    /* UTILIZADO PARA ATUALIZAR O OBJETO, APROVEITAR A REFERENCIA E DISPARA O EVENTO ONCHANGE*/
    update(reference:any)
    {
        this.clear();
        this.reference = reference;
    }

    clear()
    {
        this.doUnsubscribe();

        //const maps : any = this.getMaps();
        //const keys = Array.from(new Set(Object.keys(this._data || {}).concat(Object.keys(maps))))

        for (const field in this)
        {
            delete this[field];
        }
    }

    del()
    {
        this.doUnsubscribe();
        this.reference.delete();
    }
    
    isOwner():boolean
    {
        /* PEGA NO _DATA PARA NÃO ABIR A REFERENCE */
        if (this.core().user)
        {
            /* NÃO ABRIR OWNER, POIS PRECISA DO RETORNO IMEDIATO */
            if (this.core().user.isMaster() || (this["owner"] && this.core().user.reference && this.core().user.reference.id == this["owner"].id))
            {
                return true;
            }
        }

        return false;
    }

    createIndexes()
    {
        return new Promise<any>((resolve) =>
        {
            this.getProperty("form").then(async (form) => 
            {
                if (form)
                {
                    /* INDICES */
                    if (form.indexes)
                    {
                        const indexes = {};

                        for (const key in form.indexes)
                        {
                            let value = await this.getProperty(form.indexes[key].path);

                            if (value === "") // getProperty sempre retorna '' quando não encontra
                            {
                                value = form.indexes[key].default;
                            }

                            indexes[form.indexes[key].key] = value;
                        }

                        if (Object.keys(indexes).length)
                        {
                            console.log("save indexes", indexes);

                            await this.reference.update({
                                indexes : indexes
                            });
                        }
                    }

                    /* SEARCH */
                    if (form._search)
                    {
                        const search = [];

                        for (const key in form._search)
                        {
                            let fullWord = await this.getProperty(form._search[key].path);

                            if (fullWord)
                            {
                                if (fullWord instanceof Array)
                                {
                                    fullWord = fullWord.join(" ");
                                }

                                fullWord = "" + fullWord; // CONVERT EM STRING

                                if (!form._search[key].case)
                                {
                                    fullWord = fullWord.toLowerCase();
                                    fullWord = await this.core().util.removeAccents(fullWord);	
                                }

                                if (form._search[key].full)
                                {
                                    search.push(fullWord);
                                }
                                else
                                {
                                    const word = fullWord.split(" ");

                                    word.push(fullWord);
			
                                    for (const subkey in word)
                                    {
                                        const text = word[subkey];
			
                                        for (let i = 0; i < text.length; i++)
                                        {
                                            const substring = word[subkey].substring(0, text.length - i);
			
                                            if (search.indexOf(substring) == -1 && substring.length >= 2)
                                            {
                                                search.push(substring);
                                            }
                                        }
                                    }
                                }						
                            }				
                        }

                        if (search.length > 0)
                        {
                            console.log("save search", search);
						
                            await this.reference.update({
                                search : search
                            });
                        }
                    }									
                }

                resolve(null);
            });
        });
    }
    
    copy()
    {
        const classMap : any = this.getMaps();
        const model    : any = new classMap.model.klass;

        model._reference        = this._reference; /* PARA NÃO PRECISAR FAZER subscribe */
        model.id                = this["id"];
        model.exists            = this.exists;
        model._documentSnapshot = this._documentSnapshot;
        model.populate(this);

        return model;
    }

    updateName()
    {
        if (this["_pathName"])
        {
            this.getPathValue(this["_pathName"]).then((value:any) =>
            {
                this.set({
                    name : value
                })
            })
        }
        else if (this["name"])
        {
            /* NAO FAZ NADA */
        }
    }

    getAttachments(name:string)
    {
        if (this["attachments"] && this["attachments"][name])
        {
            return this["attachments"][name];
        }
    }

    isLock()
    {
        return this["_lock"] && this["_lock"] == "mobile";
    }

    isArchive()
    {
        return this["_archive"];
    }

    isCanceled()
    {
        return this["_canceled"];
    }
}
