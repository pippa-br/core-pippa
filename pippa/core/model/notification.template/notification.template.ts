/* PIPPA */
import { BaseModel } from '../base.model';
import { Form }      from '../form/form';
import { App }       from '../app/app';

export class NotificationTemplate extends BaseModel
{    	
    getMaps()
    {
        return {
            model : { klass : NotificationTemplate },
			form  : { klass : Form },
			app   : { klass : App },
        };
	}
	
	on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch) 
                {
                    const promises = []
					
					if(this['app'])
                    {
                        promises.push(this.getProperty('app', fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {                        
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
