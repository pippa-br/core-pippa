/* PIPPA */
import { BaseList } 			from '../base.list';
import { NotificationTemplate } from './notification.template';

export class NotificationTemplateCollection extends BaseList<Notification>
{
    getMaps()
    {
        return {
            model : { klass : NotificationTemplate },
            list  : { klass : NotificationTemplateCollection },
        };
    }    
}
