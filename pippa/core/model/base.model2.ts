import { Directive } from '@angular/core';
import { DocumentReference } from 'firebase/firestore';

/* PIPPA */
import { Core } from '../util/core/core';
import { TDate } from '../util/tdate/tdate';

@Directive()
export class BaseModel2 extends Object
{
	[key: string] : any;

    constructor(data:any)
    {
        super();
        
         this.initialize();

		 this['id']    = this.core().util.randomString(20);
		 this['colid'] = 'documents';
		 
		 if(data)
		 {
			this.populate(data);
		 }				 
     }

    initialize()
    {         
	}

    populate(data:any)
    {
		const maps = this.getMaps();

        for(const key in data)
		{
			if(maps[key])
			{
				this[key] = new maps[key].klass(data[key]);
			}
			else
			{
				this[key] = data[key];
			}			
		}
    }

	getProperty(name:string, fetch:boolean = false):Promise<any>
    {
        return new Promise((resolve) =>
        {
            this.getPathValue(name, fetch).then((data:any) =>
            {
                //this[name] = data;

                resolve(data);
            });
        });
    }

    getPathValue(path:string, fetch:boolean = false)
    {
        return new Promise<any>((resolve) =>
        {
            if(!path)
            {
                resolve('');
            }
            else
            {
                if(path == 'this')
                {
                    resolve(this);
                }
                else
                {
                    //path = path.replace(/\s+/g, ''); /* REMOVE ALL SPACES */

                    const promises  = [];

                    let variables : any = path.match(/\${([^{][^}]+)}/g);
                    let isVariables = false;

                    if(!variables)
                    {
                        variables = path.split(',');
                    }
                    else
                    {
                        isVariables = true;
                    }

                    for(const key in variables)
                    {
                        let subPath = variables[key];
                        let item    = this;

                        if(subPath.indexOf('loggedUser') > -1)
                        {
                            subPath = subPath.replace('loggedUser.', '');
                            item = this.core().user;
                        }

                        subPath = subPath.replace("${","").replace("}","");
                        //subPath = subPath.replace(/\s+/g, ''); /* REMOVE ESPACOS EM BRANCO */
                        const paths = subPath.split('.');

                        promises.push(this._getPathValue(item, paths, fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {
                        if(!isVariables)
                        {
                            if(values && values.length > 0)
                            {
								if(values.length == 1)
								{
									resolve(values[0]);
								}
								else
								{
									resolve(values);
								}
                            }
                            else
                            {
                                resolve('');
                            }
                        }
                        else
                        {
							const pipes  = path.split('|'); /* PIPE EXTERNO */
                            path         = pipes[0];
							const pipe   = (pipes[1] ? pipes[1] : null);
							const isCalc = pipe && pipe == 'calc';

                            for(const key in values)
                            {
                                let value = values[key];
                                path = path.replace(variables[key], (value != undefined ? value : (isCalc ? 0 : '')));
                            }

                            if(isCalc)
                            {
                                resolve(eval(path));
                            }
                            else
                            {
                                resolve(path);
                            }
                        }
                    });
                }
            }
        });
    }

    _getPathValue(data:any, paths:Array<string>, fetch:boolean = false):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if(paths.length == 0)
            {
                resolve(data);
            }
            else
            {
                const maps    = this.getMaps();
                const keyPath = paths.shift();

                if(data == undefined)
                {
                    resolve('');
                }
                else
                {
					let value = data[keyPath];
					
					/* CONVERT TIMESTAP DATA */
					if(value && typeof value.toDate == 'function')
					{
						value = value.toDate();
					}
					
                    if(value === undefined)
                    {
                        resolve('');
					}				
					/* IS PRIMITIVE */	
					else if(typeof value != "object" && typeof value != "function")
					{
						resolve(value);
					}
                    /* CLASS MAP, INSTANCIA */
                    else if(maps[keyPath] && value instanceof maps[keyPath].klass)
                    {
                        value.on(fetch).then(value2 =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
                    }
                    /* CLASS MAP E NÃO INSTANCIA */
                    else if(maps[keyPath] && !(value instanceof maps[keyPath].klass))
                    {
                        const classMap = maps[keyPath].klass;        
                        value = new classMap(value);
                        
                        value.on(fetch).then((value2:any) =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
					}
					/* DOCUMENT REFERENCE */
                    else if(value instanceof DocumentReference && keyPath != 'reference')
                    {
                        const classMap = maps.model.klass;        
                        value = new classMap(value);
                        
                        value.on(fetch).then((value2:any) =>
                        {
                            /* PARA PERMANECER A VARIABEL SALVA, FAZER AQUI POR CAUSA DO PATHS QUE É UMA LISTA */
                            this[keyPath] = value2;

                            this._getPathValue(value2, paths).then(data2 =>
                            {
                                resolve(data2);
                            });
                        });
					}
					/* ARRAY */
                    else if(value instanceof Array)
                    {
						const promises = [];
						  let value2;						
						
						for(const key in value)
						{
							if(value[key] instanceof DocumentReference)
							{
								const classMap = maps.model.klass;        
								value2 = new classMap(value[key]);
								value2 = value2.on();
							}
							else if(value[key].referencePath)
							{
								const classMap = maps.model.klass;        
								value2 = new classMap(value[key]);
								value2 = value2.on();
							}
							else
							{
								value2 = new Promise((resolve) =>
								{
									resolve(value[key]);
								})
							}	
							
							promises.push(value2);
						}
                        
						Promise.all(promises).then((values) => 
						{							
							const _paths = paths.slice(); // COPIA PARA PODER VERIFICAR A PROXIMA CHAVE
							const key 	 = _paths.shift();

							if(key == 'last')
							{
								if(values.length > 0)
								{
									const item = values[values.length - 1];

									this._getPathValue(item, _paths.slice()).then((data2) => 
									{
										resolve(data2);
									})
								}
								else
								{
									resolve(null);
								}
							}
							else if(key == 'first')
							{
								if(values.length > 0)
								{
									const item = values[0];

									this._getPathValue(item, _paths.slice()).then((data2) => 
									{
										resolve(data2);
									})
								}
								else
								{
									resolve(null);
								}
							}
							else
							{
								const promises = [];

								for(const key in values)
								{
									// SLICE COPIA O ARRAY POR CAUSA DA REFERENCIA
									promises.push(this._getPathValue(values[key], paths.slice()));
								}
	
								Promise.all(promises).then((values2) => 
								{
									resolve(values2);
								});
							}
						});
					}
					/* DATE */
					else if(value && value instanceof Date && paths.length > 0)
					{
						const key = paths.pop();
						  let date : any = value;
												
						date = new TDate({ value : date });

						if(key == 'day')
						{
							resolve(date.format('dd'));
						}
						else if(key == 'month')
						{
							resolve(date.format('MM'));
						}
						else if(key == 'year')
						{
							resolve(date.format('yyyy'));
						}
						else if(key == 'date')
						{
							resolve(date.format('dd/MM/yyyy'));
						}
						else if(key == 'datetime')
						{
							resolve(date.format('dd/MM/yyyy HH:mm'));
						}
						else if(key == 'week')
						{
							resolve(date.format('dddd'));
						}
						else
						{
							resolve(date.format('yyyy-MM-dd'));
						}
					}
                    else
                    {                      
                        this._getPathValue(value, paths).then(data2 =>
                        {
                            resolve(data2);
                        });
                    }
                }
            }
        });
    }

	core()
    {
        return Core.get();
    }
}
