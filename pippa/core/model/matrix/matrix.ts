/* PIPPA */
import { BaseModel }  from '../../model/base.model';
import { MatrixType } from '../../type/matrix/matrix.type';

export class Matrix extends BaseModel
{
	public data      : any;
	public items     : any;
	public _variant  : any;
	public isDefault : boolean;
	public type      : any;
	public mode      : string;

	initialize()
	{		
		this.isDefault = true;
		this.type 	   = MatrixType.COMBINE;
		this.mode      = 'ref';
		this.variant   = [ 
		{ 
			items : [{
				id 	  : '_default',
				value : '_default',
			}]
		}]
	}

	populate(data:any)
	{
		/// PORQUE O VARIANTE VEM PRIMERIO QUE O TYPE, E PRECISAMOS DO TYPE PARA O VARIANT
		if(data && data.type)
		{
			this.type = data.type;
		}

		super.populate(data);
	}

	combine(list:any, lists:any)
	{
		const _lists = [];

		for(const _list of lists)
		{		
			for(const item of list)
			{			
				const __list = _list.concat([])
				__list.push(item);
				_lists.push(__list);					
			}		
		}		

		return _lists;
	}

	set variant(lists:any)
	{
		if(lists)
		{		
			let allLists : any = [];
			let listChange = false; // para não pegar lista fazia

			if(this.type.value == MatrixType.COMBINE.value || 
			   this.type.value == MatrixType.FIRST.value || 
			   this.type.value == MatrixType.LAST.value)
			{
				allLists = [[]];

				for(const list of lists)
				{
					if(list)
					{
						allLists   = this.combine(list.items, allLists);
						listChange = true;

						if(list.items.length > 0 && list.items[0].value !== '_default')
						{
							this.isDefault = false;
						}
					}
				}
			}
			else if(this.type.value == MatrixType.SEPARATE.value)
			{
				for(const list of lists)
				{
					if(list)
					{
						listChange = true;

						for(const item of list.items)
						{
							allLists.push([item]);
						}
	
						if(list.items.length > 0 && list.items[0].value !== '_default')
						{
							this.isDefault = false;
						}
					}
				}
			}
			
			if(listChange)
			{
				this.items 	  = allLists;
				this._variant = lists;
			}
			else
			{
				this.items = [[ 
				{ 
					id 	  : '_default',
					value : '_default',
				}]];
				this._variant = [ 
				{ 
					items : [{
						id 	  : '_default',
						value : '_default',
					}]
				}]
			}
		}			
	}

	get variant()
	{
		return this._variant;
	}

	getValue(list:Array<any>, name?:any)
	{
		const path = this.getPath(list);
		
		if(this.data && this.data[path])
		{
			if(name)
			{
				return this.data[path][name];
			}
			else
			{
				return this.data[path];
			}			
		}

		return null;
	}

	getValueByVariant(variant:any, name?:any)
	{
		let data : any = null;

		if(this.isDefault)
		{
			return this.data;
		}

		for(const list of this.items)
		{
			for(const item of list)
			{
				if(item.value == variant.value)
				{
					if(!data)
					{
						data = {};
					}

					const path  = this.getPath(list);
					const value = this.getValue(list, name)
					data[path]  = value;

					break;
				}
			}
		}
		
		return data;
	}

	setValue(list:Array<any>, value:any)
    {
		const path = this.getPath(list);
		
		if(!this.data)
		{
			this.data = {};
		}

		this.data[path] = value;
	}

	getPath(list:Array<any>)
	{
		let path = '';
		let i   = 0;

		for(const path2 in list)
		{
			path += list[path2].value + (i == (list.length - 1) ? '' : '-');
			i++;
		}

		return path;
	}

	normalizeData()
	{
		const data : any = {};

		// LIMPA O DATA DE ITEMS REMOVIDOS
		for(const item of this.items)
		{
			const path = this.getPath(item);

			// DATA
			if(this.data)
			{
				data[path] = this.data[path];
			}	
		}

		return data;
	}

	getData()
	{
		return {
			data 	: this.normalizeData(),
			variant : this.variant,
			type 	: this.type,
			mode    : this.mode
		}
	}
}
