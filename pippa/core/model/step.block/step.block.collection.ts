/* APP */
import { BaseList } from '../base.list';
import { StepBlock } from './step.block';

export class StepBlockCollection extends BaseList<StepBlock>
{
    getMaps()
    {
        return {
            model : { klass : StepBlock },
        };
    }

    getSort()
    {
        this._sort = function(a:any, b:any)
        {
            if(a.stepOrder < b.stepOrder) return -1;
            if(a.stepOrder > b.stepOrder) return 1;

            return 0;
        };

        return this._sort;
    }

    doRows()
    {
        for(const key in this)
        {
            this[key].doRows();
        }
    }
}
