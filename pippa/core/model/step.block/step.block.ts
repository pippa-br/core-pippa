/* PIPPA */
import { BaseModel }       from '../base.model';
import { BlockCollection } from '../block/block.collection';

export class StepBlock extends BaseModel
{
    /* DEFAULT */
    //public label  : string;

    /*
    public order  : number;
    public parent : string;*/

    /* CONTROL */
    public items      : BlockCollection = new BlockCollection();
    public position   : number; /* POSTION NO ARRAY DE STEPS */
    public stepOrder  : number; /* ORDERNÇÃO FEITA EM NIVEL DE FORM ITEM POIS A ORDEM É A NIVEL DE BANCO */
    public rows       : any;

    constructor(args?:any, fetch:boolean=false)
    {
        super(args, fetch);

        Object.defineProperty(this, "items",     { writable: true, enumerable: false, configurable: true }); 
        Object.defineProperty(this, "position",  { writable: true, enumerable: false, configurable: true }); 
        Object.defineProperty(this, "stepOrder", { writable: true, enumerable: false, configurable: true }); 
        Object.defineProperty(this, "rows",      { writable: true, enumerable: false, configurable: true }); 
    }

    getMaps()
    {
        return {
            model : { klass : StepBlock },
        };
    }

    initialize()
    {
        /* DEFAULT */
        this['label'] = "";        

        /* CONTROL */
        this.position  = 0;
        this.rows      = [];
        this.items     = new BlockCollection();                
    }

    doRows()
    {
        const rows : any = new BlockCollection();

        for(const key in this.items)
        {
            const item : any = this.items[key];

            if(item)
            {
                if(!rows[item.row])
                {
                    rows[item.row] = new BlockCollection();
                }

                rows[item.row].set(item);
            }
        }

        /* COMO O ITEM Ë ADICIONADO EM ROWS[i] PODE HAVER I VAZIOS*/
        const _rows : any = new BlockCollection();

        for(const key in rows)
        {
            if(rows[key])
            {
                _rows.push(rows[key]);
                rows[key].doSort();
            }
        }

        this.rows = _rows;
    }
}
