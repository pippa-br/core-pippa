/* PIPPA */
import { BaseModel } from '../../model/base.model';
import { User }      from '../../model/user/user';
import { Document }  from '../../model/document/document';

export class Chat extends BaseModel
{
    getMaps()
    {
        return {
            model    : { klass : Chat },
            sender   : { klass : User },
			receiver : { klass : User },
			document : { klass : Document },
        };
    }

    initialize()
    {
        this['count'] = 0;
        this['group'] = false;
    }

    on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* SENDER */
                    if(this['sender'])
                    {                        
                        promises.push(this.getProperty('sender', fetch));
                    }

                    /* RECEIVER */
                    if(this['receiver'])
                    {
                        promises.push(this.getProperty('receiver', fetch));
                    }

					/* TARGET */
                    if(this['target'])
                    {
                        promises.push(this.getProperty('target', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    });
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
