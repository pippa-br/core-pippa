import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList } from '../base.list';
import { Chat } from './chat';

export class ChatCollection extends BaseList<Chat>
{
    getMaps():any
    {
        return {
			model : { klass : Chat },
			list  : { klass : ChatCollection },
        };
    }
}
