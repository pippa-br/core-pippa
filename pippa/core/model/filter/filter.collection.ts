import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList }   from '../base.list';
import { Filter } from './filter';

export class FilterCollection extends BaseList<Filter>
{
    getMaps()
    {
        return {
			model : { klass : Filter },
			list  : { klass : FilterCollection },
        };
    }
}
