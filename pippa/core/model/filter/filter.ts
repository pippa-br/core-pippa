/* PIPPA */
import { BaseModel } from '../base.model';
import { FormItemCollection } from '../form.item/form.item.collection'

export class Filter extends BaseModel
{
    /*public description     : string;
    public defaultField    : string;
    public defaultOperator : string;
    public defaultValue    : string;
    public items           : FormItemCollection;
    public buttons         : any;
    public trClass         : any;
    public type            : any;*/

    getMaps()
    {
        return {
			model : { klass : Filter },
			items : { klass : FormItemCollection },
        };
    }

    initialize()
    {
	}
	
	collectionName()
	{
		return 'filter';
	}

	on(fetch:boolean = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if(fetch)
                {
                    const promises = [];

                    /* ITEMS */
                    if(this['items'])
                    {
                        promises.push(this.getProperty('items', fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
}
