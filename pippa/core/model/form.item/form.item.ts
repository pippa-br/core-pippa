/* PIPPA */
import { BaseModel }          from "../base.model";
import { Field }              from "../field/field";
import { Option }             from "../option/option";
import { OptionCollection }   from "../option/option.collection";
import { StepItem }           from "../step.item/step.item";
import { FormItemCollection } from "../form.item/form.item.collection";
import { WhereCollection } 	  from "../where/where.collection";
import { SlotType }           from "../../type/slot/slot.type";
import { Grid }               from "../../model/grid/grid";
import { FieldType }          from "../../type/field/field.type";

export class FormItem extends BaseModel
{
    /* CONTROL */
    public list    : FormItemCollection;
    public version : number; // INFORMA UM NUMERO PARA CONTROLAR A ATUALIZAÇÃO DO OBJETO

    constructor(args?:any, fetch = false)
    {
        super(args, fetch);

        Object.defineProperty(this, "list",    { writable : true, enumerable : false, configurable : true }); 
        Object.defineProperty(this, "version", { writable : true, enumerable : false, configurable : true }); 
    }

    getMaps()
    {
        return {
            model   : { klass : FormItem },
            list    : { klass : FormItemCollection },
            field   : { klass : Field },
            _field  : { klass : Field },
            option  : { klass : Option },
            options : { klass : OptionCollection },
            step    : { klass : StepItem },
            grid    : { klass : Grid },
            items   : { klass : FormItemCollection },  
            filters : { klass : WhereCollection },  
        };
    }

    initialize()
    {
        this.list    = new FormItemCollection();
        this.version = 0;

        this["row"]       = 0;
        this["col"]       = 0;
        this["stepOrder"] = 0;        
        this["_slot"]     = SlotType.CONTENT;
        this["enabled"]   = true;
        this["default"]   = false;
        this["items"]     = new FormItemCollection();
    }
	
    /* REMOVE OS CAMPOS DO FORM ITEM PARA PEGAR O DEFAULT DO FIELD */
    normalize()
    {
        this["label"]       = undefined;
        this["name"]        = undefined; 
        this["path"]        = undefined;        
        this["hasLabel"]    = undefined;
        this["required"]    = undefined;
        this["editable"]    = undefined;
        this["initial"]     = undefined;
        this["_groups"]     = undefined;
        this["setting"]     = undefined;
        this["option"]      = undefined;
        this["options"]     = undefined;
        this["grid"]        = undefined;
        this["information"] = undefined;
        this["description"] = undefined;
        this["filters"]     = undefined;
    }

    populate(data)
    {
        super.populate(data);

        if (data && data.field)
        {
            this["_field"] = data.field;

            if (!this["name"])
            {
                this["name"] = this.field.name;
            }

            if (!this["path"])
            {
                this["path"] = this.field.name;
            }
	
            if (!this["label"])
            {
                this["label"] = this.field.label;
            }
			
            if (this["information"] === undefined)
            {
                this["information"] = this.field.information;
            }

            if (this["description"] === undefined)
            {
                this["description"] = this.field.description;
            }

            if (this["grid"] === undefined)
            {
                this["grid"] = this.field.grid;
            }
	
            if (this["hasLabel"] === undefined)
            {
                this["hasLabel"] = this.field.hasLabel;
            }

            if (this["required"] === undefined)
            {
                this["required"] = this.field.required;
            }

            if (this["editable"] === undefined)
            {
                this["editable"] = this.field.editable;
            }            

            if (this["initial"] === undefined)
            {
                this["initial"] = this.field.initial;
            }            

            if (this["_groups"] === undefined)
            {
                this["_groups"] = this.field._groups;
            }
	
            if (this["setting"] === undefined)
            {
                this["setting"] = this.field.setting;
            }
	
            if (this["option"] === undefined)
            {
                this["option"] = this.field.option;				
            }

            if (this["options"] === undefined)
            {
                this["options"] = this.field.options;				
            }

            if (!this["filters"] || this["filters"].length == 0)
            {
                this["filters"] = this.field.filters;							
            } 

            /* ROW E COL 0, CONSIDERA O ITEM FIELD */
            if (parseInt(this.field.row) > 0)
            {
                this["row"] = this.field.row;
            }
			
            if (parseInt(this.field.col) > 0)
            {
                this["col"] = this.field.col;
            }
        }
        else
        {
            //this['row'] = 0;
            //this['col'] = 0;
        }		
    }

    set field(value:any)
    {
        this["$field"] = value;
    }

    get field()
    {
        return this["$field"];
    }

    set option(value:any)
    {
        this["_option"] = value;
    }

    get option()
    {
        return this["_option"];
    }

    set options(value:any)
    {
        this["_options"] = value;
    }

    get options()
    {
        return this["_options"];
    }
	
    set filters(value:any)
    {
        this["_filters"] = value;
    }

    get filters()
    {
        return this["_filters"];
    }

    set setting(value:any)
    {
        this["_setting"] = value;
    }

    get setting()
    {
        return this["_setting"];
    }

    set grid(value:any)
    {
        this["_grid"] = value;
    }

    get grid()
    {
        return this["_grid"];
    }

    set hasLabel(value:any)
    {
        this["_hasLabel"] = value;
    }

    get hasLabel()
    {
        return this["_hasLabel"];
    }
	
    set required(value:any)
    {
        this["_required"] = value;
    }

    get required()
    {
        return this["_required"];
    }

    set editable(value:any)
    {
        this["_editable"] = value;
    }

    get editable()
    {
        return this["_editable"];
    }

    set initial(value:any)
    {
        this["_initial"] = value;
    }

    get initial()
    {
        return this["_initial"];
    }

    set _groups(value:any)
    {
        this["__groups"] = value;
    }

    get _groups()
    {
        return this["__groups"];
    }

    set name(value:any)
    {
        this["_name"] = value;
    }

    get name()
    {
        return this["_name"];
    }
	
    set path(value:any)
    {
        this["_path"] = value;
    }

    get path()
    {
        return this["_path"];
    }

    set label(value:any)
    {
        this["_label"] = value;
    }

    get label()
    {
        return this["_label"];
    }
	
    set slot(value:any)
    {
        this["_slot"] = value;
    }

    get slot()
    {
        return this["_slot"];
    }

    on(fetch = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if (fetch)
                {
                    const promises = []

                    /* ON FIELD */
                    if (this["field"])
                    {
                        promises.push(this.getProperty("field", fetch));
                    }

                    /* ON STEP */
                    if (this["step"])
                    {
                        promises.push(this.getProperty("step", fetch));
                    }

                    /* ON OPTION */
                    if (this["option"])
                    {
                        promises.push(this.getProperty("option", fetch));
                    }
					
                    /* ON OPTION */
                    if (this["options"])
                    {
                        promises.push(this.getProperty("options", fetch));
                    }

                    /* ON ITEMS */
                    if (this["items"] && this["items"].length > 0)
                    {
                        promises.push(this.getProperty("items", fetch));
                    }

                    /* ON GRID */
                    if (this["grid"])
                    {
                        promises.push(this.getProperty("grid", fetch));
                    }
					
                    /* ON filters */
                    if (this["filters"])
                    {
                        promises.push(this.getProperty("filters", fetch));
                    }

                    Promise.all(promises).then((values) =>
                    {
                        resolve(this);

                        /* PARA CADASTROS ANTIGOS DEPOIS REMOVER */
                        /*if(this.field.items && this.field.items.length > 0)
                        {
                            this.items = new FormItemCollection(this.field.items);

                            this.items.on(true).then(() =>
                            {
                                resolve(this);
                            });
                        }
                        else
                        {
                            resolve(this);
                        }*/
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }
    
    merge(formItem:any)
    {
        this["items"].setItems(formItem.items);
    }

    getFormItensOption()
    {
        const formItems : any = [];

        for (const key in this["items"])
        {
            const formItem   = this["items"][key].copy();
            formItem.groupBy = "Formulário";

            formItems.push(formItem);
        }

        const _fields = FieldType.getFieldsDefault();

        for (const key in _fields)
        {
            const formItem : any = {
                id      : _fields[key].id,
                _label  : _fields[key].label,
                _path   : _fields[key].name,
                field   : _fields[key],
                groupBy : "Campos",
            };

            formItems.push(formItem);
        }

        return {
            record : false,
            items  : formItems
        };
    }    
}
