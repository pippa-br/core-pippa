/* APP */
import { BaseList }           from "../base.list";
import { FormItem }           from "./form.item";
import { StepItemCollection } from "../step.item/step.item.collection";
import { StepItem }           from "../step.item/step.item";

export class FormItemCollection extends BaseList<FormItem>
{
    getMaps():any
    {
        return {
            model : { klass : FormItem },
            list  : { klass : FormItemCollection },
        };
    }

    getSort()
    {
        this._sort = function(a:any, b:any)
        {
            if (a.row < b.row) return -1;
            if (a.row > b.row) return 1;

            if (a.col < b.col) return -1;
            if (a.col > b.col) return 1;

            return 0;
        };

        return this._sort;
    }

    /* BY FIELD NAME */
    getByFieldName(name:string)
    {
        return this.findByFieldName(name);
    }

    findByFieldName(name:string):number
    {
        let data : any;

        for (const key in this)
        {
            const item : any = this[key];

            if (item.field.name == name)
            {
                data = item;
                break;
            }
            else if (item.field.isContainer())
            {
                data = item.items.findByFieldName(name);

                if (data)
                {
                    break;
                }
            }
        }

        return data;
    }

    /* BY FIELD  */
    hasByField(field:any):boolean
    {
        return this.findPositionByField(field) != -1;
    }

    delByField(field:any):boolean
    {
        const value = this._delByField(field);

        this.dispatchUpdate();

        return value;
    }

    _delByField(field:any):boolean
    {
        const position = this.findPositionByField(field);

        if (position > -1)
        {
            const item = this[position];
            return super._del(item);
        }

        return false;
    }

    findPositionByField(field:any):number
    {
        let i = 0;

        for (const key in this)
        {
            const item : any = this[key];

            if (item.field.id == field.id)
            {
                return i;
            }

            i++;
        }

        return -1;
    }

    getColumns()
    {
        const columns = [];

        for (const key in this)
        {
            const item = new FormItem(this[key].getData());

            item["row"] = parseInt(key);

            if (item.field.viewColummn)
            {
                columns.push(item);
            }
        }

        return columns;
    }

    lastRow():number
    {
        let row = 0;

        if (this.length)
        {
            const item : any = this[this.length - 1];
            row              = item.row;
        }

        return row;
    }

    getItemsBySteps(ids:string) // DEPOIS PASSAR ISSO PARA NAMES
    { 
        const items = new FormItemCollection();
        const steps = ids.split(",");
		
        for (const key in this)
        {
            const item : any = this[key];
            const step : any = item.step;

            if (step && steps.indexOf(step.id) > -1)
            {
                items.set(item);
            }
        }

        return items;
    }

    filterItemsBySteps(names:string)
    { 
        const steps = ("" + names).split(",");
		
        this.setFilter((item:any) => 
        {
            const step : any = item.step;

            if (steps.indexOf(step.name) > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        });
    }

    /* PASSAMOS DATA PARA VERIFICAR SE A TAB SERÁ ATIVA, USADA MUITO NA VIEWER */
    getSteps(data?:any, form?:any, groups?:any)
    {
        const steps      = new StepItemCollection();
        const querySteps = this.core().queryParams.steps ? this.core().queryParams.steps.split(",") : null;
        const ids        = [];
        let total        = 0;
        let count        = 0; // FORMS COMPLETE
        let percentage   = 0;

        if (!groups && this.core().user)
        {
            groups = this.core().user.getGroups();
        }

        for (const key in this)
        {
            const item : any = this[key];
            const step : any = item.step;
            let active       = true;

            if (step)
            {
                /* A STEP É LIBERADA COM UMA FLAG NO DOCUMENT */
                if (data && data[step.name] != undefined)
                {
                    //console.log(step.name, data && data[step.name])

                    active = data[step.name];
                }

                /* A STEP É LIBERADA COM PERMISSÕES NO STEP */
                if (groups && active)
                {
                    active = step.hasGroups(groups);
                }
                
                /*if(step._groups && step._groups.length > 0)
				{
					if(this.core().user)
					{
						active = this.core().user.hasGroups(step._groups);
					}	
				}*/

                /* MODE FORM ADD OR SET */
                if (form && active)
                {
                    if (form.isAddModeForm())
                    {
                        if (step.onlySet)
                        {
                            active = false;
                        }
                    }
                    else if (form.isSetModeForm())
                    {
                        if (step.onlyAdd)
                        {
                            active = false;
                        }
                    }                                
                }

                /* A STEP É LIBERADA POR PARAMETROS */
                if (querySteps && active)
                {												
                    const key = step.name || step.id;
                    active    = false;

                    if (querySteps.indexOf(key) > -1)
                    {
                        active = true;
                    }
                }

                if (ids.indexOf(step.id) == -1)
                {
                    if (active)
                    {
                        steps.set(step);
                        ids.push(step.id);
	
                        total++;	
                    }
                }				
            }
        }

        if (steps.length == 0)
        {
            steps.push(new StepItem({
                id    : 0,
                label : "Geral"
            }));

            total = 1;
            count = 0;
        }
        else
        {
            steps.doSort();

            let i = 0;

            for (const key in steps)
            {
                const step : any = steps[key];
                step.position    = i;
                i++;

                if (data && step.isComplete(data))
                {
                    count++;
                }
            }
        }

        percentage = Math.round((count * 100) / total);

        steps._total      = total;
        steps._count      = count;
        steps._percentage = percentage;
		
        return steps;
    }
}
