import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList } from '../base.list';
import { Chart } from './chart';

export class ChartCollection extends BaseList<Chart>
{
    getMaps():any
    {
        return {
            model : { klass : Chart },
            list  : { klass : ChartCollection },
        };
    }

    initialize(items)
    {
        super.initialize(items);

        this.setSort(function(a, b)
        {
            if(a.date < b.date) return 1;
            if(a.date > b.date) return -1;

            return 0;
        });
    }
}
