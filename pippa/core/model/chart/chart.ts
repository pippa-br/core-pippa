/* PIPPA */
import { BaseModel } 		  from '../base.model';
import { Form }               from '../form/form';

export class Chart extends BaseModel
{
	static PIE_TYPE   = { value : 'pie', label : 'Pie'};
	static BAR_TYPE   = { value : 'bar', label : 'Bar'};
    static LINE_TYPE  = { value : 'line', label : 'Line'};
    static CARD_TYPE  = { value : 'card', label : 'Card'};

	static COUNT_FUNCTION   = { value : 'count', label : 'Count'};
	static SUM_FUNCTION     = { value : 'sum',   label : 'Sum'};
    static AVERAGE_FUNCTION = { value : 'average',   label : 'Average'};
	static VALUE_FUNCTION   = { value : 'value', label : 'Valor'};

	static VALUE_FORMAT    = { value : 'value',    label : 'Valor'};
	static CURRENCY_FORMAT = { value : 'currency', label : 'Moeda'};

    getMaps()
    {
        return {
            model : { klass : Chart },
            form  : { klass : Form },      
        };
    }

    initialize()
    {
	}	
}
