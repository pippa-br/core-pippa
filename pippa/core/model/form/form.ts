import { EventEmitter } from "@angular/core";
import { FormGroup }    from "@angular/forms";
import cleanDeep 		from "clean-deep";

/* PIPPA */
import { BaseModel }          from "../../../core/model/base.model";
import { FieldCollection }    from "../../model/field/field.collection";
import { FormItemCollection } from "../../model/form.item/form.item.collection";
import { Params }             from "../../util/params/params";
import { FieldType }          from "../../type/field/field.type";
import { FormType }           from "../../type/form/form.type";
import { TDate } from "../../util/tdate/tdate";

export class Form extends BaseModel
{    
    /* ONLY VIEWER */
    public mode            : string;
    public submitEvent     : EventEmitter<any>;
    public saveEvent       : EventEmitter<any>;
    public cancelEvent     : EventEmitter<any>;
    //public setting         : any;
    public hasButtons      : boolean;
    public hasLabel        : boolean;
    public hasSubmitButton : boolean;
    public hasCancelButton : boolean;
    public hasCloseButton  : boolean;
    public hasHeader       : boolean;
    public viewEdit        : boolean;
    public formGroup       : FormGroup;
    public root            : any;
    public instance        : any; /* INSTANCIA DO FORM */
    public instances       : any; /* INSTANCIA DOS FIELDS */
    public parent          : Form;
    public error           : string;
    public component       : any;
    public iconAdd         : string;
    public iconSet         : string;
    public iconCancel      : string;
    public complete        : any;
    public setting        : any;
    public _disable        : boolean;
    public autoSave        : boolean;

    constructor(args?:any, fetch = false)
    {
        super(args, fetch);

        Object.defineProperty(this, "mode",            { writable : true, enumerable : false, configurable : true });
        //Object.defineProperty(this, "setting",         { writable: true, enumerable: false, configurable: true });
        Object.defineProperty(this, "submitEvent",     { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "saveEvent",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "cancelEvent",     { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasButtons",      { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasLabel",        { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasSubmitButton", { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasCancelButton", { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasCloseButton",  { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "hasHeader",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "viewEdit",        { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "formGroup",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "instance",        { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "instances",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "parent",          { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "error",           { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "component",       { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "iconAdd",         { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "iconSet",         { writable : true, enumerable : false, configurable : true });         
        Object.defineProperty(this, "iconCancel",      { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "root",            { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "complete",        { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "_disable",        { writable : true, enumerable : false, configurable : true });
        Object.defineProperty(this, "autoSave",        { writable : true, enumerable : false, configurable : true });
    }

    collectionName()
    {
        return "forms";
    }

    getMaps()
    {
        return {
            model : { klass : Form },
            form  : { klass : Form },
            items : { klass : FormItemCollection },
        };
    }

    initialize()
    {
        this.mode            = Params.ADD_MODE_FORM;
        this.iconAdd         = "add-circle-outline";
        this.iconSet         = "checkmark-circle-outline";
        this.iconCancel      = "close-circle-outline";
        this.hasButtons      = true;
        this.hasLabel        = true;
        this.hasSubmitButton = true;
        this.hasCancelButton = true;
        this.hasCloseButton  = true;
        this.hasHeader       = true;
        this.viewEdit        = false;
        this.formGroup       = new FormGroup({}, { updateOn : "submit" });
        this.root            = this.formGroup.root;
        this.submitEvent     = new EventEmitter();
        this.cancelEvent     = new EventEmitter();
        this.saveEvent    	  = new EventEmitter();

        this["openMode"]       = FormType.ROUTER_OPEN_MODE;
        this["orderFormItem"]  = true;
        this["saveFixed"]      = false;
        this["stepSave"]       = false;
        this["sumForm"]        = false;
        this["fieldsForm"]     = new FieldCollection();
        this["fieldsDefault"]  = new FieldCollection();
        this["fieldsDocument"] = new FieldCollection();
        this["itemsDefault"]   = new FormItemCollection();
        this["items"]          = new FormItemCollection();
        this["type"]           = FormType.FLAT_TYPE;
    }

    get saveId()
    {
        return this["setting"] && this["setting"].saveId != undefined ? this["setting"].saveId : "save";
    }

    get addText()
    {
        return this["setting"] && this["setting"].addText != undefined ? this["setting"].addText : "Enviar";
    }

    get setText()
    {
        return this["setting"] && this["setting"].setText != undefined ? this["setting"].setText : "Salvar";
    }

    get nextText()
    {
        return this["setting"] && this["setting"].nextText != undefined ? this["setting"].nextText : "Avançar";
    }

    get prevText()
    {
        return this["setting"] && this["setting"].prevText != undefined ? this["setting"].prevText : "Voltar";
    }

    get cancelText()
    {
        return this["setting"] && this["setting"].cancelText != undefined ? this["setting"].cancelText : "Fechar";
    }

    getInstance(name:string)
    {
        if (this.instances)
        {
            return this.instances[name];
        }
    }
	
    changePermission()
    {

    }

    isParent()
    {
        return this.parent == undefined;
    }

    isMaster()
    {
        return this["id"] == "master";
    }
	
    /*createIndexes()
	{
		return new Promise<void>((resolve) =>
        {
			this.getProperty('form').then(async (form) => 
			{				
				resolve();
			});
        });
	}*/

    on(fetch = false):Promise<BaseModel>
    {
        return new Promise<BaseModel>((resolve) =>
        {
            super.on(fetch).then(() =>
            {
                if (fetch)
                {
                    const promises = [];

                    /* ITEMS */
                    if (this["items"])
                    {
                        promises.push(this.getProperty("items", fetch));
                    }

                    Promise.all(promises).then(() =>
                    {
                        resolve(this);
                    })
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }

    /* PEGA OS ITENS COM OS FILTROS */
    getItems()
    {        
        const _items = new FormItemCollection();

        for (const key in this["items"])
        {
            const item = this["items"][key];
            let   del  = false;

            if (this.mode)
            {
                /* MODE FORM */
                if (this.isAddModeForm())
                {
                    if (item.field.onlySet != undefined && item.field.onlySet)
                    {
                        del = true;
                    }
                }
                else if (this.isSetModeForm())
                {
                    if (item.field.onlyAdd != undefined && item.field.onlyAdd)
                    {
                        del = true;
                    }
                }
								
                if (!del)
                {
                    /* GROUPS */
                    if (item.step && item.step._groups && item.step._groups.length > 0)
                    {
                        if (this.core().user)
                        {
                            del = !this.core().user.hasGroups(item.step._groups);
                        }	
                        else
                        {
                            del = true;
                        }				
                    }
					
                    /* FIELD */
                    if (item._groups && item._groups.length > 0)
                    {
                        if (this.core().user)
                        {
                            del = !this.core().user.hasGroups(item._groups);
                        }	
                        else
                        {
                            del = true;
                        }
                    }
                }				
            }

            if (del)
            {
                item.field.hidden = true;                				
            }

            if (item.field.name == "materialStep")
            {
                //console.error('------', item.field.hidden)
            }

            _items.set(item);
        }
        
        return _items;
    }
    
    getDescription()
    {
        if (this.isSetModeForm()) /* VISUALIZAR SOMENTE EM SET MODE */
        {
            if (this["setDescription"] && this["setDescription"] != "")
            {
                return this["setDescription"];
            }
            else if (this["description"] && this["description"] != "")
            {
                return this["description"];
            }
        }
        else if (this.isAddModeForm()) /* VISUALIZAR SOMENTE EM  MODE */
        {
            if (this["addDescription"] && this["addDescription"] != "")
            {
                return this["addDescription"];
            }
            else if (this["description"] && this["description"] != "")
            {
                return this["description"];
            }
        }
    }

    isAddModeForm()
    {
        return (this.mode == Params.ADD_MODE_FORM);
    }

    isSetModeForm()
    {
        return (this.mode == Params.SET_MODE_FORM);
    }

    doAddModeForm()
    {
        this.mode = Params.ADD_MODE_FORM;

        console.log("mode form ", this.mode);
    }

    doSetModeForm()
    {
        this.mode = Params.SET_MODE_FORM;

        console.log("mode form ", this.mode);
    }

    onSave()
    {
        this.saveEvent.emit();
    }

    onSubmit(event:any)
    {
        this.submitEvent.emit(event);
    }

    onCancel(event:any)
    {
        this.cancelEvent.emit(event);
    }

    set disable(value:boolean)
    {
        for (const key in this["items"])
        {
            const item = this["items"][key];

            if (item.field.instance)
            {
                item.field.instance.disable = value;
            }
        }

        this._disable = value;
    }

    get disable()
    {
        return this._disable;
    }

    reset()
    {
        //this.formGroup.reset();
        for (const key in this["items"])
        {
            const item = this["items"][key];

            if (item.field.instance)
            {
                item.field.instance.reset();
            }
        }
    }

    /* UTILIZADO COM SAVE FIXED! POIS PRECISA VALIDAR AS CHAMADAS ANSYC COM SIGLE */
    updateValueAndValidity()
    {
        for (const key in this["items"])
        {
            const item = this["items"][key];

            if (item.field.instance)
            {
                item.field.instance.updateValueAndValidity();
            }
        }
    }

    /* USANDO QUANDO TEMOS UM OBJETO DE OUTRO FORM E QUEREMOS TRANSFORMA PARA ESSE FORM. EX; GERAR PAYMENT */
    parseToData(data:any)
    {
        return new Promise<any>(resolve =>
        {
            this.getInitialData().then(value =>
            {	
                /* MERGE */			
                value = Object.assign(value, data);
				
                /* REFERENCE */
                if (this.reference)
                {
                    value.form = this.reference;
                }

                /* CODE FORM */
                if (this["code"] != undefined)
                {
                    value.codeForm = this["code"];
                }

                /* LEVEL FORM */
                if (this["_level"] != undefined)
                {
                    value["_level"] = this["_level"];
                }

                if (this["projectID"] != undefined)
                {
                    value["projectID"] = this["projectID"];
                }
								
                if (this["accid"] != undefined)
                {
                    value["accid"] = this["accid"];
                }	
				
                if (this["appid"] != undefined)
                {
                    value["appid"] = this["appid"];
                }

                if (this.core().user && !this.core().user.isGuest())
                {
                    if (!value.owner)
                    {
                        value.owner = this.core().user.reference;
                    }

                    if (!value.user)
                    {
                        value.user = this.core().user.reference;
                    }
                }

                value.postdate = new TDate().toDate();
                value.lastdate = new TDate().toDate();
                value.order    = new TDate().unix();

                /* INVERSED BY */
                for (const key in this["items"])
                {
                    const field = this["items"][key].field;

                    if (field.setting && field.setting.inversedBy && value[field.name])
                    {
                        if (!value.inversedBy)
                        {
                            value.inversedBy = {};
                        }

                        value.inversedBy[field.setting.inversedBy] = value[field.name];
                    }
                }

                console.log("parseToData", value);

                resolve(value);
            });
        });
    }

    getInitialData()
    {
        return new Promise<any>(resolve =>
        {
            let data = {};

            if (this.isAddModeForm())
            {
                for (const key in this["items"])
                {
                    const formItem = this["items"][key];
                    data           = Object.assign({}, data, this._getInitialData(formItem));
                }

                // INITAL FORM
                if (this.setting && this.setting.initialData)
                {
                    data = Object.assign({}, data, this.setting.initialData);
                }
                
                resolve(cleanDeep(data, { emptyArrays : false }));    
            }
            else
            {
                resolve(data);
            }
        });
    }

    _getInitialData(formItem:any)
    {
        const data = {};

        if (formItem.field.type.value == FieldType.type("Select").value || 
            formItem.field.type.value == FieldType.type("Radio").value)
        {
            if (formItem.field.option && formItem.field.option.items)
            {
                for (const key in formItem.field.option.items)
                {
                    const item = formItem.field.option.items[key];
					
                    if (item.selected || formItem.initial === item.value)
                    {
                        data[formItem.field.name] = item;
                        break;
                    }
                }
            }
        }
        else if (formItem.field.type.value == FieldType.type("MultiSelect").value)
        {
            if (formItem.field.option && formItem.field.option.items)
            {
                const values = formItem.initial.split(",");
                const items  = [];

                for (const key in formItem.field.option.items)
                {
                    const item = formItem.field.option.items[key];

                    for (const value of values)
                    {
                        if (item.selected || value === item.value)
                        {
                            items.push(item);                            
                        }
                    }					
                }

                //data[formItem.field.name] = items;                
            }
        }
        else if (formItem.field.type.value == FieldType.type("PaymentStatus").value)
        {
            data[formItem.field.name] = {
                status : formItem.initial
            };
        }
        else if (formItem.field.type.value == FieldType.type("Hidden").value)
        {
            /* CASSO UM SELECT E ALTERADO PARA O HIDDEN NO CASO ONLY ADMIN*/
            if (formItem.field.option && formItem.field.option.items && formItem.field.option.items.length > 0)
            {
                let bindValue : any;

                /* SETTINGS */
                if (formItem.field.setting && formItem.field.setting)
                {
                    /* BIND VALUE */
                    if (formItem.field.setting.bindValue)
                    {
                        bindValue = formItem.field.setting.bindValue;
                    }
                }

                for (const key in formItem.field.option.items)
                {
                    const item = formItem.field.option.items[key];

                    if (item.selected || formItem.initial == item[bindValue])
                    {
                        data[formItem.field.name] = item;
                        break;
                    }
                }
            }
            else if (formItem.field.setting && Object.keys(formItem.field.setting).length > 0)
            {
                data[formItem.field.name] = formItem.field.setting;
            }
            else if (formItem.initial)
            {
                data[formItem.field.name] = formItem.initial;
            }
        }
        else if (formItem.field.type.value == FieldType.type("Date").value)
        {
            if (formItem.field.setting && formItem.field.setting.hasDefault)
            {
                data[formItem.field.name] = new Date();
            }
            else if (formItem.initial == "now") 
            {
                data[formItem.field.name] = new Date();
            }
            else
            {
                data[formItem.field.name] = formItem.initial;
            }
        }         
        else
        {
            data[formItem.field.name] = formItem.initial;
        }

        /* SUB ITEMS - NÃO SUBFORMS */
        if (formItem.field.type.value != FieldType.type("SubForm").value)
        {
            for (const key in formItem.items)
            {
                if (!data[formItem.field.name])
                {
                    data[formItem.field.name] = {};
                }
	
                const formItem2           = formItem.items[key];
                data[formItem.field.name] = Object.assign({}, data[formItem.field.name], this._getInitialData(formItem2)) ;
            }
        }        

        return data;
    }
	
    getItemsData()
    {
        return new Promise<any>(resolve =>
        {
            /* DATA */
            let data = {};

            for (const key in this["items"])
            {
                const formItem = this["items"][key];
                data           = Object.assign({}, data, this._getItemsData(formItem));
            }			
			
            resolve(data);
        });
    }
	
    _getItemsData(formItem)
    {
        const data = {};

        data[formItem.field.name] = "";

        /* SUB ITEMS - NÃO SUBFORMS */
        if (formItem.field.type.value != FieldType.type("SubForm").value)
        {
            for (const key in formItem.items)
            {
                if (!data[formItem.field.name])
                {
                    data[formItem.field.name] = {};
                }
	
                const formItem2           = formItem.items[key];
                data[formItem.field.name] = Object.assign({}, data[formItem.field.name], this._getItemsData(formItem2)) ;
            }
        }        

        return data;
    }

    /* TO APP */
    toAsk()
    {
        const steps : any = {};

        for (const key in this["items"])
        {
            const item : any = this["items"][key];

            if (!steps[item.step.id])
            {
                steps[item.step.id] = {
                    label : item.step.label,
                    asks  : [],
                    type  : {
                        label : "Step",
                        valus : "step"
                    }
                };
            }

            const data = {
                name  : item.field.name,
                label : item.field.label,
                type  : item.field.type,
                asks  : []
            };

            for (const key2 in item.items)
            {
                const item2 = item.items[key2];

                data.asks.push({
                    name   : item2.field.name,
                    label  : item2.field.label,
                    type   : item2.field.type,
                    option : (item2.field.option ? item2.field.option.reference : null)
                });
            }

            steps[item.step.id].asks.push(data);
        }

        const asks : Array<any> = [];

        for (const key in steps)
        {
            asks.push(steps[key]);
        }

        return asks;
    }

    getFormItemsDefault()
    {
        /* DEFAULT */
        const items : any = new FormItemCollection();

        /* FROM FORM */
        for (const key in this["items"])
        {
            const item   = this["items"][key].copy();
            item.groupBy = "Formulário";

            items.push(item);

            if (item.field.type.value == FieldType.type("VideoUrl").value)
            {
                const item   = this["items"][key].copy();
                item.id      = item.id + "_2";
                item.label   = "Vídeo Thumbnail";
                item.groupBy = "Formulário";
                item.path    = item.field.name + ".thumbnailUrl";
                item.type    = FieldType.type("Image");
            }
        }

        return items;
    }

    getOrderOptions()
    {
        const fields : any = [];

        for (const key in this["items"])
        {
            const field : any = {
                id      : this["items"][key].field.id,
                label   : this["items"][key].field.label,
                value   : this["items"][key].field.name,
                type    : this["items"][key].field.type,
                groupBy : "Formulário",
            };

            fields.push(field);
        }

        const _fields = FieldType.getFieldsDefault();

        for (const key in _fields)
        {
            const field : any = {
                id      : _fields[key].id,
                label   : _fields[key].label,
                value   : _fields[key].name,
                type    : _fields[key].type,
                groupBy : "Campos",
            };

            fields.push(field);
        }

        return {
            items : fields
        };
    }		
}
