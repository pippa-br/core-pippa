/* APP */
import { BaseList } from '../base.list';
import { Form } from './form';

export class FormCollection extends BaseList<Form>
{
    getMaps()
    {
        return {
            model : { klass : Form },
            list  : { klass : FormCollection },
        };
    }

    get indexPath():string
    {
		return this.core().account.projectID + '-' + this.core().account.code + '-' + this.appid + '-forms';
    }

    populate(items:any)
    {
        if(items)
        {
            for(const key in items)
            {
                if(items[key].id == 'master')
                {
                    delete items[key];
                }
            }
        }

        return super.populate(items);
    }
}
