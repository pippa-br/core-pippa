/* PIPPA */
import { BaseModel } from '../../model/base.model';
import { Form }      from '../../model/form/form';

export class Account extends BaseModel
{
    getMaps()
    {
        return {
            model  : { klass : Account },
            form   : { klass : Form },
        };
    }

    getMenuAppid()
    {
		const key : string = 'menu_appid_' + this.core().platform.value;
		
		console.log('menu key', key);

        return this[key];
	}

	getNotificationFormPath()
    {
        const key : string = 'notification_form_path_' + this.core().platform.value;
        return this[key];
	}

	accidName()
	{
		return this.core().masterAccount.code;
	}
}
