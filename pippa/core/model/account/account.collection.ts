import { Injectable, Input } from '@angular/core';

/* APP */
import { BaseList }    from '../base.list';
import { Account } from './account';

export class AccountCollection extends BaseList<Account>
{
    getMaps():any
    {
        return {
            model : { klass : Account },
            list  : { klass : AccountCollection },
        };
    }
}
