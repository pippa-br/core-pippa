export class Address extends Object
{
    id           : number;
    accid        : number;
    appid        : number;
    city         : string;
    complement   : string;
    district     : string;
    housenumber  : string;
    ibge         : string;
    zipcode      : string;
    street       : string;
    lat          : string;
    lng          : string;
    locationtype : string;
    state        : string;
    country      : string;
    value        : any = {};
}
