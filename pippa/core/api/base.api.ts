import { HttpClient, HttpRequest } from "@angular/common/http";
import { interval, Observable, throwError } from "rxjs";
import { NgZone } from "@angular/core";
import * as _ from "lodash" 
import { map, catchError } from "rxjs/operators";
import { DocumentReference } from "firebase/firestore"; 
import { doc } from "firebase/firestore";

/* APPP */
import { Core } from "../util/core/core";

/* UTIL */
import { Params }            from "../util/params/params";
import { Result }            from "../util/result/result";
import { Collection }        from "../util/collection/collection";
import { LoadingController } from "../controller/loading/loading.controller";
import { environment } 		 from "src/environments/environment";
import { TDate } from "../util/tdate/tdate";

export class BaseAPI
{
    public isFirebase  = false;

    protected resultFactory:any;
    protected paramsFactory:any;

    constructor(
        public http              : HttpClient,
        public zone              : NgZone,
        public loadingController : LoadingController,
    )
    {
    }

    core()
    {
        return Core.get()
    }

    api()
    {
        return Core.get().api
    }

    public createResult : (params:Params) => Result = function(params:Params)
    {
        return new Result(params);
    }

    public createParams : (args?:any) => Params = function(args?:any)
    {
        return this.core().paramsFactory.create(args);
    }

    public getInformationAddress(params:Params)
    {
        const promise : Promise<any> = new Promise((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
                {
                    params.apiUrl = "https://viacep.com.br/ws/" + params.post.zipcode + "/json";
                    this.http.request("get", params.apiUrl).pipe(
                        catchError(e => 
                        {
                            resolve(null);
                            return this.handleError(e);
                        })).subscribe(response =>
                    {
                        const result = this.createResult(params);
                        result.data  = response;

                        resolve(result);
                    })
                });                
            });
        });

        return promise;
    }    

    /* CHAMADAS GENERICAS */
    request(params:Params)
    {
        return new Promise<any>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
                {
                    if (!params.headers)
                    {
                        params.headers = new Headers();
                        params.headers.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                        params.headers.append("Accept", 	  "application/json");	
                    }

                    const options = {
                        headers         : params.headers,
                        params          : params.parseQuery(),
                        withCredentials : params.withCredentials,
                    };

                    /* LOAD */
                    this.core().loadingController.start(params.loading).then(() =>
                    {
                        this.http.post(params.apiUrl, params.getPost(), options).pipe(
                            catchError(e =>
                            {
                                /* LOAD */
                                this.core().loadingController.stop();
                                resolve(null);
                                return this.handleError(e);
                            })).subscribe(response =>
                        {
                            /* LOAD */
                            this.core().loadingController.stop();
                            resolve(response);
                        });
                    });
                });				
            });
        });
    }

    getRequest(params:Params)
    {
        return new Promise<any>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
                {
                    if (!params.headers)
                    {
                        params.headers = new Headers();
                        params.headers.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                        params.headers.append("Accept", 	  "application/json");	
                    }

                    const options = {
                        headers         : params.headers,
                        params          : params.parseQuery(),
                        withCredentials : params.withCredentials,
                    };

                    /* LOAD */
                    this.core().loadingController.start(params.loading).then(() =>
                    {
                        this.http.get(params.apiUrl, params.getPost()).pipe(
                            catchError(e =>
                            {
                                /* LOAD */
                                this.core().loadingController.stop();
                                resolve(null);
                                return this.handleError(e);
                            })).subscribe(response =>
                        {
                            /* LOAD */
                            this.core().loadingController.stop();
                            resolve(response);
                        });
                    });
                });				
            });
        });
    }

    async createTask(name:string, path:string, data:any)
    {
        const url = (this.core().production ? this.core().apiUrl : "http://localhost:8080") + "/" + path;

        const task = {
            task : name,
            url  : url,
            data : data,
        }

        const result = await this.callable("taskApi/task", task);

        return result;
    }

    async callable(url:string, data:any, maps?:any, withCredentials?:boolean, noReference?:boolean)
    {
        return new Promise((resolve) => 
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
                {	
                    data = this.core().util.parseApiData(data);

                    if (environment["noCache"])
                    {
                        data.noCache = true;
                    }

                    //const callable = this.core().fireFunctions.httpsCallable(url);		
                    //const result   = await callable(data).toPromise();

                    const headers = new Headers();
                    //headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                    headers.append("Accept", 	  	 "application/json");	
                    headers.append("x-access-token", this.core().jwtToken);

                    let urlRequest = (this.core().production ? this.core().apiUrl : this.core().apiDevUrl) + "/" + url;

                    if (url.indexOf("http") != -1)
                    {
                        urlRequest = url;
                    }

                    const options : any = {
                        method  : "post",
                        headers : headers,
                        url     : urlRequest, 
                    };

                    if (withCredentials === undefined)
                    {
                        options.withCredentials = true;
                    }
                    else
                    {
                        options.withCredentials = withCredentials;
                    }

                    /* LOAD */
                    this.http.post(urlRequest, data, options).pipe(
                        catchError(e => 
                        {
                            /* LOAD */
                            //this.core().loadingController.stop();
                            resolve(null);
                            return this.handleError(e);
                        })).subscribe(async (result:any) => 
                    {
                        if (!maps)
                        {
                            maps = {};
                        }

                        if (result.data)
                        {
                            if (data.map)
                            {
                                if (maps.model)
                                {
                                    result.data = new maps.model(result.data);
                                    await result.data.reload();	
                                }
                            }
                            else
                            {
                                if (maps.model)
                                {
                                    result.data = new maps.model(this.convertApiData(result.data, noReference));
                                    await result.data.reload();
                                    await result.data.on();
                                }			
                                else
                                {
                                    result.data = this.convertApiData(result.data, noReference);
                                }	
                            }
                        }

                        if (result.collection)
                        {
                            let list : any = [];

                            if (maps.list)
                            {
                                list = new maps.list();
                            }

                            for (const item of result.collection)
                            {	
                                if (!data.map)
                                {
                                    for (const key in item)
                                    {
                                        item[key] = this.convertApiData(item[key], noReference);
                                    }						
                                }			

                                if (maps.list)
                                {
                                    list.add(item);
                                }
                                else
                                {
                                    list.push(item);
                                }
                            }

                            // RELOAD COM MAPITEMS
                            if (maps.list)
                            {
                                await list.doReloadData();
                            }

                            result.collection = list;
                        }

                        console.log("callable", url, data, result);

                        if (result.error)
                        { 
                            console.error(result.error, result.error.details);
                        }

                        resolve(result)
                    });
                });			
            });			
        })				
    }

    async callFormData(url:string, formData:FormData, withCredentials?:boolean, noReference?:boolean)
    {
        return new Promise((resolve) => 
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(async () => 
                {
                    await this.core().loadingController.start();

                    const headers = new Headers();
                    headers.append("x-access-token", this.core().jwtToken);

                    let urlRequest = (this.core().production ? this.core().apiUrl : this.core().apiDevUrl) + "/" + url;

                    if (url.indexOf("http") != -1)
                    {
                        urlRequest = url;
                    }

                    const options : any = {
                        headers : headers,
                    };

                    if (withCredentials === undefined)
                    {
                        options.withCredentials = true;
                    }
                    else
                    {
                        options.withCredentials = withCredentials;
                    }

                    /* LOAD */
                    this.http.post(urlRequest, formData, options).pipe(
                        catchError(e => 
                        {
                            /* LOAD */
                            this.core().loadingController.stop();
                            resolve(null);
                            return this.handleError(e);
                        })).subscribe((result:any) => 
                    {
                        if (result.data)
                        {
                            result.data = this.convertApiData(result.data, noReference);
                        }

                        if (result.collection)
                        {
                            for (const key in result.collection)
                            {
                                const data = result.collection[key];

                                for (const key2 in data)
                                {
                                    data[key2] = this.convertApiData(data[key2], noReference);
                                }				
                            }
                        }

                        console.log("callFormData", url, formData, result);

                        if (result.error)
                        { 
                            console.error(result.error, result.error.details);
                        }

                        this.core().loadingController.stop();

                        resolve(result)
                    });	
                });				
            });					
        });		
    }

    convertApiData(data:any, noReference:boolean)
    { 
        if (!data)
        {
            return data;
        }
        else if (!noReference && data instanceof Object && data.referencePath)
        {
            return doc(this.core().fireStore, data.referencePath);
        }
        else if (typeof data == "string" && this.isDate(data))
        {
            return new TDate({ value : data }).toDate();
        }
        else if (typeof data.toDate == "function")
        {
            return data.toDate();
        }
        else if (data instanceof Array)
        {			
            const items : any = [];

            for (const key in data)
            {
                const _data = this.convertApiData(data[key], noReference);
                items.push(_data);	
            }

            return items;
        }
        else if (data instanceof Object)
        {			
            const items : any = {};

            for (const key in data)
            {
                const _data = this.convertApiData(data[key], noReference); 
                items[key]  = _data	
            }

            return items;
        }
        else
        {
            return data;
        }				
    }

    isDate(date:string)
    {
        //https://stackoverflow.com/questions/3143070/javascript-regex-iso-datetime		
        return /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/.test(date);
    }

    /* METHODS */
    call(params:Params)
    {
        return new Promise<any>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(async () => 
                {
                    if (!params.headers)
                    {
                        params.headers = new Headers();
                        params.headers.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                        params.headers.append("Accept", 	  "application/json");	
                    }

                    const options : any = {
                        headers         : params.headers,
                        params          : params.parseQuery(),
                        withCredentials : params.withCredentials,
                    };

                    /* LOAD */
                    await this.core().loadingController.start();

                    this.http.request(params.apiUrl, params.getPost(), options).pipe(
                        catchError(e => 
                        {
                            /* LOAD */
                            this.core().loadingController.stop();
                            resolve(null);
                            return this.handleError(e);
                        })).subscribe((response:any) =>
                    {
                        const result = this.createResult(params);

                        /* CAST DATA */
                        if (response.data)
                        {
                            if (params.model)
                            {
                                result.data = new params.model(response.data);
                            }
                            else
                            {
                                result.data = response.data;
                            }
                        }

                        let collection : any;

                        /* CAST COLLECTION */
                        if (response.collection)
                        {
                            if (params.collectionModel)
                            {
                                collection = new params.collectionModel(response.collection);
                            }
                            else
                            {
                                collection = new Collection({
                                    params    : params,
                                    pageCount : response.pageCount,
                                });

                                for (const key in response.collection)
                                {
                                    if (params.model)
                                    {
                                        collection.push(new params.model(response.collection[key]));
                                    }
                                    else
                                    {
                                        collection.push(response.collection[key]);
                                    }
                                }
                            }
                        }

                        result.collection = collection;
                        result.count      = response.count;
                        result.total      = response.total;
                        result.status     = response.status;
                        result.form       = response.form;
                        result.message    = response.message || response.error;
                        result.pageCount  = response.pageCount;

                        /* LOAD */
                        this.core().loadingController.stop();

                        resolve(result);
                    });
                });				
            });
        });
    }

    public async handleError (error: Response | any)
    {
        let errMsg: string;

        if (error instanceof Response)
        {
            const body = await error.json() || "";
            const err  = body.error || JSON.stringify(body);
            errMsg     = `${error.status} - ${error.statusText || ""} ${err}`;
        }
        else
        {
            errMsg = error.message ? error.message : error.toString();
        }

        console.log(errMsg);

        return throwError(error);
    }
}
