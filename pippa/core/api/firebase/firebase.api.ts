import { Injectable, } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgZone } from "@angular/core";
import { map, catchError } from 'rxjs/operators';

/* FIREBASE */
//import { AngularFirestore } from '@angular/fire/firestore';

/* PIPPA */
import { BaseAPI }           from '../../api/base.api';
import { Result }            from '../../util/result/result';
import { Params }            from '../../util/params/params';
import { Query }             from '../../util/query/query';
import { BaseList }          from '../../model/base.list';
import { BaseModel }         from '../../model/base.model';
import { LoadingController } from '../../controller/loading/loading.controller';
import { Types } from '../../type/types';
import { doc, getDoc } from 'firebase/firestore';

@Injectable({
    providedIn: 'root'
})
export class FireBaseAPI extends BaseAPI
{
    public isFirebase : boolean = true;

    constructor(
        public http              : HttpClient,
        public zone              : NgZone,
        //public firestore  : AngularFirestore,
        public loadingController : LoadingController,
    )
    {
        super(http, zone, loadingController);

        //console.log('this.firestore', this.firestore);

        //this.core().fireStore  = this.firestore;
        this.core().loadingController = this.loadingController;
        this.core().zone              = this.zone;
    }

    /* LIST */
    getList(params:Params):Promise<Result>
    {
        return new Promise<Result>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
				{
                    this.loadingController.start(params.loading).then(async () =>
                    {
                        const result      = this.createResult(params);
                        result.collection = params.collection;

                        if(result.collection)
                        {
                            result.collection.reload().then(() =>
                            {                        
                                this.loadingController.stop().then(() => 
                                {
                                    result.status = true;
                                    resolve(result);
                                });                        
                            });
                        } 
                        else
                        {
                            result.status = true;
                            resolve(result);
                        }                       
                    });
                });                
            });
        });
    }

    getListReference : (params:Params) => Promise<Result> = function(params:Params)
    {
        return new Promise<Result>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
				{
                    this.loadingController.start(params.loading).then(() =>
                    {
                        let result    = this.createResult(params);
                        let reference = this.core().fireStore.collection(params.path);
    
                        console.log('getListReference', params.path);
    
                        if(params.collectionModel)
                        {
                            result.collection = new params.collectionModel();
                        }
                        else
                        {
                            result.collection = new BaseList();
                            params.model      = params.model;
                        }
                        
                        let query = new Query({
                            query   : reference.query,
                            where   : params.where,
                            orderBy : params.orderBy,
                            asc     : params.asc,
                            perPage : params.perPage,
                            page    : params.page,
                        });
    
                        result.collection.reference  = reference;
                        result.collection.appid      = params.appid;
                        result.status 				 = true;
                        result.collection.autoReload = params.autoReload;
                        result.collection.onlyCount  = params.onlyCount;
                        result.collection.setQuery(query);
    
                        this.loadingController.stop().then(() => 
                        {
                            resolve(result);
                        });                    
                    });
                });                
            });
        });
    }

    async getItems(path:string)
    {
        const collection = await this.core().fireStore.collection(path).get().toPromise();
        let items = [];

        for(let item of collection.docs)
        {
            items.push(item.data())
        }

        return items;
    }

    async getItem(path:any)
    {
        const docRef = doc(this.core().fireStore, path);

        // Obtém o documento
        const docSnap = await getDoc(docRef);

        return docSnap.data();
    }

    /* OBJECT */
    getObject(params:Params)
    {
        return new Promise<Result>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
				{
                    this.loadingController.start(params.loading).then( async () =>
                    {
                        console.log('path object', params.path);
                        
                        let result    = this.createResult(params);
                        //let reference = this.core().fireStore.doc(params.path);                    
    
                        //let snapshot  = reference.snapshotChanges();
    
                        /* PARA RESOLVER O ANSYNC */
                        //snapshot.first().subscribe(action =>
                        //const unsubscribe = snapshot.subscribe((action:any) =>
                        //{
                            //unsubscribe.unsubscribe();
    
                            //let data = {};
    
                            //if(!action.payload.exists)
                            //{
                                /* ESTAVA ZERANDO O OBJETO */
                                //reference.set(data);
                            //}
                            //else
                            //{
                                //data = action.payload.data()
                            //}
                        
                            // TODO getObject não faz reload no data
                            const resultData : any = await this.callable(params.getEndPoint, 
                            {
                                accid    : params.accid,
                                appid    : params.appid,
                                colid    : params.colid,
                                map		 : params.map,
                                mapItems : params.mapItems,
                                includeForm : params.includeForm, 
                                document : {
                                    referencePath : params.path
                                }
                            });
    
                            console.log('getObject', params.path, resultData.data);
    
                            result.data = params.model ? new params.model() : new BaseModel();
                            //result.data.exists    = action.payload.exists;
                            //result.data.reference = action.payload.ref // AQUI ADICIONA O ID;
                            result.data.populate(resultData.data);
                            await result.data.reload();
    
                            result.data.on().then(() =>
                            {
                                this.loadingController.stop().then(() => 
                                {
                                    result.status = true;                                
                                    resolve(result);    
                                });
                            });
                        });
                    //});
                });                
            });
        });
    }

    getObjectReference(params:Params)
    {
        return new Promise<Result>((resolve) =>
        {
            this.zone.runOutsideAngular(() =>
            {
                this.zone.run(() => 
				{
                    this.loadingController.start(params.loading).then(() =>
                    {
                        let result    = this.createResult(params);
                        let reference = this.core().fireStore.doc(params.path);
    
                        console.log('getObjectReference', params.path);
    
                        result.data            = new params.model();
                        result.data._reference = reference;
    
                        result.status = true;
    
                        this.loadingController.stop().then(() =>
                        {
                            resolve(result);
                        });                    
                    });
                });                
            });
        });
	}    
}
