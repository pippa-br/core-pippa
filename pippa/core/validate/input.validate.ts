import { AbstractControl, ValidatorFn, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { Observable } from "rxjs";
import validator from "validator";
import * as _ from "lodash" 

//https://dzone.com/articles/how-to-create-custom-validators-in-angular
import { Core }   		   from "../util/core/core";
import { Params }  		   from "../util/params/params";
import { WhereCollection } from "../model/where/where.collection";
import { Document } 	   from "../model/document/document";
import { DocumentPlugin }  from "../plugin/document/document.plugin";
import { environment }     from "src/environments/environment";
import { TDate }           from "../util/tdate/tdate";
import { FunctionsType } from "../type/functions/functions.type";

//@dynamic
export class InputValidate
{
    static loadingController : any;

    static custom(field:any, form:any, component:any) : AsyncValidatorFn
    {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> =>
        {
            return new Promise<ValidationErrors>(async (resolve) =>
            {
                if (field.setting && field.setting.customValidate)
                {
                    const fc       = FunctionsType.get().getFunction(field.setting.customValidate);                    
                    const validate = await fc(field, form, component);

                    resolve(validate);
                }
                else
                {
                    resolve(null);
                }
            });
        };
    }
    
    static search(field:any, form:any, component:any) : AsyncValidatorFn
    {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> =>
        {
            return new Promise<ValidationErrors>(async (resolve) =>
            {
                let type :any;
                let pathMaxCount : string;
                let mapItems : any;

                if (field.single)
                {
                    type = "single";
                }
                else if (field.setting && field.setting.autoComplete)
                {
                    if (form.isSetModeForm())
                    {
                        component.displayControl(true);
                    }

                    type = "autoComplete";
                }
                else if (field.setting && field.setting.maxCount)
                {
                    type         = "maxCount";
                    pathMaxCount = field.setting.maxCount;
                }

                if (field.setting && field.setting.mapItems)
                {
                    mapItems = field.setting.mapItems;
                }

                //const valueChanges = await control.valueChanges.toPromise()

                //console.error(valueChanges, control.pristine)

                if (!control.valueChanges || control.pristine || !type)
                {
                    resolve(null);
                }
                else
                {
                    if (control.pristine || control.value == "")
                    {
                        resolve(null);
                    }
                    else
                    {
                        /* LOADING */
                        component.isLoading = true;
                        //Core.get().loadingController.start();

                        /* PARA NÃO FAZER VARIAS BUSCA AO DIGITAR */
                        if (component._searchChangeSetTimeout)
                        {
                            clearTimeout(component._searchChangeSetTimeout);
                        }

                        component._searchChangeSetTimeout = setTimeout(async () =>
                        {							
                            const where = new WhereCollection();
                            const value = control.value;

                            if (typeof value == "string")
                            {
                                //value = value.toLowerCase(); // NÃO FAZER ISSO POR CONTA DE BUSCA FULL
                            }

                            /* FILTRO PARA O CAMPO UNIQUE, USEI PARA PEGAR UNIQUE DE DADOS APOS UMA DATA  */
                            if (field.singleFilters)
                            {
                                where.setItems(field.singleFilters);
                            }

                            where.add({
                                field : field.name,
                                value : value,
                                type  : field.type,
                            });	

                            /* PARA USUARIO LOGADO É UNICO POR USUARIO */
                            /*if(Core.get().user && !Core.get().user.isGuest())
							{
								where.add({
									field : 'owner',
									value : Core.get().user,
									type  : FieldType.type('ReferenceSelect'),
								});
							}*/

                            const filter = await where.parseFilter();
                            const app    = Core.get().getApp(form.appid);

                            /* LOAD DATA */
                            const plugin = await Core.get().injector.get(DocumentPlugin);

                            if (type == "maxCount")
                            {
                                const result = await plugin.count(Core.get().createParams({
                                    appid           : app.code,
                                    groupCollection : app.colid != "documents" && app.colid != "forms",
                                    where           : filter,
                                    loading         : false,
                                }));
			
                                const document : any = new Document(value);

                                document.on().then(() =>
                                {
                                    document.getProperty(pathMaxCount).then((value2) => 
                                    {
                                        console.log("count", type, filter, result.total, value2);

                                        if (value2 >= result.total)
                                        {
                                            resolve(null);                                            
                                        }
                                        else
                                        {
                                            resolve({ maxCount : true })
                                        }		
                                    });
                                });
                            }
                            else if (type == "single")
                            {
                                const result = await plugin.getData(Core.get().createParams({
                                    appid           : app.code,
                                    groupCollection : app.colid != "documents" && app.colid != "forms",
                                    perPage         : 1,
                                    where           : filter,
                                    orderBy         : "postdate",
                                    asc             : false,
                                    loading         : false,
                                    mapItems        : {
                                        referencePath : mapItems || environment.defaultMapItems
                                    },
                                    map : true,
                                }));
			
                                console.log("single", type, filter, result.collection);
								
                                if (result.collection && result.collection.length > 0)
                                {
                                    if (component.data)
                                    {
                                        let has = false;

                                        for (const key in result.collection)
                                        {
                                            const data = result.collection[key];

                                            if (component.data.id == data.id)
                                            {
                                                has = true;
                                                break;
                                            }
                                        }

                                        if (has)
                                        {
                                            resolve(null);                                                
                                        }
                                        else
                                        {
                                            resolve({ single : true })
                                        }
                                    }
                                    else
                                    {
                                        resolve({ single : true })
                                    }
                                }
                                else
                                {
                                    resolve(null);
                                }
                            }
                            else if (type == "autoComplete")
                            {
                                const result = await plugin.getData(Core.get().createParams({
                                    appid           : app.code,
                                    groupCollection : app.colid != "documents" && app.colid != "forms",
                                    perPage         : 1,
                                    where           : filter,
                                    orderBy         : "postdate",
                                    asc             : false,
                                    loading         : false,
                                    mapItems        : {
                                        referencePath : mapItems || environment.defaultMapItems
                                    },
                                    map : true,
                                }));
			
                                console.log("autoComplete", type, filter, result.collection);

                                if (result.collection && result.collection.length > 0)
                                {
                                    const document : any = new Document(result.collection[0]);

                                    document.on().then(() =>
                                    {
                                        component.displayControl(true);

                                        /* NO CASO DE AUTO COMPLETE DOS DADOS QUE ESTAO PREENCHIDOS TEM PRIORIDADE */
                                        for (const key in form.instance._initialData)
                                        {
                                            document[key] = _.merge({}, document[key], form.instance._initialData[key])
                                        }

                                        /* PARAMS DATA TEM PRIORIDADE */
                                        if (Core.get().queryParams && Core.get().queryParams.data)
                                        {
                                            for (const key in Core.get().queryParams.data)
                                            {
                                                document[key] = _.merge({}, document[key], Core.get().queryParams.data[key])
                                            }
                                        }

                                        /* RESET FLAGS */
                                        document._archive  = false;
                                        document._canceled = false;

                                        /* ATUALIZA MANUALMENTE POIST TEM O RESET NO METODO DATA */
                                        form.formGroup.patchValue(document.parseData());
                                        form.formGroup.markAsPristine();
                                        form.component.instance.data = document;
                                        form.instance.isEdit         = false;
                                        form.doAddModeForm();

                                        console.log("autoComplete", document.parseData());

                                        resolve(null);
                                    });
                                }
                                else
                                {
                                    component.displayControl(true);
                                    resolve(null);
                                }
                            }
                            else
                            {
                                resolve(null);
                            }

                            /* LOADING */
                            component.isLoading = false;
                            //Core.get().loadingController.stop();

                        }, 500);
                    }
                }
            });
        };
    }

    static compare(name): ValidatorFn
    {
        return (c: AbstractControl): {[key: string]: any} =>
        {
            if (!c.value)
            {
                return null;
            }

            // self value (e.g. retype password)
            const v = c.value;

            // control value (e.g. password)
            const e = c.root.get(name);

            // value not equal
            if (e && v !== e.value) return {
                compare : true
            }

            return null;
        }
    }

    static listMultiSelect(c: AbstractControl)
    {
        if (!c.value)
        {
            return null;
        }

        // self value (e.g. retype password) 
        const v = c.value;
        
        // value not equal
        if (v.length == 2 && v[0].items && v[1].items &&  v[0].items.length > 0 && v[1].items.length > 0) 
        {
            return null;
        }

        return {
            required : true
        }
    }

    static listMultiRadio(count:number): ValidatorFn
    {
        return (c: AbstractControl): {[key: string]: any} =>
        {
            const v : any = c.value;

            if (count == 0)
            {
                return null;
            }

            if (v && v.length == count)
            {
                for (const item of v)
                {
                    if (!item.id)
                    {
                        return {
                            listMultiRadio : true
                        }
                    }
                }

                return null;
            }
            else
            {
                return {
                    listMultiRadio : true
                }
            }
        }
    }

    static url(name:string): ValidatorFn
    {
        name;

        return (c: AbstractControl): {[key: string]: any} =>
        {
            const v : string = c.value;

            if (/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(v))
            {
                return null;
            }
            else
            {
                return {
                    url : true
                }
            }
        }
    }

    static zipcode(c:AbstractControl)
    {
        const v : string = c.value;

        if (/^[0-9]{5}-[0-9]{3}$/i.test(v))
        {
            return null;
        }
        else
        {
            return {
                zipcode : true
            }
        }
    }

    static date(c: AbstractControl)
    {
        let date : any;
        
        if (c.value === undefined || c.value === null)
        {
            return null;
        }
        else if (typeof c.value == "boolean")
        {           
            // *
        }
        else if (typeof c.value == "string")
        {			
            date =  new TDate({ value : c.value, mask : "dd/MM/yyyy" });
        }
        else 
        {
            date = new TDate({ value : c.value });
        }

        if (date && date.isValid())
        {
            return null;
        }
        else
        {
            return {
                date : true
            }
        }
    }

    static duplidateOption(c: AbstractControl)
    {
        if (!c.value)
        {
            return null;
        }

        const value : any = c.value;
        let duplidate     = false;
        let duplicates    = "";

        for (const key in value)
        {
            for (const key2 in value)
            {
                if (value[key].id != value[key2].id && value[key].value == value[key2].value)
                {
                    duplidate   = true;
                    duplicates += value[key].value + ", ";
                }
            }    
        }

        console.error("duplicates", duplicates);

        return !duplidate ? null : { duplicates : duplicates, duplidateOption : true };
    }

    static dayMonth(c: AbstractControl)
    {
        let date : any;
        
        if (!c.value)
        {
            return null;
        }
        else if (typeof c.value == "string" && c.value.length == 5)
        {			
            date = new TDate({ value : c.value, mask : "DD/MM" });
        }

        if (date && date.isValid())
        {
            return null;
        }
        else
        {
            return {
                date : true
            }
        }
    }
	
    static monthYear(c: AbstractControl)
    {
        let date : any;
        
        if (!c.value)
        {
            return null;
        }
        else if (typeof c.value == "string" && c.value.length == 5)
        {			
            date = new TDate({ value : c.value, mask : "MM/YY" });
        }

        if (date && date.isValid())
        {
            return null;
        }
        else
        {
            return {
                date : true
            }
        }
    }
	
    static year(c: AbstractControl)
    {
        let date : any;
        
        if (!c.value)
        {
            return null;
        }
        else if (typeof c.value == "string" && c.value.length == 4)
        {			
            date = new TDate({ value : c.value, mask : "yyyy" });
        }

        if (date && date.isValid())
        {
            return null;
        }
        else
        {
            return {
                date : true
            }
        }
    }

    static creditCard(c: AbstractControl)
    {
        if (!c.value)
        {
            return null;
        }
		
        const v: string = c.value;
		
        return validator.isCreditCard(v) ? null : { creditCard : true };
    }

    static agree(formControl : AbstractControl)
    {
        if (!formControl.value)
        {
            return {
                agree : true,
            };
        }
        else
        {
            return null;
        }
    }

    static email(formControl : AbstractControl)
    {
        if (!formControl.value)
        {
            return null;
        }

        const EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!EMAIL_REGEXP.test(formControl.value))
        {
            return {
                email : true,
            };
        }
        else
        {
            return null;
        }
    }

    static cpf(c: AbstractControl)
    {
        if (!c.value)
        {
            return null;
        }

        const cpf = c.value.replace(/[^\d]+/g, "");
        if (cpf == "") return {
            cpf : true
        };
        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return {
                cpf : true
            };
        // Valida 1o digito
        let add = 0;
        for (let i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        let rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return {
                cpf : true
            };
        // Valida 2o digito
        add = 0;
        for (let i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return {
                cpf : true
            };

        return null;
    }

    static cnpj(formControl: AbstractControl)
    {
        if (!formControl.value)
        {
            return null;
        }

        let c   = formControl.value;
        const b = [ 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 ];

        if ((c = c.replace(/[^\d]/g, "")).length != 14)
        {
            return {
                cnpj : true
            };
        }

        if (/0{14}/.test(c))
        {
            return {
                cnpj : true
            };
        }

        for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);

        if (c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        {
            return {
                cnpj : true
            };
        }

        for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);

        if (c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        {
            return {
                cnpj : true
            };
        }

        return null;
    }

    static createResult(params:Params)
    {
        return Core.get().resultFactory.create(params);
    }

    static createParams(args)
    {
        return Core.get().paramsFactory.create(args);
    }
}
