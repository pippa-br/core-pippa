import { Component, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { PopoverController, AlertController } from "@ionic/angular";
import { FormControl, } from "@angular/forms";

/* PIPPA */
import { BaseInput }       from "../../input/base.input";
import { FormFormPopover } from "../../../popover/form.form/form.form.popover";
import { Form }            from "../../../model/form/form";
import { Field }           from "../../../model/field/field";
import { FormItem }        from "../../../model/form.item/form.item";
import { FieldType }       from "../../../type/field/field.type";
import { Grid }            from "../../../model/grid/grid";
import { FormItemPopover } from "../../../popover/form.item/form.item.popover";

@Component({
    selector        : ".grid-setting",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'has-grid' : grid}">
                                <input type="hidden" formControlName="{{field.name}}" (ngModelChange)="onModelChange($event)"/>
                                <ion-button clear (click)="setGrid()">
                                    <ion-icon name="grid-outline"></ion-icon>
                                </ion-button>
                                <p class="links-bar">
                                    <a (click)="onGridClear()"><small>limpar</small></a>
                                </p>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class GridSetting extends BaseInput implements OnDestroy
{
    public parent            : string;
    public popoverController : PopoverController;
    public alertController   : AlertController;
    public grid              : Grid;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.field.name + "-input input col grid-setting ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        this.grid = new Grid();

        const formItensOption = this.formItem.getFormItensOption();

        this.form = new Form({
            items : [
                {
                    step : {
                        id : 0,
                    },
                    row   : 1,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SELECT"),
                        name        : "type",
                        label       : "Tipo",
                        placeholder : "Tipo",
                        required    : true,
                        option      : {
                            record : false,
                            items  : [
                                Grid.TABLE_TYPE,
                                Grid.TILE_TYPE,
                                Grid.CARD_TYPE,
                            ]
                        },
                    })
                },
                /*{
                    step : {
                        id : 0,
                    },
                    row : 2,
                    col : 1,
                    field : new Field({
                        type        : FieldType.type('SELECT'),
                        name        : 'orderBy',
                        label       : 'Ordenar',
                        placeholder : 'Ordenar',
                        option      : orderOptions,
                        required    : true,
                        setting     : {
                            bindValue : 'id',
                        }
                    })
                },
                {
                    step : {
                        id : 0,
                    },
                    row : 3,
                    col : 1,
                    field : new Field({
                        type        : FieldType.type('SELECT'),
                        name        : 'asc',
                        label       : 'Tipo de Ordenação',
                        placeholder : 'Tipo de Ordenação',
                        required    : false,
                        setting     : {
                            bindValue : 'id',
                        },
                        option : {
                            items : [
                                {
                                    id       : 1,
                                    label    : 'Crescente',
                                    value    : true,
                                    selected : true,
                                },
                                {
                                    id    : 0,
                                    label : 'Decrescente',
                                    value : false,
                                }
                            ]
                        },
                    })
                },*/
                {
                    step : {
                        id : 0,
                    },
                    row   : 4,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("ColumnGrid"),
                        name        : "items",
                        label       : "Items",
                        placeholder : "Items",
                        required    : false,
                        option      : formItensOption,
                    })
                },
                {
                    step : {
                        id : 0,
                    },
                    row   : 5,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("Select"),
                        name        : "orderBy",
                        label       : "Ordenar Por",
                        placeholder : "Ordenar Por",
                        required    : false,
                        option      : formItensOption,
                    }),
                    setting : 
                    {
                        bindValue     : "id",
                        bindLabel     : "_label",
                        searchable    : true,
                        onSetSelected : ((event:any) => 
                        {
                            const formItem = new FormItem(event.data);

                            formItem.on().then(() => 
                            {
                                this.openFormItem(event.target, formItem, formItensOption);
                            });                            
                        })
                    }
                },
                {
                    step : {
                        id : 0,
                    },
                    row   : 6,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "buttons",
                        label       : "Buttões",
                        placeholder : "Buttões",
                        required    : false,                            
                    }),
                    items : [ {
                        row   : 1,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("SelectIcon"),
                            name        : "icon",
                            label       : "Icon",
                            placeholder : "Icon",
                        })
                    },
                    {
                        row   : 1,
                        col   : 2,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "label",
                            label       : "Label",
                            placeholder : "Label",
                        })
                    },
                    {
                        row   : 1,
                        col   : 3,
                        field : new Field({
                            type        : FieldType.type("Text"),
                            name        : "router",
                            label       : "Rota",
                            placeholder : "Rota",
                        })
                    },
                    {
                        row   : 2,
                        col   : 1,
                        field : new Field({
                            type        : FieldType.type("ItemsSetting"),
                            name        : "params",
                            label       : "Parametros",
                            placeholder : "Parametros",
                            required    : false,
                        })
                    } ]
                },
            ]
        });
    }    

    onGridClear()
    {
        this.grid = null;
        this.setValue(null);
    }

    onModelChange(data:any)
    {
        if (data && !this.dirty)
        {
            this.grid  = new Grid(data);
            this.dirty = true;
        }
    }

    /* FIELD */
    setGrid()
    {
        this.popoverController.create(
            {
                component : FormFormPopover,
                componentProps :
            {
                data  : this.grid,
                form  : this.form,
                onAdd : (async (event:any) =>
                {
                    this.grid = new Grid(event.data);
                    await this.grid.on();
                    //this.formItem.grid = this.grid;
                    this.setValue(event.data);

                    this.popoverController.dismiss();
                }),
                onSet : (async (event:any) =>
                {
                    this.grid.populate(event.data);
                    await this.grid.on();
                    //this.formItem.grid = this.grid;
                    this.setValue(event.data);
					
                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
                showBackdrop : false,
                cssClass     : "right-popover",
            //enableBackdropDismiss : false,
            })
        .then(popover =>
        {
            popover.present();
        });
    }

    openFormItem(target:any, formItem:any, option:any)
    {
        const popoverController = this.core().injector.get(PopoverController);

        popoverController.create(
            {
                component : FormItemPopover,
                componentProps :
            {
                data   : formItem,
                acl    : this.form.instance.acl,
                option : option,
                onSet  : ((event:any) =>
                {
                    formItem.populate(event.data);
                    target.setValue(formItem.parseData());

                    popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    popoverController.dismiss();
                }),
            },
                showBackdrop : false,
                cssClass     : "right-popover",
            })
        .then((popover:any) =>
        {
            popover.present();
        });
    }
}
