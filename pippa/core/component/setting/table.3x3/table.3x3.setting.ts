import { Component, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../../input/base.fieldset.input';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { FieldType }          from '../../../type/field/field.type';
import { Field }              from '../../../model/field/field';

@Component({
    selector		: `.table3x3-setting`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>`,
})
export class Table3x3Setting extends BaseFieldsetInput implements AfterViewInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.field.name + '-setting setting col table3x3-setting ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
		
		await this.createComponents();
    }

    async createComponents()
    {
        const items = new FormItemCollection([
            {
                row : 1,
                field : new Field({
                    type        : FieldType.type('SubForm'),
                    name        : 'categories',
                    label       : 'Categorias',
                    placeholder : 'Categorias',
                    owner       : this,
                    onInit      : (event:any) =>
                    {
                        if(event.data && event.data.length)
                        {
                            const item = this.formItem.items.getByFieldName('option');
                            item.field.option = {
                                items : event.data
                            };
                        }
                    },
                    onChange : (event:any) =>
                    {
                        if(event.data && event.data.length)
                        {
                            const item = this.formItem.items.getByFieldName('option');
                            item.field.option = {
                                items : event.data
                            };
                        }
                    }
                }),
                items : [
                    {
                        row : 0,
                        col : 0,
                        field : new Field({
                            type        : FieldType.type('text'),
                            name        : 'label',
                            label       : 'Label',
                            placeholder : 'Label',
                        })
                    },
                    {
                        row : 0,
                        col : 1,
                        field : new Field({
                            type        : FieldType.type('text'),
                            name        : 'value',
                            label       : 'Valor',
                            placeholder : 'Valor',
                        })
                    },
                    {
                        row : 2,
                        col : 0,
                        field : new Field({
                            type        : FieldType.type('RichText'),
                            name        : 'text',
                            label       : 'Texto',
                            placeholder : 'Texto',
                            viewColummn : false,
                        })
                    }
                ]
            },
            {
                row : 2,
                field : new Field({
                    type        : FieldType.type('SubForm'),
                    name        : 'rows',
                    label       : 'Afirmações',
                    placeholder : 'Afirmações',
                    //initial     : formItem.field.initial.rows,
                }),
                items : [
                    {
                        row : 0,
                        col : 0,
                        field : new Field({
                            type        : FieldType.type('text'),
                            name        : 'label',
                            label       : 'Label',
                            placeholder : 'Label',
                            //initial     : formItem.field.initial.rows,
                        })
                    },
                    {
                        row : 0,
                        col : 1,
                        field : new Field({
                            type        : FieldType.type('Select'),
                            name        : 'option',
                            label       : 'Categoria',
                            placeholder : 'Categoria',
                            //initial     : formItem.field.initial.rows,
                        })
                    }
                ]
            },
            {
                row : 3,
                field : new Field({
                    type        : FieldType.type('ItemsSetting'),
                    name        : 'columns',
                    label       : 'Respostas',
                    placeholder : 'Respostas',
                    initial     : this.formItem.field.initial.columns,
                })
            },
        ]);

		items.doSort();
		
		await this.formItem.items.updateItems(items);
    }
}
