import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController} from '@ionic/angular'

/* PIPPA */
import { Collection }       from '../../../util/collection/collection';
import { BaseInput }        from '../../input/base.input';
import { BaseList }         from '../../../model/base.list';
import { FormFormPopover }  from '../../../popover/form.form/form.form.popover';
import { BaseModel }        from '../../../model/base.model';
import { Form }             from '../../../model/form/form';
import { FieldType }        from '../../../type/field/field.type';

@Component({
    selector : `.disc-setting`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : form?.submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input type="hidden" formControlName="{{field.name}}" (ngModelChange)="onModelChange($event)"/>
                        <ion-grid class="list">

                            <!--- ROWS --->

                            <div *ngFor="let item of rows; let last = last;">
                                <ion-row>
                                    <ion-col size="11">
                                        <input type="text"
                                               [(ngModel)]="item.label"
                                               [ngModelOptions]="{standalone: true}"
                                               placeholder="Texto"/>
                                    </ion-col>
                                    <ion-col size="1" class="del-button">
                                        <a (click)="onRemove(item)">
                                            <ion-icon name="remove-circle-outline"></ion-icon>
                                        </a>
                                    </ion-col>
                                </ion-row>
                                <ion-row *ngFor="let i of [0,1]; let last2 = last;">
                                    <ion-col size="8">
                                        <input type="text"
                                               [(ngModel)]="item.options[i].label"
                                               [ngModelOptions]="{standalone: true}"
                                               placeholder="Texto"/>
                                    </ion-col>
                                    <ion-col size="4">
                                        <ng-select [items]="items"
                                                   [placeholder]="'Selecione'"
                                                   [(ngModel)]="item.options[i].value"
                                                   [ngModelOptions]="{standalone: true}"
                                                   cancelText="Fechar"
                                                   okText="Selecionar"
                                                   (change)="updateOptions()"
                                                   (clear)="onDeselect(item)">
                                        </ng-select>
                                    </ion-col>
                                </ion-row>
                            </div>
                            <ion-row>
                                <ion-col size="5">
                                </ion-col>
                                <ion-col size="1" align-self-end class="add-button">
                                    <a (click)="onAdd()">
                                        <ion-icon name="add-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>

                            <!--- DOCUMENTS --->

                            <ion-row>
                                <ion-col>
                                    <h4 class="label-title">
                                        <span>Documentos</span>
                                    </h4>
                                </ion-col>
                            </ion-row>
                            <ion-row *ngFor="let item of documents">
                                <ion-col size="9">
                                    <ng-select [items]="optionsDisc"
                                               [placeholder]="'Selecione'"
                                               [(ngModel)]="item.name"
                                               [ngModelOptions]="{standalone: true}"
                                               cancelText="Fechar"
                                               okText="Selecionar"
                                               (change)="updateOptions()"
                                               (clear)="onDeselect(item)">
                                    </ng-select>
                                </ion-col>
                                <ion-col size="1" class="open-button">
                                    <a (click)="openDocument(item)">
                                        <ion-icon name="create-outline"></ion-icon>
                                    </a>
                                </ion-col>
                                <ion-col size="1" class="del-button" >
                                    <a (click)="onDelDocument(item)">
                                        <ion-icon name="remove-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                            <ion-row>
                                <ion-col size="5">
                                </ion-col>
                                <ion-col size="1" class="add-button">
                                    <a (click)="onAddDocument()">
                                        <ion-icon name="add-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                        </ion-grid>
                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class DiscSetting extends BaseInput
{
    public isInit      : boolean        = false;
    public rows        : BaseList<any>  = new BaseList<any>();
    public documents   : Collection     = new Collection();
    public optionsDisc : Array<string>  = ['D','I','S','C'];
    public items       : Array<string>  = ['A','B','D','E'];

    constructor(
        public modalController : ModalController,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.field.name + '-input input col disc-setting setting ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
    }

	createValidators()
	{
        let validators = [];

        if(this.field.required)
        {
            validators.push(Validators.required);
        }
	}

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.rows.setItems(value.rows);
            this.documents.setItems(value.documents);
            this.dirty = true;
        }
    }

    onAddDocument()
    {
        this.documents.push({
            id    : this.core().util.randomString(8),
            name  : '',
            text  : '',
            order : this.rows.length,
        });
    }

    openDocument(item:any)
    {
        const form = new Form({
            items : [
                {
                    field : {
                        name : 'text',
                        type : FieldType.type('RichText'),
                    }
                }
            ]
        });

        const model = new BaseModel();
        model.populate(item);

        this.modalController.create({
            component       : FormFormPopover,
            componentProps  : {
                data : model,
                form : form,
                onAdd : (event:any) =>
                {
                    item.text = event.data.text;
                    this.updateOptions();
                    this.modalController.dismiss();
                },
                onSet : (event:any) =>
                {
                    item.text = event.data.text;
                    this.updateOptions();
                    this.modalController.dismiss();
                },
                onClose : () =>
                {
                    this.modalController.dismiss();
                }
            },
            showBackdrop    : true,
            backdropDismiss : true,
        })
        .then((modal:any) =>
        {
            modal.present();
        });
    }

    onDeselect(item:any)
    {
        item.options = [{
            label : '',
            value : '',
        },
        {
            label : '',
            value : '',
        }];

        this.updateOptions();
    }

    onAdd()
    {
        this.rows.push({
            id       : this.core().util.randomString(8),
            options  : [{
                label : '',
                value : '',
            },
            {
                label : '',
                value : '',
            }],
            label : '',
            order : this.rows.length,
        });

        //this.updateOptions();
    }

    onRemove(item:any)
    {
        this.rows.del(item);
        this.updateOptions();
    }

    onDelDocument(item:any)
    {
        this.documents.del(item);
        this.updateOptions();
    }

    updateOptions()
    {
        this.isInit = true;

        let rows      = [];
        let documents = [];

        for(let key in this.rows)
        {
            if(this.rows[key].options[0] && this.rows[key].options[0].value != '' ||
               this.rows[key].options[1] && this.rows[key].options[1].value != '')
            {
                rows.push({
                    id      : this.rows[key].id,
                    label   : this.rows[key].label,
                    options : this.rows[key].options,
                    order   : this.rows[key].order,
                });
            }
        }

        for(let key in this.documents)
        {
            if(this.documents[key].name != '' && this.documents[key].text != '')
            {
                documents.push({
                    id    : this.documents[key].id,
                    name  : this.documents[key].name,
                    text  : this.documents[key].text,
                    order : this.documents[key].order,
                });
            }
        }

        if(rows.length > 0)
        {
            this.setValue({
                rows      : rows,
                documents : documents,
            });
        }
        else
        {
            this.setValue('');
        }
    }
}
