import { Component, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../../input/base.fieldset.input';
import { FieldType }          from '../../../type/field/field.type';
import { Field }              from '../../../model/field/field';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';

@Component({
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: '<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>',
})
export class FieldTypeSetting extends BaseFieldsetInput implements AfterViewInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.field.name + '-input input field-type-input col ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
		
		await this.createComponents();
    }

    async createComponents()
    {
        let items = new FormItemCollection([{
                row : 2,
                col : 0,
                field : new Field({
                    type        : FieldType.type('MultiSelect'),
                    name        : 'items',
                    label       : 'Tipos de Campos',
                    placeholder : 'Tipos de Campos',
                    option     : {
                        items : FieldType.getItems(),
                    },
                    onChange    : (value:any) =>
                    {
                        this.setValue(value.data);
                    }
                })
        }]);

        items.doSort();
		
		await this.formItem.items.updateItems(items);
    }
}
