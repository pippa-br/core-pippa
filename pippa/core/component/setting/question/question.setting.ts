import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* FIELD */
import { BaseInput }  from '../../input/base.input';
import { Collection } from '../../../../core/util/collection/collection';

@Component({
    selector 		: `.question-setting`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : form?.submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup">
								<input type="hidden" formControlName="{{field.name}}" (ngModelChange)="onModelChange($event)"/>
								<ion-grid class="list">
									<ion-row *ngFor="let item of inputs; let i = index; let last = last;">
										<ion-col size="8">
											<input type="text"
												[(ngModel)]="item.label"
												[ngModelOptions]="{standalone: true}"
												(ngModelChange)="updateItem()"
												placeholder="{{item.placeholder}}"/>
										</ion-col>
										<ion-col size="2">
											<input id="item-{{i}}" type="checkbox" [value]="i" [checked]="item.correct" (change)="onCheckbox(item)"/>
											<label for="item-{{i}}"></label>
										</ion-col>
										<ion-col size="1" class="del-button">
											<a (click)="onRemove(item)">
												<ion-icon name="remove-circle"></ion-icon>
											</a>
										</ion-col>
										<ion-col size="1" class="add-button">
											<a *ngIf="last" (click)="onAdd()">
												<ion-icon name="add-circle-outline"></ion-icon>
											</a>
										</ion-col>
									</ion-row>
									<ion-row *ngIf="inputs.length == 0">
										<ion-col size="5">
										</ion-col>
										<ion-col size="1" class="add-button" align-self-end>
											<a (click)="onAdd()">
												<ion-icon name="add-circle-outline"></ion-icon>
											</a>
										</ion-col>
									</ion-row>
								</ion-grid>
								<error-input [control]="formControl" [submitted]="submitted">
								</error-input>
							</div>
						</div>`
})
export class QuestionSetting extends BaseInput
{
    inputs     = new Collection();
    countLabel = 1;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(field:any, formGroup:any)
    {
        await super.createControls(field, formGroup);

        this.nameClass += this.field.name + '-input input setting col question-setting ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.field.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onAdd()
    {
        this.inputs.push({
            id          : this.core().util.randomString(8),
            correct     : false,
            placeholder : 'Opção ' + (this.countLabel),
            label       : '',
        });

        this.countLabel++;
    }

    onCheckbox(item:any)
    {
        for(let key in this.inputs)
        {
            this.inputs[key].correct = false;
        }

        item.correct = true;
        this.updateItem();
    }

    onRemove(item:any)
    {
        this.countLabel--;
        this.inputs.del(item);
        this.updateItem();
    }

    updateItem()
    {
        let items = [];

        for(let key in this.inputs)
        {
            if(this.inputs[key].value != '')
            {
                items.push({
                    id      : this.inputs[key].id,
                    correct : this.inputs[key].correct,
                    label   : this.inputs[key].label,
                });
            }
        }

        if(items.length > 0)
        {
            this.setValue({
                items : items
            });
        }
        else
        {
            this.setValue('');
        }
    }

    onModelChange(data:any)
    {
        if(data && !this.dirty)
        {
			/* CADASTROS ANTIGOS */
			if(data[0])
			{
				const items = [];

				for(const key in data)
				{
					const value : any = key;

					if(!isNaN(value))
					{
						items.push(data[key]);
					}
				}

				data.items = items;
			}

            this.inputs.setItems(data.items);
            this.dirty = true;
        }
    }
}
