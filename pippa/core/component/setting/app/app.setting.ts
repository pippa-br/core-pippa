import { Component, ChangeDetectionStrategy, AfterViewInit, ChangeDetectorRef } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../../input/base.fieldset.input';
import { FieldType }          from '../../../type/field/field.type';
import { Field }              from '../../../model/field/field';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';

@Component({
    selector        : `.app-setting`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>`,
})
export class AppSetting extends BaseFieldsetInput implements AfterViewInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.field.name + '-input input col app-setting ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;

		await this.createComponents();
    }

    async createComponents()
    {
        const items = new FormItemCollection([{
            id  : 'app',
            row : 1,
            col : 1,
            field : new Field({
                id    : 'app',
                type  : FieldType.type('ReferenceApps'),
                name  : 'app',
                label : 'Referência',
            })
        }]);

        await this.formItem.items.updateItems(items);        
    }
}
