import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseInput }    from "../../input/base.input";
import { BaseList }     from "../../../model/base.list";
import { BaseModel }    from "../../../model/base.model";
import { UploadPlugin } from "../../../plugin/upload/upload.plugin";
import { InputValidate } from "../../../validate/input.validate";

@Component({
    selector      		: ".items-setting",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(set)="onSet($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup">
								<input type="hidden" formControlName="{{field.name}}" (ngModelChange)="onModelChange($event)"/>
								
								<input #fileInput
									type="file"
									style="display: none;"
									[accept]="accept"
									(change)="onUpload($event)" />
									
								<table class="striped list">
									<tr *ngFor="let item of inputs; let last = last;">
										<td *ngIf="showID">
											<div>
												<input value="{{item.id}}" readonly/>
											</div>
										</td>
										<td>
											<div>
												<input type="text"
													[(ngModel)]="item.label"
													[ngModelOptions]="{standalone: true}"
													[readonly]="item.value == 'master'"
													(keyup)="optionLabel(item)"
													placeholder="Label"/>
											</div>
										</td>
										<td *ngIf="(_hasValue) || (core().user && core().user.isMaster())">
											<div>
												<input type="text"
													[(ngModel)]="item.value"
													[ngModelOptions]="{standalone: true}"
													[readonly]="item.value == 'master'"
													(keyup)="optionValue(item)"												   
													placeholder="Valor"/>
											</div>
										</td>
										<td *ngIf="hasDescription">
											<div>
												<input type="text"
													[(ngModel)]="item.description"
													[ngModelOptions]="{standalone: true}"
													(keyup)="optionValue(item)"												   
													placeholder="Descrição"/>
											</div>
										</td>
										<td *ngIf="hasOrder">
											<div>
												<ng-select
														[ngModel]="item.order"
														[ngModelOptions]="{standalone: true}"
														cancelText="Fechar"
														okText="Selecionar"
														[items]="orders"
														[bindLabel]="'label'"
										   				[bindValue]="'value'"
														notFoundText="Item não encontrado"
														(change)="optionOrder(item, $event)">
												</ng-select>
											</div>
										</td>									
										<td class="buttons-col">
											<div class="buttons-div">
												<ion-button *ngIf="hasImage && !item.image" (click)="addImage(item)">
													<ion-icon name="attach-outline"></ion-icon>
												</ion-button>
												<ion-button *ngIf="hasImage && item.image" (click)="delImage(item)">
												<ion-icon name="close-outline"></ion-icon>
												</ion-button>												
												<ion-button (click)="onRemove(item)" *ngIf="item.value != 'master'">
													<ion-icon name="remove-outline"></ion-icon>
												</ion-button>
												<ion-button *ngIf="last" (click)="onAdd()">
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
										</td>								
									</tr>
									<tr *ngIf="inputs.length == 0" class="empty-row" (click)="onAdd()">
										<td>
											<div>
												<label>Nenhum item cadastrado!</label>
											</div>
										</td>
										<td class="buttons-col">
											<div class="buttons-div">
												<ion-button>
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
										</td>
									</tr>
								</table>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('duplidateOption')">Chaves Duplicadas</span>
							</error-input>
						</div>`
})
export class ItemsSetting extends BaseInput
{
	@ViewChild("fileInput", { static : false }) fileInput: ElementRef;

	public inputs         : BaseList<any> = new BaseList<any>();
	public orders		  : [];
	public countLabel       = 1;
	public showID          = false;
	public hasImage        = false;
	public hasOrder        = false;
	public hasDescription  = false;
	public _hasValue 	   = false;
	public accept           = "jpg,png,sgv";
	public selectedItem   : any;

	constructor(
		public uploadPlugin      : UploadPlugin,
        public changeDetectorRef : ChangeDetectorRef
	)
	{
	    super(changeDetectorRef);
	}

	async createControls(formItem:any, formGroup:any)
	{
	    await super.createControls(formItem, formGroup);

	    //this.field.label = 'Opções';

	    this.nameClass += this.field.name + "-input input col items-setting ";
	    this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
	}

	createValidators()
	{
	    let orderBy     = "order";
	    this.validators = [];

	    if (this.field.required)
	    {
	        this.validators.push(Validators.required);
	    }

	    this.validators.push(InputValidate.duplidateOption);

	    /* ITEMS DEFAULT */
	    if (this.setting.items)
	    {
	        this.inputs.setItems(this.field.setting.items);
	    }
		
	    if (this.setting.showID)
	    {
	        this.showID = this.setting.showID
	    }

	    if (this.setting.hasImage !== undefined)
	    {
	        this.hasImage = this.setting.hasImage;
	    }

	    if (this.setting.hasDescription !== undefined)
	    {
	        this.hasDescription = this.setting.hasDescription;
	    }

	    if (this.setting.hasValue !== undefined)
	    {
	        this._hasValue = this.setting.hasValue;
	    }

	    if (this.setting.hasOrder !== undefined)
	    {
	        this.hasOrder = this.setting.hasOrder;
	    }		

	    if (this.setting.orderBy !== undefined)
	    {
	        orderBy = this.setting.orderBy;
	    }

	    this.inputs.setSort((a, b) =>
	    {
	        if (a[orderBy] > b[orderBy]) 
	        {
			  	return 1;
	        }

	        if (a[orderBy] < b[orderBy]) 
	        {
			  	return -1;
	        }
			
	        return 0;
	    });
	}

	createOrders()
	{
	    const orders : any = [];

	    for (const key in this.inputs)
	    {
	        orders.push({
	            label : Number(key) + 1,
	            value : Number(key) + 1
	        });
	    }

	    this.orders = orders;
	}
	
	addImage(item)
	{
	    this.selectedItem = item;
	    this.fileInput.nativeElement.click();
	}
	
	onUpload(event:any)
	{
	    this.uploadPlugin.uploadMultiple(event.target.files).then((files:any) =>
	    {
	        this.selectedItem.image = files[0];

	        this.fileInput.nativeElement.value = "";
			
	        this.updateOption();
	    });
	}

	delImage(item)
	{
	    delete item.image;
	    this.updateOption();
	}

	onAdd()
	{
	    this.inputs.push(new BaseModel({
	        id        		: this.core().util.randomString(8),
	        value     		: "",
	        label     		: "",
	        image     		: null,
	        description : "",
	        order     		: this.inputs.length,
	    }));

	    this.countLabel++;

	    this.createOrders();
	}

	onRemove(item:any)
	{
	    this.inputs.del(item);
	    this.updateOption();
	}	

	optionLabel(item:any)
	{
	    if (this.core().user && this.core().user.isMaster())
	    {
	        this.updateOption();
	    }
	    else /* USUARIO NAS MATER NÃO TEM ACESSO AO VALUE */
	    {
	        item.value = item.label;
	        this.updateOption();
	    }
	}

	optionValue(item:any)
	{
	    this.updateOption();
	}

	optionOrder(item:any, data)
	{
	    item.order = data.value + (item.order < data.value ? 0.1 : -0.1);

	    this.inputs.doSort();
		
	    for (const key in this.inputs)
	    {
	        this.inputs[key].order = Number(key) + 1;
	    }

	    this.updateOption();
	}

	doSubmitted()
	{
	    this.inputs.doSort();
	    this.updateOption();
	}

	updateOption()
	{
	    const itens = [];			

	    for (const key in this.inputs)
	    {
	        if (this.inputs[key].value !== "")
	        {
	            const data = this.inputs[key];
	            let  value = data.value;

	            if (value == "false")
	            {
	                value = false;
	            }
	            else if (value == "true")
	            {
	                value = true;
	            }
	            else if (typeof value == "string" && value.charAt(0) == "0" && value.length > 1)
	            {
	                // PARA NÃO CONVERSER EM NUMERO TIPO 01
	            }
	            else if (this.core().util.isNumber(value))
	            {
	                value = Number(value);
	            }

	            data.value = value;

	            const _data : any = {
	                id 	  : data.id,
	                value : data.value,
	                label : data.label
	            };

	            if (this.hasImage && data.image)
	            {
	                _data.image = data.image;
	            }

	            if (this.hasDescription && data.description)
	            {
	                _data.description = data.description;
	            }	

	            if (this.hasOrder && data.order !== undefined)
	            {
	                _data.order = data.order;
	            }	

	            itens.push(_data);
	        }
	    }

	    this.setValue(itens);

	    console.log("update items", itens, this.inputs);
	}

	onModelChange(data:any)
	{
	    // NAO ATUALIZAR ITEMS VAZIO
	    if (data && data.length > 0 && !this.dirty)
	    {
	        this.inputs.updateItems(data);
	        this.createOrders();

	        this.dirty = true;
	        this.emitChange(data);
	    }
	}
}
