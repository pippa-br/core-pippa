import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector		: `.toggle-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<p *ngIf="output?.value">
					   		<ion-icon name="checkmark-outline" class="icon icon-md item-icon"></ion-icon>
					   </p>`
})
export class ToggleOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'toggle-output ';
    }
}
