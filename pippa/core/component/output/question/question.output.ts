import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.question-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p *ngIf="output">							
							<label *ngFor="let option of output.formItem.field.option.items; let first = first;"
							       [ngClass]="{'selected' : output.formItem.field.option.items[output.value].id == option.id}">
								<span>{{option.label}}</span> <ion-icon *ngIf="option.correct" name="checkmark-circle-outline"></ion-icon>
							</label>							
						</p>`
})
export class QuestionOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'question-output ';
    }
}
