import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }    from '../base.output';
import { FunctionsType } from '../../../type/functions/functions.type';

@Component({
    selector        : `.function-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<span>{{output?.value}}</span>`
})
export class FunctionOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'function-output ';
    }

    onClick(e?:any)
    {
        if(FunctionsType.get().getFunction(this.output.formItem.path))
        {
            if(FunctionsType.get().getFunction(this.output.formItem.path + ':click'))
            {
                FunctionsType.get().getFunction(this.output.formItem.path + ':click')({
                    app  : this.core().currentApp,
                    data : this.output.data
                });
            }
        }
        else
        {
            console.log('FunctionsType.get().getFunction not found', this.output.formItem.path);
        }
    }
}
