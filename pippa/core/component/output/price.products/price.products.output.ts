import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController, ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }  from '../base.output';
import { ViewerModal } from '../../../modal/viewer/viewer.modal';

@Component({
    selector        : `.price-products-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<ul>
							<li *ngIf="output && output.value">
								<a *ngFor="let item of output.value; let first = first;" (click)="openModal(item.priceTable)">
									 {{item.code}} - {{item.name}}
								</a>
							</li>
					   </ul>`
})
export class PriceProductsOutput extends BaseOutput
{	
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public modalController   : ModalController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'price-products-output ';
	}
	
	async loadOutput()
	{
		// if(this.setting.viewerPath)
        // {
        //     const paths = this.setting.viewerPath.split('/');

        //     const params = this.createParams({
        //         getEndPoint : Types.GET_DOCUMENT_API,
        //         accid : paths[0],
        //         appid : paths[1],
        //         colid : paths[2],
        //         path  : this.setting.viewerPath,
        //         model : Viewer,
        //         mapItems    : {
        //             referencePath : environment.defaultMapItems
        //         },
        //         map : true,
        //     })

        //     this.core().api.getObject(params).then((result:any) =>
        //     {
        //         this.viewer = result.data;
        //         this.markForCheck();
        //     });
		// };

		this.markForCheck();
	}

	async openModal(data)
	{
		await this.core().loadingController.start();

		this.core().loadingController.stop();

		this.modalController.create(
		{
			component 	    : ViewerModal,
			backdropDismiss : false,
            cssClass        : 'full',
			componentProps : {
				document : data,
				onClose    : () =>
				{
					this.modalController.dismiss();
				}	
			}
		})
		.then(modal =>
		{
			modal.present();
		});
	}
	
    /* OVERRIDER */
    rendererComplete()
    {
    }
}
