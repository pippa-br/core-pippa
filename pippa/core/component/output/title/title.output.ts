import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.title-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p [innerHtml]="output?.value" hide-delay="0" hide-delay-mobile="0" placement="bottom"></p>`
})
export class TitleOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'title-output ';
    }
}
