import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.table-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<table class="striped" *ngIf="output">			
							<tr>
								<td *ngFor="let item of headers;">
									{{item.label}}
								</td>								
							</tr>
							<tr *ngFor="let item of data;">
								<td *ngFor="let item2 of item.items;">
									{{item2.value}}
								</td>			
							</tr>
					   </table>`
})
export class TableOutput extends BaseOutput
{
	public headers : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'table-output ';
	}

	loadOutput()
	{
		this.data    = this.output.value;
		this.headers = [];

		for(const key in this.data)
		{
			if(key == '0')
			{
				for(const item of this.data[key].items)
				{
					this.headers.push({
						label : item.label
					});	
				}
			}
		}

		this.markForCheck();
	}
}
