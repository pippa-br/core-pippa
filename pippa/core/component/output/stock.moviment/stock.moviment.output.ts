import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }  from '../base.output';
import { Types } 	   from '../../../type/types';
import { environment } from 'src/environments/environment';
import { Grid } 	   from '../../../model/grid/grid';

@Component({
    selector        : `.stock-moviment-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div *ngIf="output"
						    dynamic-list
							[grid]="gridSelected"
							[collection]="output?.value"
							(rendererComplete)="markForCheck();">
					   </div>`
})
export class StockMovimentOutput extends BaseOutput
{
	public gridSelected : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'stock-moviment-output ';
    }

    loadOutput()
    {
		//console.error('0-00000', this.output, this.viewerSelected);

        if(this.output.formItem.setting)
        {
			if(this.output.formItem.setting.gridPath)
			{
				const paths =this.output.formItem.setting.gridPath.split('/');

                const params = this.createParams({
                    getEndPoint : Types.GET_DOCUMENT_API,
                    accid : paths[0],
                    appid : paths[1],
                    colid : paths[2],
                    path  : this.output.formItem.setting.gridPath,
                    model : Grid,
                    mapItems    : {
                        referencePath : environment.defaultMapItems
                    }
                })

				this.core().api.getObject(params).then((result:any) =>
				{
					this.gridSelected = result.data;
					this.markForCheck();
				});
			}    
        }        
    }
}
