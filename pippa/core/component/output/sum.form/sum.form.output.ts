import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { DomController } from "@ionic/angular";

/* PIPPA */
import { BaseOutput } from "../base.output";

@Component({
    selector        : "[sum-form-output]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<meter min="0" max="100" high="0" disabled
                       value="{{output?.percentage}}"
                       low="{{output?.minPercentage}}"
                       optimum="{{output?.minPercentage + 1}}">
                </meter>
                <span [hidden]="!output">{{output?.total}}/{{output?.count}}&nbsp;&nbsp;<small>({{output?.percentage}}%)</small></span>`
})
export class SumFormOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.renderer.addClass(this.elementRef.nativeElement, "sum-form-output");

        this.nameClass += "sum-form-output ";
    }
}
