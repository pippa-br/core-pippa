import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { DomController, ModalController } from "@ionic/angular";

/* PIPPA */
import { BaseOutput }  from "../base.output";
import { EventPlugin } from "../../../plugin/event/event.plugin";
import { ViewerModal } from "../../../modal/viewer/viewer.modal";
import { Viewer } from "../../../model/viewer/viewer";

@Component({
    selector        : ".reference-list-output",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div *ngIf="output && !output.grid && items.length > 0" [ngClass]="{'has-photo' : hasPhoto}">
                            <span class="count" *ngIf="showCount && items.length">
                                <ion-badge color="medium">{{output.value.length}}</ion-badge>
                            </span>
                            <p class="list-items">
                                <span *ngFor="let item of items; let first = first;" (click)="onViewer(item)">
                                    <img *ngIf="hasPhoto && item.photo"  height="18" width="18" [src]="item.photo._url"/>
                                    <img *ngIf="hasPhoto && !item.photo" height="18" width="18" src="/assets/img/avatar.png"/>
                                    {{item.label}}
                                </span>
                            </p>
                        </div>
                        <div dynamic-list
                             *ngIf="output && output.grid && items.length > 0"
                             [grid]="output.grid"
                             [collection]="output.value"
                             (viewer)="onViewer($event)"
                             (rendererComplete)="onRendererComplete();markForCheck();">
                        </div>`
})
export class ReferenceListOutput extends BaseOutput
{
    public items : any = [];
    public hasPhoto    = false;
    public showCount   = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public eventPlugin       : EventPlugin,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
        public modalController   : ModalController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += "reference-list-output ";
    }

    loadOutput()
    {
        if (this.setting.hasPhoto)
        {
            this.hasPhoto = true;
        }

        if (this.setting.showCount)
        {
            this.showCount = true;
        }
		
        const promises = [];

        for (const key in this.output.value)
        {
            const data = this.output.value[key];
			
            if (this.setting.fieldLabel)
            {
                promises.push(data.getProperty(this.setting.fieldLabel));
            }
            else if (this.setting.labelFunction)
            {
                promises.push(this.core().functions[this.setting.labelFunction](data));
            }
            else
            {
                promises.push(new Promise((resolve) => 
                {
                    resolve(data.name);
                }));
            }			
        }

        this.items = [];

        Promise.all(promises).then((values) => 
        {
            for (const key in values)
            {
                this.items.push({
                    label : values[key],
                    data  : this.output.value[key]
                });
            }

            this.markForCheck();
        })
    }

    onViewer(event:any)
    {
        console.error("sss", event.data);

        this.modalController.create(
            {
                component : ViewerModal,
                componentProps :
            {
                //viewer   : new Viewer({ items : this.formItem.items }),
                document : event.data,				
                onClose  : (() =>
                {
                    this.modalController.dismiss();
                }),
            },
                cssClass : "full",
            })
        .then(popover =>
        {
            popover.present(); 
        });
    }
}
