import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.percentage-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p class="meter-wrapper" [ngClass]="{'has-grafic' : hasGrafic}">
							<meter *ngIf="hasGrafic" min="0" max="100" low="25" high="75" optimum="100" disabled value="{{output?.value}}"></meter>
							<span>{{output?.value}}%</span>
					   </p>`
})
export class PercentageOutput extends BaseOutput
{
	public hasGrafic : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'percentage-output ';
	}
	
	loadOutput()
	{
		if(this.setting.hasGrafic)
		{
			this.hasGrafic = this.setting.hasGrafic;
		}
	}
}
