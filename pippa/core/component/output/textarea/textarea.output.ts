import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController, ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } 	   from '../base.output';
import { Form }       	   from '../../../model/form/form';
import { FormFormPopover } from '../../../popover/form.form/form.form.popover';

@Component({
    selector 		: `.textarea-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<p *ngIf="output && !isInput" [innerHtml]="value"></p>
					   <div class="input-wrapper" *ngIf="isInput">
					   		<a (click)="onEdit()">
							   <ion-icon name="document-text-outline"></ion-icon>
							</a>
					   </div>`
})
export class TextareaOutput extends BaseOutput
{
	public value : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public modalController   : ModalController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'value-output ';
	}

	loadOutput()
    {
        if(this.output.value !== undefined)
        {
            this.value = this.output.value;
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}
	}

	onEdit()
	{
		/* FORM */
		const form = new Form({
			name       : this.formItem.label,
			items      : [this.formItem],
			viewEdit   : true,
		});

		this.modalController.create(
		{
			component      : FormFormPopover,
			componentProps :
			{
				data  : this.data,
				form  : form,
				onSet : ((event:any) =>
				{
					this.setValue(event.data[this.formItem.path]);
					this.modalController.dismiss();
				}),
				onClose : (() =>
				{
					this.modalController.dismiss();
				}),
			},
		})
		.then(popover =>
		{
			popover.present();
		});
	}
}
