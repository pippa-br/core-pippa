import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.charges-pagarme-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<table class="striped list">
                            <tr>
                                <th>Status</th>
                                <th>Método de Pagamento</th>
                                <th>Operadora</th>
                                <th>Antifraud</th>
                            </tr>
                            <tr *ngFor="let item of charges">
                                <td>
                                    {{item.status}}
                                </td>
                                <td>
                                    {{item.payment_method}}
                                </td>
                                <td>
                                    {{item.pagarme}}
                                </td>
                                <td>
                                    {{item.antifraud}}
                                </td>
                            </tr>
                       </table>`
})
export class ChargesPagarmeOutput extends BaseOutput
{
    public charges = [];

    public statusMap = {
		status   		: 'Recusado',
		new      		: 'Novo',
		paid     		: 'Pago',
		refunded 		: 'Estornado',
		canceled 		: 'Cancelado',
		waiting_payment : 'Aguardando',
		pending		    : 'Aguardando',
		failed		    : 'Recusado',
	}

	public paymentMethodMap = {
		boleto   	: 'Boleto',
		pix   		: 'PIX',
		credit_card : 'Cartão de Credito',
	}

	public antifraudMap = {
		reproved : 'Reprovado',
        approved : 'Aprovado',
	}
    
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'charges-pagarme-output ';
    }

    loadOutput()
    {
        this.charges = [];

        if(this.output.value?.charges)
        {
            for(const item of this.output.value.charges)
            {
                this.charges.push({
                    status 		   : this.statusMap[item.status] || item.status,
                    payment_method : this.paymentMethodMap[item.payment_method] || item.payment_method,
                    pagarme  	   : item.last_transaction?.acquirer_message || '-',
                    antifraud 	   : this.antifraudMap[item.last_transaction?.antifraud_response?.status] || item.last_transaction?.antifraud_response?.status || '-',
                })
            }
        }
    }
}
