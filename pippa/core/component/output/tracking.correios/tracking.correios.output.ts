import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { Types } from '../../../type/types';
import { environment } from 'src/environments/environment';
import { platform } from 'os';

@Component({
    selector 		: `.tracking-correios-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<table class="striped list">
                            <tr *ngIf="loading">
                                <th>Carregando...</th>
                            </tr>
                            <tr *ngIf="events.length">
                                <th>Data</th>
                                <th>Situação</th>
                                <th>Local</th>
                            </tr>
                            <tr *ngFor="let item of events">
                                <td>
                                    {{item.date|dateFormat:'dd/MM/yyyy HH:mm:ss'}}
                                </td>
                                <td>
                                    {{item.description}}
                                </td>
                                <td>
                                    {{item.locale}}
                                </td>
                            </tr>
                       </table>`
})
export class TrackingCorreiosOutput extends BaseOutput
{
    public events = [];    
    public loading : boolean = false;
    
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'tracking-correios-output ';
    }

    loadOutput()
    {
        this.events = this.output.value;  
        this.markForCheck();

        // if(this.output.format)
        // {
        //     this.loading = true;

        //     const data : any = {
        //         accid       : 'default',
        //         appid       : 'apps',
        //         colid       : 'documents',
        //         code        : this.output.format,
        //         platform    : this.core().platform,
        //         mapItems    : {
        //             referencePath : environment.defaultMapItems
        //         }
        //     }

        //     if(this.setting && this.setting.shipping)
        //     {
        //         data.shipping = this.setting.shipping;
        //     }
    
        //     this.core().api.callable(Types.TRACK_CORREIOS_API, data).then((result:any) =>
        //     {
        //         if(result.data && result.data.events)
        //         {
        //             this.events = result.data.events
        //         }            

        //         this.loading = false;
    
        //         this.markForCheck();
        //     });
        // }		       
    }
}
