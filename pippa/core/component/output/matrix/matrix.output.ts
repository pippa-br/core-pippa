import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, ViewChildren, ViewContainerRef, QueryList } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { Matrix } 	  from '../../../model/matrix/matrix';
import { Document }   from '../../../model/document/document';
import { FieldType }   from '../../../type/field/field.type';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { TransformFactory } from '../../../factory/transform/transform.factory';

import { OutputFactory } from '../../../factory/output/output.factory';

@Component({
    selector        : `.matrix-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-grid *ngIf="!d2" class="grid-matrix" 
								[class.inline]="inline" 
								[class.onlyOne]="onlyOne"
								[class.noVariant]="noVariant">
							<ion-row *ngFor="let item of items">
								<ion-col class="variant" size="2" *ngFor="let item2 of item.field.list">
									<label *ngIf="item2.label">{{item2.label}} <small>({{item2.value}})</small></label>
								</ion-col>
								<ion-col>
									<ng-template #templates></ng-template>
								</ion-col>
							</ion-row>
						</ion-grid>
						<ion-grid *ngIf="d2" class="grid-matrix">
							<ion-row *ngIf="itemsD2.length">
								<ion-col class="variant" size="2">
									<label></label>											
								</ion-col>
								<ion-col class="variant" *ngFor="let item2 of itemsD2[0].cols">
									<label *ngIf="item2.label">{{item2.label}}</label>											
								</ion-col>										
							</ion-row>
							<ion-row *ngFor="let item of itemsD2">
								<ion-col class="variant" size="2">
									<label *ngIf="item.row.label">{{item.row.label}}</label>											
								</ion-col>										
								<ion-col *ngFor="let item2 of item.cols">
									<ng-template #templates></ng-template>
								</ion-col>
							</ion-row>
							<ion-row>
								<ion-col size="12">
										<mat-paginator 
													[length]="total"
													[pageSize]="perPage"
													itemsPerPageLabel=""
													nextPageLabel="Próxima Página"
													previousPageLabel="Página Anterior"
													firstPageLabel="Primeira Página"
													lastPageLabel="Última Página"
													getRangeLabel="getRangeLabel"
																						(page)="onPage($event)"></mat-paginator>
								</ion-col>
							</ion-row>
						</ion-grid>`
})
export class MatrixOutput extends BaseOutput
{
	@ViewChildren('templates', { read: ViewContainerRef }) templates : QueryList<ViewContainerRef>;

	public matrix 	  : any;
	public items  	  : any;
	public fullItems  : any;
	public total 	  : number = 0;
	public perPage 	  : number = 100;
	public components : any;
	public d2		  : boolean = false;
	public inline	  : boolean = false;
	public onlyOne	  : boolean = false;
	public noVariant  : boolean = false;
	public itemsD2    : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'matrix-output ';
	}

	loadOutput()
	{
		this.d2	       = this.setting.d2 		!== undefined ? this.setting.d2 	   : false;
		this.inline	   = this.setting.inline    !== undefined ? this.setting.inline    : false;
		this.onlyOne   = this.setting.onlyOne   !== undefined ? this.setting.onlyOne   : false;
		this.noVariant = this.setting.noVariant !== undefined ? this.setting.noVariant : false;
		this.perPage   = this.setting.perPage   !== undefined ? this.setting.perPage   : 100;
		this.matrix    = new Matrix(this.output.value);
		
		// BUILD ITEMS
		let items = new FormItemCollection();
		//let i = 0;

		for(const item of this.output.formItem.items)
		{
			//if(i == 0)
			//{
				//item.hasLabel = false;
			//}
			
			//i++
		}

		for(const list of this.matrix.items)
		{
			const path = this.matrix.getPath(list);

			const formItem : any = {
				field : {
					name 	 : '_' + this.core().util.randomString(20),
					list 	 : list,
					type	 : FieldType.type('DataForm'),
					hasLabel : false,
				},
				items : this.output.formItem.items.copy()
			};

			// ITEMS
			items.add(formItem);
		}

		this.fullItems = items;
		this.total 	   = items.length;

		this.makePage(0);
	}

	createComponent()
	{		
		setTimeout(async () => 
		{
			this.clear();

			this.components = [];
			const templates = this.templates.toArray();

			for(const key in templates)
			{
				if(this.items[key])
				{
					const data 						 = new Document();
					data[this.items[key].field.name] = this.matrix.getValue(this.items[key].field.list);
				
					// CASO DE IMAGENS
					if(this.onlyOne)
					{
						for(const formItem of this.items[key].items)
						{
							const setting = formItem._setting || {};
							setting.onlyFirst = true;

							formItem._setting = setting;
						}												
					}				
					
					const component = await OutputFactory.createComponent(this.items[key], templates[key]);
					
					TransformFactory.transform(this.items[key], data).then((output:any) =>
					{
						component.instance.output = output;
					});

					this.components.push(component);	
				}				
			}

			this.markForCheck();
		});
	}

	makePage(page:any)
	{
		const items = []

		for(let i = this.perPage * (page); i < this.perPage * (page + 1); i++)
		{
			if(this.fullItems.length > i)
			{
				items.push(this.fullItems[i]);
			}			
		}

		this.items = items;

		if(this.d2)
		{
			this.transformD2();
		}

		this.createComponent();		
	}

	transformD2()
	{
		const d2    = [];
		let current : any = { id : ''};

		for(const item of this.items)
		{
			if(current.id !== item.field.list[0].id)
			{
				current = {
					id    : item.field.list[0].id,
					row   : item.field.list[0],
					cols  : [],
				}

				d2.push(current);
			}

			if(item.field.list[1])
			{
				current.cols.push(item.field.list[1]);
			}			
		}

		this.itemsD2 = d2;
	}

		onPage(event:any)
    {        
        console.log('onPage', event);
        this.makePage(event.pageIndex);
    }

	clear()
    {
        for(const key in this.components)
        {
            this.components[key].destroy();
        }

		const templates = this.templates.toArray();
	
		for(const template of templates)
		{
			template.clear();
		}
    }
}
