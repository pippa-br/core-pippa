import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.qrcode-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p><img *ngIf="output && output.format" [src]="output.format" width="150"/></p>`
})
export class QRCodeOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'qrcode-output ';
    }
}
