import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `[has-document-output]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<meter *ngIf="output?.value" min="0" max="100" high="0" value="100" low="0" optimum="100"></meter>`
})
export class HasDocumentOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.renderer.addClass(this.elementRef.nativeElement, 'has-document-output');

        this.nameClass += 'has-document-output ';
    }
}
