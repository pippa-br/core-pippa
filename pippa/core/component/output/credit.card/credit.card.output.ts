import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.credit-card-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p>{{output?.format}}</p>`
})
export class CreditCardOutput extends BaseOutput 
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'credit-card-output ';
    }
}