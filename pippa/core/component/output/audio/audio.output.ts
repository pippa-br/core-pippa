import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }   from '../base.output';
import { GalleryModal } from '../../../modal/gallery/gallery.modal';

@Component({
    selector        : `.audio-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="file">
							<audio controls [src]="file._url">
									Your browser does not support the <code>audio</code> element.
							</audio>
                       </p>`
})
export class AudioOutput extends BaseOutput
{
	public file 		: any;
	public displayTitle : boolean = true;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'audio-output ';
    }

    loadOutput()
    {
		if(this.output.value.length > 0)
		{
			this.file = this.output.value[0];
		}			
    }    
}
