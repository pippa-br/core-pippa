import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.cron-job-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<p *ngIf="output">			
							<label>
								<span>{{frequency?.label}}</span>
							</label>
							<label *ngFor="let option of M;">
								<span>{{option.label}}</span>
							</label>
							<label *ngFor="let option of dw;">
								<span>{{option.label}}</span>
							</label>
							<label *ngFor="let option of D;">
								<span>{{option.label}}</span>
							</label>
							<label *ngFor="let option of h;">
								<span>{{option.label}}</span>
							</label>
							<label *ngFor="let option of m;">
								<span>{{option.label}}</span>
							</label>
					   </p>`
})
export class CronJobOutput extends BaseOutput
{
	public frequency : any;
	public M 		 : any = [];
	public dw 		 : any = [];
	public D 		 : any = [];
	public h 		 : any = [];
	public m 		 : any = [];

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'cron-job-output ';
	}
	
	loadOutput()
	{
		this.frequency = this.output.value.frequency;

		if(this.output.value.M)
		{
			this.M = this.output.value.M;
		}

		if(this.output.value.dw)
		{
			this.dw = this.output.value.dw;
		}

		if(this.output.value.D)
		{
			this.D = this.output.value.D;
		}

		if(this.output.value.h)
		{
			this.h = this.output.value.h;
		}

		if(this.output.value.m)
		{
			this.m = this.output.value.m;
		}

		this.markForCheck();
	}
}
