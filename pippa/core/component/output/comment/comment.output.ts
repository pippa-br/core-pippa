import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } 	  from '../base.output';
import { MenuCollection } from '../../../model/menu/menu.collection';
import { MenuPlugin } 	  from '../../../plugin/menu/menu.plugin';

@Component({
    selector 		: `.comment-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<form *ngIf="hasAdd">
							<textarea
									  autosize
									  autocomplete="false"
									  [(ngModel)]="value"
									  [ngModelOptions]="{standalone: true}"
									  [placeholder]="'Escreva seu comentário'">
							</textarea>
							<ion-button color="medium" (click)="onAdd(value)">Comentar</ion-button>
						</form>
						<ion-list class="comments" *ngFor="let item of collection">
							<ion-item>
								<ion-avatar slot="start">
									<img *ngIf="item.owner" src="{{item.owner | property : 'photo._url' : './assets/img/avatar.png' | async}}">
								</ion-avatar>
								<ion-label>
									<h2>{{item.owner | property : 'name' | async }}</h2>
									<h3>{{item.postdate | dateFormat : 'dd/MM/yyyy HH:ss'}}</h3>
									<div class="message" [innerHtml]="item.message"></div>									
								</ion-label>
								<ion-button class="del-button" *ngIf="item.isOwner()" (click)="onDel(item)">
									<ion-icon name="trash-outline"></ion-icon>
								</ion-button>								
							</ion-item>
							<ion-list class="child-comments" *ngFor="let child of item._children">
								<ion-item>
									<ion-avatar slot="start">
										<img *ngIf="child.owner" src="{{child.owner | property : 'photo._url' : './assets/img/avatar.png' | async}}">
									</ion-avatar>
									<ion-label>
										<h2>{{child.owner | property : 'name' | async }}</h2>
									  <h3>{{child.postdate | dateFormat : 'dd/MM/yyyy HH:ss'}}</h3>
										<div class="message" [innerHtml]="child.message"></div>									
									</ion-label>
									<ion-button class="del-button" *ngIf="child.isOwner()" (click)="onDelChild(item, child)">
										<ion-icon name="trash-outline"></ion-icon>
									</ion-button>								
								</ion-item>
							</ion-list>
							<a class="comment-button" (click)="onOpen(item)">
								Comentar 
								<ion-icon *ngIf="!item.open" name="caret-up-outline"></ion-icon>
								<ion-icon *ngIf="item.open" name="caret-down-outline"></ion-icon>
							</a>
							<form *ngIf="item.open">
								<textarea
									autosize
									autocomplete="false"
									[(ngModel)]="item.value"
									[ngModelOptions]="{standalone: true}"
									[placeholder]="'Escreva seu comentário'"></textarea>
								<ion-button color="medium" (click)="onAddChild(item)">Comentar</ion-button>
							</form>
					   </ion-list>`
})
export class CommentOutput extends BaseOutput
{
	public appid      : any;
	public form 	  : any;
	public collection : any;
	public model      : any;
	public code       : string;
	public value      : string;
	public hasAdd     : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public menuPlugin        : MenuPlugin,
		public alertController   : AlertController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

		this.nameClass += 'comment-output ';		
	}

	async loadOutput()
    {
		await this.output.collection.on(true);

		this.form 		= this.output.form;		
		this.appid 		= this.output.appid;
		this.code       = this.output.code;
		this.collection = this.output.collection;		
		this.hasAdd     = this.output.hasAdd;	
		
		if(this.noEmpty)
		{
			this.display = false; 
		}

		this.markForCheck();
	}

	onOpen(item)
	{
		item.open = !item.open;
	}

	async onAdd(value:any)
	{
		if(value)
		{
			const data = await this.form.parseToData({
				message : value,
				code    : this.code,
				_level  : 1,
			});

			await this.menuPlugin.add(this.appid, this.form, { data : data});
			
			await this.collection.reload();

			this.value = '';				
			this.markForCheck();			
		}		
	}

	async onAddChild(item:any)
	{		
		if(item.value)
		{
			const value = item.value;

			item.value = '';
			item.open = false;
	
			if(!item._children)
			{
				item._children = new MenuCollection();
			}
	
			const data = await this.form.parseToData({
				message : value,
				code    : this.code,
				_level  : 2,
			});
	
			const data2 = await this.menuPlugin.add(this.appid, this.form, { data : data})
	
			item._children.add(data2);
	
			await item.set({
				_children : item._children.getData()
			});	
			
			await this.collection.reload();
	
			this.markForCheck();
		}		
	}

	async onDelChild(item:any, child:any)
	{
		const alert = await this.alertController.create({
			header  : 'Atenção!',
			message : 'Deseja remover esse comentário?',
			buttons : [
				{
					text: 'Cancelar',
					role: 'cancel',
					cssClass: 'secondary',
				}, 
				{
					text: 'Sim',
					handler : async () => 
					{						
						item._children.del(child);

						await item.set({
							_children : item._children.getData()
						});

						await this.menuPlugin.del(child);						
						
						await this.collection.reload();

						this.markForCheck();
					}
				}
			]
		});		 

		await alert.present();
	}

	async onDel(item:any)
	{
		const alert = await this.alertController.create({
			header  : 'Atenção!',
			message : 'Deseja remover esse comentário?',
			buttons : [
				{
					text: 'Cancelar',
					role: 'cancel',
					cssClass: 'secondary',
				}, 
				{
					text	: 'Sim',
					handler : async () => 
					{
						await this.menuPlugin.del(item);

						this.collection.del(item);

						await this.collection.reload();

						this.markForCheck();
					}
				}
			]
		});
	
		await alert.present();		
	}
}
