import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { DomController, ModalController } from "@ionic/angular";

/* PIPPA */
import { BaseOutput } 		from "../base.output";
import { GridPopover } 		from "../../../popover/grid/grid.popover";
import { SubFormTransform } from "../../../component/transform/sub.form/sub.form.transform";
import { WhereCollection } from "../../../model/where/where.collection";
import { FieldType } from "../../../type/field/field.type";
import { DocumentPlugin } from "../../../plugin/document/document.plugin";
import { Core } from "../../../util/core/core";
import { environment } from "src/environments/environment";
import { ViewerModal } from "../../../modal/viewer/viewer.modal";
import { Viewer } from "../../../model/viewer/viewer";
import { Matrix } from "../../../model/matrix/matrix";

@Component({
    selector        : ".sub-form-output",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : ` <div class="check-search" *ngIf="validate && !this.checkOrder">
							<ion-searchbar animated 
								placeholder="Conferir" 
								showCancelButton="never"
								[cdkTrapFocusAutoCapture]="focus" 
								[cdkTrapFocus]="focus"
								[(ngModel)]="term"
								[debounce]="1000"
								(ionInput)="onSearch($event)">
							</ion-searchbar>
						</div>
						<div dynamic-list
							*ngIf="!isModal"
                            [grid]="gridOutput"
                            [collection]="collection"
                            (viewer)="onViewer($event)"
                            (rendererComplete)="onRendererComplete();markForCheck();">
					</div>
					<p class="p-modal" *ngIf="isModal">
							<a (click)="openModal()"><ion-icon name="list-circle-outline"></ion-icon></a>
					</p>` 
})
export class SubFormOutput extends BaseOutput
{
    public gridOutput  : any;
    public collection  : any;
    public isModal      = false;
    public term        : string;
    public _searchTime : any;
    public plugin	   : any;
    public validate     = false;
    public checkOrder   = false;
    public focus  	    = true;
    public fieldCodeTable  = "codeTable";

    constructor(
			public elementRef        : ElementRef,
			public renderer          : Renderer2,
			public changeDetectorRef : ChangeDetectorRef,
			public domController     : DomController,
			public modalController   : ModalController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += "sub-form-output ";
    }

    async loadOutput()
    {
        this.checkOrder = this.output.data.checkOrder;
        this.gridOutput = this.output.grid;
        this.collection = this.output.value;

        if (this.setting.isModal != undefined)
        {
            this.isModal = this.setting.isModal;
        }

        if (this.setting.validate != undefined)
        {
            this.validate = this.setting.validate;
        }

        if (this.setting.fieldCodeTable != undefined)
        {
            this.fieldCodeTable = this.setting.fieldCodeTable;
        }

        if (this.validate)
        {
            for (const item of this.collection)
            {
                item._archive = this.checkOrder;
            }

            this.plugin = await Core.get().injector.get(DocumentPlugin);
        }		

        this.markForCheck();
    }

    async openModal()
    {
        await this.core().loadingController.start();

        const output = await SubFormTransform.getOutput(this.formItem, this.data);

        this.core().loadingController.stop();

        this.modalController.create(
            {
                component  	    : GridPopover,
                backdropDismiss : false,
                cssClass        : "full",
                componentProps  : {
                    title      : this.formItem.label,
                    grid       : output.grid,
                    collection : output.value,
                    onClose    : () =>
                    {
                        this.modalController.dismiss();
                    }	
                }
            }).then(modal =>
        {
            modal.present();
        });
    }

    onViewer(event:any)
    {
        this.modalController.create(
            {
                component      : ViewerModal,
                componentProps : {
                    viewer   : new Viewer({ items : this.formItem.items }),
                    document : event.data,				
                    onClose  : (() =>
                    {
                        this.modalController.dismiss();
                    }),
                },
                cssClass : "full",
            }).then(popover =>
        {
            popover.present();
        });
    }

    async onSearch(event:any)
    {
        if (this.term)
        {
            const where = new WhereCollection();	

            where.add({
                field    : "search",
                value    : event.target.value,
                operator : "array-contains",
                type     : FieldType.type("Text"),
            });

            const result = await this.plugin.getData(Core.get().createParams({
                appid    : "product",
                perPage  : 1,
                where    : where,
                orderBy  : "postdate",
                asc      : false,
                loading  : false,
                mapItems : {
                    referencePath : environment.defaultMapItems
                },
                map : true,
            }));

            console.log("search", where, result.collection);

            let validate : any  = false;
            let hasSearch : any = false;

            if (result.collection && result.collection.length > 0)
            {
                const matrix = new Matrix(result.collection[0][this.fieldCodeTable]);

                for (const item of this.collection)
                {
                    const value = matrix.getValue(item.variant);

                    if ((value.code + "") == event.target.value)
                    {
                        validate  = await this.valdiateQuantity(item);
                        hasSearch = true;
                        break;
                    }
                }

                if (!hasSearch)
                {
                    this.showAlert("Produto", "Produto não encontrado!");
                }
                else if (validate)
                {
                    // VERIFICA SE TODOS FORAM CHECADOS
                    let allChecks = true;

                    for (const item of this.collection)
                    {
                        allChecks = allChecks && (item._archive || false);
                    }

                    if (allChecks)
                    {
                        await this.plugin.set(this.output.data.appid, this.output.data, this.output.data.form, 
                            {
                                data : 
{
    checkOrder : true
}
                            }, true);	

                        this.checkOrder = true;

                        if (this.viewer)
                        {
                            this.viewer.component.instance.onReload();
                        }
                    }
                }
                else
                {
                    this.showAlert("Produto", "Quantidade invalida!"); 
                }
            }			
            else
            {
                this.showAlert("Produto", "Produto não encontrado!");
            }

            this.term = "";
            this.markForCheck();
        }	
    }

    valdiateQuantity(item)
    {
        return new Promise((resolve) => 
        {
            if (!item.checkQuantity)
            {
                item.checkQuantity = 0;
            }	

            item.checkQuantity++;

            if (item.quantity == item.checkQuantity)
            {
                item._archive = true;
                this.collection.dispatchUpdate();
                resolve(true);
            }
            else
            {
                resolve(false);
            }

            // this.showAlert('Quantidade', 'Digite a quantidade do produto', 
            // [{
            // 	text: 'Ok',
            // 	handler: (data:any) => 
            // 	{
            // 		if(data.quantity == item.quantity)
            // 		{						
            // 			item._archive = true;
            // 			this.collection.dispatchUpdate();
            // 			resolve(true);
            // 		}
            // 		else
            // 		{
            // 			resolve(false);
            // 		}
            // 	}
            // }], [{
            // 	name: 'quantity',
            // 	type: 'number'
            // }]);
        });		
    }

    /* OVERRIDER */
    rendererComplete()
    {
        // OVERRIDER
    }
}
