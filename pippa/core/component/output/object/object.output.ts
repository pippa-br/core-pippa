import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.object-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<p *ngIf="output">			
							<label *ngFor="let item of items;">
								<span>{{item.label}}: {{item.value}}</span>
							</label>
					   </p>`
})
export class ObjectOutput extends BaseOutput
{
	public items : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'object-output ';
	}

	loadOutput()
	{
		this.data  = this.output.value;
		this.items = [];

		for(const key in this.data)
		{
			this.items.push({
				label : key,
				value : this.data[key]
			})
		}

		this.markForCheck();
	}
}
