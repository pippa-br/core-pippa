import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { Share } from '@capacitor/share';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.video-url-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<ion-fab vertical="top" horizontal="end" color="light" slot="fixed" *ngIf="hasShare">
							<ion-fab-button color="light" size="small" (click)="onShare()">
								<ion-icon name="share-social-outline"></ion-icon>
							</ion-fab-button>
						</ion-fab>
						<div class="video-wrapper" *ngIf="output && display" (clickOutside)="onClickedOutside()">
							<div plyr [plyrPlaysInline]="true" [plyrPoster]="thumb?._url" [plyrOptions]="videoOptions" [plyrSources]="videoSources" (plyrInit)="player = $event" (plyrPlay)="played($event)"></div>
					   </div>`
})
export class VideoUrlOutput extends BaseOutput
{
	public embed 		: string;
	public hasShare 	: boolean = false;
	public videoSources : any;
	public thumb 		: any;
	public player 		: any;
	public videoOptions : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'video-url-output ';
	}
	
	played(event: Plyr.PlyrEvent) 
	{
	}
	  
	play(): void 
	{
		//this.player.play(); // or this.plyr.player.play()
	}

	onClickedOutside()
	{
		this.player.pause();
	}

    loadOutput()
    {
		this.videoSources = [{
			  src	   : this.output.value,
			  provider : this.output.type,
		}];

		this.thumb = this.output.data.image || this.output.data.thumb;
		this.videoOptions = {
			quality : { default: 576, options: [4320, 2880, 2160, 1440, 1080, 720, 576, 480, 360, 240] },
		}

        if(this.output.value)
        {
            //this.embed = this.output.value.getEmbed();
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}

		if(this.setting.hasShare)
		{
			this.hasShare = this.setting.hasShare;
		}

		this.markForCheck();
	}

	onClick(e?:any)
	{

	}
	
	async onShare()
	{
		const shareRet = await Share.share({
			title : this.output.value.title,
			text  : this.output.data.name,
			url   : this.output.value.url,
			dialogTitle: 'Compartilhar'
		});
	}
}
