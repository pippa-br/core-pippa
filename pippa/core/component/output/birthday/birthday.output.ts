import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `.birthday-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<p *ngIf="output && output.value">
                    {{output.value | dateFormat}}&nbsp;&nbsp;<small>({{output.value | age}}) anos</small>
                </p>`
})
export class BirthdayOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'birthday-output ';
    }
}
