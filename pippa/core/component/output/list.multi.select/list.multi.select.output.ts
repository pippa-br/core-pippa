import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }  from '../base.output';
import { EventPlugin } from '../../../plugin/event/event.plugin';

@Component({
    selector 		: `.list-multi-select-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<div *ngIf="output">							
							<div class="list-items" *ngFor="let option of output.value;">
								<p *ngFor="let item of option.items;">
									<label>{{item.label}}</label>
								</p>
							</div>
					   </div>`
})
export class ListMultiSelectOutput extends BaseOutput
{
	public router : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public eventPlugin       : EventPlugin,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'list-multi-select-output ';
	}

	/* REMOVI CLICK OUTPUT */
    onClick(e?:any)
    {		
	};
}
