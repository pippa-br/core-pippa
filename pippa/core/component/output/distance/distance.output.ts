import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.distance-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<p *ngIf="info">
					   		{{info}}
					   </p>`
})
export class DistanceOutput extends BaseOutput
{
    public info : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'info-output ';
    }

    loadOutput()
    {
		if(this.output.value)
		{
			this.info = this.output.value.label;
		}        
    }
}
