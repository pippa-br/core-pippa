import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { DomController } from "@ionic/angular";

/* PIPPA */
import { BaseOutput } from "../base.output";

@Component({
    selector      		: ".currency-output",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: "<p [ngClass]=\"{'negative' : price < 0, 'positive' : price > 0}\" [innerHtml]=\"output?.format\"></p>"
})
export class CurrencyOutput extends BaseOutput
{
    public price : number;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += "currency-output ";
    }

    loadOutput()
    {
        this.price = this.output.value;
    }
}
