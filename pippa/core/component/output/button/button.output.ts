import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, HostListener } from "@angular/core";
import { DomController } from "@ionic/angular";
import { ModalController } from "@ionic/angular";
import { AlertController } from "@ionic/angular";
import { Browser } from "@capacitor/browser";

/* PIPPA */
import { BaseOutput }      from "../base.output";
import { FunctionsType }   from "../../../type/functions/functions.type";
import { CheckoutPlugin }  from "../../../plugin/checkout/checkout.plugin";
import { FormFormPopover } from "../../../popover/form.form/form.form.popover";
import { PDFModal }   	   from "../../../modal/pdf/pdf.modal";
import { File }			   from "../../../model/file/file";

@Component({
    selector        : ".button-output",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<a *ngIf="enabled" (click)="_onClick()" [ngClass]="{'buy' : this.type == 'buy', 'route' : this.type == 'route'}">
                            <ion-icon *ngIf="this.icon" name="{{this.icon.value}}"></ion-icon>
                            {{label}}
</a>
<span class="off" *ngIf="!enabled && off">
<ion-icon *ngIf="this.icon" name="{{this.icon.value}}"></ion-icon> {{off}}
</span>`
})
export class ButtonOutput extends BaseOutput 
{
    public label   : string;
    public icon    : any;
    public type    : string;
    public off     : string;
    public enabled  = false;
    public openModal  = false;

    constructor(
public elementRef        : ElementRef,
public renderer          : Renderer2,
public changeDetectorRef : ChangeDetectorRef,
public domController     : DomController,
public alertController   : AlertController,
public checkoutPlugin    : CheckoutPlugin,
public modalController   : ModalController
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += "button-output ";
    }

    loadOutput()
    {
        this.label = this.output.formItem.label;

        if (this.setting.openModal)
        {
            this.openModal = this.setting.openModal;
        }

        if (this.output.formItem.option)
        {
            this.icon = this.output.formItem.option;
        }

        if (this.output.formItem.setting)
        {
            this.type  = this.output.formItem.setting.type;
            this.label = this.output.formItem.setting.label || this.label;
            this.off   = this.output.formItem.setting.off;
        }



        if (typeof this.output.value == "boolean")
        {
            this.enabled = this.output.value;	
        }
        else
        {
            this.enabled = true;
        }
    }

    async _onClick()
    {
        if (this.output.formItem.router)
        {
            const params        = this.output.formItem.params || {};
            params.documentPath = this.output.data.reference.path;

            if (params.dataField)
            {
                params.data                   = {};
                params.data[params.dataField] = this.output.data.reference.path

                delete params.dataField;
            }			

            this.push(this.output.formItem.router.value, params);
        }
        else if (this.type == "openUrl")
        {
            const file = new File(this.output.value)

            if (this.openModal && file.isPDF())
            {
                this.openPDF(this.output.data.name, file);
            }
            else
            {
                await Browser.open({ url : this.output.value._url });
            }
        }
        else if (this.type == "menu")
        {
            this.goMenu(this.core().menu[this.output.value]);
        }
        else if (this.type == "buy")
        {
            if (this.core().user)
            {
                const formModal = await this.checkoutPlugin.oneCheckout(
                    this.output.data, 
                    this.output.formItem.setting, 
                );

                if (formModal)
                {
                    const modalController = this.core().injector.get(ModalController);

                    //modalController.dismiss().then(async () => ESTAVA DANDO ERROR
                    //{
                    const modal = await modalController.create(
                        {
                            component    : FormFormPopover,
                            swipeToClose : true,
                            cssClass     : "full",
                            componentProps :
{
    form  : formModal,	
    onAdd : async (event) =>
    {							
        await formModal.complete(event);														
    },
    onClose : () => 
    {
        modal.dismiss();
    }
},
                        });

                    modal.present();

                    /* FACEBBOK */
                    /*if(this.output.formItem.setting._track)
{
this.core().track(this.output.formItem.setting._track);
}*/
                    //});				
                }


            }
            else
            {
                this.core().redirect = "Cursos";
                this.push("login", { "appid" : "users" })
            }			
        }
        else if (this.type == "function")
        {
            const fc = FunctionsType.get().getFunction(this.output.formItem.path + ":click");

            if (fc)
            {
                fc({
                    data : this.output.data
                });
            }
            else
            {
                console.log("this.core().functions not found", this.output.formItem.path);
            }
        }  
        else if (this.output.formItem && this.output.formItem.onClick)
        {
            this.output.formItem.onClick({
                data : this.output.data
            });
        }

    }

    async openPDF(title, pdf:any)
    {
        const modal = await this.modalController.create({
            component   	  : PDFModal,
            componentProps : {
                title   : title,
                url     : pdf._url,
                onClose : () =>
                {
                    modal.dismiss();
                }
            }
        });

        modal.present();
    }

@HostListener("click", [ "$event" ])
    onClick(e?:any)
    {        
    }
}
