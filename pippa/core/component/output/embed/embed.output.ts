import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `.embed-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="embed-responsive embed-responsive-16by9" [innerHtml]="output?.value | safe"></div>`
})
export class EmbedOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'embed-output ';
    }
}
