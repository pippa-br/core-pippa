import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }   from '../base.output';
import { GalleryModal } from '../../../modal/gallery/gallery.modal';
import { EmbedModal }   from '../../../modal/embed/embed.modal';
import { VideoPlugin }  from '../../../plugin/video/video.plugin';

@Component({
    selector        : `.image-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="files && files.length > 0" [ngClass]="{'multiple' : output.value.length > 1}">                            
                            <a *ngFor="let file of files; let i = index;" (click)="openGallery(i)">
								<img *ngIf="mode == 'image'" [src]="getUrl(file)" loading="lazy"/>
								<ion-icon *ngIf="mode == 'icon'" name="attach-outline"></ion-icon>
                            </a>                            
                       </p>
                       <p *ngIf="files && files.length == 0" class="no-image">
                            <ion-icon name="image-outline"></ion-icon>
                       </p>`
})
export class ImageOutput extends BaseOutput
{
    public hasDownload : boolean = false;
	public hasOpen     : boolean = false;
    public onlyFirst   : boolean = false;
	public files       : any;
	public mode        : string  = 'image';

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'image-output ';
    }

    getUrl(file:any)
    {
        let url = file._480x480 || file._1024x1024 || file._url || '';
        url = url.replace('.webp', '.jpeg');

        return url;
    }

    loadOutput()
    {
        if(this.setting && this.setting.hasDownload)
        {
            this.hasDownload = true;
        }

        if(this.setting && this.setting.hasOpen)
        {
            this.hasOpen = true;
		}

		if(this.setting && this.setting.hasOpen)
        {
            this.hasOpen = true;
		}

        if(this.setting && this.setting.onlyFirst)
        {
            this.onlyFirst = true;
		}
		
		if(this.noEmpty && this.files && this.files.length == 0)
		{
			this.display = false;
		}        

        if(this.onlyFirst && this.output.value && this.output.value.length > 0)
        {
            this.files = [this.output.value[0]];
        }
        else
        {
            this.files = this.output.value;
        }

		if(this.setting.pathVideo)
		{
			this.output.formItem.onClick = async (event:any) =>		
			{
				const modalController = this.core().injector.get(ModalController);
				const videoPlugin 	  = this.core().injector.get(VideoPlugin);
				let url 			  = await event.data.getProperty(this.setting.pathVideo);

				console.log(url, this.setting.pathVideo, event.data);

				let name = event.data.name;

				if(this.setting.pathName)
				{
					name = await event.data.getProperty(this.setting.pathName);
				}				
				
				videoPlugin.getEmbed(url).then(embed => 
				{
					console.log('embed', embed);

					modalController.create(
					{
						component      : EmbedModal,
						componentProps :
						{
							title : name,
							embed : embed.getEmbed(),
							onClose : (() =>
							{
								modalController.dismiss();
							}),
						},
						cssClass : 'full video-modal',
					})
					.then((modal:any) =>
					{
						modal.present();
					});
				});				
			}			
		}
    }

    openGallery(i:number)
    {
        if(this.hasOpen)
        {
            const modalController = this.core().injector.get(ModalController);

            modalController.create(
            {
                component      : GalleryModal,
                componentProps : 
                {
					slideID      : this.output.formItem.id, 
                    title        : this.output.formItem.label,
                    images       : this.output.value,
                    hasDownload  : this.hasDownload,
                    initialSlide : i,
                    onClose : () => 
                    {
                        modalController.dismiss();
                    }
                },
                cssClass : 'full'
            })
            .then(modal => 
            {
                modal.present()
            })

            //await Browser.open({ url : file._url });
        }
    }
}
