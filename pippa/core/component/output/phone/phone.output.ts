import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.phone-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template		: `<p *ngIf="output" [innerHtml]="text"></p>`
})
export class PhoneOutput extends BaseOutput
{
	public text : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'phone-output ';
	}

	loadOutput()
    {
        if(this.output.value !== undefined)
        {
            this.text = this.output.value;
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}
	}
}
