import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Browser } from '@capacitor/browser';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { FunctionsType } from '../../../type/functions/functions.type';

@Component({
    selector        : `.url-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="output && output.value">
                            <a (click)="_openUrl()" target="_blank">
                                <span class="label" *ngIf="!icon">{{label}}</span>
                                <ion-icon *ngIf="icon && icon.value" name="{{icon.value}}" class="icon icon-md item-icon {{icon.value}}"></ion-icon>
                                <img *ngIf="icon && icon._url" src="{{icon._url}}"/>
                            </a>
                       </p>`
})
export class UrlOutput extends BaseOutput
{
	public icon  : any;
	public label : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,		
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'url-output ';
    }

	_openUrl()
	{
		if(this.setting.function)
		{
			const fc = FunctionsType.get().getFunction(this.setting.function);

            if(fc)
            {
                fc({
                    data : this.output.data
                });
            }
            else
            {
                console.log('this.core().functions not found', this.setting.function);
            }
		}
		else
		{
			this.openUrl(this.output?.value);
		}		
	}

    loadOutput()
    {
        if(this.output.formItem.option)
        {
            this.icon = this.output.formItem.option;
		}		

		if(this.setting.label)
        {
            this.label = this.setting.label;
		}		
		else if(this.output.value)
		{
			this.label = this.output.value;
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}
    }
}
