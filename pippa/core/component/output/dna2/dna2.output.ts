import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }      from '../base.output';
import { EmbedModal }      from '../../../modal/embed/embed.modal';
import { ModalController } from '../../../controller/modal/modal.controller';

@Component({
    selector 		: `.dna2-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<table class="striped no-output">
							<tr *ngFor="let item of columns;">
								<td>{{item.label}}</td>
								<td>{{item.percentage}}%</td>
							</tr>
						</table>`
})
export class DNA2Output extends BaseOutput
{
    public columns : Array<any>;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public modalController   : ModalController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'dna2-output '; 
    }

    loadOutput()
    {
        this.columns = this.output.value.columns;
    }

    onMore(item:any)
    {
        this.modalController.create(EmbedModal,
        {
            title   : item.label,
            embed   : item.text,
            onClose : () =>
            {
                this.modalController.dismiss()
            }
        }, true);
    }
}
