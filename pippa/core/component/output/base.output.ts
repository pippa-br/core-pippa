import { Input, HostListener, ElementRef, Renderer2, HostBinding, OnDestroy, Output, EventEmitter, ChangeDetectorRef, Directive } from '@angular/core';
import { DomController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { Core }   from '../../../core/util/core/core';
import { IApp }   from '../../../core/interface/app/i.app';
import { Params } from '../../util/params/params';
import { FunctionsType } from '../../../core/type/functions/functions.type';

@Directive()
export abstract class BaseOutput implements OnDestroy
{
    public _promise            : Promise<any>;
    public _output             : any;
	public _display            : boolean = true;
	public _displayParent      : boolean = true;
    public _disable            : boolean = false;
    public _isRendererComplete : boolean = false;
	public _resolve            : any;    
	public noEmpty             : boolean = false; 
    public map                 : boolean = false; 
	public parent              : any;
	public setting             : any;
	public field               : any;
	public formItem            : any;
	public data  	           : any;
	public isInput 			   : boolean = false;
	public siblingReferences   : any;
	public siblingParams       : any;
	public siblingFunction     : any;
	public siblingAction       : any;
	public siblingValues       : any;
	public subscriptions       : Subscription = new Subscription();
	public grid                : any;
	public _viewer             : any;

    @HostBinding('class') nameClass : string = 'output ';
	@HostBinding('class.hide') hide : boolean = false;

    @Output('rendererComplete') rendererCompleteEvent : EventEmitter<any> = new EventEmitter();

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }

    @HostListener('click', ['$event'])
    onClick(e?:any)
    {
		if(this.output.formItem.router)
		{
			this.toRoute();
		}
		else
		{
			e;

			if(this.output.formItem && this.output.formItem.onClick)
			{
				this.output.formItem.onClick({
					data : this.output.data
				});
			}
		}
	};

	toRoute()
	{
		if(this.output.formItem.router)
		{
			this.output.formItem.params = this.output.formItem.params;
			this.core().push(this.output.formItem.router.value, this.output.formItem.params);	
		}
	}
	
    isRendererComplete()
    {
        return new Promise((resolve) =>
        {
            this._resolve = resolve;

            if(this._isRendererComplete)
            {
                this._resolve();
            }
        });
    }

	async openUrl(url:string)
	{
		this.core().util.openUrl(url);	
	}

    rendererComplete()
    {
        this.onRendererComplete();
        this.markForCheck();
    }

    onRendererComplete()
    {
        this._isRendererComplete = true;
        this.rendererCompleteEvent.emit();

        if(this._resolve)
        {
            this._resolve();
        }
    }

    set classes(values:any)
    {
        this.domController.write(() => 
        {
            for(let key in values)
            {
                if(values[key] != '')
                {
                    this.renderer.addClass(this.elementRef.nativeElement, values[key])
                }
            }    
        })
    }

    @Input()
    set output(value:any)
    {
        if(value)
        {
			this._output  = value;			
			this.formItem = this.output.formItem;
			this.field    = this.output.formItem.field;
			this.data     = this.output.data;
            this.map      = this.output.map;

			this.initSettings();
			///this.initSibling();	
			//O PORQUE DISSO??? QUANDO ENCONTRAR PRECISA REVER POIS ELE PEGA O SIBLIGN DO FIELD E DEVERIA PEGAR APENAS DO OUTPUT algo do tipo siblingOutput = true

            this.loadOutput();
            
            this.rendererComplete();
        }
    }

    get output():any
    {
        return this._output;
	}
	
	initSettings()
    {
        /* DENIFIR AQUI */
		//this.siblingFunction;
		
		this.setting = this.output.formItem.setting || {};

        if(this.setting.noEmpty) 
		{
			this.noEmpty = this.setting.noEmpty;
		}

		if(this.setting.isInput) 
		{
			this.isInput = this.setting.isInput;
		}

		/* SIBLING FIELD */
		if(this.setting.siblingReference)
		{
			this.siblingReferences = this.setting.siblingReference.split(',');
			this.siblingValues     = [];

			/* PARAMS */
			if(this.setting.siblingParams)
			{
				this.siblingParams = this.setting.siblingParams;
			}
			else
			{
				this.siblingParams = '';
			}

			/* ACTION */
			if(this.setting.siblingAction)
			{
				this.siblingAction = this.setting.siblingAction;
			}
			else
			{
				this.siblingAction = 'input';
			}

			/* FUNCTION */
			if(this.setting.siblingFunction)
			{
				this.siblingFunction = FunctionsType.get().getFunction(this.setting.siblingFunction);
			}
		}
    }

    initSibling()
    {
        for(const key in this.siblingReferences)
        {
			const path = this.siblingReferences[key];

           console.log('siblingControls', this.siblingReferences[key], this.data[path]);

            this.siblingValues[key] = this.data[path];

			this.subscriptions.add(this.data.onPropertyUpdate(path).subscribe((value:any) =>
			{
				console.log('siblingControls', this.field.name, value);
				this.siblingValues[key] = value;
				this.doSiblingFunction();
			}));
        }

        //this.doSiblingFunction();
	}

	doSiblingFunction()
    {
		this.siblingFunction(this.data, this.siblingParams).then((value:any) =>
		{
			console.log('siblingAction', this.siblingAction, this.siblingParams, value, this.data);

			if(this.siblingAction == 'maxlength')
			{                    
				//this.maxlength(value);
			}
			else if(this.siblingAction == 'display')
			{                    
				//this.display = value;
			}
			else
			{
				/* NÃO PODE USAR DIRETAMENTE O SETVALUE POIS TEM COMPONENT COM OVERRIDE */
				/* SOMENTE SELECIONA O QUE TEM VALOR, LIMPEZA E TRATATO COM siblingClear ??? pois preciso jogar valor 0 */
				if(value != undefined)
				{
					this.setValue(value);
				}
			}
		});
    }
	
	setValue(value)
	{
		const data 				 = {};
		data[this.formItem.path] = value;

		this.data.set(data);
		
		this.grid.instance.collectionChangeEvent.emit();
	}

    loadOutput()
    {

    }

	get viewer()
    {
        return this._viewer;
    }

    set viewer(value:any)
    {
		this._viewer = value;
	}

    get display():boolean
    {
        return this._display;
    }

    set display(value:boolean)
    {
		this._display 	   = value;
		this.hide     	   = !value;
		this.disable  	   = !value;
		this.displayParent = value;
	}
	
	get displayParent():boolean
    {
        return this._displayParent;
    }

    set displayParent(value:boolean)
    {
		this._displayParent = value;
		this.hide     	    = !value;

		/* DISPLAY O PARENT */
		if(this.parent)
		{
			this.parent.displayParent = value;
		}		                
    }

    set disable(value:boolean)
    {
		this._disable = value;
	
        /* DISABILITA O PARENT */
        if(this.parent)
        {
            this.parent.disable = value;
        }

        this.markForCheck();
    }
    
    get disable():boolean
    {
        return this._disable;
    }

    //setStyle()
    //{
        //if(this.output.column && this.output.column.col && this.elementRef && this.renderer)
    //    {
            //this.renderer.setElementAttribute(this.elementRef.nativeElement, 'col-' + this.output.field.col, 'true');
    //    }

        //this.renderer.addClass(this.elementRef.nativeElement, 'col');
    //}

    round(value:any)
    {
        return Math.round(value);
    }

    core()
    {
        return Core.get()
    }

	showAlert(title:string, message:string, buttons?:any, inputs?:any)
    {
		const alertController = this.core().injector.get(AlertController);

        const alert = alertController.create({
            header  : title,
			message : message,
			inputs  : inputs || [],
            buttons : buttons || ['OK']
        })
        .then((alert:any) =>
        {
            alert.present();
        });
	}

    getApp(value:any)
    {
        return this.core().getApp(value);
    }

    goApp(app:IApp, args?:any)
    {
        this.core().navAppController.goApp(app, args);
    }

    push(viewer:any, args?:any, queryParams?:any)
    {
        this.core().push(viewer, args, queryParams);
    }

    goMenu(menu:any)
    {
        this.core().goMenu(menu);
    }

    api()
    {
        return Core.get().api
	}
	
	createResult(params:Params)
    {
        return this.core().resultFactory.create(params);
    }

    createParams(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    ngOnDestroy()
    {
		//console.log('output ngOnDestroy');
		
		if(this.subscriptions)
		{
			this.subscriptions.unsubscribe();
		}		
    }

    trackById(index:any, item:any) { index; return item.id; }
}
