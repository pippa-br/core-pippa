import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.select-icon-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p>
                            <ion-icon *ngIf="output && output.value && output.value.value" name="{{output.value.value}}" class="icon icon-md item-icon"></ion-icon>
                            <img *ngIf="output && output.value && output.value._url" src="{{output.value._url}}"/>
                       </p>`
})
export class SelectIconOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'select-icon-output ';
    }
}
