import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.video-playlist-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<div *ngIf="output && grid && collection">
							<div dynamic-list [grid]="grid" [collection]="collection"></div>
							<ion-infinite-scroll threshold="100px" (ionInfinite)="loadData()" [disabled]="!collection.hasNextPage()">
								<ion-infinite-scroll-content
									loadingSpinner="bubbles" 
									loadingText="Carregando...">
								</ion-infinite-scroll-content>
							</ion-infinite-scroll>
					   </div>
					   <div *ngIf="output && !output.grid" [innerHtml]="output.value.getEmbed() | safe"></div>`
})
export class VideoPlaylistOutput extends BaseOutput
{
	public collection;
	public grid;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'video-playlist-output ';
	}
	
	loadOutput()
	{
		this.output.value.isInfinite = true;

		this.grid 		= this.output.grid;
		this.collection = this.output.value;		
	}

	loadData()
	{
		this.collection.nextPage();
	}
}
