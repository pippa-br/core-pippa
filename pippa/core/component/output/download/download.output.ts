import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { PDFModal }   from "../../../../core/modal/pdf/pdf.modal";

@Component({
    selector        : `.download-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="output">
                            <span *ngIf="hasCount" class="count">
                                <ion-badge color="medium">{{output.value.length}}</ion-badge>
                            </span> 
                            <span *ngFor="let file of output.value;" [ngClass]="{'no-icon' : !icon && type != 'button'}">
								<a (click)="openUrl(file)" target="_blank" *ngIf="!type">
									<span *ngIf="!icon">{{getName(file)}}</span>
									<ion-icon *ngIf="icon && icon.value" name="{{icon.value}}" class="icon icon-md item-icon {{icon.value}}"></ion-icon>
									<img *ngIf="icon && icon._url" src="{{icon._url}}"/>
								</a>
								<ion-button (click)="openUrl(file)" target="_blank" *ngIf="type == 'button'">
									<span *ngIf="!icon">{{label}}</span>
									<ion-icon *ngIf="icon && icon.value" name="{{icon.value}}" class="icon icon-md item-icon {{icon.value}}"></ion-icon>
									<img *ngIf="icon && icon._url" src="{{icon._url}}"/>
								</ion-button>
                            </span>
                       </p>`
})
export class DownloadOutput extends BaseOutput
{
	public hasCount  : boolean = false;
	public openModal : boolean = false;
	public icon      : any;
	public label     : string;
	public type      : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public modalController   : ModalController
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'download-output ';
    }

    loadOutput()
    {
        if(this.setting.hasCount)
        {
            this.hasCount = this.setting.hasCount;
		}

		if(this.setting.label)
        {
            this.label = this.setting.label;
		}

		if(this.setting.type)
        {
            this.type = this.setting.type;
		}

		if(this.setting.openModal)
        {
            this.openModal = this.setting.openModal;
		}

        if(this.output.formItem.option)
        {
            this.icon = this.output.formItem.option;
		}
	}
	
	getName(file:any)
	{
		return this.label || file.name;
	}

    async openUrl(file:any)
    {
		console.log('file', file);
		
		if(this.openModal && file.isPDF())
		{
			this.openPDF(this.output.data.name, file);			
			//this.documentViewerNative.open(file.url);	
		}
		else
		{
			await Browser.open({url:file.url});
		}
	}
	
	async openPDF(title, pdf:any)
	{
		const modal = await this.modalController.create({
			component	  : PDFModal,
			componentProps : {
				title   : title,
				url     : pdf._url,
				onClose : () =>
				{
					modal.dismiss();
				}
			}
		});

		modal.present();
	}
}
