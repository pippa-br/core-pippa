import { Component, Input, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }      from '../base.output';
import { Output }          from '../../../model//output/output';
import { ModalController } from '../../../../core/controller/modal/modal.controller';
import { EmbedModal }      from '../../../modal/embed/embed.modal';

@Component({
    selector        : `.table-3x3-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<canvas baseChart
                                [datasets]="barChartData"
                                [labels]="barChartLabels"
                                [options]="barChartOptions"
                                [legend]="barChartLegend"
                                [type]="barChartType"
                                (chartClick)="chartClicked($event)">
                        </canvas>`
})
export class Table3x3Output extends BaseOutput
{
    public barChartLabels:string[] = [];
    public barChartType:string     = 'bar';
    public barChartLegend:boolean  = true;
    public minValue                = 8;

    public barChartData:any[] = [
        {
            data : [],
            label : '',
            backgroundColor : [],
        },
        {
            label : 'Média',
            data  : [],
            type  : 'line'
        },
    ];

    barChartColors: any [] = [[]];

    public barChartOptions : any =
    {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    autoSkip: false
                }
            }]
        }
    };

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public modalController   : ModalController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'table-3x3-output ';
    }

    @Input()
    set output(output:any)
    {
        if(output)
        {
            this.barChartData[0].label = output.label;

            for(let key in output.value.answers)
            {
                let item = output.value.answers[key];
                this.barChartLabels.push(item.label);
                this.barChartData[0].data.push(item.amount);

                if(item.amount >= this.minValue)
                {
                    this.barChartData[0].backgroundColor.push('yellowgreen');
                }
                else
                {
                    this.barChartData[0].backgroundColor.push('grey');
                }

                this.barChartData[1].data.push(this.minValue);
			}
        }

        super.output = output;
    }

    get output():any
    {
        return this._output;
    }

    public chartClicked(e:any):void
    {
        if(e.active.length)
        {
            let item = this.output.value.answers[e.active[0]._index];

            if(item.amount >= this.minValue)
            {
                this.modalController.create(EmbedModal,
                {
                    embed : item.text,
                    onClose : () =>
                    {
                        this.modalController.dismiss()
                    }
                })
                .then(modal =>
                {
                    modal.present();
                });
            }
        }
    }
}
