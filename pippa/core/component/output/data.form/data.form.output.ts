import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `.data-form-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div *ngIf="output && output.viewer"
                     dynamic-viewer
                     [viewer]="output.viewer"
                     [data]="output.value"
                     (rendererComplete)="onRendererComplete()">
                </div>`
})
export class DataFormOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'data-form-output ';
    }

    /* OVERRIDER */
    rendererComplete()
    {
    }
}
