import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.integer-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<p *ngIf="output && !isInput" [innerHtml]="value"></p>
					   <div class="input-wrapper" *ngIf="isInput">
							<a (click)="onDecrease()" *ngIf="isIncrease">
								<ion-icon name="remove-circle-outline"></ion-icon>
							</a>														
							<input type="tel"								   
							  	   [(ngModel)]="value"
							  	   autocomplete="false"
								   [ngModelOptions]="{standalone: true}"
								   [readonly]="isIncrease"
							  	   (keyup)="onInput($event)"							
							  	   [mask]="mask"
								   [placeholder]="field.placeholder"/>
							<a (click)="onIncrease()" *ngIf="isIncrease">
								<ion-icon name="add-circle-outline"></ion-icon>
							</a>
					   </div>`
})
export class IntegerOutput extends BaseOutput
{
	public value 	  : number;	
	public isIncrease : boolean = false;
	public mask       : string  = '0{10}';
	
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'integer-output ';
	}

	loadOutput()
    {
        if(this.output.value !== undefined)
        {
            this.value = this.output.value;
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}

		if(this.setting.isIncrease)
		{
			this.isIncrease = this.setting.isIncrease;
		}
	}

	onIncrease()
	{
		this.value++;
		this.setValue(this.value);
	}

	onDecrease()
	{
		if(this.value > 1)
		{
			this.value--;
			this.setValue(this.value);
		}		
	}

	onInput(event)
	{	
		const value = parseInt(event.srcElement.value);

        if(isNaN(value) || value === 0)
        {
			//this.setValue(0);
        }
        else
        {
            this.setValue(value);
        }
	}	
}
