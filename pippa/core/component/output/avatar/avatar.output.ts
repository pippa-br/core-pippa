import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { DomController } from '@ionic/angular';
import { ModalController } from '@ionic/angular'
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';

/* PIPPA */
import { BaseOutput }   from '../base.output';
import { UploadPlugin } from '../../../../core/plugin/upload/upload.plugin';
import { AvatarModal }  from '../../../../core/modal/avatar/avatar.modal';

@Component({
    selector        : `.avatar-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p>
                            <a (click)="changeAvatar()">
                                <img *ngIf="output && !output.value" class="avatar-img" src="./assets/img/avatar.png"/>
                                <img *ngIf="output && output.value" [src]="output?.value?._url"/>
                                <label *ngIf="editable"><ion-icon name="camera-outline"></ion-icon><br>Alterar Foto</label>
                            </a>
                       </p>
                       <input #fileInput type="file" (change)="onUpload($event)" style="display: none;"/>`
})
export class AvatarOutput extends BaseOutput
{
    public editable : boolean = false;

    @ViewChild('fileInput', {static: true}) fileInput : ElementRef;

    constructor(
        public elementRef            : ElementRef,
        public renderer              : Renderer2,
        public changeDetectorRef     : ChangeDetectorRef,
        public uploadPlugin          : UploadPlugin,
        public modalController       : ModalController,
        public actionSheetController : ActionSheetController,
        public domController         : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'avatar-output ';
    }

    loadOutput()
    {
        if(this.output.formItem.setting && this.output.formItem.setting.editable)
        {
            this.editable = this.output.formItem.setting.editable
        }
    }

    changeAvatar()
    {
        if(this.editable)
        {
            if(!this.core().isMobile())
            {
                this.fileInput.nativeElement.click();
            }        
            else
            {
                this.actionSheetController.create(
                {
                    buttons : [
                    {
                        text    : 'Camera',
                        icon    : 'camera',
                        handler : () => 
                        {
                            Camera.getPhoto({
                                quality      : 90,
                                allowEditing : false,
                                resultType   : CameraResultType.DataUrl,
                                source       : CameraSource.Camera,
                            })
                            .then((image:any) => 
                            {
                                /* UPLOAD */
                                this.modalController.create(
                                {
                                    component       : AvatarModal,
                                    componentProps  : {
                                        imageBase64 : image.dataUrl,
                                        onSave      : ((event2:any) =>
                                        {
                                            /* UPLOAD */
                                            this.uploadPlugin.uploadFromString(event2.base64).then(file =>
                                            {
                                                const data : any = {}
                                                data[this.output.formItem.field.name] = file.parseData();                        
                        
                                                this.output.data.set(data);
                                                this.reset();
                        
                                                this.output.value = file;
                                                this.markForCheck();
                        
                                                this.modalController.dismiss();
                                            });
                                        }),
                                        onClose : () =>
                                        {
                                            /* PARA PERMITIR SELECIONAR O MESMO ARQUIVO 2 VEZES */
                                            this.reset();
                        
                                            this.modalController.dismiss();
                                        }
                                    }
                                }).then((modal:any) =>
                                {
                                    modal.present();
                                });
                            })
                        }
                    }, 
                    {
                        text    : 'Biblioteca',
                        icon    : 'images',
                        handler : () => 
                        {
                            this.fileInput.nativeElement.click();
                        }
                    }]
                })
                .then((actionSheet:any) => 
                {
                    actionSheet.present();
                })
            }            
        }        
    }

    onUpload(event:any)
    {
        this.modalController.create(
        {
            component      : AvatarModal,
            componentProps : {
                imageEvent : event,
                onSave     : ((event2:any) =>
                {
                    /* UPLOAD */
                    this.uploadPlugin.uploadFromString(event2.base64).then(file =>
                    {
                        const data : any = {}
                        data[this.output.formItem.field.name] = file.parseData();                        

                        this.output.data.set(data);
                        this.reset();

                        this.output.value = file;
                        this.markForCheck();

                        this.modalController.dismiss();
                    });
                }),
                onClose : () =>
                {
                    /* PARA PERMITIR SELECIONAR O MESMO ARQUIVO 2 VEZES */
                    this.reset();

                    this.modalController.dismiss();
                }
            }
        }).then((modal:any) =>
        {
            modal.present();
        });
    }

    reset()
    {
        this.fileInput.nativeElement.value = "";
    }
}
