import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.last-document-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template		: `<p>{{output?.format}}</p>`
})
export class LastDocumentOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);        
        this.nameClass += 'last-document-output ';

        //this.renderer.addClass(this.elementRef.nativeElement, 'last-document-output');
    }
}
