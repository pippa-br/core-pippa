import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }      from '../base.output';
import { EmbedModal }      from '../../../modal/embed/embed.modal';
import { ModalController } from '../../../../core/controller/modal/modal.controller';

@Component({
    selector : `.disc-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<ion-grid *ngIf="output">
                    <ion-row>
                        <ion-col size="12" size-md="8">
                            <div class="wrap-content">

                                <div class="label-c">C</div>
                                <div class="label-d">D</div>
                                <div class="label-s">S</div>
                                <div class="label-i">I</div>

                                <div class="header-e">E</div>
                                <div class="header-d">D</div>
                                <div class="footer-e">E</div>
                                <div class="footer-d">D</div>

                                <div class="left-a">A</div>
                                <div class="left-b">B</div>
                                <div class="right-a">A</div>
                                <div class="right-b">B</div>

                                <div class="wrap-table">
                                    <table class="disc-table">
                                        <tr *ngFor="let answer of output.value.square; let i = index;" [ngClass]="{'selected' : answer.selected}">
                                            <td *ngFor="let answer of output.value.square[i]; let j = index;"
                                                [ngClass]="{'axis-x' : i == ((output.value.square.length / 2) - 1), 'axis-y' : j == ((output.value.square[i].length / 2) - 1), 'active' : output.value.square[i][j] == 1 }">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </ion-col>
                        <ion-col size="12" size-md="4">

                            <h6>Significado de cada letra</h6>

                            <table class="striped no-output">
                                <tr>
                                    <td>
                                        D
                                    </td>
                                    <td>
                                        Dominância
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        I
                                    </td>
                                    <td>
                                        Influência
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        S
                                    </td>
                                    <td>
                                        Estabilidade
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        C
                                    </td>
                                    <td>
                                        Conformidade
                                    </td>
                                </tr>
                            </table>

                            <h6>Porcentagem por letra</h6>

                            <table class="striped no-output">
                                <tr *ngFor="let item of output.value.percentages; ">
                                    <td>
                                        {{item.label}}
                                    </td>
                                    <td>
                                        {{item.value}} %
                                    </td>
                                </tr>
                            </table>

                            <h6>Sua Classificação</h6>

                            <table class="striped no-output">
                                <tr>
                                    <td>
                                        Letra do DISC
                                    </td>
                                    <td>
                                        {{output.value.key}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>
                                        <a (click)="onMore()">Mais Detalhes</a>
                                    </td>
                                </tr>
                            </table>

                        </ion-col>
                    </ion-row>
                </ion-grid>`
})
export class DiscOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public modalController   : ModalController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'disc-output ';
    }

    onMore()
    {
        this.modalController.create(EmbedModal,
        {
			embed : this.output.value.text,
			
            onClose : () =>
            {
                this.modalController.dismiss()
            }
        }, true);
    }
}
