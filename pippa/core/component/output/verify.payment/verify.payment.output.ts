import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.verify-payment-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<p *ngIf="!map">
						    <i>{{output?.status}}</i> <span [innerHtml]="output?.format"></span>
					   </p>                       
                       <p *ngIf="map">
						    <i>{{output?.value}}</i>
					   </p>`
})
export class VerifyPaymentOutput extends BaseOutput
{
    public hasCharges : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'verify-payment-output ';
    }

    loadOutput()
    {
        if(this.setting.hasCharges)
        {
            this.hasCharges = this.setting.hasCharges;
        }
    }
}
