import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { DomController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.iframe-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p class="wrapper-iframe">
							<iframe *ngIf="url" class="e2e-iframe-untrusted-src"
                            		(load)="onLoad()"
                            		(error)="onLoad()"
                            		[src]="url | safe : 'resourceUrl'">
							</iframe>
					  </p>`					
})
export class IframeOutput extends BaseOutput
{
	public url     : string;
    public loading : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public loadingController : LoadingController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'iframe-output ';
    }

    async loadOutput()
    {
        if(this.output.value)
        {
			if(this.core().isMobile())
			{
				await Browser.open({url : this.output.value});

				Browser.addListener('browserFinished', () => 
				{
					this.core().back();
				});
			}
			else
			{
				this.url = this.output.value;

				this.loading = await this.loadingController.create({
					spinner         : 'dots',
					backdropDismiss : true,
				})
				
				this.loading.present();
	
				setTimeout(() => 
				{
					this.onLoad();
				}, 3000);
			}	            
        }
    }

    onLoad()
    {
        if(this.loading)
        {
            this.loading.dismiss();
            console.log('stop load', this.output.value);
        }
        else
        {
        }
    }
}
