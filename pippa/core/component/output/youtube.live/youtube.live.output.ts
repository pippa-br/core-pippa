import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }    from '../base.output';
import { YoutubePlugin } from '../../../plugin/youtube/youtube.plugin';

@Component({
    selector        : `.youtube-live-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="video-wrapper embed-responsive"
                            *ngIf="output && output.value"
                            [innerHtml]="embed | safe">
                       </div>`
})
export class YoutubeLiveOutput extends BaseOutput
{
    public embed : string;
    public time  : number = 0;
    public _setTimeout : any;    

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public youtubePlugin     : YoutubePlugin,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'youtube-live-output ';
    }

    loadOutput()
    {
        this.clear();

        this._setTimeout = setTimeout(() => 
        {
            this.loadEmbed();
        }, this.time);
    }

    loadEmbed()
    {
        this.youtubePlugin.getLive(this.output.value).then((result:any) => 
        {
            if(result.status && result.data.items.length > 0)
            {
                const video = result.data.items[0];
                this.embed  = '<iframe src="https://www.youtube.com/embed/' + video.id.videoId + '?origin=https://' + window.location.hostname + '&autoplay=1" frameborder="0" width="640" height="360"></iframe>';
                this.markForCheck();
            }
            else
            {
                this.embed = '<p>offline</p>';
                this.markForCheck();
                this.time  = 5000;
                this.loadOutput();
            }
        })
    }

    clear()
    {
        if(this._setTimeout)
        {
            clearTimeout(this._setTimeout);
            //this._setTimeout = null;
        }
    }

    ngOnDestroy()
    {
        super.ngOnDestroy();    
        this.clear();
    }
}
