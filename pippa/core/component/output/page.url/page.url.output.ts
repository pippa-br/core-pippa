import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.page-url-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p class="wrapper-iframe">
							<iframe src="javascript:void(0);" id="{{name}}"
								(load)="onLoad()"
								(error)="onLoad()">
							</iframe>
					   </p>`
})
export class PageUrlOutput extends BaseOutput implements AfterViewInit
{
    //public loading : any;
    public times   : number = 0;
    public name    : string; 

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        //public loadingController : LoadingController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'page-url-output ';
        this.name = 'iframe-' + new Date().getTime();
    }

    loadOutput()
    {
        if(this.output.value)
        {
            /*this.loadingController.create({
                spinner         : 'dots',
                backdropDismiss : true,
            })
            .then(loading =>
            {
                this.loading = loading;
                loading.present()
            });*/

            const iframe: any  = document.getElementById(this.name);
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(this.output.value._body);
            iframe.contentWindow.document.close();

            this.changeDetectorRef.markForCheck();
        }
    }

    ngAfterViewInit()
    {
        
    }

    onLoad()
    {
        /*if(this.loading)
        {
            this.loading.dismiss();
            console.log('onLoad', this.output.value);
        } */               
    }
}
