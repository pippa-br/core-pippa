import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div *ngIf="output" [innerHtml]="output.value | safe"></div>`
})
export class RichTextOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'rich-text-output ck-content ';
    }

    loadOutput()
    {
        /* PUSH CLICK */
        setTimeout(() =>
        {
            this.core().util.makeClickHtml(this.elementRef);
        });

		if(this.noEmpty)
		{
			this.display = false; 
		}
    }
}
