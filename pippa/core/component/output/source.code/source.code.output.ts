import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="code-wrapper" *ngIf="output" [innerHtml]="output.value | safe"></div>`
})
export class SourceCodeOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'source-code-output ';
    }

    loadOutput()
    {
        /* PUSH CLICK */
        setTimeout(() =>
        {
			this.core().util.makeClickHtml(this.elementRef);			
        });
    }
}
