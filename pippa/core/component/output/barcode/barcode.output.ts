import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

declare var require: any;
const JsBarcode = require('jsbarcode');

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.barcode-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p>							
					   		<svg id="barcode-{{id}}" class="barcode"></svg>
					   </p>`
})
export class BarcodeOutput extends BaseOutput
{
	public id : string;
	
    constructor(
        public elementRef        : ElementRef, 
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

		this.nameClass += 'barcode-output ';
		
		this.id = '' + Math.floor(Math.random() * 10000);
    }

    loadOutput()
    {
		setTimeout(() => 
		{
			try 
			{
				JsBarcode("#barcode-" + this.id, this.output.value, 
				{
					//width  : 1.5,
					//height : 80
				});
			}
			catch(err) 
			{
				
			}			
		});		
	}
	
	onClick(e?:any)
	{
		const barcode = document.getElementById('barcode-' + this.id)
		const tmpNode = document.createElement( "div" );
		tmpNode.appendChild( barcode.cloneNode( true ) );
		const divContents = tmpNode.innerHTML; 
		const a = window.open('', 'Imprimir'); 
		a.document.write('<html>'); 
		a.document.write('<body style="text-align: center;">'); 

		if(this.core().account.logo_print)
		{
			a.document.write('<div style="text-align: center;"><img style="width: 100px;" src="' + this.core().account.logo_print._url + '"></div>'); 
		}
		
		a.document.write(divContents); 
		a.document.write('</body></html>'); 
		a.document.close(); 		

		a.onload = () => 
		{
			a.print(); 
			setTimeout(function() 
			{
				a.close();
			}, 10);
		};		
	}
}
