import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.text-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template		: `<p *ngIf="output" [innerHtml]="text"></p>`
})
export class TextOutput extends BaseOutput
{
	public text : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'text-output ';
	}

	loadOutput()
    {
        if(this.output.value !== undefined)
        {
            this.text = this.output.value;
		}
		else if(this.noEmpty)
		{
			this.display = false; 
		}
	}
}
