import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { DomController, ModalController } from "@ionic/angular";

/* PIPPA */
import { BaseOutput } from "../base.output";
import { Viewer }     from "../../../model/viewer/viewer";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";
import { ViewerModal } from "../../../modal/viewer/viewer.modal";

@Component({
    selector        : ".reference-select-output",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="output && !viewerSelected && output?.value" (click)="onViewer()">
                            <span *ngIf="hasPhoto && output.value" class="has-photo">
                                <img *ngIf="output.value.photo" [src]="output.value.photo._url"/>
                                <img *ngIf="!output.value.photo" src="/assets/img/avatar.png"/>
                            </span>
                            <span *ngIf="!map" [innerHtml]="output.format"></span>
                            <span *ngIf="map" [innerHtml]="output.value?.name"></span>
                            <small *ngIf="hasSet">
                                <a (click)="formOpen()">(editar)</a>
                            </small>
						</p>
						<div *ngIf="viewerSelected"
                             dynamic-viewer
                            [viewer]="viewerSelected"
                            [data]="output?.value"
                            (rendererComplete)="markForCheck();">
						</div>`
})
export class ReferenceSelectOutput extends BaseOutput
{
    public hasPhoto 	   = false;
    public hasSet   	   = false;
    public viewerSelected : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
        public modalController   : ModalController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += "reference-select-output ";
    }

    loadOutput()
    {
        if (this.output.formItem.setting)
        {
            if (this.output.formItem.setting.hasPhoto)
            {
                this.hasPhoto = true;
            }

            if (this.output.formItem.setting.hasSet)
            {
                this.hasSet = true;
            } 

            if (this.output.formItem.setting.viewerPath)
            {
                const paths = this.setting.viewerPath.split("/");

                const params = this.createParams({
                    getEndPoint : Types.GET_DOCUMENT_API,
                    accid       : paths[0],
                    appid       : paths[1],
                    colid       : paths[2],
                    path        : this.setting.viewerPath,
                    model       : Viewer,
                    mapItems    : {
                        referencePath : environment.defaultMapItems
                    }
                })

                this.core().api.getObject(params).then((result:any) =>
                {
                    result.data.hasHeader = false;
                    this.viewerSelected   = result.data;                    

                    this.markForCheck();
                });
            }    
        }        
    }

    onViewer()
    {
        this.modalController.create({ 
            component      : ViewerModal, 
            componentProps : {
                //viewer   : new Viewer({ items : this.formItem.items, type : Viewer.SEGMENT_TYPE }),
                document : this.output.value,				
                onClose  : (() =>
                {
                    this.modalController.dismiss();
                }),
            },
            cssClass : "full",
        })
        .then(popover =>
        {
            popover.present(); 
        });
    }

    formOpen()
    {
        this.push("form-document",
            {
                data : this.output.value
            });
    }
}
