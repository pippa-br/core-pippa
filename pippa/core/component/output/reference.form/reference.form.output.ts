import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } 	   	from '../base.output';
import { FormFormPopover } 	from '../../../popover/form.form/form.form.popover';
import { ViewerModal } 		from '../../../modal/viewer/viewer.modal';
import { DocumentPlugin }  	from '../../../plugin/document/document.plugin';
import { SumFormTransform } from '../../transform/sum.form/sum.form.transform';
import { Form } 			from '../../../model/form/form';
import { Lesson }			from '../../../model/lesson/lesson';
import { Types } from '../../../type/types';
import { environment } from 'src/environments/environment';
import { BaseModel } from '../../../model/base.model';

@Component({
    selector 		: `.reference-form-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<p *ngIf="description">
							{{description}}
					   </p>
					   <ion-button size="small" expand="full" color="primary"
									*ngIf="data && !data.actived"
									(click)="onAdd()">
							{{label}}
						</ion-button>
						<ion-button size="small" expand="full" color="primary"
									*ngIf="data && data.actived"
									(click)="onViewer()">
							Resposta
						</ion-button>
						<div class="meter-wrapper">
							<meter min="0" max="100" low="25" high="75" optimum="100" disabled
								   value="{{data?.percentage}}"
								   low="{{data?.minPercentage}}"
								   optimum="{{data?.minPercentage + 1}}">
							</meter>
							<span *ngIf="output">
								<small>{{data?.total}}/{{data?.count}}&nbsp;&nbsp;({{data?.percentage}}%)</small>
							</span>
						</div>`
})
export class ReferenceFormOutput extends BaseOutput
{
	public form  	   : any;
	public data  	   : any;
	public label 	   : string = 'Responder';
	public description : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'reference-form-output ';
	}
	
	loadOutput()
	{
		this.data        = new Lesson(this.output.data);
		this.label 		 = this.output.formItem.label;
		this.description = this.output.formItem.field.description;

		//this.markForCheck();
	}

    async onAdd()
    {
		if(!this.form)
		{
			// const params2 = this.createParams({
			// 	path  : this.output.form.referencePath,
			// 	model : Form,
			// });

			const paths = this.output.form.referencePath.split('/');

			const params2 = this.createParams({
				getEndPoint : Types.GET_DOCUMENT_API,
				accid : paths[0],
				appid : paths[1],
				colid : paths[2],
				path  : this.output.form.referencePath,
				model : Form,
				mapItems    : {
					referencePath : environment.defaultMapItems
				}
			});
	
			const result = await this.api().getObject(params2)
			
			console.log('load form', result.data);
	
			this.form = result.data;
		}

		const modalController = this.core().injector.get(ModalController);
		const documentPlugin  = this.core().injector.get(DocumentPlugin);

		const modal = await modalController.create({
			component       : FormFormPopover,
			cssClass        : 'full',
			showBackdrop 	: false,
			backdropDismiss : true,
			componentProps  :
			{
				form  : this.form,
				onAdd : (async (event:any) =>
				{
					/* ASSOCIA A LESSON */
					event.data.lesson = {
						referencePath : this.output.data.referencePath
					};

					const result = await documentPlugin.add(this.form.appid, this.form, event)
					
					/* QUANDO HOUVER QUESTIONARIO */
					if(this.core().updateLessons)
					{
						this.core().updateLessons(result.data);
					}

					this.markForCheck();
					modalController.dismiss();					
				}),				
				onClose : () =>
				{
					modalController.dismiss();
				}
			},
		});

		modal.present();
	}
	
	async onViewer()
    {
		const modalController = this.core().injector.get(ModalController);
		//const documentPlugin  = this.core().injector.get(DocumentPlugin);

		const modal = await modalController.create({
			component       : ViewerModal,
			cssClass        : 'full',
			showBackdrop 	: false,
			backdropDismiss : true,
			componentProps  :
			{
				document : new BaseModel(this.output.data.quiz.answer),
				onClose  : () =>
				{
					modalController.dismiss();
				}
			},
		});

		modal.present();
    }
}
