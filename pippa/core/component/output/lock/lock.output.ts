import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `.lock-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<p>
                    <ion-icon *ngIf="output.value == 'mobile'" name="phone-portrait-outline"></ion-icon>
                    <ion-icon *ngIf="output.value == 'desktop'" name="desktop-outline"></ion-icon>
                </p>`
})
export class LockOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'lock-output ';
    }
}
