import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }  from '../base.output';
import { EventPlugin } from '../../../plugin/event/event.plugin';

@Component({
    selector 		: `.multi-option-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
		template		: `<p *ngIf="output">							
								<label *ngFor="let option of output.value; let first = first; let last = last" [ngClass]="{'disabled': option.disabled }">
									<a (click)="doOption(option)">
										<img *ngIf="showImage && option.image" [src]="option.image._url">
										<span>{{option.label}} <small *ngIf="!last"> ◦ </small></span>
									</a>
								</label>							
							</p>`
})
export class MultiOptionOutput extends BaseOutput
{
	public router    : string;
	public showImage : boolean = true;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public eventPlugin       : EventPlugin,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'multi-option-output ';
	}

	loadOutput()
	{
		if(this.setting.showImage !== undefined)
		{
			this.showImage = this.setting.showImage;
		}		
	}

	/* REMOVI CLICK OUTPUT */
    onClick(e?:any)
    {		
	};

	doOption(option)
	{
		//this.output.formItem.params.option = JSON.stringify(option);
		//this.toRoute();
	}
}
