import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector		: `.agree-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div *ngIf="output?.value">

							<div *ngIf="hasDescription" class="description">
								<small [innerHtml]="description | breakLine"></small>
							</div>

					   		<ion-icon name="checkmark-outline" class="icon icon-md item-icon"></ion-icon>

					   </div>`
})
export class AgreeOutput extends BaseOutput
{
	public description 	  : string;
	public hasDescription : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'agree-output ';
    }

	loadOutput()
	{
		this.description = this.output.value;

		if(this.setting.hasDescription !== undefined)
		{
			this.hasDescription = this.setting.hasDescription;
		}

		this.markForCheck();
	}
}
