import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { Viewer }     from '../../../model/viewer/viewer';
import { Types } from '../../../type/types';
import { environment } from 'src/environments/environment';

@Component({
    selector        : `.viewer-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div *ngIf="output"
							dynamic-viewer
							[viewer]="viewerSelected"
							[data]="output?.value"
							(rendererComplete)="markForCheck();">
					   </div>`
})
export class ViewerOutput extends BaseOutput
{
	public viewerSelected : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'reference-select-output ';
    }

    loadOutput()
    {
		//console.error('0-00000', this.output, this.viewerSelected);

        if(this.output.formItem.setting)
        {
			if(this.output.formItem.setting.viewerPath)
			{
				const paths =this.output.formItem.setting.viewerPath.split('/');

                const params = this.createParams({
                    getEndPoint : Types.GET_DOCUMENT_API,
                    accid : paths[0],
                    appid : paths[1],
                    colid : paths[2],
                    path  : this.output.formItem.setting.viewerPath,
                    model : Viewer,
                    mapItems    : {
                        referencePath : environment.defaultMapItems
                    }
                })

				this.core().api.getObject(params).then((result:any) =>
				{
					this.viewerSelected = result.data;
					this.markForCheck();
				});
			}    
        }        
    }
}
