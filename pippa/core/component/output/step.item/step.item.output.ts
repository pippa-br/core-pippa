import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController, ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }  	   from '../base.output';
import { ViewerModal } 	   from '../../../modal/viewer/viewer.modal';
import { FormFormPopover } from '../../../popover/form.form/form.form.popover';
import { DocumentPlugin }  from '../../../plugin/document/document.plugin';
import { Viewer }          from '../../../model/viewer/viewer';
import { Form }            from '../../../model/form/form';
import { FormType }        from '../../../type/form/form.type';

@Component({
    selector 		: `.step-item-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<p *ngIf="output" hideDelay="0" showDelay="0" matTooltip="{{tooltip}}">
							<ion-icon *ngIf="!viewerData && !editForm && output.value" name="checkmark-outline"></ion-icon>
							<a *ngIf="viewerData && output.value" (click)="openViewer()"><ion-icon name="checkmark-circle-outline"></ion-icon></a>
							<a *ngIf="editForm" (click)="openForm()"><ion-icon name="list-circle-outline"></ion-icon></a>
					   </p>`
})
export class StepItemOutput extends BaseOutput
{
	public viewerData : boolean;
	public editForm   : boolean;
	public tooltip    : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public modalController   : ModalController,
		public documentPlugin    : DocumentPlugin,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

		this.nameClass += 'step-item-output ';
	}

	loadOutput()
	{
		if(this.setting && this.setting.groupsEditForm)
		{
			const groups  = this.setting.groupsEditForm.split(',');
			this.editForm = this.core().user.hasGroups(groups);
		}
		else
		{
			this.editForm = this.setting && this.setting.editForm;
			this.markForCheck();	
		}

		if(this.setting && this.setting.groupsViewerData)
		{
			const groups    = this.setting.groupsViewerData.split(',');
			this.viewerData = this.core().user.hasGroups(groups);
		}
		else
		{
			this.viewerData = this.setting && this.setting.viewerData;
			this.markForCheck();
		}

		if(this.setting.tooltip)
		{
			this.tooltip = this.setting.tooltip;
		}		
	}
	
	async openViewer()
	{
		const form = await this.output.data.getProperty('form');

		const viewer = new Viewer({
			type  : Viewer.FLAT_TYPE,
			items : form.items.getItemsBySteps(this.output.formItem.path),
		});

		this.modalController.create(
		{
			component 	   : ViewerModal,
			cssClass       : 'full',
			componentProps : 
			{
				title    : this.formItem.label + (this.tooltip ? ' - ' + this.tooltip : ''),
				viewer   : viewer,
				document : this.output.data,
				onClose  : () =>
				{
					this.modalController.dismiss();
				}
			}
		})
		.then(modal =>
		{
			modal.present();
		});
	}

	async openForm()
	{
		const form = await this.output.data.getProperty('form');

		const stepForm = new Form({
			name  : this.output.formItem.label,
			type  : FormType.FLAT_TYPE,
			items : form.items.getItemsBySteps(this.output.formItem.path),
		});

		this.modalController.create(
		{
			component 	   : FormFormPopover,
			cssClass       : 'full',
			componentProps : 
			{
				title  : this.formItem.label,
				form   : stepForm,
				data   : this.output.data,
				onSave : async (event) => 
				{
					delete event.data._logs;

					const result 	 = await this.documentPlugin.set(this.output.data.appid, this.output.data, this.output.data.form, event, true);	
					this.output.data = result.data;

					this.grid.instance.emitReload();

					this.modalController.dismiss();
				},
				onClose  : () =>
				{
					this.modalController.dismiss();
				}				
			}
		})
		.then(modal =>
		{
			modal.present();
		});
	}
}
