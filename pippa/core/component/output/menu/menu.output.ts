import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }      from '../base.output';
import { ModalController } from '../../../controller/modal/modal.controller';
import { MenuCollection }  from '../../../model/menu/menu.collection';

@Component({
    selector 		: `.menu-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<ul class="menu-tile">
						<li *ngFor="let menu of itens">
							<a menuClose (click)="goMenu(menu)">
								<ion-icon [name]="menu?.icon?.value"></ion-icon> 
								<p>{{menu?.name}}</p>
							</a>                        
						</li>
					</ul>`
}) 
export class MenuOutput extends BaseOutput
{
    public itens : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public modalController   : ModalController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'menu-output ';
    }

    async loadOutput()
    {
		this.itens = await new MenuCollection(this.output.value).on(true);
		this.markForCheck();
	}    
	
	goMenu(menu:any)
    {
		if(menu.params)
		{
			menu.params.push({
				label : 'documentPath',
				value : this.output.data.reference.path
			});	
		}
		else
		{ 
			menu.params = [{
				label : 'documentPath',
				value : this.output.data.reference.path,
			}];	
		}	

        this.core().goMenu(menu);
    }
}
