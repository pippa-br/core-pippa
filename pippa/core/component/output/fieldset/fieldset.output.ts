import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector : `.fieldset-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    //template : `<div dynamic-list [grid]="output?.grid" [collection]="output?.value"></div>`
    template : `<div dynamic-viewer [viewer]="output?.viewer" [data]="output?.value"></div>`
})
export class FieldsetOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'fieldset-output ';
    }
}
