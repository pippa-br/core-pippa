import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

/* PIPPA */
import { BaseOutput } from '../base.output';
import { PDFModal }   from "../../../../core/modal/pdf/pdf.modal";

@Component({
    selector        : `.upload-json-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p *ngIf="output">
							<span *ngIf="hasCount" class="count">
								<ion-badge color="medium">{{output.value.length}}</ion-badge>
							</span> 
							<span *ngFor="let file of output.value;">
								<a (click)="openUrl(file)">{{getLabel(file)}}</a>
							</span>
						</p>`
})
export class UploadJsonOutput extends BaseOutput
{
	public hasCount  : boolean = false;
	public openModal : boolean = false;
	public label     : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public modalController   : ModalController
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'upload-json-output ';
    }

    loadOutput()
    {
        if(this.setting.hasCount)
        {
            this.hasCount = true;
		}
		
		if(this.setting.openModal)
        {
            this.openModal = this.setting.openModal;
		}		

		if(this.setting.label)
        {
            this.label = this.setting.label;
		}
	}

	getLabel(file:any)
	{
		if(this.label)
		{
			return this.label;
		}
		else
		{
			return file.name;
		}
	}

    async openUrl(file:any)
    {
		console.log('file', file);
		
		if(this.openModal && file.isPDF())
		{
			this.openPDF(this.output.data.name, file);			
			//this.documentViewerNative.open(file.url);	
		}
		else
		{
			await Browser.open({url:file._url});
		}
	}
	
	async openPDF(title, pdf:any)
	{
		const modal = await this.modalController.create({
			component	  : PDFModal,
			componentProps : {
				title   : title,
				url     : pdf._url,
				onClose : () =>
				{
					modal.dismiss();
				}
			}
		});

		modal.present();
	}

	// REMOVE O CLICK DE PAGINA
    onClick(e?:any)
    {
		
	};
}
