import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController, ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `.star-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<div class="stars" [ngClass]="{'has-edit' : hasEdit}">
							<span *ngFor="let star of stars" (click)="onUpdate(star)"
								  class="star" [class.active]="star <= rating">
								  &#9733;
							</span>
					   </div>`
})
export class StarOutput extends BaseOutput
{
	public stars   : number[] = [1,2,3,4,5];
	public rating  : number   = 0;
	public form    : any;
	public hasEdit : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'star-output ';
	}

	loadOutput()
    {
        if(this.output.value !== undefined)
        {
            this.rating = this.output.value;
		}

		if(this.noEmpty)
		{
			this.display = false; 
		}

		if(this.setting.hasEdit)
		{
			this.hasEdit = this.setting.hasEdit; 
		}

		this.markForCheck();
	}

	onUpdate(star:number)
	{
		if(this.hasEdit)
		{
			const data = {};

			data[this.formItem.path] = star;

			this.output.data.save(data);

			this.rating = star;
		}
	}	
}
