import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { DomController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput }       from '../base.output';
import { FunctionsType }    from '../../../type/functions/functions.type';
import { CheckoutPlugin }   from '../../../plugin/checkout/checkout.plugin';
import { FormFormPopover }  from '../../../popover/form.form/form.form.popover';

@Component({
    selector        : `.share-buttons-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<a shareButton="whatsapp" [url]="url" [title]="title">
							<ion-icon name="logo-whatsapp"></ion-icon>
					   </a>
					   <a shareButton="telegram" [url]="url" [title]="title">
					   		<ion-icon name="send-outline"></ion-icon>
					   </a>
					   <a shareButton="facebook" [url]="url" [title]="title">
							<ion-icon name="logo-facebook"></ion-icon>
					   </a>
					   <a shareButton="twitter" [url]="url" [title]="title">
							<ion-icon name="logo-twitter"></ion-icon>
					   </a>					   
					   <a shareButton="email" [url]="url" [title]="title">
							<ion-icon name="mail-outline"></ion-icon>
					   </a>`
})
export class ShareButtonsOutput extends BaseOutput 
{
    public url     	   : string;
    public title       : string;
	public description : string;
	public image       : any;
	public enabled     : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
		public domController     : DomController,
		public alertController   : AlertController,
		public checkoutPlugin    : CheckoutPlugin,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'share-buttons-output ';
    }

    async loadOutput()
    {
        if(this.setting.url)
        {
            this.url = await this.output.data.getProperty(this.setting.url);
		};

		if(this.setting.title)
        {
            this.title = await this.output.data.getProperty(this.setting.title);
		};

		if(this.setting.description)
        {
            this.description = await this.output.data.getProperty(this.setting.description);
		};

		if(this.setting.image)
        {
            this.image = await this.output.data.getProperty(this.setting.image);
		};

		if(this.output.value != undefined)
		{
			this.enabled = this.output.value;			
		}
		else
		{
			this.enabled = true;
		}
    }

    async _onClick()
    {		     
	}
	
	@HostListener('click', ['$event'])
    onClick(e?:any)
    {        
    };
}
