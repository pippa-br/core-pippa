import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { DomController } from '@ionic/angular';
import { Browser } from '@capacitor/browser';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector        : `.pdf-output`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<p class="wrapper-iframe">
							<ngx-extended-pdf-viewer *ngIf="show" 
													 [src]="url" 
													 useBrowserLocale="true" 
													 [mobileFriendlyZoom]="zoom"
													 [language]="language"
  													 textLayer="true"
													 (pdfLoaded)="onLoad()">
							</ngx-extended-pdf-viewer>							
					  </p>`					
})
export class PDFOutput extends BaseOutput
{
	public show       : boolean = false;
	public url        : string;
    public loading    : any;
    public times      : number = 0;
	public zoom       : string = '100%';
	public language   : string = 'pt-PT';
	public pdfLoading : any = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public loadingController : LoadingController,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.nameClass += 'pdf-output ';
    }

    loadOutput()
    {
        if(this.output.value && this.output.value && this.output.value[0])
        {
			this.url = this.output.value[0]._url;
        }
    }

	set viewer(value:any)
	{
		super.viewer = value;

		// ESSE COMPONENTE DE UM BUG SE ELE ESTIVER EM UM STEP NÃO VISIVEL, NÃO RENDERIZA
		value.component.instance.changeStepEvent.subscribe(async (event:any) => 
		{
			if(!this.pdfLoading && event.step.id == this.formItem.step.id)
			{
				this.show 		= true;
				this.pdfLoading = true;	

				this.loading = await this.loadingController.create({
					spinner         : 'dots',
					backdropDismiss : true,
				})
				
				this.loading.present();
				this.markForCheck();
			}			
		});
	}

    onLoad()
    {
        this.loading.dismiss();
		this.markForCheck();
    }
}
