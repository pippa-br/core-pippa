import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseOutput } from '../base.output';

@Component({
    selector 		: `[sum-documents-output]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<meter min="0" max="100" high="0" value="{{output?.percentage}}" low="{{output?.minPercentage}}" optimum="{{output?.minPercentage + 1}}"></meter>
					  <span *ngIf="output">{{output.total}}/{{output.count}}&nbsp;&nbsp;<small>({{output.percentage}}%)</small></span>`
})
export class SumDocumentsOutput extends BaseOutput
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.renderer.addClass(this.elementRef.nativeElement, 'sum-documents-output');

        this.nameClass += 'sum-documents-output ';
    }
}
