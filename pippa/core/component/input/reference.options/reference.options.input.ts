import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl, } from "@angular/forms";
import { PopoverController, AlertController } from "@ionic/angular";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { Option }             from "../../../../core/model/option/option";
import { Form }               from "../../../../core/model/form/form";
import { OptionPlugin }       from "../../../../core/plugin/option/option.plugin";

@Component({
    selector        : ".reference-options-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                             [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                 [form]="form"
                                 [formItem]="formItem"
                                 (edit)="onEdit($event)"
                                 (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                                <ion-grid>
                                    <ion-row class="item">
                                        <ion-col>

                                            <ng-select [(ngModel)]="selectedId"
                                                       [ngModelOptions]="{standalone: true}"
                                                       [disabled]="!editable"
                                                       [items]="items"
													   [loading]="loading"
													   [multiple]="multiple"
													   [hideSelected]="true"
													   appendTo="body"
                                                       [placeholder]="field.label"
                                                       [bindLabel]="bindLabel"
                                                       [bindValue]="bindValue"
                                                       cancelText="Fechar"
                                                       okText="Selecionar"
													   (close)="onClose()"
													   (search)="onSearch($event)"
                                                       (change)="onInput($event)">
                                            </ng-select>
                                            <p *ngIf="selected">
                                                <a (click)="onEditItem(selected)">
                                                    Editar
                                                </a>
                                                |
                                                <a (click)="onDelItem(selected)">
                                                    Deletar
                                                </a>
                                            </p>
                                        </ion-col>
                                        <ion-col size="2" class="ion-text-center">
                                            <a (click)="addDocument()" class="button-icon">
                                                <ion-icon name="add-circle-outline"></ion-icon>
                                            </a>
                                        </ion-col>
                                    </ion-row>
                                </ion-grid>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class ReferenceOptionsInput extends BaseReferenceInput
{
    public context           : string;
    public popoverController : PopoverController;
    public alertController   : AlertController;
    public formOption        : Form;
    public multiple           = false;
    public appid               = "option";

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
        this.plugin            = this.core().injector.get(OptionPlugin);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col reference-options-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        this.context = null;

        if (this.setting.context)
        {
            this.context = this.setting.context;
        }

        if (this.setting.multiple)
        {
            this.multiple = this.setting.multiple;
        }			

        if (this.setting.appid)
        {
            this.appid = this.setting.appid;
        }			

        /* ADD APP */
        this.option = new Option({
            app : this.core().getApp(this.appid)
        }); 

        this.loadOption();

        this.inputAdd = true;
    }

    onModelChange(data:any)
    {
        if (data && !this.dirty)
        {
            this.dirty = true;
            this.populate(data);	
            
            console.error(data);

        }
    }

    /* OVERRIDE */
    doAdd(data:any)
    {
        this.onInput(data);
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.onInput(data);
    }

    onEditItem(option:Option)
    {
        this.addDocument(option);
    }

    onDelItem(option:Option)
    {
        this.alertController.create({
            header  : "Alerta",
            message : "Deseja remover esse item?",
            buttons : [
                {
                    text    : "Fechar",
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        option.del();
                        return true;
                    }
                }
            ]
        }).then(alert =>
        {
            alert.present();
        });
    }
}
