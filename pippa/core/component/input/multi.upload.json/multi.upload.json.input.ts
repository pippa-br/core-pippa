import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';

/* PIPPA */
import { BaseInput }      from '../base.input';
import { FileCollection } from '../../../model/file/file.collection';

@Component({
    selector        : `.multi-file-json-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div header-input [formItem]="formItem" (set)="onSet($event)" (del)="onDel()"></div>
                       <div [formGroup]="field.formGroup">
                            <div file-viewer-block [files]="files" [hasDel]="true" (del)="onDelFile($event)"></div>
                            <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onChange($event)"/>
                            <input #fileInput *ngIf="!this.core().isApp" type="file" ng2FileSelect multiple [uploader]="uploader" style="display: none;"/>
                            <button ion-button full large (click)="onClick()">{{this.field.label}}</button>
                            <div class="error" *ngIf="submitted">
                                <span *ngIf="field.formGroup.get(field.name).hasError('required')">Arquivo Requerido</span>
                            </div>
                       </div>`
})
export class MultiUploadJsonInput extends BaseInput implements OnInit
{
    @ViewChild('fileInput', {static: false}) fileInput : ElementRef;

    uploader : FileUploader;
    files    : FileCollection = new FileCollection();
    isInit   : boolean        = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col multi-file-json-field';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onClick()
    {
        this.isInit = true;

        if(this.core().isApp)
        {
            /*this.cameraNative.open(
            {
                hasCrop : false
            })
            .then(file =>
            {
                this.files.push(file);
                this.setValue(this.files);
            });*/
        }
        else
        {
            /*this.uploader = this.fileTransferNative.createUploader({
                onCompleteItem : (file:any) =>
                {
                    this.files.push(file);
                    this.setValue(this.files);
                }
            });

            this.fileInput.nativeElement.click();*/
        }
    }

    onChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(!this.isInit)
        {
            this.isInit = true;

            if(value)
            {
                this.files = new FileCollection(value);
            }
        }
    }

    onDelFile(file:any)
    {
        this.files.del(file);
        this.setValue(this.files);
    }

    ngOnInit()
    {
    }
}
