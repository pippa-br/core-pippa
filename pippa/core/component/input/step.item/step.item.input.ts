import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseStepInput }      from '../../input/base.step.input';
import { FormItemPopover }    from '../../../popover/form.item/form.item.popover';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { StepItemCollection } from '../../../model/step.item/step.item.collection';
import { StepItem }           from '../../../model/step.item/step.item';

@Component({
    selector        : `.step-item-input`,
    changeDetection : ChangeDetectionStrategy.OnPush, 
    templateUrl     : `../base.step.input.html`
})
export class StepItemInput extends BaseStepInput implements OnInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }

    getOption()
    {
        return this.formItem.getFormItensOption();
    }

    getFormPopover()
    {
        return FormItemPopover;
    }

    getMaps()
    {
        return {
            list           : { klass : FormItemCollection },
            stepCollection : { klass : StepItemCollection },
            step           : { klass : StepItem },
        }
    }    
}
