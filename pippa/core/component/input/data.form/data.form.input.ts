import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';

/* FIELD */
import { BaseInput }        from '../base.input';
import { Form }             from '../../../model/form/form';
import { StepItem }         from '../../../model/step.item/step.item';
import { BaseModel }        from '../../../model/base.model';
import { FormFormPopover }  from '../../../../core/popover/form.form/form.form.popover';
import { Document }         from '../../../model/document/document';
import { TransformFactory } from '../../../factory/transform/transform.factory';

@Component({
    selector        : `.data-form-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && count == total, 'ng-invalid' : !formControl?.valid }">
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<form dynamic-form
									#dynamicForm
									*ngIf="!openModal"
									[data]="model"
									[form]="subForm"                      
									class="form-form">
								</form>

                                <div class="buttons" *ngIf="openModal">
                                    <a *ngIf="!model" (click)="onAdd()">{{field.label}} ({{count}}/{{total}})</a>
                                    <a *ngIf="model" (click)="onSet()">{{field.label}} ({{count}}/{{total}})</a>
                                </div>

                            </div>
                        </div>`,
})
export class DataFormInput extends BaseInput
{
    @ViewChild('container', { read: BaseInput, static: false }) container : BaseInput

    public model     : any;
    public subForm   : any;
    public total     : number = 0;
    public count     : number = 0;
	public openModal : boolean = true;

    constructor(
        public modalController   : ModalController,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        /* UTILIZADO PARA O SUBFORM TER ACESSO AS CONTROLLERS DO FORM */
        for(const key in this.formItem.items)
        {
            this.formItem.items[key].field.subFormGroup = this.field.formGroup;
        }

		if(this.setting.openModal !== undefined)
		{
			this.openModal = this.setting.openModal;
		}

        /* STEP */
        const step = new StepItem({
            label : this.formItem.field.label
        });

        const items = [];

        for(const key in this.formItem.items)
        {
            const item = this.formItem.items[key];
            item.step = step;
            items.push(item);
        }

        /* FORM */
        this.subForm = new Form({
            name       : this.formItem.label,
            items      : items,
            viewEdit   : true,
            parent     : this.form,
			hasHeader  : false,
			hasButtons : false,
			onInput    : (event:any) => 
			{
				this.setValue(this.normalizeData(event.data));
			}
        });

        this.nameClass += this.formItem.name + '-input input col data-form-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        const validators = [];

        if(this.formItem.required)
        {
            validators.push(Validators.required);
        }
	}
	
	normalizeData(data:any)
	{
		/* SET ATTACHMENT */
		this.setAttachment(data.attachments);
		delete data.attachments;
		delete data._logs;
		delete data._steps;
		delete data.inversedBy;
		delete data.initInversedBy;

		return data;
	}

    /* FIELD */
    onOpenSubForm(data?:any)
    {
        this.modalController.create(
        {
            component      : FormFormPopover,
            componentProps :
            {
                data  : data,
                form  : this.subForm,
                acl   : this.form.instance ? this.form.instance.acl : null,
                onAdd : ((event:any) =>
                {
                    this.setValue(this.normalizeData(event.data));

                    this.modalController.dismiss();
                }),
                onSet : ((event:any) =>
                {
                    this.setValue(this.normalizeData(event.data));

                    this.modalController.dismiss();
                }),
                onClose : (() =>
                {
                    this.modalController.dismiss();
                }),
            },
        })
        .then(popover =>
        {
            popover.present();
        });
    }

	doAdd(event:any)
	{

	}

	doSet(event:any)
	{
		
	}

    onAdd()
    {
        this.onOpenSubForm();
    }

    onSet()
    {
        this.model.attachments = this.getAttachment();
        
        this.onOpenSubForm(this.model);
    }

	/* CHAMADAS ESXTERNAS */
	populate(value:any)
	{
		if(value)
		{
			this.model = new BaseModel();
			this.model.populate(value);				
		}

		super.populate(value);
	}
	
    onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        // NAO PRECISA DISSO, COM MATRXI DA BUG
        // if(value && !this.dirty)
        // {
        //     this.dirty = true;
            
        //     this.model = new BaseModel();
        //     this.model.populate(value);            
        // }

        const data : any = {};

        if(value)
        {
            data[this.formItem.name] = value;
        }

        const document = new Document(data);

        document.on().then(() =>
        {
            TransformFactory.transform(this.formItem, document).then((value2:any) =>
            {
                this.total = value2.total;
                this.count = value2.count;
            });
        });
    }
}
