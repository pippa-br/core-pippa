import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";

@Component({
    selector        : ".checkbox-title-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div header-input
                        [form]="form"
                        [formItem]="formItem"
                        (edit)="onEdit($event)"
                        (del)="onDel()"></div>
                            <div [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                                <input id="{{labelId}}" type="checkbox" formControlName="{{formItem.name}}" [value]="1">
                                <label for="{{labelId}}">{{field.description}}</label>
                            </div>
                            <div class="error" *ngIf="submitted">
                            <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                        </div>`
})
export class CheckboxTitleInput extends BaseInput
{
    public labelId  = "";

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col checkbox-title-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        this.labelId = this.formItem.id + "-" + new Date().getTime();
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
    }
}
