import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseModel } from '../../../model/base.model';
import { Viewer } from '../../../model/viewer/viewer';
import { Types } from '../../../type/types';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `viewer-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div class="input-wrapper">

                            <div 
                                dynamic-viewer
                                [viewer]="viewerSelected"
                                [data]="updateData"
                                (rendererComplete)="markForCheck();">
                            </div>

					   </div>`
})
export class ViewerInput extends BaseInput
{
    public viewerSelected : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        formItem.required = false;

        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col viewer-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';        
    }

    createValidators()
	{
		if(this.setting)
        {
			if(this.setting.viewerPath)
			{
				const paths =this.setting.viewerPath.split('/');

                const params = this.createParams({
                    getEndPoint : Types.GET_DOCUMENT_API,
                    accid : paths[0],
                    appid : paths[1],
                    colid : paths[2],
                    path  : this.setting.viewerPath,
                    model : Viewer,
                    mapItems    : {
                        referencePath : environment.defaultMapItems
                    }
                })

				this.core().api.getObject(params).then((result:any) =>
				{
					this.viewerSelected = result.data;

					this.markForCheck();
				});
			}    
        }
	}

    set updateData(value:any)
    {
        this._updateData = value;

        this.markForCheck();
    }

    get updateData()
    {
        return this._updateData;
    }
}
