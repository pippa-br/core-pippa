import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* FIELD */
import { BaseInput } from '../base.input';

/* CORE */
import { Collection } from '../../../../core/util/collection/collection';

@Component({
    selector        : `.list-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                     <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                        <ion-grid>
                            <ion-row *ngFor="let item of inputs">
                                <ion-col size="11">
                                    <input type="text" (keyup)="onKey($event, item)" value="{{item.value}}" placeholder="{{item.placeholder}}"/>
                                </ion-col>
                                <ion-col size="1" class="del-button">
                                    <a (click)="onRemove(item)">
                                        <ion-icon name="remove-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                            <ion-row>
                                <ion-col size="1" class="add-button">
                                    <a (click)="onAdd()">
                                        <ion-icon name="add-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                        </ion-grid>
                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class ListInput extends BaseInput
{
    inputs     = new Collection();
    countLabel = 1;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        if(this.field.option && this.field.option.items)
        {
            for(let i in this.field.option.items)
            {
                this.inputs.push({
                    placeholder : 'Opção ' + (i),
                    id          : i,
                    value       : this.field.option.items[i],
                });

                this.countLabel++;
            }

            this.updateInput();
        }
        else
        {
            // this.onAdd();
            // this.onAdd();
            // this.onAdd();
            // this.onAdd();
            // this.onAdd();
        }

        this.nameClass += this.formItem.name + '-input input col list-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onKey(event:any, item:any)
    {
        item.value = event.target.value;
        this.updateInput();
    }

    onAdd()
    {
        this.inputs.push({
            placeholder : 'Opção ' + (this.countLabel),
            id          : this.inputs.length,
            value       : '',
        });

        this.countLabel++;
    }

    onRemove(item:any)
    {
        this.countLabel--;
        this.inputs.del(item);
        this.updateInput();
    }

    updateInput()
    {
        let itens = [];

        for(let key in this.inputs)
        {
            if(this.inputs[key].value != '')
            {
                itens.push(this.inputs[key].value);
            }
        }

        if(itens.length > 0)
        {
            this.setValue(itens);
        }
        else
        {
            this.setValue('');
        }
    }

    onModelChange(data:any)
    {
        if(data && !this.dirty)
        {
            for(const key in data)
            {
                this.inputs.push({
                    id    : key,
                    value : data[key],
                });
            }

            this.dirty = true;
        }
    }
}
