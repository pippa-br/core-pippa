import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import * as _ from "lodash" 
//import { DragulaService } from 'ng2-dragula';

/* PIPPA */
import { BaseInput }          from '../../input/base.input';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { StepItemCollection } from '../../../model/step.item/step.item.collection';
import { StepFormPopover }    from '../../../popover/step.form/step.form.popover';

//[dragula]='dragulaCol' [dragulaModel]='items'

//[dragula]="dragulaStep"
//[dragulaModel]="stepsItems"

@Component({
    selector        : `.reference-step-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (edit)="onEdit($event)"
                     (del)="onDel()">
                </div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                    <ion-grid>
                        <ion-row>
                            <ion-col class="bag-items" size="6">
                                <div class="bag-wrapper">
                                    <label class="header-step">Campos</label>
                                    <div class="bag-container drop-col">
                                        <div class="drag-col" *ngFor="let item of items; trackBy: trackById">
                                            <label>{{item.field.label}} <ion-icon name="reorder-four"></ion-icon></label>
                                        </div>
                                    </div>
                                </div>
                            </ion-col>
                            <ion-col class="bag-stepsItems" size="6">
                                <ion-grid>
                                    <ion-row *ngFor="let step of stepsItems; trackBy: trackById">
                                        <ion-col>
                                            <ion-grid class="bag-wrapper">
                                                <ion-row>
                                                    <ion-col>
                                                        <label class="header-step">
                                                            {{step.label}}
                                                            <a (click)="openStep(step)">
                                                                <ion-icon name="create-outline"></ion-icon>
                                                            </a>
                                                            <a (click)="onDelStep(step)">
                                                                <ion-icon name="remove-circle-outline"></ion-icon>
                                                            </a>
                                                        </label>
                                                    </ion-col>
                                                    <ion-col size="1" class="drag-step">
                                                        <ion-icon name="reorder-four-outline"></ion-icon>
                                                    </ion-col>
                                                </ion-row>
                                                <ion-row>
                                                    <ion-grid>

                                                        <!--- ROWS --->

                                                        <ion-row *ngFor="let rows of step.rows; let i = index; trackBy: trackById"
                                                                 [attr.data-step]="step.id"
                                                                 [attr.data-row]="i">
                                                            <ion-col>
                                                                <ion-grid>

                                                                    <ion-row>

                                                                        <!--- COLS --->

                                                                        <ion-col class="drag-col"
                                                                                 [attr.data-step]="step.id"
                                                                                 [attr.data-row]="i"
                                                                                 [attr.data-col]="j"
                                                                                 *ngFor="let item of rows;let j = index; trackBy: trackById">
                                                                            <label>{{item.field.label}}</label>
                                                                        </ion-col>

                                                                    </ion-row>

                                                                </ion-grid>
                                                            </ion-col>
                                                            <ion-col size="1" class="drag-row">
                                                                <ion-icon name="reorder-four-outline"></ion-icon>
                                                            </ion-col>
                                                        </ion-row>

                                                        <!--- ADD ROW --->

                                                        <ion-row [attr.data-step]="step.id"
                                                                 [attr.data-row]="-1"
                                                                 class="no-drag">
                                                            <ion-col>
                                                                <ion-grid>
                                                                    <ion-row class="drop-col no-drag add-drag-row">
                                                                    </ion-row>
                                                                </ion-grid>
                                                            </ion-col>
                                                        </ion-row>

                                                    </ion-grid>
                                                </ion-row>
                                            </ion-grid>
                                        </ion-col>
                                    </ion-row>
                                    <!--- ADD STEP --->
                                    <ion-row class="step">
                                        <ion-col>
                                            <label>
                                                <a (click)="openStep()">
                                                    <ion-icon name="add-circle-outline"></ion-icon>
                                                </a>
                                            </label>
                                        </ion-col>
                                    </ion-row>
                                </ion-grid>
                            </ion-col>
                        </ion-row>
                    </ion-grid>
                    <div class="error" *ngIf="submitted">
                        <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                    </div>
                </div>`
})
export class ReferenceStepInput extends BaseInput implements OnInit
{
    public collection        : any;
    public alertController   : AlertController;
    public defaultItems      : FormItemCollection;
    public modelItems        : FormItemCollection;
    public fieldItems        : FormItemCollection;
    public allSteps          : StepItemCollection;
    public items             : FormItemCollection;
    public stepsItems        : StepItemCollection;
    public dragulaStep       : string;
    public dragulaRow        : string;
    public dragulaCol        : string;
    public popoverController : any;

    constructor(
        //public dragulaService : DragulaService,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.modelItems = new FormItemCollection();

        this.nameClass += this.formItem.name + '-input input col reference-step-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';        
    }

    update()
    {
        const items = [];
        let i       = 0;
        let j       = 0;
        let k       = 0;

        for(const key in this.stepsItems)
        {
            const step : any = this.stepsItems[key];

            i = 0;

            for(const key2 in step.rows)
            {
                k = 0;

                for(const key3 in step.rows[key2])
                {
                    const data = step.rows[key2][key3];

                    data.row = i;
                    data.col = k;

                    step.items.set(data);
                    step.stepOrder = j

                    const _data = data.parseData();
                    //_data.step  = step.reference;

                    items.push(_data);

                    k++;
                }

                i++;
            }

            j++
        }

        this.stepsItems.doMatrix();
        this.stepsItems.doSort();

        this.setValue(items);
    }

    onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(value && !this.dirty)
        {
            this.modelItems = new FormItemCollection(_.merge({}, value)); /* POR CAUSA DO BIND COM O FORM */

            this.modelItems.on(true).then(() =>
            {
                for(const key in this.modelItems)
                {
                    const item : any = this.modelItems[key];
                    item.id = this.modelItems[key].field.id;
                }

                this.displaySteps();

                this.dirty = true;
            });
        };
    }

    displaySteps()
    {
        if(this.defaultItems && this.fieldItems && this.allSteps)
        {
            const totalItems = new FormItemCollection();
            const modelItems = new FormItemCollection();

            totalItems.setItems(this.defaultItems);
            totalItems.setItems(this.fieldItems);

            this.items = new FormItemCollection();
            this.stepsItems = new StepItemCollection();

            for(const key in this.allSteps)
            {
                const step = this.allSteps[key];
                this.stepsItems.set(step.copy());
            }

            for(const key in totalItems)
            {
                const itemField : any = totalItems[key];
                const item      = this.modelItems.get(itemField);
                let exists      = false;

                if(item)
                {
                    const stepItem = item.step;

                    for(const key in this.stepsItems)
                    {
                        let step : any = this.stepsItems[key];

                        if(stepItem.id == step.id)
                        {
                            if(itemField.enabled)
                            {
                                step.items.set(item);
                                exists = true;
                            }
                            else
                            {
                                step.items.del(item);
                            }

                            break;
                        }
                    }

                    if(!exists)
                    {
                        if(itemField.enabled)
                        {
                            this.items.set(item);
                        }
                    }
                    else
                    {
                        modelItems.set(item);
                    }
                }
                else
                {
                    if(itemField.enabled)
                    {
                        this.items.set(itemField);
                    }
                }
            }

            this.modelItems = modelItems;

            this.stepsItems.doMatrix();
            this.stepsItems.doSort();

            this.update();
        }
    }

    getStep(step:any):any
    {
        for(const key in this.stepsItems)
        {
            const step : any = this.stepsItems[key];

            if(step.id == step)
            {
                return this.stepsItems[key];
            }
        }
    }

    /* BIND COM FIELDS EXTERNOS */
    getFields()
    {
        return new Promise<void>(resolve =>
        {
            this.fieldItems = new FormItemCollection();

            if(this.field.setting.fields)
            {
                const names = this.field.setting.fields.split(',');
                let count   = 0;
                let size    = names.length;

                for(const key in names)
                {
                    const name  = names[key];
                    const field = this.form.instances[name].field;
                    const items = this.form.instances[name].items;

                    /* CHANGE CHECKBOX */
                    field.onChange = (() =>
                    {
                        this.getFields().then(() =>
                        {
                            this.displaySteps();
                        });
                    });

                    for(const key2 in items)
                    {
                        const item = items[key2];
                        item.id    = item.field.id; /* O ID DO ITEM DEIXA COM O ID DO FILED */
                        this.fieldItems.set(item);
                    }

                    count++;

                    if(count == size)
                    {
                        resolve();
                    }
                }
            }
            else
            {
                resolve();
            }
        });
    }

    getDefaultItems()
    {
        return new Promise<void>(resolve =>
        {
            this.defaultItems = new FormItemCollection();

            /* CAMPOS EXTRAS */
            if(this.field.setting.defaultItems)
            {
                this.field.setting.defaultItems.on().then(() =>
                {
                    for(const key in this.field.setting.defaultItems)
                    {
                        const item = this.field.setting.defaultItems[key];
                        item.id    = item.field.id; /* O ID DO ITEM DEIXA COM O ID DO FILED */
                        this.defaultItems.set(item);
                    }

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    }

    getSteps()
    {
        return new Promise<void>(resolve =>
        {
            if(this.field.setting.stepsItems)
            {
                const parent = this.field.setting.stepsItems;
                const params = this.createParams({
                    path            : this.core().account.code + '/' + this.core().currentApp.code + '/stepsItems',
                    collectionModel : StepItemCollection,
                    where           : [{
                        field    : 'parent',
                        operator : '==',
                        value    : parent,
                    }],
                    orderBy : 'order',
                });

                this.api().getList(params).then((result:any) =>
                {
                    this.allSteps = result.collection

                    this.subscriptions.add(result.collection.onChange().subscribe(() =>
                    {
                        this.allSteps = result.collection
                        this.displaySteps();
                    }));

                    resolve();
                });
            }
            else
            {
                resolve();
            }
        });
    }

    /* STEP */
    openStep(item?:any)
    {
        //this.core().pushRight('field-form-overlay');

        const parent = this.field.setting.stepsItems;

        this.popoverController.create(
        {
            component      : StepFormPopover,
            componentProps :
            {
                data  : item,
                field : this.field,
                acl   : this.form.instance.acl,
                onAdd : ((event:any) =>
                {
                    /* LOAD */
                    this.core().loadingController.start().then(() =>
                    {
                        const data  = event.data;
                        data.parent = parent;
                        data.accid  = this.core().acccount.accid;
                        data.appid  = this.core().currentApp.code;

                        this.allSteps.save(data).then(() =>
                        {
                            this.popoverController.dismiss();

                            /* LOAD */
                            this.core().loadingController.stop();
                        });
                    });
                }),
                onSet : ((event:any) =>
                {
                    /* LOAD */
                    this.core().loadingController.start().then(() =>
                    {
                        const data = event.data;
                        data.accid = this.core().acccount.accid;
                        data.appid = this.core().currentApp.code;

                        item.set(data).then(() =>
                        {
                            //this.items.set(item);

                            this.popoverController.dismiss();

                            /* LOAD */
                            this.core().loadingController.stop();
                        });
                    });
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass : 'right-popover',
            //enableBackdropDismiss : false,
        })
        .then((popover:any) =>
        {
            popover.present();
        });
    }

    onDelStep(step:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text: 'Fechar',
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text: 'Remover',
                    handler : () =>
                    {
                        step.del();
                        return true;
                    }
                }
            ]
        }).then((alert:any) =>
        {
            alert.present();
        });
    }

    ngOnInit()
    {
        this.getDefaultItems().then(() =>
        {
            this.getSteps().then(() =>
            {
                this.getFields().then(() =>
                {
                    this.displaySteps();
                });
            });
        });
    }
}
