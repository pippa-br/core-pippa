import { Component, ChangeDetectionStrategy, ChangeDetectorRef, HostBinding } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';

@Component({
    selector : `.textarea-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    //styles   : ['textarea { resize: none; width:100%; }'],
    template : `<div class="input-wrapper"
					[ngClass]="{ 'ng-submitted' : submitted, 
								 'ng-valid' 	: formControl?.valid && this.hasValue(), 
								 'ng-invalid'   : !formControl?.valid,
								 'hidden'       : hidden }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">

                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                        <textarea
                               autosize
                               autocomplete="false"
                               [ngModel]="value"
                               [ngModelOptions]="{standalone: true}"
                               [readonly]="!editable"
                               (keyup)="onInput($event)"
                               [placeholder]="field.placeholder">
                        </textarea>

                        <error-input [control]="formControl" [submitted]="submitted">
                        </error-input>
                    </div>
                </div>`
})
export class TextareaInput extends BaseTextInput
{
    @HostBinding('style.height') height : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col textarea-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        if(this.setting && this.setting.height)
        {
            this.height = this.setting.height;
        }
    }

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		}
	}
}
