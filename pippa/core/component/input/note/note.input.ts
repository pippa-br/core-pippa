import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector        : `.note-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div class="item-input"
								 [ngClass]="{'icon' : field?.getIcon(), 'danger' : this.type == 'danger', 'warning' : this.type == 'warning' }">
								<span class="note-type" [innerHtml]="text"></span>
                            </div>                            
                        </div>`
})
export class NoteInput extends BaseInput
{
	public type : string;
	public text : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col note-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';	
		
		if(this.setting.type)
		{
			this.type = this.setting.type;			
		}

		this.text = this.field.description;
	}
	
	populate(value:string)
	{
		this.text = value;
		this.markForCheck();
	}
}
