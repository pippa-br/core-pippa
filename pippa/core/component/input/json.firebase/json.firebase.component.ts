import { Component, Input, Output, ChangeDetectorRef, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormModel } from './json.firebase.model';
import { BaseInput } from '../base.input';

export interface listOfTypes {
  label: string, value: number
}

@Component({
    selector: '.json-firebase',
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl: './json.firebase.component.html'
})
export class JsonFirebaseInput extends BaseInput implements OnInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

  @Input() public listForm: Array<FormModel>;
  @Output() public onSaved: EventEmitter<any> = new EventEmitter();

  public listOfTypes: Array<listOfTypes> = [
    { label: 'String', value: 0 }, { label: 'Number', value: 1 },
    { label: 'Boolean', value: 2 }, { label: 'Map', value: 3 },
    { label: 'Array', value: 4 }, { label: 'Null', value: 5 },
    { label: 'Date', value: 6 }
  ];

  ngOnInit(): void {
    this.nameClass += this.formItem.name + '-input input col button-field ';
    this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    if (this.listForm === undefined) {
      this.listForm = [];
    }
  }

  public addFatherField(): void {
    this.listForm.push(new FormModel());
    this.fixPositionFatherSon();
    this.buildJson();
  }

  private fixPositionFatherSon(): void {
    let newList = [];
    let { fatherList, sonList } = this.sliceFatherAndSon(this.listForm);

    fatherList = fatherList.sort(this.orderByPosition);

    // Join Fathers and Sons
    if (sonList.length > 0) {

      sonList = sonList.sort(this.orderByPosition);
      const lengthTotal = fatherList.length + sonList.length;

      fatherList.forEach((father: FormModel) => {
        newList.push(father);
        sonList.forEach((son: FormModel) => {
          if (father.id === son.parent) {
            newList.push(son);
          }
        });
      });

      while (lengthTotal > newList.length) {

        newList.forEach((fatherOrdened: FormModel, index: number) => {

          sonList.forEach((son: FormModel) => {
            const notDuplicated: boolean = fatherOrdened.id === son.parent && newList.indexOf(son) === -1;
            if (notDuplicated) {
              newList.splice(index + 1, 0, son);
            }
          });

        });

      }

    } else {
      newList = fatherList;
    }

    // Fix the Position
    this.fixPosition(newList);
  }

  private sliceFatherAndSon(listToSlice: Array<any>) {
    let fatherList = [];
    let sonList = [];
    // Slice the fathers and sons
    listToSlice.forEach((item: FormModel) => {
      if (item.parent === null || item.parent === undefined) {
        fatherList.push(item);
      } else {
        sonList.push(item);
      }
    });
    return { fatherList, sonList };
  }

  public typeChange(father: FormModel, event: CustomEvent): void {

    this.disableFatherValidation(father);

    const canAddSon: boolean = event.detail.value === 3 || event.detail.value === 4;

    if (canAddSon) {
      const son = new FormModel();
      son.parent = father.id;
      this.listForm.push(son);
      this.fixPositionFatherSon();
      this.buildJson();
      return;
    }

    // Remove Child's
    this.deleteMenu(father);
    this.buildJson();

  }

  private disableFatherValidation(father: FormModel) {
    this.listForm = this.listForm.map((item: FormModel) => {
      if (father.id === item.id) {
        if (father.jsonForm.controls.fieldValue.disabled) {
          father.jsonForm.controls.fieldValue.enable();
        } else if (item.jsonForm.value.typeSelected === 3 || item.jsonForm.value.typeSelected === 4
          || item.jsonForm.value.typeSelected === 5) {
          father.jsonForm.controls.fieldValue.disable();
        }
        return father;
      }
      if (item.jsonForm.value.typeSelected !== 3
        && item.jsonForm.value.typeSelected !== 4
        && item.jsonForm.value.typeSelected !== 5) {
        const newItem = item;
        newItem.jsonForm.controls.fieldValue.enable();
        return newItem;
      }
      return item;
    });
  }

  private deleteMenu(father: FormModel): void {
    let newList: Array<FormModel> = [];
    let { fatherList, sonList } = this.sliceFatherAndSon(this.listForm);

    sonList = sonList
      .filter((son: FormModel) => son.parent !== father.id);

    fatherList = fatherList.sort(this.orderByPosition);

    // // Join Fathers and Sons
    if (sonList.length > 0) {
      sonList = sonList.sort(this.orderByPosition);

      fatherList.forEach((father: FormModel) => {
        newList.push(father);
        sonList.forEach((son: FormModel) => {
          if (father.id === son.parent) {
            newList.push(son);
          }
        });
      });

      newList.forEach((fatherOrdened: FormModel, index: number) => {

        sonList.forEach((son: FormModel) => {
          const notDuplicated: boolean = fatherOrdened.id === son.parent && newList.indexOf(son) === -1;
          if (notDuplicated) {
            newList.splice(index + 1, 0, son);
          }
        });
      });

    } else {
      newList = fatherList;
    }

    // Fix the Position
    this.fixPosition(newList);
    this.buildJson();

  }

  private fixPosition(newList: FormModel[]) {
    this.listForm = newList
      .map((item: FormModel, index: number): FormModel => {
        item.position = index;
        return item;
      })
      .sort(this.orderByPosition);
  }

  private orderByPosition(a: FormModel, b: FormModel): number {
    if (a.position < b.position) { return -1; }
    if (a.position > b.position) { return 1; }
    return 0;
  }

  public deleteItem(father: FormModel): void {
    this.fixPosition(this.listForm
      .filter((item: FormModel) => father.id !== item.id
        && father.id !== item.parent));
    this.buildJson();
  }

  public addSonField(father: FormModel): void {
    const canAddSon: boolean = father.jsonForm.value.typeSelected === 3 || father.jsonForm.value.typeSelected === 4;
    if (canAddSon) {
      const son = new FormModel();
      son.parent = father.id;
      this.listForm.push(son);
      this.fixPositionFatherSon();
      this.buildJson();
    }
  }

  public buildJson(): void {
    const canSave: boolean = this.listForm.every((value: FormModel) => value.jsonForm.valid);

    let newList = [];
    if (canSave) {
      let { fatherList, sonList } = this.sliceFatherAndSon(this.listForm);

      fatherList = fatherList.sort(this.orderByPosition);

      // Join Fathers and Sons
      if (sonList.length > 0) {

        sonList = sonList.sort(this.orderByPosition);

        const lengthTotal = fatherList.length + sonList.length;

        fatherList.forEach((father: FormModel) => {

          let objToPush = { id: father.id, typeSelected: father.jsonForm.value.typeSelected };
          const key = father.jsonForm.value.fieldName;

          if (father.jsonForm.value.typeSelected === 3) {
            objToPush[key.replace(' ', '')] = {};
          }

          if (father.jsonForm.value.typeSelected === 4) {
            objToPush[key.replace(' ', '')] = [];
          }

          newList.push(objToPush);

          sonList.forEach((son: FormModel) => {

            if (father.id === son.parent) {

              let sonToPush = { id: son.id, parent: son.parent, typeSelected: son.jsonForm.value.typeSelected };
              const sonKey = son.jsonForm.value.fieldName;

              if (son.jsonForm.value.typeSelected === 3) {
                sonToPush[sonKey.replace(' ', '')] = {};
              }
              if (son.jsonForm.value.typeSelected === 4) {
                sonToPush[sonKey.replace(' ', '')] = [];
              }
              if (son.jsonForm.value.typeSelected !== 3 && son.jsonForm.value.typeSelected !== 4) {
                sonToPush[sonKey.replace(' ', '')] = son.jsonForm.value.fieldValue;
              }
              newList.push(sonToPush);

            }

          });

        });

        while (lengthTotal > newList.length) {

          newList.forEach((fatherOrdened: FormModel, index: number) => {

            sonList.forEach((son: FormModel) => {

              let sonToPush = { id: son.id, parent: son.parent, typeSelected: null };

              if (son.jsonForm !== undefined) {

                sonToPush.typeSelected = son.jsonForm.value.typeSelected;

                const sonKey = son.jsonForm.value.fieldName;

                if (son.jsonForm.value.typeSelected === 3) {
                  sonToPush[sonKey.replace(' ', '')] = {};
                }
                if (son.jsonForm.value.typeSelected === 4) {
                  sonToPush[sonKey.replace(' ', '')] = [];
                }
                if (son.jsonForm.value.typeSelected !== 3 && son.jsonForm.value.typeSelected !== 4) {
                  sonToPush[sonKey.replace(' ', '')] = son.jsonForm.value.fieldValue;
                }
              }

              const notDuplicated: boolean = fatherOrdened.id === sonToPush.parent
                && newList.findIndex((item: FormModel) => item.id === sonToPush.id) === -1;

              if (notDuplicated) {
                newList.splice(index + 1, 0, sonToPush);
              }

            });

          });

        }

        newList = this.sanitizeObject(newList);

      } else {
        newList = fatherList.map((father: FormModel) => {
          let fatherToPush: any = {};
          const fatherKey = father.jsonForm.value.fieldName;
          fatherToPush[fatherKey.replace(' ', '')] = father.jsonForm.value.fieldValue === undefined
            ? null : father.jsonForm.value.fieldValue;
          return fatherToPush;
        });
      }

      console.log(
        newList
      );

      this.onSaved.emit(newList);

    }

  }

  public sanitizeObject(newList: Array<any>): Array<any> {
    return newList
      .map(item => {
        item.child = [];
        return item;
      })
      .map((item, i, self) => {
        if (item.parent) {
          self = self
            .map(fatherFound => {
              if (fatherFound.id === item.parent) {
                fatherFound.child.push(item);
              }
              return fatherFound;
            });
        }
        return item;
      })
      .filter(item => !item.parent)
      .map((subitem) => {
        return this.sanitizeChild(subitem);
      });
  }

  public sanitizeChild(item) {
    const copyItem = JSON.parse(JSON.stringify(item));
    delete item.id;
    delete item.parent;
    delete item.typeSelected;

    if (item && item.child && item.child.length === 0) {
      delete item.child;
      return item;
    }

    const keyChild = Object.keys(item)[0];

    if (copyItem.typeSelected === 4) {
      item.child.forEach(child => {
        item[keyChild].push(this.sanitizeChild(child));
      });
      delete item.child;
      return item;
    }

    if (copyItem.typeSelected === 3) {
      const childs = []
      item.child.forEach(child => {
        childs.push(this.sanitizeChild(child));
      });
      childs.forEach(child => {
        const keyProperty = Object.keys(child)[0];
        item[keyChild][keyProperty] = child[keyProperty];
      });
      delete item.child;
      return item;
    }
  }

}
