import { FormGroup, FormControl, Validators } from '@angular/forms';

export class FormModel {
    public position: number = 0;
    public jsonForm: FormGroup = new FormGroup({
        typeSelected: new FormControl(0),
        fieldName: new FormControl('', Validators.required),
        fieldValue: new FormControl('', Validators.required),
    });
    public id: string = this.generateRandomID();
    public parent: string = null;

    constructor(params?: FormModel) {
        if (params !== undefined) {
            this.jsonForm = params.jsonForm;
            this.position = params.position;
            this.id = params.id;
            this.parent = params.parent;
        }
    }

    public setFieldValueRequired() {
        this.jsonForm.controls.fieldValue.setValidators(Validators.required);
    }
    public setFieldValueUnrequired() {
        this.jsonForm.controls.fieldValue.setValidators([]);
    }

    public generateRandomID(): string {
        return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
    }
}