import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector        : `.question-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                <label *ngFor="let item of items; let i = index;">
                                    <input type="radio" formControlName="{{formItem.name}}" [value]="i"/>
                                    {{item.label}}
                                    <ion-icon *ngIf="item.correct && (core().user.showAnswer || core().user.isAdmin())" name="checkmark-outline"></ion-icon>
                                </label>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class QuestionInput extends BaseInput
{
    public items : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col question-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';		
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    async loadOption()
    {		
        this.items = this.option['items'];
    }
}
