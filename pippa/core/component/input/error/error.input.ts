import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

@Component({
    selector        : "error-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<span *ngIf="submitted && hasError('required')">{{requiredText | translate}}</span>
                       <span *ngIf="hasError('single')">{{singleText | translate}}</span>
                       <span *ngIf="hasError('maxCount')">{{maxCountText | translate}}</span>
                       <span *ngIf="hasError('custom')">{{getError('custom') | translate}}</span>
                       <ng-content></ng-content>`
})
export class ErrorInput
{
    public _control   : any; 
    public _submitted : any;
    public subscribe  : any; 

	@Input() requiredText  = "Campo Obrigatório";
	@Input() singleText    = "Item já Cadastrado!";
	@Input() maxCountText  = "Limite máximo já cadastrado!";

	constructor(
        public changeDetectorRef : ChangeDetectorRef
	) 
	{
	}

    @Input() // NÃO PODE DEIXAR O NOME DE FORMCONTROL POIS DAR ERROR
	set control(value:any)
	{
	    this._control = value;

	    if (this._control)
	    {
	        if (this.subscribe)
	        {
	            this.subscribe.unsubscribe();
	        }

	        this.subscribe = this._control.statusChanges.subscribe((status:boolean) => 
	        {
	            this.changeDetectorRef.markForCheck();
	        })
	    }        
	}

    get control()
    {
        return this._control;
    }

    @Input()
    set submitted(value:any)
    {
        this._submitted = value;
        this.changeDetectorRef.markForCheck();
    }

    get submitted()
    {
        return this._submitted;
    }

    hasError(name:any)
    {
        if (this.control)
        {
            return this.control.hasError(name);
        }
    }

    getError(name:any)
    {
        if (this.control)
        {
            return this.control.getError(name);
        }
    }
}
