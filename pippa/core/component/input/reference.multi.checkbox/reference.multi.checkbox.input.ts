import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, ValidationErrors, ValidatorFn, FormGroup } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { BaseModel }          from '../../../model/base.model';
import { BaseList }           from '../../../model/base.list';
import { MenuPlugin }		  from '../../../plugin/menu/menu.plugin';

@Component({
    selector 		: `.reference-multi-checkbox-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup">
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
								<ul>
									<li>
										<ion-searchbar *ngIf="hasSearch"
													autocomplete="off"
													mode="ios"
													cancelButtonText=""
													placeholder="Buscar"
													showCancelButton="always"
													searchIcon="search"
                                                    [debounce]="1000"
													(ionInput)="onSearch($event)">
										</ion-searchbar>
									</li>
									<li *ngFor="let item of items">
										<input id="{{item.id}}"
											class="checkbox-item"
											type="checkbox"
											[checked]="isChecked(item)"
											(click)="onInput2($event, item)">
										<label for="{{item.id}}">

											<img *ngIf="hasPhoto && item.photo"  height="30" width="30" [src]="item.photo._url"/>
											<img *ngIf="hasPhoto && !item.photo" height="30" width="30" src="/assets/img/avatar.png"/>

											{{item[bindLabel]}}
										</label>
										<ul class="level-2" *ngIf="isChecked(item) && level > 1">
											<li *ngFor="let item2 of item._children">
												<input id="{{item2.id}}"
													class="checkbox-item"
													type="checkbox"
													[checked]="isChecked(item2)"
													(click)="onInput2($event, item2)">
												<label for="{{item2.id}}">

													<img *ngIf="hasPhoto && item2.photo"  height="30" width="30" [src]="item2.photo._url"/>
													<img *ngIf="hasPhoto && !item2.photo" height="30" width="30" src="/assets/img/avatar.png"/>

													{{item2[bindLabel]}}
												</label>
												<ul class="level-3" *ngIf="isChecked(item2) && level > 2">
													<li *ngFor="let item3 of item2._children">
														<input id="{{item3.id}}"
															class="checkbox-item"
															type="checkbox"
															[checked]="isChecked(item3)"
															(click)="onInput2($event, item3)">
														<label for="{{item3.id}}">

															<img *ngIf="hasPhoto && item3.photo"  height="30" width="30" [src]="item3.photo._url"/>
															<img *ngIf="hasPhoto && !item3.photo" height="30" width="30" src="/assets/img/avatar.png"/>

															{{item3[bindLabel]}}
														</label>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
								<error-input [control]="formControl" [submitted]="submitted">
									<span *ngIf="hasError('minSelected')">{{'Selecione no mínimo' | translate}} {{minSelected + ' item(s)'}}</span>
									<span *ngIf="hasError('maxSelected')">{{'Selecione no máximo' | translate}} {{maxSelected + ' item(s)'}}</span>
								</error-input>
							</div> 
						</div>`
})
export class ReferenceMultiCheckboxInput extends BaseReferenceInput
{
    public minSelected   : number  = 0;
    public maxSelected   : number  = 0;
    public closeOnSelect : boolean = false;
    public hasSearch     : boolean = false;
	public level  	     : number  = 1;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-multi-checkbox-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';               
    }

	createValidators()
	{
		super.createValidators();
		
		/* CLOSE ON SELECT */
		if(this.setting.closeOnSelect)
		{
			this.closeOnSelect = this.setting.closeOnSelect == 'true';
		}

		/* SEARCH */
		if(this.setting.hasSearch)
		{
			this.hasSearch = this.setting.hasSearch == 'true';
		}

		/* MIN SELECTED */
		if(this.setting.minSelected)
		{
			this.minSelected = parseInt(this.setting.minSelected);
		}

		/* MAX SELECTED */
		if(this.setting.maxSelected)
		{
			this.maxSelected = parseInt(this.setting.maxSelected);
		}

		if(this.setting.level)
        {
            this.level = this.setting.level;
        }

		const validators = [];

        if(this.minSelected > 0)
        {
            validators.push(this.minValidator());
        }

        if(this.maxSelected > 0)
        {
            validators.push(this.maxValidator());
        }
	}

    minValidator() : ValidatorFn
    {
        return (group: FormGroup): ValidationErrors =>
        {
            group;

            let count : number = 0;

            if(this.selected)
            {
                count = this.selected.length;
            }

            if(count < this.minSelected)
            {
                this.setError('minSelected');
            }
            else
            {
                this.delError('minSelected');
            }

            return;
        };
    }

    maxValidator() : ValidatorFn
    {
        return (group: FormGroup): ValidationErrors =>
        {
            group;

            let count : number = 0;

            if(this.selected)
            {
                count = this.selected.length;
            }

            if(count > this.maxSelected)
            {
                this.setError('maxSelected');
            }
            else
            {
                this.delError('maxSelected');
            }

            return;
        };
    }

    getPlugin()
    {
		return this.core().injector.get(MenuPlugin);
    }	

    onSearch(event:any)
    {
        this.term = event.srcElement.value;
        this.reload();
    }

    async mergeSelected()
    {
        if(this.selected && this.collection)
        {
            /* VERIFICA SE JA EXISTE */
            const selecteds = []

            for(const item of this.selected)
            {
                let has = false;

                for(const item2 of this.collection)
                {
                    if(item.id == item2.id)
                    {
                        has = true;
                    }
					else if(this.level > 1)
					{
						for(const item3 of item2._children)
						{
							if(item.id == item3.id)
							{
								has = true;
							}
						}	
					}
                }

                if(!has)
                {
                    selecteds.push(item);
                }
            }

            for(const key in selecteds)
            {
                this.collection._add(selecteds[key]);
            }
        }
    }

    isChecked(item:any)
    {
        if(this.selectedId && this.selectedId.includes(item.id))
        {
            return true;
        }

        return false;
    }

    onInput2(event:any, item:any):any
    {
        if(!this.selected)
        {
            this.selected = new BaseList();
        }

        if(event.currentTarget.checked)
        {
            item.selected = true;
            this.selected.add(item);
        }
        else
        {
            item.selected = false;
            this.selected.del(item);
        }
		
		if(this.selected == undefined)
        {
            this.selectedId = null;
            this.selected   = null;

            this.setValue(null);
        }
        else
        {
            const references = [];

            for(const key in this.selected)
            {
                const item = this.selected[key];
                references.push(item.reference);
            }

            this.setValue(references);
        }
    }

    /* METODO USADO PARA CHAMADAS EXTERNAS */
    populate(list:any)
    {
        return new Promise<any>((resolve) =>
        {
            if(list)
            {				
				this.updateValue(list);
				this.doLabels();	

				this.dirty = true;
			}
			
			resolve(list);
    
            //super.populate(list);
        });        
    }

    onModelChange(items:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(items && !this.dirty)
        {
			this.dirty = true;
            const references = [];

            for(const key in items)
            {
                const item : any = new BaseModel(items[key]);
                item._groupBy    = 'Todos';
                references.push(item);
            }

            const list : any = new BaseList(items);

            /* GET ONLY IDS */
            list.on().then(() =>
            {
                const ids = [];

                for(const key in list)
                {
                    ids.push(list[key][this.bindValue]);
                }

                this.selectedId = ids;
                this.selected   = list;

                /* POR CAUSA DOS VALIDADES */
                this.formControl.updateValueAndValidity();

                this.doLabels();
            });            
        }
    }
}
