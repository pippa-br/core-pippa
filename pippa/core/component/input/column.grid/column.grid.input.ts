import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { PopoverController } from "@ionic/angular";

/* PIPPA */
import { BaseInput }          from "../base.input";
import { FormItem }           from "../../../model/form.item/form.item";
import { Field }              from "../../../model/field/field";
import { FormItemCollection } from "../../../model/form.item/form.item.collection";
import { FormItemPopover }    from "../../../../core/popover/form.item/form.item.popover";

//[dragula]='bagItems' [(dragulaModel)]='items' class="bag" 

@Component({
    selector        : ".column-grid-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                [hasAdd]="true"
                                (add)="onAdd()"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ion-grid>
                                    <ion-row *ngFor="let item of items; let last = last; let i = index; trackBy : trackById">
                                        <ion-col size="9">
                                            <ng-select [(ngModel)]="item.field.type.value"
                                                       [ngModelOptions]="{standalone: true}"
                                                       [items]="option.items"
                                                       [bindLabel]="bindLabel"
                                                       [bindValue]="bindValue"
                                                        groupBy="groupBy"
                                                        appendTo="body"
                                                       [placeholder]="'Tipo'"
                                                       cancelText="Fechar"
                                                       okText="Selecionar"
                                                       (change)="onInput(item, $event)">
                                            </ng-select>
                                        </ion-col>                                        
                                        <ion-col size="3" class="col-buttons">
                                            <a class="set-button" (click)="openFormItem(item, i, option)">
                                                <ion-icon name="create-outline"></ion-icon>
                                            </a>
                                            <a class="del-button" (click)="onRemove(item)">
                                                <ion-icon name="remove-circle-outline"></ion-icon>
                                            </a>
                                            <a>
                                                <ion-icon name="reorder-four-outline"></ion-icon>
                                            </a>
                                        </ion-col>
                                    </ion-row>
                                </ion-grid>
                                <error-input [control]="formControl" [submitted]="submitted">
                                </error-input>
                            </div>
                        </div>`
})
export class ColumnGridInput extends BaseInput
{
    public items    : FormItemCollection = new FormItemCollection();
    public bagItems : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col column-grid-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        /* DRAG */
        /*this.bagItems  = 'bag-' + this.formItem.name;
        const bagItems = this.dragulaService.find(this.bagItems);

        if(bagItems)
        {
            this.dragulaService.destroy(this.bagItems);
        }

        this.dragulaService.createGroup(this.bagItems,
        {
            invalid : function (el)
            {
                return el.tagName == 'ion-grid';
            }
        });

        this.subscriptions.add(this.dragulaService.dropModel(this.bagItems).subscribe(() =>
        {
            this.updateFormItems();
        }));*/
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }	
    }

    onModelChange(value:any)
    {
        if (value && !this.dirty)
        {
            this.items.setItems(value);

            this.dirty = true;

            /* UTLIZAR PARA FAZER UPDATE E REMOVER O SELECTED */
            //this.updateFormItems();
        }
    }

    onAdd()
    {
        this.items.push(new FormItem({
            field : new Field()
        }));
    }

    onInput(formItem:FormItem, formItemSelected:any)
    {
        if (formItem)
        {
            delete formItemSelected.id;
            formItem.populate(formItemSelected);
        }

        this.updateFormItems();
    }

    onRemove(item:any)
    {
        this.items.del(item);
        this.updateFormItems();
    }

    updateFormItems()
    {
        /* SETIMEOUT FAZ O DRAG FUNCIONAR */
        //setTimeout(() =>
        //{
        const itens = [];

        for (const key in this.items)
        {
            itens.push(this.items[key].parseData());
        }

        if (itens.length > 0)
        {
            this.setValue(itens);
        }
        else
        {
            this.setValue(null);
        }
        //});
    }

    openFormItem(formItem:any, index:any, option:any)
    {
        const popoverController = this.core().injector.get(PopoverController);
		
        popoverController.create(
            {
                component : FormItemPopover,
                componentProps :
            {
                data   : formItem,
                acl    : this.form.instance.acl,
                option : option,
                onSet  : ((event:any) =>
                {
                    //this.items[index] = new FormItem(event.data);
                    formItem.populate(event.data);
                    popoverController.dismiss();                    
                    
                    this.updateFormItems();
                }),
                onClose : (() =>
                {
                    popoverController.dismiss();
                }),
            },
                showBackdrop : false,
                cssClass     : "right-popover",
            })
        .then((popover:any) =>
        {
            popover.present();
        });
    }
}
