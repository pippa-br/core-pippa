import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `label-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
									'ng-valid' 	: formControl?.valid && this.hasValue(), 
									'ng-invalid'   : !formControl?.valid,
									'hidden'       : hidden }">

							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>

					   </div>`
})
export class LabelInput extends BaseInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        formItem.required = false;

        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col label-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }
}
