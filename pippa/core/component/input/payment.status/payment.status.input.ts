import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseModel } from '../../../model/base.model';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector        : `.payment-status-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                                <ng-select #select
                                           [(ngModel)]="selectedValue"
                                           [ngModelOptions]="{standalone: true}"
                                           [disabled]="!editable"
                                           [searchable]="searchable"
                                           [bindLabel]="bindLabel"
                                           [bindValue]="bindValue"
										   groupBy="groupBy"
										   appendTo="body"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           [items]="items"
                                           notFoundText="Item não encontrado"
                                           [placeholder]="field.label"
                                           [clearable]="clearable"
                                           (change)="onInput($event)"
                                           (clear)="onReset()">
                                </ng-select>
                                <p *ngIf="onSetSelected" class="links-bar">
                                    &nbsp;<a *ngIf="hasValue()" (click)="_onSetSelected()"><small>editar</small></a>
                                </p>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class PaymentStatusInput extends BaseInput implements OnInit
{
    @ViewChild('select', {static: false}) select : any;

    public clearable      : boolean = true;
    public searchable     : boolean = false;
    public selectedValue  : any;
    public onSetSelected  : any;
    public items          : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col payment-status-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';			
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		}

        if(this.setting.pagarm1)
        {
            this.items = [
            {
                id    : 'new',
                label : 'Novo',
                value : 'new',
            },
            {
                id    : 'pending',
                label : 'Aguardando',
                value : 'waiting_payment',
            },
            {
                id    : 'paid',
                label : 'Pago',
                value : 'paid',
            },
            {
                id    : 'canceled',
                label : 'Cancelado',
                value : 'canceled',
            },
            {
                id    : 'failed',
                label : 'Recusado',
                value : 'failed',
            }];
        }
        else
        {
            this.items = [
            {
                id    : 'new',
                label : 'Novo',
                value : 'new',
            },
            {
                id    : 'pending',
                label : 'Aguardando',
                value : 'pending',
            },
            {
                id    : 'paid',
                label : 'Pago',
                value : 'paid',
            },
            {
                id    : 'canceled',
                label : 'Cancelado', 
                value : 'canceled',
            },
            {
                id    : 'failed',
                label : 'Recusado',
                value : 'failed',
            },
            {
                id    : 'verify',
                label : 'Verificar Pagamento',
                value : 'verify',
            }];
        }
	}

    _onSetSelected()
    {
        this.onSetSelected({
            target : this,
            data   : this.getValue(),
        });
    }

    reset()
    {
        if(!this.field.fixed)
        {
            this.updateValue(null);
            this.selectedValue = null
        }
    }

    onInput(item:any)
    {
        if(item == undefined)
        {
            this.setValue(null);
            this.selectedValue = null
        }
        else
        {
			const data  = this.getValue() || {};
			data.status = item.value;

			this.setValue(data);
        }

        /*if(this.field.onInput)
        {
            this.field.onInput({
                field : this.field,
                data  : item,
            });
        }*/
    }

    onModelChange(data:any)
    {
        /* NÃO PRECISA DE DIRTY */
        if(data)
        {
            this.selectedValue = data.status;
            this.displayControlByValue(data); /* DEIXAR AQUI, POIS O DATA PODE VIM DEPOIS */
        }
    }

    // displayControlByValue(data:any)
    // {
    //     if(this.isControlField && data)
    //     {
    //         this.displayFields(this.controlField, false); /* DESABILITA TODOS */

    //         if(this.field.setting && this.field.setting[data[this.bindValue]] != undefined)
    //         {
    //             const value = this.field.setting[data[this.bindValue]];

    //             this.displayFields(value, true); /* HABILITA O ATUAL */
    //         }
    //     }
    // }

    async displayControlByValue(data:any)
    {
        if(this.isControl)
        {
			this.displayControl(false); /* DESABILITA TODOS */

            if(this.setting && data)
            {
				const path  = 'status';		
				const model = await new BaseModel(data).on();
				const value = await model.getProperty(path);

				if(value != undefined)
				{
					this.displayControlByNames(this.setting[value], true); /* HABILITA O ATUAL */	
				}				
            }
        }
    }
}
