import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { InputValidate } from "../../../validate/input.validate";

@Component({
    selector        : ".cnpj-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (set)="onSet($event)"
                         (del)="onDel()"></div>
                    <div class="item-input" [formGroup]="field.formGroup">

                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                        <input type="tel"
                               [ngModel]="value"
                               autocomplete="false"
                               [ngModelOptions]="{standalone: true}"
                               (keyup)="onInput($event)"
                               [mask]="mask"
                               [placeholder]="field.placeholder"/>

                        <error-input [control]="formControl" [submitted]="submitted">
                            <span *ngIf="hasError('cnpj')">{{'CNPJ inválido' | translate}}</span>
                        </error-input>
                    </div>
                </div>`
})
export class CNPJInput extends BaseTextInput
{
    public mask  = "00.000.000/0000-00";

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col cnpj-field ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);            
        }		

        this.validators.push(InputValidate.cnpj);
        this.asyncValidators = [ InputValidate.search(this.field, this.form, this) ];
    }

    // onInput = (event:any) =>
    // {
    //     let value = event.srcElement.value.trim();

    //     if (value && value.length >= 18)
    //     {
    //         value = value.replace("-", "").substring(0, 8);

    //         /* GET INFORMATION */
    //         const params = this.createParams({
    //             cnpj : value
    //         });

    //         this.core().api.callable("cnpjwsApi/check", params).then((result:any) =>
    //         {
    //             console.error(result);

    //             // this.field.formGroup.controls["zipcode"].setErrors(null);

    //             // if (result.data && !result.data.erro)
    //             // {
    //             //     console.log("Information Address", result.data);

    //             //     /* TRANSFORM UPPERCASE */
    //             //     if (this.formItem.field.uppercase)
    //             //     {
    //             //         for (const key in result.data)
    //             //         {
    //             //             result.data[key] = result.data[key].toUpperCase();
    //             //         }
    //             //     }

    //             //     this.field.formGroup.patchValue({
    //             //         street   : result.data.logradouro,
    //             //         district : result.data.bairro,
    //             //         city     : result.data.localidade,
    //             //         ibge     : result.data.ibge,
    //             //         state    : result.data.uf,
    //             //         country  : {
    //             //             value : "br"
    //             //         }
    //             //     });																
    //             // }
    //             // else
    //             // {
    //             //     // this.field.formGroup.controls['zipcode'].setErrors({zipcode:true});

    //             //     // console.error('invalid CEP', result.data);
    //             // }
    //         });
    //     }
    // };
}
