import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { IconType }  from '../../../type/icon/icon.type';

@Component({
    selector        : `.password-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid' 	: !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="{{type}}"
									   [placeholder]="field.placeholder"
									   [value]="value"
                                       autocomplete="false"
                                       (blur)="onInput($event)"
                                       (keyup)="onInput($event)"/>

                                <icon-input [isLoading]="isLoading"
                                            [icon]="icon"
                                            (click)="changeType()">
                                </icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('minlength')">{{'Mínimo de 8 caracteres' | translate}}</span>
                            </error-input>
                        </div>`
})
export class PasswordInput extends BaseTextInput
{
    public type  : string = 'password';
    public icon  : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col password-field ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        this.icon = IconType.EYE_OFF;
    }

	createValidators()
	{
        this.validators = [];

        if(!this.hidden)
        {
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);            
			}            

			this.validators.push(Validators.minLength(8));
        }		
	}

    changeType()
    {
        if(this.type == 'password')
        {
            this.type = 'text';
            this.icon = IconType.EYE;
        }
        else
        {
            this.type = 'password';
            this.icon = IconType.EYE_OFF;
        }
    }

    onInput(event:any)
    {
        this.setValue(event.srcElement.value);
    }

    setValue(value:any)
    {
        if(value)
        {
            const md5     = new Md5();
            const encrypt = md5.appendStr(value).end();

            if(value.length >= 8)
            {
                super.setValue(encrypt);
            }
            else if(value.length < 8 && value.length > 0)
            {
                super.setValue(value);
            }

            this.value = value;
        }
    }

    onModelChange(value:any)
    {
        value;
    }
}
