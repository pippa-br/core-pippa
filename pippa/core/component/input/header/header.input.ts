import { Component, Input, EventEmitter, Output, HostBinding, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy  } from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';

/* PIPPA */
import { Field } from '../../../model/field/field';

@Component({
    selector        : `div[header-input]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-grid class="item-header" *ngIf="formItem">
                            <ion-row>
                                <ion-col *ngIf="!onlyButtons && hasLabel" class="ion-align-self-center">
                                    <h4 class="label-title">
                                        <span>
                                            {{formItem.label | translate}}
                                            <ion-icon class="information-outline" *ngIf="formItem.information" name="information-circle" placement="top"></ion-icon>
                                            <span *ngIf="formItem.required">*</span>
                                        </span>
                                    </h4>
                                </ion-col>
                                <ion-col *ngIf="!onlyButtons && hasLabel && label2">
                                    <h4 class="label-title">
                                        <span>
                                            {{label2 | translate}}
                                        </span>
                                    </h4>
                                </ion-col>
                                <ion-col size="1" class="col-buttons" *ngIf="hasAdd || hasSet || hasDel">
                                    <a *ngIf="hasAdd" (click)="onAdd($event)">
                                        <ion-icon name="add-circle-outline"></ion-icon>
                                    </a>
                                    <a *ngIf="hasSet" (click)="onEdit()">
                                        <ion-icon name="create-outline"></ion-icon>
                                    </a>
                                    <a *ngIf="hasDel" (click)="onDel()">
                                        <ion-icon name="close-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                            <ion-row *ngIf="!onlyButtons && formItem.description" class="description">
                                <ion-col>
                                    <small [innerHtml]="formItem.description | breakLine"></small>
                                </ion-col>
                            </ion-row>
                       </ion-grid>`
})
export class HeaderInput implements OnDestroy
{
	public _formItem   : any;
	public unsubscribe : any;

    @HostBinding('class') nameClass : string = 'header-input ';

    @Output('add')  addEvent  : EventEmitter<any> = new EventEmitter();
    @Output('set')  setEvent  : EventEmitter<any> = new EventEmitter();
    @Output('del')  delEvent  : EventEmitter<any> = new EventEmitter();
    @Output('edit') editEvent : EventEmitter<any> = new EventEmitter();

    @Input() label2      : string;
    @Input() onlyButtons : boolean = false;
    @Input() hasAdd      : boolean = false;
    @Input() hasSet      : boolean = false;
    @Input() hasDel      : boolean = false;
    @Input() form        : any;

    constructor(
        public popoverController : PopoverController,
        public alertController   : AlertController,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
    }

    @Input()
    set formItem(value:any)
    {
		this._formItem = value;
		
		if(this._formItem)
		{
			this.unsubscribe = this._formItem.field.onUpdate().subscribe(() => 
			{
				this.changeDetectorRef.markForCheck();
			});	
		}

		if(!this.hasLabel)
		{
			this.nameClass += 'hidden ';
		}

        this.changeDetectorRef.markForCheck();
    }

    get formItem():any
    {
        return this._formItem;
    }

    get hasLabel()
    {
        return this.form && this.formItem && this.form.hasLabel && this.formItem.hasLabel;
	}
	
	ngOnDestroy()
    {
		if(this.unsubscribe)
		{
			this.unsubscribe.unsubscribe();
		}		
	}

    /*openField(event)
    {
        this.popoverController.create(
        {
            component      : FieldFormPopover,
            componentProps : {
                data  : event.data,
                onAdd : (event =>
                {
                    this.field.items.set(event.data);
                    this.bindFields();
                    this.emitSet(this.field);
                }),
            },
            cssClass : 'field-popover'
        })
        .then( popover =>
        {
            popover.present();
        })
    }*/

    /*bindFields()
    {
        if(this.field)
        {
            for(let key in this.field.items)
            {
                let item = this.field.items[key];

                if(this.form && this.form.viewEdit)
                {
                    setTimeout(() =>
                    {
                        item.field.onEdit = (event =>
                        {
                            this.openField(event);
                        });

                        item.field.onDel = (event =>
                        {
                            this.alertController.create({
                                header   : 'Alerta',
                                message : 'Deseja remover esse item?',
                                buttons : [
                                    {
                                        text: 'Fechar',
                                        handler : () =>
                                        {
                                            //alert.dismiss();
                                            return false;
                                        }
                                    },
                                    {
                                        text: 'Remover',
                                        handler : () =>
                                        {
                                            this.field.items.del(event.data);
                                            this.emitSet(this.field);

                                            //alert.dismiss();
                                            return false;
                                        }
                                    }
                                ]
                            }).then(alert =>
                            {
                                alert.present();;
                            })
                        });
                    });
                }
            }
        }
    }*/

    onAdd(event:any)
    {
        this.addEvent.emit(event);
    }

    emitSet(data:any)
    {
        this.setEvent.emit({
            data : data
        });
    }

    onEdit()
    {
        this.editEvent.emit();
    }

    onDel()
    {
        this.delEvent.emit();
    }
}
