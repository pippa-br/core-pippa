import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput,  }   from '../base.input';
import { InputValidate } from '../../../validate/input.validate';
import { Types } from '../../../type/types';
import { environment } from 'src/environments/environment';

@Component({
    selector        : `.agree-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                 [form]="form"
                                 [formItem]="formItem"
                                 (edit)="onEdit($event)"
                                 (del)="onDel()">
                            </div>
                            <div [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)">
								
								<div *ngIf="description" class="description">
									<small [innerHtml]="description | breakLine"></small>
								</div>
                                
                                <input id="{{field.id}}"
                                       [(ngModel)]="isChecked"
                                       [ngModelOptions]="{standalone: true}"
                                       type="checkbox"
                                       (change)="onBlur($event)">
                                <label for="{{field.id}}">Eu li e concordo: {{label}}</label>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('agree')">Você deve concordar com esse termo!</span>
                            </error-input>
                        </div>`
})
export class AgreeInput extends BaseInput implements AfterViewInit
{
	public label       : string;
	public description : string;
    public isChecked   : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    } 

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + '-input input col agree-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

		this.label           = formItem.label;
		this.field.hasLabel  = false;
		this.description     = formItem.description;
		formItem.description = "";

		if(this.setting.documentPath)
		{
			const paths = this.setting.documentPath.split('/');

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid : paths[0],
                appid : paths[1],
                colid : paths[2],                
                path  : this.setting.documentPath,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                }
            })

			const result = await this.core().api.getObject(params);
			
			if(result.data)
			{
				this.description = result.data.content;
				this.markForCheck();
			}
		}
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			this.validators.push(InputValidate.agree);
		}
	}

    onModelChange(value:any)
    {
        value;

        if(!this.dirty)
        {
            this.isChecked = value;
        }
    }

    onBlur(event:any)
    {
        event;
        this.setValue(this.isChecked);
    }
}
