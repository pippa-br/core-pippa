import { Component, Input, Output, ViewChild, ViewContainerRef, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy, AfterViewInit } from "@angular/core";
import { FormGroup } from "@angular/forms";

/* PIPPA  */
import { BaseInput }      from "../base.input";
import { PartialType }    from "../../../type/partial/partial.type";
import { BaseModel }      from "../../../model/base.model";
import { PartialFactory } from "../../../factory/partial/partial.factory";

@Component({
    selector        : "[fieldset-input]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div class="header-wrapper">
                                <div header-input
                                        [form]="form"
                                        [formItem]="formItem"
                                        [hasAdd]="hasAdd"
                                        (edit)="onEdit($event)"
                                        (del)="onDel()">
                                </div>
                                <icon-input [isLoading]="isLoading"
                                            [icon]="field?.getIcon()">
                                </icon-input>
                            </div>
                            <ion-grid class="container-wrap" [formGroup]="formGroup">
                                <ng-template #template></ng-template>
                            </ion-grid>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`,
})
export class FieldsetInput extends BaseInput implements OnDestroy, AfterViewInit
{
    @ViewChild("template", { read : ViewContainerRef, static : true }) template : ViewContainerRef;
	@Input() hasAdd         = false;

	public formGroup          : any 	   = new FormGroup({}, { updateOn : "submit" })
	public components         : Array<any> = [];
	public isAfterViewInit        = true;
	public isCreateComponents     = false;

	constructor(
        public changeDetectorRef : ChangeDetectorRef
	)
	{
	    super(changeDetectorRef);
	}

	async createControls(formItem:any, formGroup:any)
	{
	    /* NAO TEM SUPER */
	    this.formItem = formItem;
	    this.field    = formItem.field;
	    this.hidden   = formItem.field.hidden;
	    this.setting  = this.formItem.setting || {};

	    this.formGroup = new FormGroup({}, { updateOn : "submit" });
	    formGroup.addControl(formItem.name, this.formGroup);
		
	    this.formGroup.instance = this; // FIELDSET NÃO TEM CONTROL, INSTANCE FICA NO GRUPO
	    this.field.formGroup    = this.formGroup;

	    this.nameClass += this.formItem.name + "-input input fieldset-input col ";
	    this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

	    /* DISPLAY */
	    //this.display = this.field.display;

	    //this.clear();

	    /* SALVA A REFERENCIA DE TODOS OS COMPONENTS */
	    if (this.form)
	    {
	        if (!this.form.instances)
	        {
	            this.form.instances = {};
	        }

	        this.form.instances[this.formItem.name] = this;

	        /* VALIDATE */
	        this.subscriptions.add(this.form.saveEvent.subscribe((event:any) =>
	        {
	            this.onValidate(event);
	        }));
	    }
	    else
	    {
	        console.error("not form");
	    }

	    this.subscriptions.add(this.formItem.items.onUpdate().subscribe(async () =>
	    {
	        await this.createComponents();
	    }));

	    await this.createComponents();
	}

	/* OVERRIDER */
	rendererComplete()
	{
	}

	async createComponents()
	{
	    if (this.formItem && this.template && !this.isCreateComponents)
	    {
	        this.isCreateComponents = true;

	        this.clear();

	        console.log("Create Components FieldsetInput");

	        this.components = [];

	        const rows = {};
			
	        /* HIDDENS */
	        rows["hiddens"] = {
	            component : await PartialFactory.createComponent(PartialType.type("Div"), this.template, { classes : [ "hiddens" ] }),
	            total     : 0,
	            columns   : []
	        }

	        this.components.push(rows["hiddens"].component);

	        for (const key in this.formItem.items)
	        {
	            const formItem = this.formItem.items[key];
				
	            /* IMPORTANTE: PASSA INFORMAÇÃO PARA OS FILHOS */
	            formItem.field.display = this.field.display;
	            formItem.field.hidden  = this.field.hidden;

	            if (!formItem.field.blocked)
	            {
	                let colComponent : any;

	                if (formItem.field.isHidden())
	                {
	                    colComponent = await rows["hiddens"].component.instance.addPartial(PartialType.type("Div"),
	                        {
	                            classes : []
	                        });
	
	                    rows["hiddens"].columns.push(colComponent);
	                    rows["hiddens"].total++;						
	                }
	                else
	                {
	                    if (!rows[formItem.row])
	                    {
	                        const classes = [];
	                        classes.push(formItem.field.type.value.toLowerCase() + "-row");
						
	                        rows[formItem.row] = {
	                            component : await PartialFactory.createComponent(PartialType.type("Row"), this.template, { classes : classes }),
	                            total     : 0,
	                            columns   : []
	                        }
	
	                        this.components.push(rows[formItem.row].component);							
	                    }

	                    /* ADICIONA EVENTO AOS FIELDS */
	                    formItem.field.onChange = (event:any) =>
	                    {
	                        /* POR ALGUM MOTIVO O EVENTO CHANGE PEGA NO DIV */
	                        this.updateEvent.emit(this.getValue());
	                    };
						
	                    const classes = [];
	                    classes.push(formItem.field.type.value.toLowerCase() + "-col");
	
	                    colComponent = await rows[formItem.row].component.instance.addPartial(PartialType.type("Col"),
	                        {
	                            classes : classes
	                        });
	
	                    rows[formItem.row].columns.push(colComponent);
	                    rows[formItem.row].total++;
	                }

	                /* INPUT */
	                const inputComponent = await colComponent.instance.addInput(formItem, this.formGroup,
	                    {
	                        form : this.form,
	                    });

	                /* ATTACHMENTS */
	                if (formItem.field.hasAttachments())
	                {
	                    await colComponent.instance.addPartial(PartialType.type("Attachments"));
	                }

	                this.subscriptions.add(inputComponent.instance.setEvent.subscribe((event:any) =>
	                {
	                    this.onSet(event);
	                }));
	            }
	        }

	        /*if(this.form.instance.data)
            {
                this.form.instance.data.getPathValue(this.formItem.name).then((value:any) =>
                {
                    this.field.formGroup.patchValue(value);
                });
            }*/

	        /* CALCULAR COLS */
	        for (const key in rows)
	        {
	            for (const key2 in rows[key].columns)
	            {
	                const col                                   = Math.round(12 / rows[key].total);
	                rows[key].columns[key2].instance.attributes = [ {
	                    name  : "size",
	                    value : 12
	                },
	                {
	                    name  : "size-sm",
	                    value : col,
	                } ];
	            }
	        }

	        const promises = [];

	        for (const key in this.components)
	        {
	            const instance = this.components[key].instance;
	            const promise  = instance.isRendererComplete();

	            promises.push(promise);
	        }					

	        Promise.all(promises).then(() =>
	        {
	            this.onRendererComplete();
	            this.markForCheck();

	            /* QUANDO REDESENHAR ADICIONA O DATA AOS INPUTS */
	            this.data = this._data;

	            this.isCreateComponents = false;
	        });
	    }
	}

	/*setValue(value:any)
    {
        this.updateValue(value);
        this.emitChange(value);
    }*/

	getValue()
	{
	    if (this.formGroup)
	    {
	        return this.formGroup.value;
	    }
	}

	populate(value: any) 
	{
	    this.patchValue(value);
	}

	set data(value:any)
	{
	    this._data = value;
		
	    if (this._data && this.formGroup)
	    {
	        if (typeof this._data.getPathValue != "function")
	        {
	            this._data = new BaseModel(this._data);
	        }

	        this._data.getPathValue(this.formItem.path).then((data2:any) =>
	        {
	            if (data2)
	            {
	                if (typeof data2.parseData == "function")
	                {
	                    this.patchValue(data2.parseData());
	                }
	                else
	                {
	                    this.patchValue(data2);
	                }

	                /* ALTEREI AQUI PARA OS FILHOS TER APENAS AOS DADOS DO PAI, 
						PQ QUANDO USAMOS DISABLED PRECISAMOS DEPOIS DO ACESSO PARA POPULAR */
	                for (const key in this.components)
	                {
	                    this.components[key].instance.data = data2;
	                }			
	            }                
	        });            
	    }
	}

	get data()
	{
	    return this._data;
	}

	set updateData(value:any)
	{
	    this._updateData = value;
		
	    if (this._updateData && this.formGroup)
	    {
	        if (typeof this._updateData.getPathValue != "function")
	        {
	            this._updateData = new BaseModel(this._updateData);
	        }

	        this._updateData.getPathValue(this.formItem.path).then((data2:any) =>
	        {
	            if (data2)
	            {
	                for (const key in this.components)
	                {
	                    this.components[key].instance.updateData = data2;
	                }			
	            }                
	        });            
	    }
	}

	get updateData()
	{
	    return this._updateData;
	}

	validateSave()
	{
	    for (const key in this.components)
	    {
	        if (!this.components[key].instance.validateSave())
	        {
	            return false;
	        }
	    }
		
	    return true;
	}

	validate()
	{
	    for (const key in this.components)
	    {
	        if (!this.components[key].instance.validate())
	        {
	            return false;
	        }
	    }
		
	    return true;
	}

	set submitted(value:boolean)
	{
	    super["submitted"] = value;

	    for (const key in this.components)
	    {
	        this.components[key].instance.submitted = value;
	    }
	}

	set display(value:boolean)
	{
	    super["display"] = value;

	    for (const key in this.components)
	    {
	        this.components[key].instance.display = value;
	    }

	    // QUANDO EXISTE REGRA DE CONTROL FIELD TEM QUE ATUALIZAR, POIS FIZEMOS DISPLAY TRUE ACIMA
	    for (const key in this.components)
	    {
	        this.components[key].instance.updateDisplayControlByValue();
	    }
	}

	updateValue(value:any)
	{
	    if (value)
	    {
	        this.formGroup.patchValue(value);
	    }        			
	}

	valueDefault()
	{
	    return null;
	}

	patchValue(value:any)
	{
	    if (value)
	    {
	        this.formGroup.patchValue(value);
	    }

        
	    //this.formGroup.controls[this.formItem.name].patchValue(value);

	}

	reset()
	{
	    if (this.field)
	    {
	        for (const key in this.formItem.items)
	        {
	            if (this.formItem.items[key].field.instance)
	            {
	                this.formItem.items[key].field.instance.reset();
	            }
	        }
	    }

	    this.dirty = false;
	}

	isValid()
	{
	    for (const key in this.components)
	    {
	        if (!this.components[key].instance.isValid())
	        {
	            return false;
	        }
	    }

	    return true;
	}

	clear()
	{
	    for (const key in this.components)
	    {
	        this.components[key].destroy();
	    }

	    if (this.template)
	    {
	        this.template.clear();
	    }

	    this.components = [];
	}

	ngOnDestroy()
	{
	    super.ngOnDestroy();
	    this.clear();
	}

    /* OVERRRIDE */
    //destroy()
    //{
    //}
}
