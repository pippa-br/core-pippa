import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector : `.scale-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (edit)="onEdit($event)"
                     (del)="onDel()">
                </div>
                <div [formGroup]="field.formGroup" class="item-input" [ngClass]="{'ng-submitted': submitted }">
                    <label>Baixo</label>
                    <label *ngFor="let item of items">
                        <input type="radio" formControlName="{{formItem.name}}" [value]="item.value">
                        <span>{{item.label}}</span>
                    </label>
                    <label>Alto</label>
                </div>
                <div class="error" *ngIf="submitted">
                   <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                </div>`
})
export class ScaleInput extends BaseInput
{
    public items : any;
    
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col scale-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
		this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    async loadOption()
    {
        this.items = this.option['items'];
    }
}
