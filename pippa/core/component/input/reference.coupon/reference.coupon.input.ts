import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { Document }           from '../../../model/document/document';

@Component({
    selector 		: `.reference-coupon-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>
							<div class="item-input"
								[formGroup]="field.formGroup"
								[ngClass]="{'has-add' : inputAdd }">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="text"
									#viewInput
									autocomplete="false"
									[ngModel]="_code"
									[ngModelOptions]="{standalone: true}"
									[readonly]="form?.isSetModeForm() && !field.editable"
									(keyup)="onInput($event)"
									[placeholder]="field.placeholder"/>

								<small *ngIf="selected" class="percentage">Desconto de {{label}}</small>

								<icon-input [isLoading]="isLoading">
								
								</icon-input>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('invalid')">Cupom Invalido!</span>
							</error-input>
						</div>`
})
export class ReferenceCouponInput extends BaseReferenceInput
{
	public _code  	 : string;
	public label  	 : string;
	public lowerCase : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-coupon-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
		super.createValidators();
		
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onInput(event:any)
    {
        this.term = event.srcElement.value;
        this.setValue(null);
        this.reload();
    }

    async doLabels()
    {
		this.isLoading = true;

        if(this.collection && this.term)
        {
            let hasItem : boolean = false;
            let item    : any;

            for(const key in this.collection)
            {
				item = this.collection[key];							

                if(item.status && item._code == this.term)
                {
					hasItem = true;
					this.selected = item;
                    break;
                }
            }

            if(hasItem)
            {
                this.setValue(this.selected);
				this.delError('invalid');

				if(item._percentage)
				{
					this.label = item._percentage + '%';
				}
				else if(item._value)
				{
					this.label = 'R$ ' + item._value.toLocaleString('pt-BR', { minimumFractionDigits : 2, maximumFractionDigits : 2});
				}
            }
            else
            {
				this.setValue(null);
				this.setError('invalid');				
            }
        }
        else
        {
			this.setValue(null);
			this.delError('invalid');
		}
		
		this.isLoading = false;
		this.markForCheck();
    }

    /* OVERRIDE */
    doAdd(data:any)
    {
        this.populate(data);
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.populate(data);
    }

    onModelChange(data:any)
    {
        if(data && !this.dirty)
        {
            const document : any = new Document(data);

            document.on().then(() =>
            {
				this._code 	  = document._code;
				this.selected = document;
                this.dirty    = true;
            });
        }

        this.displayControlByValue(data);
    }
}
