import { Input, Output, OnInit, OnDestroy, AfterViewInit, HostBinding, EventEmitter, ChangeDetectorRef, Directive } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Subscription } from "rxjs";

/* CORE */
import { Core }          	from "../../util/core/core";
import { Params }        	from "../../util/params/params";
import { BaseModel }     	from "../../model/base.model";
import { Option }        	from "../../model/option/option";
import { OptionCollection } from "../../model/option/option.collection";
import { FunctionsType } 	from "../../../core/type/functions/functions.type";
import { Document }      	from "../../model/document/document";

@Directive()
export abstract class BaseInput implements AfterViewInit, OnDestroy, OnInit
{
    @HostBinding("class") 	   nameClass   = "";
    @HostBinding("class.hide") hide 	  = false;

    @Output("add")              addEvent              : EventEmitter<any> = new EventEmitter();
    @Output("del")              delEvent              : EventEmitter<any> = new EventEmitter();
    @Output("set")              setEvent              : EventEmitter<any> = new EventEmitter();
    @Output("edit")             editEvent             : EventEmitter<any> = new EventEmitter();
    @Output("cancel")           cancelEvent           : EventEmitter<any> = new EventEmitter();
    @Output("init")             initEvent             : EventEmitter<any> = new EventEmitter();
    @Output("update")           updateEvent           : EventEmitter<any> = new EventEmitter();
    @Output("attachment")       attachmentEvent       : EventEmitter<any> = new EventEmitter();
    @Output("rendererComplete") rendererCompleteEvent : EventEmitter<any> = new EventEmitter();

    public formItem          : any;
    public formGroup         : any;
    public component         : any;
    public formControl       : any ;
    public validators        : any;
    public asyncValidators   : any;
    //public control           : any;
    public bindValue         : string;
    public bindLabel         : string;
    public bindDescription   : string;
    public controlPath       : string;
    public dirty              = false;
    public option            : any  = new Option();
    public options           : any  = new OptionCollection();
    public setting           : any;
    public siblingContents   : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingReferences : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingFields     : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingOperators  : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingValues     : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingTypes      : Array<any>; /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingControls   : Array<any>;    /* UTILIZADO JUNTO COM OUTRO REFERENCE */
    public siblingFunction   : any; /* FUNCAO A SER EXECUTADA */
    public siblingParams     : any; /* PARMAS PARA A FUNCAO A SER EXECUTADA */
    public siblingAction     : any; /* Ação que será tomada depois, default populate */
    public parent            : any;
    public initial           : any;
    public uppercase          = false;
    public readonly           = false;    
    public changeInput        = false; // QUANDO HOUVE MUDANCA PROVOCADA PELO USUARIO
    public singleText 		  = "Item já Cadastrado!";
    public lastValue 		 : any;
    public valueChangesSetTimeout : any = {};

    public _form       : any;
    public _steps      : any;
    public _data       : any;
    public _updateData : any;
    public _icon       : any;

    protected _field              : any;
    protected isCreateControls     = false;
    protected controlField        : string;
    protected controlStep         : string;
    protected controlType         : string;
    protected isControlField       = false;
    protected isControlStep        = false;
    protected subscriptions       : Subscription = new Subscription();
    protected _isRendererComplete  = false;
    protected _resolve            : any;
    protected _isLoading           = false;
    protected _submitted           = false;
    protected _hidden          	   = false;

    constructor(
public changeDetectorRef : ChangeDetectorRef
    )
    {
        this.initialize();
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }

    initialize()
    {
    // OVERRIDER
    }	

    /* PARA OVERRIDER */
    ngOnInit()
    {
        // OVERRIDER
    }

    ngAfterViewInit()
    {
        this.displayControl(false);
        this.updateDisplayControlByValue();
        this.rendererComplete();
    }

    updateDisplayControlByValue()
    {
        if (this.hasValue())
        {
            this.displayControlByValue(this.getValue());
        }
    }

    isRendererComplete()
    {
        return new Promise((resolve) =>
        {
            this._resolve = resolve;

            if (this._isRendererComplete)
            {
                this._resolve();
            }
        });
    }

    rendererComplete()
    {
        this.onRendererComplete();
    }

    onRendererComplete()
    {
        this._isRendererComplete = true;
        this.rendererCompleteEvent.emit();

        if (this._resolve)
        {
            this._resolve();
        }
    }

    async createControls(formItem:any, formGroup:any)
    {
        this.formItem        = formItem;
        this.field           = formItem.field;
        this.formGroup       = formGroup;
        this.field.formGroup = formGroup;
        this.bindLabel       = this.field.getBindLabel();
        this.bindValue       = this.field.getBindValue();
        this.initial         = this.field.initial;
        this.setting         = this.formItem.setting || {};
        this.hidden          = this.field.hidden; // USADO EM PERMISSOES DE ACESSO
        this.readonly        = this.field.readonly;
        this.uppercase       = this.field.uppercase;

        // CREATE VALIDATORS
        this.createValidators();

        // CREATE CONTROL
        this.formGroup.addControl(formItem.name, new FormControl(this.valueDefault(), {
            validators    		: this.validators,
            asyncValidators : this.asyncValidators,
            updateOn      		: "blur"
        }));

        //NOT this.formControl = formControl; /* IMPORTANTE: PEGAR O FIELD CADASTRADO POR CAUSA DO FILDSET */
        this.formControl          = this.formGroup.get(formItem.name);
        this.formControl.instance = this; /* ADICIONA A INSTANCE AO CONTROLLER */

        if (this.field)
        {
            this.field.placeholder = (this.field.placeholder && this.field.placeholder != "" ? this.field.placeholder : this.field.label);

            /* BIND COMPONENT */
            this.field.instance = this;

        /* ON CLICK */
        //if(this.field.onClick)
        //{
        //this._instance.click.subscribe(this.onClick);
        //}
        }

        /* SALVA A REFERENCIA DE TODOS OS COMPONENTS */
        if (this.form)
        {
            if (!this.form.instances)
            {
                this.form.instances = {};
            }

            this.form.instances[this.formItem.name] = this;

            /* VALIDATE */
            this.subscriptions.add(this.form.saveEvent.subscribe((event:any) =>
            {
                this.onValidate(event);
            }));

            if (!this.form.loadOptionPromises)
            {
                this.form.loadOptionPromises = [];
            }
        }
        else
        {
            console.error("not form");
        }

        // PRIMEIRO CARREGAR AS CONGIFURAÇÃO DEPOIS CARREGADO OS APPS
        this.initSettings();
        this.initSibling();

        //const promises = []

        /* OPTION */
        if (this.formItem.option)
        {
            const data = await this.formItem.getProperty("option", true);

            //this.markForCheck();
            this.option = data;
            this.form.loadOptionPromises.push(this.loadOption());
        }

        /* OPTIONS */
        if (this.formItem.options)
        {
            const data = await this.formItem.getProperty("options", true);

            //this.markForCheck();
            this.options = data;
            this.form.loadOptionPromises.push(this.loadOptions());
        }

        // PRECISA LIBERAR OS OUTROS FILEDS PARA SER CRIADOS EM PARALELO EM QUANTO CARREGA OS OPTIONS;
        //Promise.all(promises).then(() => 
        //{	
        /* DISPLAY */
        this.display = this.field.display;

        this.markForCheck();	
        //});
    }

    createValidators()
    {
        // OVERRIDER
    }

    initSettings()
    {
        /* DENIFIR AQUI */
        this.siblingFunction = this.reload;

        /* SETTINGS */
        if (this.formItem.setting)
        {
        /* CONTROL FIELD */
            if (this.setting.controlField)
            {
                this.isControlField = true;
                this.controlField   = this.setting.controlField;
            }

            /* CONTROL STEP */
            if (this.setting.controlStep)
            {
                this.isControlStep = true;
                this.controlStep   = this.setting.controlStep;
            }

            if (this.setting.controlPath)
            {
                this.controlPath = this.setting.controlPath;
            }					

            /* CONTROL FIELD */
            if (this.setting.controlType)
            {
                this.controlType = this.setting.controlType;
            }
            else
            {
                this.controlType = "display";
            }

            /* BIND VALUE  */
            if (this.setting.bindValue)
            {
                this.bindValue = this.setting.bindValue;
            }

            /* BIND LABEL  */
            if (this.setting.bindLabel)
            {
                this.bindLabel = this.setting.bindLabel;
            }

            /* BIND DESCRIPTION  */
            if (this.setting.bindDescription)
            {
                this.bindDescription = this.setting.bindDescription;
            }

            /* FIELDS */
            if (this.setting.siblingField)
            {
                this.siblingFields = this.setting.siblingField.split(",");
            }		

            /* SIBLING FIELD */
            if (this.setting.siblingReference)
            {
                this.siblingReferences = this.setting.siblingReference.split(",");
                this.siblingValues     = [];
                this.siblingTypes      = [];
                this.siblingControls   = [];

                /* NOT FIELDS */
                if (!this.siblingFields)
                {
                /* USA O MESMO NOME DOS REFERENCES */
                    this.siblingFields = this.siblingReferences;
                }

                /* FUNCTION */
                if (this.setting.siblingFunction)
                {
                    this.siblingFunction = FunctionsType.get().getFunction(this.setting.siblingFunction);
                }

                /* PARAMS */
                if (this.setting.siblingParams)
                {
                    this.siblingParams = this.setting.siblingParams;
                }
                else
                {
                    this.siblingParams = "";
                }

                /* OPERATOR */
                if (this.setting.siblingOperator)
                {
                    this.siblingOperators = this.setting.siblingOperator.split(",");
                }
                else
                {
                    this.siblingOperators = [ "==" ];
                }

                /* ACTION */
                if (this.setting.siblingAction)
                {
                    this.siblingAction = this.setting.siblingAction;
                }
                else
                {
                    this.siblingAction = "input";
                }
            }
        }
        else
        {
            this.setting = {};
        }
    }    

    initSibling()
    {
        for (const key in this.siblingReferences)
        {
            this.siblingControls[key] = this.getFormControl(this.siblingReferences[key]);            

            console.log("initSibling", this.siblingReferences[key], this.siblingControls[key]);

            if (this.siblingControls[key])
            {
                this.siblingValues[key] = this.siblingControls[key].value;
                this.siblingTypes[key]  = this.siblingControls[key].instance.field.type;

                this.subscriptions.add(this.siblingControls[key].valueChanges.subscribe((value:any) =>
                {
                    if (this.valueChangesSetTimeout[key])
                    {
                        clearTimeout(this.valueChangesSetTimeout[key]);	
                    }

                    // EVITA VARIAS CHAMARA CONCORRENTES
                    this.valueChangesSetTimeout[key] = setTimeout(() => 
                    {
                        console.log("siblingControls", this.formItem.name, this.siblingControls[key], value);

                        /* VERIFICA SE OS DADOS É OBJECT */
                        let value2 = this.siblingValues[key];
                        let value3 = value;

                        if (value2 && typeof value2 == "object")
                        {
                            value2 = value2.id;
                        }

                        if (value3 && typeof value3 == "object")
                        {
                            value3 = value3.id;
                        }

                        /* QUANDO ALTERA O SELECT TEM QUE LIMPAR O SELECIONADO QUE FAZ PARTE DE OUTRO FILTRO*/
                        if (value2 && value3 && value2 != value3)
                        {
                            this.siblingClear();
                        }

                        this.siblingValues[key] = value;
                        this.doSiblingFunction();                        
                    }, 100);

                }));

                // QUANDO USAMOS SUBFORM OS DADOS JA FORAM CARREGADOS NO FORM PAI
                // DEPOIS REVER A MELHOR FORMA
                if (this.siblingReferences[key].indexOf("root") > -1)
                {
                    this.doSiblingFunction();
                }			
            }
            else
            {
                this.siblingValues[key] = null;
            }
        }
    }

    doSiblingFunction()
    {
        this.doSiblingData().then((data:any) =>
        {
            this.siblingFunction(data, this.siblingParams, this.setting).then((value:any) =>
            {
                console.log("siblingAction", this.field.name, this.siblingAction, this.siblingParams, value, data);

                if (this.siblingAction == "maxlength")
                {                    
                    this.maxlength(value);
                }
                else if (this.siblingAction == "minlength")
                {                    
                    this.minlength(value);
                }
                else if (this.siblingAction == "slug")
                {                    
                    this.slug(value);
                }
                else if (this.siblingAction == "display")
                {                    
                    this.display = value;
                }
                else if (this.siblingAction == "populateOnlyAdd")
                {                    
                    if (this.isAddModeForm())
                    {
                        this.populate(value);
                    }
                }                
                else if (this.siblingAction == "setCollection")
                {                    
                    if (!(value instanceof Array))
                    {
                        if (!value)
                        {
                            value = [];	
                        }
                        else
                        {
                            value = [ value ];	
                        }						
                    }

                    this.setCollection(value);
                }
                else if (this[this.siblingAction])
                {                    
                    this[this.siblingAction](value);
                }								
                else
                {
                    /* NÃO PODE USAR DIRETAMENTE O SETVALUE POIS TEM COMPONENT COM OVERRIDE */
                    /* SOMENTE SELECIONA O QUE TEM VALOR, LIMPEZA E TRATATO COM siblingClear ??? pois preciso jogar valor 0 */
                    if (value !== undefined)
                    {                        
                        this.populate(value);
                    }
                }
            });
        });
    }

    doSiblingData():any
    {
        return new Promise<any>(resolve =>
        {
            if (this.siblingReferences && this.siblingValues)
            {
                /* COLOCAMOS EM UM OBJETO PARA TER VARIOS CAMPOS PARA CONSULTA OU CALCULOS */
                const data = {
                    _parentValue : this.form.root.value,
                    _formValue   : this.form.formGroup.value,
                    _fieldValue  : this.getValue(),
                };

                for (const key in this.siblingReferences)
                {
                    data[this.siblingReferences[key].replace("root.", "").replaceAll(".", "_")] = (this.siblingValues[key] != undefined ? this.siblingValues[key] : "");
                }

                const model = new BaseModel(data);
                model.on().then(() =>
                {
                    resolve(model);
                });
            }
            else
            {
                resolve(null);
            }
        });
    }

    isAddModeForm()
    {
        return this.form && this.form.isAddModeForm();
    }

    isSetModeForm()
    {
        return this.form && this.form.isSetModeForm();
    }

    @Input()
    set form(data:any)
    {
        this._form = data;
    }

    get form():any
    {
        return this._form;
    }

    @Input()
    set steps(data:any)
    {
        this._steps = data;
    }

    get steps():any
    {
        return this._steps;
    }

    @Input()
    set field(data:any)
    {
        this._field = data;

        /* DETECT CHANGES */
    }

    get field():any
    {
        return this._field;
    }

    get hidden()
    {
        return this._hidden;
    }

    set hidden(value:boolean)
    {
        this._hidden = value;
    }

    reload()
    {
        return new Promise((resolve:any) =>
        {
            resolve();
        })
    }

    setCollection(value:any)
    {
        value;
    }

    maxlength(value:any)
    {
        value;
    }

    minlength(value:any)
    {
        value;
    }

    slug(value:any)
    {
        value;
    }

    /* CHAMADAS EXTERNAS ENTRE INPUTS */
    populate(value:any):any
    {
        /* NÃO CHAMAR SET POR CAUSA DOS LOGS */
        this.updateValue(value);
    }

    createResult(params:Params)
    {
        return this.core().resultFactory.create(params);
    }

    createParams(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    ngOnDestroy()
    {
        console.log("input ngOnDestroy");

        if (this.formGroup)
        {
            /* QUANDO UTILIZAMOS O RECORD O FIELD NÃO TEM PARENT, POIS É UM NOVO GRUPO */
            if (this.formGroup.parent)
            {
                const parent : any = this.formGroup.parent;
                parent.removeControl(this.formItem.name);
            }
            else
            {
                this.formGroup.removeControl(this.formItem.name);
            }

            /*if(this.formItem.items && this.formItem.items.length > 0)
            {
            this.formItem.items.on().then(() =>
            {
            for(const key in this.formItem.items)
            {
            const item = this.formItem.items[key];
            item.delInstance();
            }
            })
            }*/
        }

        this.subscriptions.unsubscribe();
    }

    onValidate(event:any)
    {
        event;
    }

    setValue(value:any)
    {
        this.setLog(value);
        this.updateValue(value);
        this.emitInput(value);
    }

    getValue()
    {
        if (this.formControl)
        {
            return this.formControl.value;
        }
    }

    hasValue()
    {
        const value = this.getValue();

        return value != "" && value != null;
    }

    parseData()
    {
        return this.formGroup.root.value;
    }

    setError(key:string)
    {
        if (this.formControl)
        {
        //const errors = this.formControl.errors || {}; PQ PRECISARIA LISTAR MAIS DE UM ERROR
            const errors = {};
            errors[key]  = true;

            this.formControl.setErrors(errors);

            console.log("set errors", errors);
        }
    }

    delError(key:string)
    {
        if (this.formControl)
        {
            const errors = this.formControl.errors || {};
            delete errors[key];

            if (Object.keys(errors).length > 0)
            {
                this.formControl.setErrors(errors);
            }
            else
            {
                this.formControl.setErrors(null);
            }

            console.log("del errors", errors);
        }
    }

    hasError(name:string)
    {
        if (this.formControl && this.form)
        {
            return this.submitted && !this.formControl.hasError("required") && this.formControl.hasError(name);
        }
    }

    getFormControl(name:string):any
    {
        const names   : any = name.split(".");
        let formGroup : any = this.formGroup.root;        

        if (names.length > 0 && names[0] == "root")
        {            
            formGroup = this.form.root;
            names.shift();
        }

        for (const key in names)
        {
            const _name   = names[key]
            const control = formGroup.controls[_name];

            if (control instanceof FormGroup)
            {
                formGroup = control;

                /* CASO O ULTIMO FOR UM FORMGROUP */
                if (parseInt(key) == names.length - 1)
                {
                    return control;
                }
            }
            else
            {
                return control;
            }
        }        
    }

    /*getSubFormControl(name:string):any
    {
    if(this.field.subFormGroup)
    {
    /* BUSCA NO SUBGROUP, CASO CONTRARIO BUSCA NO FORMGROUP */
    /*const control = this.field.subFormGroup.controls[name];

    if(control)
    {
    return control;
    }
    else
    {
    return this.getFormControl(name);
    }
    }
    else
    {
    return this.getFormControl(name);
    }
    }*/

    updateValue(value:any)
    {
        this._updateValue(value);
    }

    /* POIS TEM CASO QUE HERARQUIA QUE PRECISA TER ACESSO AO ORIGINAL, EX: SELECT QUE EXTENDE DO BASEREFERENCE */
    _updateValue(value:any)
    {
        /* PRIMEIREO TEM QUE ATIVAR ANTES DE ATUALIZAR OS VALORES */
        this.displayControlByValue(value);

        if (value === "")
        {
            value = this.valueDefault();
        }

        if (value != undefined && value != null)
        {
        //this.dirty = true;
        }

        if (this.formControl)
        {
            if (value == undefined)
            {
                value = null;
            }

            if (this.uppercase && typeof value == "string")
            {
                value = value.toUpperCase();
            }

            // ULTIMO VALOR VALIDO
            if (value !== null)
            {
                this.lastValue = value; 
            }

            this.formControl.setValue(value);
            this.formControl.markAsDirty();
        }

        /* INVERSED BY */
        this.updateInversedBy(value, "inversedBy");	

        this.emitChange(value);
    }

    updateInversedBy(value:any, name:string)
    {
        if (this.formItem.setting && this.formItem.setting.inversedBy)
        {
            const control = this.getFormControl(name);

            if (control)
            {
                console.log(name, this.formItem.setting.inversedBy, value);

                let valueBy = control.value;

                if (valueBy == undefined || valueBy == "")
                {
                    valueBy = {};
                }

                valueBy[this.formItem.setting.inversedBy] = value;

                control.setValue(valueBy);
            }
        }
    }

    setLog(value:any)
    {
        const control = this.getFormControl("_logs");

        if (control)
        {
            let valueLog = control.value;

            if (!valueLog || valueLog == "")
            {
                valueLog = {};
            }

            if (value)
            {
                valueLog[this.formItem.name] = value;
            }			

            //console.log('valueLog', value, valueLog);

            control.setValue(valueLog);
        }
    }

    setAttachment(value:any)
    {
        const control = this.getFormControl("attachments");

        if (control)
        {
            let valueAttachment = control.value;

            if (!valueAttachment || valueAttachment == "")
            {
                valueAttachment = {};
            }

            if (!valueAttachment[this.formItem.name])
            {
                valueAttachment[this.formItem.name] = {};
            }

            valueAttachment[this.formItem.name] = value;

            console.log("setAttachment", valueAttachment);

            control.setValue(valueAttachment);
        }
    }

    setAttachmentByType(value:any, attachment:any)
    {
        const control = this.getFormControl("attachments");

        if (control)
        {
            let valueAttachment = control.value;

            if (!valueAttachment || valueAttachment == "")
            {
                valueAttachment = {};
            }

            if (!valueAttachment[this.formItem.name])
            {
                valueAttachment[this.formItem.name] = {};
            }

            if (!valueAttachment[this.formItem.name][attachment.value])
            {
                valueAttachment[this.formItem.name][attachment.value] = {};
            }

            valueAttachment[this.formItem.name][attachment.value] = value;

            console.log("setAttachmentByType", valueAttachment);

            control.setValue(valueAttachment);
        }
    }

    getAttachmentByType(attachment:any)
    {
        const control = this.getFormControl("attachments");

        if (control && control.value)
        {
            const valueAttachment = control.value;

            if (valueAttachment && valueAttachment[this.formItem.name] && valueAttachment[this.formItem.name][attachment.value])
            {
                return new Document({
                    value : valueAttachment[this.formItem.name][attachment.value]
                });
            }
        }

        return;
    }

    getAttachment()
    {
        const control = this.getFormControl("attachments");

        if (control && control.value)
        {
            const valueAttachment = control.value;

            return valueAttachment[this.formItem.name];
        }

        return;
    }

    valueDefault()
    {
        return null;
    }

    valueData()
    {
        if (this.data && this.data[this.formItem.name])
        {
            return this.data[this.formItem.name];
        }
        else if (this.lastValue)
        {
            return this.lastValue;
        }

        return null;
    }

    patchValue(value:any)
    {
        if (this.formControl)
        {
            return this.formControl.patchValue(value);
        }
    }

    onModelChange(data:any)
    {
        data;
    }

    onAttachment(attachment:any)
    {
        console.log("attachment", attachment);

        this.attachmentEvent.emit(attachment);
    }

    emitInput(value?:any)
    {
        if (value == "")
        {
            value = null;
        }

        if (this.field.onInput)
        {
            this.field.onInput({
                field : this.field,
                data  : value,
            });
        }



        if (this.form.onInput)
        {
            this.form.onInput({
                field : this.field,
                data  : this.form.formGroup.value,
            });
        }


    }

    emitChange(value:any)
    {
        if (value == "")
        {
            value = null;
        }

        if (this.field.onChange)
        {
            this.field.onChange({
                field : this.field,
                data  : value,
            });
        }
    }

    /* DIFERENTE DO CLEAR NÃO VERIFICA O FIXED */
    siblingClear()
    {
        this.updateValue(this.valueDefault());
    }

    reset()
    {
        if (!this.field.fixed)
        {
            this.updateValue(this.valueDefault());
        }

        this.dirty = false;
    }

    /* UTILIZADO NO WIZARD */
    validateSave()
    {
        return true;
    }

    /* UTILIZADO NO WIZARD */
    validate()
    {
        if (this.field.disable || !this.formControl)
        {
            return true;
        }

        return this.formControl.valid;
    }

    onReset()
    {
        this.reset();

        if (this.field.onReset)
        {
            this.field.onReset();
        }
    }

    onSubmit()
    {
        if (this.field.onSubmit)
        {
            this.field.onSubmit();
        }
    }

    onAdd(event:any)
    {
        if (this.field.onAdd)
        {
            this.field.onAdd(event)
        }

        this.addEvent.emit(event);
    }

    onSet(event?:any)
    {
        if (this.field.onSet)
        {
            this.field.onSet({
                data : this.field
            });
        }

        if (this.formItem.onSet)
        {
            this.formItem.onSet({
                data : this.formItem
            });
        }

        this.setEvent.emit(event);
    }

    onEdit(event?:any)
    {
        event;

        if (this.field.onEdit)
        {
            this.field.onEdit({
                data : this.field
            })
        }

        if (this.formItem.onEdit)
        {
            this.formItem.onEdit({
                data : this.formItem
            });
        }

        this.editEvent.emit({
            data : this.field
        });
    }

    onDel(event?:any)
    {
        event;

        if (this.field.onDel)
        {
            this.field.onDel({
                data : this.field
            })
        }

        this.delEvent.emit({
            data : this.field
        });
    }

    onCancel(event?:any)
    {
        event;

        if (this.field.onCancel)
        {
            this.field.onCancel({
                data : this.field
            })
        }

        this.cancelEvent.emit({
            data : this.field
        });
    }

    /*onInit(value)
    {
    if(!this.isInit && value)
    {
    this.emitInit(value);
    this.isInit = true;

    /* EVENT */
    /*if(this.field.onInit)
    {
    this.field.onInit(this.getValue());
    }
    }
    }

    emitInit(value)
    {
    if(this.field.onInit)
    {
    this.field.onInit({
    data  : value,
    field : this.field
    })
    }

    this.initEvent.emit({
    data  : value,
    field : this.field
    });
    }*/

    /* UTLIZADO PARA UPDATE INTERNO */
    onUpdate()
    {
        this.updateEvent.emit({
            data : this.field
        });
    }

    displayFields(names:any, value:any)
    {
        if (names)
        {
            const fields = names.split(",");

            for (const key in fields)
            {
                const field     = fields[key];
                const component = this.form.instances[field];

                if (component)
                {                    
                    const types = this.controlType.split(",")

                    for (const key in types)
                    {
                        component.changeInput = this.changeInput;
                        component[types[key]] = value;
                    }
                }
                else
                {
                    console.error("input not found", field)
                }
            }
        }
    }

    displaySteps(names:any, value:any)
    {
        if (names)
        {
            const stepsNames = names.split(",");

            for (const key in stepsNames)
            {
                const step1 = stepsNames[key];

                for (const key2 in this.steps)
                {
                    const step2 = this.steps[key2];

                    if (step2.id == step1 || step2.name == step1)
                    {
                        step2.display = value;
                        step2.dispatchUpdate();

                        // DISPLAY FILHOS
                        for (const item of step2.items)
                        {
                            item.field.component.instance.display = value;
                        }

                        break;
                    }
                    else
                    {
                        console.error("step not found", step1, step2.id)
                    }
                }				
            }
        }
    }

    get display():boolean
    {
        return this.field && this.field.display;
    }

    set display(value:boolean)
    {
        this.disable       = !value; // FAZ RESET
        this.field.display = value;    
        this.hide          = !value;

        console.log("display", this.formItem.name, value);    

        /* DISABILITA O PARENT */
        if (this.parent)
        {
            this.parent.displayOnlyParent = value;
        } 
    }

    set disable(value:boolean)
    {
        this.field.disable = value;

        if (this.formControl)
        {
            if (value)
            {
                this.updateValidators(false);
                this.reset();
            //this.formControl.disable();								
            }
            else
            {   
                this.updateValidators(true);
                //this.formControl.enable();	

                /* ATUALIZA O VALOR INICIAL OU DEFAULT, NÃO USAR RESET POIS ELE LIMPA A COLLECTION DO SUBFORM */
                this.updateActive(this.initial || this.valueData() || this.valueDefault());				
            }
        }

        /* DISABILITA O PARENT */
        if (this.parent)
        {
            this.parent.disable = value;
        }            

        console.log("disable", this.formItem.name, value, this.initial, this.valueData(), this.valueDefault());    

        this.markForCheck();		
    }

    get disable():boolean
    {
        return this.field && this.field.disable;
    }

    set editable(value:boolean)
    {
        this.formItem.editable = value;  

        /* ATUALIZA O VALOR INICIAL OU DEFAULT, NÃO USAR RESET POIS ELE LIMPA A COLLECTION DO SUBFORM */		
        this.updateActive(this.initial || this.valueData() || this.valueDefault());

        /* DISABILITA O PARENT */
        if (this.parent)
        {
            this.parent.editable = value;
        }            

        console.log("editable", this.formItem.name, value);    

        this.updateValidators(value);

        console.log("editable", value);

        this.markForCheck();
    }

    get editable()
    {
        return this.formItem.editable;  
    }

    set editableReset(value:boolean)
    {
        this.editable = value;

        // SO SE HOUVER ACAO DO USUARIO
        if (this.changeInput)
        {
            this.reset();
        }		
    }

    updateValidators(value:boolean)
    {
        if (this.validators)
        {
            if (value)
            {
                this.formControl.setValidators(this.validators);
                this.formControl.updateValueAndValidity();
            }
            else
            {
                this.formControl.setValidators(null);
                this.formControl.updateValueAndValidity();
            }
        }

        if (this.asyncValidators)
        {
            if (value)
            {
                this.formControl.setAsyncValidators(this.asyncValidators);
                this.formControl.updateValueAndValidity();
            }
            else
            {
                this.formControl.setAsyncValidators(null);
                this.formControl.updateValueAndValidity();
            }
        }
    }

    set updateValueAndValidity(value:boolean)
    {		
        this.formControl.updateValueAndValidity();
    }

    updateActive(data)
    {		
        this.updateValue(data);
    }

    get isControl()
    {
        return this.isControlField || this.isControlStep;
    }

    displayControl(value:any)
    {
        if (this.isControlField)
        {
            this.displayFields(this.controlField, value);					
        }

        if (this.isControlStep)
        {
            this.displaySteps(this.controlStep, value);					
        }
    }

    displayControlByNames(names:any, value:any)
    {
        if (names)
        {
            if (this.isControlField)
            {
                this.displayFields(names, value);					
            }

            if (this.isControlStep)
            {
                this.displaySteps(names, value);					
            }
        }		
    }

    /* METHODOS PARA OVERRIDER */
    displayControlByValue(data:any)
    {
        data;
    }

    set submitted(value:boolean)
    {
        this._submitted = value;
        this.doSubmitted();
        this.markForCheck();		
    }

    get submitted()
    {
        return this._submitted;
    }

    set data(value:any)
    {
        this._data = value;
    }

    get data()
    {
        return this._data;
    }

@Input()
    set updateData(value:any)
    {
        this._updateData = value;
    }

get updateData()
{
    return this._updateData;
}

doSubmitted()
{
// OVERRIDER
}

@Input()
set isLoading(value:boolean)
{
/* GARANTE QUE HOUVE MUDANCA DE STATUS */
    const hasChange = this._isLoading != value;

    this._isLoading = value;

    if (hasChange)
    {
        this.markForCheck();
    }
}

get isLoading()
{
    return this._isLoading;
}

initFields()
{
// OVERRIDER
}

getAcl(name:string, data?:any)
{
    return this.form.component.instance.acl.getKey(name, data);
}

core()
{
    return Core.get()
}

api()
{
    return Core.get().api
}

trackById(index:number, item:any)
{
    return (item && item.id ? item.id : index);
}

async loadOption()
{
// OVERRIDER
}

async loadOptions()
{
// OVERRIDER
}

destroy()
{
    if (this.component)
    {
        this.component.destroy();
    }
}
}
