import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector : `.embed-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    styles   : ['textarea { resize: none; width:100%; }'],
    template : `<div header-input [formItem]="formItem" (set)="onSet($event)" (del)="onDel()"></div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <textarea autosize type="text"
                             [readonly]="form?.isSetModeForm() && !field.editable"
                             [placeholder]="this.field.placeholder"
                             formControlName="{{formItem.name}}"></textarea>
                    <div class="error" *ngIf="submitted">
                        <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                    </div>
                </div>`
})
export class EmbedInput extends BaseInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col embed-input ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }	
	}
}
