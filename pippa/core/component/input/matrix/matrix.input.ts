import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChildren, ViewContainerRef, QueryList } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { MatrixType }    	  from '../../../type/matrix/matrix.type';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { FieldType } 		  from '../../../type/field/field.type';
import { InputFactory } 	  from '../../../factory/input/input.factory';
import { Form } 			  from '../../../model/form/form';
import { Matrix } 			  from '../../../model/matrix/matrix';

@Component({
    selector        : `.matrix-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'has-add' : inputAdd }">
                                
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<ion-searchbar animated 
											   *ngIf="!noSearch"
                                               placeholder="Buscar" 
											   showCancelButton="never"
											   [(ngModel)]="term"
											   [debounce]="1000"
											   [ngModelOptions]="{standalone: true}"
                                               (ionInput)="onSearch($event)">
                                </ion-searchbar>
								
								<ion-grid *ngIf="!d2" class="grid-matrix">
									<ion-row *ngFor="let item of items">
										<ion-col class="variant" size="2" *ngFor="let item2 of item.field.list">
											<label *ngIf="item2[bindLabel]">{{item2[bindLabel]}}<small>{{item2.value}}</small></label>
										</ion-col>
										<ion-col>
											<ng-template #templates></ng-template>
										</ion-col>
									</ion-row>
								</ion-grid>

								<ion-grid *ngIf="d2" class="grid-matrix">
									<ion-row *ngIf="itemsD2.length">
										<ion-col class="variant" size="2">
											<label></label>											
										</ion-col>
										<ion-col class="variant" *ngFor="let item2 of itemsD2[0].cols">
											<label *ngIf="item2[bindLabel]">{{item2[bindLabel]}}</label>											
										</ion-col>										
									</ion-row>
									<ion-row *ngFor="let item of itemsD2">
										<ion-col class="variant" size="2">
											<label *ngIf="item.row[bindLabel]">{{item.row[bindLabel]}}</label>											
										</ion-col>										
										<ion-col *ngFor="let item2 of item.cols">
											<ng-template #templates></ng-template>
										</ion-col>
									</ion-row>
								</ion-grid>
								
								<mat-paginator 
									  [length]="total"
									  [pageSize]="perPage"
									  itemsPerPageLabel=""
									  nextPageLabel="Próxima Página"
									  previousPageLabel="Página Anterior"
									  firstPageLabel="Primeira Página"
									  lastPageLabel="Última Página"
									  getRangeLabel="getRangeLabel"
                                      (page)="onPage($event)">
                       			</mat-paginator>

                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class MatrixInput extends BaseReferenceInput implements OnInit
{
	@ViewChildren('templates', { read: ViewContainerRef }) templates : QueryList<ViewContainerRef>;

	public mask 	  	 : string = '9{10}';
	public term 	  	 : string = '';
	public matrix 	  	 : any;
	public items 	  	 : any;
	public itemsD2    	 : any;
	public fullItems  	 : any;
	public components 	 : any;
	public formGroupM 	 : any;
	public formM 	  	 : any;
	public perPage    	 : any;
	public total 	  	 : any;
	public d2		  	 : boolean = false;
	public noSearch		 : boolean = false;
	public searchTimeout : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

		this.matrix = new Matrix();
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + '-input input col matrix-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';		
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		} 

		if(this.setting.type == 'separate')
		{
			this.matrix.type = MatrixType.SEPARATE;
		}
		else if(this.setting.type == 'fisrt')
		{
			this.matrix.type = MatrixType.FIRST;
		}
		else if(this.setting.type == 'last')
		{
			this.matrix.type = MatrixType.LAST;
		}

		if(this.setting.mode == 'flat')
		{
			this.matrix.mode = 'flat';
		}

		if(this.setting.d2 !== undefined)
		{
			this.d2 = this.setting.d2;
		}

		if(this.setting.noSearch !== undefined)
		{
			this.noSearch = this.setting.noSearch;
		}

		if(this.setting.perPage)
		{
			this.perPage = this.setting.perPage;
		}
		else
		{
			this.perPage = 100;
		}
		
		this.setCollection(null);
	}

    async loadOptions()
    {
		this.setCollection(this.options);
	}

	onSearch(event)
	{
		this.term = event.detail.value;
		this.makePage(0);
	}

	setInputValue(list:any, value:any)
    {
		// PARA EVITAR ATUALIZAR ON MODEL CHANGE
		this.dirty = true;

		this.matrix.setValue(list, value);
        this.setValue(this.matrix.getData());			
	}

	getInputValue(list:any)
	{
		return this.matrix.getValue(list);
	}

	createComponent()
	{		
		setTimeout(async () => 
		{
			this.clear();

			this.formGroupM = new FormGroup({});
			this.formM      = new Form();
			this.components = [];
			const templates = this.templates.toArray();
			const promises  = [];
			
			for(const key in templates)
			{
				if(this.items[key] && templates[key])
				{
					promises.push(InputFactory.createComponent(this.items[key], templates[key], this.formGroupM, 
					{
						form : this.formM
					}));

					// const inputComponent = await InputFactory.createComponent(this.items[key], templates[key], this.formGroupM, 
					// {
					// 	form : this.formM
					// });

					// inputComponent.instance.populate(value);

					// this.components.push(inputComponent);	
				}
			}

			const inputs = await Promise.all(promises);

			for(const key in templates)
			{
				if(this.items[key] && templates[key])
				{
					const value 		 = this.getInputValue(this.items[key].field.list);
					const inputComponent = inputs[key];

					inputComponent.instance.populate(value);

					this.components.push(inputComponent);	
				}
			}

			this.markForCheck();

			/*if(this.hasBug)
			{
				this.setValue(this.matrix.getData());

				console.error('---', this.matrix.getData());	
			}*/
		});
	}

	async updateValues()
	{
		if(this.components && this.components.length > 0)
		{
			for(const key in this.components)
			{
				if(this.items[key])
				{
					const value = this.getInputValue(this.items[key].field.list);
					this.components[key].instance.populate(value);
				}				
			}
		}
	}
	
	valueDefault():any
    {
        return null;
	}

	reset()
    {
        if(!this.field.fixed)
        {
            this.updateValue(null);					
		}

		super.reset();
    }

	async setCollection(data:any)
	{
		// UPDATE MATRIX
		this.matrix.variant = data;
		
		// BUILD ITEMS
		let items = new FormItemCollection();	

		for(const list of this.matrix.items)
		{
			//const path = this.matrix.getPath(list);

			const formItem : any = {
				field : {
					name 	: '_' + this.core().util.randomString(20),
					list 	: list,
					type	: FieldType.type('DataForm'),
					setting : {
						openModal : false,
					},
					onInput : (event:any) =>
					{
						this.setInputValue(event.field.list, event.data);
					}
				},		
				items : this.formItem.items.copy()
			};

			// ITEMS
			items.add(formItem);
		}

		this.fullItems = items;
		this.total 	   = items.length;

		this.makePage(0);
	}

	transformD2()
	{
		const d2    = [];
		let current : any = { id : ''};

		for(const item of this.items)
		{
			if(current.id !== item.field.list[0].id)
			{
				current = {
					id    : item.field.list[0].id,
					row   : item.field.list[0],
					cols  : [],
				}

				d2.push(current);
			}

			if(item.field.list[1])
			{
				current.cols.push(item.field.list[1]);
			}			
		}		

		this.itemsD2 = d2;
	}

	makePage(page:any)
	{
		const items   = [];
		let fullItems = [];

		if(this.term)
		{
			fullItems = this.fullItems.filter((item) => 
			{
				if(item.field.list)
				{
					let label = '';

					for(const item2 of item.field.list)
					{
						label += item2.label.toLocaleLowerCase() + ' ';
					}

					if(label.includes(this.term.toLocaleLowerCase()))
					{
						return item;
					}
				}
			});	
		}
		else
		{
			fullItems = this.fullItems;
		}	

		for(let i = this.perPage * (page); i < this.perPage * (page + 1); i++)
		{
			if(fullItems.length > i)
			{
				items.push(fullItems[i]);
			}			
		}

		this.items = items;
		this.total = fullItems.length;

		if(this.d2)
		{
			this.transformD2();
		}		

		this.createComponent();
		this.markForCheck();
	}

	onPage(event:any)
    {        
        console.log('onPage', event);
        this.makePage(event.pageIndex);
    }

	getRangeLabel(page, pageSize, length) 
	{
		if (length === 0 || pageSize === 0) 
		{
		  	return '0 de ' + length;
		}

		length = Math.max(length, 0);
		const startIndex = page * pageSize;
		const endIndex = startIndex < length ?
		Math.min(startIndex + pageSize, length) :
		startIndex + pageSize;
		return startIndex + 1 + ' - ' + endIndex + ' de ' + length;
	};

	updateValue(data:any)
	{		
		this._updateValue(data);
	}

	clear()
    {
        for(const key in this.components)
        {
            this.components[key].destroy();
        }

		const templates = this.templates.toArray();
	
		for(const template of templates)
		{
			template.clear();
		}
    }

	///public hasBug = false;

    async onModelChange(data:any)
    {
		if(data && !this.dirty)
		{
			// BUG
			/*if(!(data.variant instanceof Array))
			{
				const _data = {};

				for(const key in data.data)
				{
					_data[key] = {
						images : data.data[key]
					}
				}
	
				data.data 	 = _data;
				data.variant = [{ items : [data.variant]}];	
				this.hasBug  = true; 
			}*/

			this.matrix.populate(data); 
			this.dirty  = true;

			await this.updateValues();
		} 
    }
}
