import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { InputValidate } from '../../../validate/input.validate';

@Component({
    selector 		: `.year-input`,
	changeDetection : ChangeDetectionStrategy.OnPush,
	template        : `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="formGroup">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel" 
									   [placeholder]="this.field.placeholder"
									   [mask]="mask"
									   [ngModel]="value"									
									   [ngModelOptions]="{standalone: true}"
									   (keyup)="onInput($event)"/>

								<error-input [control]="formControl" [submitted]="submitted">
									<span *ngIf="hasError('date')">Data Invalida!</span>
								</error-input>

							</div>
						</div>`
})
export class YearInput extends BaseTextInput
{
	public mask 	 : string = '0000';
	public inputMask : string = 'yyyy'; // DEPOIS FAZER VALIDADOR

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col year-field ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
	}

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
		}
		
		this.validators.push(InputValidate.year);
	}
}
