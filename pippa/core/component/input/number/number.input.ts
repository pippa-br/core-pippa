import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';

@Component({
    selector		: `.number-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"
									autocomplete="false"
									[ngModel]="value"
									[ngModelOptions]="{standalone: true}"
									[readonly]="!editable"
									(keyup)="onInput($event)"
									currencyMask
									[min]="minimum"
									[max]="maximum"
									[options]="{ prefix : '', thousands : '.', decimal : ',', precision : precision, align : 'left' }"
									[placeholder]="field.placeholder"/>

							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('min')">Valor menor que {{minimum}} o permitido</span>
								<span *ngIf="hasError('max')">Valor maior que {{maximum}} o permitido!</span>
							</error-input>
						</div>`
})
export class NumberInput extends BaseTextInput
{
    public minimum   : number;
	public maximum   : number;
	public precision : number = 2;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col number-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.editable)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
	
			if(this.setting.minimum)
			{
				this.minimum = this.setting.minimum;
				this.validators.push(Validators.min(this.minimum));
			}
	
			if(this.setting.maximum)
			{
				this.maximum = this.setting.maximum;
				this.validators.push(Validators.max(this.maximum));
			}
		}
		
		if(this.setting.precision)
        {
            this.precision = this.setting.precision;
        }	
	}

    onInput(event:any)
    {
        if(event.srcElement)
        {
            const value = parseFloat(event.srcElement.value.replace('.', '').replace(',','.'));

            if(isNaN(value))
            {
                this.setValue(this.valueDefault());
            }
            else
            {
                this.setValue(value);
            }
        }
        else
        {
            this.setValue(this.valueDefault());
        }
    }

    valueDefault():any
    {
        return 0;
    }
}
