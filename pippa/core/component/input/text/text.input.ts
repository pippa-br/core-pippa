import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { InputValidate } from "../../../validate/input.validate";

@Component({
    selector        : ".text-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit()"
                                (del)="onDel()">
                            </div>
							
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="text"
                                    autocomplete="false"
                                    [ngModel]="value"
                                    [maxlength]="max"
                                    [ngModelOptions]="{standalone: true}"
                                    [readonly]="!editable || readonly"
                                    (keyup)="onInput($event)"
                                    [placeholder]="field.placeholder | translate"/>

                                <icon-input [isLoading]="isLoading"
                                            [icon]="field.getIcon()">
                                </icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class TextInput extends BaseTextInput
{
    public max  = 10000;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col text-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        } 

        if (this.setting.maxlength)
        {
            this.max = this.setting.maxlength;
        }

        this.asyncValidators = [ InputValidate.search(this.field, this.form, this) ];

        super.createValidators()
    }
}
