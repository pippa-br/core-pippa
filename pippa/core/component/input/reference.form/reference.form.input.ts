import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { Form }               from '../../../../core/model/form/form';
import { FormPlugin }         from '../../../../core/plugin/form/form.plugin';

@Component({
    selector 		: `.reference-form-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
								<ng-select [(ngModel)]="selectedId"
										[ngModelOptions]="{standalone: true}"
										[disabled]="form?.isSetModeForm() && !field.editable"
										[items]="items"
										appendTo="body"
										[loading]="loading"
										[virtualScroll]="virtualScroll"
										[placeholder]="field.label"
										[bindLabel]="bindLabel"
										[bindValue]="bindValue"
										cancelText="Fechar"
										okText="Selecionar"
										(close)="onClose()"
										(scrollToEnd)="onScrollToEnd()"
										(search)="onSearch($event)"
										(change)="onInput($event)">
								</ng-select>
								<icon-input [isLoading]="isLoading"></icon-input>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
							</error-input>`
})
export class ReferenceFormInput extends BaseReferenceInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col question-reference-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
		super.createValidators();
		
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    getPlugin()
    {
        return this.core().injector.get(FormPlugin);
    }

    onModelChange(data:any)
    {
        if(data && !this.dirty)
        {
            const model : any = new Form(data)

            model.on(false).then(() =>
            {
                this.selected   = model;
                this.selectedId = model.id;
            });
        }
    }
}
