import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";

@Component({
    selector        : ".checkbox-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper ng-valid" [ngClass]="{'ng-submitted' : submitted}">
                            <div header-input
                                [onlyButtons]="true"
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input id="{{labelId}}"
                                       class="checkbox-item"
                                       type="checkbox"
                                       formControlName="{{formItem.name}}"
                                       [(ngModel)]="selected"
                                       (ngModelChange)="onModelChange($event)"
                                       (change)="onInput()">

                                <label for="{{labelId}}">{{field.label}}</label>

                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
						</div>
						<attachment-input *ngIf="field.hasAttachments()" [field]="field" [formGroup]="formGroup" 
                                    (attachment)="onAttachment($event)">
                        </attachment-input>`
})
export class CheckboxInput extends BaseInput
{
    public labelId  : string;
    public selected  = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);  

        this.nameClass += this.formItem.name + "-input input col checkbox-input " + (this.field.viewEdit ? "view-edit " : " ");
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        this.labelId = this.formItem.id + "-" + new Date().getTime();
    }
	
    onModelChange(value:any)
    {
        if (value !== undefined && !this.dirty)
        {            
            this.selected = value;
            this.dirty    = true;
            this.displayControlByValue(this.selected);
        }        
    }

    onInput()
    {        
        // usa ! pois o evento change demora para atualizar
        this.displayControlByValue(!this.getValue());
    }

    displayControlByValue(value:any)
    {
        if (this.isControlField)
        {
            this.displayFields(this.controlField, false); /* DESABILITA TODOS */

            if (value && this.field.setting && this.field.setting[1] != undefined) /* STATUS 1 */
            {
                const fields = this.field.setting[1];
                this.displayFields(fields, true); /* HABILITA O ATUAL */
            }
            else if (!value && this.field.setting && this.field.setting[0] != undefined) /* STATUS 0 */
            {
                const fields = this.field.setting[0];
                this.displayFields(fields, true); /* HABILITA O ATUAL */
            }
            else
            {
                this.displayFields(this.controlField, value); /* HABILITA O ATUAL */
            }
        }
    }

    valueDefault()
    {
        return false;
    }
}
