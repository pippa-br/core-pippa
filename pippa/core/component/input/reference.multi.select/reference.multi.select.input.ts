import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { BaseModel }          from "../../../model/base.model";
import { BaseList }           from "../../../model/base.list";
import { Grid } from "../../../model/grid/grid";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";
import { ModalController } from "@ionic/angular";
import { ViewerModal } from "../../../modal/viewer/viewer.modal";
import { Viewer } from "../../../model/viewer/viewer";

@Component({
    selector        : ".reference-multi-select-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
									'ng-valid' 	: formControl?.valid && hasValue(), 
                                    'ng-invalid'   : !formControl?.valid,
									'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit()"
                                (del)="onDel()">
                            </div>
                            <div class="item-input"
                                [formGroup]="field.formGroup"
                                [ngClass]="{'has-add' : inputAdd }">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ng-select [(ngModel)]="selectedId"
                                        *ngIf="!hiddenSelect" 
                                        [ngModelOptions]="{standalone: true}"
                                        [disabled]="!editable"
                                        [items]="items"
										[loading]="loading"
										appendTo="body"
                                        [multiple]="true"
                                        [hideSelected]="true"
                                        [closeOnSelect]="closeOnSelect"
                                        notFoundText="Item não encontrado"                                        
                                        [placeholder]="formItem.label"
                                        [bindLabel]="bindLabel"
                                        [bindValue]="bindValue"
                                        [groupBy]="groupBy"
                                        [selectableGroup]="true"
                                        cancelText="Fechar"
										okText="Selecionar"
										(close)="onClose()"
										(search)="onSearch($event)"
                                        (change)="onInput($event)">										

                                    <ng-template ng-label-tmp let-item="item" let-clear="clear">
                                        <span class="ng-value-icon left" (click)="clear(item)" aria-hidden="true">×</span>
                                        <img (click)="addDocument(item)" *ngIf="hasPhoto && item.photo"  height="28" width="28" [src]="item.photo._url"/>
                                        <img (click)="addDocument(item)" *ngIf="hasPhoto && !item.photo" height="28" width="28" src="/assets/img/avatar.png"/>
                                        <span (click)="addDocument(item)">{{item[bindLabel]}}</span>
                                    </ng-template>

                                    <ng-template ng-option-tmp let-item="item" let-index="index" let-search="searchTerm">
										<div class="title-option">
                                            <img *ngIf="hasPhoto && item.photo"  height="30" width="30" [src]="item.photo._url"/>
                                            <img *ngIf="hasPhoto && !item.photo" height="30" width="30" src="/assets/img/avatar.png"/>
                                            <span>{{item[bindLabel]}}</span>
										</div>
                                    </ng-template>

                                </ng-select>                                
                                <icon-input *ngIf="inputAdd && editable"
                                            [isLoading]="isLoading"
											class="add-icon"
                                            [icon]="addIcon"
                                            (click)="addDocument()">
                                </icon-input>
                            </div>
                            <p class="sort-loading" *ngIf="selectedSort && isLoading">Carregando...</p>
                            <div class="sort-container" cdkDropListGroup *ngIf="selectedSort">                                
                                <div
                                    *ngFor="let item of itemsSelected; let i = index"
                                    cdkDropList
                                    cdkDropListOrientation="horizontal"
                                   [cdkDropListData]="{item:item,index:i}" (cdkDropListDropped)="drop($event)">
                                    <div cdkDrag class="sort-box">
                                        <img [src]="item.indexes?.thumb?._150x150"/>
                                    </div>
                                </div>
                            </div>
                            <div dynamic-list
                                    #dynamicList  										 							                              
                                    *ngIf="subGrid"
                                    [grid]="subGrid"
                                    [acl]="form.instance.acl"
                                    [fixedHeader]="false"
                                    [collection]="selectedCollection"
                                    (viewer)="onViewer($event)"
                                    (collectionChange)="onCollectionChange();">
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class ReferenceMultiSelectInput extends BaseReferenceInput
{
    public closeOnSelect      = false;	
    public subGrid            : any;
    public selectedCollection : any;
    public popover            : any;
    public hiddenSelect       = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
        public modalController   : ModalController,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col reference-multi-select-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";        
    }

    createValidators()
    {
        super.createValidators();
		
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        } 

        /* CLOSE ON SELECT */
        if (this.setting.closeOnSelect)
        {
            this.closeOnSelect = this.setting.closeOnSelect;
        }
        
        if (this.setting.hiddenSelect)
        {
            this.hiddenSelect = this.setting.hiddenSelect;
        }

        /* GRID */
        if (this.setting.gridPath)
        {
            const paths = this.setting.gridPath.split("/");

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid       : paths[0],
                appid       : paths[1],
                colid       : paths[2],
                path        : this.setting.gridPath,
                model       : Grid,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                },
                map : true,
            })

            this.core().api.getObject(params).then((result:any) =>
            {
                this.subGrid = result.data;
                // this.subGrid.hasAddButton	 = false;
                // this.subGrid.fixedHeader     = false;
                // this.subGrid.hasSetButton    = false;
                // this.subGrid.hasDelButton    = false;
                this.subGrid.hasViewerButton = true;
                this.subGrid.disabledButtons = false
                
                this.markForCheck();
            });
        }        
    }

    async mergeSelected()
    {
        if (this.selected && this.collection)
        {
            /* VERIFICA SE JA EXISTE */
            this.selectedCollection = new BaseList([]);

            for (const item of this.selected)
            {
                const model = new BaseModel(item);
                await model.on();

                this.collection._set(model);
                this.selectedCollection.set(model);
            }
        }
    }

    onCollectionChange()
    {
        this.markForCheck();
    }

    /* METODO USADO PARA CHAMADAS EXTERNAS */
    populate(list:any)
    {
        return new Promise<void>(async (resolve) =>
        {
            if (list == null)
            {
                this.selected   = null;
                this.selectedId = null;
            }
            else
            {
                const ids = [];
    
                for (const key in list)
                {
                    ids.push(list[key][this.bindValue]);
                }
    
                this.selectedId = ids;
                this.selected   = list;
            }
    
            // UPDATE VALUES
            if (list == undefined)
            {
                this.updateValue([]);
            }
            else
            {
                const references = [];

                for (const key in list)
                {
                    const item = list[key];
                    references.push(item.reference);
                }

                this.updateValue(references);								
            }

            resolve();
        });           
    }

    async onInput(items:any)
    {
        this.changeInput = true;
        
        if (items == undefined)
        {
            this.setValue([]);
        }
        else
        { 
            this.setValue(items);
        }

        if (this.subGrid && this.selected && this.collection)
        {
            /* VERIFICA SE JA EXISTE */
            this.selectedCollection = new BaseList([]);

            for (const item of this.selected)
            {
                const model = new BaseModel(item);
                await model.on();

                this.selectedCollection.set(model);
            }
        }        
    }

    async onViewer(event:any)
    {
        this.popover = await this.modalController.create(
            {
                component : ViewerModal,
                componentProps :
                {
                    viewer   : new Viewer({ items : this.formItem.items }),
                    document : event.data,				
                    onClose  : (() =>
                    {
                        this.popover.dismiss();
                    }),
                },
                cssClass : "full",
            });

        this.popover.present();
    }

    /* OVERRIDE */
    async doAdd(data:any)
    {
        if (!this.selected)
        {
            this.selected = [];
        }

        this.selected.push(data);
        await this.doLabels();

        this.onInput(this.selected);
    }

    /* OVERRIDE */
    async doSet(data:any)
    {
        if (!this.selected)
        {
            this.selected = [];
        }

        this.selected.push(data);
        await this.doLabels();

        this.onInput(this.selected);
    }

    async onModelChange(items:any)
    {
        /* TRATAR O VALOR DO BIND */
        if (items && Object.keys(items).length > 0 && !this.dirty)
        {
            const ids    = [];
            const _items = []; // POR CONTA DE TYPEOBJECT SER SALVO COMO OBJECT			

            for (const key in items)
            {
                if (key !== "_dataReferences")
                {
                    ids.push(items[key][this.bindValue]);
                    _items.push(items[key]);	
                }
            }

            this.selectedId = ids;
            this.selected   = _items; // SEMPRE SÃO REFERENCIAS

            /* INVERSED BY */
            this.updateInversedBy(items, "inversedBy");

            /* INIT INVERSED BY */
            this.updateInversedBy(items, "initInversedBy");

            await this.doLabels();

            this.dirty = true;
        }
    }

    valueDefault()
    {
        return [];
    }
}
