import { AfterViewInit, ChangeDetectorRef, Input, Directive } from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { FormControl } from '@angular/forms';
import * as _ from "lodash" 

/* PIPPA */
import { BaseInput }          from './base.input';
import { StepFormPopover }    from '../../popover/step.form/step.form.popover';
import { FormItemCollection } from '../../model/form.item/form.item.collection';

@Directive()
export abstract class BaseStepInput extends BaseInput implements AfterViewInit
{
    public alertController   : AlertController;
    public defaultItems      : any;
    public modelItems        : any;
    public formItems         : any;
    public items             : any;
    public stepsItems        : any;
    public allSteps          : any;
    public dragulaStep       : string;
    public dragulaRow        : string;
    public dragulaCol        : string;
    public popoverController : any;
	public editFormItem      : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        const maps = this.getMaps();

        this.modelItems = new maps.list.klass();
        this.formItems  = new maps.list.klass();
		this.items      = new maps.list.klass();

        this.nameClass += this.formItem.name + '-input input col base-step-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        this.allSteps = new maps.stepCollection.klass();

        /* SETTINGS ITEMS */
        if(this.field.setting.items)
        {
            this.items = this.field.setting.items;
        }

        /* SETTINGS EDIT FORMITEM */
        if(this.field.setting.editFormItem)
        {
            this.editFormItem = this.field.setting.editFormItem;
        }
	}

	getAllColIds()
	{
		const ids : any = [];

		for(const key in this.stepsItems)
		{
			for(const key2 in this.stepsItems[key].matrix)
			{
				for(const key3 in this.stepsItems[key].matrix[key2])
				{
					ids.push(this.stepsItems[key].matrix[key2][key3].id);
				}	
			}
		}

		return ids;
	}

	getAllStepIds()
	{
		const ids : any = [];

		for(const key in this.stepsItems)
		{
			ids.push(this.stepsItems.id);
		}

		return ids;
	}

	getRowIds()
	{
		let ids : any = [];

		for(const key in this.stepsItems)
		{
			for(const key2 in this.stepsItems[key].matrix)
			{
				ids.push(this.stepsItems[key].matrix[key2].id);
			}
		}

		return ids;
	}

	getColIds()
	{
		const ids : any = ['itemsList'];

		for(const key in this.stepsItems)
		{			
			for(const key2 in this.stepsItems[key].matrix)
			{
				for(const key3 in this.stepsItems[key].matrix[key2])
				{					
					ids.push(this.stepsItems[key].matrix[key2][key3].id);
				}	
			}
		}

		/* FAZER AQUI POR CONTA DOS NOVOS STEPS */
		for(const key in this.allSteps)
		{
			ids.push('newList' + this.allSteps[key].id);
		}

		return ids;
	}

	stepPredicate(item: any) 
	{
		return false;
	}

	async addRow(step, event)
	{
		const data : any = event.previousContainer.data[event.previousIndex];	
		data.step = step;
			
		event.previousContainer.data.delItem(data);

		step.matrix.add(new FormItemCollection());
		step.matrix[step.matrix.length - 1].add(new FormItemCollection());
		step.matrix[step.matrix.length - 1][0].setItem(data);	
		
		await this.update();
	}

	async onColDrop(step, event) 
	{
		/* MOVE LINE */
		if(event.previousContainer === event.container) 
		{
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

			console.log('move line', event.container.data);

			/* REPOSICIONA AS COL */
			for(const key in event.container.data)
			{
				for(const key2 in event.container.data[key])
				{
					//event.container.data[key][key2][0] 
				}
			}
		} 
		else /* MOVE COL */
		{			
			console.log('move col', event.container.data);

			const colFrom : any = event.previousContainer.data;
			const colTo   : any = event.container.data;
			const data    : any = event.previousContainer.data[event.previousIndex];
			data.step = step;

			colFrom.delItem(data);	
			colTo.setItem(data);
			
			/* REPOSICIONA AS COL */
			for(const key in event.container.data)
			{
				event.container.data[key].col = key;
			}

			for(const key in event.previousContainer.data)
			{
				event.previousContainer.data[key].col = key;
			}
		}
		
		await this.update();
	}

	async onStepDrop(step, event) 
	{
		if(event.previousContainer === event.container) 
		{
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} 
		else 
		{			
			
		}
		
		await this.update();
	}

    async update()
    {	
		await this.stepsItems.normalizeMatrix();
		await this.stepsItems.normalizeMatrix(); /* POIS TEM UM BUG QUE NÃO REMOVE A ROW */

		const updates : any    = [];
        let i         : number = 0;
        let j         : number = 0;
		let k         : number = 0;

        for(const key in this.stepsItems)
        {
            const step = this.stepsItems[key];
			step.stepOrder = i;
			j = 0;

            for(const key2 in step.matrix)
            {
				const rows = step.matrix[key2];
				k = 0; 

                for(const key3 in rows)
                {
					const col = rows[key3]; 										     

					for(const key4 in col)
                	{
						const data = col[key4];

						/* A POSIÇÃO DEVER SER ATUALIZADA NO UPDATE POR CAUSA DOS STEPS */
						data.row = j;
						data.col = k;						
	
						updates.push(data.parseData());													

						k++;
					}					
				}
				
				j++
            }

            i++
		}
		
		this.setValue(updates);
		this.stepsItems.doSort();
		
		console.log('update', updates);		
    }

    async onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(value && value.length > 0 && !this.dirty)
        {
            this.dirty  = true;
            const maps  = this.getMaps();
            const items = new maps.list.klass(_.merge({}, value)); /* POR CAUSA DO BIND COM O FORM */

            //for(const key in items)
            //{
                //items[key].id = items[key].field.id;
            //}

            this.modelItems.setItems(items);

            this.allSteps = this.modelItems.getSteps();                

            for(const key in this.modelItems)
            {
                const item = this.modelItems[key];

                if(item.default)
                {
                    this.formItems.set(item);
                }
            }

            await this.displaySteps();
        };
    }

    async displaySteps()
    {
        if(this.defaultItems && this.formItems && this.allSteps)
        {
            const maps       = this.getMaps();
            const totalItems = new maps.list.klass();
            const modelItems = new maps.list.klass();

            totalItems.setItems(this.defaultItems);
            totalItems.setItems(this.formItems);

            this.stepsItems = new maps.stepCollection.klass();

            for(const key in this.allSteps)
            {
                const step = this.allSteps[key];
                this.stepsItems.set(step.copy());
			}
			
            if(totalItems.length > 0)
            {
                this.items = new maps.list.klass();

                for(const key in totalItems)
                {
                    const itemField = totalItems[key];

                    const item = this.modelItems.get(itemField);
                    let exists = false;

                    if(item)
                    {                                            
                        if(item.field)
                        {
                            /* SE TEVE AUTERACAO NO FIELD A VERSION É ALTERADO E O ITEM NORMALIZADO */
                            if(item.version != itemField.version)
                            {
								item.normalize();
								
								item.populate({
									field : itemField.field
								});                                                        
                            }                                                        
                        }

                        item.items = itemField.items;

                        const stepItem = item.step;

                        for(const key in this.stepsItems)
                        {
                            let step = this.stepsItems[key];

                            if(stepItem && stepItem.id == step.id)
                            {
                                if(itemField.enabled)
                                {
                                    step.items.set(item);
                                    item.step = step; /* ATUALIZA O STEP ALTERADO */
                                    exists = true;
                                }
                                /*else
                                {
                                    step.items.del(item);
                                }*/

                                break;
                            }
                        }

                        if(!exists)
                        {
                            if(itemField.enabled)
                            {
                                this.items.set(item);
                            }
                        }
                        else
                        {
                            modelItems.set(item);
                        }
                    }
                    else
                    {
                        if(itemField.enabled)
                        {
                            this.items.set(itemField);
                        }
                    }
                }

                this.modelItems = modelItems;

                await this.stepsItems.doMatrix();
                this.stepsItems.doSort();

                await this.update();
			}
			else
			{
				// DEPOIS REVER POIS QUANDO REMOVI TODOS OS ITEMS NÃO ESTA LIMPANDO OS STEPES
			}                        
        }
    }

    getStep(step:any)
    {
        for(const key in this.stepsItems)
        {
            if(this.stepsItems[key].id == step)
            {
                return this.stepsItems[key];
            }
        }
    }

    /* BIND COM FIELDS EXTERNOS */
    getFormItems()
    {
        return new Promise<void>(resolve =>
        {
            const maps      = this.getMaps();
            const formItems = new maps.list.klass();

            if(this.field.setting.fields)
            {
                const names     = this.field.setting.fields.split(',');
                let count       = 0;
                let size        = names.length;

                for(const key in names)
                {
                    const name  = names[key];
                    const field = this.form.instances[name].field;
                    const items = this.form.instances[name].items;

                    /* CHANGE CHECKBOX */
                    field.onChange = (() =>
                    {                        
                        this.getFormItems().then(async () =>
                        {
                            await this.displaySteps();
                        });
                    });

                    for(const key2 in items)
                    {
                        const item = items[key2];
                        formItems.set(item);
					}
					
                    count++;

                    if(count == size)
                    {
						this.formItems.setItems(formItems);																

                        resolve();
                    }
                }
            }
            else if(this.field.setting.formItems)
            {
                this.formItems.setItems(this.field.setting.formItems);

                resolve();
            }
            else
            {
                resolve();
            }
        });
    }

    getDefaultItems()
    {
        return new Promise<void>(resolve =>
        {
            const maps         = this.getMaps();
            const defaultItems = new maps.list.klass();

            /* CAMPOS EXTRAS */
            if(this.field.setting.defaultItems && this.field.setting.defaultItems.length > 0)
            {
                for(const key in this.field.setting.defaultItems)
				{
					const item = this.field.setting.defaultItems[key];
					//item.id    = item.field.id; /* O ID DO ITEM DEIXA COM O ID DO FILED */
					defaultItems.set(item);
				}

				this.defaultItems = defaultItems;

				resolve();
            }
            else
            {
                this.defaultItems = defaultItems;

                resolve();
            }
        });
    }

    /* STEP */
    openStep(item?:any)
    {
        this.popoverController.create(
        {
            component      : StepFormPopover,
            componentProps :
            {
                data  : item,
                field : this.field,
                acl   : this.form.instance.acl,
                onAdd : (async (event:any) =>
                {
					/* REMOVE LOGS */
					delete event.data._logs;

                    const maps  = this.getMaps();
                    const step  = new maps.step.klass(event.data);
                    await step.doMatrix();

                    this.allSteps.set(step);

                    await this.displaySteps();

                    this.popoverController.dismiss();
                }),
                onSet : (async (event:any) =>
                {
					/* REMOVE LOGS */
					delete event.data._logs;

                    item.populate(event.data)
                    this.allSteps.set(item);

                    await this.displaySteps();

                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass : 'right-popover',
        })
        .then((popover:any) =>
        {
            popover.present();
        });
    }

    onDelStep(step:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text: 'Fechar',
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text: 'Remover',
                    handler : async () =>
                    {
                        this.allSteps.del(step);
                        this.displaySteps();
                        return true;
                    }
                }
            ]
        }).then((alert:any) =>
        {
            alert.present();
        });
    }

    /* FORM ITEM */
    addFormItem()
    {
        this.openFormItem(null, this.getOption())
    }

    setFormItem(item:any)
    {
        this.openFormItem(item, this.getOption())
    }
    
    getOption()
    {
        return {
            items : []
        };
    }

    getFormPopover()
    {        
    }

    getMaps():any
    {
        return {}
    }

    async delFormItem(item:any)
    {
        this.modelItems.del(item);
        this.formItems.del(item);
    
        await this.displaySteps();
    }

    openFormItem(item?:any, option?:any)
    {
        this.popoverController.create(
        {
            component      : this.getFormPopover(),
            componentProps :
            {
                data   : item,
                acl    : this.form.instance.acl,
                option : option,
                onSet  : (async (event:any) => /* SEMPRE É SET */
                {
					/* REMOVE LOGS */
					delete event.data._logs;
					                    
                    if(!item) // NEW
                    {
                        event.data.default = true;

                        this.items.set(event.data);
                        this.formItems.set(event.data);
                    }
                    else
                    {	
						item.populate(event.data);
						await item.on();

						//this.items.set(item);
						//this.formItems.set(item);
                    }

                    await this.update();

                    this.popoverController.dismiss();                    
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass     : 'right-popover',
        })
        .then((popover:any) =>
        {
            popover.present();
        });
    }

    ngOnInit()
    {
        this.getDefaultItems().then(() =>
        {
            this.getFormItems().then(async () =>
            {
                await this.displaySteps();
            });
        });
    }
}
