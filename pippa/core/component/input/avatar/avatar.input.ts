import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular'
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';

/* PIPPA */
import { BaseInput }    from '../base.input';
import { File }         from '../../../../core/model/file/file';
import { UploadPlugin } from '../../../../core/plugin/upload/upload.plugin';
import { AvatarModal }  from '../../../../core/modal/avatar/avatar.modal';

@Component({
    selector        : `[avatar-input]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template        :  `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 	 : formControl?.valid && this.hasValue(), 
							'ng-invalid' : !formControl?.valid,
							'hidden'     : hidden }">

							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onChange($event)"/>
								<input #fileInput type="file" (change)="onUpload($event)" style="display: none;"/>

								<div class="img-wrapper" *ngIf="field.editable" [ngClass]="{'has-photo':file}">
									<a (click)="onAdd()">
										<img *ngIf="!file" src="./assets/img/avatar-edit.png" ion-button clear/>
										<img *ngIf="file" [src]="file.url" ion-button clear/>										
									</a>
								</div>

								<div class="img-wrapper" *ngIf="!field.editable">
									<img *ngIf="!file" src="./assets/img/avatar.png" ion-button clear/>
								</div>

							</div>

							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="invalidUrl">Url Invalida!</span>
							</error-input>

						</div>`
})
export class AvatarInput extends BaseInput
{
    @ViewChild('fileInput', {static: false}) fileInput : ElementRef;

    file          : File;
    url           : string  = '';
    uploadPercent : number  = 0; /* DEPOIS JOGAR PARA UM OVERLAY */
    invalidUrl    : boolean = false;
	aspectRatio   : number  = 1;

    constructor(
        public uploadPlugin          : UploadPlugin,
        public modalController       : ModalController,
        public changeDetectorRef     : ChangeDetectorRef,
        public actionSheetController : ActionSheetController,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col avatar-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(!this.hidden && this.formItem.required)
        {
            this.validators.push(Validators.required);
        }	
		
		if(this.setting.aspectRatio)
		{
			this.aspectRatio = this.setting.aspectRatio;
		}
	}

    onAdd()
    {
        if(!this.core().isMobile())
        {
            this.fileInput.nativeElement.click();
        }        
        else
        {
            this.actionSheetController.create(
            {
                header  : 'Imagem',
                buttons : [
                {
                    text    : 'Camera',
                    icon    : 'camera',
                    handler : () => 
                    {
                        Camera.getPhoto({
                            quality      : 90,
                            allowEditing : true,
                            resultType   : CameraResultType.DataUrl,
                            source       : CameraSource.Camera,
                        })
                        .then((image:any) => 
                        {
                            /* UPLOAD */
                            this.modalController.create(
                            {
                                component       : AvatarModal,
                                componentProps  : {
                                    imageBase64 : image.dataUrl,
									aspectRatio : this.aspectRatio,
                                    onSave      : ((event2:any) =>
                                    {
                                        /* UPLOAD */
                                        this.uploadPlugin.uploadFromString(event2.base64).then(result =>
                                        {
                                            this.setFile(result.data);
											this.reset();
											
											this.modalController.dismiss();
                                        });
                                    }),
                                    onClose : () =>
                                    {
                                        /* PARA PERMITIR SELECIONAR O MESMO ARQUIVO 2 VEZES */
                                        this.reset();
                    
                                        this.modalController.dismiss();
                                    }
                                }
                            }).then((modal:any) =>
                            {
                                modal.present();
                            });
                        })
                    }
                }, 
                {
                    text    : 'Biblioteca',
                    icon    : 'images',
                    handler : () => 
                    {
                        this.fileInput.nativeElement.click();
                    }
                }]
            })
            .then((actionSheet:any) => 
            {
                actionSheet.present();
            })
        }
    }

    onUpload(event:any)
    {
        this.modalController.create(
        {
            component       : AvatarModal,
            componentProps  : {
                imageEvent  : event,
				aspectRatio : this.aspectRatio,
                onSave      : ((event2:any) =>
                {
                    this.uploadPlugin.uploadFromString(event2.base64).then(result =>
                    {
                        this.setFile(result.data);
                        this.reset();

                        this.modalController.dismiss();
                    });
                }),
                onClose : () =>
                {
                    /* PARA PERMITIR SELECIONAR O MESMO ARQUIVO 2 VEZES */
                    this.reset();

                    this.modalController.dismiss();
                }
            }
        }).then((modal:any) =>
        {
            modal.present();
        });
    }

    reset()
    {
        this.fileInput.nativeElement.value = "";

        this.dirty = false;
    }

    setFile(file:any)
    {
        this.file = new File(file);
        this.setValue(file.parseData());
        this.invalidUrl = false;
        this.url = file.url;
    }

    onChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(value && !this.dirty)
        {
            this.file = new File(value);
            this.url  = this.file.url;

            this.dirty = true;
        }
    }

    onDelFile()
    {
        this.uploadPercent = 0;
        this.file = null;
        this.url = '';
        this.setValue('');
    }
}
