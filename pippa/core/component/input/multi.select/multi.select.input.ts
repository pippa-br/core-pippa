import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, SkipSelf } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { BaseModel } from "../../../model/base.model";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { TDate } from "../../../util/tdate/tdate";

@Component({
    selector        : ".multi-select-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted, 'has-add' : inputAdd }">
								
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ng-select #select
										   [(ngModel)]="selecteds"
										   [ngModelOptions]="{standalone: true}"
                                           [items]="items"
                                           [addTag]="hasAdd"
										   [loading]="loading"
                                           [disabled]="!editable"
										   [bindLabel]="bindLabel"
                                           [closeOnSelect]="closeOnSelect"
                                           [clearSearchOnAdd]="true"
                                           [hideSelected]="true"
                                           [hideSelected]="true"
										   multiple="true"
										   appendTo="body"
                                           notFoundText="Item não encontrado"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           [placeholder]="field.label"
                                           [clearable]="clearable"
                                           (change)="onInput($event)"
                                           (clear)="onReset()">

                                      <ng-template ng-label-tmp let-item="item">                                           
                                            <span class="item-label" [ngClass]="{'disabled': item.disabled }">
                                                {{item[bindLabel]}} 
                                                <ion-icon (click)="onToggleDisabled(item)" *ngIf="hasDisabled && !item.disabled" name="lock-open-outline"></ion-icon>
                                                <ion-icon (click)="onToggleDisabled(item)" *ngIf="hasDisabled && item.disabled" name="lock-closed-outline"></ion-icon>
                                                <ion-icon (click)="onRemove(item)" name="close-circle-outline"></ion-icon>
                                            </span>                                           
                                      </ng-template>

								</ng-select>

                                <p *ngIf="onSetSelected" class="links-bar">
                                    &nbsp;<a *ngIf="hasValue()" (click)="_onSetSelected()"><small>editar</small></a>
								</p>
								
								<icon-input *ngIf="inputAdd"
                                            [isLoading]="isLoading"
                                            [icon]="_icon"
                                            (click)="onEditItem()">
								</icon-input>
								                                
							</div>							
							<error-input [control]="formControl" [submitted]="submitted">
							</error-input>							
                        </div>`
})
export class MultiSelectInput extends BaseReferenceInput
{
    @ViewChild("select", { static : false }) select : any;

    public selecteds   	 : any;
    public closeOnSelect  = false
    public inputAdd       = false;
    public onSetSelected : any;
    public clearable      = true;
    public loading        = false;
    public hasAdd        : any     = false;
    public hasDisabled   : any     = false;
    public items         : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col multi-select-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        /* SETTINGS */
        if (this.field.setting)
        {
            /* HAS CLEAR */
            if (this.setting.clearable)
            {
                this.clearable = this.setting.clearable == "true";
            }

            /* CLOSE ON SELECT */
            if (this.setting.closeOnSelect)
            {
                this.closeOnSelect = this.setting.closeOnSelect == "true";
            }

            /* HAS DISABLED */
            if (this.setting.hasDisabled)
            {
                this.hasDisabled = this.setting.hasDisabled;
            }

            /* ON SET SELECTED */
            if (this.setting.onSetSelected)
            {
                this.onSetSelected = this.setting.onSetSelected;
            }

            /* HAS ADD */
            if (this.setting.inputAdd)
            {
                this.app      = this.core().getApp("option");
                this._icon    = { value : "create" };
                this.inputAdd = true;
            }

            /* ADD TAG */
            if (this.setting.hasAdd)
            {
                this.hasAdd = (name:string) =>
                {
                    return new Promise((resolve) =>
                    {
                        //this.loading = true;
                        setTimeout(() => 
                        {
                            resolve({
                                label : name,
                                value : Math.floor(new TDate().unix() * Math.random())
                            });
                        });
                    })
                };
            }
        }
    }

    onRemove(item:any)
    {
        const items = [];

        for (const item2 of this.selecteds)
        {
            if (item2[this.bindValue] !== item[this.bindValue])
            {
                items.push(item2);
            }
        }

        this.selecteds = items;

        this.onInput(this.selecteds)
    }

    onToggleDisabled(item:any)
    {
        const items = [];

        for (const item2 of this.selecteds)
        {
            if (item2.id == item.id)
            {
                item2.disabled = !item2.disabled;
                item.disabled  = !item.disabled;
            }

            items.push(item2);
        }

        this.selecteds = items;

        this.onInput(this.selecteds)
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        }
    }

    async loadOption()
    {
        this.items = this.option["items"];
    }

    async onEditItem()
    {
        await this.addDocument(this.option);
    }

    _onSetSelected()
    {
        this.onSetSelected({
            target : this,
            data   : this.getValue(),
        });
    }

    reset()
    {
        this.patchValue([]);

        this.dirty = false;
    }

    onInput(data:any)
    {
        // REMOVE EXTRA DATA
        for (const item of data)
        {
            delete item._label;
            delete item.colid;
        }

        this.setValue(data);

        /*if(this.field.onInput)
        {
            this.field.onInput({
                field : this.field,
                data  : data,
            });
        }*/
    }

    async setCollection(items)
    {
        const _items = [];

        // CASO DE ARRAY DENTRO DE ARRAY
        for(const item of items)
        {
            if(item instanceof Array)
            {
                _items.push(...item);
            }
            else
            {
                _items.push(item);
            }
        }

        items = _items;

        await super.setCollection(items);

        // BUG: NAO ESTAVA ABRINDO OS ITEMS DO DROP, SO NO SEGUNDO CLICK
        if (this.select)
        {
            this.select.focus();
        }		
    }

    async mergeSelected()
    {
        if (this.selecteds && this.collection)
        {
            for (const item of this.collection)
            {
                for (const item2 of this.selecteds)
                {
                    /* VERIFICA SE JA EXISTE */
                    if (item.id == item2.id)
                    {
                        this.collection.set(item2)
                    }
                }
            }            
        }
    }

    /*updateValue(data)
	{		
		if(data == undefined)
        {
			this.selecteds = null
        }
        else
        {
			this.selecteds = data;
		}
		
		super._updateValue(data);
	}*/

    async onModelChange(data:any)
    {
        if (data)
        {
            /* DEIXAR SEPARADO DO DATA */
            if (!this.dirty)
            {
                this.dirty = true;
                /*const selecteds = [];

				data.forEach(item => 
				{
					selecteds.push(item[this.bindValue]);
				});

				this.selecteds = selecteds;

				console.error(data, this.selecteds)*/

                this.selecteds = data;

                await this.displayControlByValue(data);
	
                /* NÃO USASAR this.onSelect POIS ESSA FUNCAO E PARA INTERACAO NA INTERFACE, usar onChange */	
                this.emitChange(data);
            }					

            //console.error(data, this.selecteds);
        } 
    }

    async displayControlByValue(data:any)
    {
        if (this.isControl)
        {
            this.displayControl(false); /* DESABILITA TODOS */

            if (this.setting && data)
            {
                for (const item of data)
                {
                    const path  = this.controlPath || this.bindValue;		
                    const model = await new BaseModel(item).on();
                    const value = await model.getProperty(path);
    
                    //console.error(item, path);
    
                    if (value != undefined)
                    {
                        this.displayControlByNames(this.setting[value], true); /* HABILITA O ATUAL */	
                    }
                }								
            }
        }
    }
}
