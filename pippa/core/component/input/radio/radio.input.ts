import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators, FormGroup } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";
import { BaseModel } from "../../../model/base.model";

@Component({
    selector        : ".radio-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{ 'ng-submitted'    : submitted,
                            'ng-valid'        : formControl?.valid,
                            'ng-invalid'      : !formControl?.valid,
                            'has-attachments' : field.hasAttachments(),
                            'hidden'          : hidden}">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)">
                                <label *ngFor="let item of items; trackBy : trackById;"
                                    class="label-radio"
                                    [ngClass]="{'disabled' : item._canceled}">
                                    <input type="radio"
                                        [name]="item.id"
                                        [value]="item.id"
                                        [(ngModel)]="selectedId"
                                        [disabled]="item._canceled"
                                        [ngModelOptions]="{standalone: true}"
                                        (change)="onChange(item)">
                                    {{item.label}}&nbsp;<small *ngIf="showValue">({{getValueOption(item)}})</small>
                                </label>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>
                        <attachment-input *ngIf="field.hasAttachments()" [field]="field" (attachment)="onAttachment($event)">
                        </attachment-input>`
})
export class RadioInput extends BaseInput
{
    public selectedId : any;
    public showValue   = false;
    public items      : any;
    
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:FormGroup)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col radio-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        /* SETTINGS */
        if (this.setting)
        {
            if (this.setting.inline)
            {
                this.nameClass += "inline "
            }

            if (this.setting.showValue)
            {
                this.showValue = true;
            }
        }
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        }
    }

    onChange(item:any)
    {
        this.updateValue(item);
        //this.displayControlByValue(item);
    }

    async loadOption()
    {
        const items = this.option["items"]

        /* CADASTROS ANTIGOS */
        for (const key in items)
        {
            if (items[key].id == undefined)
            {
                items[key].id = items[key].value;
            }
        }

        this.items = items;	
    }

    /* A CASOS QUE O VALUE DO OPTION É UM OBJETO: {label, value} */
    getValueOption(data:any)
    {
        if (data)
        {
            if (this.controlPath) // PARA PEGAR O ID
            {
                return 	data[this.controlPath];
            }
            else if (data.value && data.value.value !== undefined)
            {
                return data.value.value;
            }
            else if (data.value !== undefined)
            {
                return data.value;
            }
            else
            {
                return data;
            }
        }
    }
	
    updateActive(item:any)
    {
        super.updateActive(item);
		
        if (item)
        {
            if (item.value != undefined)
            {
                this.selectedId = (item.id ? item.id : item.value); /* CADASTROS ANTIGOS */
            }
            else
            {
                this.selectedId = item;
            }    		
        }
    }

    onModelChange(data:any)
    {
        if (data && !this.dirty)
        {			            
            this.dirty = true;
            this.updateActive(data);
            this.displayControlByValue(data);

            // PARA POPULAR VIA URL
            for (const key in this.items)
            {
                if (this.selectedId == this.items[key][this.bindValue])
                {
                    data = this.items[key];
                    this.updateValue(data);
                    break;
                }
            }	
        }
    }

    setCollection(value:any)
    {
        this.items = value;

        this.markForCheck();
    }

    async displayControlByValue(data:any)
    {
        if (this.isControl)
        {
            this.displayControl(false); /* DESABILITA TODOS */

            if (this.setting && data)
            {
                const path  = this.controlPath || this.bindValue;		
                const model = await new BaseModel(data).on();
                const value = await model.getProperty(path);

                if (value != undefined)
                {
                    this.displayControlByNames(this.setting[value], true); /* HABILITA O ATUAL */	
                }				
            }
        }
    }
}
