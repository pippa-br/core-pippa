import { Component, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { ActionSheetController, AlertController } from "@ionic/angular";
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import * as loadImage from "blueimp-load-image"

/* PIPPA */
import { BaseInput }        from "../base.input";
import { FileCollection }   from "../../../../core/model/file/file.collection";
import { UploadPlugin }     from "../../../../core/plugin/upload/upload.plugin";
import { ComentImageModal } from "../../../../core/modal/coment.image/coment.image.modal";
import { ModalController }  from "../../../../core/controller/modal/modal.controller";

@Component({
    selector        : "[upload-json-input]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper" 
													[ngClass]="{ 'ng-submitted' : submitted, 
													'ng-valid' 	: formControl?.valid && this.hasValue(), 
													'ng-invalid'   : !formControl?.valid,
													'hidden'       : hidden,
													'no-editable'	: !editable,
													'boxes'		: boxes}">
                            <div header-input 
                                 [form]="form" 
                                 [formItem]="formItem" 
                                 (edit)="onEdit($event)" 
                                 (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)" />
                                
                                <input #fileInput
                                        type="file"
                                        style="display: none;"
                                        [multiple]="multiple"
                                        [accept]="accept"
                                        (change)="onUpload($event)" />

								<div class="striped list list-container" cdkDropListGroup mixedCdkDragDrop [orientation]="dropListOrientation" (dropped)="drop($event)">
									<div *ngFor="let file of files;let last = last;" cdkDropList mixedCdkDropList>										
										<div class="col-container" cdkDrag [cdkDragDisabled]="dragDisabled">
											<div class="cell">
												<div [ngClass]="file.type">
													<a href="{{file._url}}" target="_blank">
														<span *ngIf="!showThumb || !file.isImage()">{{file.name}}</span>
														<img src="{{file._url}}" *ngIf="showThumb && file.isImage()">
													</a>
												</div>
											</div>
											<div class="buttons-col" *ngIf="editable">
												<div class="buttons-div">
													<ion-button class="add-button" *ngIf="last && multiple && !isMaxFiles()" (click)="onAdd()">
														<ion-icon name="add-outline"></ion-icon>
													</ion-button>
													<ion-button (click)="openModal(file)" *ngIf="hasComment">
														<ion-icon name="chatboxes-outline"></ion-icon>
													</ion-button>
													<ion-button class="set-button" (click)="onSet(file)">
														<ion-icon name="create-outline"></ion-icon>
													</ion-button>
													<ion-button class="del-button" (click)="onRemove(file)">
														<ion-icon name="remove-outline"></ion-icon>
													</ion-button>												
												</div>
											</div>
										</div>
									</div>
									<div *ngFor="let file of samples;let last = last;" cdkDropList mixedCdkDropList>
										<div class="col-container" cdkDrag [cdkDragDisabled]="dragDisabled">
											<div class="cell">
												<div [ngClass]="file.type" (click)="onSet(file)">
													<span>{{file.sample}}</span>
												</div>
											</div>
											<div class="buttons-col" *ngIf="editable">
												<div class="buttons-div">												
													<ion-button class="set-button" (click)="onSet(file)">
														<ion-icon name="create-outline"></ion-icon>
													</ion-button>
												</div>
											</div>
										</div>
									</div>
                                    <div class="add-input-button" *ngIf="files.length == 0 && editable && !isMaxFiles()" (click)="onAdd()">
										<div class="col-container">
											<div class="no-items">
												<label>Adicionar {{label | translate}}</label>
											</div>											
										</div>
										<div class="buttons-col">
											<div class="buttons-div">
												<ion-button>
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="invalidUrl">Url Invalida!</span>
								<span *ngIf="invalidSize">Tamanho maior que {{maxSize}}MB!</span>
								<span *ngIf="invalidMaxFiles">Nº de arquivos maior que máximo permitido ({{maxFiles}})</span>
                            </error-input>
                        </div>
                        <attachment-input *ngIf="field.hasAttachments()" [field]="field" (attachment)="onAttachment($event)">
                        </attachment-input>`
})
export class UploadJsonInput extends BaseInput
{
    @ViewChild("fileInput", { static : false }) fileInput: ElementRef;

    public files       		   : FileCollection = new FileCollection();
    public samples       	   : FileCollection = new FileCollection();
    public invalidUrl  		           = false;
    public multiple    		           = false;
    public hasComment  		           = false;
    public accept      		            = "*";
    public label       		            = "Upload";
    public maxSize     		     		= 1;
    public maxFiles     	     		= 999;
    public invalidSize 		    		= false;
    public invalidMaxFiles	    		= false;	
    public boxes 	   		    		= false;
    public showThumb   		    		= true;
    public frontResize   	    		= false;	
    public dragDisabled   	    		= false;	
    public dropListOrientation   		= "horizonntal";
    public selected 		   : any;

    constructor(
		public alertController   	 : AlertController,
        public uploadPlugin          : UploadPlugin,
        public modalController       : ModalController,
        public changeDetectorRef     : ChangeDetectorRef,
        public actionSheetController : ActionSheetController,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col upload-json-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        /* MULTIPLE*/
        if (this.setting.multiple !== undefined)
        {
            this.multiple = this.setting.multiple;
        }

        /* HAS COMMENT */
        if (this.setting.hasComment !== undefined)
        {
            this.hasComment = this.setting.hasComment;
        }

        /* ACCEPT */
        if (this.setting.accept)
        {
            this.accept = this.setting.accept;
        }

        /* LABEL */
        if (this.setting.label)
        {
            this.label = this.setting.label;
        }
        else
        {
            this.label = this.field.label;
        }

        /* MAXSIZE */
        if (this.setting.maxSize)
        {
            this.maxSize = this.setting.maxSize;
        }

        /* SHOW THUMBS */
        if (this.setting.showThumb !== undefined)
        {
            this.showThumb = this.setting.showThumb;
        }

        /* MAX FILES */
        if (this.setting.maxFiles !== undefined)
        {
            this.maxFiles = this.setting.maxFiles;
        }

        /* drag Disabled */
        if (this.setting.dragDisabled !== undefined)
        {
            this.dragDisabled = this.setting.dragDisabled;
        }
		
        /* front resize */
        if (this.setting.frontResize !== undefined)
        {
            this.frontResize = this.setting.frontResize;
        }
	
        /* SAMPLES */
        this.createSamples();

        /* BOXES */
        if (this.setting.boxes)
        {
            this.boxes               = this.setting.boxes;
            this.dropListOrientation = "horizontal";
        }

        this.accept += ";capture=camera";
    }

    createSamples()
    {		
        if (this.setting.samples !== undefined)
        {
            this.samples  = new FileCollection();
            const samples = this.setting.samples.split(",");

            for (const name of samples)
            {
                if (!this.files.hasSample(name))
                {
                    this.samples.setItem({
                        type   : "sample",
                        sample : name,
                    });	
                }				
            }
        }
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        }
    }

    drop(event: CdkDragDrop<string[]>) 
    {
        moveItemInArray(this.files, event.previousIndex, event.currentIndex);
        this.updateFiles();
    }

    onAdd()
    {
        if (!this.core().isMobile())
        {
            this.fileInput.nativeElement.click();
        }
        else
        {
            this.actionSheetController.create(
                {
                    header  : "Imagem",
                    buttons : [
                        {
                            text    : "Camera",
                            icon    : "camera",
                            handler : () => 
                            {
                                //const base64 = "data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";

                                Camera.getPhoto({
                                    quality      : 80,
                                    allowEditing : false,
                                    resultType   : CameraResultType.DataUrl,
                                    source       : CameraSource.Camera,
                                })
                        .then(async (image:any) => 
                        {
                            await this.core().loadingController.start(true);

                            loadImage(image.dataUrl, { maxWidth : 1024, canvas : true, }).then((data) =>
                            {								
                                data.image.toBlob((blob) => 
                                {				
                                    const file = new File([ blob ], this.core().util.randomString(10) + ".jpeg", { type : "image/jpeg" });

                                    this.singleUpload(file);
                                }, "image/jpeg", 0.7);
                            });

                            // fromURL(image.dataUrl, 80, 'auto', 1024, 'webp').then((blob) => 
                            // {
                            // 	blobToURL(blob).then((url) =>
                            // 	{
                            // 		//this.core().util.resizeBase64Img(url, 1024, 1024).then((data) => 
                            // 		//{
                            // 			/* UPLOAD */
                            // 			this.uploadPlugin.uploadFromString(url).then(result =>
                            // 			{
                            // 				this.files.setItem(result.data);

                            // 				this.updateFiles();
                            // 				this.markForCheck();
                            // 				this.clear();
											
                            // 				this.modalController.dismiss();
                            // 			});	
                            // 		//})
                            // 	});						
                            // });																				
                        })
                            }
                        }, 
                        {
                            text    : "Biblioteca",
                            icon    : "images",
                            handler : () => 
                            {
                                this.fileInput.nativeElement.click();
                            }
                        } ]
                })
            .then((actionSheet:any) => 
            {
                actionSheet.present();
            })
        }
    }

    onSet(item:any)
    {
        this.selected = item;
        this.onAdd();
    }

    isMaxFiles()
    {
        return this.files.length + this.samples.length == this.maxFiles;
    }

    async onUpload(event:any)
    {
        if (event.target.files.length + this.isMaxFiles() <= this.maxFiles || this.selected)
        {
            this.invalidSize = false;

            if (this.validateSizes(event.target.files))
            {
                if (this.multiple && !this.selected)
                {
                    if (this.frontResize)
                    {
                        const promises = [];

                        for (const item of event.target.files)
                        {
                            promises.push(new Promise((resolve) => 
                            {
                                loadImage(item, { maxWidth : 1024, canvas : true }).then((data) =>
                                {
                                    data.image.toBlob((blob) => 
                                    {				
                                        const file = new File([ blob ], this.core().util.randomString(10) + ".jpeg", { type : "image/jpeg" });
			
                                        resolve(file)
                                    }, "image/jpeg", 0.7);
                                });
                            }))
                        }
	
                        const values = await Promise.all(promises)
						
                        this.uploadPlugin.uploadMultiple(values).then((files:any) =>
                        {
                            this.files.setItems(files);
			
                            this.updateFiles();
			
                            this.markForCheck();
                            this.clear();
                        });
                    }
                    else
                    {
                        this.uploadPlugin.uploadMultiple(event.target.files).then((files:any) =>
                        {
                            this.files.setItems(files);
			
                            this.updateFiles();
			
                            this.markForCheck();
                            this.clear();
                        });
                    }					
                }
                else
                {
                    if (this.core().isMobile() && this.isFileImage(event.target.files[0]))
                    {
                        loadImage(event.target.files[0], { maxWidth : 1024, canvas : true }).then((data) =>
                        {
                            data.image.toBlob((blob) => 
                            {				
                                const file = new File([ blob ], this.core().util.randomString(10) + ".jpeg", { type : "image/jpeg" });

                                this.singleUpload(file);
                            }, "image/jpeg", 0.7);
                        });
                    }
                    else if (this.frontResize && this.isFileImage(event.target.files[0]))
                    {
                        loadImage(event.target.files[0], { maxWidth : 1024, canvas : true }).then((data) =>
                        {
                            data.image.toBlob((blob) => 
                            {				
                                const file = new File([ blob ], this.core().util.randomString(10) + ".jpeg", { type : "image/jpeg" });

                                this.singleUpload(file);
                            }, "image/jpeg", 0.7);
                        });
                    }
                    else
                    {
                        this.singleUpload(event.target.files[0]);
                    }
                }
            }  
            else
            {
                if (this.core().isMobile())
                {
                    this.alertController.create({
                        header  : "Alerta",
                        message : "Tamanho maior que" + this.maxSize + "MB!",
                        buttons : [
                            {
                                text    : "Fechar",
                                handler : () =>
                                {
                                    this.alertController.dismiss();
                                }
                            },
                        ]
                    })
					.then(alert =>
					{
					    alert.present();
					}) 
                }

                this.invalidSize = true;
                this.clear();
            }    
        }
        else
        {
            this.invalidMaxFiles = true;
        }		  
    }

    singleUpload(file:any)
    {
        this.uploadPlugin.uploadMultiple([ file ]).then((files:any) =>
        {
            if (this.hasComment)
            {
                this.openModal(files[0]);
            }
            else if (this.selected)
            {
                this.selected.populate(files[0]);
                this.files.setItem(this.selected);
                this.selected = null;

                // ATUALIZA OS SAMPLES
                this.createSamples();

                this.markForCheck();
                this.updateFiles();
            }
            else
            {
                this.files.setItems(files);

                this.markForCheck();
                this.updateFiles();
            }

            this.clear();
        });
    }

    isFileImage(file) 
    {
        return file && file["type"].split("/")[0] === "image";
    }

    validateSizes(files:any)
    {
        for (const file of files)
        {
            const mb = file.size / 1024 / 1024;

            if (mb > this.maxSize)
            {
                return false;
            }
        }

        return true;
    }
	
    clear()
    {
        this.fileInput.nativeElement.value = "";
    }

    reset()
    {
        this.files = new FileCollection();
        this.createSamples()
        this.dirty = false;
    }

    updateFiles()
    {
        if (this.multiple)
        {
            this.setValue(this.files.parseData());
        }
        else if (this.files.length > 0)
        {
            this.setValue(this.files[0].parseData());
        }
        else
        {
            this.setValue("");
        }

        this.invalidUrl = false;
    }

    openModal(file: any)
    {
        this.modalController.create(ComentImageModal,
            {
                file   : file,
                onSave : (file2 : any) =>
                {
                    this.files.set(file2);
                    this.updateFiles();
                    this.modalController.dismiss();
                },
                onClose : () =>
                {
                    this.modalController.dismiss();
                }
            }, true)
        .then(modal =>
        {
            modal.present();
        });
    }

    /* CHAMADAS ESXTERNAS */
    populate(value:any)
    {
        this.files = new FileCollection(value);
        super.populate(value);
    }

    onRemove(item:any)
    {
        this.alertController.create({
            header  : "Alerta",
            message : "Deseja remover esse item?",
            buttons : [
                {
                    text    : "Fechar",
                    handler : () =>
                    {
                        this.alertController.dismiss();
                    }
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        this.files.del(item);
        				this.updateFiles();

                        // ATUALIZA OS SAMPLES
                        this.createSamples();
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        })        
    }

    onModelChange(value:any)
    {
        if (value && !this.dirty)
        {
            if (typeof value == "string")
            {
                this.files = new FileCollection([ {
                    name : value.replace(/^.*[\\\/]/, ""),
                    _url : value
                } ]);
            }
            else if (this.multiple)
            {
                this.files = new FileCollection(value);
            }
            else
            {
                this.files = new FileCollection([ value ]);
            }

            // CADASTROS ANTIGOS SEM SAMPLES
            if (this.setting.samples !== undefined)
            {
                const samples = this.setting.samples.split(",");
                let i         = 0;

                for (const file of this.files)
                {
                    if (samples[i])
                    {
                        file.sample = samples[i];											
                    }					

                    i++;
                }
            }

            // ATUALIZA OS SAMPLES
            this.createSamples();

            this.dirty = true;
        }
    }
}
