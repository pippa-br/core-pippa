import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { BaseModel }    	  from "../../../model/base.model";
import { OptionPlugin } 	  from "../../../plugin/option/option.plugin";
import { BaseList } 		  from "../../../model/base.list";

@Component({
    selector        : ".select-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'has-add' : inputAdd }">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
								
                                <ng-select #select
                                           [(ngModel)]="selectedId"
                                           [ngModelOptions]="{standalone: true}"
                                           [disabled]="(!editable && isSetModeForm()) || readonly"
                                           [searchable]="searchable"
                                           [bindLabel]="bindLabel"
                                           [bindValue]="bindValue"
                                           appendTo="body"
                                           groupBy="groupBy"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           [items]="items"
                                           notFoundText="Item não encontrado"
                                           [placeholder]="(isLoading ? 'Carregando...' : field.label)"
                                           [clearable]="clearable"
                                           (change)="onInput($event)"
                                           (clear)="onReset()">
                                </ng-select>
								
								<p *ngIf="onSetSelected" class="links-bar">
                                    &nbsp;<a *ngIf="hasValue()" (click)="_onSetSelected()"><small>editar</small></a>
								</p>
								
								<icon-input *ngIf="inputAdd"
                                            [isLoading]="isLoading"
                                            [icon]="_icon"
                                            (click)="onEditItem()">
								</icon-input>
								
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class SelectInput extends BaseReferenceInput implements OnInit
{
    @ViewChild("select", { static : false }) select : any;

    public clearable       = true;
    public searchable      = false;
    public inputAdd        = false;
    public selectedId     : any;
    public selected       : any;
    public onSetSelected  : any;
    public items          : any;
    public _icon          : any ;
    public orderBy        : string;
    public asc      	   = true;
    public nullItem        = false;
    public mergeOptions    = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col select-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";        
    }

    async createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        }

        /* HAS CLEAR */
        if (this.setting.clearable)
        {
            this.clearable = this.setting.clearable;
        }

        /* HAS SEARCH */
        if (this.setting.searchable != undefined)
        {
            this.searchable = this.setting.searchable;
        }

        /* ON SET SELECTED */
        if (this.setting.onSetSelected)
        {
            this.onSetSelected = this.setting.onSetSelected;
        }

        /* MERGE OPTIONS */
        if (this.setting.mergeOptions)
        {
            this.mergeOptions = this.setting.mergeOptions;
        }

        /* HAS ADD */
        if (this.setting.inputAdd)
        {
            this.app      = this.core().getApp("option");
            this._icon    = { value : "create" };
            this.inputAdd = true;
        }

        // ORDER BY
        if (this.setting.orderBy)
        {
            this.orderBy = this.setting.orderBy;
        }

        // ASC
        if (this.setting.asc !== undefined)
        {
            this.asc = this.setting.asc;
        }

        // ITEM NULL
        if (this.setting.nullItem !== undefined)
        {
            this.nullItem = this.setting.nullItem;
        }

        if (this.setting.optionID)
        {
            const params = this.createParams({
                appid : "option",
                where : [ {
                    field : "code",
                    value : this.setting.optionID
                } ],
                orderBy : "name"
            });

            const optionPlugin = this.core().injector.get(OptionPlugin);
            const result       = await optionPlugin.getData(params);

            if (result.collection.length > 0)
            {
                this.option = result.collection[0];
                await this.loadOption();
            }
        }
        else if (this.setting.api)
        {
            const result = await this.core().api.callable(this.setting.api, 
                {
                    query : this.setting.query || ""
                }, 
                {
                    list : BaseList
                });

            this.isLoading = false;

            this.collection = result.collection;
            await this.doLabels();
        }
    }

    async loadOption()
    {
        let items = this.option["items"];

        if (items && (!this.core().user || !this.core().user.isMaster()))
        {
            items = items.filter((item) =>
            {
                return item.value != "master";
            });
        }

        if (this.orderBy)
        {
            items.sort((a:any, b:any) => 
            {
                if (this.asc)
                {
                    return a[this.orderBy] - b[this.orderBy];					
                }
                else
                {
                    return b[this.orderBy] - a[this.orderBy];						
                }				
            })	
        }

        if (this.nullItem)
        {
            items.unshift({
                id 	  : "null", // USAR STRING PARA PODER SELECIONAR AO EDITAR
                label : "Nenhum",
                value : "null"
            })
        }		

        this.items = items;	
    }

    async mergeSelected()
    {
        if (this.selected && this.items)
        {
            // MERGE SELECTED
            let has = false;

            for (const data of this.items)
            {				
                /* VERIFICA SE JA EXISTE */
                if (this.selected[this.bindValue] == data[this.bindValue])
                {
                    has = true;
                    break;
                }
            }				
			
            if (!has)
            {		
                this.items = [ ...this.items, this.selected ];
            }
        }
    }
	
    async onEditItem()
    {
        await this.addDocument(this.option);
    }

    async setCollection(items:any)
    {
        const _items = [];

        // CASO DE ARRAY DENTRO DE ARRAY
        for(const item of items)
        {
            if(item instanceof Array)
            {
                _items.push(...item);
            }
            else
            {
                _items.push(item);
            }
        }

        items = _items;

        if (this.mergeOptions)
        {
            for (const item of items)
            {
                let has = false;

                for (const item2 of this.items)
                {
                    if (item.id == item2.id)
                    {
                        has = true;
                        break;
                    }
                }

                if (!has)
                {
                    this.items.unshift(item);
                }
            }

            this.items = [ ...this.items ]
        }
        else
        {
            this.items = items;
        }

        this.markForCheck();
    }
	
    /* OVERRIDE */
    doAdd(data:any)
    {
        this.option = data;
        this.loadOption();
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.option = data;
        this.loadOption();
    }

    _onSetSelected()
    {
        this.onSetSelected({
            target : this,
            data   : this.getValue(),
        });
    }

    reset()
    {
        if (!this.field.fixed)
        {
            this.updateValue(null);
        }
		
        this.dirty = false;
    }

    onInput(item:any)
    {
        this.changeInput = true;
        this.setValue(item);
    }

    updateValue(data)
    {		
        if (!data)
        {
            this.selectedId = null
            this.selected   = null;
        }
        else
        {
            /* POR CAUSA DO BASEREFERENCE */
            if (typeof data.getData == "function")
            {
                data = data.getData();
            }

            this.selectedId = data[this.bindValue];
            this.selected   = data;
        }

        super._updateValue(data);
    }

    async onModelChange(data:any)
    {
        if (data)
        {
            /* DEIXAR SEPARADO DO DATA */
            if (!this.dirty)
            {
                this.selectedId = data[this.bindValue];
                this.selected   = data;
                this.dirty      = true;
				
                // PARA POPULAR VIA URL
                for (const key in this.items)
                {
                    if (this.selectedId == this.items[key][this.bindValue])
                    {
                        data = this.items[key];
                        this.updateValue(data);
                        break;
                    }
                }
				
                await this.displayControlByValue(data);
                this.mergeSelected();
	
                /* NÃO USASAR this.onSelect POIS ESSA FUNCAO E PARA INTERACAO NA INTERFACE, usar onChange */	
                this.emitChange(data);	
            }				
        } 
    }

    async displayControlByValue(data:any)
    {
        if (this.isControl)
        {
            this.displayControl(false); /* DESABILITA TODOS */

            if (this.setting && data)
            {
                const path  = this.controlPath || this.bindValue;		
                const model = await new BaseModel(data).on();
                const value = await model.getProperty(path);

                if (value != undefined)
                {
                    this.displayControlByNames(this.setting[value], true); /* HABILITA O ATUAL */	
                }				
            }
        }
    }
}
