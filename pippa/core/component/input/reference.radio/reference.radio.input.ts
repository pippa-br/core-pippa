import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { InputValidate } 	  from "../../../validate/input.validate";
import { Viewer }             from "../../../model/viewer/viewer";
import { Document }           from "../../../model/document/document";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";

@Component({
    selector        : ".reference-radio-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                             <div header-input
                                  [form]="form"
                                  [formItem]="formItem"
                                  (edit)="onEdit()"
                                  (del)="onDel()">
                            </div>
                            <div class="item-input"
                                 [formGroup]="field.formGroup"
                                 [ngClass]="{'has-add' : inputAdd && editable}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <label *ngFor="let item of items; trackBy : trackById;"
                                    class="label-radio"
                                    [ngClass]="{'disabled' : item._canceled}">
                                    <input type="radio"
                                        [name]="item.id"
                                        [value]="item.id"
                                        [(ngModel)]="selectedId"
                                        [disabled]="item._canceled"
                                        [ngModelOptions]="{standalone: true}"
                                        (change)="onInput(item)">
                                    {{item._label}}
                                </label>

                                <div dynamic-viewer
                                     [viewer]="viewer"
                                     [data]="selected"
                                     (rendererComplete)="markForCheck();">
                                </div>                                   

                                <icon-input *ngIf="inputAdd && editable"
                                            [isLoading]="isLoading"
                                            [icon]="_icon"
                                            (click)="addDocument()">
                                </icon-input>
                            </div>
                            <error-input [singleText]="singleText" [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class ReferenceRadioInput extends BaseReferenceInput
{	
    public viewer : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + "-input input col reference-radio-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        if (this.setting.viewerPath)
        {
            const paths = this.setting.viewerPath.split("/");

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid       : paths[0],
                appid       : paths[1],
                colid       : paths[2],
                path        : this.setting.viewerPath,
                model       : Viewer,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                },
                map : true,
            })

            this.core().api.getObject(params).then((result:any) =>
            {
                this.viewer = result.data;
                this.markForCheck();
            });
        }
		
        if (this.setting.singleText)
        {
            this.singleText = this.setting.singleText;
        }
    }

    createValidators()
    {
        super.createValidators();
		
        this.validators      = [];
        this.asyncValidators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }

            this.asyncValidators.push(InputValidate.search(this.field, this.form, this));
        } 
    }

    /* OVERRIDE */
    doAdd(data:any)
    {
        this.populate(data);
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.populate(data);
    }

    async onModelChange(data:any)
    {
        if (data && !this.dirty)
        {
            this.dirty = true;

            const model : any = new Document(data);
            await model.on();

            this.selected   = model;
            this.selectedId = model.id;
			
            this.doLabels();

            /* INVERSED BY */
            this.updateInversedBy(data, "inversedBy");

            /* INIT INVERSED BY */
            this.updateInversedBy(data, "initInversedBy");

            //this.markForCheck();

            //this.displayControlByValue(data);
        }        
    }
}
