import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput }  from '../base.input';
import { Collection } from '../../../../core/util/collection/collection';

@Component({
    selector : `.arrange-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (set)="onSet($event)"
                     (del)="onDel()">
                </div>
                <div class="item-input"
                     [formGroup]="field.formGroup"
                     [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                    <ion-grid>
                        <ion-row>
                            <ion-col col-5>

                                <ng-select #select0
                                           *ngIf="this.isSelect0"
										   [items]="options0"
										   appendTo="body"
                                           [placeholder]="field.option.items[0].label"
                                           [attr.disabled]="form?.isSetModeForm() && !field.editable"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           (change)="onSelectChange(0, $event)"
                                           (clear)="onDeselected(0)">
                                </ng-select>

                                <input type="text" #text0
                                                   *ngIf="!this.isSelect0"
                                                   [placeholder]="field.option.items[0].label" [value]="field.initial"
                                                   [readonly]="form?.isSetModeForm() && !field.editable"
                                                   (blur)="onTextBlur(0, $event)"/>

                            </ion-col>
                            <ion-col col-5>

                                <ng-select #select1
                                           *ngIf="this.isSelect1"
										   [items]="options1"
										   appendTo="body"
                                           [placeholder]="field.option.items[1].label"
                                           [attr.disabled]="form?.isSetModeForm() && !field.editable"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           (change)="onSelectChange(1, $event)"
                                           (clear)="onDeselected(1)">
                                </ng-select>

                                <input type="text" #text1
                                                   *ngIf="!this.isSelect1"
                                                   [placeholder]="field.option.items[1].label" [value]="field.initial"
                                                   [readonly]="form?.isSetModeForm() && !field.editable"
                                                   (blur)="onTextBlur(1, $event)"/>

                            </ion-col>
                            <ion-col col-2>
                                <button class="add-button" ion-button icon-only outline (click)="onAdd()">
                                    <ion-icon name="add-circle-outline"></ion-icon>
                                </button>
                            </ion-col>
                        </ion-row>
                    </ion-grid>
                    <ion-grid class="display">
                        <ion-row *ngFor="let item of options; let i = index">
                            <ion-col col-5>
                                <label>{{item[0].label}}</label>
                            </ion-col>
                            <ion-col col-5>
                                <label>{{item[1].label}}</label>
                            </ion-col>
                            <ion-col col-2>
                                <button class="del-button" ion-button icon-only outline (click)="onRemove(item, i)">
                                    <ion-icon name="remove-circle-outline"></ion-icon>
                                </button>
                            </ion-col>
                        </ion-row>
                    </ion-grid>
                    <div class="error" *ngIf="submitted">
                        <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                    </div>
                </div>`
})
export class ArrangeInput extends BaseInput
{
    @ViewChild('select0', {static: false}) select0 : any;
    @ViewChild('select1', {static: false}) select1 : any;
    @ViewChild('text0',   {static: false}) text0   : any;
    @ViewChild('text1',   {static: false}) text1   : any;

    isSelect0 : boolean = true;
    isSelect1 : boolean = true;

    options0 : Array<any> = [];
    options1 : Array<any> = [];

    items   : Array<any> = [];
    options : Collection = new Collection();

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col arrange-field ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* 0 */
        this.options0 = [];

        for(let key in this.field.option.items[0].options)
        {
            let item = this.field.option.items[0].options[key];
            this.options0.push({
                label : item.value + ' - ' + item.label,
                value : item.value,
            });
        }

        this.isSelect0 = this.options0.length > 0;

        /* 1 */
        this.options1 = [];

        for(let key in this.field.option.items[1].options)
        {
            let item = this.field.option.items[1].options[key];
            this.options1.push({
                label : item.value + ' - ' + item.label,
                value : item.value,
            });
        }

        this.isSelect1 = this.options1.length > 0;

        this.sort();
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    sort()
    {
        if(this.isSelect0)
        {
            this.options0.sort(function(a, b)
            {
                if(isNaN(a.value) || isNaN(b.value))
                {
                    if (a.value < b.value)
                    {
                        return -1;
                    }

                    if(a.value > b.value)
                    {
                        return 1;
                    }

                    return 0;
                }
                else
                {
                    return a.value - b.value;
                }
            });

            this.options0 = this.options0.concat([]);
        }

        if(this.isSelect1)
        {
            this.options1.sort(function(a, b)
            {
                if(isNaN(a.value) || isNaN(b.value))
                {
                    if (a.value < b.value)
                    {
                        return -1;
                    }

                    if(a.value > b.value)
                    {
                        return 1;
                    }

                    return 0;
                }
                else
                {
                    return a.value - b.value;
                }
            });

            this.options1 = this.options1.concat([]);
        }
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.options.setItems(value);

            for(let key in value)
            {
                let item = value[key];
                this.removeSelectedOption(item);
            }
        }
    }

    removeSelectedOption(option:any)
    {
        /* REMOVE OPTIONS0 */
        let i = 0;
        for(let key in this.options0)
        {
            let opt = this.options0[key];

            if(opt.value == option[0].value)
            {
                this.options0.splice(i, 1);
                this.options0 = this.options0.concat([]);
            }

            i++;
        }

        /* REMOVE OPTIONS1 */
        let j = 0;
        for(let key in this.options1)
        {
            let opt = this.options1[key];

            if(opt.value == option[1].value)
            {
                this.options1.splice(j, 1);
                this.options1 = this.options1.concat([]);
            }

            j++;
        }
    }

    onAdd()
    {
        console.log(this.items);

        if(this.items.length == 2 && this.items[0] && this.items[1])
        {
            this.removeSelectedOption(this.items);
            this.options.push(this.items);
            this.items = [];

            if(this.select0)
            {
                this.select0.clear();
            }

            if(this.select1)
            {
                this.select1.clear();
            }

            if(this.text0)
            {
                this.text0.nativeElement.value = '';
            }

            if(this.text1)
            {
                this.text1.nativeElement.value = '';
            }

            this.updateValue();
        }
    }

    onSelectChange(key:any, option:any)
    {
        this.option[key] = option;
    }

    onTextBlur(key:any, event:any)
    {
        this.option[key] = {
            value : event.target.value,
            label : event.target.value,
        };
    }

    onDeselected(key:any)
    {
        this.option[key] = false;
    }

    onRemove(item:any, i:any)
    {
        this.options.splice(i, 1);

        if(this.isSelect0)
        {
            this.options0.push(item[0]);
            this.options0 = this.options0.concat([]);
        }

        if(this.isSelect1)
        {
            this.options1.push(item[1]);
            this.options1 = this.options1.concat([]);
        }

        this.sort();
        this.updateValue();
    }

    updateValue()
    {
        let itens = [];

        for(let key in this.options)
        {
            itens.push(this.options[key]);
        }

        if(itens.length > 0)
        {
            this.setValue(itens);
        }
        else
        {
            this.setValue(null);
        }
    }
}
