import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { InputValidate } from "../../../validate/input.validate";

@Component({
    selector      		: ".cpf-cnpj-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(set)="onSet($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"
									autocomplete="false"
									[ngModel]="value"
									[mask]="mask"
									[ngModelOptions]="{standalone: true}"
									(keyup)="onInput($event)"
									[placeholder]="field.placeholder"/>

								<icon-input [isLoading]="isLoading"
											[icon]="field.getIcon()">
								</icon-input>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('cpf')">{{'CPF inválido' | translate}}</span>
								<span *ngIf="hasError('cnpj')">{{'CNPJ inválido' | translate}}</span>
							</error-input>
						</div>`
})
export class CPFCNPJInput extends BaseTextInput
{
    public mask : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.mask = "000.000.000-00||00.000.000/0000-00"
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col cpf-cnpj-field ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }

        this.asyncValidators = [ InputValidate.search(this.field, this.form, this) ];
    }
}
