import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Validators } from "@angular/forms";
import { AlertController, ModalController, ToastController } from "@ionic/angular";

/* FIELD */
import { BaseInput }       from "../base.input";
import { Form }            from "../../../model/form/form";
import { BaseList }        from "../../../model/base.list";
import { Grid }            from "../../../model/grid/grid";
import { FormFormPopover } from "../../../../core/popover/form.form/form.form.popover";
import { DynamicList }     from "../../../../core/dynamic/list/dynamic.list";
import { FieldType }       from "../../../../core/type/field/field.type";
import { Viewer }          from "../../../model/viewer/viewer";
import { ViewerModal }     from "../../../modal/viewer/viewer.modal";
import { BaseModel }       from "../../../model/base.model";
import { InputValidate } from "../../../validate/input.validate";
@Component({
    selector        : ".sub-form-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
									'ng-valid' 	: formControl?.valid && this.hasValue(), 
									'ng-invalid'   : !formControl?.valid,
									'hidden'       : hidden,
									'inline'   	: inline }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (add)="onAdd()"
                                (edit)="onEdit($event)">
                            </div>
                            <div class="item-input" [formGroup]="formGroup">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<form dynamic-form
									#dynamicForm
									*ngIf="!openModal"
									[data]="selected"
									[form]="subForm"                      
									(add)="doAdd($event)"
									(set)="doSet(selected, $event)"
									(close)="doCancel()"
									class="form-form">
								</form>

                                <div dynamic-list
                                     #dynamicList  										 							                              
                                     [grid]="subGrid"
                                     [acl]="form.instance.acl"
                                     [fixedHeader]="false"
                                     [collection]="collection"
                                     (add)="onAdd()"
                                     (set)="onSet($event)"
                                     (del)="onDel($event)"
                                     (viewer)="onViewer($event)"
                                     (collectionChange)="onCollectionChange();">
                                </div>

							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('noSave')">{{field.label}} Você precisa salvar os dados para continuar</span>
                            </error-input>
                        </div>`,
})
export class SubFormInput extends BaseInput
{
    @ViewChild("dynamicList", { static : true }) dynamicList : DynamicList; 

    public subForm     			: any;
    public collection  			: any;
    public subGrid        	: any;
    public selected        	: any;
    public inputAdd     		 = true;
    public openModal     		 = true;
    public inline     			 = false;	
    public editForm     		  = false;	
    public limite     			  = 50;
    public pathId     			  = "";
    public timeCollectionChange : any;
    public popover : any;
    public addText  = "Adicionar";
    public setText  = "Salvar";

    constructor(
        public alertController   : AlertController,
        public modalController   : ModalController,
        public toastController   : ToastController,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.collection = new BaseList();

        /* GRID */
        if (formItem.grid && formItem.grid.items && formItem.grid.items.length > 0)
        {
            for (const key in formItem.grid.items)
            {   
                formItem.grid.items[key].row = parseInt(key);
            }
            
            this.subGrid = formItem.grid;
        }
        else
        {
            this.subGrid = new Grid({
                items : this.formItem.items.getColumns(),
                type  : Grid.TABLE_TYPE,
            });    
        }				

        if (this.setting.hasSetButton !== undefined)
        {
            this.subGrid.hasSetButton = this.setting.hasSetButton;
        }
        else
        {
            this.subGrid.hasSetButton = true;
        }

        if (this.setting.hasDelButton !== undefined)
        {
            this.subGrid.hasDelButton = this.setting.hasDelButton;
        }
        else
        {
            this.subGrid.hasDelButton = true;
        }

        if (this.setting.showNoItems !== undefined)
        {
            this.subGrid.showNoItems = this.setting.showNoItems;
        }

        if (this.setting.orderBy !== undefined)
        {
            this.subGrid.orderBy = this.setting.orderBy;
        }

        if (this.setting.inputAdd !== undefined)
        {
            this.inputAdd = this.setting.inputAdd;
        }
		
        if (this.setting.openModal !== undefined)
        {
            this.openModal = this.setting.openModal;
        }

        if (this.setting.limite !== undefined)
        {
            this.limite = this.setting.limite;
        }

        if (this.setting.addText !== undefined)
        {
            this.addText = this.setting.addText;
        }

        if (this.setting.setText !== undefined)
        {
            this.setText = this.setting.setText;
        }

        if (this.setting.pathId !== undefined)
        {
            this.pathId = this.setting.pathId;
        }

        this.subGrid.hasAddButton    = this.editable && this.openModal && this.inputAdd;
        this.subGrid.fixedHeader     = false;
        this.subGrid.hasSetButton    = this.editable && this.subGrid.hasSetButton;
        this.subGrid.hasDelButton    = this.editable && this.subGrid.hasDelButton;
        this.subGrid.hasViewerButton = true;
        //this.subGrid.disabledButtons = !this.editable;

        //this.doSort();

        /* UTILIZADO PARA O SUBFORM TER ACESSO AS CONTROLLERS DO FORM */
        //for(const key in this.formItem.items)
        //{
        //this.formItem.items[key].field.subFormGroup = this.field.formGroup;
        //}

        this.nameClass += this.formItem.name + "-input input col sub-form-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        if (!this.openModal)
        {
            this.creatForm();
        }		
    }

    createValidators()
    {
        this.validators      = [];
        this.asyncValidators = [];
        
        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        }

        this.asyncValidators.push(InputValidate.custom(this.field, this.form, this));
    }

    creatForm()
    {
        /* FORM */
        this.subForm = new Form({
            name       	    : this.formItem.label,
            items      	    : this.formItem.items.copy(),
            viewEdit   	    : true,
            parent     	    : this.form,
            saveFixed  	    : true,     
            root       	    : this.formGroup.root,
            hasSubmitButton : !(!this.editable || this.readonly),
            hasCancelButton : this.openModal,
            hasHeader       : this.openModal,
            setting    	    : {
                addText : this.addText,
                setText : this.setText,
            }
        });
    }

    onCollectionChange() //????
    {
        /*if(this.timeCollectionChange)
		{
			clearTimeout(this.timeCollectionChange);
		}

		this.timeCollectionChange = setTimeout(() => 
		{
			this.setValue(this.collection.parseData());
			this.collection.dispatchUpdate(); // ATUALIZA A GRID			
		}, 300);	*/
    }

    validateSave()
    {
        if(this.editForm)
        {
            this.setError('noSave');

            return false;
        }

        return true;  
    }


    async onViewer(event:any)
    {
        this.popover = await this.modalController.create(
            {
                component      : ViewerModal,
                componentProps : {
                    viewer   : new Viewer({ items : this.formItem.items }),
                    document : event.data,				
                    onClose  : (() =>
                    {
                        this.popover.dismiss();
                    }),
                },
                cssClass : "full",
            });

        this.popover.present();
    }

    /* FIELD */
    async onOpenSubForm(data?:any)
    {
        this.creatForm();

        this.popover = await this.modalController.create(
            {
                component : FormFormPopover,
                componentProps :
            {
                data  : data,
                form  : this.subForm,
                acl   : this.form.instance.acl,
                onAdd : ((event:any) =>
                {
                    this.doAdd(event);
                }),
                onSet : ((event:any) =>
                {
                    this.doSet(data, event);
                }),
                onClose : (() =>
                {
                    this.popover.dismiss();
                }),
            },
                cssClass : "full",
            });

        this.popover.present();
    }

    normalizeCollection(collection:any)
    {
        for (const item of collection)
        {
            delete item.colid;
            delete item._steps;
            delete item.attachments;
            delete item.owner;
            delete item.user;
            //delete item.id;
        }

        return collection;
    }

    async doAdd(event:any)
    {
        this.editForm = false;
        
        const data = event.data;		
        data.owner = this.core().user ? this.core().user.reference : null; // PRECISO DO LOG DE QUEM ALTEROU
        data.user  = this.core().user ? this.core().user.reference : null; // PRECISO DO LOG DE QUEM ALTEROU

        if (!data.id)
        {
            if (this.pathId)
            {
                const model = new BaseModel(data);   
                data.id     = await model.getProperty(this.pathId);
            }   
            else
            {
                data.id = this.core().util.randomString(20);
            }                     
        }

        if (!this.collection.has(data))
        {
            this.collection.set(data);

            this.doSort().then(() => 
            {
                this.dirty = true; // SE ADICIONAR NÃO PRECISA DE ONCHANGE

                this.setValue(this.normalizeCollection(this.collection.parseData()));

                // FECHA O NAO FIXO
                if (!this.field.fixed)
                {
                    this.doCancel();

                    if (this.openModal && this.popover)
                    {
                        this.popover.dismiss();
                    }					
                }
                else
                {
                    this.toastController.create({
                        message  : "Item adciionado!",
                        duration : 500,
                        color    : "dark",
                        position : "top",
                    }).then((toast) => 
                    {
                        toast.present();
                    });
                }
            });
        }      
        
        this.displayControlByValue(data);
    }

    doSet(data, event:any)
    {
        this.editForm = false;

        const data2 = Object.assign({}, event.data);		
        data.user   = this.core().user; // PRECISO DO LOG DE QUEM ALTEROU

        if (!data2.id)
        {
            data2.id = data.id;
        }
        
        // NO CASO DE TROCA DE ID
        if(data.id != data2.id)
        {
            this.collection.del(data);
        }

        this.collection.set(data2);

        this.doSort().then(() => 
        {
            this.setValue(this.normalizeCollection(this.collection.parseData()));
            this.doCancel();

            if (this.openModal && this.popover)
            {
                this.popover.dismiss();
            }			
        });		

        this.displayControlByValue(data2);
    }

    doCancel()
    {
        this.selected = null;
    }

    /* OVERRIDE */
    setLog(value:any)
    {
        const control = this.getFormControl("_logs");

        if (control && value)
        {
            let valueLog = control.value;

            if (!valueLog || valueLog == "")
            {
                valueLog = {};
            }
			
            const logs = [];

            value.forEach(item => 
            {
                logs.push(item._logs);	
                delete item._logs;
            });

            valueLog[this.formItem.name] = logs;

            console.log("valueLog", logs, valueLog);

            control.setValue(valueLog);
        }
    }

    setCollection(list:any)
    {
        this.collection.clear();
        this.collection.setItems(list);

        this.setValue(this.normalizeCollection(this.collection.parseData()));

        this.markForCheck();
    }

    onAdd()
    {
        if (this.openModal)
        {
            if (this.collection.length > this.limite - 1)
            {
                this.alertController.create({
                    header  : "Alerta",
                    message : "Quantidade Máxima de Registro Já Atingida para esse Cadastro",
                    buttons : [
                        {
                            text    : "Fechar",
                            handler : () =>
                            {
                                this.alertController.dismiss();
                            }
                        },						
                    ]
                }).then(alert =>
                {
                    alert.present();
                })
            }
            else
            {
                this.onOpenSubForm();
            }			
        }
    }

    /* GRID */
    onSet(event:any)
    {
        this.editForm = true;

        if (this.openModal)
        {
            this.onOpenSubForm(event.data);
        }
        else
        {
            this.selected = event.data;
        }        
    }

    onDel(event:any)
    {
        this.alertController.create({
            header  : "Alerta",
            message : "Deseja remover esse item?",
            buttons : [
                {
                    text    : "Fechar",
                    handler : () =>
                    {
                        this.alertController.dismiss();
                    }
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        this.collection.del(event.data);
                        this.setValue(this.normalizeCollection(this.collection.parseData()));
                        this.alertController.dismiss();
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        })
    }

    reset()
    {
        if (!this.field.fixed)
        {
            this.updateValue(null);
			
            if (this.collection)
            {
                this.collection.clear();
            }			
        }

        super.reset();
    }


    /* METODO USADO PARA CHAMADAS EXTERNAS */
    populate(list:any)
    {
        this.setCollection(list);
    }

    siblingClear()
    {
        //this.updateValue(this.valueDefault());
    }
    
    /*onSet(event)
    {
        console.log('----->', this.field, event.data);

        //this.formItem.items = event.data.items;
        //event.data = this.field;
        //console.log('event.data', event.data);

        super.onSet(event);
    }*/

    onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if (value && !this.dirty)
        {	
            this.dirty = true;		
            this.collection.setItems(value);
			
            //this.collection.populate(value.map((x:any) => Object.assign({}, x)));     
			
            this.collection.on().then(() => 
            {
                //console.error(this.collection);
				
                this.doSort().then(() => 
                {
                    /* EVENT */
                    if (this.field.onInit)
                    {
                        this.field.onInit({
                            data : this.getValue()
                        });
                    }
                });
            });                         
        }
    }

    doSort()
    {   
        return new Promise((resolveFull:any) => 
        {
            if (this.subGrid.orderBy && this.collection)
            {                            
                this.collection.sortAsync((item) => 
                {
                    return new Promise((resolve) => 
                    {	
                        let isSimple = true;
                        let orderBy  = this.subGrid.orderBy;

                        if (typeof this.subGrid.orderBy == "object")
                        {
                            orderBy  = this.subGrid.orderBy._path;
                            isSimple = true;
                        }

                        item.getPathValue(orderBy).then((value2:any) => 
                        {                        
                            if (!value2 || isSimple)
                            {
                                resolve(value2);
                            }
                            else if (this.subGrid.orderBy.field.type.value == FieldType.type("Date").value)
                            {
                                if (this.subGrid.orderBy._setting && this.subGrid.orderBy._setting.onlyDay)
                                {
                                    resolve(value2.toDate().getDate());
                                }
                                else
                                {
                                    resolve(value2);
                                }
                            }
                            else
                            {
                                resolve(value2);
                            }  
                        })   
                    });
                })
                .then(() => 
                {
                    resolveFull();
                });
            } 
            else
            {
                resolveFull();
            }
        });
    }

    destroy()
    {
        super.destroy();

        this.dynamicList.destroy();
    }
	
    valueDefault()
    {
        return [];
    }

    displayControlByValue(data:any)
    {
        if (this.isControlField)
        {
            this.displayFields(this.controlField, false); /* DESABILITA TUDO */ 
            
            // if (this.controlPath && this.selected)
            // {
            //     const model = new Document(this.selected);
				
            //     model.on().then(() => 
            //     {
            //         model.getProperty(this.controlPath).then((value:string) => 
            //         {
            //             if (this.setting && this.setting[value] != undefined)
            //             {
            //                 const field = this.setting[value];
			
            //                 this.displayFields(field, true); /* HABILITA FIELD */
            //             }
			
            //             //OLD this.controlField = data.value;
            //             //this.displayFields(this.controlField, true); /* HABILITA O ATUAL */
			
            //             this.markForCheck();        
            //         });
            //     });                
            // }
        }
    }

    //addField(field:Field):any
    //{
    //this.container.addField(field);
    //}
}
