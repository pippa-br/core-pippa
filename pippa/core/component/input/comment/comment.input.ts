import { Component, ChangeDetectionStrategy, ChangeDetectorRef, HostBinding } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { BaseTextInput }  from '../base.text.input';
import { MenuCollection } from '../../../model/menu/menu.collection';
import { MenuPlugin } 	  from '../../../plugin/menu/menu.plugin';
import { FormPlugin } 	  from '../../../plugin/form/form.plugin';

@Component({
    selector 		: `.comment-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : (formControl?.valid), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<ion-list class="child-comments" *ngFor="let child of _children">
									<ion-item>
										<ion-avatar slot="start">
											<img *ngIf="child.owner" src="{{child.owner | property : 'photo._url' : './assets/img/avatar.png' | async}}">
										</ion-avatar>
										<ion-label>
											<h2>{{child.owner | property : 'name' | async }}</h2>
											<h3>{{child.postdate | dateFormat : 'dd/MM/yyyy HH:ss'}}</h3>
											<div class="message" [innerHtml]="child.message"></div>									
										</ion-label>
										<ion-button class="del-button" *ngIf="child.isOwner()" (click)="onDelChild(child)">
											<ion-icon name="trash-outline"></ion-icon>
										</ion-button>								
									</ion-item>
								</ion-list>
								<a class="comment-button" (click)="onOpen()">
									Comentar 
									<ion-icon *ngIf="!open" name="caret-up-outline"></ion-icon>
									<ion-icon *ngIf="open" name="caret-down-outline"></ion-icon>
								</a>
								<form *ngIf="open">
									<textarea
										autosize
										autocomplete="false"
										[(ngModel)]="value"
										[ngModelOptions]="{standalone: true}"
										[placeholder]="'Escreva seu comentário'"></textarea>
									<ion-button color="medium" (click)="onAddChild(value)">Comentar</ion-button>
								</form>

								<error-input [control]="formControl" [submitted]="submitted">
								</error-input>
							</div>
						</div>`
})
export class CommentInput extends BaseTextInput
{
	public _children : any
	public open 	 : boolean = false;
	public value     : string;

    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public alertController   : AlertController,
		public menuPlugin        : MenuPlugin,
		public formPlugin        : FormPlugin,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col comment-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

		const params = this.createParams({
			appid : formItem.appid || 'comment',
		});

		const result = await this.formPlugin.getData(params);

		for(let key in result.collection)
		{
			const form = result.collection[key];

			if(form.id != 'master')
			{						
				form.hasHeader = false;
				form.hasCancelButton = false;
				form.setting = {
					setText : 'Comentar'
				}
				this.form = form;

				break;		
			}
		}	
	}
	
	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

	onOpen()
	{
		this.open = !this.open;
	}

	async onAddChild(value:string)
	{		
		if(value)
		{			
			this.open = false;
	
			if(!this._children)
			{
				this._children = new MenuCollection();
			}
	
			const data = await this.form.parseToData({
				message : value,
				code    : this.data.code,
				_level  : 2,
			});

			const data2 = await this.menuPlugin.add(this.data.appid, this.form, { data : data})
	
			this._children.add(data2);
	
			this.data.set({
				_children : this._children.getData()
			});

			this.setValue(this._children.getData());

			console.error(this.data);
			
			value = '';
			this.markForCheck();
		}		
	}

	async onDelChild(child:any)
	{
		const alert = await this.alertController.create({
			header  : 'Atenção!',
			message : 'Deseja remover esse comentário?',
			buttons : [
				{
					text: 'Cancelar',
					role: 'cancel',
					cssClass: 'secondary',
				}, 
				{
					text: 'Sim',
					handler: () => 
					{						
						this._children.del(child);
						
						this.data.set({
							_children : this._children.getData()
						});

						this.setValue(this._children.getData());

						this.menuPlugin.del(child).then(() => 
						{
							this.markForCheck();
						});						
					}
				}
			]
		});		 

		await alert.present();
	}

	async onModelChange(value : any)
    {
        if(value && !this.dirty)
        {
			this.dirty = true;
			const _children = new MenuCollection(value);
			await _children.on();

			this._children = _children;
			this.markForCheck();

			console.error(value);
        }
    }
}
