import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { BaseModel }          from "../../../model/base.model";
import { BaseList }           from "../../../model/base.list";
import { CdkDragDrop, CdkDragEnter, moveItemInArray } from "@angular/cdk/drag-drop";
import { AlertController } from "@ionic/angular";

@Component({
    selector        : ".product-multi-select-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit()"
                                (del)="onDel()">
                            </div>
                            <div class="item-input"
                                [formGroup]="field.formGroup"
                                [ngClass]="{'has-add' : inputAdd }">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ng-select #selectDropdown
                                        [items]="items"
										[loading]="loading"
										appendTo="body"
                                        [multiple]="false"
                                        [hideSelected]="false"
                                        [clearable]="false"
                                        [closeOnSelect]="false"
                                        notFoundText="Item não encontrado"                                        
                                        [placeholder]="formItem.label"
                                        [bindLabel]="bindLabel"
                                        [bindValue]="bindValue"
                                        [groupBy]="groupBy"
                                        [selectableGroup]="true"
                                        cancelText="Fechar"
										okText="Selecionar"
										(close)="onClose()"
										(search)="onSearch($event)"
                                        (change)="onInput($event)">	                                              

                                    <ng-template ng-label-tmp let-item="item" let-clear="clear">
                                        <span class="ng-value-icon left" (click)="clear(item)" aria-hidden="true">×</span>
                                        <img (click)="addDocument(item)" width="30" height="30" [src]="getUrl(item)"/>
                                        <span (click)="addDocument(item)">{{item[bindLabel]}}</span>
                                    </ng-template>

                                    <ng-template ng-option-tmp let-item="item" let-index="index" let-search="searchTerm">
										<div class="title-option">
                                            <img (click)="addDocument(item)" width="30" height="30" [src]="getUrl(item)"/>
                                        	<span>{{item[bindLabel]}}</span>
										</div>
                                    </ng-template>

                                </ng-select>                                
                                <icon-input *ngIf="inputAdd && editable"
                                            [isLoading]="isLoading"
											class="add-icon"
                                            [icon]="addIcon"
                                            (click)="addDocument()">
                                </icon-input>
                            </div>
                            <p class="sort-loading" *ngIf="selectedSort && isLoading">Carregando...</p>
                            <div class="sort-container" cdkDropListGroup *ngIf="selectedSort">                                
                                <div
                                    *ngFor="let item of itemsSelected; let i = index"
                                    cdkDropList
                                    cdkDropListOrientation="horizontal"
			                        [cdkDropListData]="{item:item,index:i}" (cdkDropListDropped)="drop($event)">
                                    <div cdkDrag class="sort-box">                                        
                                        <img [src]="getUrl(item)"/>
                                        <label>{{item[bindLabel]}}</label>
                                        <ion-icon name="close-circle-outline" (click)="delInput(item)"></ion-icon>
                                    </div>
                                </div>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class ProductMultiSelectInput extends BaseReferenceInput
{
    @ViewChild("selectDropdown") selectDropdown;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
        public alertController   : AlertController,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col product-multi-select-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";        
    }

    getUrl(file:any)
    {        
        if (file.indexes && file.indexes.thumb)
        {
            let url = file.indexes.thumb._480x480 || file.indexes.thumb._300x300 || file.indexes.thumb._url;
            url     = url.replace(".webp", ".jpeg");

            return url;    
        }
    }

    createValidators()
    {
        super.createValidators();
		
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
        } 
    }

    //https://stackblitz.com/edit/angular-drag-drop-flex-wrap?file=src%2Fapp%2Fapp.component.html,src%2Fapp%2Fapp.component.ts
    //https://stackblitz.com/edit/angular-cdk-drag-drop-sortable-flex-wrap?file=src%2Fapp%2Fapp.component.ts
    async drop(event: CdkDragDrop<any>) 
    {
        const fromIndex = event.previousContainer.data.index;
        let toIndex     = event.container.data.index;

        if (fromIndex > toIndex)
        {
            toIndex = toIndex + event.currentIndex;
        }
        else
        {
            toIndex = toIndex + (event.currentIndex == 0 ? -1 : 0);
        }

        moveItemInArray(this.selected,      fromIndex, toIndex);
        moveItemInArray(this.itemsSelected, fromIndex, toIndex);

        this.setValue(this.selected);
    }

    async mergeSelected()
    {
        if (this.selected && this.collection)
        {
            /* VERIFICA SE JA EXISTE */
            const selecteds = [];
            const promises  = [];

            for (const item of this.selected)
            {
                const model = new BaseModel(item);
                promises.push(model.on());                
            }

            const values = await Promise.all(promises);

            for (const value of values)
            {
                this.collection._set(value);
            }
        }
    }

    /* METODO USADO PARA CHAMADAS EXTERNAS */
    populate(list:any)
    {
        return new Promise<void>(async (resolve) =>
        {
            if (list == null)
            {
                this.selected = null;
            }
            else
            {    
                this.selected = list;
            }
    
            // UPDATE VALUES
            if (list == undefined)
            {
                this.updateValue([]);
            }
            else
            {
                const references = [];

                for (const key in list)
                {
                    const item = list[key];
                    references.push(item.reference);
                }

                this.updateValue(references);								
            }

            resolve();
        });           
    }

    onInput(item:any)
    {
        if (item)
        {
            if (!this.selected)
            {
                this.selected = [];
            }
    
            let hasItem = false;
    
            for (const item2 of this.selected)
            {
                if (item.id == item2.id)
                {
                    hasItem = true;
                }
            }
    
            if (!hasItem)
            {
                this.selected.unshift(item);
            }        
    
            this.setValue(this.selected);            
        } 
    }

    onClose()
    {
        super.onClose();
        this.selectDropdown.handleClearClick();
    }

    async delInput(item:any)
    {
        if (item)
        { 
            const alert = await this.alertController.create({
                header  : "Atenção!",
                message : "Deseja remover esse item?",
                buttons : [
                    {
                        text     : "Cancelar",
                        role     : "cancel",
                        cssClass : "secondary",
                    }, 
                    {
                        text    : "Sim",
                        handler : () => 
                        {			                        
                            if (!this.selected)
                            {
                                this.selected = [];
                            }
                        
                            for (const key in this.selected)
                            {
                                if (item.id == this.selected[key].id)
                                {
                                    this.selected.splice(key, 1);
                                }
                            }
                
                            this.setValue(this.selected);                          			
                        }
                    }
                ]
            });

            await alert.present();    
        }         		    
    }

    /* OVERRIDE */
    async doAdd(data:any)
    {
        if (!this.selected)
        {
            this.selected = [];
        }

        this.selected.push(data);
        await this.doLabels();

        this.onInput(this.selected);
    }

    /* OVERRIDE */
    async doSet(data:any)
    {
        if (!this.selected)
        {
            this.selected = [];
        }

        this.selected.push(data);
        await this.doLabels();

        this.onInput(this.selected);
    }

    async onModelChange(items:any)
    {
        /* TRATAR O VALOR DO BIND */
        if (items && Object.keys(items).length > 0 && !this.dirty)
        {
            const _items = []; // POR CONTA DE TYPEOBJECT SER SALVO COMO OBJECT			

            for (const key in items)
            {
                if (key !== "_dataReferences")
                {
                    _items.push(items[key]);	
                }
            }

            this.selected = _items; // SEMPRE SÃO REFERENCIAS

            /* INVERSED BY */
            this.updateInversedBy(items, "inversedBy");

            /* INIT INVERSED BY */
            this.updateInversedBy(items, "initInversedBy");

            await this.doLabels();
            await this.doSelectedLabels();

            this.dirty = true;
        }
    }

    valueDefault()
    {
        return [];
    }
}
