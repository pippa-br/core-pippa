import { Component, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { ActionSheetController, AlertController } from "@ionic/angular";
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import * as loadImage from "blueimp-load-image"

/* PIPPA */
import { BaseInput }        from "../base.input";
import { FileCollection }   from "../../../../core/model/file/file.collection";
import { UploadPlugin }     from "../../../../core/plugin/upload/upload.plugin";
import { ComentImageModal } from "../../../../core/modal/coment.image/coment.image.modal";
import { ModalController }  from "../../../../core/controller/modal/modal.controller";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";

@Component({ 
    selector        : "[video-upload-input]",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper" 
											[ngClass]="{ 'ng-submitted' : submitted, 
											'ng-valid' 	: formControl?.valid && this.hasValue(), 
											'ng-invalid'   : !formControl?.valid,
											'hidden'       : hidden,
											'no-editable'	: !editable,
											'boxes'		: boxes}">
                            <div header-input 
                                 [form]="form" 
                                 [formItem]="formItem" 
                                 (edit)="onEdit($event)" 
                                 (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)" />
                                
                                <input #fileInput
                                        type="file"
                                        style="display: none;"
                                        [multiple]="multiple"
                                        [accept]="accept"
                                        (change)="onUpload($event)" />

								<input #fileInput2
                                        type="file"
                                        style="display: none;"
                                        [multiple]="false"
                                        [accept]="'images/*'"
                                        (change)="onUpload2($event)" />		

								<table class="striped list" cdkDropList cdkDropListOrientation="{{dropListOrientation}}" (cdkDropListDropped)="drop($event)">
                                    <tr *ngFor="let file of files;let last = last;" cdkDrag [cdkDragDisabled]="dragDisabled">
                                        <td>
											<div [ngClass]="file.type">
												<a href="{{file._url}}" target="_blank">
													<span *ngIf="!showThumb || !file.isImage()">{{file.name}}</span>
													<img src="{{file._url}}" *ngIf="showThumb && file.isImage()">
												</a>
											</div>
                                        </td>
										<td class="buttons-col" *ngIf="editable">
											<div class="buttons-div">
												<ion-button class="add-button" *ngIf="last && multiple && !isMaxFiles()" (click)="onAdd()">
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
												<ion-button (click)="openModal(file)" *ngIf="hasComment">
													<ion-icon name="chatboxes-outline"></ion-icon>
												</ion-button>
												<ion-button class="set-button" (click)="onThumb(file)">
													<ion-icon name="image-outline"></ion-icon>
												</ion-button>
												<ion-button class="set-button" (click)="onSet(file)">
													<ion-icon name="create-outline"></ion-icon>
												</ion-button>
												<ion-button class="del-button" (click)="onRemove(file)">
													<ion-icon name="remove-outline"></ion-icon>
												</ion-button>												
											</div>
                                        </td>
                                    </tr>
									<tr *ngFor="let file of samples;let last = last;" cdkDrag [cdkDragDisabled]="dragDisabled">
										<td>
											<div [ngClass]="file.type" (click)="onSet(file)">
												<span>{{file.sample}}</span>
											</div>
                                        </td>
										<td class="buttons-col" *ngIf="editable">
											<div class="buttons-div">																								
												<ion-button class="set-button" (click)="onSet(file)">
													<ion-icon name="create-outline"></ion-icon>
												</ion-button>
											</div>
                                        </td>
                                    </tr>
                                    <tr class="add-button" *ngIf="files.length == 0 && editable && !isMaxFiles()" (click)="onAdd()">
										<td>
											<div class="no-items">
												<label>Adicionar {{label | translate}}</label>
											</div>											
										</td>
										<td class="buttons-col">
											<div class="buttons-div">
												<ion-button>
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="invalidUrl">Url Invalida!</span>
								<span *ngIf="invalidSize">Tamanho maior que {{maxSize}}MB!</span>
								<span *ngIf="invalidMaxFiles">Nº de arquivos maior que máximo permitido ({{maxFiles}})</span>
                            </error-input>
                        </div>
                        <attachment-input *ngIf="field.hasAttachments()" [field]="field" (attachment)="onAttachment($event)">
                        </attachment-input>`
})
export class VideoUploadInput extends BaseInput
{
@ViewChild("fileInput",  { static : false }) fileInput  : ElementRef;
@ViewChild("fileInput2", { static : false }) fileInput2 : ElementRef;

public files       		   : FileCollection = new FileCollection();
public samples       	   : FileCollection = new FileCollection();
public invalidUrl  		           = false;
public multiple    		           = false;
public hasComment  		           = false;
public accept      		            = "video/*";
public label       		            = "Upload";
public maxSize     		     		= 1;
public maxFiles     	     		= 999;
public invalidSize 		    		= false;
public invalidMaxFiles	    		= false;	
public boxes 	   		    		= false;
public showThumb   		    		= true;
public dragDisabled   	    		= false;	
public dropListOrientation   		= "vertical";
public selected 		   : any;
public video 		  	   : any;	

constructor(
public alertController   	 : AlertController,
public uploadPlugin          : UploadPlugin,
public modalController       : ModalController,
public changeDetectorRef     : ChangeDetectorRef,
public actionSheetController : ActionSheetController,
)
{
    super(changeDetectorRef);
}

async createControls(formItem:any, formGroup:any)
{
    await super.createControls(formItem, formGroup);

    this.nameClass += this.formItem.name + "-input input col upload-json-input ";
    this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

    /* MULTIPLE*/
    if (this.setting.multiple !== undefined)
    {
        this.multiple = this.setting.multiple;
    }

    /* HAS COMMENT */
    if (this.setting.hasComment !== undefined)
    {
        this.hasComment = this.setting.hasComment;
    }

    /* ACCEPT */
    if (this.setting.accept)
    {
        this.accept = this.setting.accept;
    }

    /* LABEL */
    if (this.setting.label)
    {
        this.label = this.setting.label;
    }
    else
    {
        this.label = this.field.label;
    }

    /* MAXSIZE */
    if (this.setting.maxSize)
    {
        this.maxSize = this.setting.maxSize;
    }

    /* SHOW THUMBS */
    if (this.setting.showThumb !== undefined)
    {
        this.showThumb = this.setting.showThumb;
    }

    /* MAX FILES */
    if (this.setting.maxFiles !== undefined)
    {
        this.maxFiles = this.setting.maxFiles;
    }

    /* drag Disabled */
    if (this.setting.dragDisabled !== undefined)
    {
        this.dragDisabled = this.setting.dragDisabled;
    }

    /* SAMPLES */
    this.createSamples();

    /* BOXES */
    if (this.setting.boxes)
    {
        this.boxes               = this.setting.boxes;
        this.dropListOrientation = "horizontal";
    }

    this.accept += ";capture=camera";
}

createSamples()
{		
    if (this.setting.samples !== undefined)
    {
        this.samples  = new FileCollection();
        const samples = this.setting.samples.split(",");

        for (const name of samples)
        {
            if (!this.files.hasSample(name))
            {
                this.samples.setItem({
                    type   : "sample",
                    sample : name,
                });	
            }				
        }
    }
}

createValidators()
{
    this.validators = [];

    if (!this.hidden)
    {
        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
    }
}

drop(event: CdkDragDrop<string[]>) 
{
    moveItemInArray(this.files, event.previousIndex, event.currentIndex);
    this.updateFiles();
}

onAdd()
{
    this.fileInput.nativeElement.click();
}

onThumb(file:any)
{
    this.video = file;
    this.fileInput2.nativeElement.click();
}

onSet(item:any)
{
    this.selected = item;
    this.onAdd();
}

isMaxFiles()
{
    return this.files.length + this.samples.length == this.maxFiles;
}

async uploadMultipleExternal(inputs:any, resize?:boolean)
{
    const promises : any = [];

    for (const key in inputs)
    {
        const item = inputs[key];

        if (typeof item === "object")
        {
            promises.push(this.uploadFromInputTempExternal(item));
        }
    }

    const results : any = await Promise.all(promises);

    const files = new FileCollection();

    for (const result of results)
    {
        files.add(result.data);
    }

    console.log("uploadMultiple", files);

    return files;
}

async uploadFromInputTempExternalOld(file:any)
{
    await this.core().loadingController.start();

    // CREATE FILE
    const result = await this.core().api.callable("https://node-binext-c7qans2swq-rj.a.run.app/storageV2/uploadVideo", 
        {
            maccid    		 : this.core().masterAccount.code,
            accid     		 : this.core().account.code,
            originalname : file.name,
            folder       : "videos",
        });

    //let formData : FormData = new FormData();
    //formData.append('--upload-file', input, input.name);

    const headers = new Headers();
    headers.append("Content-Type", file.type);

    /* SAVE FILE */
    await this.core().api.http.put(result.data.endpoint, file, { headers }).toPromise();
    delete result.data.endpoint;

    // PUBLIC FILE
    // await this.core().api.callable(Types.UPLOAD_MAKE_PUBLIC_DOCUMENT_API, 
    // {
    // 	maccid 	: this.core().masterAccount.code,
    // 	accid  	: this.core().account.code,
    // 	data 	: result.data,
    // });

    this.core().loadingController.stop();		

    return result

    // await this.core().loadingController.start();

    // let formData : FormData = new FormData();
    // formData.append('file', file, file.name);
    // // formData.append('form', JSON.stringify({ 				
    // // 	referencePath : form.reference.path, 
    // // }));
    // formData.append('maccid', 	 this.core().masterAccount.code);
    // formData.append('accid', 	 this.core().account.code);
    // formData.append('appid', 	 'apps');
    // formData.append('platform',  this.core().platform);
    // formData.append('colid', 	 'documents');
    // //formData.append('parseJson', 'form');
    // formData.append('fields',    'maccid,accid,appid,colid,platform');

    // // CREATE FILE
    // const result = await this.core().api.callFormData('https://node-binext-c7qans2swq-rj.a.run.app/storageV2/uploadVideo', formData);		

    // this.core().loadingController.stop();

    return result
}

async uploadFromInputTempExternal(file:any)
{
    await this.core().loadingController.start();

    const formData : FormData = new FormData();
    formData.append("file", file, file.name);
    formData.append("mapItems", JSON.stringify({ 				
        referencePath : environment.defaultMapItems,
    }));
    formData.append("maccid", 	 this.core().masterAccount.code);
    formData.append("accid", 	 this.core().account.code);
    formData.append("appid", 	 "apps");
    formData.append("platform",  this.core().platform);
    formData.append("colid", 	 "documents");
    //formData.append('parseJson', 'form');
    formData.append("fields",    "maccid,accid,appid,colid,platform,mapItems");

    // CREATE FILE
    const result = await this.core().api.callFormData(Types.UPLOAD_VIDEO_API, formData);		

    this.core().loadingController.stop();

    return result
}

onUpload(event:any)
{
    if (event.target.files.length + this.isMaxFiles() <= this.maxFiles || this.selected)
    {
        this.invalidSize = false;

        if (this.validateSizes(event.target.files))
        {
            if (this.multiple && !this.selected)
            {
                this.uploadMultipleExternal(event.target.files).then((files:any) =>
                {
                    this.files.setItems(files);

                    this.updateFiles();

                    this.markForCheck();
                    this.clear();
                });
            }
            else
            {
                this.singleUpload(event.target.files[0]);
            }
        }  
        else
        {			
            this.invalidSize = true;
            this.clear();
        }    
    }
    else
    {
        this.invalidMaxFiles = true;
    }		  
}

onUpload2(event:any)
{
    if (event.target.files.length + this.isMaxFiles() <= this.maxFiles || this.selected)
    {
        this.invalidSize = false;

        if (this.validateSizes(event.target.files))
        {
            if (this.multiple && !this.selected)
            {
                this.uploadMultipleExternal(event.target.files).then((files:any) =>
                {
                    this.files.setItems(files);

                    this.updateFiles();

                    this.markForCheck();
                    this.clear();
                });
            }
            else
            {
                this.singleUploadThumb(event.target.files[0]);
            }
        }  
        else
        {
            this.invalidSize = true;
            this.clear();
        }    
    }
    else
    {
        this.invalidMaxFiles = true;
    }		  
}

singleUpload(file:any)
{
    this.uploadMultipleExternal([ file ]).then((files:any) =>
    {
        if (this.hasComment)
        {
            this.openModal(files[0]);
        }
        else if (this.selected)
        {
            this.selected.populate(files[0]);
            this.files.setItem(this.selected);
            this.selected = null;

            // ATUALIZA OS SAMPLES
            this.createSamples();

            this.markForCheck();
            this.updateFiles();
        }					
        else
        {
            this.files.setItems(files);

            this.markForCheck();
            this.updateFiles();
        }

        this.clear();
    });
}

singleUploadThumb(file:any)
{
    this.uploadPlugin.uploadMultiple([ file ]).then((files:any) =>
    {
        if (this.hasComment)
        {
            this.openModal(files[0]);
        }
        else if (this.selected)
        {
            this.selected.populate(files[0]);
            this.files.setItem(this.selected);
            this.selected = null;

            // ATUALIZA OS SAMPLES
            this.createSamples();

            this.markForCheck();
            this.updateFiles();
        }
        else if (this.video)
        {
            this.video.thumb = files[0];
            this.files.setItem(this.video);
            this.video = null;

            // ATUALIZA OS SAMPLES
            this.createSamples();

            this.markForCheck();
            this.updateFiles();
        }	
        else
        {
            this.files.setItems(files);

            this.markForCheck();
            this.updateFiles();
        }

        this.clear();
    });
}

validateSizes(files:any)
{
    for (const file of files)
    {
        const mb = file.size / 1024 / 1024;

        if (mb > this.maxSize)
        {
            return false;
        }
    }

    return true;
}

clear()
{
    this.fileInput.nativeElement.value = "";
}

reset()
{
    this.files = new FileCollection();
    this.createSamples()
    this.dirty = false;
}

updateFiles()
{
    if (this.multiple)
    {
        this.setValue(this.files.parseData());
    }
    else if (this.files.length > 0)
    {
        this.setValue(this.files[0].parseData());
    }
    else
    {
        this.setValue("");
    }

    this.invalidUrl = false;
}

openModal(file: any)
{
    this.modalController.create(ComentImageModal,
        {
            file   : file,
            onSave : (file2 : any) =>
            {
                this.files.set(file2);
                this.updateFiles();
                this.modalController.dismiss();
            },
            onClose : () =>
            {
                this.modalController.dismiss();
            }
        }, true)
.then(modal =>
{
    modal.present();
});
}

/* CHAMADAS ESXTERNAS */
populate(value:any)
{
    this.files = new FileCollection(value);
    super.populate(value);
}

onRemove(item:any)
{
    this.alertController.create({
        header  : "Alerta",
        message : "Deseja remover esse item?",
        buttons : [
            {
                text    : "Fechar",
                handler : () =>
                {
                    this.alertController.dismiss();
                }
            },
            {
                text    : "Remover",
                handler : () =>
                {
                    this.files.del(item);
                    this.updateFiles();

                    // ATUALIZA OS SAMPLES
                    this.createSamples();
                }
            }
        ]
    })
.then(alert =>
{
    alert.present();
})        
}

onModelChange(value:any)
{
    if (value && !this.dirty)
    {
        if (typeof value == "string")
        {
            this.files = new FileCollection([ {
                name : value.replace(/^.*[\\\/]/, ""),
                _url : value
            } ]);
        }
        else if (this.multiple)
        {
            this.files = new FileCollection(value);
        }
        else
        {
            this.files = new FileCollection([ value ]);
        }

        // CADASTROS ANTIGOS SEM SAMPLES
        if (this.setting.samples !== undefined)
        {
            const samples = this.setting.samples.split(",");
            let i         = 0;

            for (const file of this.files)
            {
                if (samples[i])
                {
                    file.sample = samples[i];											
                }					

                i++;
            }
        }

        // ATUALIZA OS SAMPLES
        this.createSamples();

        this.dirty = true;
    }
}
}
