import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";

@Component({
    selector        : ".checkbox-object-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper ng-valid"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [onlyButtons]="true"
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input id="{{labelId}}"
                                        class="checkbox-item"
                                        type="checkbox"
                                        formControlName="{{formItem.name}}"
                                        [(ngModel)]="selected"
                                        (ngModelChange)="onModelChange($event)"
                                        (change)="onInput()">

                                <label for="{{labelId}}">{{field.label}}</label>

                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class CheckboxObjectInput extends BaseInput
{
    public labelId     = "";
    public selected   : any;
    public dataOption : any; 

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col checkbox-object-input " + (this.field.viewEdit ? "view-edit " : " ");
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
        this.labelId    = this.formItem.id + "-" + new Date().getTime();
    }

    createValidators()
    {
        if (this.setting.data)
        {
            this.dataOption = this.setting.data;
        }
    }

    onModelChange(value:any)
    {
        if (!this.dirty && value)
        {
            this.dirty = true;
            this.updateValue(value);
            this.selected = value;            
        }
    }

    onInput()
    {
        /* O EVENTO É DISPARADO E DEPOIS TROCA O VALOR POR ISSO ! */
        if (!this.getValue())
        {
            /* MULTI CHECK BOX SALVAR OBJETO */
            if (this.dataOption)
            {
                this.updateValue(this.dataOption);
                this.selected = this.dataOption;
            }
        }
        else
        {
            this.updateValue(this.valueDefault());
            this.selected = null;
        }
    }

    valueDefault()
    {
        return null;
    }
}
