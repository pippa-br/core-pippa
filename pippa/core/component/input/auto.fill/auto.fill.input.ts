import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
/* PIPPA */
import { BaseInput } from '../base.input';
import { BaseModel } from '../../../../core/model/base.model';
import { TDate } from '../../../util/tdate/tdate';

@Component({
    selector : `.auto-fill-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (edit)="onEdit()"
                     (del)="onDel()">
                </div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden"
                           formControlName="{{formItem.name}}"
                           (ngModelChange)="onModelChange($event)"/>
                    <input type="text" readonly [value]="selected?._label"/>
                </div>`
})
export class AutoFillInput extends BaseInput implements AfterViewInit
{
    public fieldFunction    : any;
    public changeReferences : any;
    public changeControls   : any;
    public selected         : any;
    public type             : string = 'function';
    public fieldName        : string;
    public interval         : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col auto-fill-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* LABEL FIELD */
        if(this.field.setting.fieldFunction)
        {
            this.fieldFunction = this.field.setting.fieldFunction;
        }

        /* CHANGE FIELD */
        if(this.field.setting && this.field.setting.changeReference)
        {
            this.changeReferences = this.field.setting.changeReference.split(',');
            this.changeControls   = [];

            for(const key in this.changeReferences)
            {
                this.changeControls[key] = this.getFormControl(this.changeReferences[key]);

                if(this.changeControls[key])
                {
                    this.changeControls[key].registerOnChange((value:any) =>
                    {
                        this.changeReference(value);
                    });
                }
            }
        }

        /* TYPE */
        if(this.field.setting && this.field.setting.type)
        {
            this.type = this.field.setting.type;
        }

        /* FIELD NAME */
        if(this.field.setting && this.field.setting.fieldName)
        {
            this.fieldName = this.field.setting.fieldName;
        }
    }

    changeReference(value:any)
    {
        this.selected = null;
        this.setValue(null);

        if(this.interval)
        {
            clearInterval(this.interval);
        }

        if(this.fieldFunction)
        {
            setTimeout(() =>
            {
                this.core().functions[this.fieldFunction]({
                    value : value,
                    data  : this.parseData(),
                })
                .then((data:any) =>
                {
                    this.selected = data;

                    this.setValue(data);
                });
            });
        }
        else if(this.type == 'date' && this.fieldName)
        {
            const model : any = new BaseModel(value)
            model.on().then(() =>
            {
                const date : any = new TDate().add(model[this.fieldName], 'seconds');
                date._label = date.format('dd/MM/yyyy HH:mm');;

                this.selected = date;
                this.setValue(date.toDate());

                this.interval = setInterval(() =>
                {
                    const date : any = new TDate().add(model[this.fieldName], 'seconds');
                    date._label = date.format('dd/MM/yyyy HH:mm');;

                    this.selected = date;
                    this.setValue(date.toDate());

                }, 1000);
            });
        }
    }

    /*onModelChange(value)
    {
        if(!this.isInit)
        {
            this.isInit = true;
            this.setValue(value);

            /* EVENT */
            /*if(this.field.onInit)
            {
                this.field.onInit(this.getValue());
            }
        }
    }*/
}
