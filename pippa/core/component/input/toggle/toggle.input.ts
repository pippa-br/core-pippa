import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseInput } from "../base.input";
@Component({
    selector        : ".toggle-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                 [form]="form"
                                 [formItem]="formItem"
                                 (edit)="onEdit($event)"
                                 (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
								
                                <ion-toggle [ngModel]="checked"
                                            [ngModelOptions]="{standalone: true}"
                                            [checked]="checked"
                                            [enableOnOffLabels]="true"
                                            [disabled]="indeterminate"
                                            (ionChange)="onInput($event)">
                                </ion-toggle>

                                <ion-checkbox *ngIf="hasIndeterminate" color="dark" [checked]="!indeterminate" (ionChange)="onIndeterminateChange($event)"></ion-checkbox>

                                <error-input [control]="formControl" [submitted]="submitted">
                                </error-input>
                            </div>
                        </div>`
})
export class ToggleInput extends BaseInput
{
    public checked           = false;
    public inversed          = false;
    public indeterminate     = false;
    public hasIndeterminate  = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
        
        this.nameClass += this.formItem.name + "-input input col toggle-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        if (this.setting.inversed !== undefined)
        {
            this.inversed = this.setting.inversed;
        }

        if (this.setting.hasIndeterminate !== undefined)
        {
            this.hasIndeterminate = this.setting.hasIndeterminate;
            
            // JÁ PARA COMECAR DESATIVADO
            this.indeterminate = true;
            this.setValue(null);            
        }
    }

    onInput(event:any)
    {
        if (event)
        {
            this.checked = event.detail.checked;
        }

        if (this.inversed)
        {
            this.setValue(!this.checked);
        }
        else
        {
            this.setValue(this.checked);
        }
    }

    async onModelChange(value:any)
    {
        if (!this.dirty)
        {
            //console.error('xxx', this.field.name, value);

            // if(this.indeterminate)
            // {
            //     this.dirty = true;
            //     this.setValue(null);
            //     await this.displayControlByValue(value);                
            // }
            if (this.hasIndeterminate)
            {
                this.dirty = true;

                if (value === null)
                {
                    this.setValue(null);
                    this.indeterminate = true;
                }
                else
                {
                    this.setValue(value);
                    this.indeterminate = false;
                    this.checked       = value;
                }

                //console.error('xxx', value);

                await this.displayControlByValue(value);
            } 
            else if (value === undefined || value === "")
            {
                this.dirty = true;
                this.setValue(false);
                await this.displayControlByValue(false);                
            }
                       
            else
            {
                this.checked = value;
                await this.displayControlByValue(value); 
            }    
        }        
    }

    async displayControlByValue(value:boolean)
    {
        if (this.isControl)
        {
            this.displayControl(false); /* DESABILITA TODOS */

            if (this.setting)
            {
                if (this.setting[1] && value)
                {
                    this.displayControlByNames(this.setting[1], true); /* HABILITA O ATUAL */					
                }
                else if (this.setting[0] && !value)
                {
                    this.displayControlByNames(this.setting[0], true); /* HABILITA O ATUAL */	
                }
            }
        }
    }

    onIndeterminateChange(event)
    {
        if (event)
        {
            this.indeterminate = !event.detail.checked;

            if (this.indeterminate)
            {
                this.checked = false;
                this.setValue(null);
            }
            else
            {
                this.setValue(false);                
            }
        }
    }

    valueDefault()
    {
        return this.hasIndeterminate ? null : false;
    }
}
