import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";

@Component({
    selector      		: ".percentage-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit()"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"
											currencyMask
											[ngModel]="value"
											[ngModelOptions]="{standalone: true}"
											[readonly]="!editable"
											[mask]="mask"
											(keyup)="onInput($event)"
											[options]="{ prefix : '', suffix : '%', thousands : '.', decimal : ',', align : 'left', precision : precision }"
											[placeholder]="field.placeholder"/>

								<icon-input [isLoading]="isLoading"
											[icon]="field.getIcon()">
								</icon-input>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('max')">Quantidade máxima {{maximum}}% permitida!</span>
							</error-input>
						</div>`
})
export class PercentageInput extends BaseTextInput
{
    public mask      = "000";
    public maximum   = 100;
    public precision = 2;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col percentage-field ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }

        if (this.setting.maximum)
        {
            this.maximum = this.setting.maximum;				
        }

        if (this.setting.precision)
        {
            this.precision = this.setting.precision;
        }

        this.validators.push(Validators.max(this.maximum));
    }

    onInput(event:any)
    {
        const value = parseFloat(event.srcElement.value.replace(",", ".").replace("%", ""));
		
        if (isNaN(value))
        {
            this.setValue(0);
        }
        else
        {
            this.setValue(value);
        }
    }
	
    valueDefault():any
    {
        return 0;
    }
}
