import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { IconType }      from '../../../type/icon/icon.type';

@Component({
    selector        : `.confirm-password-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 
										'ng-valid' 	   : formControl?.valid && this.hasValue(), 
										'ng-invalid'   : !formControl?.valid,
										'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="{{type}}"
									   [value]="value"
                                       [placeholder]="field.placeholder"
                                       (keyup)="onInput($event)"/>

                                <icon-input [isLoading]="isLoading"
                                            [icon]="icon"
                                            (click)="changeType()">
                                </icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('minlength')">{{'Mínimo de 8 caracteres' | translate}}</span>
                                <span *ngIf="hasError('compareError')">A confirmação está diferente da senha</span>
                            </error-input>
                        </div>`
})
export class ConfirmPasswordInput extends BaseTextInput implements AfterViewInit
{
    public password        : string = '';
    public comparePassword : any    = '';
    public type            : string = 'password';
    public icon            : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col confirm-password-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        this.icon = IconType.EYE_OFF;
    }

	createValidators()
	{
        this.validators = [];

        if(!this.hidden) 
        {
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}            
			
            this.validators.push(Validators.minLength(8));
        }	
	}

    changeType()
    {
        if(this.type == 'password')
        {
            this.type = 'text';
            this.icon = IconType.EYE;
        }
        else
        {
            this.type = 'password';
            this.icon = IconType.EYE_OFF;
        }
    }

    onInput(event:any)
    {
        this.setValue(event.srcElement.value);        
	}
	
	setValue(value:any)
    {
        if(value)
        {
            const md5            = new Md5();
			this.comparePassword = md5.appendStr(value).end();

			super.setValue(this.comparePassword);
			this.checkValidate();

            this.value = value;
        }
    }

    checkValidate()
    {
        if(this.formItem.required)
        {
            this.delError('compareError');

            if(this.password != this.comparePassword)
            {
                this.setError('compareError');
            }
        }        
    }

    ngAfterViewInit()
    {
        super.ngAfterViewInit();

        if(this.formGroup.get('password'))
        {
            this.formGroup.get('password').registerOnChange((value:any) =>
            {
                this.password = value;

                this.checkValidate();
            });
        }
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.comparePassword = value;
            this.dirty = true;
        }
    }
}
