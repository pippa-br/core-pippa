import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Validators } from '@angular/forms';
import { Geolocation } from '@capacitor/geolocation';

/* FIELD */
import { BaseInput } from '../base.input';

@Component({
    selector        : `.geolocation-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <div class="buttons">
                                    <a (click)="onAdd()"><ion-icon name="pin-outline"></ion-icon> {{field.label}}</a>
                                </div>

                                <agm-map [latitude]='latitude' [longitude]='longitude' [zoom]="8" [mapTypeId]='mapType'></agm-map>
                            </div>
                        </div>`,
})
export class GeolocationInput extends BaseInput
{
    @ViewChild('container', { read: BaseInput, static: false }) container : BaseInput;
    public mapType   : string = 'roadmap'; //roadmap, satellite, hybrid, terrain
    public latitude  : number = 0;
    public longitude : number = 0;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col geolocation-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }    

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    async onAdd()
    {
        const coordinates = await Geolocation.getCurrentPosition();

        //this.latitude = 
        console.log('Current', coordinates);
    }

    onSet()
    {
    }

    onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(value && !this.dirty)
        {
            this.dirty = true;
        }        
    }
}
