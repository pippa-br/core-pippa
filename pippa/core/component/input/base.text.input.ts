import { Directive } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "./base.input";

@Directive()
export abstract class BaseTextInput extends BaseInput
{
    public minimum : number;
    public maximum : number;
    public value   : string;
    public convert  = false;
	
    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        if (this.setting.convert)
        {
            this.convert = this.setting.convert;
        }		
    }

    initialize()
    {
        this.value = this.valueDefault();
    }

    onModelChange(value:any)
    {
        if (this.convert)
        {
            if (value instanceof Object)
            {
                value = JSON.stringify(value);
            }
            else if (value instanceof Array)
            {
                value = "[]";
            }
            else if (value === null)
            {
                value = "null";
            }
        }

        if (value !== undefined)
        {
            this.value = value;
            this.dirty = true;	
        }	

        //this.markForCheck();
    }

    onInput(event:any)
    {
        let value = event.srcElement.value.trim();

        if (this.convert && value !== undefined && value !== "")
        {
            value = value.trimStart();
            value = value.trimEnd();

            const x = Number(value);

            if (value == "false")
            {
                value = false;
            }
            else if (value == "true")
            {
                value = true;
            }
            else if (value == "null")
            {
                value = null;
            }
            else if (this.core().util.isJson(value))
            {
                value = JSON.parse(value);
            }
            else if (value == "[]")
            {
                value = [];
            }					
            else if (!isNaN(x))
            {
                value = x;
            }
        }

        this.setValue(value);
    }

    /* CHAMADAS ESXTERNAS */
    populate(value:any)
    {
        this.value = value;
        super.populate(value);
    }

    maxlength(value:any)
    {
        this.maximum = value;

        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }

        if (this.minimum !== undefined)
        {
            this.validators.push(Validators.min(this.minimum));
        }

        if (this.maximum !== undefined)
        {
            this.validators.push(Validators.max(this.maximum));
        }
			
        this.formControl.setValidators(this.validators);
        this.formControl.updateValueAndValidity();
    }
	
    minlength(value:any)
    {
        this.minimum = value;
        this.formControl.setValidators([ Validators.required, Validators.min(value) ]);
        this.formControl.updateValueAndValidity();

        /* POPULA COM O MIN */
        this.populate(value);
    }

    valueDefault()
    {
        return "";
    }

    // reset()
    // {
    //     if(!this.field.fixed)
    //     {
    // 		this.value = this.valueDefault();
    //         this.updateValue(this.value);
    //     }

    //     this.dirty = false;
    // }
}


