import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { AppPlugin }          from '../../../plugin/app/app.plugin';
import { Option }             from '../../../model/option/option';

@Component({
    selector        : `.reference-apps-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                             [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
                             <div header-input
                                  [form]="form"
                                  [formItem]="formItem"
                                  (edit)="onEdit()"
                                  (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                                <ng-select [(ngModel)]="selectedId"
                                           [ngModelOptions]="{standalone: true}"
                                           [disabled]="!editable"
										   [items]="items"
										   appendTo="body"
                                           [loading]="loading"
                                           [placeholder]="field.label"
                                           [bindLabel]="bindLabel"
                                           [bindValue]="bindValue"
                                           cancelText="Fechar"
										   okText="Selecionar"
										   (search)="onSearch($event)"
                                           (close)="onClose()"
                                           (change)="onInput($event)">
                                </ng-select>
                                <icon-input [isLoading]="isLoading"></icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>`
})
export class ReferenceAppsInput extends BaseReferenceInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.plugin = this.core().injector.get(AppPlugin);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-select-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* ADD APP */
        this.option = new Option({
            app : this.core().getApp('apps')
        });

        this.loadOption();        
    }

	createValidators()
	{
		super.createValidators();
		
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onModelChange(data:any)
    {
        if(data && !this.dirty)
        {
            this.populate(data);
            this.dirty = true;
        }
    }
}
