import { Component, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseInput } from '../base.input';

@Component({
    selector : `.time-seconds-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input #secondInput type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                        <ion-grid>
                            <ion-row>
                                <ion-col class="input-col">
                                    <input type="tel"
                                           #hourInput
                                           [mask]="mask"
                                           placeholder="Horas"
                                           [disabled]="field.disable"
                                           (blur)="onHourBlur($event)"/>
                                           <label>Horas</label>
                                </ion-col>
                                <ion-col class="input-col">
                                    <input type="tel"
                                           #minuteInput
                                           [mask]="mask"
                                           placeholder="Minutos"
                                           [disabled]="field.disable"
                                           (blur)="onMinuteBlur($event)"/>
                                    <label>Minutos</label>
                                </ion-col>
                            </ion-row>
                        </ion-grid>
                        <error-input [control]="formControl" [submitted]="submitted">
                        </error-input>
                    </div>
                </div>`
})
export class TimeSecondsInput extends BaseInput implements OnInit
{
    @ViewChild('hourInput',   {static: false}) hourInput   : any;
    @ViewChild('minuteInput', {static: false}) minuteInput : any;
    @ViewChild('secondInput', {static: false}) secondInput : any;

	public mask : string = '00';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col time-seconds-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		} 
	}

    onHourBlur(event:any)
    {
        event;
        this.setTotal();
    }

    onMinuteBlur(event:any)
    {
        event;
        this.setTotal();
    }

    setTotal()
    {
        let hours   = this.hourInput.nativeElement.value;
        let minutes = this.minuteInput.nativeElement.value;
        let seconds = 0;

        if(hours && hours != '')
        {
            hours = parseInt(hours);
        }
        else
        {
            hours = 0;
        }

        if(minutes && minutes != '')
        {
            minutes = parseInt(minutes);
        }
        else
        {
            minutes = 0;
        }

        seconds = (hours * 60 * 60) + (minutes * 60);

        this.setValue(seconds);
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            const data = this.core().util.secondsToHours(value);

            this.minuteInput.nativeElement.value = data.minutes;
            this.hourInput.nativeElement.value   = data.hours;

            this.dirty = true;
        }
    }
}
