import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BaseInput } from '../base.input';
import { Timestamp } from 'firebase/firestore';

/* PIPPA */
import { InputValidate } from '../../../validate/input.validate';
import { TDate } from '../../../util/tdate/tdate';

@Component({
    selector : `.birthday-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                        <input type="tel"
                               autocomplete="false"
                               [mask]="maskDate"
							   [(ngModel)]="dateValue"
							   [ngModelOptions]="{standalone: true}"
                               [placeholder]="field.placeholder"
                               [disabled]="field.disable"
                               (keyup)="onInput($event)"/>
                    </div>
					<error-input [control]="formControl" [submitted]="submitted">
						<span *ngIf="hasError('date')">Data Inválida</span>
                    </error-input>
                </div>`
})
export class BirthdayInput extends BaseInput
{
	dateMask  : string  = 'dd/MM/yyyy';
	maskDate  : string  = '00/00/0000';
    dateValue : string  = ''; 

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col birthday-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
		}
		
		this.validators.push(InputValidate.date);
	}

    onInput(event:any)
    {
		let date : any;

		if(event.srcElement.value.length == 10)
		{
			date = new TDate({ value : event.srcElement.value, mask : this.dateMask});
		}		
		
        if(date && date.isValid())
        {
            this.setValue(date.toDate());
        }
        else
        {
            this.setValue(false);
        }
    }

	/* METODO PARA CHAMADAS EXTERNAS */
    populate(value:any)
    {
        let date : any = new TDate({ value : value });        

        if(date.isValid())
        {
            this.dateValue = date.format(this.dateMask);
            this.updateValue(date.toDate());
        }
	}
	
	reset()
    {   
		if(!this.field.fixed)
		{
			this.dateValue = '';
		}

        super.reset();
    }

    onModelChange(value : any)
    {
        if(value && !this.dirty)
        {
			this.dirty = true;
            this.populate(value);            
        }
    }
}
