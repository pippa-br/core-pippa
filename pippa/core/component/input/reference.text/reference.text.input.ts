import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { Viewer }             from "../../../model/viewer/viewer";
import { FieldType } 	      from "../../../type/field/field.type";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";

@Component({
    selector        : ".reference-text-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{ 'ng-submitted' : submitted, 
                            'ng-valid' 	 : formControl?.valid && this.hasValue(), 
                            'ng-invalid' : !formControl?.valid,
                            'hidden'     : hidden }">
                             <div header-input
                                  [form]="form"
                                  [formItem]="formItem"
                                  (edit)="onEdit()"
                                  (del)="onDel()">
                            </div>
                            <div class="item-input"
                                 [formGroup]="field.formGroup"
                                 [ngClass]="{'has-add' : inputAdd && editable, 'has-set' : inputSet && editable && selected}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="text"
                                       autocomplete="false"
                                       [(ngModel)]="label"
                                       [mask]="mask"
                                       [ngModelOptions]="{standalone: true}"
                                       [readonly]="!editable || readonly || this.hasValue()"                                 
                                       (keyup)="onInput($event)"
                                       [placeholder]="field.placeholder || 'Buscar'"/>

                                    <a class="clearIcon" *ngIf="hasValue()" (click)="onClear()">
                                       <ion-icon name="close-outline"></ion-icon>
                                   </a>  
                                     
                                  <icon-input *ngIf="inputAdd && editable"
                                            [isLoading]="isLoading"
                                            class="add-icon"
                                            [icon]="{ value : 'add-circle-outline' }"
                                            (click)="addDocument()">
                                </icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted" [requiredText]="'Dados Inválidos!'">
                            </error-input>
                            <div dynamic-viewer
                                [viewer]="viewer"
                                [data]="selected"
                                (rendererComplete)="markForCheck();">
                            </div>                                     
                        </div>`
})
export class ReferenceTextInput extends BaseReferenceInput
{
    public label         : string;
    public fieldSearch   : any;
    public fieldOperator  = "";
    public viewer        : any;
    public mask          : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col reference-text-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        if (this.setting.fieldOperator)
        {
            this.fieldOperator = this.setting.fieldOperator;
        }

        if (this.setting.fieldSearch)
        {
            this.fieldSearch = this.setting.fieldSearch.split(",");
        }

        if (this.setting.mask)
        {
            this.mask = this.setting.mask;
        }

        if (this.setting.viewerPath)
        {
            const paths = this.setting.viewerPath.split("/");            

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid       : paths[0],
                appid       : paths[1],
                colid       : paths[2],
                path        : this.setting.viewerPath,
                model       : Viewer,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                },
                map : true,
            })

            this.core().api.getObject(params).then((result:any) =>
            {
                this.viewer = result.data;
                this.markForCheck();
            });
        }
    }

    createValidators()
    {
        super.createValidators();
		
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
    }

    onClear()
    {
        this.reset();
    }

    /* OVERRIDER */
    reload()
    {
        return new Promise((resolve:any) =>
        {
            resolve();
        })
    }

    onInput(event:any)
    {
        const value = event.srcElement.value;

        if (this.fieldSearch)
        {
            this.label = value;

            if (value)
            {                
                this.getWhere().then((where:any) => 
                {	
                    this.fieldSearch.forEach((field) => 
                    {
                        where.add({
                            field    : field,
                            operator : this.fieldOperator,
                            value    : value,
                            type     : FieldType.type("Text"),
                        })
                    });	

                    this.isLoading                 = true;
                    this.collection._query.where   = where;
                    this.collection._query.loading = false;
                    this.collection.search(this.term);
                });
            }
        }
        else
        {
            console.error("fieldSearch not found");
        }
    }

    reset()
    {
        if (!this.field.fixed)
        {
            this.label = "";
        }

        super.reset();
    }

    changeItems()
    {
        /* SOMENTE SE TIVER FEITO A BUSCA, POIS O BASE REFERENCE JÁ FAZ BUSCA */
        if (this.label)
        {
            if (this.items.length > 0)
            {
                this.setValue(this.items[0]);
            }
			
            this.submitted = true;
        }
    }

    /* OVERRIDE */
    doAdd(data:any)
    {
        this.setValue(data);
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.setValue(data);
    }

    updateActive(item:any)
    {
        super.updateActive(item);

        if (item)
        {
            this.populate(item).then(async (model) => 
            {
                this.label = await model.getPathValue(this.fieldLabel);                

                /* INVERSED BY */
                //this.updateInversedBy(item, 'inversedBy');
		
                /* INIT INVERSED BY */
                //this.updateInversedBy(item, 'initInversedBy');  								
            });     		
        }
    }

    onModelChange(data:any)
    {
        if (data && !this.dirty)
        {
            this.dirty = true;
            this.updateActive(data);
        }

        //this.displayControlByValue(data);
    }
}
