import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';

@Component({
    selector : `.url-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit()"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                        <input type="text"
                               autocomplete="false"
                               [ngModel]="value"
                               [ngModelOptions]="{standalone: true}"
                               [readonly]="form?.isSetModeForm() && !field.editable"
                               (keyup)="onInput($event)"
                               [placeholder]="field.placeholder"/>

                        <icon-input [isLoading]="isLoading"
                                    [icon]="field.getIcon()">
                        </icon-input>
                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class UrlInput extends BaseTextInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col url-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
	}

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		}
	}
	
	onModelChange(value:any)
    {
		/* CADASTROS ANTIGOS */
		if(!this.dirty && value instanceof Object)
		{
			value = value.url;
			this.setValue(value);
		}

        this.value = value;
		this.dirty = true;			

        //this.markForCheck();
    }
}
