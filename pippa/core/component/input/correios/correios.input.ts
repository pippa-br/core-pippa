import { Component, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { InputValidate }      from '../../../validate/input.validate';
import { BaseModel } 		  from '../../../model/base.model';

@Component({
    selector        : `.correios-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit()"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<ng-select #select
                                           [(ngModel)]="selectedId"
                                           [ngModelOptions]="{standalone: true}"
                                           [disabled]="!editable"
                                           [searchable]="searchable"
                                           [bindLabel]="bindLabel"
										   [bindValue]="bindValue"
										   appendTo="body"
                                           groupBy="groupBy"
                                           cancelText="Fechar"
                                           okText="Selecionar"
                                           [items]="shippings"
                                           notFoundText="Item não encontrado"
                                           [placeholder]="(isLoading ? 'Carregando...' : field.label)"
                                           [clearable]="clearable"
                                           (change)="onInput($event)"
                                           (clear)="onReset()">
								</ng-select>
								
								<icon-input [isLoading]="isLoading"
                                            [icon]="_icon">
								</icon-input>
								
                            </div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('correios')">Problema com os correios!</span>
                            </error-input>
                        </div>`
})
export class CorreiosInput extends BaseReferenceInput implements AfterViewInit
{
    public clearable   : boolean = true;
	public searchable  : boolean = false;
	public hasShipping : boolean = true;
	public hasPickup   : boolean = false;
	public address     : any;
	public product     : any;
	public shippings   : any = [];
	public timeAPI     : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col correios-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';	
		
		if(this.setting.hasPickup != undefined)
		{
			this.hasPickup = this.setting.hasPickup;
		}
	}
	
	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}	
		}

		this.asyncValidators = [InputValidate.search(this.field, this.form, this)];
	}

	ngAfterViewInit()
    {
		super.ngAfterViewInit();

        if(this.formGroup.get('address'))
        {			
			this.formGroup.get('address').instance.field.onComplete = (async (value:any) =>
            {	
				this.address = value;
				await this.caculateShipping();
            });
		}
		
		if(this.formGroup.get('product'))
        {			
			this.formGroup.get('product').instance.field.onChange = (async (value:any) =>
            {				
				await this.caculateShipping();				
			});					
		}		
	}

	async caculateShipping()
	{
		this.reset();
		
		if(this.formGroup.get('product'))
		{
			/* POIS TEM PRODUCTO QUE PASSADO POR PARAMETRO E NÃO DISPARA O EVENTO DE ONCHANGE */
			// SELECTED É REFERENCIA
			this.product = this.formGroup.get('product').instance.selected;

			if(this.product && this.address)
			{
				this.product = new BaseModel(this.product);
				await this.product.on();

				this.hasShipping = this.product.hasShipping;
				
				if(this.hasShipping)
				{
					this.delError('correiosError');
					
					this.shippings = [];

					if(this.timeAPI)
					{
						clearTimeout(this.timeAPI);
					}

					this.timeAPI = setTimeout(() => 
					{
						this.isLoading = true;

						const data = {
							accid       : this.core().account.code,
							appid       : 'apps',
							colid       : 'documents',
							services 	: this.core().account.correios.services,
							origin   	: this.core().account.correios.origin,
							platform    : this.core().platform,
							destination : this.address.zipcode,
							format   	: 1,  
							width    	: 10,
							height   	: 1,
							length   	: 15,
							weight   	: 0.1
						}

						this.core().api.callable('shippingApi/calculateShippings', data).then(async (result) => 
						{
							if(result.status)
							{
								if(this.hasPickup)
								{
									result.collection.push({
										id 	  : "pickup",
										label : "Retirada no Local",
										value : 0
									});
								}

								this.shippings = result.collection;
								//this.setValue(data);

								console.log('shippings', result);
							}
							else
							{
								//this.setValue(null);
								this.setError('correiosError');
							}

							this.isLoading = false;
						});
					}, 250);				
				}
				else
				{
					this.shippings = [{
						id 	  : "free",
						label : "Frete Grátis",
						value : 0
					}];
				}
			}
		}		
	}

	reset()
    {
        if(!this.field.fixed)
        {
			this.updateValue(null);
		}
		
		this.dirty = false;
    }

    onInput(item:any)
    {
		this.setValue(item);
	}

	updateValue(data)
	{		
		if(!data)
        {
			this.selectedId = null
			this.selected   = null;
        }
        else
        {
			/* POR CAUSA DO BASEREFERENCE */
			if(typeof data.getData == 'function')
			{
				data = data.getData();
			}

			this.selectedId = data[this.bindValue];
			this.selected   = data;
		}
		
		super._updateValue(data);
	}

    async onModelChange(data:any)
    {
		if(data)
		{
			/* DEIXAR SEPARADO DO DATA */
			if(!this.dirty)
			{
				this.selectedId = data[this.bindValue];
				this.selected   = data;
				this.dirty 		= true;

				for(const key in this.items)
				{
					if(this.selectedId == this.items[key][this.bindValue])
					{
						data = this.items[key];
						this.updateValue(data);
						break;
					}
				}
	
				await this.displayControlByValue(data);
	
				/* NÃO USASAR this.onSelect POIS ESSA FUNCAO E PARA INTERACAO NA INTERFACE, usar onChange */	
				this.emitChange(data);	
			}				
		} 
    }
}
