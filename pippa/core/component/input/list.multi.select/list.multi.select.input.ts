import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, SkipSelf } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { InputValidate } from '../../..';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { TDate } from '../../../util/tdate/tdate';

@Component({
    selector        : `.list-multi-select-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
								
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<table>
									<tr *ngFor="let item of list; let i = index">
										<td>
											{{item.name}}
										</td>
										<td>
											<ng-select #select
													[(ngModel)]="selecteds[i].items"
										   			[ngModelOptions]="{standalone: true}"
													[items]="item.items"
													[addTag]="hasAdd"
													[loading]="loading"
													[bindLabel]="bindLabel"
													[closeOnSelect]="closeOnSelect"
													[clearSearchOnAdd]="true"
													[hideSelected]="true"
													[hideSelected]="true"
													multiple="true"
													appendTo="body"
													notFoundText="Item não encontrado"
													cancelText="Fechar"
													okText="Selecionar"
													[placeholder]="item.name"
													[clearable]="clearable"
													(change)="onInput($event)"
													(clear)="onReset()">
											</ng-select>
										</td>
									</tr> 
								</table>								                               
								                                
							</div>							
							<error-input [control]="formControl" [submitted]="submitted">
							</error-input>							
                        </div>`
})
export class ListMultiSelectInput extends BaseReferenceInput
{
    @ViewChild('select', {static: false}) select : any;

	public selecteds   	 : Array<any>;
    public closeOnSelect : boolean = false
    public clearable     : boolean = true;
    public loading       : boolean = false;
    public hasAdd        : any     = false;
    public list          : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + '-input input col list-multi-select-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* SETTINGS */
        if(this.field.setting)
        {
            /* HAS CLEAR */
            if(this.setting.clearable)
            {
                this.clearable = this.setting.clearable == 'true';
            }

            /* CLOSE ON SELECT */
            if(this.setting.closeOnSelect)
            {
                this.closeOnSelect = this.setting.closeOnSelect == 'true';
            }

            /* ADD TAG */
            if(this.setting.hasAdd)
            {
                this.hasAdd = (name:string) =>
                {
                    return new Promise((resolve) =>
                    {
                        //this.loading = true;
                        setTimeout(() => {
                            resolve({
                                label : name,
                                value : Math.floor(new TDate().unix() * Math.random())
                            });
                        });
                    })
                };
            }
        }
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
				this.validators.push(InputValidate.listMultiSelect);
			}
		}
	}

	/*getSelecteds(i:number)
	{
		if(this.selecteds && this.selecteds[i])
		{
			return this.selecteds[i];
		}
		else
		{
			return [];
		}
	}*/

    async loadOptions()
    {
		this.selecteds = [];

		for(const item of this.options)
		{
			this.selecteds.push({
				type  : item.reference,
				items : []
			});
		}

        this.list = this.options;
    }

    reset()
    {
        this.patchValue([]);
        this.dirty = false;
    }

    onInput(data:any)
    {
		this.setValue(this.selecteds);

        if(this.field.onInput)
        {
            this.field.onInput({
                field : this.field,
                data  : data,
            });
        }
	}

	async setCollection(items)
	{
		this.reset();

		this.selecteds = [];

		for(const item of items)
		{
			this.selecteds.push({
				type  : item.reference,
				items : []
			});
		}
		
		this.list = items;

		this.markForCheck();

		//await super.setCollection(items);

		// BUG: NAO ESTAVA ABRINDO OS ITEMS DO DROP, SO NO SEGUNDO CLICK
		//if(this.select)
		//{
			//this.select.focus();
		//}
	}

	/*updateValue(data)
	{		
		if(data == undefined)
        {
			this.selecteds = null
        }
        else
        {
			this.selecteds = data;
		}
		
		super._updateValue(data);
	}*/

    async onModelChange(data:any)
    {
		if(data && data.length > 0)
		{
			/* DEIXAR SEPARADO DO DATA */
			if(!this.dirty)
			{
				this.dirty 	    = true;
				/*const selecteds = [];

				data.forEach(item => 
				{
					selecteds.push(item[this.bindValue]);
				});

				this.selecteds = selecteds;*/

				// BUG DE NÃO ADD OUTROS ARRAYS
				if(data.length == 1)
				{
					data.push({
						items : []
					});
				}

				this.selecteds = data;

				await this.displayControlByValue(data);
	
				/* NÃO USASAR this.onSelect POIS ESSA FUNCAO E PARA INTERACAO NA INTERFACE, usar onChange */	
				this.emitChange(data);	
			}					

			//console.error(data, this.selecteds);
		} 
    }
}
