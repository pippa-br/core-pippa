import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";

@Component({
    selector        : ".hidden-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template       	: `<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                        <input type="hidden" formControlName="{{formItem.name}}"/>
                    </div>`
})
export class HiddenInput extends BaseInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col hidden-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    /* NAO RESETAR QUANDO LIMPAR O FOMULARIO */
    reset()
    {
        // OVERRIDER
    }
}
