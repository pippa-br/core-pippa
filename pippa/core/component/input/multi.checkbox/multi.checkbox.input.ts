import { Component, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ValidationErrors, ValidatorFn, FormGroup } from "@angular/forms"

/* FIELD */
import { BaseFieldsetInput }  from "../../input/base.fieldset.input";
import { FieldType }          from "../../../type/field/field.type";
import { Field }              from "../../../model/field/field";
import { FormItemCollection } from "../../../model/form.item/form.item.collection";

@Component({
    selector      		: ".multi-checkbox",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted'    : submitted,
                                         'ng-valid' 	   : formControl?.valid && this.hasValue(), 
                                         'ng-invalid'      : !formControl?.valid,
										 'has-attachments' : field.hasAttachments(),
										 'hidden'          : hidden}">
							<div class="item-input" [formGroup]="field.formGroup">
								<div fieldset-input #template 
									(rendererComplete)="onRendererComplete()"
									(update)="onChange($event)">
								</div>
							</div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="submitted && invalidMinSelected">{{'Selecione no mínimo' | translate}} {{minSelected + ' item(s)'}}</span>
							</error-input>
						</div>`, 
})
export class MultiCheckboxInput extends BaseFieldsetInput
{
    public minSelected          = 0;
    public invalidMinSelected  = false

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        this.clear();
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col multi-checkbox-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        if (this.setting)
        {
            if (this.setting.minSelected)
            {
                this.minSelected = parseInt(this.setting.minSelected);
            }

            if (this.setting.inline)
            {
                this.nameClass += "inline "
            }
            else
            {
                this.nameClass += "no-inline "
            }
        }
		
        await this.createComponents();
    }

    async createComponents()
    {
        const items = new FormItemCollection();

        if (this.formItem.field.option)
        {
            await this.formItem.field.option.on();
			
            let i = 0;

            for (const key in this.formItem.field.option.items)
            {
                const option        = this.formItem.field.option.items[key];
                const checkboxField = new Field({
                    type        : FieldType.type("CheckboxObject"),
                    name        : key,
                    label       : option.label,
                    placeholder : option.label,
                    setting     : {
                        data : option,
                    }
                });

                items.set({
                    row   : i,
                    field : checkboxField
                });

                i++;
            }  
        }
		
        items.doSort();

        await this.formItem.items.updateItems(items);

        if (this.minSelected > 0)
        {
            this.template.formGroup.setValidators(this.minValidator());
        }
    }

    minValidator()
    {
        return (group: FormGroup): ValidationErrors =>
        {
            let count = 0;

            for (const key in group.controls)
            {
                const control : any = group.controls[key];

                if (control.value)
                {
                    count++;
                }
            }

            this.invalidMinSelected = count < this.minSelected;

            for (const key in group.controls)
            {
                const control : any = group.controls[key];

                if (this.invalidMinSelected)
                {
                    control.setErrors({ isEmpty : true });
                }
                else
                {
                    control.setErrors(null);
                }
            }

            return;
        };
    }

    /* A CASOS QUE O VALUE DO OPTION É UM OBJETO: {label, value} */
    getValueOption(data:any)
    {
        if (data)
        {
            if (this.controlPath) // PARA PEGAR O ID
            {
                return 	data[this.controlPath];
            }
            else if (data.value && data.value.value !== undefined)
            {
                return data.value.value;
            }
            else if (data.value !== undefined)
            {
                return data.value;
            }
            else
            {
                return data;
            }
        }
    }

    onChange(item:any)
    {
        this.changeDisplayFields(item);
    }
	
    changeDisplayFields(data:any)
    {
        if (this.isControlField)
        {
            this.displayFields(this.controlField, false); /* DESABILITA O ANTERIOR */

            for (const key in data)
            {
                let value = this.getValueOption(data[key]);

                if (value && this.field.setting && this.field.setting[value] != undefined)
                {
                    value = this.field.setting[value];

                    this.displayFields(value, true); /* HABILITA O ATUAL */
                }
            }
        }
    }

    clear()
    {
        if (this.template)
        {
            this.template.clear();
        }
    }
}
