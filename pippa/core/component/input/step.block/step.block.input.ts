import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseStepInput }       from '../../input/base.step.input';
import { BlockType }           from '../../../type/block/block.type';
import { FormBlockPopover }    from '../../../popover/form.block/form.block.popover';
import { BlockCollection }     from '../../../model/block/block.collection';
import { StepBlockCollection } from '../../../model/step.block/step.block.collection';
import { StepItem }            from '../../../model/step.item/step.item';

@Component({
    selector        : `.step-block-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    templateUrl     : `../base.step.input.html`
})
export class StepBlockInput extends BaseStepInput implements OnInit
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);

        this.editFormItem = true;
    }

    getOption()
    {
        return {
            items : BlockType.getItems()
        };
    }

    getFormPopover()
    {
        return FormBlockPopover;
    }

    getMaps()
    {
        return {
            list           : { klass : BlockCollection },
            stepCollection : { klass : StepBlockCollection },
            step           : { klass : StepItem },
        }
    }
}
