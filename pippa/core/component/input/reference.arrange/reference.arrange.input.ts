import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { Field }              from '../../../model/field/field';
import { FieldType }          from '../../../type/field/field.type';

@Component({
    selector 		: `.reference-arrange-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>`
})
export class ReferenceArrangeInput extends BaseReferenceInput /* ESSE COMPONENT NAO PODE TAMBEM EXTENDER DO BASEFIELDSET */
{
    @ViewChild('template', {static: false}) template : any;

    public fieldItems : FormItemCollection;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        formItem.field.required = false;

        this.fieldItems = formItem.items.copy();
        formItem.items  = new FormItemCollection();

        this.template.form = this.form;
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-arrange-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

    changeItems()
    {
        this.fieldItems.on(true).then(() =>
        {
            const addItems = new FormItemCollection()
            let i = 0;

            for(const key in this.items)
            {
                const data     = this.collection[key];
                const subItems = new FormItemCollection();
                const ref      = data.reference;

                const fieldHiddenLabel = new Field({
                    type    : FieldType.type('HiddenLabel'),
                    name    : '0',
                    label   : data.name,
                    initial : ref,
                });

                subItems.set({
                    row   : i,
                    field : fieldHiddenLabel
                });

                let j = 0;

                for(const key2 in this.fieldItems)
                {
                    const fieldItem = this.fieldItems[key2].field._copy();
                    fieldItem.name  = '' + (j + 1);

                    subItems.set({
                        row   : i,
                        col   : j,
                        field : fieldItem
                    });

                    j++;
                }

                const fieldFieldset = new Field({
                    type     : FieldType.type('Fieldset'),
                    name     : data.id,
                    items    : subItems,
                    hasLabel : false,
                });

                addItems.set({
                    row   : i,
                    field : fieldFieldset
                });

                i++;
            }

            this.field.items.updateItems(addItems);
        });
    }

    /* OVERRIDER */
    rendererComplete()
    {
    }

    set data(value:any)
    {
        this._data         = value;
        this.template.data = value;
    }

    get data()
    {
        return this._data;
    }

    /*siblingReload()
    {
        if(this.collection && this.siblingFields)
        {
            this.initSiblingFields = true;

            const where = [];

            for(const key in this.siblingFields)
            {
                where.push({
                    field    : this.siblingFields[key],
                    operator : (this.siblingOperators[key] != undefined ? this.siblingOperators[key] : '=='),
                    value    : this.siblingValues[key],
                });
            };

            const query = this.collection.getQuery();

            query.where = where;

            this.collection.setQuery(query)

            this.collection.reload();
        }
    }*/
}
