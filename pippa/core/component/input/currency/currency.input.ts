import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";

@Component({
    selector        : ".currency-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{ 'ng-submitted' : submitted, 
									'ng-valid' 	: formControl?.valid, 
									'ng-invalid'   : !formControl?.valid,
									'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup"> 

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                                
                                <input type="tel"
                                       [ngModel]="value"
                                       [ngModelOptions]="{standalone: true}"
                                       (keyup)="onInput($event)"
                                       [placeholder]="field.placeholder"
                                       [readonly]="!editable || readonly"
                                       autocomplete="false"
                                       [min]="minimum"
                                       [max]="maximum"
                                       currencyMask
                                       [options]="{ prefix : prefix + ' ', thousands : '.', decimal : ',', align : 'left', precision : precision }"/>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('min')">Valor menor que {{minimum}} o permitido</span>
								<span *ngIf="hasError('max')">Valor maior que {{maximum}} o permitido!</span>
                            </error-input>
                        </div>`
})
export class CurrencyInput extends BaseTextInput
{
    public minimum : number;
    public maximum : number;
    public prefix  : string;
    public precision  = 2;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col currency-field ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
		
        this.convert = true;
    }

    createValidators()
    {
        this.validators = [];

        if (this.editable)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
	
            if (this.setting.minimum)
            {
                this.minimum = this.setting.minimum;
                this.validators.push(Validators.min(this.minimum));
            }
	
            if (this.setting.maximum)
            {
                this.maximum = this.setting.maximum;
                this.validators.push(Validators.max(this.maximum));
            }
        }       

        if (this.setting.prefix)
        {
            this.prefix = this.setting.prefix;
        }
        else
        {
            this.prefix = "R$";
        }

        if (this.setting.precision)
        {
            this.precision = this.setting.precision;
        }
    }

    onInput(event:any)
    {
        if (event.srcElement)
        {
            const value = parseFloat(event.srcElement.value.replace(/\R\$/g, "").replace(/\./g, "").replace(/\,/g, ".").replace(/\- /g, "-"));

            if (isNaN(value))
            {
                this.setValue(this.valueDefault());
            }
            else
            {
                this.setValue(value);
            }
        }
        else
        {
            this.setValue(this.valueDefault());
        }
    }

    populate(value: any) 
    {
        value = parseFloat(value);

        if (isNaN(value)) 
        {
            value = 0;
        }
        else 
        {
            value = parseFloat(value.toFixed(this.precision));
        }

        super.populate(value);
    }

    valueDefault():any
    {
        return 0;    
    }
}
