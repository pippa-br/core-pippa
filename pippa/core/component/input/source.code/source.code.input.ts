import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { UploadPlugin }  from '../../../../core/plugin/upload/upload.plugin';

@Component({
    selector        : `.source-code-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (set)="onSet($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input #fileInput type="file" (change)="onUpload($event)" style="display: none;"/>

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <a class="image-upload" (click)="onAdd()">
                                    <ion-icon name="cloud-upload-outline"></ion-icon>
                                </a>

                                <ngx-monaco-editor [options]="config" 
                                                   [ngModelOptions]="{standalone: true}"
                                                   [(ngModel)]="value"
                                                   (onInit)="onInit($event)">
                                </ngx-monaco-editor>

                                <error-input [control]="formControl" [submitted]="submitted">
                                </error-input>
                            </div>
                        </div>` 
})
export class SourceCodeInput extends BaseTextInput
{
    @ViewChild('fileInput', {static: false}) fileInput: ElementRef;

    public editor : any;
    public config : any = {
        theme    : 'vs-dark', 
        language : 'html',
        automaticLayout : true,
    };

    constructor(
        public uploadPlugin      : UploadPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col source-code-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onInit(editor:any)
    {
        editor.onDidChangeModelContent((event) => 
        {
            this.setValue(this.value);
        });      

		this.editor = editor;
		
		setTimeout(() => 
		{
			this.markForCheck();
			this.markForCheck();
		}, 3000);
	}
	
	ngAfterViewInit()
    {
        super.ngAfterViewInit();

        setTimeout(() => 
		{
			this.markForCheck();
			this.markForCheck();
		}, 3000);		
    }

    onAdd()
    {
        this.fileInput.nativeElement.click();
    }

    onUpload(event:any)
    {
        this.uploadPlugin.uploadMultiple(event.target.files).then((files:any) =>
        {
            //let data = this.editor.parseData();
            this.value = '<figure><img src="' + files[0]._url + '"></figure>' + this.value;
            //this.editor.setData(data);
            this.markForCheck();
        });
    }
}
