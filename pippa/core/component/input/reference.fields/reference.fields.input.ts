import { Component, OnDestroy, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';
import { FormControl, } from '@angular/forms';
import * as _ from "lodash" 

/* PIPPA */
import { BaseInput }          from '../base.input';
import { FieldCollection }    from '../../../../core/model/field/field.collection';
import { FormItemCollection } from '../../../../core/model/form.item/form.item.collection';
import { FieldFormPopover }   from '../../../../core/popover/field.form/field.form.popover';
import { Field }              from '../../../../core/model/field/field';

//[dragula]="dragulaRow"
//[dragulaModel]="item.items"

@Component({
    selector        : `.reference-fields-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (edit)="onEdit($event)"
                     (del)="onDel()">
                </div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                    <ion-grid>
                        <ion-row class="item" *ngFor="let item of items; trackBy: trackById">
                            <ion-col>
                                <input id="{{item.id}}" type="checkbox" (click)="onSelectItem(item)" [checked]="item.enabled">
                                <label for="{{item.id}}">
                                    {{item.field.label}}&nbsp;&nbsp;<small *ngIf="item.field.required">*</small>
                                </label>
                            </ion-col>
                            <ion-col size="2">
                                <small>{{item.field.name}}</small>
                            </ion-col>
                            <ion-col size="1">
                                <small>{{item.field.type.label}}</small>
                            </ion-col>
                            <ion-col size="1" class="buttons">
                                <a (click)="openSubItem(item)" *ngIf="item.field.isContainer()">
                                    <ion-icon name="add-outline"></ion-icon>
                                </a>
                                <a (click)="onEditItem(item)">
                                    <ion-icon name="create-outline"></ion-icon>
                                </a>
                                <a (click)="onDelItem(item)">
                                    <ion-icon name="close-outline"></ion-icon>
                                </a>
                            </ion-col>
                            <ion-col size-xl="12" *ngIf="item.field.isContainer()" >
                                <ion-grid class="sub-fields"
                                          >
                                    <ion-row class="ion-align-items-center drag-row item"
                                             *ngFor="let subItem of item.items; trackBy: trackById; let i = index;"
                                             [attr.data-parent]="item.id">
                                        <ion-col class="col-move">
                                            <ion-icon name="reorder-four-outline"></ion-icon> <label>{{subItem?.field?.label}}&nbsp;&nbsp;<small *ngIf="subItem.field.required">*</small></label>
                                        </ion-col>
                                        <ion-col size="2" class="col-move">
                                            <small>{{subItem?.field?.name}}</small>
                                        </ion-col>
                                        <ion-col size="1" class="col-move">
                                            <small>{{subItem?.field?.type?.label}}</small>
                                        </ion-col>
                                        <ion-col size="1" class="buttons">
                                            <a (click)="onEditSubItem(item, subItem)">
                                                <ion-icon name="create-outline"></ion-icon>
                                            </a>
                                            <a (click)="onDelSubItem(item, subItem)">
                                                <ion-icon name="close-outline"></ion-icon>
                                            </a>
                                        </ion-col>
                                    </ion-row>
                                </ion-grid>
                            </ion-col>
                        </ion-row>
                    </ion-grid>
                    <div class="error" *ngIf="submitted">
                        <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                    </div>
                </div>
                <ion-grid class="footer-input">
                    <ion-row>
                        <ion-col>
                            <input id="{{field.id}}_all" type="checkbox" (click)="onSelectAll()" [checked]="hasAll()">
                            <label for="{{field.id}}_all">
                                Selecionar Todos
                            </label>
                        </ion-col>
                        <ion-col>
                            <a (click)="openItem()">
                                <ion-icon name="add-circle-outline"></ion-icon>
                            </a>
                        </ion-col>
                    </ion-row>
                </ion-grid>`
})
export class ReferenceFieldsInput extends BaseInput implements OnDestroy, OnInit
{
    public items             : FormItemCollection;
    public modelItems        : FormItemCollection;
    public collection        : FieldCollection;
    public parent            : string;
    public group             : string;
    public groupName         : string;
    public unsubscribe       : any;
    public popoverController : PopoverController;
    public alertController   : AlertController;
    public dragulaRow        : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-fields-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        this.parent     = this.formItem.name;
        this.group      = this.field.group;
        this.groupName  = this.field.groupName;
        this.items      = new FormItemCollection();
        this.modelItems = new FormItemCollection();

        /* DRAG ROW */
        /*this.subscriptions.add(this.dragulaService.dropModel(this.dragulaRow).subscribe((event) =>
        {
            setTimeout(() =>
            {
                const el    : any = event.el;
                const field : any = this.items.getById(el.dataset.parent);
                this.items.move(event.sourceIndex, event.targetIndex);

                /* DEFINE O ROW */
                /*for(const key in this.items)
                {
                    const item : any = this.items[key];
                    item.row = parseInt(key);
                }

                field.set(field.parseData());
            });
        }));*/
    }

    update()
    {
        this.setValue(this.items.parseData());
    }

    onSelectItem(item:any)
    {
        item.enabled = !item.enabled;
        this.update();
    }

    onSelectAll()
    {
        for(const key in this.items)
        {   
            const item : any = this.items[key];
            item.enabled = !item.enabled;
        }

        this.update();
    }

    hasAll()
    {
        let count = 0;

        for(const key in this.items)
        {
            const item : any = this.items[key];

            if(item.enabled)
            {
                count++;
            }
        }

        return count > 0 && count == this.items.length;
    }

    onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(value && !this.dirty)
        {
            const items = new FormItemCollection(_.merge({}, value))  /* POR CAUSA DO BIND COM O FORM */

            items.on(true).then(() =>
            {
                this.modelItems.setItems(items);
                this.dirty = true;

                this.displayItems();

                this.emitChange(this.modelItems);

                /* EMIT FORM CHANGE */
                //this.form.doChange();
                //this.collection.setItems(value);

                //console.log('33333333333', this.items);

                //this.items.on().then(() =>
                //{
                    //this.updateValue(this.items.parseData());
                //});
            });
        }
    }

    displayItems()
    {
        if(this.collection)
        {
            for(const key in this.collection)
            {
                const item : any = this.collection[key];

                this.items.set({
                    id    : item.id,
                    field : item,
                });
            }

            for(const key in this.modelItems)
            {
                this.items.set(this.modelItems[key]);
            }
        }
    }

    /* FIELD */
    openItem(item?:any)
    {
        this.popoverController.create(
        {
            component      : FieldFormPopover,
            componentProps :
            {
                data     : (item ? item.field : null),
                formItem : item,
                field    : this.field,
                acl      : this.form.instance.acl,
                onAdd : ((event:any) =>
                {
                    /* LOAD */
                    this.core().loadingController.start().then(() =>
                    {
                        let data        = event.data;
                        data.parent     = this.parent;
                        data.group      = this.group;
                        data.groupName  = this.groupName;
                        data.accid      = this.core().account.code;
                        data.appid      = this.core().currentApp.code;

                        this.collection.save(data).then(() =>
                        {
                            this.popoverController.dismiss();

                            /* LOAD */
                            this.core().loadingController.stop();
                        });
                    });
                }),
                onSet : ((event:any) =>
                {
                    /* LOAD */
                    this.core().loadingController.start().then(() =>
                    {
                        let data        = event.data;
                        data.parent     = this.parent;
                        data.group      = this.group;
                        data.groupName  = this.groupName;
                        data.accid      = this.core().account.code;
                        data.appid      = this.core().currentApp.code;

                        item.field.set(data).then(() =>
                        {
                            //this.items.set(item);

                            this.popoverController.dismiss();

                            /* LOAD */
                            this.core().loadingController.stop();
                        });
                    });
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass : 'right-popover',
        })
        .then(popover =>
        {
            popover.present();
        });
    }

    onEditItem(item:any)
    {
        this.openItem(item);
    }

    onDelItem(item:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text: 'Fechar',
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text: 'Remover',
                    handler : () =>
                    {
                        this.items.del(item);
                        item.field.del();

                        this.update();

                        return true;
                    }
                }
            ]
        }).then(alert =>
        {
            alert.present();
        });
    }

    /* SUB FIELD */
    openSubItem(item:any, subItem?:any)
    {
        this.popoverController.create(
        {
            component : FieldFormPopover,
            componentProps :
            {
                data  : (subItem ? subItem.field : null),
                field : this.field,
                acl   : this.form.instance.acl,
                onAdd : ((event:any) =>
                {
                    const data = event.data;
                    data.id    = data.name;
                    data.accid = this.core().account.code;
                    data.appid = this.core().currentApp.code;

                    item.items.set({
                        id    : data.id,
                        field : new Field(data),
                    });

                    this.update();

                    this.popoverController.dismiss();
                }),
                onSet : ((event:any) =>
                {
                    subItem.field.populate(event.data);
                    item.items.set(subItem);

                    this.update();

                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass : 'right-popover',
        })
        .then(popover =>
        {
            popover.present();
        })
    }

    onEditSubItem(item:any, subItem:any)
    {
        this.openSubItem(item, subItem);
    }

    onDelSubItem(item:any, subItem:any)
    {
        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse item?',
            buttons : [
                {
                    text: 'Fechar',
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text: 'Remover',
                    handler : () =>
                    {
                        item.items.del(subItem);
                        this.update();

                        return true;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        });
    }

    getFields()
    {
        /* LIST */
        const order = this.form.instance.acl.getKey('reference_fields_order',
        {
            field : 'label',
            asc   : true
        });

        let params = this.createParams({
            path            : this.core().account.code + '/' + this.core().currentApp.code  + '/fields',
            collectionModel : FieldCollection,
            where           : [{
                field    : 'parent',
                operator : '==',
                value    : this.parent,
            }],
            orderBy : order.field,
            perPage : 999,
            asc     : order.asc,
        });

        this.api().getList(params).then((result:any) =>
        {
            this.collection = result.collection;

            result.collection.on(true).then(() =>
            {
                this.doUnsubscribe();

                this.unsubscribe = this.collection.onUpdate().subscribe(() =>
                {
                    this.displayItems();
                    this.emitChange(this.items);
                });
            });
        });
    }

    ngOnInit()
    {
        this.getFields();
    }

    doUnsubscribe()
    {
        /* ON CHANGE */
        if(this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
    }

    ngOnDestroy()
    {
        super.ngOnDestroy();
        this.doUnsubscribe();
    }
}
