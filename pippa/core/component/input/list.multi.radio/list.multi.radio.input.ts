import { Component, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, SkipSelf } from '@angular/core';
import { BaseModel, InputValidate } from '../../..';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';

@Component({
    selector        : `.list-multi-radio-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 					: formControl?.valid && this.hasValue(), 
							'ng-invalid'   				: !formControl?.valid,
							'hidden'       				: hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
							<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
								
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<ion-grid>
									<ion-row *ngFor="let item of list; let i = index">										
										<ion-col>
											<span class="items-label">{{getLabel(i, item)}}</span>
											<div class="items-input">
												<label *ngFor="let item2 of item.items; trackBy : trackById;"
													class="label-radio"
													[ngClass]="{'disabled' : item2._canceled}">
													<input type="radio"
														[name]="item2[bindValue]"
														[value]="item2[bindValue]"
														[ngModel]="getngModel(i)"
														[disabled]="item2._canceled"
														[ngModelOptions]="{standalone: true}"
														(change)="onInput2(i, item2)">
													{{item2?.label}}
												</label>
											</div>
										</ion-col>
									</ion-row> 
								</ion-grid>								                               
								                                
							</div>				

							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('listMultiRadio')">Selecione as opções</span>
							</error-input>							
                        </div>`
})
export class ListMultiRadioInput extends BaseReferenceInput
{
	public selecteds  : Array<any>;
    public list       : any = [];
	public labels     : Array<any>;
	public fieldLabel : string;
	public bindValue  : string;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + '-input input col list-multi-radio-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* SETTINGS */
        if(this.field.setting)
        {
        }
    }

	createValidators()
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(InputValidate.listMultiRadio(this.list.length));
			}
		}

		if(this.setting.inline)
		{
			this.nameClass += 'inline ';
		}
		else
		{
			this.nameClass += 'no-inline ';
		}

		if(this.setting.labels)
		{
			this.labels = this.setting.labels.split(',');
		}

		if(this.setting.fieldLabel)
		{
			this.fieldLabel = this.setting.fieldLabel;
		}

		if(this.setting.bindValue)
		{
			this.bindValue = this.setting.bindValue;
		}
	}

	getngModel(i)
	{
		if(this.selecteds && this.selecteds[i])
		{
			return this.selecteds[i][this.bindValue];
		}		
		else
		{
			return
		}
	}

	getLabel(i:number, item:any)
	{
		if(this.labels && this.labels[i])
		{
			return this.labels[i];
		}
		else
		{
			return item.name;
		}
	}

    async loadOptions()
    {
		this.selecteds = [];

		for(const item of this.options)
		{
			this.selecteds.push({});
		}

        this.list = this.options;		
    }

    reset()
    {
        this.patchValue([]);
        this.dirty = false;
		this.markForCheck();
    }

    onInput2(i:number, data:any)
    {
		this.selecteds[i] = data;
		this.setValue(this.selecteds);

        if(this.field.onInput)
        {
            this.field.onInput({
                field : this.field,
                data  : data,
            });
        }
	}

	updateValidators2(): void 
	{
		this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(InputValidate.listMultiRadio(this.list.length));
			}
		}

		this.formControl.setValidators(this.validators);
		this.formControl.updateValueAndValidity();
	}

	async setCollection(items)
	{		
		if(!this.dirty)
		{
			// POR CAUSA DO EDITAR QUE VEM COM DADOS
			//this.patchValue(this.getValue());
			this.reset();

			this.selecteds = [];

			for(const item of items)
			{
				this.selecteds.push({});
			}
		}

		if(this.fieldLabel)
		{
			this.labels = [];

			for(const item of items)
			{			
				const data = new BaseModel(item)
	
				this.labels.push(await data.getProperty(this.fieldLabel));
			}						
		}

		this.list = items || [];
		this.updateValidators2();
		
		this.markForCheck();

		//await super.setCollection(items);

		// BUG: NAO ESTAVA ABRINDO OS ITEMS DO DROP, SO NO SEGUNDO CLICK
		//if(this.select)
		//{
			//this.select.focus();
		//}
	}

	/*updateValue(data)
	{		
		if(data == undefined)
        {
			this.selecteds = null
        }
        else
        {
			this.selecteds = data;
		}
		
		super._updateValue(data);
	}*/

    async onModelChange(data:any)
    {
		if(data && data.length > 0)
		{
			/* DEIXAR SEPARADO DO DATA */
			if(!this.dirty)
			{
				this.dirty = true;
				/*const selecteds = [];

				data.forEach(item => 
				{
					selecteds.push(item[this.bindValue]);
				});

				this.selecteds = selecteds;*/

				this.selecteds = data;

				await this.displayControlByValue(data);
	
				/* NÃO USASAR this.onSelect POIS ESSA FUNCAO E PARA INTERACAO NA INTERFACE, usar onChange */	
				this.emitChange(data);
			}					

			//console.error(data, this.selecteds);
		} 
    }
}
