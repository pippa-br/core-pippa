import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector : `.color-picker-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit()"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input type="hidden"
                               formControlName="{{formItem.name}}"
                               (ngModelChange)="onModelChange($event)"/>

                        <input type="text"
                               readonly
                               [(colorPicker)]="color"
                               [style.background]="color"
                               [value]="color"
                               [cpPosition]="'bottom'"
                               (colorPickerChange)="onColorChange()"/>
                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class ColorPickerInput extends BaseInput
{
    public color : string = "";

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col color-picker-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.color = value;
            this.dirty = true;
        }
    }

    onColorChange()
    {
        this.setValue(this.color);
    }
}
