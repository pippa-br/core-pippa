import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `.scan-button-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <ion-button (click)="onClick()" [disabled]="!editable">
							<ion-icon name="scan-circle-outline"></ion-icon> Scan
                        </ion-button>
                    </div>
                </div>`
})
export class ScanButtonInput extends BaseInput
{
    constructor(
		public changeDetectorRef : ChangeDetectorRef,
		public barcodeScanner    : BarcodeScanner
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col scan-button-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';        
	}

    onClick()
    {
		this.barcodeScanner.scan().then((data:any) => 
		{
			if(!this.siblingFields)
			{
				this.siblingFields = ['_code'];
			}
	
			console.error('Barcode data', data, this.siblingFields);
	
			for(const key in this.siblingFields)
			{
				const value = this.siblingFields[key];
				this.form.getInstance(value).setText(data.text) /* esta apenas para ReferenceText */
			}					
		}).catch(err => 
		{
			console.log('Error', err);
		});
    }    
}
