import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput }   from '../../input/base.input';
import { BaseList }    from '../../../model/base.list';

@Component({
    selector        : `.object-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (set)="onSet($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
								
                                <table class="striped list">
                                    <tr *ngFor="let item of inputs; let last = last;">
                                        <td>
											<div>
												<input type="text"
													[(ngModel)]="item.key"
													[ngModelOptions]="{standalone: true}"
													(ngModelChange)="updateOption()"
													placeholder="Key"/>
											</div>
                                        </td>
                                        <td>
											<div>
												<input type="text"
													[(ngModel)]="item.value"
													[ngModelOptions]="{standalone: true}"
													(ngModelChange)="updateOption()"
													placeholder="Valor"/>
											</div>
                                        </td>
										<td class="buttons-col">
											<div class="buttons-div">
												<ion-button (click)="onRemove(item)">
													<ion-icon name="remove-outline"></ion-icon>
												</ion-button>
												<ion-button *ngIf="last" (click)="onAdd()">
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
										</td>
                                    </tr>
                                    <tr *ngIf="inputs.length == 0" class="empty-row" (click)="onAdd()">
										<td>
											<div>
												<label>Nenhum item cadastrado!</label>
											</div>											
										</td>
										<td class="buttons-col">
											<div class="buttons-div">
												<ion-button>
													<ion-icon name="add-outline"></ion-icon>
												</ion-button>
											</div>
										</td>
									</tr>
                                </table>
                                <div class="error" *ngIf="submitted">
                                    <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                                </div>
                            </div>
                        </div>`
})
export class ObjectInput extends BaseInput
{
    public inputs : BaseList<any> = new BaseList<any>();

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-options input options col object-options ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.inputs = new BaseList<any>();

            for(let key in value)
            {
                this.inputs.push({
                    id    : key,
                    key   : key,
                    value : value[key],
                });
            }

            this.dirty = true;
        }
    }

    onAdd()
    {
        this.inputs.push({
            id    : this.core().util.randomString(8),
            key   : '',
            label : '',
        });
    }

    onRemove(item:any)
    {
        this.inputs.del(item);
        this.updateOption();
    }

    updateOption()
    {
		const data = {};				

        for(let key in this.inputs)
        {
            if(this.inputs[key].key !== '' && this.inputs[key].value !== '')
            {
                let  value = this.inputs[key].value;

                if(value == 'false')
                {
                    value = false;
                }
                else if(value == 'true')
                {
                    value = true;
                }
                else if(this.core().util.isNumber(value))
                {
                    value = Number(value);
                }

                data[this.inputs[key].key] = value;
            }
		}
		
        this.setValue(data);
    }
}
