import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { Validators } from "@angular/forms";
import slugify from "slugify";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { InputValidate } from "../../../validate/input.validate";

@Component({
    selector        : ".alnum-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit($event)"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">

                        <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                        <input type="text"
                               autocomplete="false"
                               [ngModel]="value"
                               [mask]="mask"
                               [patterns]="patterns"
                               [readonly]="(!editable && isSetModeForm()) || readonly"
                               [ngModelOptions]="{standalone: true}"
                               (keyup)="onInput($event)"
                               [placeholder]="field.placeholder"/>

                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class AlnumInput extends BaseTextInput
{
    public mask     = "A{50}";
    public patterns = { "A" : { pattern : new RegExp("\[a-zA-Z0-9_.-\]") } };

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col alnum-field ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }

        this.asyncValidators = [ InputValidate.search(this.field, this.form, this) ];

        if (this.setting.patterns)
        {
            this.patterns = this.setting.patterns;
        }
    }

    /* CHAMADAS ESXTERNAS */
    slug(value:any)
    {
        this.value = slugify(value, {  lower : true });
        super.populate(this.value);
    }
}
