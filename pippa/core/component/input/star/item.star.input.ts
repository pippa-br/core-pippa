import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector : 'item-star',
    template : `<span class="star" [class.active]="active" (click)="handleRate()">&#9733;</span>`,
})
export class ItemStarInput
{
    @Input() active: boolean;
    @Input() position: number;
    @Output() rate : EventEmitter<any> = new EventEmitter();

    handleRate()
    {
        this.rate.emit(this.position);
    }
}
