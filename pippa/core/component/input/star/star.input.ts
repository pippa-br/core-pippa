import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* FIELD */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `.star-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div header-input [formItem]="formItem" (set)="onSet($event)" (del)="onDel()"></div>
					   <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
							<input type="hidden" [(ngModel)]="rating" formControlName="{{formItem.name}}"/>
							<div class="stars">
								<item-star *ngFor="let star of stars"
											[active]="star <= rating"
											[position]="star"
											(rate)="onRate($event)">
								</item-star>
							</div>
							<div class="error" *ngIf="submitted">
								<span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
							</div>
					   </div>`
})
export class StarInput extends BaseInput
{
    //@Input()  starCount : number;
    @Input()  rating    : number;
    @Output() rate = new EventEmitter();

    public stars : number[] = [1,2,3,4,5];
    //_rating = this.rating;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }    

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col star-field';
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

	onRate(star)
    {
        this.rate.emit(star);
        this.rating = star;

        console.log(this.rating);

        //this.field.formGroup.get(this.formItem.name).setValidators(null);
        //this.field.formGroup.get(this.formItem.name).updateValueAndValidity();
    }
}
