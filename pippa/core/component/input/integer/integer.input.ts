import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";

@Component({
    selector        : ".integer-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
                                'ng-valid' 	: formControl?.valid && this.hasValue(), 
                                'ng-invalid'   : !formControl?.valid,
                                'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"								
                                       [ngModel]="value"
                                       autocomplete="false"
                                       [ngModelOptions]="{standalone: true}"
                                       (keyup)="onInput($event)"
                                       [readonly]="!editable"
                                       currencyMask
                                       [min]="minimum"
                                       [max]="maximum"
                                       [options]="{ prefix : '', thousands : '', decimal : '.', align : 'left', precision : precision }"
                                       [placeholder]="field.placeholder | translate"/>

                            </div>

                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('min')">Quantidade mínima {{minimum}} permitida!</span>
                                <span *ngIf="hasError('max')">Quantidade máxima {{maximum}} permitida!</span>
                            </error-input>
                            
                        </div>`
})
export class IntegerInput extends BaseTextInput
{
    public precision = 0;
    public hasEmpty  = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col integer-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
		
        this.convert = true;
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }
			
            if (this.setting.minimum)
            {
                this.minimum = this.setting.minimum;
                this.validators.push(Validators.min(this.minimum));
            }
	
            if (this.setting.maximum)
            {
                this.maximum = this.setting.maximum;
                this.validators.push(Validators.max(this.maximum));
            }
            
            if (this.setting.hasEmpty)
            {
                this.hasEmpty = true;
            }

            if (this.setting.precision)
            {
                this.precision = this.setting.precision;
            }
        }	
    }
	
    populate(value:any):any
    {
        value = parseFloat(value);

        if (isNaN(value))
        {
            value = this.hasEmpty ? "" : 0;
        }		

        super.populate(value);
    }

    valueDefault():any
    {
        return this.hasEmpty ? "" : 0;
    }

    onInput(event:any)
    {
        const value = parseFloat(event.srcElement.value);

        if (isNaN(value))
        {
            this.setValue(0);
        }
        else
        {
            this.setValue(value);
        }
    }
}
