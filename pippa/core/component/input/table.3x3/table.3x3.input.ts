import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../../input/base.fieldset.input';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { FieldType }          from '../../../type/field/field.type';
import { Field }              from '../../../model/field/field';

@Component({
    selector 		: `.table3x3-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>`,
})
export class Table3x3Input extends BaseFieldsetInput
{
    public listTextSelectField : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
		this.formItem.field.hasLabel = false;

        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col table3x3-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
		
		await this.createComponents();
    }

    async createComponents()
    {
        super.ngAfterViewInit();

        const items = new FormItemCollection();

        /* DEPOIS TROCAR options por option */
        if(this.formItem.field.option.rows && this.formItem.field.option.columns)
        {
            let i = 0;

            for(const key in this.formItem.field.option.rows)
            {
                const row = this.formItem.field.option.rows[key];
                const radioItem : any = {
                    row   : i,
                    field : new Field({
                        type        : FieldType.type('Radio'),
                        name        : key,
                        label       : row['label'],
                        placeholder : row['label'],
                        option      : {
                            items : this.formItem.field.option.columns
                        },
                        setting : {
                            inline : true,
                        },
                        row : key,
                    })
                };

                items.push(radioItem);
                i++;
            }
		}
				
		await this.formItem.items.updateItems(items);
    }
}
