import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../../input/base.fieldset.input';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { Field }              from '../../../model/field/field';
import { FieldType }          from '../../../type/field/field.type';

@Component({
    selector 		: `.cron-job-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: '<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>',
})
export class CronJobInput extends BaseFieldsetInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.field.name + '-input input col cron-job-input ';
		this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
		
		await this.createComponents();
    }

    async createComponents()
    {
		const items = new FormItemCollection([
			{
				row   : 1,
				col   : 1,
				field : {
					type     : FieldType.type('Select'),
					label    : 'Repetir a cada',
					name     : 'frequency',
					required : true,
					option  : {
						items : [
							{
								label : 'Minuto',
								value : 1
							},
							{
								label : 'Hora',
								value : 2
							},
							{
								label : 'Dia',
								value : 3
							},
							{
								label : 'Semana',
								value : 4
							},
							{
								label : 'Mês',
								value : 5
							},
							{
								label : 'Ano',
								value : 6
							}
						]
					},
					setting : {
						controlField : 'M,dw,D,h,m',
						2 : 'm',
						3 : 'h,m',
						4 : 'dw,h,m',
						5 : 'D,h,m',
						6 : 'M,D,h,m',
					},
					/*onInput : (event:any) =>
                    {
						if(event.data)
						{
							this.updateCron();	
						}
					}*/
				}				
			},
			{
				row   : 2,
				col   : 1,
				field : {
					type     : FieldType.type('MultiSelect'),
					label    : 'Meses',
					name     : 'M',
					required : true,
					option   : {
						items : [
							{
								label : 'Janeiro',
								value : 1
							},
							{
								label : 'Fevereiro',
								value : 2
							},
							{
								label : 'Março',
								value : 3
							},
							{
								label : 'Abril',
								value : 4
							},
							{
								label : 'Maio',
								value : 5
							},
							{
								label : 'Junho',
								value : 6
							},
							{
								label : 'Julho',
								value : 7
							},
							{
								label : 'Agosto',
								value : 8
							},
							{
								label : 'Setembro',
								value : 9
							},
							{
								label : 'Outubro',
								value : 10
							},
							{
								label : 'Novembro',
								value : 11
							},
							{
								label : 'Dezembro',
								value : 12
							},
						]
					}
				}				
			},
			{
				row   : 3,
				col   : 1,
				field : {
					type     : FieldType.type('MultiSelect'),
					label    : 'Dias da Semana',
					name     : 'dw',
					required : true,
					option   : {
						items : [
							{
								label : 'Domingo',
								value : 1
							},
							{
								label : 'Segunda',
								value : 2
							},
							{
								label : 'Terça',
								value : 3
							},
							{
								label : 'Quarta',
								value : 4
							},
							{
								label : 'Quinta',
								value : 5
							},
							{
								label : 'Sexta',
								value : 6
							},
							{
								label : 'Sábado',
								value : 7
							}
						]
					}
				}				
			},
			{
				row   : 4,
				col   : 1,
				field : {
					type     : FieldType.type('MultiSelect'),
					label    : 'Dia',
					name     : 'D',
					required : true,
					option   : {
						items : [
							{
								label : '1ª',
								value : 1
							},
							{
								label : '2ª',
								value : 2
							},
							{
								label : '3ª',
								value : 3
							},
							{
								label : '4ª',
								value : 4
							},
							{
								label : '5ª',
								value : 5
							},
							{
								label : '6ª',
								value : 6
							},
							{
								label : '7ª',
								value : 7
							},
							{
								label : '8ª',
								value : 8
							},
							{
								label : '9ª',
								value : 9
							},
							{
								label : '10ª',
								value : 10
							},
							{
								label : '11ª',
								value : 11
							},
							{
								label : '12ª',
								value : 12
							},
							{
								label : '13ª',
								value : 13
							},
							{
								label : '14ª',
								value : 14
							},
							{
								label : '15ª',
								value : 15
							},
							{
								label : '16ª',
								value : 16
							},
							{
								label : '17ª',
								value : 17
							},
							{
								label : '18ª',
								value : 18
							},
							{
								label : '19ª',
								value : 19
							},
							{
								label : '20ª',
								value : 20
							},
							{
								label : '21ª',
								value : 21
							},
							{
								label : '22ª',
								value : 22
							},
							{
								label : '23ª',
								value : 23
							},
							{
								label : '24ª',
								value : 24
							},
							{
								label : '25ª',
								value : 25
							},
							{
								label : '26ª',
								value : 26
							},
							{
								label : '27ª',
								value : 27
							},
							{
								label : '28ª',
								value : 28
							},
							{
								label : '29ª',
								value : 29
							},
							{
								label : '30ª',
								value : 30
							},
							{
								label : '31ª',
								value : 31
							},
						]
					}	
				}				
			},
			{
				row   : 5,
				col   : 1,
				field : {
					type     : FieldType.type('MultiSelect'),
					label    : 'Horas',
					name     : 'h',
					required : true,
					option  : {
						items : [
							{
								label : '0h',
								value : 0,
							},
							{
								label : '1h',
								value : 1,
							},
							{
								label : '2h',
								value : 2,
							},
							{
								label : '3h',
								value : 3,
							},
							{
								label : '4h',
								value : 4,
							},
							{
								label : '5h',
								value : 5,
							},
							{
								label : '6h',
								value : 6,
							},
							{
								label : '7h',
								value : 7,
							},
							{
								label : '8h',
								value : 8,
							},
							{
								label : '9h',
								value : 9,
							},
							{
								label : '10h',
								value : 10,
							},
							{
								label : '11h',
								value : 11,
							},
							{
								label : '12h',
								value : 12,
							},
							{
								label : '13h',
								value : 13,
							},
							{
								label : '14h',
								value : 14,
							},
							{
								label : '15h',
								value : 15,
							},
							{
								label : '16h',
								value : 16,
							},
							{
								label : '17h',
								value : 17,
							},
							{
								label : '18h',
								value : 18,
							},
							{
								label : '19h',
								value : 19,
							},
							{
								label : '20h',
								value : 20,
							},
							{
								label : '21h',
								value : 21,
							},
							{
								label : '22h',
								value : 22,
							},
							{
								label : '23h',
								value : 23,
							},
						]
					}		
				}				
			},
			{
				row   : 6,
				col   : 1,
				field : {
					type     : FieldType.type('MultiSelect'),
					label    : 'Minutos',
					name     : 'm',
					required : false,
					option  : {
						items : [
							{
								label : '0min',
								value : 0,
							},
							{
								label : '5min',
								value : 5,
							},
							{
								label : '10min',
								value : 10,
							},
							{
								label : '15min',
								value : 15,
							},
							{
								label : '20min',
								value : 20,
							},
							{
								label : '25min',
								value : 25,
							},
							{
								label : '30min',
								value : 30,
							},
							{
								label : '35min',
								value : 35,
							},
							{
								label : '40min',
								value : 40,
							},
							{
								label : '45min',
								value : 45,
							},
							{
								label : '50min',
								value : 50,
							},
							{
								label : '55min',
								value : 55,
							},
						]
					}
				}					
			},
			/*{
                row   : 7,
                col   : 1,
                field : new Field({
                    type : FieldType.type('Hidden'),
                    name : 'cron',
                })
            },*/
		]);
		
		items.doSort();

		await this.formItem.items.updateItems(items);
	}
	
	/*updateCron()
	{
		const control = this.field.formGroup;							
		console.error('------', control.value);
	}*/
}
