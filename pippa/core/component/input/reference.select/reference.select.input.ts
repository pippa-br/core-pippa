import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseReferenceInput } from "../base.reference.input";
import { InputValidate } 	  from "../../../validate/input.validate";
import { Viewer }             from "../../../model/viewer/viewer";
import { Document }           from "../../../model/document/document";
import { Types }              from "../../../type/types";
import { environment }        from "src/environments/environment";

@Component({
    selector        : ".reference-select-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
                                         'ng-valid' 	: formControl?.valid && this.hasValue(), 
                                         'ng-invalid'   : !formControl?.valid,
                                         'hidden'       : hidden }">
                             <div header-input
                                  [form]="form"
                                  [formItem]="formItem"
                                  (edit)="onEdit()"
                                  (del)="onDel()">
                            </div>
                            <div class="item-input"
                                 [formGroup]="field.formGroup"
                                 [ngClass]="{'has-add' : inputAdd && editable, 'has-set' : inputSet && editable && selected}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ng-select #select
                                           [(ngModel)]="selectedId"
                                           [ngModelOptions]="{standalone: true}"
                                           [disabled]="!editable || readonly"
                                           [searchable]="searchable"
                                           [items]="items"
                                           loadingText="Carregando..."
                                           minTermLength="3"
										   appendTo="body"
                                           [virtualScroll]="virtualScroll"
                                           [notFoundText]="getNotFoundText()"
                                           [loading]="isLoading"
                                           [placeholder]="getPlaceholder()"
                                           [bindLabel]="bindLabel"
                                           [bindValue]="bindValue"
                                           [searchFn]="customSearchFn"
                                           cancelText="Fechar"
										   okText="Selecionar"
										   (search)="onSearch($event)"
                                           (close)="onClose()"                                           
                                           (scrollToEnd)="onScrollToEnd()"
                                           (change)="onInput($event)">

                                        <ng-template ng-label-tmp let-item="item" let-clear="clear">
                                                <span class="ng-value-icon left" (click)="clear(item)" aria-hidden="true">×</span>
                                                <img *ngIf="hasPhoto" height="28" width="28" [src]="item|avatar:fieldPhoto|async"/>
                                                <span class="ng-value">{{item[bindLabel]}}</span>										
                                        </ng-template>

                                        <ng-template ng-option-tmp let-item="item" let-index="index" let-search="searchTerm">
                                            <div class="title-option">
                                                    <img *ngIf="hasPhoto" height="30" width="30" [src]="item|avatar:fieldPhoto|async"/>
                                                    <span>{{item[bindLabel]}}</span>
                                            </div>
                                            <small *ngIf="bindDescription">{{item[bindDescription]}}</small>
                                        </ng-template>

                                </ng-select>

								<icon-input *ngIf="inputSet && editable && selected"
											[isLoading]="isLoading"
											class="set-icon"
											[icon]="{ value : 'create-outline'}"
											(click)="addDocument(selected)">
								</icon-input>
                                
                                <icon-input *ngIf="inputAdd && editable"
                                            [isLoading]="isLoading"
											class="add-icon"
                                            [icon]="addIcon"
                                            (click)="addDocument()">
                                </icon-input>

                            </div>

							<div dynamic-viewer
								[viewer]="viewer"
								[data]="selected"
								(rendererComplete)="markForCheck();">
							</div>                                   

                            <error-input [singleText]="singleText" [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class ReferenceSelectInput extends BaseReferenceInput
{	
    @ViewChild("select", { static : false }) select : any;
    public viewer : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
				
        this.nameClass += this.formItem.name + "-input input col reference-select-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        if (this.setting.viewerPath)
        {
            const paths = this.setting.viewerPath.split("/");

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid       : paths[0],
                appid       : paths[1],
                colid       : paths[2],
                path        : this.setting.viewerPath,
                model       : Viewer,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                },
                map : true,
            })

            this.core().api.getObject(params).then(async (result:any) =>
            {
                this.viewer = result.data;
                this.markForCheck();
            });
        }
		
        if (this.setting.singleText)
        {
            this.singleText = this.setting.singleText;
        }
    }

    createValidators()
    {
        super.createValidators();

        this.validators      = [];
        this.asyncValidators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
            }

            this.asyncValidators.push(InputValidate.search(this.field, this.form, this));
            this.asyncValidators.push(InputValidate.custom(this.field, this.form, this));
        }
    }

    getNotFoundText()
    {        
        return this.term ? "Item não encontrado" : "Digite para buscar";
    }

    getPlaceholder()
    {
        return this.searchable ? "Digite para buscar" : "Selecione";
    }    

    /* OVERRIDE */
    doAdd(data:any)
    {
        this.populate(data);
    }

    /* OVERRIDE */
    doSet(data:any)
    {
        this.populate(data);
    }

    async onModelChange(data:any)
    {        
        if (data && !this.dirty)
        {
            this.dirty = true;

            if (data.referencePath)
            {
                data = new Document(data);
                await data.on();
				
                this.updateValue(data);
            }
            else
            {
                this.selected   = data; // SEMPRE SÃO REFERENCIAS
                this.selectedId = data.id;
            }            

            // FAZER ANTES DO LABEL
            this.displayControlByValue(data);

            await this.doLabels();	

            /* INVERSED BY */
            //this.updateInversedBy(data, 'inversedBy');

            /* INIT INVERSED BY */
            //this.updateInversedBy(data, 'initInversedBy');                        
	
            this.markForCheck();
        }        
    }
}
