import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { InputValidate } from '../../../validate/input.validate';

@Component({
    selector : `.zipcode-br-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted,
					 			 'ng-valid' 	: formControl?.valid && this.hasValue(), 
								  'ng-invalid'  : !formControl?.valid,
								  'hidden'      : hidden }">
                     <div header-input
                          [form]="form"
                          [formItem]="formItem"
                          (edit)="onEdit($event)"
                          (del)="onDel()">
                     </div>
                     <div class="item-input" [formGroup]="field.formGroup">

                         <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                         <input type="tel"
                                [ngModel]="value"
                                [ngModelOptions]="{standalone: true}"
                                (keyup)="onInput($event)"
                                [mask]="getMask()"
								[readonly]="!editable"
                                [placeholder]="field.placeholder | translate"/>

                         <icon-input [isLoading]="isLoading"
                                     [icon]="field.getIcon()">
                        </icon-input> 
                     </div>
                     <error-input [control]="formControl" [submitted]="submitted">
                        <span *ngIf="hasError('zipcode')">{{'CEP inválido' | translate}}</span>
                     </error-input>
                 </div>` 
})
export class ZipcodeBRInput extends BaseTextInput
{
	public hasMask : boolean = true;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
	}
	
	getMask() 
	{
		if(this.hasMask) 
		{
			return '00000-000';
		}
		else
		{
			return;
		}
	}

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col cep-field ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);

				if(this.hasMask)
				{
					this.validators.push(InputValidate.zipcode);
				}				
			}	
		}
	}
}
