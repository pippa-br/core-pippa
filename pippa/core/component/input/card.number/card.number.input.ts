import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput }     from '../base.text.input';
import { InputValidate } from '../../../validate/input.validate';

@Component({
    selector 		: `.card-number-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
						[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
						<div header-input
							[form]="form"
							[formItem]="formItem"
							(set)="onSet($event)"
							(del)="onDel()">
						</div>
						<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

							<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

							<input type="tel" 
								   [placeholder]="field.placeholder | translate"
								   [mask]="mask"
								   [ngModel]="value"									
								   [ngModelOptions]="{standalone: true}"
								   [readonly]="!editable"
								   (keyup)="onInput($event)"/>

							<icon-input [isLoading]="isLoading"
										[icon]="field.getIcon()">
							</icon-input>
						</div>
						<error-input [control]="formControl" [submitted]="submitted">
							<span *ngIf="hasError('creditCard')">{{'Cartão Inválido' | translate}}</span>
						</error-input>
					</div>`
})
export class CardNumberInput extends BaseTextInput
{
	public mask     : string  = '9{20}';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass  = this.formItem.name + '-input input col credit-field ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
        //this.width      = this.domSanitizer.bypassSecurityTrustStyle(this.field.width + '%');
    }

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
				this.validators.push(InputValidate.creditCard);
			}	
		}			
	}

    onInput(event:any)
    {
        const number = event.srcElement.value;
		this.setValue(number);
	}

	onModelChange(value:any)
    {
		if(!this.dirty && value)
		{
			const r = /\b(?:\d{4}[ -]?){3}(?=\d{4}\b)/gm;
			const s = `**** **** **** `;
			value   = value.replace(r, s);
			
			this.value = value;
			this.dirty = true;
		}
    }
}
