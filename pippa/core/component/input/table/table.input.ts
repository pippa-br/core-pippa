import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput }   from '../../input/base.input';
import { BaseList }    from '../../../model/base.list';

@Component({
    selector        : `.table-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (set)="onSet($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">
                                
															<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
															
																<table class="striped list">
																<tr>
																	<td *ngFor="let item of cols; let last = last;">
																		<div class="buttons-div">
																			<input type="text"
																					[(ngModel)]="item.value"
																					[ngModelOptions]="{standalone: true}"
																					(ngModelChange)="updateTable()"
																					placeholder="Valor"/>
																			<ion-button (click)="onColDel(item)">
																				<ion-icon name="remove-outline"></ion-icon>
																			</ion-button>
																		</div>
																	</td>
																	<td class="buttons-col">
																		<div class="buttons-div">
																			<ion-button (click)="onColAdd()">
																				<ion-icon name="add-outline"></ion-icon>
																			</ion-button>
																		</div>
																	</td>
																</tr>
																		<tr *ngFor="let item of rows; let last = last; let i = index;">
																				<td *ngFor="let item2 of item.items; let last2 = last; let k = index;">
																						<div>
																							<input type="text"
																								[(ngModel)]="item2.value"
																								[ngModelOptions]="{standalone: true}"
																								(ngModelChange)="updateTable();onFocus(i, k);"
																								#inputTable 
																								placeholder="Valor"/>
																						</div>
																				</td>
																				<td *ngIf="item.items.length == 0" [attr.colspan]="cols.length">
																				</td>
																				<td class="buttons-col">
																					<div class="buttons-div">
																						<ion-button *ngIf="last" (click)="onRowEmpty(item)">
																							<ion-icon name="return-down-back-outline"></ion-icon>
																						</ion-button>	
																						<ion-button (click)="onRowDel(item)">
																							<ion-icon name="remove-outline"></ion-icon>
																						</ion-button>
																						<ion-button *ngIf="last" (click)="onRowAdd()">
																							<ion-icon name="add-outline"></ion-icon>
																						</ion-button>
																					</div>
																				</td>
																		</tr>
																		<tr *ngIf="rows.length == 0" class="empty-row" (click)="onRowAdd()">
																				<td [attr.colspan]="cols.length">
																					<div>
																						<label>Nenhum item cadastrado!</label>
																					</div>											
																				</td>
																				<td class="buttons-col">
																					<div class="buttons-div">
																						<ion-button>
																							<ion-icon name="add-outline"></ion-icon>
																						</ion-button>
																					</div>
																		</td>
																</tr>
																</table>
															<error-input [control]="formControl" [submitted]="submitted"></error-input>

														</div>
                        </div>`
})
export class TableInput extends BaseInput
{
		@ViewChildren('inputTable') inputFields: QueryList<ElementRef>;
    public cols : BaseList<any> = new BaseList<any>();
		public rows : BaseList<any> = new BaseList<any>();
		//public countEmpty : number = 0;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-options input col table-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

				this.cols.add({
					id 	   : this.core().util.randomString(8),
					value : "A"
				});
				this.cols.add({
					id 	   : this.core().util.randomString(8),
					value : "B"
				});
				this.cols.add({
					id 	   : this.core().util.randomString(8),
					value : "C"
				});
    }

		createValidators()
		{
				this.validators = [];

				if(this.formItem.required)
				{
						this.validators.push(Validators.required);
				}
		}

		onFocus(rowIndex: number, cellIndex: number) 
		{
				let countEmpty = 0;

				for(let i = this.rows.length; rowIndex < i; i--)
				{
						if(this.rows[i].items.length == 0)
						{
								countEmpty++;
						}
				}

				const flattenedIndex = (rowIndex - countEmpty) * this.rows[0].items.length + cellIndex;

				setTimeout(() => {
					const inputField = this.inputFields.toArray()[flattenedIndex];
					inputField.nativeElement.focus();
				});
		}

    onModelChange(value:any)
    {
				//console.error('xxx', JSON.stringify(value));

				// 	DEPOIS REMOVER SIMPLE LIFE
				if(this.isSetModeForm() && value === undefined)
				{
						value = [{"items":[{"label":"Tamanho"},{"label":"Busto"},{"label":"Cintura"},{"label":"Quadril"},{"label":"Comprimento"}]},{"items":[{"label":"Tamanho"},{"label":"Busto"},{"label":"Cintura"},{"label":"Quadril"},{"label":"Comprimento"}]},{"items":[{"label":"Tamanho"},{"label":"Busto"},{"label":"Cintura"},{"label":"Quadril"},{"label":"Comprimento"}]},{"items":[{"label":"Tamanho"},{"label":"Busto"},{"label":"Cintura"},{"label":"Quadril"},{"label":"Comprimento"}]},{"items":[{"label":"Tamanho"},{"label":"Busto"},{"label":"Cintura"},{"label":"Quadril"},{"label":"Comprimento"}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[]},{"items":[]},{"items":[{"label":"Tamanho","value":"Tamanho"},{"label":"Busto","value":"Cintura"},{"label":"Cintura","value":"Quadril"},{"label":"Quadril","value":"Comprimento"},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]},{"items":[{"label":"Tamanho","value":""},{"label":"Busto","value":""},{"label":"Cintura","value":""},{"label":"Quadril","value":""},{"label":"Comprimento","value":""}]}];
				}
			
        if(value && value.length > 0 && !this.dirty)
        {
						this.dirty = true;

            this.cols = new BaseList<any>();
						this.rows = new BaseList<any>();						

            for(let key in value)
            {
								let i       = 0;
								const items = new BaseList();
								
								for(const key2 in value[key].items)
								{
									if(key == '0')
									{
										this.cols.add({
											id    : this.core().util.randomString(8),
											value : value[key].items[key2].label,
										});		
									}

									items.add(Object.assign({}, {
										id    : this.cols[i].id,
										value : value[key].items[key2].value,
									}))

									i++;
								}
								
								this.rows.add({
									id    : this.core().util.randomString(8),
									items : items
								});
            }            
        }
    }

    onRowAdd()
    {
				const items = new BaseList();

				for(const id of this.cols)
				{
					items.add(Object.assign({}, {
						id    : id,
						value : "",
					}))
				}

        this.rows.add({
            id    : this.core().util.randomString(8),
            items : items
        });
    }

    onRowDel(item:any)
    {
				//  if(item.items.length == 0)
				//  {
				// 		this.countEmpty--;
				//  }

        this.rows.del(item);
        this.updateTable();
    }

		onRowEmpty(item:any)
		{
				this.rows.add({
            id    : this.core().util.randomString(8),
            items : []
        });

				//this.countEmpty++;

        this.updateTable();
		}

		onColAdd()
    {
				let id = this.core().util.randomString(8);

				this.cols.add({
					id	  : id,
					value : "",
				});

						for(const item of this.rows)
				{
					item.items.add({
						id    : id,
						value : "",
					})
				}
    }

    onColDel(item:any)
    {		
				for(const row of this.rows)
				{
					row.items.del(item);
				}

				this.cols.del(item);

        this.updateTable();
    }

    updateTable()
    {
			const datas = [];

        for(let item of this.rows)
        {
						let data = [];

						for(let key in item.items)
						{
							const item2 = item.items[key];
							const col   = this.cols[key];
							let value   = item2.value;
							
							if(item2.value !== '')
							{
								if(value == 'false')
								{
									value = false;
								}
								else if(value == 'true')
								{
									value = true;
								}
								else if(this.core().util.isNumber(value))
								{
									value = Number(value);
								}					
							}

							data.push({
								label : col.value,
								value : value
							});
						} 

						datas.push({
							items : data
						});
					}
		
        this.setValue(datas);
    }
}
