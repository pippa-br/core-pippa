import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { FormControl } from "@angular/forms";

/* PIPPA */
import { BaseInput } from "../base.input";
import { Document }  from "../../../model/document/document";

@Component({
    selector        : ".reference-checkbox-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper ng-valid"
                        [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid, 'ng-invalid' : !formControl?.valid }">
                        <div header-input
                            [onlyButtons]="true"
                            [form]="form"
                            [formItem]="formItem"
                            (edit)="onEdit($event)"
                            (del)="onDel()">
                        </div>
                        <div class="item-input" [formGroup]="field.formGroup">

                            <input id="{{labelId}}"
                                    class="checkbox-item"
                                    type="checkbox"
                                    formControlName="{{formItem.name}}"
                                    [(ngModel)]="selected"
                                    [readonly]="!editable || readonly"
                                    (ngModelChange)="onModelChange($event)"
                                    (change)="onInput()">

                            <label for="{{labelId}}" [ngClass]="{ 'disabled-checkbox': !editable || readonly }">{{field.label}}</label>

                        </div>
                        <error-input [control]="formControl" [submitted]="submitted">
                        </error-input>
                    </div>`
})
export class ReferenceCheckboxInput extends BaseInput
{
    public labelId  = "";
    public checked  = false;
    public selected = null;
    public model    = null;
    public label    = "";

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col reference-checkbox-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
        this.labelId    = this.formItem.id + "-" + new Date().getTime();

        if (this.setting.referencePath)
        {
            this.model = new Document({
                referencePath : this.setting.referencePath
            });
            await this.model.on();      
            
            this.label = this.model.name;
        }
        else if (this.setting.user)
        {
            this.label = this.core().user.name;
            this.model = this.core().user
        }
        else
        {
            this.label = this.field.label;
        }

        if (this.setting.checked)
        {
            this.checked = this.setting.checked;
        }
    }

    onModelChange(value:any)
    {
        if (!this.dirty && value)
        {
            this.dirty = true;
            this.updateValue(value);
            this.selected = value;            
        }
    }

    onInput()
    {
        /* O EVENTO É DISPARADO E DEPOIS TROCA O VALOR POR ISSO ! */
        if (!this.getValue())
        {
            this.setValue(this.model.reference);
            this.selected = this.model.reference;
        }
        else
        {
            this.setValue(this.valueDefault());
            this.selected = null;
        }
    }

    valueDefault()
    {
        if (this.checked)
        {
            return this.model.reference;
        }
        else
        {
            return null;
        }        
    }
}
