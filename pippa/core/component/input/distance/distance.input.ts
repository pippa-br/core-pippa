import { Component, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput }  from '../base.text.input';
import { InputValidate }  from '../../../validate/input.validate';
import { GooglePlugin }   from '../../../plugin/google/google.plugin';

@Component({
    selector        : `.distance-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										 'ng-valid' 	: formControl?.valid && this.hasValue(), 
										 'ng-invalid'   : !formControl?.valid,
										 'hidden'       : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit()"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="text"
                                       autocomplete="false"
                                       [ngModel]="info"
                                       [ngModelOptions]="{standalone: true}"
                                       [readonly]="true"
                                       [placeholder]="field.placeholder"/>

                                <icon-input [isLoading]="isLoading"
                                            [icon]="field.getIcon()">
                                </icon-input>
                            </div>
							<error-input [control]="formControl" [submitted]="submitted">
								<span *ngIf="hasError('distanceError')">Distancia não permitida!</span>
                            </error-input>
                        </div>`
})
export class DistanceInput extends BaseTextInput implements AfterViewInit
{
	public info    : string;
	public timeAPI : any;

    constructor(
		public googlePlugin      : GooglePlugin,
        public changeDetectorRef : ChangeDetectorRef,
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col distance-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';				
	}

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}	
		}

		this.asyncValidators = [InputValidate.search(this.field, this.form, this)];
	}
	
	ngAfterViewInit()
    {
		super.ngAfterViewInit();

        if(this.formGroup.get('address'))
        {			
			this.formGroup.get('address').instance.field.onComplete = ((value:any) =>
            {	
				this.delError('distanceError');

				if(this.timeAPI)
				{
					clearTimeout(this.timeAPI);
				}

				this.timeAPI = setTimeout(() => 
				{
					this.googlePlugin.getDistancePrice(this.core().account.address, value).then(async (data) => 
					{
						if(data && data.valid)
						{
							this.info = data.label;
							this.setValue(data);
						}
						else
						{
							this.info = data.label;
							this.setValue(null);
							this.setError('distanceError');
						}					
	
						//this.checkValidate();
					});
				}, 250);
            });
        }
	}
	
	onModelChange(data : any)
    {
        if(data && !this.dirty)
        {
			this.dirty = true;
			this.info  = data.label;
        }
    }
}
