import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseReferenceInput } from '../base.reference.input';

@Component({
    selector : `.reference-auto-fill`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input
                     [form]="form"
                     [formItem]="formItem"
                     (edit)="onEdit()"
                     (del)="onDel()">
                </div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden"
                           formControlName="{{formItem.name}}"
                           (ngModelChange)="onModelChange($event)"/>
                    <input type="text" readonly [value]="selected?._label"/>
                </div>`
})
export class ReferenceAutoFillInput extends BaseReferenceInput
{
    public fieldFunction    : any;
    public changeReferences : any;
    public changeControls   : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col reference-auto-fill-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

        /* LABEL FIELD */
        if(this.setting.fieldFunction)
        {
            this.fieldFunction = this.setting.fieldFunction;
        }

        /* CHANGE FIELD */
        if(this.setting && this.setting.changeReference)
        {
            this.changeReferences = this.setting.changeReference.split(',');
            this.changeControls   = [];

            for(const key in this.changeReferences)
            {
                this.changeControls[key] = this.getFormControl(this.changeReferences[key]);

                if(this.changeControls[key])
                {
                    this.changeControls[key].registerOnChange((value:any) =>
                    {
                        this.changeReference(value);
                    });
                }
            }
        }
    }

	createValidators()
	{
		super.createValidators();
		
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }		
	}

    changeReference(value:any)
    {
        value;
        this.changeItems();
    }

    changeItems()
    {
        this.selected = null;
        this.setValue(null);

        if(this.fieldFunction)
        {
            setTimeout(() =>
            {
                this.core().functions[this.fieldFunction]({
                    items : this.items,
                    data  : this.parseData(),
                })
                .then((data:any) =>
                {
                    data.getPathValue(this.fieldLabel).then((label:string) =>
                    {
                        data._label   = label;
                        this.selected = data;

                        this.setValue(data.reference);
                    });
                });
            });
        }
    }
}
