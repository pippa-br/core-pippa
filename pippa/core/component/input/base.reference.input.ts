import { OnInit, ChangeDetectorRef } from "@angular/core";
import { Subject } from "rxjs";
import { ModalController } from "@ionic/angular";
import { DocumentReference } from "firebase/firestore";

/* PIPPA */
import { BaseInput }       from "./base.input";
import { WhereCollection } from "../../../core/model/where/where.collection";
import { DocumentPlugin }  from "../../../core/plugin/document/document.plugin";
import { Document }  	   from "../../../core/model/document/document";
import { FormPlugin }      from "../../../core/plugin/form/form.plugin";
import { FormFormPopover } from "../../../core/popover/form.form/form.form.popover";
import { BaseList }        from "../../../core/model/base.list";
import { environment } from "src/environments/environment";
import { Types } from "../../type/types";
import { Form } from "../../model/form/form";
import { BaseModel } from "../../model/base.model";
import { CdkDragDrop, CdkDragEnter, moveItemInArray } from "@angular/cdk/drag-drop";

export abstract class BaseReferenceInput extends BaseInput
{
    public items         : Array<any>;
    public itemsSelected : Array<any> = [];
    public selectedId  	 : any;
    public selected   	 : any;
    public groupBy    	 : string;
    public collection 	 : any;
    public fieldLabel 	  = "${name}";
    public fieldPhoto 	  = "photo";
    public fieldData  	 : any; // PEGA UM CAMPO DE UMA COLLECTION PARA SER A COLLECTION RELACIONADA
    public orderBy    	  = "name";
    public where      	 : any;
    public term       	  = "";
    //public filters     : string = '';
    public app        	 : any;
    public forms      	 : any;
    public inputAdd   	  = false
    public inputSet   	  = false
    public hasPhoto   	  = false;
    public searchable 	  = true;
    public isInfinite   = true;
    public asc  	    = true;
    public lowerCase    = true;
    public controlPath : string;
    public accidMaster  = false;
    public groupCollection  = false;
    public onlyMaster       = false;
    public onlySearch       = false;
    public selectedSort     = false;
    public selectedPosition : number;
    public typeObject      = "Array";
    public mapItems    : any;
    public addIcon = { value : "add-circle-outline" };

    public loading = false;
    public virtualScroll  = true;
    public startAt : any;
    public endAt   : any;
    public plugin  : any;
    public perPage : number;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.plugin = this.getPlugin();
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);	
    }

    createValidators()
    {
        this.where = null;

        /* FIELD LABEL */
        if (this.setting.fieldLabel)
        {
            this.fieldLabel = this.setting.fieldLabel;
        }

        /* FIELD LABEL */
        if (this.setting.fieldPhoto)
        {
            this.fieldPhoto = this.setting.fieldPhoto;
        }

        /* FIELD DATA */
        if (this.setting.fieldData)
        {
            this.fieldData = this.setting.fieldData;
        }
		
        /* ORDER BY */
        if (this.setting.orderBy)
        {
            this.orderBy = this.setting.orderBy;
        }

        /* MAPITEMS */
        if (this.setting.mapItems)
        {
            this.mapItems = this.setting.mapItems;
        }

        /* GROUP COLLECTION */
        if (this.setting.groupCollection)
        {
            this.groupCollection = this.setting.groupCollection;
        }		

        /* ONLY MASTER */
        if (this.setting.onlyMaster)
        {
            this.onlyMaster = this.setting.onlyMaster;
        }
		
        /* ONLY SEARCH */
        if (this.setting.onlySearch)
        {
            this.onlySearch = this.setting.onlySearch;
        }

        /* ASC */
        if (this.setting.asc !== undefined)
        {
            this.asc = this.setting.asc;
        }		

        /* HITSPERPAGE */
        if (this.setting.perPage)
        {
            this.perPage = this.setting.perPage;
        }
        else
        {
            this.perPage = 24;
        }

        /* HAS SEARCH */
        if (this.setting.searchable != undefined)
        {
            this.searchable = this.setting.searchable == "true";
        }

        /* ADD */
        if (this.setting.inputAdd)
        {
            this.inputAdd = true;				
        }

        /* SET */
        if (this.setting.inputSet)
        {
            this.inputSet = true;
        }
		
        /* PHOTO */
        if (this.setting.hasPhoto)
        {
            this.hasPhoto = true;
        }

        /* CONTROL PATH */
        if (this.setting.controlPath)
        {
            this.controlPath = this.setting.controlPath;
        }

        /* ACCID MASTER PATH */
        if (this.setting.accidMaster)
        {
            this.accidMaster = this.setting.accidMaster;
        }

        /* ACCID MASTER PATH */
        if (this.setting.selectedPosition !== undefined)
        {
            this.selectedPosition = this.setting.selectedPosition;
        }

        /* CRIAR UM FILTER */
        if (this.formItem.filters)
        {
            this.where = this.formItem.filters;			
        }
        else
        {
            this.where = new WhereCollection();
        }

        /* TYPE OBJECT */
        if (this.setting.typeObject)
        {
            this.typeObject = this.setting.typeObject;
        }

        // ORDERNAÇÃO COM THUMBS
        if (this.setting.selectedSort !== undefined)
        {
            this.selectedSort = this.setting.selectedSort;
        }

        if (this.setting.formPath)
        {
            const paths = this.setting.formPath.split("/");

            const params = this.createParams({
                getEndPoint : Types.GET_DOCUMENT_API,
                accid       : paths[0],
                appid       : paths[1],
                colid       : paths[2],
                path        : this.setting.formPath,
                model       : Form,
                mapItems    : {
                    referencePath : environment.defaultMapItems
                }
            })

            this.core().api.getObject(params).then((result:any) =>
            {
                this.forms = [ result.data ];
                this.markForCheck();
            });
        }
    }

    async doLabels()
    {
        this.isLoading = true;
        await this.mergeSelected();

        if (this.collection)
        {
            const items     = [];
            const labels    = [];
            const promisses = [];					

            for (const item of this.collection)
            {
                items.push(item);
                promisses.push(item.on());				
            }

            await Promise.all(promisses);

            for (const key in items)
            {
                if (this.fieldData)
                {
                    items[key] = await items[key].getProperty(this.fieldData);

                    if (!(items[key] instanceof BaseModel))
                    {
                        items[key] = new BaseModel(items[key]);
                    }
                }

                if (items[key])
                {					
                    labels.push(this.core().util.parseVariables(items[key], this.fieldLabel));	
                }
            }

            const values = await Promise.all(labels);

            for (const key in values)
            {
                items[key]._label = values[key];
            }

            this.items = items;

            this.changeItems();
            this.isLoading = false;
            this.markForCheck();
        }		
    }

    async doSelectedLabels()
    {
        if (this.selectedSort)
        {
            if (this.selected)
            {
                this.isLoading = true;
				
                const items     = [];
                const labels    = [];
                const promisses = [];

                for (let item of this.selected)
                {
                    if (!(item instanceof BaseModel))
                    {
                        item = new BaseModel(item);
                    }

                    items.push(item);
                    promisses.push(item.on());				
                }

                await Promise.all(promisses);

                for (const item of items)
                {
                    labels.push(this.core().util.parseVariables(item, this.fieldLabel));	
                }

                const values = await Promise.all(labels);

                for (const key in values)
                {
                    items[key]._label = values[key];
                }		
			
                this.itemsSelected = items;	
				
                this.markForCheck();

                this.isLoading = false;
            }
        }
    }

    //https://stackblitz.com/edit/angular-drag-drop-flex-wrap?file=src%2Fapp%2Fapp.component.html,src%2Fapp%2Fapp.component.ts
    //https://stackblitz.com/edit/angular-cdk-drag-drop-sortable-flex-wrap?file=src%2Fapp%2Fapp.component.ts
    async drop(event: CdkDragDrop<any>) 
    {
        moveItemInArray(this.selected, event.previousContainer.data.index, event.container.data.index);
        moveItemInArray(this.itemsSelected, event.previousContainer.data.index, event.container.data.index);  

        this.setValue(this.selected);
    }
	
    async loadOption()
    {
        const collection = await this.getCollection();

        //console.error('-----', this.field.name, this.collection);
        
        // NÃO PODE SER MARIO QUE ZERO collection.length > 0 pois não cria a collection
        //tem varios if verifcando a collectio no caso siblereference
        if (collection)
        {                       
            await collection.on(true);
   
            this.collection            = collection;
            this.collection.isInfinite = this.isInfinite;								

            this.subscriptions.add(this.collection.onUpdate().subscribe(async () =>
            {
                await this.doLabels();
            }));

            // CASO TENHA TIDO ALTERAÇÃO NO sibling enquanto carregava
            if (this.siblingReferences)
            {
                await this.reload(); 
            }
			
            // AUTO SELECT BY POSITION
            if (this.form.isAddModeForm() && this.selectedPosition !== undefined && this.collection[this.selectedPosition])
            {
                this.populate(this.collection[this.selectedPosition].reference);
            }
			
            await this.doLabels();

            /* PARA TRAZER DADOS INICIAIS */
            //this.onSearch({ term : ''});
        }
    }

    /*rendererComplete()
	{
		super.rendererComplete();
	}*/

    /* OVERRIDER */
    async reload()
    {	
        return new Promise(async (resolve:any) =>
        {
            if (this.collection)
            {
                const where = await this.getWhere();

                this.isLoading               = true;
                this.collection._query.where = where;
                this.collection.search(this.term, this.lowerCase);
            }
			
            resolve();
        })
    }

    async getCollection()
    {
        /* BY APP */
        if (this.option && this.option["app"])
        {                
            //this.option['app']._isPopulate = false;
            this.isLoading = true;
            this.app       = this.option["app"];

            if (!this.app.code)
            {
                this.option["app"]._isPopulate = false;
                await this.option["app"].doSnapshot();
            }
			
            //this.option.getProperty('app', true).then((app:any) =>
            //{
            /* DEPOIS REVER */
            //app.on().then(() => 
            //{
					

            //const plugin     = this.getPlugin();
            //const collection = new DocumentCollection();
            //collection.appid = app.code;

            //resolve(collection);

            /* PRECISA DA REFERENCIA DO FIREBASE POIS PRECISAMOS CRIAR DOCUMENTOS COM ACOES NO INPUT */
					
            const where = await this.getWhere();
					
            const params = this.createParams({
                appid         		: this.app.code,
                perPage       		: this.perPage,
                onlyMaster    		: this.onlyMaster,
                groupCollection : this.groupCollection,
                where         		: where,
                orderBy       		: this.orderBy,
                asc           		: this.asc, 
            });

            if (this.accidMaster)
            {
                params.accid = this.core().masterAccount.accid;
            }

            if (this.mapItems)
            {
                params.mapItems = {
                    referencePath : this.mapItems
                };
                params.map      = true;
            }
            else if (environment.defaultMapItems)
            {
                params.mapItems = {
                    referencePath : environment.defaultMapItems
                };
                params.map      = true;
            }

            let collection = null;

            if (this.onlySearch)
            {
                collection = this.plugin.createCollection(params);
            }		
            else
            {
                const result = await this.plugin.getData(params);
                collection   = result.collection;						
            }			
					
            collection.appid = this.app.code;
            this.isLoading   = false;

            // AUTO SELECT BY POSITION
            if (this.form.isAddModeForm() && this.selectedPosition !== undefined && this.collection && this.collection[this.selectedPosition])
            {
                this.updateValue(this.collection[this.selectedPosition]);
            }

            return collection;
            //})                    
            //});
        }
		
        return;
    }
	
    /* PARA METODOS EXTERNOS COMO siblingAction */
    async setCollection(items)
    {
        const maps = this.plugin.getMaps();

        this.collection = new maps.list(items);
        await this.collection.on();

        await this.doLabels();	
    }

    getPlugin()
    {
        return this.core().injector.get(DocumentPlugin);
    }

    changeItems()
    {
    }

    customSearchFn = (term: string, item: any) =>
    {
        term = term.toLocaleLowerCase();

        for (const key in item)
        {
            if (typeof item[key] == "string")
            {
                if (item[key].toLocaleLowerCase().indexOf(term) > -1)
                {
                    return true
                }                
            }
            else if (item[key] instanceof DocumentReference)
            {
				
            }
            else if (typeof item[key] == "object")
            {
                const value = this.customSearchFn(term, item[key]);

                if (value)
                {
                    return true;
                }
            }
        }
    }
	
    async mergeSelected()
    {
        if (this.selected && this.collection)
        {
            let has = false;

            for (const key in this.collection)
            {
                let data = this.collection[key];
				
                if (this.fieldData)
                {
                    data = await data.getProperty(this.fieldData);
                }

                /* VERIFICA SE JA EXISTE */
                if (this.selected.id == data.id)
                {
                    has = true;
                    break;
                }
            }

            if (!has)
            {		
                let data = this.selected;				

                if (this.fieldData)
                {
                    data                 = {};
                    data[this.fieldData] = this.selected;
                }

                this.collection._set(data);
            }
        }
    }

    async getWhere()
    {
        let where : any;

        if (!(this.where instanceof WhereCollection))
        {
            this.where = new WhereCollection(this.where);
        }
		
        await this.where.on();
        where = this.where.copy();

        if (this.siblingReferences)
        {
            for (const key in this.siblingReferences)
            {
                const operator = this.siblingOperators[key];
                const value    = this.siblingValues[key]
                const type     = this.siblingTypes[key]
                const name     = this.siblingReferences[key];

                /* IN NÃO ACEITA ARRAY VAZIOS */
                if (operator == "in" && value && value.length == 0)
                {
                    value.push(null);
                }
					
                const data = {
                    name     : name,
                    field    : this.siblingFields[key].replace("root.", ""),
                    operator : operator,
                    value    : value,
                    type     : type,
                };

                where.add(data);

                console.log("getWhere", this.formItem.name, where);
            }


        }

           
		
        return where;
    }

    onScrollToEnd()
    {
        if (this.collection && this.collection.hasNextPage())
        {
            this.collection.nextPage();
        }
    }

    onInput(item:any):any
    {
        this.setValue(item);
    }

    /* METODO USADO PARA CHAMADAS EXTERNAS, PASSANDO REFERENCE  */
    async populate(data:any)
    {
        const maps = this.plugin.getMaps();

        if (!data || data instanceof Array)
        {
            this.updateValue(data);
            await this.doLabels();

            return data;
        }
        else if (typeof data == "string")
        {
            const model = new maps.model(this.core().fireStore.doc(data).ref);
            await model.on(true);
			
            this.updateValue(model);
            await this.doLabels();

            return model;
        }
        else
        {
            const model = new maps.model(data);
            await model.on(true);
			
            this.updateValue(model);
            await this.doLabels();

            return model;
        }              
    }
	
    updateValue(data:any)
    {		
        if (!data)
        {
            this.selected   = null;
            this.selectedId = null;   
            super.updateValue(null);         
        }
        else
        {
            if (data instanceof Array)
            {
                if (data.length > 0)
                {
                    const references = [];
                    const ids        = [];
                    const selected 	 = new BaseList();
	
                    for (const key in data)
                    {
                        const item = data[key];
	
                        if (item instanceof DocumentReference)
                        {
                            references.push(item);
                        }					
                        else if (item.reference)
                        {						
                            references.push(item.reference);
                        }
                        else if (item instanceof Document)
                        {
                            references.push(item.parseData());
                        }
                        else
                        {
                            references.push(item);
                        }
						
                        ids.push(item[this.bindValue]);
                        selected.add(item);
                    }
	
                    this.selectedId = ids;
                    this.selected   = selected;
	
                    if (this.typeObject == "Array")
                    {
                        super.updateValue(references);
                    }
                    else
                    {
                        // SALVA O OBJECT EM VEZ DE ARRAY
                        const dataReferences = {
                            _dataReferences : true
                        };
	
                        for (const key in references)
                        {
                            const item              = references[key];
                            dataReferences[item.id] = item;
                        }
	
                        super.updateValue(dataReferences);
                    }
                }
                else
                {
                    super.updateValue([]);
                }								
            }
            else if (data instanceof DocumentReference)
            {
                this.selected   = data;
                this.selectedId = data.id;

                super.updateValue(data);
            }
            else if (data._dataReferences)
            {
                const ids    = [];
                const _items = []; // POR CONTA DE TYPEOBJECT SER SALVO COMO OBJECT
	
                for (const key in data)
                {
                    if (key !== "_dataReferences")
                    {
                        ids.push(data[key][this.bindValue]);
                        _items.push(data[key]);	
                    }
                }
	
                this.selectedId = ids;
                this.selected   = _items; // SEMPRE SÃO REFERENCIAS

                super.updateValue(data);
            }
            else
            {
                this.selected   = data.reference;
                this.selectedId = data.id;

                super.updateValue(data.reference);
            }
        }

        this.doSelectedLabels();
    }

    /* OVERRIDER: SEMPRE PASSAR MODEL */
    setLog(data:any)
    {
        if (!data)
        {
            super.setLog(null);         
        }
        else
        {
            super.setLog(data.reference);
        }
    }

    /* OVERRIDE */
    /*populate(value:any)
	{
		if(this.collection)
		{
			this.collection.updateItems(value);
		}
	}*/
	
    reset()
    {
        if (!this.field.fixed)
        {
            this.populate(null);
        }

        this.dirty = false;
    }

    onValidate(event:any)
    {
        event;

        if (this.getValue() == "")
        {
            this.setValue(this.valueDefault());
        }
    }

    onClose()
    {
        /* DEPOIS DE SELECTIONAR LIMPAR A BUSCA */
        if (this.collection && this.term)
        {
            this.term = "";
            //this.collection.search();			
        }
    }

    siblingClear()
    {
        this.populate(null);
		
        this.dirty = false;
    }

    displayControlByValue(data:any)
    {
        if (this.isControlField)
        {
            this.displayFields(this.controlField, false); /* DESABILITA TUDO */ 
            
            if (this.controlPath && this.selected)
            {
                const model = new Document(this.selected);
				
                model.on().then(() => 
                {
                    model.getProperty(this.controlPath).then((value:string) => 
                    {
                        if (this.setting && this.setting[value] != undefined)
                        {
                            const field = this.setting[value];
			
                            this.displayFields(field, true); /* HABILITA FIELD */
                        }
			
                        //OLD this.controlField = data.value;
                        //this.displayFields(this.controlField, true); /* HABILITA O ATUAL */
			
                        this.markForCheck();        
                    });
                });                
            }
        }
    }
	
    onSearch(event:any)
    { 
        if (!event.term)
        {
            event.term = "";
        }									

        this.term = event.term;
		
        this.isLoading = true;
        this.collection.search(this.term);
    }

    doAdd(data:any)
    {
        data;
    }

    doSet(data:any)
    {
        data;
    }

    addDocument(data?:any)
    {
        if (this.inputAdd)
        {
            this.getForms().then(async () =>
            {
                const form = this.forms[0];

                if (data)
                {
                    data = new Document(data);
                    await data.on();	
                }
                else
                {
                    // ABRRI O FORM COM O RELACIONAMETO DO WHERE
                    if (this.siblingReferences)
                    {
                        const initialData = {};

                        for (const key in this.siblingReferences)
                        {
                            const name        = this.siblingReferences[key];
                            const value       = this.siblingValues[key]														
                            initialData[name] = value;
                        }



                        form.setting = Object.assign({}, form.setting, {
                            initialData : initialData
                        })
                    }


                }

                const modalController = this.core().injector.get(ModalController);

                modalController.create(
                    {
                        component : FormFormPopover,
                        componentProps :
                    {
                        data  : data,
                        form  : form,
                        acl   : this.form.instance.acl,
                        onAdd : (async (event:any) =>
                        {
                            const result = await this.plugin.add(this.app.code, this.forms[0], event);
                            
                            this.doAdd(result.data);
                            modalController.dismiss();
                        }),
                        onSet : (async (event:any) =>
                        {
                            const result = await this.plugin.set(this.app.code, data, this.forms[0], event);
                            
                            this.doSet(result.data);
                            modalController.dismiss();
                        }),
                        onClose : (() =>
                        {
                            modalController.dismiss();
                        }),
                    },
                        cssClass : "full",
                    })
                .then((popover:any) =>
                {
                    popover.present();
                });
            });
        }
    }

    getForms()
    {
        return new Promise<void>((resolve) =>
        {
            if (this.forms)
            {
                resolve();
            }
            else
            {
                /* GET FORM */
                const params = this.createParams({
                    appid : this.app.code
                });

                const formPlugin = this.core().injector.get(FormPlugin);

                formPlugin.getData(params).then((result:any) =>
                {
                    result.collection.on().then(() =>
                    {
                        this.forms = result.collection;

                        resolve();
                    });
                });
            }
        });
    }

    setItems()
    {		
        // OVERRIDER
    }
}
