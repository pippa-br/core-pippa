import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput }      from '../base.input';
import { IconType }       from '../../../type/icon/icon.type';
import { FileCollection } from '../../../model/file/file.collection';
import { File }           from '../../../model/file/file';
import { UploadPlugin }   from '../../../plugin/upload/upload.plugin';

@Component({
    selector        : `.select-icon-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)" />
                                    
                                <input #fileInput
                                        type="file"
                                        style="display: none;"
                                        [accept]="accept"
                                        (change)="onUpload($event)" />
                                        
                                <ng-select formControlName="{{formItem.name}}"
										   [items]="icons"
										   appendTo="body"
                                           [placeholder]="field.label"
                                           cancelText="Fechar"
										   okText="Selecionar"
										   (clear)="onReset()"
                                           (change)="onSelectIcon($event)">
                                    <ng-template ng-label-tmp let-item="item">
                                        <ion-icon *ngIf="item.value" name="{{item.value}}" class="icon icon-md item-icon"></ion-icon>
                                        <img *ngIf="item._url" src="{{item._url}}"/>
                                        &nbsp;&nbsp;{{item?.label}}
                                    </ng-template>
									<ng-template ng-option-tmp let-item="item">
										<div class="title-option">
											<ion-icon *ngIf="item.value" name="{{item.value}}" class="icon icon-md item-icon"></ion-icon>
											<img *ngIf="item._url" src="{{item._url}}"/>
											&nbsp;&nbsp;{{item?.label}}
										</div>
                                    </ng-template>
                                </ng-select>

                                <a (click)="onAdd()">
                                    <ion-icon name="add-outline"></ion-icon>
                                </a>
                                
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                            </error-input>
                        </div>`
})
export class SelectIconInput extends BaseInput
{    
    @ViewChild('fileInput', {static: false}) fileInput: ElementRef;

    public files      : FileCollection;
    public invalidUrl : boolean        = false;
    public accept     : string         = 'jpg,png,sgv';
    public icons      : FileCollection;

    constructor(
        public uploadPlugin      : UploadPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col select-icon-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

		this.icons = new FileCollection();
		this.files = new FileCollection(); 

        for(const key in IconType)
        {
            const data = IconType[key];
            data.type  = File.ICON
            this.icons.add(data);
        }
    }

	createValidators()
	{
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onAdd()
    {
        this.fileInput.nativeElement.click();
    }

    onSelectIcon(file)
    {
        if(file)
        {
            this.files.set(file);
        }
        else
        {
            this.files.delAll();
        }        

        this.updateFiles();
    }

    onUpload(event:any)
    {
        this.uploadPlugin.uploadMultiple(event.target.files).then((files:any) =>
        {
            this.files.setItems(files);
            this.updateFiles();

            this.clearFileInput();
        });
    }

    reset()
    {
		this.clearFileInput();
		this.files = new FileCollection();
		
		this.updateFiles();
        this.dirty = false;
    }

	clearFileInput()
	{
		if(this.fileInput)
		{
			this.fileInput.nativeElement.value = "";
		}		
	}

    updateFiles()
    {
        if(this.files.length > 0)
        {
            this.setValue(this.files[0].parseData());
        }
        else
        {
            this.setValue(null);
		}
		
        this.invalidUrl = false;
    }
}
