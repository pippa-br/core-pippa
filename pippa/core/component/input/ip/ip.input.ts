import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `.ip-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
							<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
					  </div>`
})
export class IPInput extends BaseInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col ip-input ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';			
    }

    /* NAO RESETAR QUANDO LIMPAR O FOMULARIO */
    reset()
    {
	}
	
	ngAfterViewInit()
	{
		super.ngAfterViewInit();

		/* QUANDO EDITAR NÃO ALTERA */
		if(!this.hasValue())
		{
			this.core().api.http.get("http://api.ipify.org/?format=json").subscribe((result) =>
			{
				if(result && result.ip)
				{
					this.setValue(result.ip);
				}
			});
		}		
	}
}
