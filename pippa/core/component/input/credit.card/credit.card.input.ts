import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../base.fieldset.input';
import { Field }              from '../../../model/field/field';
import { FieldType }          from '../../../type/field/field.type';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';
import { Types } 			  from '../../../type/types';

@Component({
    selector 		: `.credit-card-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: '<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>',
})
export class CreditCardInput extends BaseFieldsetInput
{
	public hasAddress   : boolean = true;
	public installments : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input credit-card-input col ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
		
		if(this.setting.hasAddress != undefined)
		{
			this.hasAddress = this.setting.hasAddress;
		}

		if(this.setting.installments)
		{
			this.installments = this.setting.installments;
		}

		if(this.setting.maxInstallments)
		{
			this.installments = [];

			for(let i = 1; i <= this.setting.maxInstallments; i++)
			{
				this.installments.push({
					id       : i + 'x',
					label    : i + 'x',
					value    : i ,
					selected : i == 1,
					min      : (this.setting[i] ? this.setting[i] : 0)
				});
			}
		}

		await this.createComponents();
    }

    async createComponents()
    {
		let items : any;

		if(this.form.isSetModeForm())
		{
			items = new FormItemCollection([	
				{
					row : 2,
					col : 1,
					field : new Field({
						type        : FieldType.type('CardNumber'),
						name        : 'cardnumber',
						label       : 'Número do Cartão',
						placeholder : 'Número do Cartão',
						editable    : false,
					})
				},
			]);
		}
		else
		{
			items = new FormItemCollection([	
				{
					row : 1,
					col : 1,
					field : new Field({
						type        : FieldType.type('CardNumber'),
						name        : 'cardnumber',
						label       : 'Número do Cartão',
						placeholder : 'Número do Cartão',
					})
				},
				{
					row : 2,
					col : 1,
					field : new Field({
						type        : FieldType.type('MonthYear'),
						name        : 'expirydate',
						label       : 'Data de Vencimento',
						placeholder : 'Data de Vencimento',
					}),
					setting : {
						mask : '00/00'
					}
				},
				{
					row : 2,
					col : 2,
					field : new Field({
						type        : FieldType.type('CVV'),
						name        : 'cvv',
						label       : 'CVV',
						placeholder : 'CVV',
					})
				}
			]);
	
			if(this.installments)
			{
				items.add({
					row : 2,
					col : 3,
					field : new Field({
						type        : FieldType.type('Select'),
						name        : 'installments',
						label       : 'Parcelamento',
						placeholder : 'Parcelamento',
						option      : {
							items : this.installments
						}					
					})
				});
			}
	
			// DADOS DO TÍTULAR
			items.add({
				row : 3,
				col : 1,
				field : new Field({
					type        : FieldType.type('Label'),
					name        : 'ownerLabel',
					label       : 'Dados do Títular do Cartão',
					placeholder : 'Dados do Títular do Cartão',
					record      : false,
				})
			});

			items.add({
				row : 4,
				col : 1,
				field : new Field({
					type        : FieldType.type('Text'),
					name        : 'owner',
					label       : 'Nome Títular do Cartão',
					placeholder : 'Nome Títular do Cartão',
				})
			});

			items.add({
				row : 5,
				col : 1,
				field : new Field({
					type        : FieldType.type('Select'),
					name        : 'docType',
					label       : 'Tipo de Documento do Títular do Cartão',
					placeholder : 'Tipo de Documento do Títular do Cartão',
					option : {
						items 		: [
							Types.CPF_DOCUMENT_TYPE,
							Types.CNPJ_DOCUMENT_TYPE,
							Types.PASSPORT_DOCUMENT_TYPE,
						]	
					},
					initial : Types.CPF_DOCUMENT_TYPE
				}),
				setting : {
					controlField : '_cpf,_cnpj,_passport',
					controlPath  : 'value',
					cpf         : '_cpf',
					cnpj        : '_cnpj',
					passport    : '_passport',
				}
			});

			items.add({
				row : 6,
				col : 1,
				field : new Field({
					type        : FieldType.type('CPF'),
					name        : '_cpf',
					label       : 'CPF do Títular do Cartão',
					placeholder : 'CPF do Títular do Cartão',
				})
			});

			items.add({
				row : 7,
				col : 1,
				field : new Field({
					type        : FieldType.type('CNPJ'),
					name        : '_cnpj',
					label       : 'CNPJ do Títular do Cartão',
					placeholder : 'CNPJ do Títular do Cartão',
				})
			});

			items.add({
				row : 8,
				col : 1,
				field : new Field({
					type        : FieldType.type('Text'),
					name        : '_passport',
					label       : 'Passaporte do Títular do Cartão',
					placeholder : 'Passaporte do Títular do Cartão',
				})
			});
			//--------	

			if(this.hasAddress)
			{
				items.add({
					row : 9,
					col : 1,
					field : new Field({
						type        : FieldType.type('Address'),
						name        : 'address',
						label       : 'Endereço de Cobrança do Cartão',
						placeholder : 'Endereço de Cobrança do Cartão',
					})
				});
			}
	
			items.add({
				row : 10,
				col : 1,
				field : new Field({
					type        : FieldType.type('HIDDEN'),
					name        : 'cardname',
					label       : 'Bandeira do Cartão',
					placeholder : 'Bandeira do Cartão',
					required    : false
				})
			});			
		}        
 
        items.doSort();
		
		await this.formItem.items.updateItems(items);
	}

	installmentsMinValue(value:string)
	{
		if(this.installments && this.form.instances.installments)
		{
			const installments = this.installments.filter((item) => 
			{
				return value >= item.min;
			});

			this.form.instances.installments.items   = installments;
			this.form.instances.installments.initial = installments[0];
			this.form.instances.installments.populate(installments[0]);					
		}
	}
}