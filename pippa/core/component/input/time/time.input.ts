import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { TDate } from '../../../util/tdate/tdate';

@Component({
    selector 		: `.time-input`,
	changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
										'ng-valid'    	: formControl?.valid && this.hasValue(), 
										'ng-invalid'    : !formControl?.valid,
										'hidden'        : hidden }">	
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup">
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"
									[ngModel]="value"
									[ngModelOptions]="{standalone: true}"
									[mask]="mask"
									[placeholder]="field.placeholder"
									[disabled]="field.disable"
									(blur)="onBlur($event)"/>
							</div>

							<error-input [control]="formControl" [submitted]="submitted">
							</error-input>

						</div>`
})
export class TimeInput extends BaseTextInput
{
    isDateValid : boolean = false;
	inputMask   : string  = 'HH:mm';
	mask        : string  = '00:00';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col time-input ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
    }

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
		}
	}

    onBlur(event:any)
    {
        let time = new TDate({value:event.srcElement.value, mask:this.inputMask});

        this.isDateValid = time.isValid();

        if(this.isDateValid)
        {
            this.setValue(time.toDate());
        }
        else
        {
            this.setValue('');
        }
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            let time = new TDate({ value : value});

            this.isDateValid = time.isValid();

            if(this.isDateValid)
            {
                this.value = time.format(this.inputMask);
            }

            this.dirty = true;
        }
    }
}
