import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

/* FIELD */
import { BaseFieldsetInput }  from '../base.fieldset.input';
import { FieldType }          from '../../../type/field/field.type';
import { Field }              from '../../../model/field/field';
import { FormItemCollection } from '../../../model/form.item/form.item.collection';

@Component({
    selector 		: `.dna-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<div fieldset-input #template (rendererComplete)="onRendererComplete()"></div>`,
})
export class DNAInput extends BaseFieldsetInput
{
    public listDiscField : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col dna-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';

		await this.createComponents();
    }

    async createComponents()
    {
        const items = new FormItemCollection();

        if(this.formItem.field.option.rows)
        {
            let i = 0;

            for(const key in this.formItem.field.option.rows)
            {
                const row = this.formItem.field.option.rows[key];
                const radioField = new Field({
                    type        : FieldType.type('RADIO'),
                    name        : key,
                    label       : row['label'],
                    placeholder : row['label'],
                    option      : {
                        items : row.options
                    },
                    row         : key,
                });

                items.set({
                    row   : i,
                    field : radioField
                });

                i++;
            }
        }

        await this.formItem.items.updateItems(items);
    }
}
