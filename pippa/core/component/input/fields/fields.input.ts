import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";
import { PopoverController, AlertController } from "@ionic/angular";
import { FormControl, } from "@angular/forms";

/* PIPPA */
import { BaseInput }          from "../base.input";
import { Field }              from "../../../../core/model/field/field";
import { FormItemCollection } from "../../../../core/model/form.item/form.item.collection";
import { FieldFormPopover }   from "../../../../core/popover/field.form/field.form.popover";

//[dragula]="dragulaRow"
//[dragulaModel]="item.items"

@Component({
    selector        : ".fields-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div header-input
                             [form]="form"
                             [formItem]="formItem"
                             (edit)="onEdit($event)"
                             (del)="onDel()">
                        </div>
                        <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                            <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>
                            <ion-grid>
                                <ion-row class="item" *ngFor="let item of this.items; trackBy: trackById" [ngClass]="{'checked' : item.enabled}">
                                    <ion-col>
                                        <input id="{{item.id}}" type="checkbox" (click)="onSelectItem(item)" [checked]="item.enabled">
                                        <label for="{{item.id}}">
                                            <ion-icon name="git-branch-outline" *ngIf="item.field.reference"></ion-icon>  {{item.field.label}}&nbsp;&nbsp;<small *ngIf="item.field.required">*</small>
                                        </label>
                                    </ion-col>
                                    <ion-col size="2">
                                        <small>{{item.field.name}}</small>
                                    </ion-col>
                                    <ion-col size="1">
                                        <small>{{item.field.type?.label}}</small>
                                    </ion-col>
                                    <ion-col size="1" class="buttons">
                                        <a (click)="openSubItem(item)" *ngIf="item.field.isContainer()">
                                            <ion-icon name="add-outline"></ion-icon>
                                        </a>
                                        <a (click)="onEditItem(item)">
                                            <ion-icon name="create-outline"></ion-icon>
                                        </a>
                                        <a (click)="onDelItem(item)">
                                            <ion-icon name="close-outline"></ion-icon>
                                        </a>
                                    </ion-col>
                                    <ion-col size-xl="12" *ngIf="item.field.isContainer()">
                                        <ion-grid class="sub-fields">
                                            <ion-row class="ion-align-items-center drag-row item"
                                                     *ngFor="let subItem of item.items; trackBy: trackById; let i = index;"
                                                     [attr.data-parent]="item.id">
                                                <ion-col class="col-move">
                                                    <ion-icon name="reorder-four-outline"></ion-icon>
                                                    <ion-icon name="git-branch-outline" *ngIf="subItem.field.reference"></ion-icon>
                                                    <label>{{subItem.field.label}}&nbsp;&nbsp;<small *ngIf="subItem.field.required">*</small></label>
                                                </ion-col>
                                                <ion-col size="2" class="col-move">
                                                    <small>{{subItem.field.name}}</small>
												</ion-col>
												<ion-col size="1" class="col-move">
                                                    <small>{{subItem.field.row}}</small>
												</ion-col>
												<ion-col size="1" class="col-move">
                                                    <small>{{subItem.field.col}}</small>
                                                </ion-col>
                                                <ion-col size="1" class="col-move">
                                                    <small>{{subItem.field?.type?.label}}</small>
                                                </ion-col>
                                                <ion-col size="1" class="buttons">
													<a (click)="openSubItem(subItem)" *ngIf="subItem.field.isContainer()">
														<ion-icon name="add-outline"></ion-icon>
													</a>
                                                    <a (click)="onEditSubItem(item, subItem)">
                                                        <ion-icon name="create-outline"></ion-icon>
                                                    </a>
                                                    <a (click)="onDelSubItem(item, subItem)">
                                                        <ion-icon name="close-outline"></ion-icon>
                                                    </a>
                                                </ion-col>
												<ion-col size-xl="12" *ngIf="subItem.field.isContainer()">
													<ion-grid class="sub-fields">
														<ion-row class="ion-align-items-center drag-row item"
																*ngFor="let subItem3 of subItem.items; trackBy: trackById; let i = index;"
																[attr.data-parent]="subItem.id">
															<ion-col class="col-move">
																<ion-icon name="reorder-four-outline"></ion-icon>
																<ion-icon name="git-branch-outline" *ngIf="subItem3.field.reference"></ion-icon>
																<label>{{subItem3.field.label}}&nbsp;&nbsp;<small *ngIf="subItem3.field.required">*</small></label>
															</ion-col>
															<ion-col size="2" class="col-move">
																<small>{{subItem3.field.name}}</small>
															</ion-col>
															<ion-col size="1" class="col-move">
																<small>{{subItem3.field.row}}</small>
															</ion-col>
															<ion-col size="1" class="col-move">
																<small>{{subItem3.field.col}}</small>
															</ion-col>
															<ion-col size="1" class="col-move">
																<small>{{subItem3.field?.type?.label}}</small>
															</ion-col>
															<ion-col size="1" class="buttons">
																<a (click)="onEditSubItem(subItem, subItem3)">
																	<ion-icon name="create-outline"></ion-icon>
																</a>
																<a (click)="onDelSubItem(subItem, subItem3)">
																	<ion-icon name="close-outline"></ion-icon>
																</a>
															</ion-col>
														</ion-row>
													</ion-grid>
												</ion-col>
                                            </ion-row>
                                        </ion-grid>
                                    </ion-col>
                                </ion-row>
                            </ion-grid>
                            <div class="error" *ngIf="submitted">
                                <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                            </div>
                        </div>
                        <ion-grid class="footer-input">
							<ion-row>
								<!----
                                <ion-col>
                                    <input id="{{field.id}}_all" type="checkbox" (click)="onSelectAll()" [checked]="hasAll()">
                                    <label for="{{field.id}}_all">
                                        Selecionar Todos
                                    </label>
								</ion-col>
								----->
                                <ion-col>
                                    <a (click)="openItem()">
                                        <ion-icon name="add-circle-outline"></ion-icon>
                                    </a>
                                </ion-col>
                            </ion-row>
                        </ion-grid>`
})
export class FieldsInput extends BaseInput
{
    public group             : string;
    public groupName         : string;
    public popoverController : PopoverController;
    public alertController   : AlertController;
    public dragulaRow        : any;
    public items             : FormItemCollection;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + "-input input col fields-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";

        this.group     = this.field.group;
        this.groupName = this.field.groupName;
        this.items     = new FormItemCollection();
    }

    update()
    {
        const data = this.items.parseData();
        this.setValue(data);
    }

    onSelectItem(item:any)
    {
        item.enabled = !item.enabled;
        this.update();
    }

    onSelectAll()
    {
        for (const key in this.items)
        {
            const item : any = this.items[key];
            item.enabled     = !item.enabled;
        }

        this.update();
    }

    hasAll()
    {
        let count = 0;

        for (const key in this.items)
        {
            const item : any = this.items[key];

            if (item.enabled)
            {
                count++;
            }
        }

        return count > 0 && count == this.items.length;
    }

    async onModelChange(value:any)
    {
        /* TRATAR O VALOR DO BIND */
        if (value && !this.dirty)
        {
            this.items._updateItems(value);
            this.dirty = true;
			
            /* CADASTROS ANTIGOS */
            await this.items.on(true);

            this.markForCheck();

            this.emitChange(this.items);
        }
    }

    /* ITEM */
    openItem(item?:any)
    {
        this.popoverController.create(
            {
                component : FieldFormPopover,
                componentProps :
            {
                data     : (item ? item.field : null),
                field    : this.field,
                formItem : item,
                acl      : this.form.instance.acl,
                onAdd    : ((event:any) =>
                {
                    /* REMOVE LOGS */
                    delete event.data._logs;

                    const data = event.data;
                    data.id    = data.name;
                    data.accid = this.core().account.code;
                    data.appid = this.core().currentApp.code;
					
                    this.items.set({
                        id    : data.id,
                        field : new Field(data),
                    });

                    this.update();

                    this.popoverController.dismiss();
                }),
                onSet : (async (event:any) =>
                {
                    /* REMOVE LOGS */
                    delete event.data._logs;

                    const data = event.data;
                    data.accid = this.core().account.code;
                    data.appid = this.core().currentApp.code;	
					
                    item.normalize();

                    /* ALTERA FIELD */
                    item.populate({
                        field : data
                    });

                    await item.on();
					
                    item.version++;
                	//this.items.set(item);                    

                    this.update();

                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
                showBackdrop : false,
                cssClass     : "right-popover",
            })
        .then(popover =>
        {
            popover.present();
        });
    }

    onEditItem(item:any)
    {
        this.openItem(item);
    }

    onDelItem(item:any)
    {
        this.alertController.create({
            header  : "Alerta",
            message : "Deseja remover esse item?",
            buttons : [
                {
                    text    : "Fechar",
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        this.items.del(item);
                        this.update();

                        return true;
                    }
                }
            ]
        }).then(alert =>
        {
            alert.present();
        });
    }

    /* SUB ITEM */
    openSubItem(item:any, subItem?:any)
    {
        this.popoverController.create(
            {
                component : FieldFormPopover,
                componentProps :
            {
                data  : (subItem ? subItem.field : null),
                field : this.field,
                acl   : this.form.instance.acl,
                onAdd : ((event:any) =>
                {
                    /* REMOVE LOGS */
                    delete event.data._logs;

                    const data = event.data;
                    data.id    = data.name;
                    data.accid = this.core().account.code;
                    data.appid = this.core().currentApp.code;
					
                    item.items.set({
                        id    : data.id,
                        field : data,
                    });
					
                    item.items.doSort();

                    this.update();

                    this.popoverController.dismiss();                     
                }),
                onSet : ((event:any) =>
                {
                    /* REMOVE LOGS */
                    delete event.data._logs;

                    subItem.populate({
                        field : event.data
                    });

                    subItem.normalize();

                    item.items.set(subItem);
					
                    item.items.doSort();

                    item.version++;

                    this.update();

                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
                showBackdrop : false,
                cssClass     : "right-popover",
            })
        .then(popover =>
        {
            popover.present();
        })
    }

    onEditSubItem(item:any, subItem?:any)
    {
        this.openSubItem(item, subItem);
    }

    onDelSubItem(item:any, subItem:any)
    {
        this.alertController.create({
            header  : "Alerta",
            message : "Deseja remover esse item?",
            buttons : [
                {
                    text    : "Fechar",
                    handler : () =>
                    {
                        return true;
                    }
                },
                {
                    text    : "Remover",
                    handler : () =>
                    {
                        item.items.del(subItem);
                        this.update();

                        return true;
                    }
                }
            ]
        })
        .then(alert =>
        {
            alert.present();
        });
    }
}
