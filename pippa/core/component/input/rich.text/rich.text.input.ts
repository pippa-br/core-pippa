import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { Validators } from "@angular/forms";
//import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import * as Editor from "../../../../util/ckeditor5/build/ckeditor";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { UploadPlugin }  from "../../../../core/plugin/upload/upload.plugin";
import { Types }  		 from "../../../../core/type/types";

@Component({
    selector        : ".rich-text-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (set)="onSet($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <ckeditor [config]="config"
                                          [editor]="editorTemplate"
                                          (ready)="onReady($event)"
                                          (change)="onChange($event)">
                                </ckeditor>

                                <error-input [control]="formControl" [submitted]="submitted">
                                </error-input>
                            </div>
                        </div>` 
})
export class RichTextInput extends BaseTextInput
{
    @ViewChild("fileInput", { static : false }) fileInput: ElementRef;
    
    public editorTemplate = Editor;
    public editor : any;
    public config : any = {
        allowedContent : true,
        height         : 500,
        toolbar        : {
            items : [
                "Heading",
                "Undo",
                "Redo",
                "|",				
        	  	"fontFamily", "fontSize", "fontColor", "fontBackgroundColor", "Highlight", "|",
        		"bold", "italic", "strikethrough", "underline", "subscript", "superscript", "|",
        		"link", 
                "|",
                "alignment",								
                "Outdent",	
                "Indent",	
                "BulletedList",
                "NumberedList",
                "insertTable",
                "|",
                "SpecialCharacters",
                "PageBreak",
                "HorizontalLine",			
                "BlockQuote",
                "|",
                "UploadImage",
                "Image",			
                "MediaEmbed",
                "|",
                "MathType",
                "HtmlEmbed",
                "Code",
                "CodeBlock",				
            ],
            shouldNotGroupWhenFull : true,
        },
        fontSize : {
            options : [
                8,
                9,
                11,
                13,
                "default",
                17,
                19,
                21
            ]
        },
        table : {
            contentToolbar : [
                "tableColumn", "tableRow", "mergeTableCells",
                "tableProperties", "tableCellProperties"
            ],
        },
        image : {
            styles : [
                "alignLeft", "alignCenter", "alignRight"
            ],
            resizeOptions : [
                {
                    name  : "resizeImage:original",
                    label : "Original",
                    value : null
                },
                {
                    name  : "resizeImage:25",
                    label : "25%",
                    value : "25"
                },
                {
                    name  : "resizeImage:50",
                    label : "50%",
                    value : "50"
                },
                {
                    name  : "resizeImage:75",
                    label : "75%",
                    value : "75"
                },
                {
                    name  : "resizeImage:100",
                    label : "100%",
                    value : "100"
                }
            ],
            toolbar : [
                "imageStyle:alignLeft", "imageStyle:alignCenter", "imageStyle:alignRight",
                "|",
                "resizeImage",
                "|",
                "imageTextAlternative",
                "|",
                "linkImage",
            ]
        },
        link : {
            addTargetToExternalLinks : true
        },
        simpleUpload : {
            uploadUrl     		: (this.core().production ? this.core().apiUrl : "/api") + "/" + Types.UPLOAD_DOCUMENT_API + "?accid=" + this.core().masterAccount.code + "&onlyUrl=true",
            withCredentials : true,
        },
        htmlEmbed : {
            showPreviews : true,
        },
        mediaEmbed : {
            previewsInData : true
        },
        codeBlock : {
            languages : [
                { language : "plaintext", label : "Plain text", class : "" },
                { language : "css", label : "CSS" },
                { language : "html", label : "HTML" },
                { language : "javascript", label : "JavaScript", class : "js javascript js-code" },
            ]
        }		
    }

    constructor(
        public uploadPlugin      : UploadPlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col rich-text-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    onReady(editor:any) 
    {
        this.editor = editor;

        if (editor && this.value)
        {
            // BUG NO BIND DO DATA
            this.editor.setData(this.value);			
        }
    }

    createValidators()
    {
        this.validators = [];

        if (this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
    }

    onChange(event:any)
    {
        if (event && event.editor)
        {
            this.dirty = true; // PARA NAO VOLTAR O CURSO PARA O INICIO NA PRIMEIRA VEZ
            this.setValue(event.editor.getData());
        }
    }

    onModelChange(value:any)
    {
        if (!this.dirty && this.editor && value)
        {
            this.editor.setData(value);			
            this.dirty = true;
        }
    }

    onAdd()
    {
        this.fileInput.nativeElement.click();
    }
}
