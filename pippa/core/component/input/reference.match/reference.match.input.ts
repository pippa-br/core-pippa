import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';
import { BaseModel } from '../../../model/base.model';
import { BaseList }  from '../../../model/base.list';

@Component({
    selector : `.reference-match-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div header-input [formItem]="formItem" (set)="onSet($event)" (del)="onDel()"></div>
                <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onInit($event)"/>
                    <ion-grid>
                        <ion-col col-6>

                            <ng-select [(ngModel)]="selected"
                                       [ngModelOptions]="{standalone: true}"
									   [items]="items1"
									   appendTo="body"
                                       [placeholder]="field.label"
                                       cancelText="Fechar"
                                       okText="Selecionar"
                                       (change)="onChange($event)">
                            </ng-select>
                            <div class="error" *ngIf="submitted">
                                <span *ngIf="field.formGroup.get(field.name).hasError('required')">Campo Obrigatório</span>
                            </div>
                            <ion-grid>
                                <ion-row *ngFor="let item of selecteds; let last = last;" class="ion-align-items-center">
                                    <ion-col size="11">
                                        {{item.label}}
                                    </ion-col>
                                    <ion-col size="1" class="del-button" text-right>
                                        <button ion-button icon-only clear color="danger" (click)="onRemove(item)">
                                            <ion-icon name="remove-circle-outline"></ion-icon>
                                        </button>
                                    </ion-col>
                                </ion-row>
                            </ion-grid>

                        </ion-col>
                        <ion-col col-6>
                        </ion-col>
                    </ion-grid>
                </div>`
})
export class ReferenceMatchInput extends BaseInput implements OnInit
{
    items1     : BaseList<any> = new BaseList<any>();
    selecteds  : BaseList<any> = new BaseList<any>();
    isInit     : boolean = false;
    labelField : string = 'name';
    selected   : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass  = this.formItem.name + '-match input col reference-match list ';
        this.nameClass += 'row-' + this.field.row + ' col-' + this.field.col;
        //this.width      = this.domSanitizer.bypassSecurityTrustStyle(field.width + '%');

        if(this.field.setting.labelField)
        {
            this.labelField = this.field.setting.labelField;
        }

        if(this.setting.path1)
        {
            let params = this.createParams({
                path            : this.setting.path1.replace('{%accid%}', this.core().account.code),
                model           : BaseModel,
                modelCollection : BaseList,
            });

            this.api().getList(params).then((result:any) =>
            {
                /* TIME PARA ONSNAPSHOT*/
                setTimeout(() =>
                {
                    let items = new BaseList<any>();

                    for(let key in result.collection)
                    {
                        let data = result.collection[key];

                        items.push({
                            id        : data.id,
                            label     : data.name,
                            reference : data.reference,
                        });
                    }

                    this.items1 = items;

                    this.removeSelectedItems();

                }, 500);
            });
        }
    }

	createValidators()
	{
		super.createValidators();
		
        this.validators = [];

        if(this.formItem.required)
        {
            this.validators.push(Validators.required);
        }
	}

    onChange(option:any)
    {
        if(option != null)
        {
            this.selected = null;
            this.selecteds.push(option);
            this.items1.del(option);

            this.removeSelectedItems();
            this.update();
        }
    }

    onRemove(item:any)
    {
        this.selecteds.del(item);
        this.items1.push(item);

        this.removeSelectedItems();
        this.update();
    }

    update()
    {
        let items = [];

        for(let key in this.selecteds)
        {
            let item = this.selecteds[key];
            items.push(item.reference);
        }

        this.updateValue(items);
    }

    /* FEITO PARA REMOVER ITEMS SELECIONADOS */
    removeSelectedItems()
    {
        let items = new BaseList<any>();

        for(let key in this.items1)
        {
            let item    = this.items1[key];
            let hasItem = false;

            for(let key2 in this.selecteds)
            {
                let item2 = this.selecteds[key2];

                if(item.id == item2.id)
                {
                    hasItem = true;
                    break;
                }
            }

            if(!hasItem)
            {
                items.push(item);
            }
        }

        this.items1 = items;
    }

    onInit(data:any)
    {
        /* TRATAR O VALOR DO BIND */
        if(!this.isInit)
        {
            this.isInit = true;

            if(data && data.length)
            {
                let i     = 0;
                let size  = data.length;
                let items = new BaseList<any>()

                for(let key in data)
                {
                    data[key].onSnapshot((item:any) =>
                    {
                        let model : any = new BaseModel(item.data());

                        items.push({
                            id        : model.id,
                            label     : model.name,
                            reference : data[key],
                        });

                        i++;

                        if(i == size)
                        {
                            this.selecteds = items;
                            this.removeSelectedItems();
                        }
                    });
                }
            }
        }
    }

    ngOnInit()
    {
    }
}
