import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from "@angular/core";

/* FIELD */
import { BaseFieldsetInput }  from "../base.fieldset.input";
import { FieldType }          from "../../../type/field/field.type";
import { Field }              from "../../../model/field/field";
import { FormItemCollection } from "../../../model/form.item/form.item.collection";
import { GooglePlugin } 	  from "../../../plugin/google/google.plugin";

@Component({
    selector      		: ".address-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: "<div fieldset-input #template (rendererComplete)=\"onRendererComplete()\"></div>",
})
export class AddressInput extends BaseFieldsetInput implements AfterViewInit
{
    public housenumberTime : any;
    public simple           = false;
    public isBr             = true;
    public maxComplement     = 30;

    constructor(
		public googlePlugin      : GooglePlugin,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input address-input col ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
		
        if (this.setting.simple)
        {
            this.simple = this.setting.simple;
        }

        if (this.setting.maxComplement)
        {
            this.maxComplement = this.setting.maxComplement;
        }
		
        await this.createComponents();
    }

    async createComponents()
    { 
        const items = new FormItemCollection([
            {
                row   : 0,
                col   : 0,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Select")),
                    name        : "country",
                    label       : "País",
                    placeholder : "País",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                    option      : {
                        items : this.core().util.getCountries()
                    },
                    setting : 
					{
					    searchable : true,
					},
                    onInput : (event:any) =>
                    {
                        if (event.data)
                        {
                            const control = this.field.formGroup.controls["zipcode"];
							
                            if (event.data.value == "br")
                            {
                                control.instance.formItem._label   = "CEP";
                                control.instance.field.placeholder = "CEP"
                                control.instance.hasMask           = true;								
                                this.isBr                          = true;

                                control.instance.createValidators();
                                control.instance.updateValidators(true);
                            }
                            else
                            {
                                control.instance.formItem._label   = "Zipcode";	
                                control.instance.field.placeholder = "Zipcode"
                                control.instance.hasMask           = false;		
                                this.isBr                          = false;	

                                control.instance.createValidators();
                                control.instance.updateValidators(false);				
                            }

                            control.instance.field.dispatchUpdate();
                            control.instance.markForCheck();							
                        }
                    }
                })
            },
            {
                row   : 0,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("ZIPCODE"),
                    name        : "zipcode",
                    label       : "CEP",
                    placeholder : "CEP",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                    onInput     : (event:any) =>
                    {
                        if (this.isBr && event.data && event.data.length >= 9)
                        {
                            const value = event.data.replace("-", "").substring(0, 8);

                            /* GET INFORMATION */
                            const params = this.createParams({
                                post : {
                                    zipcode : value
                                }
                            });

                            this.core().api.getInformationAddress(params).then((result:any) =>
                            {
                                this.field.formGroup.controls["zipcode"].setErrors(null);

                                if (result.data && !result.data.erro)
                                {
                                    console.log("Information Address", result.data);

                                    /* TRANSFORM UPPERCASE */
                                    if (this.formItem.field.uppercase)
                                    {
                                        for (const key in result.data)
                                        {
                                            result.data[key] = result.data[key].toUpperCase();
                                        }
                                    }

                                    this.field.formGroup.patchValue({
                                        street   : result.data.logradouro,
                                        district : result.data.bairro,
                                        city     : result.data.localidade,
                                        ibge     : result.data.ibge,
                                        state    : result.data.uf,
                                        country  : {
                                            value : "br"
                                        }
                                    });																
                                }
                                else
                                {
                                    // this.field.formGroup.controls['zipcode'].setErrors({zipcode:true});

                                    // console.error('invalid CEP', result.data);
                                }
                            });
                        }
                    }
                })
            },
            {
                row   : 1,
                col   : 0,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "street",
                    label       : "Endereço",
                    placeholder : "Endereço",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                })
            },
            {
                row   : 1,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("Text"),
                    name        : "housenumber",
                    label       : "Número",
                    placeholder : "Número",
                    owner       : this,
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                    onInput     : (event:any) =>
                    {
                        if (event.data && event.data.length >= 1)
                        {
                            if (this.housenumberTime)
                            {
                                clearTimeout(this.housenumberTime);
                            }
							
                            this.housenumberTime = setTimeout(() => 
                            {
                                const address = this.field.formGroup.value;

                                if (address.street)
                                {
                                    /* GET INFORMATION */
                                    const _address = address.street + ", " + event.data + ", " + address.city + " - " + address.state + ", " + address.zipcode + ", " + address.country
	
                                    this.googlePlugin.getLocaleAddress(_address).then((data:any) =>
                                    {
                                        console.log("get housenumber", data);
	
                                        if (data.results.length)
                                        {
                                            const location     = data.results[0].geometry.location;
                                            const locationtype = data.results[0].geometry.location_type;
	
                                            this.field.formGroup.patchValue({
                                                lat          : location.lat,
                                                lng          : location.lng,
                                                locationtype : locationtype,
                                            });
											
                                            if (this.field.onComplete)
                                            {
                                                this.field.onComplete(this.field.formGroup.value);
                                            }
                                        }
                                    });
                                }
                            }, 250);
                        }
                    }
                })
            },
            {
                row   : 2,
                col   : 0,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "complement",
                    label       : "Complemento",
                    placeholder : "Complemento",
                    required    : false,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                    setting     : {
                        maxlength : this.maxComplement
                    }
                })
            },
            {
                row   : 2,
                col   : 1,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "district",
                    label       : "Bairro",
                    placeholder : "Bairro",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                })
            },
            {
                row   : 3,
                col   : 0,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "city",
                    label       : "Cidade",
                    placeholder : "Cidade",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                })
            },
            {
                row   : 3,
                col   : 1,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "state",
                    label       : "Estado",
                    placeholder : "Estado",
                    required    : this.formItem.field.required,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                })
            },
            {
                row   : 4,
                col   : 1,
                field : new Field({
                    type        : (this.simple ? FieldType.type("Hidden") : FieldType.type("Text")),
                    name        : "note",
                    label       : "Ponto de Referência",
                    placeholder : "Ponto de Referência",
                    required    : false,
                    editable    : this.formItem.field.editable,
                    uppercase   : this.formItem.field.uppercase,
                })
            },
            {
                row   : 5,
                col   : 0,
                field : new Field({
                    type     : FieldType.type("HIDDEN"),
                    name     : "ibge",
                    required : false,
                })
            },
            {
                row   : 5,
                col   : 1,
                field : new Field({
                    type     : FieldType.type("HIDDEN"),
                    name     : "lat",
                    required : false,
                })
            },
            {
                row   : 5,
                col   : 2,
                field : new Field({
                    type     : FieldType.type("HIDDEN"),
                    name     : "lng",
                    required : false,
                })
            },
            {
                row   : 5,
                col   : 3,
                field : new Field({
                    type     : FieldType.type("HIDDEN"),
                    name     : "locationtype",
                    required : false,
                })
            }
        ]);

        items.doSort();
		
        await this.formItem.items.updateItems(items);
    }	
}
