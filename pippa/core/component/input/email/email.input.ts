import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { InputValidate } from '../../../validate/input.validate';

@Component({
    selector        : `.email-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
							'ng-valid' 	 : formControl?.valid && this.hasValue(), 
							'ng-invalid' : !formControl?.valid,
							'hidden'     : hidden }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'icon' : field.getIcon()}">

                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="text"
                                    autocomplete="false"
                                    [ngModel]="value"
                                    [ngModelOptions]="{standalone: true}"
                                    [readonly]="form?.isSetModeForm() && !field.editable"
                                    (keyup)="onInput($event)"
                                    [placeholder]="field.placeholder"/>

                                <icon-input [isLoading]="isLoading"
                                            [icon]="field.getIcon()">
                                </icon-input>
                            </div>
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('email')">{{'Email inválido' | translate}}</span>
                            </error-input>
                        </div>`
})
export class EmailInput extends BaseTextInput
{
    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col email-field ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
	}

	createValidators()
	{
        this.validators = [];

        if(!this.hidden && this.formItem.required)
        {
            this.validators.push(Validators.required);            
        }

		this.validators.push(InputValidate.email);
		this.asyncValidators = [InputValidate.search(this.field, this.form, this)];
	}

	onInput(event:any)
    {
		if(event.srcElement)
		{			
			event.srcElement.value = event.srcElement.value.trim();

			if(!this.uppercase)
			{
				event.srcElement.value = event.srcElement.value.toLowerCase();
			}			
		}

		super.onInput(event);
    }
}
