import { ViewChild, AfterViewInit, Directive } from "@angular/core";

/* PIPPA */
import { BaseInput } from "./base.input";

@Directive()
export abstract class BaseFieldsetInput extends BaseInput implements AfterViewInit
{
    @ViewChild("template", { static : true }) template : any;

    async createControls(formItem:any, formGroup:any)
    {
        // PRIMEIRO CRIA DO TEMPLATE
        this.template.createControls(formItem, formGroup);

        /* NAO TEM SUPER */
        this.formGroup = formGroup;
        this.formItem  = formItem;
        this.field     = formItem.field;
        this.setting   = formItem.setting || {};

        // PARA VALIDADES
        this.formControl          = this.formGroup.get(formItem.name);
        this.formControl.instance = this; /* ADICIONA A INSTANCE AO CONTROLLER */

        this.initSettings();
        this.initSibling();

        if (this.form)
        {
            if (!this.form.instances)
            {
                this.form.instances = {};
            }

            this.form.instances[this.formItem.name] = this;

            /* VALIDATE */
            this.subscriptions.add(this.form.saveEvent.subscribe((event:any) =>
            {
                this.template.onValidate(event);
            }));
        }
    }

    destroy()
    {
        this.template.destroy();
    }

    validateSave()
    {
        return this.template.validateSave();
    }

    validate()
    {
        return this.template.validate();
    }
	
    updateValue(value:any)
    {
        if (value)
        {
            this.template.updateValue(value);
        }			
    }

    set submitted(value:boolean)
    {
        this._submitted         = value;
        this.template.submitted = value;
    }

    get submitted()
    {
        return this._submitted;
    }
	
    get display()
    {
        return this.template.display;
    }
	
    set display(value:boolean)
    {
        this.template.display = value;
    }

    get hidden()
    {
        return this.template.hidden;
    }
	
    set hidden(value:boolean)
    {
        this.template.hidden = value;
    }

    /* OVERRIDER */
    rendererComplete()
    {
    }

    populate(value: any) 
    {
        this.template.populate(value);
    }

    set data(value:any)
    {
        this.template.data = value;
    }

    get data()
    {
        return this.template.data;
    }

    set updateData(value:any)
    {
        this.template.updateData = value;
    }

    get updateData()
    {
        return this.template.updateData;
    }

    set form(value:any)
    {
        this.template.form = value;
    }

    get form()
    {
        return this.template.form;
    }

    set steps(value:any)
    {
        this.template.steps = value;
    }

    get steps()
    {
        return this.template.steps;
    }

    clear()
    {
        if (this.template)
        {
            this.template.clear();
        }
    }
}
