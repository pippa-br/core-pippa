import { Component, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from "@angular/core";
import { Validators } from "@angular/forms";

/* PIPPA */
import { BaseTextInput } from "../base.text.input";
import { InputValidate } from "../../../validate/input.validate";
import { TDate } from "../../../util/tdate/tdate";

@Component({
    selector        : ".date-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
							[ngClass]="{ 'ng-submitted' : submitted, 
                                    'ng-valid' 	: formControl?.valid && this.hasValue(), 
                                    'ng-invalid'   : !formControl?.valid,
                                    'hidden'       : hidden }">
                            <div header-input
                                    [form]="form"
                                    [formItem]="formItem"
                                    (edit)="onEdit($event)"
                                    (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'hasTime':hasTime}">
                            
                                <input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

                                <input type="tel"
                                        class="date"
                                        autocomplete="false"
                                        [(ngModel)]="dateString"
                                        [ngModelOptions]="{standalone: true}"
                                        [mask]="maskDate"
                                        [placeholder]="dateMask"
										[readonly]="(!editable && isSetModeForm()) || readonly"
                                        (keyup)="onInput()"/>
                                   
                                <input *ngIf="hasTime"
                                        type="tel"
                                        class="time"
                                        autocomplete="false"
                                        [(ngModel)]="timeString"
                                        [ngModelOptions]="{standalone: true}"
										[readonly]="(!editable && isSetModeForm()) || readonly"
                                        [mask]="maskTime"
                                        placeholder="Hora"
                                        (keyup)="onInput()"/>

                                <a class="icon" (click)="onOpenPicker(picker);">
                                    <ion-icon name="calendar-outline"></ion-icon>
                                </a>

                            </div>

                            <p *ngIf="hasNow" class="links-bar">
                                &nbsp;<a (click)="onNow()"><small>agora</small></a>
                            </p>
                            
                            <error-input [control]="formControl" [submitted]="submitted">
                                <span *ngIf="hasError('date')">Data Inválida</span>
                            </error-input>

                            <input type="text" 
                                   [matDatepicker]="picker" 
                                   class="matInput"
                                   [(ngModel)]="date"
                                   [readonly]="!editable && isSetModeForm()"
                                   [ngModelOptions]="{standalone: true}"
                                   (dateChange)="onMatInput()">

                             <mat-datepicker #picker></mat-datepicker>

                        </div>`
})
export class DateInput extends BaseTextInput implements AfterViewInit
{
    @ViewChild("picker") pickerInput : any;
    date          : any     = null;
    dateString    : string  = null;
    timeString    : string  = null;
    dateMask        = "dd/MM/yyyy";
    timeMask        = "HH:mm";
    hasDatepicker  = false;
    hasTime        = false;
    hasSeconds     = false;
    hasNow         = false;
    lastTime       = false;
    maskDate        = "00/00/0000";
    maskTime 	    = "00:00";
    addDays 	    = 0;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);
		
        this.nameClass += this.formItem.name + "-input input col date-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";        
    }

    createValidators()
    {
        this.validators = [];

        if (!this.hidden)
        {
            if (this.formItem.required)
            {
                this.validators.push(Validators.required);
                this.validators.push(InputValidate.date);
            }			
        }

        /* DATEPICKER */
        if (this.setting.hasDatepicker)
        {
            this.hasDatepicker = true;
        }
		
        if (this.setting.hasTime)
        {
            this.hasTime = true;
        }

        if (this.setting.addDays)
        {
            this.addDays = this.setting.addDays;
        }

        if (this.setting.lastTime)
        {
            this.lastTime = this.setting.lastTime;
        } 
        
        if (this.setting.hasSeconds)
        {
            this.hasSeconds = this.setting.hasSeconds;
            this.maskTime   = "00:00:00";
            this.timeMask   = "HH:mm:ss";
        }
        
        if (this.setting.hasNow)
        {
            this.hasNow = this.setting.hasNow;
        }
    }

    onNow()
    {
        const date      = new TDate();
        this.dateString = date.format(this.dateMask);
        this.timeString = date.format(this.timeMask);    
        
        this.onInput();
    }

    onOpenPicker(picker:any)
    {
        if (this.editable && !this.readonly)
        {
            picker.open(); 
        }		
    }	

    onMatInput()
    {
        if (this.date)
        {
            this.date       = new TDate({ value : this.date });
            this.dateString = this.date.format(this.dateMask);

            this.updateDate();
        }
    }

    onInput()
    {
        const value = this.dateString + (this.hasTime ? " " + this.timeString : "");

        if (this.hasTime && (value.length == 16 || value.length == 19))
        {
            const mask = this.dateMask + " " + this.timeMask;
            this.date  = new TDate({ value : value, mask : mask });
        }
        else if (!this.hasTime && value.length == 10)
        {
            this.date = new TDate({ value : value, mask : this.dateMask });
        }

        this.updateDate();
    }

    updateDate()
    {
        if (this.date && this.date.isValid())
        {
            if (this.lastTime)
            {
                this.date.setHours(23);
                this.date.setMinutes(59);
                this.date.setSeconds(59);
            }
        
            const valueData = this.date.toDate();

            this.setValue(valueData);
            this.date = valueData;
        }
        else
        {
            this.setValue(null);
        }
    }

    /* METODO PARA CHAMADAS EXTERNAS */
    populate(value:any)
    {
        let date : any;
        let mask : string;

        if (this.hasTime)
        {
            mask = this.dateMask + " " + this.timeMask;
        }
        else
        {
            mask = this.dateMask;
        }

        date = new TDate({ value : value });        

        if (date.isValid())
        {
            if (this.addDays && this.isAddModeForm())
            {
                date = date.add(this.addDays, "days");
            }

            this.dateString = date.format(this.dateMask);

            if (this.hasTime)
            {
                this.timeString = date.format(this.timeMask);
            }

            const valueData = date.toDate();
            this.updateValue(valueData);

            this.date = valueData;
        }
        else
        {
            console.error("invalid date", date);
        }
    }

    reset()
    {   
        if (!this.field.fixed)
        {
            if (this.pickerInput)
            {
                this.pickerInput.select(null);
            }    

            this.date       = null;
            this.dateString = null;
            this.timeString = null;
        }

        super.reset();

        // ISSO NAO PODE FICAR AQUI
        // if (this.formItem.initial == "now")
        // {
                    
        //     this.onNow();
        //     this.markForCheck();
        // }        
    }

    valueDefault()
    {
        return null;
    }

    onModelChange(value : any)
    {
        if (value && !this.dirty)
        {
            this.dirty = true;
            this.populate(value);            
        }
    }
}
