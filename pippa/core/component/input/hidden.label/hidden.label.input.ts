import { Component, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';
import { BaseModel } from '../../../../core/model/base.model';

@Component({
    selector : `.hidden-label-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="item-input" [formGroup]="field.formGroup" [ngClass]="{'ng-submitted': submitted }">
                    <label>{{field.label}}</label>
                    <input type="hidden"
                           formControlName="{{formItem.name}}"
                           (ngModelChange)="onInit($event)"/>
                </div>`
})
export class HiddenLabelInput extends BaseInput implements AfterViewInit
{
    public isInit    : boolean = false;
    public bindValue : string  = 'value';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col hidden-label-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
    }

    onInit(value:any)
    {
        if(!this.isInit)
        {
            this.isInit = true;
            this.setValue(value);

            /* EVENT */
            if(this.field.onInit)
            {
                this.field.onInit(this.getValue());
            }
        }
    }

    ngAfterViewInit()
    {
        super.ngAfterViewInit();

        if(!this.isInit)
        {
            /* CASSO UM SELECT E ALTERADO PARA O HIDDEN NO CASO ONLY ADMIN*/
            if(this.field.option && this.field.option.items.length > 0)
            {
                /* SETTINGS */
                if(this.field.setting)
                {
                    /* BIND VALUE */
                    if(this.field.setting.bindValue)
                    {
                        this.bindValue = this.field.setting.bindValue;
                    }
                }

                for(const key in this.field.option.items)
                {
                    const item = this.field.option.items[key];

                    if(item.selected || this.field.initial == item[this.bindValue])
                    {
                        this.isInit = true;
                        this.setValue(item);
                        break;
                    }
                }
            }
            else if(this.field.initial)
            {
                this.isInit = true;

                if(this.field.initial instanceof BaseModel)
                {
                    this.setValue(this.field.initial.reference);
                }
                else
                {
                    this.setValue(this.field.initial);
                }
            }
            else if(this.field.setting)
            {
                this.isInit = true;
                this.setValue(this.field.setting);
            }
        }
    }
}
