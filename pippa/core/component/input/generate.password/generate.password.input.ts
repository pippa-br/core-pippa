import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';

/* PIPPA */
import { BaseInput } from '../base.input';

@Component({
    selector 		: `.generate-password-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<div class="input-wrapper"
							[ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
							<div header-input
								[form]="form"
								[formItem]="formItem"
								(edit)="onEdit($event)"
								(del)="onDel()">
							</div>
							<div class="item-input" [formGroup]="field.formGroup">
							
								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<ion-button (click)="onClick()" [disabled]="!editable">
									<ion-icon name="sync-outline"></ion-icon> Gerar
								</ion-button>
							</div>
						</div>`
})
export class GeneratePasswordInput extends BaseInput
{
	public count : number;
	public type  : string = 'string';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col generate-password-input ';
        this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
        
        this.count = 8;

        if(this.setting.count)
        {
            this.count = this.setting.count;
		}
		
		if(this.setting.type)
        {
            this.type = this.setting.type;
		}
    }

    onClick()
    {
		let password = '';
		
		if(this.type == 'string')
		{
			password = this.randomString(this.count);
		}		
		else if(this.type == 'number')
		{
			password = this.randomNumber(this.count);
		}

        if(!this.siblingFields)
        {
            this.siblingFields = ['password','confirmPassword'];
        }

        for(const key in this.siblingFields)
        {
            const value = this.siblingFields[key];
			this.form.getInstance(value).setValue(password);			
		}
		
		this.setValue(password);
    }

    randomString(length:number)
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
	}
	
	randomNumber(length:number)
	{
		var text = "";
		var possible = "0123456789";
		for(var i = 0; i < length; i++)
		{
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
}
