import { Component, AfterViewInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import SignaturePad from "signature_pad";

/* PIPPA */
import { BaseInput } from "../base.input";
import { File }      from "../../../../core/model/file/file";

@Component({
    selector        : ".signature-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                     [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                    <div header-input
                         [form]="form"
                         [formItem]="formItem"
                         (edit)="onEdit()"
                         (del)="onDel()">
                    </div>
                    <div class="item-input" [formGroup]="field.formGroup">
                        <input type="hidden"
                               formControlName="{{formItem.name}}"
                               (ngModelChange)="onModelChange($event)"/>
                        <canvas #template></canvas>
                        <a (click)="clearCanvas()">Limpar</a>
                    </div>
                    <error-input [control]="formControl" [submitted]="submitted">
                    </error-input>
                </div>`
})
export class SignatureInput extends BaseInput implements AfterViewInit
{
    @ViewChild("template", { static : false }) template : any;

    public signaturePad : any;
    public file : any;
    public signaturePadOptions : any = {
        minWidth : 3,
        onEnd  	 : () =>
        {
            this.drawComplete();
        }
    };

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + "-input input col signature-input ";
        this.nameClass += "row-" + this.formItem.row + " col-" + this.formItem.col + " ";
    }

    ngAfterViewInit()
    {
        this.signaturePad = new SignaturePad(this.template.nativeElement, this.signaturePadOptions);

        super.ngAfterViewInit();
    }	

    onModelChange(value:any)
    {
        if (value && !this.dirty)
        {
            this.dirty = true;
            this.file  = new File(value);
			
            const ratio = 100 / 500;

            this.signaturePad.fromDataURL(this.file.url, {
                ratio  : ratio, 
                width  : this.template.nativeElement.offsetWidth  - this.template.nativeElement.offsetWidth * ratio, 
                height : this.template.nativeElement.offsetHeight - this.template.nativeElement.offsetHeight * ratio
            });
        }
    }

    clearCanvas()
    {
        this.signaturePad.clear();
        this.setValue(null);
    }

    drawComplete()
    {
        this.dirty = true;

        this.file = new File({
            url : this.signaturePad.toDataURL()
        });

        this.setValue(this.file.parseData());
    }

    drawStart()
    {
        // OVERRIDER
    }
}
