import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { PopoverController } from '@ionic/angular';

/* PIPPA */
import { BaseInput }         from '../base.input';
import { FormFormPopover }   from '../../../popover/form.form/form.form.popover';
import { AttachmentPopover } from '../../../popover/attachment/attachment.popover';
import { Form }              from '../../../model/form/form';
import { AttachmentType }    from '../../../type/attachment/attachment.type';
import { FieldType }         from '../../../type/field/field.type';

@Component({
    selector 		: `attachment-input`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<a *ngIf="field && field.hasAttachments()" (click)="openAttachment($event);">
							<ion-icon name="attach-outline"></ion-icon>
					  </a>`
})
export class AttachmentInput extends BaseInput
{
    constructor(
        public popoverController : PopoverController,
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    openAttachment(event:any)
    {
        this.popoverController.create(
        {
            component      : AttachmentPopover,
            event          : event,
            componentProps :
            {
                field   : this.field,
                onInput : ((item:any) =>
                {
                    this.onAttachment(item);
                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
        })
        .then(popover =>
        {
            popover.present();
        });
    }

    onAttachment(attachment:any)
    {
        const items = [];

        if(attachment.value == AttachmentType.COMMENT.value)
        {
            items.push({
                id    : 'value',
                row   : 0,
                col   : 0,
                field : {
                    id       : 'value',
                    type     : FieldType.type('Textarea'),
                    name     : 'value',
                    label    : 'Comentário',
                    required : false,
                }
            });
        }
        else if(attachment.value == AttachmentType.BONUS.value)
        {
            items.push({
                id    : 'value',
                row   : 0,
                col   : 0,
                field : {
                    id       : 'value',
                    type     : FieldType.type('Percentage'),
                    name     : 'value',
                    label    : 'Bônus',
                    required : false,
                }
            });
        }
        else if(attachment.value == AttachmentType.DOCUMENT.value ||
                attachment.value == AttachmentType.AUDIO.value ||
                attachment.value == AttachmentType.IMAGE.value ||
                attachment.value == AttachmentType.IMAGE.value ||
                attachment.value == AttachmentType.GALLERY.value)
        {
            items.push({
                id    : 'value',
                row   : 0,
                col   : 0,
                field : {
                    id    : 'value',
                    type  : FieldType.type('UploadJson'),
                    name  : 'value',
                    label : 'Documentos',
                    setting : {
                        multiple : true,
                        accept   : attachment.accept,
                    }
                }
            });
        }

        console.log('onAttachment', attachment, items);

        const form = new Form({
            items : items
        });

        const data = this.getAttachmentByType(attachment);
        this.openForm(attachment, form, data);
    }

    openForm(attachment:any, form:any, data?:any)
    {
        this.popoverController.create(
        {
            component      : FormFormPopover,
            componentProps :
            {
                form  : form,
                data  : data,
                onAdd : ((event:any) =>
                {
                    this.setAttachmentByType(event.data.value, attachment);
                    this.attachmentEvent.emit(this.getAttachment());
                    this.popoverController.dismiss();
                }),
                onSet : ((event:any) =>
                {
                    this.setAttachmentByType(event.data.value, attachment);
                    this.attachmentEvent.emit(this.getAttachment());
                    this.popoverController.dismiss();
                }),
                onClose : (() =>
                {
                    this.popoverController.dismiss();
                }),
            },
            showBackdrop : false,
            cssClass : 'right-popover',
        })
        .then(popover =>
        {
            popover.present();
        });
    }

    trackById(index:any, item:any)
    {
        index;
        return item.id;
    }
}
