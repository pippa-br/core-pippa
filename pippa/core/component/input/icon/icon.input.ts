import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";

/* PIPPA */
import { BaseInput } from "../base.input";

@Component({
    selector      		: "icon-input",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<ion-icon *ngIf="!isLoading && icon && icon.value" [name]="icon.value"></ion-icon>
					   <ion-spinner *ngIf="isLoading" [name]="spinner"></ion-spinner>`
})
export class IconInput extends BaseInput
{
    @Input() spinner  = "crescent";
    @Input() icon    : any = {};

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }
}
