import { Component, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

/* PIPPA */
import { BaseTextInput } from '../base.text.input';
import { Document }      from '../../../model/document/document';

@Component({
    selector	 	: `.cvv-input`,
	changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="input-wrapper"
                            [ngClass]="{'ng-submitted' : submitted, 'ng-valid' : formControl?.valid && this.hasValue(), 'ng-invalid' : !formControl?.valid }">
                            <div header-input
                                [form]="form"
                                [formItem]="formItem"
                                (edit)="onEdit($event)"
                                (del)="onDel()">
                            </div>
                            <div class="item-input" [formGroup]="formGroup">

								<input type="hidden" formControlName="{{formItem.name}}" (ngModelChange)="onModelChange($event)"/>

								<input type="tel"
									autocomplete="false"
									[mask]="mask"
									[ngModel]="value"									
									[ngModelOptions]="{standalone: true}"
									(keyup)="onInput($event)"
									[placeholder]="field.placeholder"/>

								<error-input [control]="formControl" [submitted]="submitted">
									<span *ngIf="hasError('minlength')">Código de Seguração do Cartão Invalido</span>
									<span *ngIf="hasError('compareError')">Código de Seguração do Cartão Invalido!</span>
								</error-input>

							</div>
						</div>`
})
export class CVVInput extends BaseTextInput implements AfterViewInit
{
	public card 	 : any;
	public value     : string  = '';
	public isCompare : boolean = false;
	public mask      : string  = '9999';

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
        super(changeDetectorRef);
    }

    async createControls(formItem:any, formGroup:any)
    {
        await super.createControls(formItem, formGroup);

        this.nameClass += this.formItem.name + '-input input col cvv-field ';
		this.nameClass += 'row-' + this.formItem.row + ' col-' + this.formItem.col + ' ';
		
		if(this.setting.isCompare !== undefined)
		{
			this.isCompare = this.setting.isCompare;
		}
	}

	createValidators()
	{
        this.validators = [];

		if(!this.hidden)
		{
			if(this.formItem.required)
			{
				this.validators.push(Validators.required);
			}
			
			this.validators.push(Validators.minLength(3));	
		}
	}

	onInput(event:any)
    {
        this.value = event.srcElement.value;
		this.setValue(this.value);
		
        this.checkValidate();
    }

    checkValidate()
    {
        if(this.isCompare && this.card && this.card.creditCard && this.formItem.required)
        {
            this.delError('compareError');

            if(this.card.creditCard.cvv != this.value)
            {
                this.setError('compareError');
            }
        }        
    }

    ngAfterViewInit()
    {
		super.ngAfterViewInit();
		
        if(this.isCompare && this.formGroup.get('card'))
        {
            this.formGroup.get('card').registerOnChange(async (value:any) =>
            {
				this.card = new Document(value);
				await this.card.on();

                this.checkValidate();
            });
        }
    }

    onModelChange(value:any)
    {
        if(value && !this.dirty)
        {
            this.value = '****';
            this.dirty = true;
        }
    }
}
