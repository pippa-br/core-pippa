// page-navigation.component.ts
import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Core } from '../../../util/core/core';

@Component({
    selector        : '[number-pagination]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-toolbar>
                            <div class="none" slot="start">
                                <div *ngIf="hasBatch" class="allBatch">
                                    <input id="allSelected"
                                        class="checkbox-item"
                                        type="checkbox"
                                        [(ngModel)]="allSelected"
                                        (change)="onAllSelected()">

                                    <label for="allSelected">{{selecteds.length}} Selecionado(s)</label>                                    
                                </div>
                            </div>
                            <div class="controller">
                                <a class="firstPage" (click)="onPage(1)" matTooltip="Ir Para Primeira Página">
                                    <ion-icon name="chevron-back-outline"></ion-icon>
                                    <ion-icon name="chevron-back-outline"></ion-icon>
                                </a>
                                <a (click)="onPrevPage()" matTooltip="Próxima Página">
                                    <ion-icon name="chevron-back-outline"></ion-icon>
                                </a> 
                                <a *ngFor="let page of pages" [ngClass]="{'selected' : currentPage == page}" (click)="onPage(page)">{{ page }}</a>
                                <a (click)="onNextPage()" matTooltip="Voltar Página">
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                </a>
                                <a class="lastPage"  (click)="onPage(totalPage)" matTooltip="Ir Para Última Página">
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                </a>
                            </div>
                            <div class="total" slot="end">
                                <span class="pages">Página {{currentPage}} de {{totalPage}}</span> - <span *ngIf="total">{{total}} Iten(s)</span>
                            </div>
                       </ion-toolbar>`,
})
export class NumberPagination {

    @Input() total            : number = 0;
    @Input() public hasBatch : boolean = false;
    
    @Output('paging')      pagingEvent      : EventEmitter<any> = new EventEmitter();
    @Output('allSelected') allSelectedEvent : EventEmitter<any> = new EventEmitter();    
    
    public perPage     : number = 25;
    public totalPage   : number = 1;
    public _collection : any;
    public currentPage : number = 1;
    public allSelected : boolean = false;
    public _selecteds  : any;
    public isMobile    : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
    }

    get pages(): number[] 
    {
        this.totalPage        = Math.ceil(this.total / this.perPage);
        const maxVisiblePages = Core.get().isMobile() ? 3 : 10;
      
        let startPage = Math.max(1, this.currentPage - Math.floor(maxVisiblePages / 2));
      
        if(startPage + maxVisiblePages - 1 > this.totalPage) 
        {
          startPage = Math.max(1, this.totalPage - maxVisiblePages + 1);
        }
      
        const endPage = Math.min(startPage + maxVisiblePages - 1, this.totalPage);
      
        return Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);
    }

    @Input() 
    set collection(value:any)
    {
        this._collection = value;

        if(value)
        {
            this.perPage = this._collection.perPage;                  
        }        
    };

    get collection()
    {
        return this._collection;
    };

    @Input() 
    set selecteds(value:any)
    {
        this._selecteds = value;

        if(value && value.length == 0)
        {
            this.allSelected = false;
        }
    };

    get selecteds()
    {
        return this._selecteds;
    };

    onPage(event:any)
    {        
        console.log('nextPageOffset', event, (event - 1) * this.perPage);

        this.collection.nextPageOffset(event).then(() =>
        {                            
            this.pagingEvent.emit(event);
        });

        this.currentPage = event;
    } 

    onAllSelected()
    {        
        this.allSelectedEvent.emit(this.allSelected);
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }    

    hasNextPage()
    {
        return this.collection && this.collection.total >= this.collection.perPage;
    }

    onPrevPage()
    {
        if(this.currentPage > 1)
        {
            this.onPage(this.currentPage - 1);
        }
    }

    onNextPage()
    {
        if(this.currentPage < this.totalPage)
        {
            this.onPage(this.currentPage + 1);
        }
    }
}