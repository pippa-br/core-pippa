import { Component, Input, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { Core }  from '../../../util/core/core';

@Component({
    selector        : '[next-pagination]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<mat-paginator [length]="total"
									  [pageSize]="_perPage"
									  itemsPerPageLabel=""
                                      showPageNumbers="true"
									  nextPageLabel="Próxima Página"
									  previousPageLabel="Página Anterior"
									  firstPageLabel="Primeira Página"
									  lastPageLabel="Última Página"
									  getRangeLabel="getRangeLabel"
                                      [pageSizeOptions]="_pageSizeOptions"                                      
                                      (page)="onPage($event)">
                       </mat-paginator>`,
})
export class NextPagination implements OnDestroy
{
    @Input()
    public total            : number = 0;
    public _perPage         : number = 24;
    public _pageSizeOptions : number[];
    public _subscribeChange : any;
    public _subscribeSearch : any;
    public _collection      : any;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
    }

    @Input() 
    set collection(value:any)
    {
        this._collection = value;

        if(value)
        {
            this._perPage = this._collection.perPage;

            this._pageSizeOptions = [];
            this._pageSizeOptions.push(this._perPage * 1);
            this._pageSizeOptions.push(this._perPage * 2);
            this._pageSizeOptions.push(this._perPage * 3);
            this._pageSizeOptions.push(this._perPage * 4);
            
            this._subscribeChange = value.onUpdate().subscribe(() => 
            {   
                this._perPage = this._collection.perPage;   
                
                this.changeDetectorRef.markForCheck();
            });            
        }        
    };

    get collection()
    {
        return this._collection;
    };

    onPage(event:any)
    {        
        console.log('onPage', event);

        if(event.previousPageIndex == event.pageIndex) /* ALTEROU O PAGESIZE */
        {
            this.collection._query.perPage = event.pageSize;

            this.collection.reload();
        }
        else if(event.previousPageIndex > event.pageIndex) /* PREV */
        {
            this.onPrevPage();
        }
        else  /* NEXT */
        {
            this.onNextPage();
        }
    }

    onPrevPage()
    {
        if(this.collection.hasPrevPage())
        {
            this.collection.prevPage().then(() =>
            {
                console.log('onPrevPage');
            })
        }
    }

    onNextPage()
    {
        if(this.collection.hasNextPage())
        {
            this.collection.nextPage().then(() =>
            {
                console.log('onNextPage');
            });
        }
    }

    ngOnDestroy()
    {
        if(this._subscribeChange)
        {
            this._subscribeChange.unsubscribe();
        }

        if(this._subscribeSearch)
        {
            this._subscribeSearch.unsubscribe();
        }
	}
	
	getRangeLabel = function (page, pageSize, length) 
    {
		if (length === 0 || pageSize === 0) {
		  return '0 de ' + length;
		}

		length = Math.max(length, 0);
		
        const startIndex = page * pageSize;
		// If the start index exceeds the list length, do not try and fix the end index to the end.
		const endIndex = startIndex < length ?
		  Math.min(startIndex + pageSize, length) :
		  startIndex + pageSize;
		return startIndex + 1 + ' - ' + endIndex + ' de ' + length;
	};

    core()
    {
        return Core.get()
    }
}
