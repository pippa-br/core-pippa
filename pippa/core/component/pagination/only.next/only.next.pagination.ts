import { Component, Input, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';

/* PIPPA */
import { Core }  from '../../../util/core/core';

@Component({
    selector        : '[only-next-pagination]',
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-toolbar>
                            <div class="none" slot="start">
                                <div *ngIf="hasBatch" class="allBatch">
                                    <input id="allSelected"
                                        class="checkbox-item"
                                        type="checkbox"
                                        [(ngModel)]="allSelected"
                                        (change)="onAllSelected()">

                                    <label for="allSelected">{{selecteds.length}} Selecionado(s)</label>                                    
                                </div>
                            </div>
                            <div class="footerBar">
                                <ion-icon [ngClass]="{'noPrevPage' : collection && !collection.hasPrevPage()}" (click)="onPrevPage()" name="chevron-back-circle-outline"></ion-icon>                                                                                            
                                <ion-icon [ngClass]="{'noNextPage' : !hasNextPage()}" (click)="onNextPage()"name="chevron-forward-circle-outline"></ion-icon>                             
                            </div>
                            <div class="total" slot="end"><span *ngIf="total">Total Iten(s): {{total}}</span></div>
                       </ion-toolbar>`,
})
export class OnlyNextPagination implements OnDestroy
{
    public _subscribeChange : any;
    public _subscribeSearch : any;
    public _collection      : any;
    public _total           : number;
    public _selecteds       : any;
    public allSelected      : boolean = false;
    
    @Output('paging')      pagingEvent      : EventEmitter<any> = new EventEmitter();
    @Output('allSelected') allSelectedEvent : EventEmitter<any> = new EventEmitter();
    
    @Input() public hasBatch : boolean = false;

    constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
    }

    @Input() 
    set collection(value:any)
    {        
        this._collection = value;        
    };

    get collection()
    {
        return this._collection;
    };

    @Input() 
    set total(value:any)
    {
        this._total = value;     
    };

    get total()
    {
        return this._total;
    };

    @Input() 
    set selecteds(value:any)
    {
        this._selecteds = value;

        if(value && value.length == 0)
        {
            this.allSelected = false;
        }
    };

    get selecteds()
    {
        return this._selecteds;
    };

    onPage(event:any)
    {        
        console.log('onPage', event);

        if(event.previousPageIndex == event.pageIndex) /* ALTEROU O PAGESIZE */
        {
            this.collection._query.perPage = event.pageSize;

            this.collection.reload();
        }
        else if(event.previousPageIndex > event.pageIndex) /* PREV */
        {
            this.onPrevPage();
        }
        else  /* NEXT */
        {
            this.onNextPage();
        }
    }

    hasNextPage()
    {
        return this.collection && this.collection.total >= this.collection.perPage;
    }

    onPrevPage()
    {
        if(this.collection.hasPrevPage())
        {
            this.collection.prevPage().then(() =>
            {                
                console.log('onPrevPage');
                this.pagingEvent.emit();
            })
        }
    }

    onNextPage()
    {
        if(this.hasNextPage())
        {
            this.collection.nextPage().then(() =>
            {                
                console.log('onNextPage');
                this.pagingEvent.emit();
            });
        }
    }

    onAllSelected()
    {        
        this.allSelectedEvent.emit(this.allSelected);
    }

    markForCheck()
	{
		this.changeDetectorRef.markForCheck();
	}

    ngOnDestroy()
    {
        if(this._subscribeChange)
        {
            this._subscribeChange.unsubscribe();
        }

        if(this._subscribeSearch)
        {
            this._subscribeSearch.unsubscribe();
        }
	}

    core()
    {
        return Core.get()
    }
}
