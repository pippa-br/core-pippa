/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { FormItem }             from '../../../model/form.item/form.item';
import { Output }               from '../../../model/output/output';
import { Document }             from '../../../model/document/document';

export class ReferenceCouponTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                if(value)
                {
                    let labelField : string = '${_code}';

                    /* LABEL FIELD */
                    if(formItem.field.setting && formItem.field.setting.labelField)
                    {
                        labelField = formItem.field.setting.labelField;
                    }

                    const document : any = new Document(value);

                    document.on().then(() =>
                    {
                        document.getPathValue(labelField).then((format:string) =>
                        {
                            if(!format || format == 'undefined')
                            {
                                format = '';
                            }

                            format += ' <small>(' + document._percentage + '%)</small>'

                            resolve(new Output({
                                formItem    : formItem,
                                format      : format,
                                label       : formItem.label,
                                data        : data,
                                value       : document,
                                count       : 1,
                                total       : 1,
                                parseAmount : (amount:number) =>
                                {
                                    if(document._percentage)
                                    {
                                        amount = amount - (document._percentage * amount) / 100;
                                    }
                                    else if(document._value)
                                    {
                                        amount = amount - document._value;
                                    }

                                    return amount;
                                }
                            }));
                        });
                    });
                }
                else
                {
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        data     : data,
                        count    : 0,
                        total    : 0,
                    }));
                }
            });
        });
    }
}
