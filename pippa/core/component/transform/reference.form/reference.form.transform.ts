/* PIPPA */
import { BaseTransform }       from '../../transform/base.transform';
import { SumFormTransform }    from '../../transform/sum.form/sum.form.transform';
import { WhereCollection }     from '../../../model/where/where.collection';
import { Output }              from '../../../model/output/output';
import { FieldType }           from '../../../type/field/field.type';
import { ReferenceCollection } from '../../../model/reference/reference.collection';
import { Core }   			   from '../../../util/core/core';

export class ReferenceFormTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        { 
            data.getPathValue(formItem.path).then((form:any) =>
            {
				resolve(new Output({
					formItem : formItem,
					label    : formItem.label,
					data     : data,
					form     : form,
				}));
				
				// FEITO NO BACK
				/*if(form)
				{
					const params = ReferenceFormTransform.createParams({
						accid           : form.accid,
						appid           : form.appid,
						colid           : 'documents',
						orderBy         : 'postdate',
						asc             : false,
						perPage         : 1,
						collectionModel : ReferenceCollection,
					});

					params.where = new WhereCollection();

					params.where.add({
						field    : 'owner',
						operator : '==',
						value    : this.core().user,
						type     : FieldType.type('ReferenceSelect')
					});
	
					params.where.add({
						field    : 'form',
						operator : '==',
						value    : {
							referencePath : form.referencePath
						},
						type : FieldType.type('ReferenceSelect')
					});

					if(formItem.setting && formItem.setting.lessonPath)
					{
						params.where.add({
							field    : formItem.setting.lessonPath,
							operator : '==',
							value    : {
								referencePath : data.referencePath
							},
							type : FieldType.type('ReferenceSelect')
						});
					}
	
					ReferenceFormTransform.api().getList(params).then((result:any) =>
					{
						if(result.collection.length > 0)
						{
							const item = result.collection[0];

							resolve(new Output({
								formItem : formItem,
								label    : formItem.label,
								value    : item,
								data     : data,
								form     : form,						
							}));
						}
						else
						{
							resolve(new Output({
								formItem : formItem,
								label    : formItem.label,
								value    : null,
								data     : data,
								form     : form,						
							}));
						}
					});
				}
				else
				{
					
				}*/                
            });
        });
    }
}
