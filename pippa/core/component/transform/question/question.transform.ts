/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class QuestionTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let amount = 0;
				let total  = 0;
				let count  = 0;
                let items  = formItem.field.option.items;

                if(items[value] != undefined)
                {
					if(items[value].correct)
					{
						amount = 1;
						count  = 1;
					}
					
					total = 1;					
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    data     : data,
                    total    : total,
					count    : count,
					amount   : amount,
                }));
            });
        });
    }
}
