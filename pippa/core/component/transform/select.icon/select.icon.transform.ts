/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { IModel }        from '../../../interface/model/i.model';
import { Output }        from '../../../model/output/output';
import { File }          from '../../../model/file/file';
import { FormItem }      from '../../../model/form.item/form.item';

export class SelectIconTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {            
                const file = new File(value);
                
                resolve(new Output({
                    formItem : formItem,
                    value    : file,
                    data     : data,
                }));
            });
        });
    }
}
