import validator from 'validator';

/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class OptionTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((map:any) =>
            {
                let total  = 0;
                let count  = 0;
				let amount = 0;
				let format = '';

                /* FOI SALVO O VALOR NÃO O OBJETO */
                if(typeof map == 'string')
                {
                    map = {
                        label : map,
                        value : map
                    }
				}

				const hasSum = formItem.field.hasSum;

                /* PRECISA REVER ONDE DEVE FICAR ESSA INFORMACAO */
                if(map && map.value != undefined)
                {
					if(hasSum)
					{
						count = 1;
						total = 1;
	
						if(map.value == '*')
						{
							/* NAO CALCULA O AMOUNT */
						}
						else if(typeof map.value == 'number')
						{
							amount = map.value;
						}
						else if(typeof map.value == 'string' && validator.isNumeric(map.value))
						{
							amount = parseFloat(map.value);
						}
					}								

					format = map.label;
				}							

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : map,
                    data     : data,
                    total    : total,
                    count    : count,
					amount   : amount,
					format   : format,
                }));
            });
        });
    }
}
