/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { File }                 from '../../../model/file/file';
import { FileCollection }       from '../../../model/file/file.collection';
import { FormItem }             from '../../../model/form.item/form.item';

export class UploadJsonTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                const files : any = new FileCollection();

                if(value && value != '')
                {
                    /* EM CASO QUE VC PRECISA PEGA A IMAGEM DENTRO DE UM OBJETO : EX: thumb do video*/
                    if(typeof value == 'string')
                    {
                        files.push(new File({
                            url : value
                        }));
                    }
                    else if(value.length != undefined)
                    {
                        files.addItems(value);
                    }
                    else
                    {
                        files.push(new File(value));
                    }
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : files,
                    data     : data,
                }));
            });
        });
    }
}
