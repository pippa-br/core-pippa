/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

//@dynamic
export class BankTransform extends BaseTransform
{
    constructor()
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    data     : data,
                    value    : value.bank.label + '<br>Ag.: ' + value.agencynumber + ' - ' + value.agencydigit
                            + ' CC.: ' + value.accountnumber + ' - ' + value.accountdigit + '<br>' + value.accounttype.label,
                }));
            });
        });
    }
}
