/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { Output }        from '../../../model/output/output';
import { BaseList }      from '../../../model/base.list';
import { BaseModel } from '../../../model/base.model';

export class StockProductsTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>(async (resolve) =>
        {
			const output = await StockProductsTransform.getOutput(formItem, data);

			resolve(output);
        });
	}

	static getOutput(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then(async (value:any) =>
            {
				const promises = [];

				for(const item of value)
				{
					const product = new BaseModel(item.product);
					promises.push(product.on());
				}

				const list : any = await Promise.all(promises);
				
				const promises2 = [];

				for(const item of list)
				{
					const stockTable = new BaseModel(item.stockTable[data.stockName.code]);
					promises2.push(stockTable.on());
				}

				const values2 : any = await Promise.all(promises2);

				for(const key in values2)
				{
					list[key].stockTable = values2[key]
				}

				resolve(new Output({
					formItem   : formItem,
					label      : formItem.label,
					value      : list,
					data       : data,
				}));
            });
        });
	}
}
