import { Directive } from '@angular/core';

/* PIPPA */
import { Core }   			from '../../util/core/core';
import { Params } 			from '../../util/params/params';
import { TransformFactory } from '../../factory/transform/transform.factory';

@Directive()
export class BaseTransform
{
    public formItem : any;

    static createResult(params:Params)
    {
        return this.core().resultFactory.create(params);
    }

    static createParams(args:any)
    {
        return this.core().paramsFactory.create(args);
    }

    static core()
    {
        return Core.get()
    }

    static api()
    {
        return Core.get().api
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        formItem;
        data;

        return;
	}
	
	static sumItems(items:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
			/* COUNTS */
			const promises = [];

			for(const key in items)
			{
				//if(items[key].field.editable)
				//{
					const promise = TransformFactory.transform(items[key], data);
					promises.push(promise);	
				//}
			}

			Promise.all(promises).then(values2 =>
			{
				let labels       : Array<string> = [];
				let values       : Array<any>    = [];
				let parseAmounts : Array<any>    = [];
				let total        : number = 0;
				let count        : number = 0;
				let amount       : number = 0;
				let percentage   : number = 0;

				for(let key in values2)
				{
					total  += (values2[key].total  ? values2[key].total  : 0);
					amount += (values2[key].amount ? values2[key].amount : 0);
					count  += (values2[key].count  ? values2[key].count  : 0);

					labels.push(values2[key].label);
					values.push(values2[key].value);                            

					if(values2[key].parseAmount)
					{
						parseAmounts.push(values2[key].parseAmount);
					}
				}

				/* PERCENTAGE */
				if(count)
				{
					percentage = Math.round((count * 100) / total);
				}

				/* PARSE AMOUNTS */
				for(let key in parseAmounts)
				{
					const parseAmount = parseAmounts[key];
					amount            = parseAmount(amount);
				}                 
				
				resolve({
					label      : labels,
					value      : values,
					data       : data,
					count      : count,
					percentage : percentage,
					total      : total,
					amount     : amount,
				});
			});
		});
	}
}
