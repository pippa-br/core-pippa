/* PIPPA */
import { BaseTransform }       from '../../transform/base.transform';
import { IModel }              from '../../../interface/model/i.model';
import { ReferenceCollection } from '../../../model/reference/reference.collection';
import { Output }              from '../../../model/output/output';
import { FormItem }            from '../../../model/form.item/form.item';
import { Form }                from '../../../model/form/form';
import { FieldType }           from '../../../type/field/field.type';
import { TransformFactory }    from '../../../factory/transform/transform.factory';

export class SumDocumentsTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const output : any = new Output({
                data       : data,
                count      : 0,
                percentage : 0,
                total      : 0,
                amount     : 0,
            });

            if(data)
            {
                if(data instanceof Form)
                {
                    SumDocumentsTransform.load(formItem, data).then(item =>
                    {
                        output.total      = item.total;
                        output.count      = item.count;
                        output.percentage = item.percentage;
                        output.amount     = item.amount;

                        resolve(output);
                    });
                }
                else if(data.form)
                {
                    data.form.on().then(() =>
                    {
                        SumDocumentsTransform.load(formItem, data.form).then(item =>
                        {
                            output.total      = item.total;
                            output.count      = item.count;
                            output.percentage = item.percentage;
                            output.amount     = item.amount;

                            resolve(output);
                        });
                    });
                }
            }
            else
            {
                resolve(output);
            }
        });
    }

    static load(formItem:any, form:any)
    {
        return new Promise<any>((resolve) =>
        {
            let total      : number = 0;
            let count      : number = 0;
            let percentage : number = 0;
            let amount     : number = 0;

            const params = this.createParams({
                path  : form.accid + '/' + form.appid + '/documents',
                order : 'order',
                asc   : false,
                collectionModel : ReferenceCollection,
            });

            if(formItem && formItem.field.where)
            {
                params.where = formItem.field.where.slice();
            }
            else
            {
                params.where = [];
            }

            params.where.push({
                field    : 'form',
                operator : '==',
				value    : form.reference,
				type     : FieldType.type('ReferenceSelect')
            });

            this.api().getList(params).then((result:any) =>
            {
                const promises = [];

                for(const key in result.collection)
                {
                    const _formItem = new FormItem({
                        field : {
                            type : FieldType.type('SumForm')
                        }
                    });

                    const promise = TransformFactory.transform(_formItem, result.collection[key]);
                    promises.push(promise);
                }

                Promise.all(promises).then(values =>
                {
                    for(const key in values)
                    {
                        const item = values[key];
                        total     += item.total;
                        count     += item.count;
                        amount    += item.amount;
                    }

                    if(count)
                    {
                        percentage = Math.round((count * 100) / total);
                    }

                    resolve(new Output({
                        total      : total,
                        count      : count,
                        percentage : percentage,
                        amount     : amount,
                    }));
                });
            });
        });
    }
}
