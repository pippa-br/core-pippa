/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { FormItem }             from '../../../model/form.item/form.item';
import { Output }               from '../../../model/output/output';
import { Document }             from '../../../model/document/document';

export class ReferenceTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {                
				let count = 0;
				let total = 0;

				const hasSum = formItem.field.hasSum;

                if(value)
                {
					if(hasSum)
					{
						total = 1;
						count = 1;
					}

                    if((typeof value == 'string') || (typeof value == 'number'))
                    {
                        resolve(new Output({
                            formItem : formItem,
                            label    : formItem.label,
                            format   : value,
                            data     : data,
                            count    : count,
                            total    : total,
                        }));
                    }
                    else
                    {
						let fieldLabel : string = '${name}';
												
                        /* LABEL FIELD */
                        if(formItem.setting && formItem.setting.fieldLabel)
                        {
                            fieldLabel = formItem.setting.fieldLabel;
						}
						
                        const model = new Document(value);

                        model.on().then(() =>
                        {
                            model.getPathValue(fieldLabel).then((format:string) =>
                            {
                                if(!format || format == 'undefined')
                                {
                                    format = '';
								}
								
                                resolve(new Output({
                                    formItem : formItem,
                                    label    : formItem.label,
                                    format   : format,
                                    data     : data,
                                    value    : model,
                                    count    : count,
                                    total    : total,
                                }));
                            });
                        });
                    }
                }
                else
                {
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        data     : data,
                        count    : count,
                        total    : total,
                    }));
                }
            });
        });
    }
}
