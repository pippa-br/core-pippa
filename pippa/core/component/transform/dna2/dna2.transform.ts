/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class DNA2Transform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                const columns = formItem.field.option.columns;
				let   count   = 0;

                /* CALCULA RESULTADOS */
                for(let key in value)
                {
                    for(let key2 in columns)
                    {
                        if(!columns[key2].total)
                        {
                            columns[key2].total = 0;
                        }

                        if(columns[key2].value == value[key].value.value)
                        {
							columns[key2].total++;
							
							count++;
						}												
                    }
                }

                for(let key in columns)
                {
                    columns[key].percentage = Math.round((columns[key].total * 100) / count);
				}
				
                /* DOCUMENTS */
                /*if(formItem.field.option.documents)
                {
                    for(let key in formItem.field.option.documents)
                    {
                        for(let key2 in answers)
                        {
                            if(formItem.field.option.documents[key].name.value == answers[key2].value)
                            {
                                answers[key2].text = formItem.field.option.documents[key].text;
                            }
                        }
                    }
                }*/

                resolve(new Output({
                    label    : formItem.label,
                    formItem : formItem,
                    data     : data,
                    value    : {
                        columns : columns,
                    },
                }));
            });
        });
    }
}
