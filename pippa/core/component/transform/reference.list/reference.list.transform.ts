/* PIPPA */
import { BaseTransform } from "../../transform/base.transform";
import { Output }        from "../../../model/output/output";
import { BaseList }      from "../../../model/base.list";
import { Grid }          from "../../../model/grid/grid";
import { Types } from "../../../type/types";
import { environment } from "src/environments/environment";

export class ReferenceListTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then(async (value:any) =>
            {
                if (value && Object.keys(value).length > 0)
                {
                    const list = new BaseList(value);
                    let grid   = null;                    

                    if (formItem.grid && formItem.grid.items && formItem.grid.items.length > 0)
                    {
                        grid                 = new Grid(formItem.grid);
                        grid.fixedHeader     = true;
                        grid.hasViewerButton = true;
                    }

                    if (formItem.setting && formItem.setting.gridPath)
                    {
                        const paths = formItem.setting.gridPath.split("/");

                        const params = this.createParams({
                            getEndPoint : Types.GET_DOCUMENT_API,
                            accid       : paths[0],
                            appid       : paths[1],
                            colid       : paths[2],
                            path        : formItem.setting.gridPath,
                            model       : Grid,
                            mapItems    : {
                                referencePath : environment.defaultMapItems
                            }
                        })

                        const result = await this.core().api.getObject(params);

                        grid                 = result.data;
                        grid.fixedHeader     = true;
                        grid.hasViewerButton = true;
                    }

                    list.on().then(() =>
                    {
                        resolve(new Output({
                            formItem : formItem,
                            label    : formItem.label,
                            value    : list,
                            data     : data,
                            grid     : grid,
                        }));
                    });

                    /*let i     = 0;
                    let size  = value.length;
                    let items = [];

                    for(let key in value)
                    {
                        value[key].onSnapshot((item:any) =>
                        {
                            let model = new Reference();
                            model.populate(item.data());
                            model._reference = item.ref;

                            items.push(model);

                            i++;

                            if(i == size)
                            {
                                resolve(new Output({
                                    label    : formItem.label,
                                    value    : items,
                                    formItem : formItem,
                                    data     : data,
                                }));
                            }
                        });
                    }*/
                }
                else
                {
                    resolve(new Output({
                        label    : formItem.label,
                        value    : [],
                        formItem : formItem,
                        data     : data,
                    }));
                }
            });
        });
    }
}
