/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class DiscTransform extends BaseTransform
{
    constructor()
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let text : string = '';
                const disc : any = {
                    'D' : 0,
                    'I' : 0,
                    'S' : 0,
                    'C' : 0,
                };
                const answers : any = {
                    'A' : 0,
                    'B' : 0,
                    'D' : 0,
                    'E' : 0,
                };

                /* CALCULA RESULTADOS */
                for(const key in value)
                {
                    const v = (value[key].value ? value[key].value : value[key]);
                    answers[v]++;
                }

                /* CALCULA BOXES */
                const size   = (formItem.field.option.rows.length % 2 == 0 ? formItem.field.option.rows.length + 4 : formItem.field.option.rows.length + 5);
                const square = [];

                for(let i = 0; i < size; i++)
                {
                    if(!square[i])
                    {
                        square[i] = [];
                    }

                    for(let j = 0; j < size; j++)
                    {
                        square[i][j] = 0;
                    }
                }

                /* PRINT E */
                for(let i = 0; i < size; i++)
                {
                    for(let j = (size / 2) - 1; j > (size / 2) - 1 - answers['E']; j--)
                    {
                        square[i][j] = 1;
                    }
                }

                /* PRINT D */
                for(let i = 0; i < size; i++)
                {
                    for(let j = (size / 2); j < (size / 2) + answers['D']; j++)
                    {
                        square[i][j] = 1;
                    }
                }

                /* PRINT B */
                for(let i = (size / 2); i < (size / 2) + answers['B']; i++)
                {
                    for(let j = 0; j < size; j++)
                    {
                        square[i][j] = 1;
                    }
                }

                /* PRINT A */
                for(let i = (size / 2) - 1; i > (size / 2) - 1 - answers['A']; i--)
                {
                    for(let j = 0; j < size; j++)
                    {
                        square[i][j] = 1;
                    }
                }

                /* TOTAL C */
                for(let i = 0; i < (size / 2); i++)
                {
                    for(let j = 0; j < (size / 2); j++)
                    {
                        if(square[i][j] == 1)
                        {
                            disc['C']++;
                        }
                    }
                }

                /* TOTAL D */
                for(let i = 0; i < (size / 2); i++)
                {
                    for(let j = (size / 2); j < size; j++)
                    {
                        if(square[i][j] == 1)
                        {
                            disc['D']++;
                        }
                    }
                }

                /* TOTAL S */
                for(let i = (size / 2); i < size; i++)
                {
                    for(let j = 0; j < (size / 2); j++)
                    {
                        if(square[i][j] == 1)
                        {
                            disc['S']++;
                        }
                    }
                }

                /* TOTAL I */
                for(let i = (size / 2); i < size; i++)
                {
                    for(let j = (size / 2); j < size; j++)
                    {
                        if(square[i][j] == 1)
                        {
                            disc['I']++;
                        }
                    }
                }

                let masterValue = 0;
                let masterKey : any;
                //const totalPercentage = 0;

                for(const key in disc)
                {
                    if(disc[key] > masterValue)
                    {
                        masterValue = disc[key];
                        masterKey   = key;
                    }

                    //totalPercentage += disc[key];
                }

                /* CALCULA PORCENTAGENS */
                const _percentages = {
                    'D' : Math.round((answers['A'] + answers['D']) * 3.57 * 100) / 100,
                    'I' : Math.round((answers['D'] + answers['B']) * 3.57 * 100) / 100,
                    'S' : Math.round((answers['E'] + answers['B']) * 3.57 * 100) / 100,
                    'C' : Math.round((answers['A'] + answers['E']) * 3.57 * 100) / 100,
                };

                /*for(const key in disc)
                {
                    const v = (disc[key] / totalPercentage) * 100;
                    _percentages[key] = Math.round(v);
                }*/

                const percentages = [];

                for(const key in _percentages)
                {
                    percentages.push({
                        label : key,
                        value : _percentages[key],
                    });
				}
				
                /* DOCUMENTS */
                if(formItem.field.option.documents)
                {
                    for(const key in formItem.field.option.documents)
                    {
                        if(formItem.field.option.documents[key].name == masterKey)
                        {
                            text = formItem.field.option.documents[key].text;
                        }
                    }
                }

                resolve(new Output({
                    label    : formItem.label,
                    formItem : formItem,
                    total    : 1,
                    data     : data,
                    count    : 1,
                    value : {
                        answers     : answers,
                        value       : masterValue,
                        disc        : disc,
                        key         : masterKey,
                        square      : square,
                        percentages : percentages,
                        text        : text,
                    },
                }));
            });
        });
    }
}
