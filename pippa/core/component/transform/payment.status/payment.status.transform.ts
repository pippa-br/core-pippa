/* PIPPA */
import { BaseTransform }   from '../../transform/base.transform';
import { Output }          from '../../../model/output/output';

export class PaymentStatusTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
			data.getPathValue(formItem.path).then((payment:any) =>
            {
				if(!payment || payment.status == 'refused')
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status : 'Recusado',
					}));
				}
				else if(payment.status == 'failed')
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status   : 'Recusado',
					}));
				}
				else if(payment.status == 'paid')
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status : 'Pago',
					}));
				}
				else if(payment.status == 'new')
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status : 'Novo',
					}));
				}
				else if(payment.status == 'canceled')
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status : 'Cancelado',
					}));
				}
				else
				{
					resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						value    : payment,
						data     : data,
						status : 'Aguardando'
					}));
				}
			});            
        });
    }    
}
