/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { FormItem }             from '../../../model/form.item/form.item';
import { Output }               from '../../../model/output/output';
import { Viewer }               from '../../../model/viewer/viewer';
import { Reference }            from '../../../model/reference/reference';

export class FieldsetTransform extends BaseTransform
{
    constructor(
    )
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                const viewer = new Viewer({
                    type  : Viewer.FLAT_TYPE,
                    items : formItem.items,
                });

                const _value = new Reference();
                _value.populate(value);

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : _value,
                    data     : data,
                    viewer   : viewer,
                }));
            });
        });
    }
}
