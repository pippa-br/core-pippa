/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { Output }               from '../../../model/output/output';
import { IModel }               from '../../../interface/model/i.model';
import { FormItem }             from '../../../model/form.item/form.item';

export class AddressTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((address:any) =>
            {
                let formart : string = '';
                let value   : Array<any> = []
                
                if(typeof address == 'object')
                {
                    value = [
                        {
                            label : 'Rua',
                            value : address.street,
                        },
                        {
                            label : 'Nº',
                            value : address.housenumber,
                        },
                        {
                            label : 'Complemento',
                            value : address.complement,
                        },
                        {
                            label : 'Bairro',
                            value : address.district,
                        },
                        {
                            label : 'Cidade',
                            value : address.city,
                        },
                        {
                            label : 'Estado',
                            value : address.state,
                        },
                        {
                            label : 'CEP',
                            value : address.zipcode,
                        },
                    ];

                    formart = address.street + ', ' + address.housenumber + ' ' + address.complement
                            + ', ' + address.district + ' - ' + address.city + ' / ' + address.state + ' - '
                            + address.zipcode;
                }
                else
                {
                    formart = address;
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    data     : data,
                    value    : value,
                    format   : formart,
                }));
            });
        });
    }
}
