/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { Output }        from '../../../model/output/output';

export class StepItemTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any> 
    {
        return new Promise<any>(async (resolve) =>
        {
            const value = await data.getPathValue('_steps.' + formItem.path);			
			
			resolve(new Output({
				formItem : formItem,
				label    : formItem.label, 
				data     : data,
				value    : value,
			}));
        });
	}	
}
