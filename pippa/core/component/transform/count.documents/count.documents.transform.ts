/* PIPPA */
import { BaseTransform }       from '../../transform/base.transform';
import { IModel }              from '../../../interface/model/i.model';
import { ReferenceCollection } from '../../../model/reference/reference.collection';
import { Output }              from '../../../model/output/output';
import { FormItem }            from '../../../model/form.item/form.item';
import { Form }                from '../../../model/form/form';
import { FieldType }           from '../../../type/field/field.type';
import { TransformFactory }    from '../../../factory/transform/transform.factory';

export class CountDocumentsTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>(async (resolve) =>
        {
			const plugin = formItem.option.app.getPlugin();

			const params = this.createParams({
				appid : formItem.option.app.code,
			});

			if(formItem.filters)
			{
				const where = formItem.filters.copy();

				for(const key in where)
				{
					where[key].innerDocument = data;
				}

				params.where = where;
			}

			const result = await plugin.count(params);

            resolve(new Output({
				formItem : formItem,
				data     : data,
				value    : result.count
			}));
        });
    }
}
