/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { Output }        from '../../../model/output/output';
import { TDate } from '../../../util/tdate/tdate';

export class VerifyPaymentTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((invoice:any) =>
            {   
                if(invoice && invoice.payment)
                {
                    VerifyPaymentTransform.verify(invoice, invoice.payment).then(result =>
                    {
                        resolve(new Output({
                            formItem : formItem,
                            label    : formItem.label,
                            value    : invoice,
                            data     : data,
														status   : result.status,
														format   : result.description,
                        }));
                    });
                }                
                else
                {
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        value    : null,
                        data     : data,
                        format   : '///',
                    }));
                }
            });
        });
    }

    static verify(invoice:any, payment:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
			if(payment.orders_paid == undefined)
			{
				if(payment.status == 'refused' || payment.status == 'failed' || payment.status == 'rejected')
				{
					resolve({
						status      : 'Recusado',
						description : 'Seu pagamento foi recusado! é necessário entrar em contato com a administradora do seu cartão. Em breve faremos uma próxima tentativa!',
					});
				}
				else if(payment.status == 'new')
				{
					resolve({
						status : 'Novo',
					})
				}
				else if(payment.status == 'paid' || payment.status == 'approved')
				{
					resolve({
						status : 'Pago',
					})
				}
				else if(payment.status == 'refunded')
				{
					resolve({
						status : 'Estornado',
					})
				}
				else if(payment.status == 'canceled' || payment.status == 'cancelled')
				{
					resolve({
						status : 'Cancelado',
					})
				}
				else if(payment.charges)
				{
					if(payment.charges && payment.charges.length > 0 && payment.charges[0].status == 'failed')
					{
						if(payment.charges[0].last_transaction.antifraud_response.status == 'reproved')
						{
							resolve({
								status      : 'Antifraud',
								description : 'Seu pagamento foi reprovado pelo antifraud! Seu dados serão analisados e tentaremos um novo processamento em breve!',
							});
						}
						else
						{
							resolve({
								status      : 'Recusado',
								description : 'Seu pagamento foi recusado! é necessário entrar em contato com a administradora do seu cartão. Em breve faremos uma próxima tentativa!',
							});	
						}
					}
					else if(payment.charges && payment.charges.length > 0 && payment.charges[0].status == 'paid')
					{
						resolve({
							status : 'Pago',
						})
					}
					else if(payment.charges && payment.charges.length > 0 && payment.charges[0].payment_method == 'boleto')
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><a class="boleto-button" target="_blank" href="' 
										  + payment.charges[0].last_transaction.pdf + '?format=pdf">Pagar Boleto</a></p><p>ou copie o código de barra:<br><br><i>' 
										  + payment.charges[0].last_transaction.line + '</i><br></p><p>Data de Vencimento: ' 
										  + new TDate({ value : payment.charges[0].last_transaction.expires_at}).format('dd/MM/yyyy') + '</p>',
						});
					}
					else if(payment.charges && payment.charges.length > 0 && payment.charges[0].payment_method == 'pix')
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><img src="' 
							+ payment.charges[0].last_transaction.qr_code_url + '"/></p><br><p>ou copie o código:<br><br><i>' 
							+ payment.charges[0].last_transaction.qr_code + '</i><br></p><p>Data de Vencimento: ' 
							+ new TDate({ value : payment.charges[0].last_transaction.expires_at }).format('dd/MM/yyyy') + '</p>',
						})
					}
					else
					{
						resolve({
							status : 'Aguardando',
						});	
					}
				}				
				else if(payment.status == 'waiting_payment')
				{					
					if(payment.payment_method == 'pix')
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><img src="https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=' + payment.pix_qr_code + '"/></p><br><p>ou copie o código:<br><br><i>' + payment.pix_qr_code + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.pix_expiration_date}).format('dd/MM/yyyy') + '</p>',
						})	
					}
					else
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><a class="boleto-button" target="_blank" href="' + payment.boleto_url + '?format=pdf">Pagar Boleto</a></p><p>ou copie o código de barra:<br><br><i>' + payment.boleto_barcode + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.boleto_expiration_date}).format('dd/MM/yyyy') + '</p>',
						});	
					}
				}
				else if(payment.status == 'in_process') // MERCADO PAGO
				{					
					if(payment.payment_method == 'pix')
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><img src="https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=' + payment.pix_qr_code + '"/></p><br><p>ou copie o código:<br><br><i>' + payment.pix_qr_code + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.pix_expiration_date}).format('dd/MM/yyyy') + '</p>',
						})	
					}
					else
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><a class="boleto-button" target="_blank" href="' + payment.boleto_url + '?format=pdf">Pagar Boleto</a></p><p>ou copie o código de barra:<br><br><i>' + payment.boleto_barcode + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.boleto_expiration_date}).format('dd/MM/yyyy') + '</p>',
						});	
					}
				}
				else if(payment.status == 'pending' && payment.charges && payment.charges.length > 0) // PAGAR.ME 2
				{					
					if(payment.charges[0].payment_method == 'pix')
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><img src="' + payment.charges[0].last_transaction.qr_code_url + '"/></p><br><p>ou copie o código:<br><br><i>' + payment.charges[0].last_transaction.qr_code + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.pix_expiration_date }).format('dd/MM/yyyy') + '</p>',
						})	
					}
					else
					{
						resolve({
							status      : 'Aguardando',
							description : '<p><a class="boleto-button" target="_blank" href="' + payment.charges[0].last_transaction.pdf + '">Pagar Boleto</a></p><p>ou código de barra:<br><br><img src="' + payment.charges[0].last_transaction.barcode + '"/><br><i>' + payment.charges[0].last_transaction.line + '</i><br></p><p>Data de Vencimento: ' + new TDate({ value : payment.charges[0].last_transaction.due_at }).format('dd/MM/yyyy') + '</p>',
						});	
					}
				}				
				else
				{
					resolve({
						status : 'Aguardando',
					})
				}
			}			          
        });
    }
}
