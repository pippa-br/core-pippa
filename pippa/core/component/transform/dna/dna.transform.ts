/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class DNATransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let answers = [];
                let columns = formItem.field.option.columns;

                /* CALCULA RESULTADOS */
                for(let key in value)
                {
                    for(let key2 in columns)
                    {
                        if(!columns[key2].options[0].total)
                        {
                            columns[key2].options[0].total = 0;
                        }

                        if(!columns[key2].options[1].total)
                        {
                            columns[key2].options[1].total = 0;
                        }

                        if(columns[key2].options[0].value == value[key].value.value)
                        {
                            columns[key2].options[0].total++;
                        }

                        if(columns[key2].options[1].value == value[key].value.value)
                        {
                            columns[key2].options[1].total++;
                        }
                    }
                }

                for(let key in columns)
                {
                    if(columns[key].options[0].total > columns[key].options[1].total)
                    {
                        answers.push(columns[key].options[0]);
                    }
                    else
                    {
                        answers.push(columns[key].options[1]);
                    }
				}
				
                /* DOCUMENTS */
                if(formItem.field.option.documents)
                {
                    for(let key in formItem.field.option.documents)
                    {
                        for(let key2 in answers)
                        {
                            if(formItem.field.option.documents[key].name.value == answers[key2].value)
                            {
                                answers[key2].text = formItem.field.option.documents[key].text;
                            }
                        }
                    }
                }

                resolve(new Output({
                    label    : formItem.label,
                    formItem : formItem,
                    data     : data,
                    value    : {
                        answers : answers,
                    },
                }));
            });
        });
    }
}
