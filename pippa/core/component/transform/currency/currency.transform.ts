/* PIPPA */
import { BaseTransform }        from "../../transform/base.transform";
import { IModel }               from "../../../interface/model/i.model";
import { Output }               from "../../../model/output/output";
import { FormItem }             from "../../../model/form.item/form.item";

export class CurrencyTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let amount    = 0;
                let total     = 0;
                let count     = 0;
                let format    = "";
                let precision = 2;
				
                const hasSum = formItem.field.hasSum;

                if (formItem.setting && formItem.setting.precision)
                {
                    precision = formItem.setting.precision;
                }

                if (value && typeof value == "string" && value.indexOf("R$") > -1)
                {
                    // NUMERO QUE JA VEM COM FORMAT
                    format = value;
                }
                else if (value)
                {
                    if (typeof value == "string")
                    {
                        value = parseFloat(value);
                    }

                    if (hasSum)
                    {
                        amount = value;
                        total  = 1;
                        count  = 1;	
                    }		
					
                    format = "<span class=\"symbol\">R$ </span>" + value.toLocaleString("pt-BR", { minimumFractionDigits : precision });
                }
                else
                {
                    value = 0;

                    format = "<span class=\"symbol\">R$ </span>" + value.toLocaleString("pt-BR", { minimumFractionDigits : precision });
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    format   : format,
                    data     : data,
                    amount   : amount,
                    total    : total,
                    count    : count,
                }));
            });
        });
    }
}
