/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { IModel }        from '../../../interface/model/i.model';
import { Output }        from '../../../model/output/output';
import { FormItem }      from '../../../model/form.item/form.item';

export class HasDocumentTransform extends BaseTransform //* ERA SumFormTransform */
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            let output = new Output({
                data : data,
            });

            if(data && formItem.field.setting && formItem.field.where)
            {
                let params = this.createParams({
                    path    : data.accid + '/' + formItem.field.setting.appid + '/documents',
                    orderBy : 'postdate',
                    asc     : false,
                });

                const promises = [];

                for(const key in formItem.field.where)
                {
                    const where = formItem.field.where[key];
                    promises.push(data.getPathValue(where.path));
                }

                Promise.all(promises).then(values =>
                {
                    for(const key in formItem.field.where)
                    {
                        formItem.field.where[key].value = values[key];
                    }

                    this.api().getList(params).then((result:any) =>
                    {
                        resolve(new Output({
                            value : result.collection.length > 0
                        }));
                    });
                });
            }
            else
            {
                resolve(output);
            }
        });
    }
}
