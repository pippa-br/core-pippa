
/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { Output }               from '../../../model/output/output';

export class PageUrlTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {           
                //const apiUrl = "https://us-central1-facilities-use.cloudfunctions.net/cacheProxy?url=";

                //this.core().api.http.request(apiUrl + value).subscribe((response:any) =>
                //{                    
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        data     : data,
                        //value    : response,
                    }));
                //});
            });
        });
    }
}
