/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class HiddenTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
				let amount;

                if(typeof value == 'number')
				{
					amount = value;
				}

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    data     : data,
                    amount   : amount,
                    total    : 1,
                    count    : 1,
                }));
            });
        });
    }
}
