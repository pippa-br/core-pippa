/* PIPPA */
import { BaseTransform }  from '../../transform/base.transform';
import { Output }         from '../../../model/output/output';
import { Types } 		  from '../../../type/types';
import { environment }    from 'src/environments/environment';
import { DocumentPlugin } from '../../../plugin/document/document.plugin';

export class StockMovimentTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>(async (resolve) =>
        {
			const output = await StockMovimentTransform.getOutput(formItem, data);

			resolve(output);
        });
	}

	static getOutput(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const params = this.createParams({
				listEndPoint : Types.COLLECTION_DOCUMENT_API,
				accid        : data.accid,
				appid        : 'movementStock',
				colid        : 'documentsMovement',
				orderBy      : 'postdate',
				asc          : false,
				searchFirst  : true,
				noCache      : true,
				mapItems     : {
					referencePath : environment.defaultMapItems
				},
				map : true,
				where : [{
					field    : 'product',
					operator : '==',
					value    : data.product
				}]
			});

			const documentPlugin = this.core().injector.get(DocumentPlugin)
			
			/* REALIZA BUSCA */
			documentPlugin.getData(params).then((result:any) =>
			{
				resolve(new Output({
					formItem : formItem,
					data 	 : data,
					value    : result.collection,
				}));
			});
        });
	}
}
