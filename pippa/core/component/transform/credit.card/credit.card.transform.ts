/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { Output }        from '../../../model/output/output';

export class CreditCardTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((creditCard:any) =>
            {
                let formart : string = '';
				let value   : Array<any> = []
				
                if(creditCard && creditCard.cardnumber)
                {
					const r = /\b(?:\d{4}[ -]?){3}(?=\d{4}\b)/gm;
					const s = `**** **** **** `;
					formart = creditCard.cardnumber.replace(r, s);
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    data     : data,
                    value    : value,
                    format   : formart,
                }));
            });
        });
    }
}
