/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { IModel }        from '../../../interface/model/i.model';
import { VideoEmbed }    from '../../../util/video.embed/video.embed';
import { Output }        from '../../../model/output/output';
import { FormItem }      from '../../../model/form.item/form.item';
import { VideoPlugin }   from '../../../plugin/video/video.plugin';

export class VideoUrlTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                if(value && typeof value == 'object')
                {
                    value = new VideoEmbed(value);

                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        value    : value,
                        data     : data,
                    }));
                }
                else if(value && typeof value == 'string')
                {
                    const videoPlugin = this.core().injector.get(VideoPlugin);

                    //videoPlugin.getEmbed(value).then(embed =>
                    //{
                        //value = embed;

                        resolve(new Output({
                            formItem : formItem,
                            label    : formItem.label,
							value    : value,
							type     : videoPlugin.getType(value),
                            data     : data,
                        }));
                    //});
                }   
                else
                {
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        data     : data,
                    }));
                }
            });
        });
    }
}
