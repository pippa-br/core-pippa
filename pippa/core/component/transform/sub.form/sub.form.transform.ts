/* PIPPA */
import { BaseTransform } from "../../transform/base.transform";
import { IModel }        from "../../../interface/model/i.model";
import { Output }        from "../../../model/output/output";
import { Grid }          from "../../../model/grid/grid";
import { BaseList }      from "../../../model/base.list";
import { FormItem }      from "../../../model/form.item/form.item";

export class SubFormTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>(async (resolve) =>
        {
            if (formItem.setting && formItem.setting.isModal != undefined && formItem.setting.isModal)
            {
                resolve(new Output({
                    formItem : formItem,
                    data     : data,
                }));
            }
            else
            {
                const output = await SubFormTransform.getOutput(formItem, data);

                resolve(output);
            }
        });
    }
	
    static getOutput(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then(async (value:any) =>
            {
                let grid;

                if (formItem.grid && formItem.grid.items && formItem.grid.items.length > 0)
                {
                    for (const key in formItem.grid.items)
                    {   
                        formItem.grid.items[key].row = parseInt(key);
                    }

                    grid                 = new Grid(formItem.grid);
                    grid.fixedHeader     = false;
                    grid.hasViewerButton = true;
                    grid.showNoItems     = false;
                    grid.disabledButtons = false;
                }
                else
                {
                    grid = new Grid({
                        type            : Grid.TABLE_TYPE,
                        items           : formItem.items.getColumns(),
                        disabledButtons : false,
                        hasViewerButton : true,
                        fixedHeader     : false,
                        showNoItems    	: false,
                    });    
                }
                //await grid.on();

                const list : any = new BaseList(value);
				
                await list.on();				
				
                const values = [];
					
                /* ADICIONA A REFERENCIA DO PAI AOS FILHOS */
                for (const key in list)
                {
                    list[key].reference = data.reference
                    values.push(await SubFormTransform.sumItems(formItem.items, list[key]));
                }

                let total      = 0;
                let count      = 0;
                let amount     = 0;
                let percentage = 0;
				
                for (const key in values)
                {
                    total  += values[key].total;
                    count  += values[key].count;
                    amount += values[key].amount;
                }

                /* PERCENTAGE */
                if (count)
                {
                    percentage = Math.round((count * 100) / total);
                }

                resolve(new Output({
                    formItem   : formItem,
                    label      : formItem.label,
                    value      : list,
                    data       : data,
                    grid       : grid,
                    total      : total,
                    count      : count,
                    amount     : amount,
                    percentage : percentage,
                }));
            });
        });
    }
}
