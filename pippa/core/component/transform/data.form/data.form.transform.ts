/* PIPPA */
import { BaseTransform }    from '../../transform/base.transform';
import { TransformFactory } from '../../../factory/transform/transform.factory';
import { IModel }           from '../../../interface/model/i.model';
import { Output }           from '../../../model/output/output';
import { Viewer }           from '../../../model/viewer/viewer';
import { Document }         from '../../../model/document/document';
import { FormItem }         from '../../../model/form.item/form.item';

export class DataFormTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let type  = Viewer.FLAT_TYPE;
                let items = formItem.items;
                let total      : number = 0;
                let count      : number = 0;
                let percentage : number = 0;

                /* COUNT */
                const promises = [];
                const subData  = new Document(value);

                subData.on().then(() =>
                {
                    for(const key in items)
                    {
                        const promise = TransformFactory.transform(items[key], subData);
                        promises.push(promise);
                    }

                    Promise.all(promises).then(values2 =>
                    {
                        for(let key in values2)
                        {
                            total += values2[key].total;
                            count += values2[key].count;
                        }

                        if(count)
                        {
                            percentage = Math.round((count * 100) / total);
                        }

                        /* GRID */
                        if(formItem.grid)
                        {
                            type  = formItem.grid.type;
                            items = formItem.grid.items;
                        }

                        const viewer = new Viewer({
                            type  : type,
                            items : items,
                        });

                        const document          = new Document(value);
                        document['name']        = formItem.label;
                        document['attachments'] = data.getAttachments(formItem.field.name);

                        document.on().then(() =>
                        {
                            resolve(new Output({
                                formItem    : formItem,
                                label       : formItem.label,
                                value       : document,
                                data        : data,
                                viewer      : viewer,
                                count      : count,
                                percentage : percentage,
                                total      : total,
                            }));
                        });
                    });
                });
            });
        });
    }
}
