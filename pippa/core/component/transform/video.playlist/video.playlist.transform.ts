import { ModalController } from '@ionic/angular';

/* PIPPA */
import { BaseTransform }     from '../../transform/base.transform';
import { IModel }            from '../../../interface/model/i.model';
import { FieldType }         from '../../../type/field/field.type';
import { Output }            from '../../../model/output/output';
import { FormItem }          from '../../../model/form.item/form.item';
import { YoutubeCollection } from '../../../util/youtube/youtube.collection';
import { ChannelCollection } from '../../../util/youtube/channel.collection';
import { Grid }              from '../../../model/grid/grid';
import { EmbedModal }        from '../../../modal/embed/embed.modal';
import { YoutubePlugin }     from '../../../plugin/youtube/youtube.plugin';
import { VideoEmbed }        from '../../../util/video.embed/video.embed';
import { SlotType } 		 from '../../../type/slot/slot.type';

export class VideoPlaylistTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        const modalController = VideoPlaylistTransform.core().injector.get(ModalController);
        const youtubePlugin   = VideoPlaylistTransform.core().injector.get(YoutubePlugin);

        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:string) =>
            {
                /* PLAYLIST */
                if(formItem.setting && formItem.setting.channel)
                {
                    const collection   = new ChannelCollection();
                    collection.channel = value;
                    collection.load();

                    const grid = new Grid({
                        type : Grid.CARD_TYPE,
                        items : [{
							slot : SlotType.TIILE,
                            field : {
                                name : 'title',
                                type : FieldType.type('Title'),    
                            }
                        },
						{
							slot : SlotType.BEFORE_CONTENT,
                            field : {
                                name    : 'thumbnailUrl',
                                type    : FieldType.type('Image'),                                
                            },
                            onClick : (event:any) =>
                            {
                                modalController.create(
                                {
                                    component      : EmbedModal,
                                    componentProps :
                                    {
                                        title : event.data.title,
                                        embed : event.data.getEmbed(),
                                        onClose : (() =>
                                        {
                                            modalController.dismiss();
                                        }),
                                    },
                                    cssClass : 'full',
                                })
                                .then((modal:any) =>
                                {
                                    modal.present();
                                });
                            }
                        }],
                        hasSetButton : false,
                        hasDelButton : false,
                    });

                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        value    : collection,
                        grid     : grid,
                        data     : data,
                    }));
                }
                else if(youtubePlugin.getPlaylistId(value))
                {
                    const collection    = new YoutubeCollection();
                    collection.playlist = youtubePlugin.getPlaylistId(value);
					collection.load();
					
                    const grid = new Grid({
                        type : Grid.CARD_TYPE,
                        items : [
						{
							slot : SlotType.TIILE,
                            field : {
                                name : 'title',
                                type : FieldType.type('Title'),    
                            }
						},
						{
							slot : SlotType.SUBTITLE,
                            field : {
                                name : 'postdate',
                                type : FieldType.type('Date'),    
                            }
                        },
						{
							slot : SlotType.BEFORE_CONTENT,
                            field : {
                                name    : 'thumbnailUrl',
                                type    : FieldType.type('Image'),                                
                            },
                            onClick : (event:any) =>
                            {
                                modalController.create(
                                {
                                    component      : EmbedModal,
                                    componentProps :
                                    {
                                        title : event.data.title,
                                        embed : event.data.getEmbed(),
                                        onClose : (() =>
                                        {
                                            modalController.dismiss();
                                        }),
                                    },
                                    cssClass : 'full',
                                })
                                .then((modal:any) =>
                                {
                                    modal.present();
                                });
                            }
                        }],
                        hasSetButton : false,
                        hasDelButton : false,
                    });

                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        value    : collection,
                        grid     : grid,
                        data     : data,
                    }));
                }
                else if(youtubePlugin.getYoutubeID(value))/* ONLY VIDEO */
                {
                    const videoEmbed = new VideoEmbed({
                        id   : youtubePlugin.getYoutubeID(value),
                        url  : value,
                        type : VideoEmbed.YOUTUBE,
                    })

                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        value    : videoEmbed,
                        data     : data,
                    }));
                }
            });
        });
    }
}
