/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { IModel }        from '../../../interface/model/i.model';
import { Output }        from '../../../model/output/output';
import { FormItem }      from '../../../model/form.item/form.item';
import { environment } from 'src/environments/environment';
import { Types } from '../../../type/types';
import { DocumentPlugin } from '../../../plugin/document/document.plugin';

export class LastDocumentTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const output = new Output({
				data 	 : data,
				formItem : formItem,
            });

            if(data && formItem.setting)
            {
                const params = this.createParams({
					listEndPoint : Types.COLLECTION_DOCUMENT_API,
                    accid        : data.accid,
					appid        : formItem.setting.appid,
					colid        : 'documents',
                    orderBy      : 'postdate',
					asc          : false,
					searchFirst  : true,
					mapItems     : {
						referencePath : environment.defaultMapItems
					},
					map : true,
                });

				params.where = formItem.filters;

				const documentPlugin = this.core().injector.get(DocumentPlugin)
				
				/* REALIZA BUSCA */
				documentPlugin.getData(params).then((result:any) =>
				{
					if(result.collection.length > 0)
					{
						const data2 = result.collection[0];

						data2.getProperty(formItem.path).then((value:any) =>
						{
							resolve(new Output({
								formItem : formItem,
								data 	 : data,
								value    : value,
								format   : value
							}));
						})
					}
					else
					{
						resolve(output);
					}
				});
            }
            else
            {
                resolve(output);
            }
        });
    }
}
