import validator from 'validator';

/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }      from '../../../model/form.item/form.item';

export class MultiCheckboxTransform extends BaseTransform
{
    constructor()
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let total = 1;
                let count = 0;
				let items = [];							

                if(value)
                {
                    for(let key in value)
                    {
                        if(value[key] && value[key].value)
                        {
                            if(typeof value[key].value == 'string' && validator.isNumeric(value[key].value))
                            {
                                count += parseFloat(value[key].value);
							}
							
							items.push(value[key]);
                        }
                    }
				}
				
                resolve(new Output({
                    label    : formItem.label,
                    value    : items,
                    formItem : formItem,
                    data     : data,
                    total    : total,
                    count    : count,
                }));
            });
        });
    }
}
