/* PIPPA */
import { BaseTransform }    from "../../transform/base.transform";
import { Output }           from "../../../model/output/output";
export class SumFormTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if (data && data.form)
            {
                data.form.on(true).then(async () =>
                {
                    const form = data.form;
					
                    const sum = await SumFormTransform.sumItems(form.items, data);

                    resolve(new Output({
                        label      : sum.labels,
                        value      : sum.values,
                        formItem   : formItem,
                        data       : data,
                        count      : sum.count,
                        percentage : sum.percentage,
                        total      : sum.total,
                        amount     : sum.amount,
                    }));
                });
            }
            else
            {
                const total      = 0;
                const count      = 0;
                const percentage = 0;
                const amount     = 0;
	
                resolve(new Output({
                    formItem   : formItem,
                    label      : formItem.label,
                    data       : data,
                    count      : count,
                    percentage : percentage,
                    total      : total,
                    amount     : amount,
                }));
            }
        });
    }
}
