/* PIPPA */
import { BaseTransform }        from '../../../../core/component/transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class TextTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
				let total  = 0;
				let count  = 0;
				
				const hasSum  = formItem.field.hasSum;
				const convert = formItem.setting && formItem.setting.convert;
				
                if(value != undefined && hasSum)
                {
					count = 1;
					total = 1;
				}

				if(value === undefined)
				{
					value = ""
				}

				if(convert)
				{
					if(value instanceof Array)
					{
						value = "[]";
					}
					else if(value === null)
					{
						value = "null";
					}
				}

				resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    format   : value,
                    data     : data,
                    total    : total,
                    count    : count,
                }));
            });
        });
    }
}
