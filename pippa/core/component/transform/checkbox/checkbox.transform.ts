/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class CheckboxTransform extends BaseTransform
{
    constructor()
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : (value ? 'Sim ': 'Não'),
                    data     : data,
                    count    : 1,
                    total    : 1,
                }));
            });
        });
    }
}
