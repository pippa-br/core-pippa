/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { Output }               from '../../../model/output/output';
import { IModel }               from '../../../interface/model/i.model';
import { FormItem }             from '../../../model/form.item/form.item';
import { Types } from '../../../type/types';
import { environment } from 'src/environments/environment';

export class AgreeTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>(async (resolve) =>
        {
			let value = formItem.description;

			if(formItem.setting && formItem.setting.documentPath)
			{
				const paths = formItem.setting.documentPath.split('/');

				const params = this.createParams({
					getEndPoint : Types.GET_DOCUMENT_API,
					accid : paths[0],
					appid : paths[1],
					colid : paths[2],
					path  : formItem.setting.documentPath,
					mapItems    : {
						referencePath : environment.defaultMapItems
					}
				})

				const result = await this.core().api.getObject(params);
				
				if(result.data)
				{
					value = result.data.content;
				}
			}
			
            resolve(new Output({
				formItem : formItem,
				label    : formItem.label,
				data     : data,
				value    : value
			}));
        });
    }
}
