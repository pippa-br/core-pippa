/* PIPPA */
import { BaseTransform } from '../../transform/base.transform';
import { Output }        from '../../../model/output/output';
import { TDate } from '../../../util/tdate/tdate';

export class DateTransform extends BaseTransform 
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let isDateValid = false;
                let format      = '';
				let date        = null;

                if(value)
                {
					date = new TDate({ value : value });

					if(date)
					{
						isDateValid = date.isValid();
					}					
				}                

                if(isDateValid)
                {
                    let mask : string = '';

                    if(formItem.setting && formItem.setting.onlyDay)
                    {
                        mask = '<1>dd</1>';
                    }
                    else
                    {
                        mask = '<1>dd<0>/</0></1><2>MM<0>/</0></2><3>yyyy</3>';

                        if(formItem.setting && formItem.setting.hasTime)
                        {
                            mask = mask + ' <4>HH<0>:</0></4><5>mm</5>';
                        }    
                    }

                    format = date.format(mask, this.core().timezone.labe);

                    format = format.replace('<1>','<span class="day">');
                    format = format.replace('</1>','</span>');
                    format = format.replace('<2>','<span class="month">');
                    format = format.replace('</2>','</span>');
                    format = format.replace('<3>','<span class="year">');
                    format = format.replace('</3>','</span>');
                    format = format.replace('<4>','<span class="hour">');
                    format = format.replace('</4>','</span>');
                    format = format.replace('<5>','<span class="minute">');
                    format = format.replace('</5>','</span>');
                    format = format.replace(new RegExp('<0>', 'g'), '<span class="separator">');
					format = format.replace(new RegExp('</0>', 'g'),'</span>');
					
					if(formItem.setting && formItem.setting.hasTimeAgo)
					{
						format += ' <small>' + date.fromNow() + '</small>';	
					}
                }

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    data     : data,
                    value    : (isDateValid ? date : null),
                    format   : format,
                }));
            });
        });
    }
}
