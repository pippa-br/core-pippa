/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class Table3x3Transform extends BaseTransform
{
    constructor()
    {
        super();
    }

    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                const rows       = formItem.field.option.rows;
                const categories = formItem.field.option.categories;
                const answers    = [];
                let total        = 0;
                let count        = 0;
                let percentage   = 0;

                for(let key in value)
                {
                    let row = rows[key];

                    for(let key2 in categories)
                    {
                        let category = categories[key2];

                        if(category.id == row.option.id)
                        {
                            if(!answers[key2])
                            {
                                answers[key2]        = {};
                                answers[key2].label  = category.label;
                                answers[key2].value  = parseInt(category.value);
                                answers[key2].order  = parseInt(key2);
                                answers[key2].text   = category.text;
                                answers[key2].amount = 0;
                            }

							const _value = (value[key].value != undefined ? value[key].value : value[key]);
							
                            answers[key2].amount += parseInt(_value);

                            count++;
                            break;
                        }
                    }

                    total++;
				}				

                if(total > 0)
                {
                    percentage = Math.round((count * 100) / total);
                }

                resolve(new Output({
                    label      : formItem.label,
                    formItem   : formItem,
                    total      : total,
                    percentage : percentage,
                    data       : data,
                    count      : count,
                    value : {
                        answers : answers
                    },
                }));
            });
        });
    }
}
