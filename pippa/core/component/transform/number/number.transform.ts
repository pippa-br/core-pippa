/* PIPPA */
import { BaseTransform }        from '../../../../core/component/transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class NumberTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
				let total  = 0;
				let count  = 0;
				let amount = 0;
				
				const hasSum = formItem.field.hasSum;
				
                if(value != undefined)
				{
					if(typeof value == 'string')
					{
						value = parseFloat(value);

						if(isNaN(value))
						{
							value = 0;
						}
					}

					if(hasSum)
					{
						amount = value;
						total  = 1;
						count  = 1;	
					}					
				}
				else
				{
					value = 0;
				}
				
				resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    format   : value,
                    data     : data,
                    total    : total,
                    count    : count,
                }));
            });
        });
    }
}
