/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class UrlTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {				
                /*if(value)
                {
                    if(value.indexOf('?') == -1)
                    {
                        value += '?t=' + new Date().getTime();
                    }
                    else
                    {
                        value += '&t=' + new Date().getTime();
                    }
                }  */              

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    data     : data,
                    value    : value,
                }));
            });
        });
    }
}
