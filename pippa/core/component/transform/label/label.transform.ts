/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class LabelTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            resolve(new Output({
                formItem : formItem,
                label    : formItem.label,
                data     : data,
                value    : formItem.label,
            }));
        });
    }
}
