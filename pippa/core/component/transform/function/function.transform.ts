/* PIPPA */
import { BaseTransform }        from '../../transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FunctionsType }        from '../../../type/functions/functions.type';
import { FormItem }             from '../../../model/form.item/form.item';

export class FunctionTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            FunctionsType.get().getFunction(formItem.path)(data).then((value:any) =>
            {
                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    data     : data,
                    count    : 1,
                    total    : 1,
                }));
            });
        });
    }
}
