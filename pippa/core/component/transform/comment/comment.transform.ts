/* PIPPA */
import { BaseTransform }  from '../../transform/base.transform';
import { Output }         from '../../../model/output/output';
import { MenuPlugin }     from '../../../plugin/menu/menu.plugin';
import { MenuCollection } from '../../../model/menu/menu.collection';
import { FormPlugin } 	  from '../../../plugin/form/form.plugin';

export class CommentTransform extends BaseTransform //* ERA SumFormTransform */
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
			data.getPathValue(formItem.path).then(async (value:any) =>
            {
				const output : any = new Output({
					data     : data,
					formItem : formItem,
					value    : value,
					appid    : formItem.setting.appid,
				});

				/* QUANDO VIZUALIZEO O COMENTARIO */
				if(formItem.path == '_children')
				{
					/* GET COLLECTION  */
					data._children    = value
					output.collection = new MenuCollection([data]);

					/* GET FORM */
					const params = this.createParams({
						appid : formItem.setting.appid,
					});

					const formPlugin = this.core().injector.get(FormPlugin);
	
					const form = await formPlugin.getOne(params);

					form.hasHeader = false;
					form.hasCancelButton = false;
					form.setting = {
						setText : 'Comentar'
					}
					output.form = form;	
					output.code = data.code;

					resolve(output);
				}
				else if(data && formItem.setting) /* QUANDO VIZUALIZEO O OBJETO A SER COMENTARIO */
				{
					let params = this.createParams({
						appid   : formItem.setting.appid,
						orderBy : 'postdate',
						asc     : false,
					});

					params.where = [{
						field : 'code',
						value : value,
					},
					{
						field    : '_level',
                    	operator : '==',
						value    : 1
					}];					

					/* GET COLLECTION  */
					const menuPlugin  = this.core().injector.get(MenuPlugin);
					let result 		  = await menuPlugin.getData(params);
					output.collection = result.collection;
					
					/* GET FORM */
					params = this.createParams({
						appid : formItem.setting.appid,
					});

					const formPlugin = this.core().injector.get(FormPlugin);
	
					result = await formPlugin.getData(params);

					for(let key in result.collection)
					{
						const form = result.collection[key];

						if(form.id != 'master')
						{						
							form.hasHeader = false;
							form.hasCancelButton = false;
							form.setting = {
								setText : 'Comentar'
							}
							output.form   = form;
							output.hasAdd = true;
							output.code   = value;

							break;		
						}
					}					
	
					resolve(output);
				}
				else
				{
					resolve(output);
				}
			});            
        });
    }
}
