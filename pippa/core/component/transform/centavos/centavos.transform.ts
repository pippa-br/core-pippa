/* PIPPA */
import { BaseTransform }        from '../../../../core/component/transform/base.transform';
import { IModel }               from '../../../interface/model/i.model';
import { Output }               from '../../../model/output/output';
import { FormItem }             from '../../../model/form.item/form.item';

export class CentavosTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                let amount = 0;
				let total  = 0;
				let count  = 0;
				
				const hasSum = formItem.field.hasSum;

				if(value != undefined && value != "")
				{
					if(typeof value == 'string')
					{
						value = parseFloat(value);
					}

					if(hasSum)
					{
						amount = value;
						total  = 1;
						count  = 1;	
					}					
				}
				else
				{
					value = 0;
				}

				value = value / 100;

                resolve(new Output({
                    formItem : formItem,
                    label    : formItem.label,
                    value    : value,
                    format   : '<span class="symbol">R$ </span>' + value.toLocaleString('pt-BR', { minimumFractionDigits : 2}),
                    data     : data,
                    amount   : amount,
                    total    : total,
                    count    : count,
                }));
            });
        });
    }
}
