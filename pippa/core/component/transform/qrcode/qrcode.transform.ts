/* PIPPA */
import { BaseTransform } from '../base.transform';
import { Output }        from '../../../model/output/output';

export class QRCodeTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {   
                if(value)
                {
                    resolve(new Output({
						formItem : formItem,
						label    : formItem.label,
						data     : data,
						value    : value,
						format   : 'https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=' + value,
					}));
                } 
                else
                {
                    resolve(new Output({
                        formItem : formItem,
                        label    : formItem.label,
                        data     : data,
                        value    : null,
                        format   : null,
                    }));  
                }                                        
            });
        });
    }
}