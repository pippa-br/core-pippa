/* PIPPA */
import { TransformFactory } from '../../../factory/transform/transform.factory';
import { BaseTransform }    from '../../transform/base.transform';
import { IModel }           from '../../../interface/model/i.model';
import { Output }           from '../../../model/output/output';
import { FormItem }         from '../../../model/form.item/form.item';
import { Reference }        from '../../../model/reference/reference';

export class SumFieldsetTransform extends BaseTransform
{
    static transform(formItem:any, data:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            data.getPathValue(formItem.path).then((value:any) =>
            {
                const model = new Reference(value);

                model.on().then(() =>
                {
                    let total      : number = 0;
                    let count      : number = 0;
                    let percentage : number = 0;

                    if(data && data.form)
                    {
                        data.form.on(true).then(() =>
                        {
                            const form = data.form;

                            /* GERAR OUTPUTS */
                            const promises1 : any =  [];

                            for(const key in form.items)
                            {
                                const _item = form.items[key];
                                promises1.push(_item);
                            }

                            Promise.all(promises1).then(values1 =>
                            {
                                const promises2 = [];

                                for(const key in values1)
                                {
                                    const _item2 : any = values1[key];

                                    /* SO ESTA PEGAN DO O 1 NIVEL */
                                    if(formItem.path == _item2.value)
                                    {
                                        for(const key2 in _item2.items)
                                        {
                                            const promise = TransformFactory.transform(_item2.items[key2], model);
                                            promises2.push(promise);
                                        }
                                    }
                                }

                                Promise.all(promises2).then(values2 =>
                                {
                                    let labels : Array<string> = [];
                                    let values : Array<any> = [];
                                    let total  : number = 0;
                                    let count  : number = 0;

                                    for(let key in values2)
                                    {
                                        total += values2[key].total;
                                        count += values2[key].count;
                                        labels.push(values2[key].label);
                                        values.push(values2[key].value);
                                    }

                                    if(count)
                                    {
                                        percentage = Math.round((count * 100) / total);
                                    }

                                    resolve(new Output({
                                        label      : labels,
                                        value      : values,
                                        formItem   : formItem,
                                        data       : data,
                                        count      : count,
                                        percentage : percentage,
                                        total      : total,
                                    }));
                                });
                            });
                        });
                    }
                    else
                    {
                        resolve(new Output({
                            formItem   : formItem,
                            label      : formItem.label,
                            data       : data,
                            count      : count,
                            percentage : percentage,
                            total      : total,
                        }));
                    }
                });
            });
        });
    }
}
