import { Component, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';

@Component({
    selector 		: `ion-row.row-partial.row`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<ng-template #template></ng-template>`
})
export class RowPartial extends BaseContainerPartial
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }
}