import { Component, ViewContainerRef, ViewChild, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';
import { PartialFactory }       from '../../../factory/partial/partial.factory';
import { PartialType }         from '../../../type/partial/partial.type';
import { DomController } from '@ionic/angular';

@Component({
    selector        : `.slides-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<swiper-container [options]="options">
							<ng-template #templateContent></ng-template>
					   </swiper-container>`
})
export class SlidesPartial extends BaseContainerPartial
{
    @ViewChild('templateContent', { read : ViewContainerRef, static : true }) templateContent : ViewContainerRef;
    @ViewChild('slides') slides : ElementRef | undefined;

    public options : object = {
	};

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
		super(elementRef, renderer, changeDetectorRef, domController);  		
    }

    async createSlidePartial()
    {		
        const component = await PartialFactory.createComponent(PartialType.type('Slide'), this.templateContent);
		this.components.push(component);   
				
		//swiper.slideReset(0);

		//swiper.updateOnWindowResize();
		//swiper.initialSlide();
				
        return component;
	}   
	
	async slideTo(index)
	{		
		this.slides?.nativeElement.swiper.slideTo(index);
		this.markForCheck();

		const swiper = await this.slides?.nativeElement.swiper.getSwiper();		
		swiper.update();

		console.log(this.renderer, this.elementRef);
	}	

    async clear()
    {
		this.templateContent.clear();

		//const swiper = await this.slides.getSwiper();		
		//swiper.destroy();
		//swiper.slideReset(0);
		//swiper.removeAllSlides();
		
        super.clear();
    }
}
