import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial } from '../base.partial';

@Component({
    selector        : `.button-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-button clear (click)="onClick($event)" hideDelay="0" showDelay="0" matTooltip="{{tooltip}}">
                            <ion-icon *ngIf="icon && icon.value" name="{{icon.value}}" class="icon icon-md item-icon"></ion-icon>
							<img *ngIf="icon && icon._url" src="{{icon._url}}"/>
							<span *ngIf="!icon">{{label | translate}}</span>
                      </ion-button>`
})
export class ButtonPartial extends BasePartial
{
    @Input() icon    : any;
	@Input() tooltip : string;
	@Input() label   : string;

    @Output('click') click : EventEmitter<any> = new EventEmitter();

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    { 
        super(elementRef, renderer, changeDetectorRef, domController);
    }

    onClick(event:any)
    {
        event.data = this.data;

        this.click.emit(event);
    };
}
