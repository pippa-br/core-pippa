import { Component, ViewContainerRef, ViewChild, HostListener, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';
import { TransformFactory }     from '../../../factory/transform/transform.factory';
import { OutputFactory }        from '../../../factory/output/output.factory';
import { PartialFactory }       from '../../../factory/partial/partial.factory';

@Component({
    selector        : `ion-segment-button.segment-button-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ng-template #template></ng-template>`
})
export class SegmentButtonPartial extends BaseContainerPartial
{
	public onClick : any
	public index   : number = 0;

    @ViewChild('template', { read: ViewContainerRef, static:true }) template : ViewContainerRef;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);        
    }

    async addOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.template, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

        this.components.push(outputComponent);

        return outputComponent;
	}
	
	@HostListener('click', ['$event'])
    _onClick(e?:any)
    {
		this.onClick(this.index);
    };
}
