import { Component, ElementRef, Renderer2, ChangeDetectorRef, ChangeDetectionStrategy }    from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';
import { DomController } from '@ionic/angular';
//import { IframeModal }                        from '../../../modal/iframe/iframe.modal';

/* PIPPA */
import { BasePartial }     from '../base.partial';
import { AttachmentType }  from '../../../type/attachment/attachment.type';
import { FormFormPopover } from '../../../popover/form.form/form.form.popover';
import { Form }            from '../../../model/form/form';
import { Document }        from '../../../model/document/document';
import { FileCollection }  from '../../../model/file/file.collection';
import { FieldType }       from '../../../type/field/field.type';
import { StepItem }        from '../../../model/step.item/step.item';

@Component({
    selector        : `.attachments-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div class="wrapper-attachments" [ngClass]="{'has-attachments' : count > 0}">
                    <p class="comment" *ngIf="comment">
                        <span class="wrapper-media" (click)="openComment(comment)">
                            <small>{{comment}} <ion-icon name="chatboxes-outline"></ion-icon></small>
                        </span>
                        <ion-icon *ngIf="isEdit" class="remove-button-outline"
                                  name="remove-circle-outline"
                                  (click)="removeAttachments(null, comment, 'comment')">
                        </ion-icon>
                    </p>
                    <p class="bonus" *ngIf="bonus">
                        <span class="wrapper-media" (click)="openBonus(bonus)">
                            <small>{{bonus}}%</small>
                        </span>
                        <ion-icon *ngIf="isEdit" class="remove-button-outline"
                                  name="remove-circle-outline"
                                  (click)="removeAttachments(null, bonus, 'bonus')">
                        </ion-icon>
                    </p>
                    <p class="videos" *ngIf="videos && videos.length > 0">
                        <span *ngFor="let item of videos; trackBy : trackById; let i = index;">
                            <div class="wrapper-media" (click)="openUrl(item.name, item._url)">
                                <ion-icon name="play-outline" class="play-button"></ion-icon>
                                <video>
                                    <source src="{{item._url}}" type="video/mp4">
                                </video>
                            </div>
                            <div class="wrapper-comment" *ngIf="item.comment || isEdit">
                                <small>{{item.comment}}</small>
                                <ion-icon name="chatbubbles-outline" *ngIf="_inputComponent" (click)="openMediaComment(item, 'video')"></ion-icon>
                            </div>
                            <ion-icon *ngIf="isEdit" class="remove-button"
                                      name="remove-circle-outline"
                                      (click)="removeAttachments(videos, item, 'video')" >
                            </ion-icon>
                        </span>
                    </p>
                    <p class="images" *ngIf="images && images.length > 0">
                        <span *ngFor="let item of images; trackBy : trackById; let i = index;" >
                            <div class="wrapper-media">
                                <img src="{{item._url}}" (click)="openUrl(item.name, item._url)" />
                            </div>
                            <div class="wrapper-comment" *ngIf="item.comment || isEdit">
                                <small>{{item.comment}}</small>
                                <ion-icon name="chatbubbles-outline" *ngIf="_inputComponent" (click)="openMediaComment(item, 'image')"></ion-icon>
                            </div>
                            <ion-icon *ngIf="isEdit" class="remove-button"
                                      name="remove-circle-outline"
                                      (click)="removeAttachments(images, item, 'image')" >
                            </ion-icon>
                        </span>
                    </p>
                    <p class="gallery" *ngIf="gallery && gallery.length > 0">
                        <span *ngFor="let item of gallery; trackBy : trackById; let i = index;" >
                            <div class="wrapper-media">
                                <img src="{{item._url}}" (click)="openUrl(item.name, item._url)" />
                            </div>
                            <div class="wrapper-comment" *ngIf="item.comment || isEdit">
                                <small>{{item.comment}}</small>
                                <ion-icon name="chatbubbles" *ngIf="_inputComponent" (click)="openMediaComment(item, 'gallery')"></ion-icon>
                            </div>
                            <ion-icon *ngIf="isEdit" class="remove-button"
                                      name="remove-circle-outline-outline"
                                      (click)="removeAttachments(gallery, item, 'gallery')" >
                            </ion-icon>
                        </span>
                    </p>
                    <p class="audios" *ngIf="audios && audios.length > 0">
                        <span *ngFor="let item of audios; trackBy : trackById; let i = index;">
                            <div class="wrapper-media">
                                <audio controls>
                                    <source src="{{item._url}}" type="audio/mp3">
                                </audio>
                            </div>
                            <div class="wrapper-comment" *ngIf="item.comment || isEdit">
                                <small>{{item.comment}}</small>
                                <ion-icon name="chatbubbles" *ngIf="_inputComponent" (click)="openMediaComment(item, 'audio')"></ion-icon>
                            </div>
                            <ion-icon *ngIf="isEdit" class="remove-button"
                                      name="remove-circle-outline"
                                      (click)="removeAttachments(audios, item, 'audio')" >
                            </ion-icon>
                        </span>
                    </p>
                    <p class="documents" *ngIf="documents && documents.length > 0">
                        <span *ngFor="let item of documents; trackBy : trackById; let i = index;">
                            <div class="wrapper-media">
                                <span (click)="openUrl(item.name, item._url)">
                                    <ion-icon name="document-outline"></ion-icon> {{item.name}}
                                </span>
                            </div>
                            <div class="wrapper-comment" *ngIf="item.comment || isEdit">
                                <small>{{item.comment}}</small>
                                <ion-icon name="chatbubbles-outline" *ngIf="_inputComponent" (click)="openMediaComment(item, 'document')"></ion-icon>
                            </div>
                            <ion-icon *ngIf="isEdit" class="remove-button"
                                      name="remove-circle-outline"
                                      (click)="removeAttachments(documents, item, 'document')" >
                            </ion-icon>
                        </span>
                    </p>
                </div>`
})
export class AttachmentsPartial extends BasePartial
{
    public _attachments      : any;
    public videos            : FileCollection;
    public comment           : string;
    public bonus             : string;
    public images            : FileCollection;
    public gallery           : FileCollection;
    public audios            : FileCollection;
    public documents         : FileCollection;
    public popoverController : PopoverController;
    public alertController   : AlertController;
    public commentForm       : any;
    public bonusForm         : any;
    public count             : number = 0;
    public _inputComponent   : any;
    public isEdit            : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);

        this.popoverController = this.core().injector.get(PopoverController);
        this.alertController   = this.core().injector.get(AlertController);

        /* FORM */
        const step = new StepItem({
            order : 0,
        });

        this.commentForm = new Form({
            name  : 'Editar Comentário',
            items : [
                {
                    id    : 'comment',
                    row   : 1,
                    col   : 1,
                    step  : step,
                    field : {
                        id          : 'comment',
                        type        : FieldType.type('Textarea'),
                        name        : 'comment',
                        label       : 'Comentário',
                        placeholder : 'Comentário',
                    }
                },
            ]
        });

        this.bonusForm = new Form({
            name  : 'Editar Bônus',
            items : [
                {
                    id    : 'bonus',
                    row   : 1,
                    col   : 1,
                    step  : step,
                    field : {
                        id          : 'bonus',
                        type        : FieldType.type('Percentage'),
                        name        : 'bonus',
                        label       : 'Bônus',
                        placeholder : 'Bônus',
                    }
                },
            ]
        });

        this.videos    = new FileCollection();
        this.images    = new FileCollection();
        this.documents = new FileCollection();
        this.audios    = new FileCollection();
    }

    get data()
    {
        return this._data;
    }

    set data(value:any)
    {
        this._data = value;

        /* ATTACHMENTS */
        if(value)
        {
            this.attachments = value.getAttachments(this.formItem.field.name);
        }
    }

    get inputComponent()
    {
        return this._inputComponent;
    }

    set inputComponent(value:any)
    {
        this._inputComponent = value;

        /* UPDATE ATTACHMENTS */
        if(value)
        {
            value.instance.onAttachment = (attachments:any) =>
            {
                console.log('attachments', attachments);
                this.attachments = attachments;
            }

            this.isEdit = true;
        }
    }

    set attachments(data:Array<any>)
    {
        this._attachments = data;
        this.count        = 0;

        if(this._attachments)
        {
            for(const key in this._attachments)
            {
                if(key == AttachmentType.COMMENT.value)
                {
                    this.comment = this._attachments[key];
                }
                else if(key == AttachmentType.BONUS.value)
                {
                    this.bonus = this._attachments[key];
                }
                else if(key == AttachmentType.VIDEO.value)
                {
                    this.videos = new FileCollection(this._attachments[key]);
                }
                else if(key == AttachmentType.IMAGE.value)
                {
                    this.images = new FileCollection(this._attachments[key]);
                }
                else if(key == AttachmentType.DOCUMENT.value)
                {
                    this.documents = new FileCollection(this._attachments[key]);
                }
                else if(key == AttachmentType.AUDIO.value)
                {
                    this.audios = new FileCollection(this._attachments[key]);
                }
                else if(key == AttachmentType.GALLERY.value)
                {
                    this.gallery = new FileCollection(this._attachments[key]);
                }

                this.count++;
            }

            this.markForCheck();
        }
    }

    get attachments()
    {
        return this._attachments;
    }

    removeAttachments(items:any, item:any, type:any)
    {
        /* POR NO HTML NAO ACEITA TYPES */
        if(type == 'video')
        {
            type = AttachmentType.VIDEO;
        }
        else if(type == 'image')
        {
            type = AttachmentType.IMAGE;
        }
        else if(type == 'comment')
        {
            type = AttachmentType.COMMENT;
        }
        else if(type == 'document')
        {
            type = AttachmentType.DOCUMENT;
        }
        else if(type == 'bonus')
        {
            type = AttachmentType.BONUS;
        }
        else if(type == 'gallery')
        {
            type = AttachmentType.GALLERY;
        }

        console.log('remove', type);

        this.alertController.create({
            header  : 'Alerta',
            message : 'Deseja remover esse anexo?',
            buttons : [
                {
                    text: 'Não',
                    handler : () =>
                    {
                        this.alertController.dismiss();
                        return false;
                    }
                },
                {
                    text: 'Sim',
                    handler : () =>
                    {
                        if(items)
                        {
                            items.del(item);

                            if(items.length == 0)
                            {
                                this.inputComponent.instance.setAttachmentByType(null, type);
                            }
                            else
                            {
                                this.inputComponent.instance.setAttachmentByType(items, type);
                            }
                        }
                        else
                        {
                            this.inputComponent.instance.setAttachmentByType(null, type);
                            this[type.value] = null;
                        }

                        this.count--;
                        this.alertController.dismiss();
                        this.markForCheck();

                        return false;
                    }
                }
            ]
        })
        .then((alert:any) =>
        {
            alert.present();
        })
    }

    openComment(comment:any)
    {
        if(this.inputComponent)
        {
            this.popoverController.create(
            {
                component      : FormFormPopover,
                componentProps :
                {
                    data  : new Document({comment:comment}),
                    form  : this.commentForm,
                    onAdd : ((event:any) =>
                    {
                        this.comment = event.data.comment;
                        this._attachments[AttachmentType.COMMENT.value] = event.data.comment;
                        this.inputComponent.instance.setAttachmentByType(event.data.comment, AttachmentType.COMMENT);
                        this.popoverController.dismiss();
                    }),
                    onSet : ((event:any) =>
                    {
                        this.comment = event.data.comment;
                        this._attachments[AttachmentType.COMMENT.value] = event.data.comment;
                        this.inputComponent.instance.setAttachmentByType(event.data.comment, AttachmentType.COMMENT);
                        this.popoverController.dismiss();
                    }),
                    onClose : (() =>
                    {
                        this.popoverController.dismiss();
                    }),
                },
                showBackdrop : false,
                cssClass : 'right-popover',
            })
            .then((popover:any) =>
            {
                popover.present();
            });
        }
    }

    openMediaComment(media:any, type:any)
    {
        /* POR NO HTML NAO ACEITA TYPES */
        if(type == 'video')
        {
            type = AttachmentType.VIDEO;
        }
        else if(type == 'image')
        {
            type = AttachmentType.IMAGE;
        }
        else if(type == 'document')
        {
            type = AttachmentType.DOCUMENT;
        }
        else if(type == 'bonus')
        {
            type = AttachmentType.BONUS;
        }
        else if(type == 'gallery')
        {
            type = AttachmentType.GALLERY;
        }

        if(this.inputComponent)
        {
            this.popoverController.create(
            {
                component      : FormFormPopover,
                componentProps :
                {
                    data  : new Document({comment:media.comment}),
                    form  : this.commentForm,
                    onAdd : ((event:any) =>
                    {
                        media.comment = event.data.comment;
                        this.inputComponent.instance.setAttachmentByType(this.images.parseData(), type);
                        this.markForCheck();

                        this.popoverController.dismiss();
                    }),
                    onSet : ((event:any) =>
                    {
                        media.comment = event.data.comment;
                        this.inputComponent.instance.setAttachmentByType(this.images.parseData(), type);
                        this.markForCheck();

                        this.popoverController.dismiss();
                    }),
                    onClose : (() =>
                    {
                        this.popoverController.dismiss();
                    }),
                },
                showBackdrop : false,
                cssClass : 'right-popover',
            })
            .then((popover:any) =>
            {
                popover.present();
            });
        }
    }

    openBonus(bonus:string)
    {
        if(this.inputComponent)
        {
            this.popoverController.create(
            {
                component      : FormFormPopover,
                componentProps :
                {
                    data  : new Document({bonus:bonus}),
                    form  : this.bonusForm,
                    onAdd : ((event:any) =>
                    {
                        this.bonus = event.data.bonus;
                        this._attachments[AttachmentType.BONUS.value] = event.data.bonus;
                        this.inputComponent.instance.setAttachmentByType(event.data.bonus, AttachmentType.BONUS);
                        this.popoverController.dismiss();
                    }),
                    onSet : ((event:any) =>
                    {
                        this.bonus = event.data.bonus;
                        this._attachments[AttachmentType.BONUS.value] = event.data.bonus;
                        this.inputComponent.instance.setAttachmentByType(event.data.bonus, AttachmentType.BONUS);
                        this.popoverController.dismiss();
                    }),
                    onClose : (() =>
                    {
                        this.popoverController.dismiss();
                    }),
                },
                showBackdrop : false,
                cssClass : 'right-popover',
            })
            .then((popover:any) =>
            {
                popover.present();
            });
        }
    }

    openUrl(title:string, url:string)
    {
        title;
        window.open(url, '_blank');
        /*const modalController = this.core().injector.get(ModalController);

        modalController.create({
            component       : IframeModal,
            backdropDismiss : false,
            cssClass        : 'full',
            componentProps  : {
                url     : url,
                title   : title,
                onClose : () =>
                {
                    modalController.dismiss();
                }
            }
        })
        .then((modal:any) =>
        {
            modal.present();
        });*/
    }
}
