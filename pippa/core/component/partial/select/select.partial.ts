import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial } from '../base.partial';

@Component({
    selector        : `.select-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<p-checkbox color="success" 
								   animation="smooth" 
								   shape="curve"
								   bigger="true" 
								   [(checked)]="selected" 
								   (change)="onInput($event)">
					   </p-checkbox>`
})
export class SelectPartial extends BasePartial
{
    @Input() labelId : any;
		public selected : boolean = false;

    @Output('click') click : EventEmitter<any> = new EventEmitter();

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    { 
        super(elementRef, renderer, changeDetectorRef, domController);
    }

	onInput(event:any)
  {
			this.selected = event.checked;

			if(this.selected)
			{
					this.parent.parent.addClasses(['selected']);
			}
			else
			{
					this.parent.parent.delClasses(['selected']);
			}		

			this.click.emit({
				data     : this.data,
				selected : this.selected
			});

			this.markForCheck();
  };
}
