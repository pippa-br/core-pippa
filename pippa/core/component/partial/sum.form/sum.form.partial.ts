import { Component, Input, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial }      from '../base.partial';
import { SumFormTransform } from '../../transform/sum.form/sum.form.transform';

@Component({
    selector : `[sum-form-partial]`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<div sum-form-output [output]="output"></div>`
})
export class SumFormPartial extends BasePartial
{
    public output : any;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }

    @Input()
    set data(data:any)
    {
        SumFormTransform.transform(null, data).then((output) =>
        {
            this.output = output;
        });
    }
}
