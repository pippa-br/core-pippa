import { ElementRef, Renderer2, AfterViewInit, OnDestroy, HostBinding, Output, Input, EventEmitter, ChangeDetectorRef, Directive } from '@angular/core';
import { Subscription } from 'rxjs';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { Core } from '../../../core/util/core/core';

@Directive()
export abstract class BasePartial implements AfterViewInit, OnDestroy
{
	@HostBinding('class.hide') hide : boolean = false;

	public _data               : any;
	public _grid               : any;
	public _viewer             : any;
	public _display            : boolean = true;
	public _displayParent      : boolean = true;
    public _disable            : boolean = false;
    public _offsetX            : number;
    public _attributes         : any;
    public _isRendererComplete : boolean = false;
    public _resolve            : any;
    public subscriptions       : Subscription = new Subscription();
    public formItem            : any;
    public parent              : any;

    @Output('rendererComplete') rendererCompleteEvent : EventEmitter<any> = new EventEmitter();

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
    }

    markForCheck()
    {
        this.changeDetectorRef.markForCheck();
    }

    ngAfterViewInit()
    {        
        this.rendererComplete();
    }

    isRendererComplete()
    {
        return new Promise((resolve) =>
        {
            this._resolve = resolve;

            if(this._isRendererComplete)
            {
                this._resolve();
            }
        });
    }

    rendererComplete()
    {
        this.onRendererComplete();
    }

    onRendererComplete()
    {
        this._isRendererComplete = true;
        this.rendererCompleteEvent.emit();

        if(this._resolve)
        {
            this._resolve();
        }
    }

    get offsetX()
    {
        return this._offsetX;
    }

    set offsetX(value:number)
    {
        this.domController.write(() => 
        {        
            this._offsetX = -1 * value;

            this.renderer.setStyle(this.elementRef.nativeElement, 'margin-left', this._offsetX + 'px');                
        })
    }

    get data()
    {
        return this._data;
    }

	@Input()
    set data(value:any)
    {
        this._data = value;
	}
	
	get grid()
    {
        return this._grid;
    }

    set grid(value:any)
    {
        this._grid = value;
    }

	get viewer()
    {
        return this._viewer;
    }

    set viewer(value:any)
    {
        this._viewer = value;
    }

    set attributes(values)
    {
        this.domController.write(() => 
        {
            this._attributes = values;

            for(let key in values)
            {
                if(values[key]['name'] != '')
                {
                    this.renderer.setAttribute(this.elementRef.nativeElement, values[key]['name'], values[key]['value'])
                }
            }    
        });
    }

    get attributes()
    {
        return this._attributes;
    }

    set classes(values:any)
    {
        this.addClasses(values);
	}

	addClasses(values:any)
    {
        this.domController.write(() => 
        {
            for(let key in values)
            {
                if(values[key] != '')
                {
                    this.renderer.addClass(this.elementRef.nativeElement, values[key])
                }
            }    
        });
	}
	
	delClasses(values:any)
    {
        this.domController.write(() => 
        {
            for(let key in values)
            {
                if(values[key] != '')
                {
					this.renderer.removeClass(this.elementRef.nativeElement, values[key]);
                }
            }    
        });
    }

    get display():boolean
    {
        return this._display;
    }

    set display(value:boolean)
    {
        this._display	   = value;
		this.disable	   = !value;  
		this.hide     	   = !value;
		this.displayParent = value;
	}
	
	get displayParent():boolean
    {
        return this._displayParent;
    }

    set displayParent(value:boolean)
    {
		this._displayParent = value;
		this.hide     	    = !value;

		/* DISPLAY O PARENT */
		if(this.parent)
		{
			this.parent.displayParent = value;
		}		                
    }
    
    set disable(value:boolean)
    {		
        this._disable = value;
        this.markForCheck();
    }
    
    get disable():boolean
    {
        return this._disable;
    }
    
    validateSave()
    {
        return true;
    }

    validate()
    {
        return true;
    }

    core()
    {
        return Core.get()
    }

    ngOnDestroy()
    {
        //console.log('partial ngOnDestroy');
	}
	
	trackById(index:number, item:any)
    {
        index;
        return item.id;
    }
}
