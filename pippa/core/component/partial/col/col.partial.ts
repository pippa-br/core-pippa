import { Component, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';

@Component({
    selector 		: `ion-col.col-partial.col`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template 		: `<ng-template #template></ng-template>`
})
export class ColPartial extends BaseContainerPartial
{
    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }
}
