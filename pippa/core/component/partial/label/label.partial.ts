import { Component, Input, Output, ChangeDetectionStrategy, ElementRef, Renderer2, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial } from '../base.partial';

@Component({
    selector        : `.label-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<label (click)="onClick($event)" hideDelay="0" showDelay="0" matTooltip="{{tooltip}}">{{label}}</label>`
})
export class LabelPartial extends BasePartial
{
    public label   : string;
	public tooltip : string;

	@Output('click') click : EventEmitter<any> = new EventEmitter();

    constructor( 
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }

	onClick(event:any)
    {
        event.data = this.data;

        this.click.emit(event);
    };
}
