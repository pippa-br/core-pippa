import { ElementRef, Renderer2, ViewContainerRef, ViewChild, OnDestroy, ChangeDetectorRef, Directive, HostBinding } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial }      from './base.partial';
import { PartialFactory }   from '../../factory/partial/partial.factory';
import { InputFactory }     from '../../factory/input/input.factory';
import { TransformFactory } from '../../factory/transform/transform.factory';
import { OutputFactory }    from '../../factory/output/output.factory';

@Directive()
export abstract class BaseContainerPartial extends BasePartial implements OnDestroy
{
    @ViewChild('template', { read : ViewContainerRef, static : true }) template : ViewContainerRef;	

    public components  : Array<any> = [];
	public _data       : any;
    public _updateData : any;    
	public _viewer     : any;
	public _grid       : any;
    public _submitted  : boolean = false;
    public isValid     : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }

    async addPartial(partial:any, args?:any)
    {
        const component           = await PartialFactory.createComponent(partial, this.template, args);
        component.instance.parent = this;
		component.instance.grid   = this.grid;

        this.components.push(component);

        return component;
    }

	async addOutput(formItem:any, data:any, args?:any)
    {
        const component 		  = await OutputFactory.createComponent(formItem, this.template, args);
		component.instance.parent = this;
		component.instance.grid   = this.grid;		

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            component.instance.output = output;
        });

        this.components.push(component);

        return component;
    }

    async addInput(formItem:any, formGroup:any, args:any)
    {
        const inputComponent 		   = await InputFactory.createComponent(formItem, this.template, formGroup, args);
		inputComponent.instance.parent = this;

        this.components.push(inputComponent);

        return inputComponent;
    }

    isRendererComplete():any
    {
        return new Promise((resolve) =>
        {
            this._resolve = resolve;

            const promises = [];

            for(const key in this.components)
            {
                const instance = this.components[key].instance;
                const promise  = instance.isRendererComplete();

                promises.push(promise);
            }

            Promise.all(promises).then(() =>
            {
                this.onRendererComplete();
            });

			this._resolve();
        });
    }

    /* OVERRIDER */
    rendererComplete()
    {
	}

    updateDisplayControlByValue()
    {
        for(const key in this.components)
        {
            this.components[key].instance.updateDisplayControlByValue();
        }
    }

    set display(value:boolean)
    {
		this._display	   = value;   
		this.disable 	   = !value;         
		this.hide     	   = !value;
		this.displayParent = value;

        for(const key in this.components)
        {
            this.components[key].instance.display = value;
        }
	}

	set displayOnlyParent(value:boolean)
    {
		this._display = value;   
		this.disable  = !value;         
		this.hide     = !value;
	}
	
	set displayParent(value:boolean)
    {
		/* DISPLAY O PARENT */
		if(this.parent && this.components.length == 1)
		{
			this._displayParent 	  = value;
			this.hide     	    	  = !value;
			this.parent.displayParent = value;
		}		                
    }

    set submitted(value:boolean)
    {
        this._submitted = value;

        for(const key in this.components)
        {
            this.components[key].instance.submitted = value;
        }
    }

    get submitted()
    {
        return this._submitted;
    }

    set data(value:any)
    {
        this._data = value;

        for(const key in this.components)
        {
            this.components[key].instance.data = value;
        }
    }

    get data()
    {
        return this._data;
	}

    set updateData(value:any)
    {
        this._updateData = value;

        for(const key in this.components)
        {
            this.components[key].instance.updateData = value;
        }
    }

    get updateData()
    {
        return this._updateData;
	}
	
	set grid(value:any)
    {
		this._grid = value;
		
	    for(const key in this.components)
        {
            this.components[key].instance.grid = value;
        }
    }

    get grid()
    {
        return this._grid;
    }

	set viewer(value:any)
    {
        this._viewer = value;

        for(const key in this.components)
        {
            this.components[key].instance.viewer = value;
        }
    }

    get viewer()
    {
        return this._viewer;
	}

    validateSave()
    {
        this.isValid = true;

        for(const key in this.components)
        {
            if(!this.components[key].instance.validateSave())
            {
                this.isValid = false;
            }
        }

        return this.isValid;
    }


    validate()
    {
        this.isValid = true;

        for(const key in this.components)
        {
            if(!this.components[key].instance.validate())
            {
                this.isValid = false;
            }
        }

        return this.isValid;
    }

    clear()
    {
        if(this.template)
        {
            this.template.clear();
        }        

        for(const key in this.components)
        {
            this.components[key].destroy();
        }

        this.components = [];
        this.subscriptions.unsubscribe();
    }

    ngOnDestroy()
    {
        this.clear();
    }
}
