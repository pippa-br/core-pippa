import { Component, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef, ViewChild, ViewContainerRef } from '@angular/core';
import { DomController }  from '@ionic/angular';
import { PartialFactory } from '../../../factory/partial/partial.factory';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';

@Component({
    selector        : `swiper-slide.slide-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<div class="buttons">
                            <ng-template #templateButtons></ng-template>
					   </div>
					   <ng-template #template></ng-template>`
})
export class SlidePartial extends BaseContainerPartial
{
    @ViewChild('templateButtons', { read: ViewContainerRef, static:true }) templateButtons : ViewContainerRef;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }
    
    async addButtonsPartial(partial:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, this.templateButtons, args);
        this.components.push(component);

        return component;
    }

    clear()
    {
        this.templateButtons.clear();

        super.clear();
    }
}
