import { Component, ViewContainerRef, ViewChild, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';
import { TransformFactory }     from '../../../factory/transform/transform.factory';
import { OutputFactory }        from '../../../factory/output/output.factory';
import { PartialFactory }       from '../../../factory/partial/partial.factory';

@Component({
    selector	 	: `ion-card.card-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<ion-card-header class="card-header" [ngClass]="{'has-header-content' : hasHeaderContent}">
							<ion-card-title>
								<ng-template #templateTitle></ng-template>
							</ion-card-title>
							<ion-card-subtitle>
								<ng-template #templateSubTitle></ng-template>
							</ion-card-subtitle>
						</ion-card-header>
						<div class="before-content">
							<ng-template #templateBefore></ng-template>
						</div>
						<ion-card-content class="card-content">
							<ng-template #template></ng-template>
						</ion-card-content>
						<div class="after-content">
							<ng-template #templateAfter></ng-template>
						</div>                
						<ion-card-header class="card-footer">
							<ng-template #templateFooter></ng-template>
						</ion-card-header>`
})
export class CardPartial extends BaseContainerPartial
{
    @ViewChild('templateTitle',    { read: ViewContainerRef, static:true }) templateTitle    : ViewContainerRef;
    @ViewChild('templateSubTitle', { read: ViewContainerRef, static:true }) templateSubTitle : ViewContainerRef;
    @ViewChild('templateFooter',   { read: ViewContainerRef, static:true }) templateFooter   : ViewContainerRef;
    @ViewChild('templateBefore',   { read: ViewContainerRef, static:true }) templateBefore   : ViewContainerRef;
	@ViewChild('templateAfter',    { read: ViewContainerRef, static:true }) templateAfter    : ViewContainerRef;
	
	public hasHeaderContent : boolean = false;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }

    async addTitleOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.templateTitle, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

		this.components.push(outputComponent);
		
		this.hasHeaderContent = true;

        return outputComponent;
    }

    async addSubTitleOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.templateSubTitle, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

		this.components.push(outputComponent);
		
		this.hasHeaderContent = true;

        return outputComponent;
    }

    async addFooterOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.templateFooter, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

        this.components.push(outputComponent);

        return outputComponent;
    }

	async addBeforeOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.templateBefore, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

        this.components.push(outputComponent);

        return outputComponent;
    }

	async addAfterOutput(formItem:any, data:any, args?:any)
    {
        const outputComponent = await OutputFactory.createComponent(formItem, this.templateAfter, args);

        TransformFactory.transform(formItem, data).then((output:any) =>
        {
            outputComponent.instance.output = output;
        });

        this.components.push(outputComponent);

        return outputComponent;
    }

    async addFooterPartial(partial:any, args?:any)
    {
        const component = await PartialFactory.createComponent(partial, this.templateFooter, args);
        this.components.push(component);

        return component;
    }

    clear()
    {
        this.templateTitle.clear(); 
        this.templateSubTitle.clear(); 
        this.templateFooter.clear();
        this.templateBefore.clear();
        this.templateAfter.clear();

        super.clear();
    }
}
