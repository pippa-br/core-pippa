import { Component, Input, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BasePartial } from '../base.partial';

@Component({
    selector : `.text-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template : `<p [innerHtml]="text"></p>`
})
export class TextPartial extends BasePartial
{
    @Input() text : string;

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
        super(elementRef, renderer, changeDetectorRef, domController);
    }
}
