import { Component, ViewContainerRef, ViewChild, ChangeDetectionStrategy, ElementRef, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DomController } from '@ionic/angular';

/* PIPPA */
import { BaseContainerPartial } from '../base.container.partial';
import { PartialFactory }       from '../../../factory/partial/partial.factory';
import { PartialType }         from '../../../type/partial/partial.type';

@Component({
    selector        : `.tab-partial`,
    changeDetection : ChangeDetectionStrategy.OnPush,
    template        : `<ion-grid>
                            <ion-row class="row-tab">
                                <ion-col size-xs="12" size-sm="12" size-md="3" size-lg="3" size-xl="3">
                                    <ion-segment [value]="selected">
                                        <ng-template #templateSegmentButton></ng-template>
                                    </ion-segment>
                                </ion-col>
                                <ion-col size-xs="12" size-sm="12" size-md="9" size-lg="9" size-xl="9">
									<ng-template #templateContent></ng-template>
                                </ion-col>
                            </ion-row>
                       </ion-grid>`
})
export class TabPartial extends BaseContainerPartial
{
    @ViewChild('templateSegmentButton', { read : ViewContainerRef, static : true }) templateSegmentButton : ViewContainerRef;
    @ViewChild('templateContent',       { read : ViewContainerRef, static : true }) templateContent       : ViewContainerRef;

	public count    : number  = 0;
	public slides   : any;
	public selected : string = 'tab0';

    constructor(
        public elementRef        : ElementRef,
        public renderer          : Renderer2,
        public changeDetectorRef : ChangeDetectorRef,
        public domController     : DomController,
    )
    {
		super(elementRef, renderer, changeDetectorRef, domController);  		
	}
	
	async createSlides()
	{
		this.slides = await PartialFactory.createComponent(PartialType.type('Slides'), this.templateContent);
		this.components.push(this.slides);	
	}

    async createSegmentButtonPartial()
    {
		let classes : any = [];

		if(this.count == 0)
		{
			classes.push('segment-button-checked'); // DEPOIS REMOVER
		}

		const args = { 
			index   : this.count,
			attributes : [{
				name  : 'value',
				value : 'tab' + this.count
			}],
			classes : classes,
			onClick : (async (index:number) =>
			{		
				this.slides.instance.slideTo(index);   
				
				console.log('go', event, index);

				/* SLIDE ESTAVA TRAVANDO */
				this.markForCheck();
			})
		};

		this.count++

        const component = await PartialFactory.createComponent(PartialType.type('SegmentButton'), this.templateSegmentButton, args);
		this.components.push(component);
		
        return component;
    }

    async createSlidePartial()
    {		
		return this.slides.instance.createSlidePartial();
    }   

    async clear()
    {
		this.slides.instance.clear();
        this.templateSegmentButton.clear();
		this.templateContent.clear();
		this.count = 0;
		
        super.clear();
    }
}
