import { Component, Input, ViewChild, ViewContainerRef, OnDestroy } from '@angular/core';
import { Howl } from 'howler';

/* CORE */
import { BaseComponent } from '../../../../core/base/base.component';

@Component({
	selector: 'radio-player-block',
	template: `<div #backgroundImage class="background-image">
					<h1>{{ stationTitle }}</h1>
				</div>
				<ion-footer>
					<div class="container" id="container">
						<div #circle class="circle" style="transform: rotate(90deg) !important">
							<ion-avatar id="station-img" *ngFor="let station of stations" (click)="start(station)"
										[ngStyle]="{'top':station.top + 'px', 'left':station.left + 'px', 'transform' : 'rotate(' + (station.degrees + 90) + 'deg)'}">
								<img src="{{ station.thumb._url }}" style="top:{{station.sin}};left:{{station.cos}}">
							</ion-avatar>
						</div>
					</div>
					<ion-toolbar>
						<ion-row>
							<ion-col size="12" class="ion-text-center">
								<p class="station-title">{{ activeTrack?.name }}</p>
							</ion-col>
							<ion-col size="12" >        
								<ion-range min="0" max="100" step="5" color="success" value="100" (ionChange)="updateVolume($event)">
									<ion-icon slot="start-outline" name="volume-medium-outline"></ion-icon>
								</ion-range>           
							</ion-col>
							<ion-col size="12" class="ion-text-center">
								<ion-button fill="clear prev-button" (click)="prev()">
									<ion-icon slot="icon-only" name="play-skip-back-outline"></ion-icon>          
								</ion-button>
								<ion-button fill="clear" (click)="togglePlayer(false)" *ngIf="!isPlaying" >
									<ion-icon slot="icon-only" name="play-outline"></ion-icon>          
								</ion-button>
								<ion-button fill="clear"(click)="togglePlayer(true)" *ngIf="isPlaying">
									<ion-icon slot="icon-only" name="pause-outline"></ion-icon>          
								</ion-button>
								<ion-button fill="clear next-button" (click)="next()">
									<ion-icon slot="icon-only" name="play-skip-forward-outline"></ion-icon>          
								</ion-button>
							</ion-col>
						</ion-row>
					</ion-toolbar>
				</ion-footer>`,
})

export class RadioPlayerBlock extends BaseComponent implements OnDestroy
{
	@ViewChild('backgroundImage', { read: ViewContainerRef, static:true }) backgroundImage : any;
	@ViewChild('circle', 		  { read: ViewContainerRef, static:true }) circle 		   : any;

	public activeTrack  : any = null;
	public player	    : Howl = null;
	public isPlaying    : Boolean = false;
	public stationTitle : string;
	public _stations    : any;
	public radius       : number = 0;
	public minDegrees   : number = 0;
	
	@Input() offset : number = 0;

	@Input()
	set stations(values)
	{			
		if(values && values.length  > 0)
		{
			this.radius 	= (window.screen.availHeight / 2) + this.offset;
			this.minDegrees = 360 / values.length;
			let i  : number = 0;

			for(const key in values)
			{
				values[key].degrees  = this.minDegrees * i + 45;
				values[key].radians  = (values[key].degrees) * (Math.PI / 180);
				values[key].top  	 = Math.round(Math.sin(values[key].radians) * this.radius);
				values[key].left 	 = Math.round(Math.cos(values[key].radians) * this.radius); 
				values[key].auto 	 = (values[key].auto == undefined ? true : values[key].auto); 
				i++;
			}	
			
			setTimeout(() => 
			{
				this.start(this.stations[0]);
			});
		}

		this._stations = values;
	}

	get stations()
	{
		return this._stations;
	}

	start(track:any) 
	{
		if(this.player) 
		{
			this.player.stop();
			this.player.unload();
			this.player._src = track.url;
			this.player.load();
		}
		else
		{
			this.stationTitle = track.title || track.name;
			this.activeTrack  = track;

			this.player = new Howl(
			{
				src    : [track.url],
				html5  : true,
				format : ['mp3', 'aac'],
				volume : 1,
			});
	
			this.player.on('load', (e) =>
			{
				console.log('load', e);
	
				this.isPlaying = true;
				
				if(this.activeTrack.auto)
				{
					this.player.play();
				}				
			});
	
			this.player.on('loaderror', (e) =>
			{
				console.log('loaderror', e);
			});
	
			this.player.on('mute', (e) =>
			{
				console.log('mute', e);
			});
	
			this.player.on('fade', (e) =>
			{
				console.log('fade', e);
			});
	
			this.player.on('playerror', (e) =>
			{
				console.log('playerror', e);
			});
	
			this.player.on('end', () =>
			{
				this.player.stop();
			});		
		}			

		this.circle.element.nativeElement.style.transform = 'rotate(-' + ((track.degrees) + 90) +'deg)';
		this.backgroundImage.element.nativeElement.style.backgroundImage = "linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(" + track.image._url + ")";		
	}

	togglePlayer(pause) 
	{		
		if(this.player)
		{
			this.isPlaying = !pause;
			
			if(pause) 
			{
				this.player.pause();
			}
			else 
			{
				this.player.play();
			}
		}		
	}

	onChange(track)
	{
		let current = this.stations.indexOf(this.activeTrack);
		let index   = this.stations.indexOf(track);

		if(current > index)
		{
			this.next()
		}
		else
		{
			this.prev()
		}
	}

	next() 
	{
		let index = this.stations.indexOf(this.activeTrack);
		
		if(index != this.stations.length - 1) 
		{
			this.start(this.stations[index + 1]);
		}
		else 
		{
			this.start(this.stations[0]);
		}
	}

	prev() 
	{
		let index = this.stations.indexOf(this.activeTrack);
		
		if(index > 0) 
		{
			this.start(this.stations[index - 1]);
		}
		else 
		{
			this.start(this.stations[this.stations.length - 1]);
		}
	}

	updateVolume(e) 
	{
		console.log((e.detail.value / 100).toFixed(2));
		const vol = (e.detail.value / 100).toFixed(2);
		this.player.volume(vol);
	}

	ngOnDestroy()
    {
		this.player.stop();
        this.player.unload();

        console.log('sound destroy');
    }
}
