import { Component, ViewChild, EventEmitter, AfterViewInit, Input } from '@angular/core';

/* PIPPA */
import { BaseComponent }  from '../../../base/base.component';
import { DocumentPlugin } from '../../../plugin/document/document.plugin';

@Component({
    selector : 'bible-block',
	template : `<div class="books">
					<swiper-container placeholder="Livro" interface="popover" [value]="selectedBook" (ionChange)="selectBook($event)">
						<ion-select-option *ngFor="let item of books" [value]="item">{{item.name}}</ion-select-option>
					</swiper-container>
					<swiper-container *ngIf="selectedBook" placeholder="Capitulos" interface="popover" [value]="selectedChapter"
								(ionChange)="selectChapter($event)">
						<ion-select-option *ngFor="let item of chapters" [value]="item">{{item.name}}</ion-select-option>
					</swiper-container>
				</div>
				<div class="verses">
					<p *ngFor="let item of verses">
						{{item.number}} - {{item.text}}
					</p>				
				</div>`,
})
export class BibleBlock extends BaseComponent implements AfterViewInit
{
	public _appid   	   : string;
	public books    	   : any
	public chapters 	   : any;
	public verses 		   : any;
	public selectedBook    : any;
	public selectedChapter : any;

    @Input() 
    set appid(appid:string)
    {
        this._appid = appid;
    }

    get appid()
    {
        return this._appid;
    }

    ngAfterViewInit()
    {   
		const params = this.createParams({
			apiUrl : 'https://www.abibliadigital.com.br/api/books',
			method : 'get',
			withCredentials : false,
		});

		this.core().api.getRequest(params).then((result) => 
		{
			if(result)
			{
				this.books = result;
				this.selectBook({
					detail : {
						value : result[0]
					}
				})
				this.selectChapter({
					detail : {
						value : {
							name  : 1,
							value : 1
						}
					}
				});
			}	
		});
	}

	selectBook(event:any)
	{
		//book.abbrev.pt

		this.selectedBook    = event.detail.value;
		this.selectedChapter = null;

		const chapters = [];

		for(let i = 1; i <= this.selectedBook.chapters; i++)
		{
			chapters.push({
				name  : i,
				value : i,
			});
		}

		this.chapters = chapters;
	}

	selectChapter(event:any)
	{		
		const chapter = event.detail.value;

		const params = this.createParams({
			apiUrl : 'https://www.abibliadigital.com.br/api/verses/nvi/' + this.selectedBook.abbrev.pt  + '/' + chapter.value,
			method : 'get',
			withCredentials : false,
		});
		
		this.core().api.getRequest(params).then((result) => 
		{	
			this.verses = result.verses;			
		});
	}
}
