import { Component, Input, OnDestroy } from '@angular/core';
import { NavParams, NavController } from '@ionic/angular';

declare var Howler : any;
declare var Howl   : any;

/* CORE */
import { BaseComponent } from '../../../../core/base/base.component';

@Component({
    selector : 'audio-player-block',
    template : `<ion-item>
                    <ion-icon *ngIf="!isPlaying" item-start name="play-outline" (tap)="play()"></ion-icon>
                    <ion-icon *ngIf="isPlaying" item-start name="pause-outline" (tap)="pause()"></ion-icon>
                    <div class="controller" item-content>
                        <ion-range [max]="duration" [(ngModel)]="time" (ionBlur)="changeTime()">
                            <ion-label *ngIf="time != 0" range-right>{{timeFormat(time)}}</ion-label>
                            <ion-label *ngIf="time == 0" range-right>{{timeFormat(duration)}}</ion-label>
                        </ion-range>
                    </div>
                    <ion-avatar item-end>
                        <img [src]="photo">
                    </ion-avatar>
                </ion-item>`,
})
export class AudioPlayerBlock extends BaseComponent implements OnDestroy
{
    @Input() photo : string;

    sound;
    int;
    url;
    time = 0;
    duration = 0;
    isPlaying = false;

    constructor(
    )
    {
        super();
    }

    @Input()
    set src(url)
    {
        this.url = url;

        this.sound = new Howl({
            src     : this.url,
            html5   : !this.core().isApp,
            volume  : 1.0,
            preload : true
        });

        this.sound.on('load', () =>
        {
            this.duration = this.sound.duration() * 100;

            console.log('load', this.sound.duration());
        });

        this.sound.on('loaderror', (e) =>
        {
            console.log('loaderror', e);
        });

        this.sound.on('mute', (e) =>
        {
            console.log('mute', e);
        });

        this.sound.on('fade', (e) =>
        {
            console.log('fade', e);
        });

        this.sound.on('playerror', (e) =>
        {
            console.log('playerror', e);
        });

        this.sound.on('end', () =>
        {
            this.stop();
        });
    }

    changeTime()
    {
        if(this.time > this.duration)
        {
            this.stop();
        }
        else if(this.time == 0)
        {
            this.stop();
        }
        else
        {
            this.seek(this.time / 100);
        }
    }

    play()
    {
        this.sound.play();

        this.isPlaying = true;
        console.log('play');

        this.int = setInterval(() => {
            this.time++;
        }, 10); /* MENOR INTERVALO DO setInterval é 10 */
    }

    seek(time)
    {
        this.sound.seek(time);
    }

    pause()
    {
        this.sound.pause();

        this.isPlaying = false;
        clearInterval(this.int);
        console.log('pause');
    }

    stop()
    {
        this.sound.stop();
        this.clear();

        console.log('stop');
    }

    clear()
    {
        clearInterval(this.int); 
        this.time = 0;
        this.isPlaying = false;
        console.log('clear');
    }

    timeFormat(milliseconds)
    {
        var secs    = milliseconds / 100;
        var minutes = Math.floor(secs / 60) || 0;
        var seconds = (secs - minutes * 60) || 0;

        seconds = Math.round(seconds);
        minutes = Math.round(minutes);

        return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
    }

    ngOnDestroy()
    {
		this.sound.stop();
        this.sound.unload();

        console.log('sound destroy');
    }
}
