import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

/* PIPPA */
import { Core } from '../../../util/core/core';
import { BaseComponent } from '../../../base/base.component';

@Component({
    selector    : 'menu-footer-block',
    templateUrl : './menu.footer.block.html',
})
export class MenuFooterBlock extends BaseComponent implements OnDestroy
{
    @Output() page      : EventEmitter<any> = new EventEmitter();    
    @Input() hasProfile : boolean = true;    
    
    public _menu  : any;    
    public itens : Array<any>;    
    public unsubscribe;

    constructor(
        public actionSheetController: ActionSheetController
    )
    {
        super();
    }

    @Input()
    set menu(value:any)
    {
        if(value)
        {
            this.doUnsubscribe();

            this.unsubscribe = value.onUpdate().subscribe(async (value2) => 
            {                
                this.menu = value2;
            });

            const itens = [];

            for(const key in value)
            {
                const menu = value[key];

				if(menu.slots)
                {
					let has : boolean = false;

					for(const key2 in menu.slots)
					{
						if(menu.slots[key2].value == 'footer')
						{
							has = true;
						}
					}

					if(has)
					{
						itens.push(menu);
					}
                }
                else if(menu.slot)
                {
                    if(menu.slot.value == 'footer')
                    {
                        itens.push(menu);
                    }
                }
                else
                {
                    itens.push(menu);
                }
            }

            this.itens = itens;
        }        

        this._menu = value;
    }

    get menu()
    {
        return this._menu;
    }

    doUnsubscribe()
    {
        if(this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
    }

    async openOptions(item:any)
    {
        if(item.router)
        {
            this.page.emit(item);
        }
        else
        {
            const options = [];

            for(const key in item._children)
            {
                options.push({
                    text    : item._children[key].name,
                    handler : () =>
                    {
                        this.page.emit(item._children[key]);
                    }
                });
            }

            const actionSheet = await this.actionSheetController.create({
                header  : item.name,
                buttons : options,
            });

            await actionSheet.present();            
        }
    }

    core()
    {
        return Core.get();
    }

    trackById(index:any, item:any) { index; return item.id; }

    ngOnDestroy()
    {
        this.doUnsubscribe();
    }
}
