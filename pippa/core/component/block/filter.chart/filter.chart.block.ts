import { Component, Input, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { PopoverController } from "@ionic/angular";

/* CORE */
import { BaseComponent }   from "../../../../core/base/base.component";
import { FilterPlugin }    from "../../../../core/plugin/filter/filter.plugin";
import { WhereCollection } from "../../../model/where/where.collection";
import { FormFormPopover } from "../../../popover/form.form/form.form.popover";
import { Document } from "../../../model/document/document";

@Component({
    selector    				: "filter-chart-block",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="filter-chart">
										<ion-segment (ionChange)="onChangeType()" [(ngModel)]="paginationType">
										<ion-segment-button *ngFor="let item of pagination" [value]="item.value">
											<ion-label>{{item.label}}</ion-label>
										</ion-segment-button>			
										<ion-segment-button (click)="openFilter()" *ngIf="pagination && pagination.length > 0">		
											<ion-icon name="funnel-outline"></ion-icon>								 											
										</ion-segment-button>
										<ion-segment-button (click)="reload()" *ngIf="pagination && pagination.length > 0">		
											<ion-icon name="refresh-outline"></ion-icon>					 											
										</ion-segment-button>
									</ion-segment>
									<div class="labels-chart">
											<span *ngFor="let item of labels" 
												[ngClass]="{'enabled':item.enabled, 'disabled' : !item.enabled}">
												{{item.label}}: {{item.value}}
											</span>											
									</div>
					   </div>`,
})
export class FilterChartBlock extends BaseComponent implements OnDestroy
{
    public where				  : any = new WhereCollection();
    public paginationType : any;	
    public pagination     : any     = [];
    public _chart     	  : any;
    public _app  				  : any;
    public dataFilters    : any;
    public labels         : any;

@Input()
    public filter : any;

@Output("change") changeEvent : EventEmitter<any> = new EventEmitter();

constructor(
public popoverController : PopoverController,
public filterPlugin      : FilterPlugin,
public changeDetectorRef : ChangeDetectorRef
)
{
    super();
}

@Input()
set app(value:any)
{
    this._app = value;
}

get app()
{
    return this._app;
}

@Input() set chart(value:any)
{
    this._chart = value;		

    if (value)
    {
        this.paginationType = value.paginationType.value;
        this.pagination     = value.pagination;
    }			
}

get chart()
{
    return this._chart;
}

async onChangeType()
{
    this.clearFilter();

    for (const item of this.pagination)
    {
        if (item.value == this.paginationType)
        {
            this.changeEvent.emit({ paginationType : item });	 
            break
        }					
    }			 
}

markForCheck()
{
    this.changeDetectorRef.markForCheck();
}	

ngOnDestroy()
{
    this.clearFilter();
}


async openFilter()
{
    this.filter.setting = {
        addText    : "Filtrar",
        setText    : "Filtrar",
        cancelText : "Limpar",
    };

    const popover = await this.popoverController.create(
        {
            component      : FormFormPopover,
            componentProps : {
                data   : this.dataFilters,
                form   : this.filter,
                onSave : async (event:any) => 
                {
                    console.log("loadFilters", event.data);

                    for (const key in this.filter.items)
                    {
                        const formItem = this.filter.items[key];
                        const noFilter = formItem.setting && formItem.setting.noFilter;
                        const noZero   = formItem.setting && formItem.setting.noZero;
                        const noEmpty  = formItem.setting && formItem.setting.noEmpty;
                        const model    = new Document(event.data);
                        let value      = await model.getProperty(formItem.name); 

                        /* POR CONTA DE BUSCAS OR */
                        if (value instanceof Array && value.length == 0)
                        {
                            value = null;					
                        }
                        else if (noZero && value === 0) 
                        {
                            value = null;
                        }
                        else if (noEmpty && (value === "" || value === false))
                        {
                            value = null;
                        }

                        if (!noFilter && value !== null)
                        {
                            const where = {
                                id       : formItem.id,
                                label    : formItem.label,
                                name     : formItem.name,
                                field    : formItem.path,
                                operator : formItem.setting.operator,
                                order    : (formItem.setting.order ? formItem.setting.order : parseInt(key) + 100),
                                value    : value,
                                type     : formItem.field.type,
                                setting  : formItem.setting
                            };

                            this.where.set(where);
                        }
                        else
                        {
                            for (const item of this.where)
                            {
                                if (item.field == formItem.path)
                                {
                                    item.enabled = false;
                                }
                            }
                        }
                    }										

                    this.dataFilters = new Document(event.data);

                    this.labels = await this.where.getLabels();

                    //this.load();
                    this.changeEvent.emit({ where : this.where });

                    popover.dismiss();
                    this.markForCheck();
                },
                onCancel : () => 
                {
                    this.clearFilter();

                    //this.load();
                    this.changeEvent.emit({ where : this.where });

                    this.paginationType = this.chart.paginationType.value;

                    popover.dismiss();
                    this.markForCheck();
                },
                onClose : () => 
                {
                    popover.dismiss();
                },
            },
            showBackdrop : false,
            cssClass     : "right-popover",
        });

    popover.present();
}

reload()
{
    this.changeEvent.emit({ where : this.where });
}

clearFilter()
{
    this.where       = new WhereCollection();
    this.dataFilters = null;	
    this.labels      = [];	

//this.changeEvent.emit({ where : this.where });
}
}
