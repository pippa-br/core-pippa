import { Component, ViewChild, EventEmitter, AfterViewInit, Input, ElementRef } from '@angular/core';

/* PIPPA */
import { BaseComponent }  from '../../../base/base.component';
import { DocumentPlugin } from '../../../plugin/document/document.plugin';

@Component({
    selector : 'slide-block',
    template : `<swiper-container #slides pager="true" 
							(slidechange)="onIonSlideWillChange()"
							[options]="slideOpts">
                    <swiper-slide *ngFor="let item of collection">
                        <img [src]="item.image._url"/>
                    </swiper-slide>
				</swiper-container>
				<a class="swiper-button-prev swiper-button-black" (click)="prevSlide()" *ngIf="!isBeginning">
					<ion-icon name="chevron-back-outline"></ion-icon>
				</a>
				<a class="swiper-button-next swiper-button-black" (click)="nextSlide()" *ngIf="!isEnd">
					<ion-icon name="chevron-forward-outline"></ion-icon>
				</a>`,
})
export class SlideBlock extends BaseComponent
{
    @ViewChild('slides') slides : ElementRef | undefined;
	
	public isBeginning : boolean = true;
    public isEnd       : boolean = true;
    public _appid      : string;
    public collection  : any

    @Input()
    public delay : number = 3000;

    public slideOpts = {
		initialSlide   : 0,
        speed          : 400,
		loop           : true,
		autoplay: {
			delay: this.delay,
		},
    }
    
    @Input()
    set appid(appid:string)
    {
        this._appid = appid;

        this.initialize();
    }

    get appid()
    {
        return this._appid;
    }

    initialize()
    {   
        if(this.appid && this.isInitialize)
        {
            const plugin = this.core().injector.get(DocumentPlugin);
            const params = this.createParams({
                appid   : this.appid,
                orderBy : 'order',
            });
            
            plugin.getData(params).then((result) => 
            {
				this.collection = result.collection;
				
				console.log('slides', this.collection);
            });
        }             
	}
	
	nextSlide() 
    {
        this.slides?.nativeElement.swiper.slideNext();
    }
    
    prevSlide() 
    {
        this.slides?.nativeElement.swiper.slidePrev();
    }

    onIonSlideWillChange()
    {
        this.slides?.nativeElement.swiper.isBeginning().then((value) => 
        {
            this.isBeginning = value;
        });

        this.slides?.nativeElement.swiper.isEnd().then((value) => 
        {
            this.isEnd = value;
        });
    }
}
