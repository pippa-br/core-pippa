import { Component, Input, OnInit } from '@angular/core';

/* PIPPA */
import { BaseComponent }  from '../../../base/base.component';
import { DocumentPlugin } from '../../../plugin/document/document.plugin';
import { TDate } from '../../../util/tdate/tdate';
import { addProductCart, productClick, productPurchases } from 'src/dataconnect-generated/js/default-connector';

@Component({ 
    selector : `analytics-viewer-block`,
    template : `<div class="analytics-viewer-block">
                    <table class="striped" *ngIf="productClick">
                        <tr>
                            <th>Produtos Mais Clicados</th>
                        </tr>
                        <tr *ngFor="let item of productClick">
                            <td>{{item.key}}</td>
                            <td>{{item.percentage}}%</td>
                        </tr>
                    </table>
                    <table class="striped" *ngIf="addProductCart">
                        <tr>
                            <th>Produtos Mais Adicionados ao Carrinho</th>
                        </tr>
                        <tr *ngFor="let item of addProductCart">
                            <td>{{item.key}}</td>
                            <td>{{item.percentage}}%</td>
                        </tr>
                    </table>
                    <table class="striped" *ngIf="productPurchases">
                        <tr>
                            <th>Produtos Mais Comprados</th>
                        </tr>
                        <tr *ngFor="let item of productPurchases">
                            <td>{{item.key}}</td>
                            <td>{{item.percentage}}%</td>
                        </tr>
                    </table>
                </div>`
})
export class AnalyticsViewerBlock extends BaseComponent implements OnInit
{
    //@Input() event : any;
    public _event    : any;
    public analytics : any;
    public addProductCart : any;
    public productClick : any;    
    public productPurchases : any;

    constructor(
        public documentPlugin : DocumentPlugin,
    )
    {
        super();
    }

    @Input()
    set event(value:any)
    {
        this._event = value;        
    };

    get event()
    {
        return this._event;        
    };

    initialize(isInitialize:boolean)
    {      
        if (this.event == "month")
        {
            this.getCheckout("month", new TDate().format("yyyy-MM"))
        }		
        else if (this.event == "week")
        {
            this.getCheckout("week", new TDate().format("yyyy-WW"))
        }        
    }
    
    async getCheckout(type:string, date:string)
    {
        const result = await addProductCart();
        this.addProductCart = this.parseData(result?.data?.addProductCarts)        

        const result2 = await productClick();
        this.productClick = this.parseData(result2?.data?.productClicks)        

        const result3 = await productPurchases();
        this.productPurchases = this.parseData(result3?.data?.productPurchases)        
    }

    parseData(list:any)
    {
        if(list)
        {
            let total = 0;

            for(const key in list)
            {
                total += list[key].total;
            } 

            for(const key in list)
            {
                list[key].percentage = +Number(list[key].total * 100 / total).toFixed(2)                
            }            
        }
        else
        {
            return [];
        }        

        return list;
    }


    ngOnInit()
    {
    }
}
