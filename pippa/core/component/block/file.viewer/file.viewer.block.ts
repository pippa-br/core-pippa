import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

/* PIPPA */
import { BaseComponent }  from '../../../base/base.component';
import { FileCollection } from '../../../model/file/file.collection';
import { File }           from '../../../model/file/file';

@Component({
    selector : `div[file-viewer-block]`,
    template : `<ul class="file-viewer-block">
                    <li *ngFor="let file of files">
                        <div class="{{file.type}}" (click)="open(file)">
                            <img [src]="file.url" *ngIf="file.isImage()"/>
                            <audio-player-block [src]="file.url" [photo]="photo" *ngIf="file.isAudio()"></audio-player-block>
                            <ion-icon *ngIf="file.isDocument()" name="document-outline"></ion-icon>
                            <p *ngIf="file.isDocument()" class="name">{{file.name}}</p>
                            <div class="embed-responsive embed-responsive-16by9" *ngIf="file.isVideo()">
                                <video class="embed-responsive-item" width="320" height="240" controls>
                                    <source src="{{file.url}}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            </div>
                        </div>
                        <a *ngIf="hasDel" class="del-button" (click)="onDel(file)">Remover</a>
                    </li>
                </ul>`
})
export class FileViewerBlock extends BaseComponent implements OnInit
{
    @Input() files  : FileCollection;
    @Input() hasDel : boolean = false;
    @Input() photo  : string;

    @Output('del') delEvent : EventEmitter<any> = new EventEmitter();

    constructor(
    )
    {
        super();
    }

    @Input()
    set file(file:File)
    {
		this.files = new FileCollection();

		console.error('----', file);

        this.files.add(file);
    };

    open(file:File)
    {
        console.log('Open file', file);

        if(file.isDocument())
        {
            //this.browserNative.openDocument(file.url);
        }
        else if(file.isImage())
        {
            //this.photoViewerNative.show(file.url);
        }
        else if(file.isAudio())
        {
            /* NÃO FAZ NADA */
        }
        else
        {
            //this.browserNative.openFile(file.url);
        }
    }

    onDel(file:File)
    {
        this.files.del(file);
        this.delEvent.emit(file);
    }

    ngOnInit()
    {
    }
}
