import { Component, Input, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { PopoverController } from "@ionic/angular";

/* CORE */
import { BaseComponent }   from "../../../../core/base/base.component";
import { WhereCollection } from "../../../../core/model/where/where.collection";
import { FilterPlugin }    from "../../../../core/plugin/filter/filter.plugin";
import { ListPopover }     from "../../../../core/popover/list/list.popover";
import { Document }        from "../../../../core/model/document/document";
import { FormFormPopover } from "../../../../core/popover/form.form/form.form.popover";
import { FieldType }       from "../../../../core/type/field/field.type";

@Component({
    selector      		: "filter-header-block",
    changeDetection : ChangeDetectionStrategy.OnPush,
    template      		: `<div class="filter-bar">
							<div class="filter-wrapper" *ifAcl="'filter_document';acl:acl;value:false;">
								<ion-chip *ngFor="let item of labels" 
											[ngClass]="{'enabled':item.enabled, 'disabled' : !item.enabled}"
											(click)="goListFilter($event)">
									<ion-label>{{item.label}}: {{item.value}}</ion-label>
								</ion-chip>
								<ion-chip class="close-filter enabled" 
											*ngIf="whereFilters && whereFilters.length > 0" 
											(click)="clearFilter()">
									<ion-icon name="close-outline"></ion-icon>
								</ion-chip>
								<ion-buttons slot="end">
                                    <ion-button icon-only (click)="goListFilter($event)">
                                        <ion-icon name="funnel-outline"></ion-icon>
                                    </ion-button>
                                </ion-buttons>
							</div>
							</div>`,
})
export class FilterHeaderBlock extends BaseComponent implements OnDestroy
{
@Output("change") changeEvent : EventEmitter<any> = new EventEmitter();
public whereFilters : any = new WhereCollection();
public _where  		: any;
public _app  		: any;
public _acl  		: any;	
public dataFilters  : any;
public labels       : any;		

constructor(
public popoverController : PopoverController,
public filterPlugin      : FilterPlugin,
public changeDetectorRef : ChangeDetectorRef
)
{
    super();
}

@Input()
public filters : any;

@Input()
set where(value:any)
{
    this._where = value;		
}

get where()
{
    return this._where;
}

@Input()
set app(value:any)
{
    this._app = value;
}

get app()
{
    return this._app;
}

@Input()
set acl(value:any)
{
    this._acl = value;	

    this.markForCheck();
}

get acl()
{
    return this._acl;
}

async getFilters()
{
/* LOAD FILTER */
    const params = this.createParams({
        appid  	: this.app.code,
        orderBy : "name",
    });				

    params.where = new WhereCollection();

    const values = [ {
        id    : "all",
        label : "Todos",
        value : "all",
    } ];

    if (this.core().user)
    {
        values.push(this.core().user.getGroup());
    }

    params.where.set({
        field    : "groups",
        operator : "array-contains-any",
        value    : values,
        type   	 : FieldType.type("MultiSelect"),
    });

    const result = await this.filterPlugin.getData(params);
    this.filters = result.collection;

    this.markForCheck();
}

async openFilter(form:any)
{
    form.setting = {
        addText    : "Filtrar",
        setText    : "Filtrar",
        cancelText : "Limpar",
    };

    const popover = await this.popoverController.create(
        {
            component      : FormFormPopover,
            componentProps : {
                data   : this.dataFilters,
                form   : form,
                onSave : async (event:any) =>
                {
                    console.log("loadFilters", event.data);

                    this.where.delItems(this.whereFilters);
                    this.where.enabledAll();															

                    this.whereFilters = new WhereCollection();							

                    for (const key in form.items)
                    {
                        const formItem = form.items[key];
                        const noFilter = formItem.setting && formItem.setting.noFilter;
                        const noZero   = formItem.setting && formItem.setting.noZero;
                        const noEmpty  = formItem.setting && formItem.setting.noEmpty;
                        const model    = new Document(event.data);
                        let value      = await model.getProperty(formItem.name); 

                        /* POR CONTA DE BUSCAS OR */
                        if (value instanceof Array && value.length == 0)
                        {
                            value = null;					
                        }
                        else if (noZero && value === 0) 
                        {
                            value = null;
                        }
                        else if (noEmpty && (value === "" || value === false))
                        {
                            value = null;
                        }

                        if (!noFilter && value !== null)
                        {
                            const where = {
                                id       : formItem.id,
                                label    : formItem.label,
                                name     : formItem.name,
                                field    : formItem.path,
                                operator : formItem.setting.operator,
                                order    : (formItem.setting.order ? formItem.setting.order : parseInt(key) + 100),
                                value    : value,
                                type     : formItem.field.type,
                                setting  : formItem.setting
                            };

                            this.whereFilters.set(where);
                        }
                        else
                        {
                            for (const item of this.where)
                            {
                                if (item.field == formItem.path)
                                {
                                    item.enabled = false;
                                }
                            }
                        }
                    }

                    this.where.setItems(this.whereFilters);
                    this.dataFilters = new Document(event.data);

                    this.labels = await this.where.getLabels();

                    //this.load();
                    this.changeEvent.emit();

                    popover.dismiss();
                    this.markForCheck();
                },
                onCancel : () => 
                {
                    this.clearFilter();

                    //this.load();
                    this.changeEvent.emit();

                    popover.dismiss();
                    this.markForCheck();
                },
                onClose : () => 
                {
                    popover.dismiss();
                },
            },
            showBackdrop : false,
            cssClass     : "right-popover",
        });

    popover.present();
}

clearFilter()
{
    if (this.where)
    {
        this.where.delItems(this.whereFilters);
        this.where.enabledAll();
    }

    this.whereFilters = new WhereCollection();
    this.dataFilters  = null;	
    this.labels       = [];	

    this.changeEvent.emit();
}

async goListFilter(event:any) 
{
    if (!this.filters)
    {
        await this.getFilters();
    }

    if (this.filters.length == 1)
    {
        this.openFilter(this.filters[0]);
    }
    else
    {
        this.popoverController.create(
            {
                component : ListPopover,
                event     : event,
                componentProps :
{
    title    : "Filtros",
    items    : this.filters,
    onSelect : ((form:any) =>
    {
        this.popoverController.dismiss();
        this.openFilter(form);
    })
},
                showBackdrop : false,
            })
.then(popover =>
{
    popover.present();
});
    }
}

markForCheck()
{
    this.changeDetectorRef.markForCheck();
}	

ngOnDestroy()
{
    this.clearFilter();
}
}
