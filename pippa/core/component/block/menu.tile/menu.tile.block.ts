import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

/* PIPPA */
import { BaseComponent } from '../../../base/base.component';

@Component({
	selector 		: 'menu-tile-block',
	changeDetection : ChangeDetectionStrategy.OnPush,
	template 		: `<ul class="menu-tile-flat" *ngIf="flat">
							<li *ngFor="let item of items" class="{{'item-' + item.id}}">
								<a menuClose (click)="goMenu(item)">
									<ion-icon [name]="item?.icon?.value"></ion-icon> 
									<p>{{item?.name | translate}}</p>
								</a>
							</li> 
					   </ul>
					   <ul class="menu-tile" *ngIf="!flat">
					   		<li class="item-tile" *ngFor="let item of items">
							   	<label>{{item?.name | translate}}</label>
								<ul class="sub-menu-tile">
								   <li *ngFor="let item2 of item._children" class="{{'item-' + item2.id}}">
									   <a menuClose (click)="goMenu(item2)">
										   <ion-icon [name]="item2?.icon?.value"></ion-icon> 
										   <p>{{item2?.name | translate}}</p>
									   </a>                        
								   </li>
							   </ul>
							</li>					   		
					   </ul>`,
})
export class MenuTileBlock extends BaseComponent
{
	public _menu : any;    
    public items : Array<any>;
	public unsubscribe;  

	@Input()
	public flat : boolean = true;

	constructor(
        public changeDetectorRef : ChangeDetectorRef
    )
    {
		super()
    }

	@Input()
    set menu(value:any)
    {
        if(value)
        {
			if(this.flat)
			{
				value = value.toFlat();	
			}			

            this.doUnsubscribe();

            this.unsubscribe = value.onUpdate().subscribe((value2:any) => 
            {
                this.menu = value2;
            });

            const items = [];

            for(const menu of value)
            {
				if(this.flat || (!this.flat && menu._children && menu._children.length > 0))
				{
					if(menu.slots)
					{
						let has : boolean = false;
	
						for(const key2 in menu.slots)
						{
							if(menu.slots[key2].value == 'tile')
							{
								has = true;
							}
						}
	
						if(has)
						{
							items.push(menu);
						}
					}
					else if(menu.slot)
					{
						if(menu.slot.value == 'tile')
						{
							items.push(menu);
						}
					}
					else
					{
						items.push(menu);
					}
				}				
            }

            this.items = items;
        }        

		this._menu = value;
		
		this.changeDetectorRef.markForCheck();
    }

    get menu()
    {
        return this._menu;
    }

    doUnsubscribe()
    {
        if(this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
	}
	
    goMenu(menu:any)
    {
        this.core().goMenu(menu);
    }    
}
