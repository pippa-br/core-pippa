import { Component, Input } from '@angular/core';

/* PIPPA */
import { BaseComponent } from '../../../base/base.component';
import { EventPlugin }   from '../../../plugin/event/event.plugin';

@Component({
    selector : 'user-profile-block',
    template : `<a (click)="goProfile()">
                    <label>
						{{user?.name}}
						<small>{{core().versionApp}}<br>{{core().timezone.label}}</small>
                    </label>
                    <div class="avatar">
                        <img [src]="photo">
                    </div>
                </a>`,
})
export class UserProfileBlock extends BaseComponent
{
    public _user : any;
    public photo : string = "./assets/img/avatar.png";

    goProfile()
    {
        if(this.user.isGuest())
        {
            const eventPlugin = this.core().injector.get(EventPlugin);
            eventPlugin.dispatch('no-login');
        }   
        else
        {
            this.push('auth-viewer-profile', { appid : this.user.appid});
        }     
    }

    @Input('user') 
    set user(user:any)
    {
        this._user = user;
        
        if(!user)
        {

        }
        else if(user.photo)
        {
            this.photo = user.photo._url;
        }
        else if(user.avatar)
        {
            this.photo = user.avatar._url;
        }
    };

    get user()
    {
        return this._user;
    };
}
