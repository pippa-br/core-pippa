import { Component, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;

/* PIPPA */
import { BaseComponent } from '../../../base/base.component';

@Component({
    selector : 'menu-nav-block',
    template : `<ion-header>
                    <ion-toolbar>
                        <a (click)="goFirstMenu()">
                            <img class="logo" *ngIf="core().account?.logo" [src]="core().account?.logo._url"/>
							<label *ngIf="!core().account?.logo">{{core().account?.name}}</label>
                        </a>
                    </ion-toolbar>
                </ion-header>

                <ion-content no-bounce shadowCss=".inner-scroll{ overflow: visible !important; }">

					<!-- ALERT SOUND --->
					<a class="alert-sound" *ngIf="core().account?.show_alert" (click)="openAlert()">
						<ion-icon name="notifications-outline"></ion-icon>
						<ion-badge *ngIf="core().account.alerts && core().account.alerts.length > 0" color="warning">{{core().account.alerts.length}}</ion-badge>
					</a>

					<!--- POR CAUSA DO MOBILE --->
					<user-profile-block *ngIf="core().masterAccount?.show_user_profile" [user]="core().user"></user-profile-block>

				    <ul class="menu-list">
                        <li *ngFor="let menu of items" class="{{'item-' + menu.id}}" [ngClass]="{'children' : menu._children && menu._children.length > 0}">
                            <a menuClose (click)="goMenu(menu)">
                                <ion-icon *ngIf="menu.icon && menu.icon.value" name="{{menu.icon.value}}" class="icon icon-md item-icon"></ion-icon>
                                <img *ngIf="menu.icon && menu.icon._url" src="{{menu.icon._url}}"/>
                                <span>{{menu?.name | translate}}</span>
                            </a>
                            <ul *ngIf="menu._children">
                                <li *ngFor="let subMenu of menu._children" class="{{'item-' + subMenu.id + (subMenu.separator ? ' separator' : '' )}}">
                                    <a menuClose (click)="goMenu(subMenu)" (contextmenu)="newTab(subMenu)">
                                        <ion-icon *ngIf="subMenu.icon && subMenu.icon.value" name="{{subMenu.icon.value}}" class="icon icon-md item-icon"></ion-icon>
                                        <img *ngIf="subMenu.icon && subMenu.icon._url" src="{{subMenu.icon._url}}"/>
                                        <span>{{subMenu?.name | translate}}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>                        
                        <li *ngIf="core().account?.show_language">
                            <a menuClose (click)="onLanguage()">
                                <ion-icon name="globe-outline"></ion-icon> {{core()?.currentLanguage}}
                            </a>
                        </li>
						<li *ngIf="hasProfile && !core().accountProfile && core().user && !core().user.isGuest()">
                            <a menuClose (click)="logout()">
                                <ion-icon name="power-outline"></ion-icon> {{'Sair' | translate }}
                            </a>
                        </li>
                        <li *ngIf="hasProfile && core().accountProfile">
                            <a menuClose (click)="logout()">
                                <ion-icon name="arrow-back-outline"></ion-icon> {{'Voltar' | translate }}
                            </a>
						</li>
						<li *ngIf="core().solved" class="solved-wrapper">
                            <small class="solved">
                                <a target="_blank" href="{{core().solved.url}}">{{core().solved.name}}</a> {{core().versionApp}}<br>{{core().timezone.label}}
                            </small>
                        </li>
					</ul>									

				</ion-content>
				<ion-footer *ngIf="core()?.masterAccount?.network">
					<ion-toolbar>
						<ion-buttons slot="start">
							<ion-button *ngIf="core()?.masterAccount?.network?.facebook" (click)="openUrl(core().masterAccount.network.facebook)">
								<ion-icon name="logo-facebook"></ion-icon>
							</ion-button>
							<ion-button *ngIf="core()?.masterAccount?.network?.youtube" (click)="openUrl(core().masterAccount.network.youtube)">
								<ion-icon name="logo-youtube"></ion-icon>
							</ion-button>
							<ion-button *ngIf="core()?.masterAccount?.network?.instagram" (click)="openUrl(core().masterAccount.network.instagram)">
								<ion-icon name="logo-instagram"></ion-icon>
							</ion-button>
							<ion-button *ngIf="core()?.masterAccount?.network?.twitter" (click)="openUrl(core().masterAccount.network.twitter)">
								<ion-icon name="logo-twitter"></ion-icon>
							</ion-button>
							<ion-button *ngIf="core()?.masterAccount?.network?.website" (click)="openUrl(core().masterAccount.network.website)">
								<ion-icon name="globe-outline"></ion-icon>
							</ion-button>
						</ion-buttons>					
  					</ion-toolbar>
				</ion-footer>`,
})
export class MenuNavBlock extends BaseComponent implements OnDestroy
{
    @Output('logout') logoutEvent  : EventEmitter<any> = new EventEmitter();
	@Input() hasProfile : boolean = true;

    public _menu  : any;    
    public items : Array<any>;    
    public unsubscribe;    

    @Input()
    set menu(value:any)
    {
        if(value)
        {
            this.doUnsubscribe();

            this.unsubscribe = value.onUpdate().subscribe((value2:any) => 
            {
                this.menu = value2;
            });

            const items = [];

            for(const key in value)
            {
                const menu = value[key];

				if(menu.slots)
                {
					let has : boolean = false;

					for(const key2 in menu.slots)
					{
						if(menu.slots[key2].value == 'nav')
						{
							has = true;
						}
					}

					if(has)
					{
						items.push(menu);
					}
                }
                else if(menu.slot)
                {
                    if(menu.slot.value == 'nav')
                    {
                        items.push(menu);
                    }
				}				
                else
                {
                    items.push(menu);
                }
            }

            this.items = items;
        }        

        this._menu = value;
    }

    get menu()
    {
        return this._menu;
    }

    doUnsubscribe()
    {
        if(this.unsubscribe)
        {
            this.unsubscribe.unsubscribe();
        }
	}
	
	openAlert()
	{
		/* LIMPA OS ALERTAS */
		this.core().account.set({
			alerts : [],
		});
	}

    goMenu(menu:any)
    {
        this.core().goMenu(menu);
    }

    newTab(menu:any)
    {
        menu.newTab = true;
        this.core().goMenu(menu);
    }

    logout()
    {
        this.logoutEvent.emit(event);
    }

	async openUrl(link:any)
	{
		await Browser.open({ url: link });
	}

	async onLanguage()
	{
		// LIMPA O IDIOMA SELEIONADOs
		await this.core().storagePlugin.del('menu_appid_' + this.core().platform.value);

		this.push('auth-loading');
	}

    ngOnDestroy()
    {
        this.doUnsubscribe();
    }
}
