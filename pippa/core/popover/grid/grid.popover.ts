import { Component } from "@angular/core";

/* PIPPA */
import { BaseFormModal } from "../../base/form.modal/base.form.modal";
@Component({
    selector : ".form-form-popover",
    template : `<ion-header>
									<ion-toolbar>
										<ion-title>
											{{title}}
										</ion-title>
										<ion-buttons slot="end" (click)="onClose()">
											<ion-button icon-only>
												<ion-icon name="close-outline"></ion-icon>
											</ion-button>
										</ion-buttons>
									</ion-toolbar>
								</ion-header>
								<div dynamic-list
										[grid]="grid"
										(viewer)="onViewer($event)"
										[collection]="collection">
								</div>`
})
export class GridPopover extends BaseFormModal
{
    public title      : string;
    public grid 	  : any;
    public onViewer   : any;
}
