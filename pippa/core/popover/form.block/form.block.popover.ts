import { Component, OnInit } from '@angular/core';

/* PIPPA */
import { BaseFormModal }      from '../../base/form.modal/base.form.modal';
import { Block }              from '../../model/block/block';
import { Field }              from '../../model/field/field';
import { FormItem }           from '../../model/form.item/form.item';
import { FormItemCollection } from '../../model/form.item/form.item.collection';
import { BlockType }          from '../../type/block/block.type';
import { FieldType }          from '../../type/field/field.type';
import { FormType }           from '../../type/form/form.type';
import { SlotType }           from '../../type/slot/slot.type';
import { Form }               from '../../model/form/form';

@Component({
    selector : 'form-block-popover',
    template : `<form dynamic-form
                      [form]="form"
                      [data]="data"
                      [acl]="acl"
                      (add)="onAdd($event)"
                      (set)="onSet($event)"
                      (close)="onClose()"
                      class="block-form">
              </form>`
})
export class FormBlockPopover extends BaseFormModal implements OnInit
{
    public _type     : any;
    public formItems : any;
    public option    : any;

    ngOnInit()
    {
        this.formItems = new FormItemCollection();

        this.form = new Form({
            name  : '',
            type  : FormType.FLAT_TYPE,
        });

        this.formItems.set({
            id    : '_type',
            row   : 0,
            col   : 0,
            field : new Field({
                id          : '_type',
                type        : FieldType.type('Select'),
                name        : '_type',
                label       : 'Tipo',
                placeholder : 'Tipo',
                clearable   : false,
                option      : this.option,
                setting : {
                    bindValue  : 'value',
                    searchable : true,
                },
                onChange : (event:any) =>
                {
                    if(event.data)
                    {
                        /* ATUALIZA O DATA QUANDO HOUVER ALTERAÇÃO DO TYPE */
                        if(this.data)
                        {
                            const data : any = new Block(this.data.parseData());
                            data._type = event.data;
                            this.data  = data;
                        }

                        this._type = event.data;
                        this.createFields();
                    }
                },
            })
        });

        if(this.data)
        {
            //this.data.on(true).then(() =>
            this.data.on().then(() =>
            {
                this._type = this.data._type;
                //this.form.doSetModeForm();
                this.createFields();
            });
        }
        else
        {
            this.createFields();
        }
    }

    set data(value:any)
    {
        if(value)
        {
            this._data = value;
        }
    }

    get data():any
    {
        return this._data;
    }

    createFields()
    {
        if(this._type)
        {
            console.log('seleced type', this._type);

            this.formItems.set({
                id  : '_label',
                row : 1,
                col : 1,
                field : new Field({
                    id          : '_label',
                    type        : FieldType.type('Text'),
                    name        : '_label',
                    label       : 'Label',
                    placeholder : 'Label',
                })
            });         

            this.formItems.set(new FormItem({
                id  : '_setting',
                row : 6,
                col : 1,
                field : new Field({
                    id          : '_setting',
                    type        : FieldType.type('ObjectInput'),
                    name        : '_setting',
                    label       : 'Configurações',
                    required    : false,
                    viewOptions : true,
                })
            }));
        }

        this.formItems.doSort();

        this.formItems.on(true).then(() =>
        {
            console.log('create items');

            this.form._fullItems.updateItems(this.formItems);
            this.form.items.updateItems(this.formItems);
        });
    }
}
