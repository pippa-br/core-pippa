import { Component } from "@angular/core";

/* PIPPA */
import { BaseModal } from "../../base/modal/base.modal";

@Component({
    selector : "list-popover",
    template : `<ion-list *ngIf="items">
                    <ion-list-header *ngIf="title">
                        <ion-label>
                            {{title}}
                        </ion-label>
                    </ion-list-header>
                    <ion-item class="button" *ngFor="let item of items" (click)="onClick(item)">
                        <ion-button expand="block" fill="clear" size="small" color="dark">
                            <ion-icon *ngIf="item.icon" name="{{item.icon.value}}">
                            </ion-icon> <span>{{item.name || item.label}}</span>
                        </ion-button>
                    </ion-item>
                </ion-list>`
})
export class ListPopover extends BaseModal
{
    public title     = "";
    public items    : Array<any>;
    public onSelect : any; /* POR CAUSA DO DAILY DO POPOVER */

    onClick(item:any)
    {
        this.onSelect(item);
    }
}
