import { Component, ElementRef, ViewChild } from "@angular/core";

/* PIPPA */
import { BaseFormModal } from "../../base/form.modal/base.form.modal";
import { FormType } from "../../type/form/form.type";

@Component({
    selector    : ".form-form-popover",
    templateUrl : "form.form.popover.html"
})
export class FormFormPopover extends BaseFormModal
{
    @ViewChild("footer", { read : ElementRef }) public footer : ElementRef;

    onRendererComplete(event)
    {   
        if(this.footer)
        {
            const childElements = this.footer.nativeElement.childNodes;

            for (const child of childElements) 
            {
                this.footer.nativeElement.removeChild(child);
            }
            
            this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)
        }       
    }

    hasFooter()
    {
        return this.form && this.form.type && this.form.type.value != FormType.EXPAND_TYPE.value;
    }
}
