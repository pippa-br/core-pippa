import { Component, OnInit } from "@angular/core";

/* PIPPA */
import { BaseFormModal }      from "../../base/form.modal/base.form.modal";
import { Field }              from "../../../core/model/field/field";
import { FormItem }           from "../../../core/model/form.item/form.item";
import { FormItemCollection } from "../../../core/model/form.item/form.item.collection";
import { FieldType }          from "../../../core/type/field/field.type";
import { FormType }           from "../../type/form/form.type";
import { SlotType }           from "../../../core/type/slot/slot.type";
import { Form }               from "../../../core/model/form/form";

@Component({
    selector : "form-item-popover",
    template : `<form dynamic-form
                      [form]="form"
                      [data]="data"
                      [acl]="acl"
                      (set)="_onSave($event)"
                      (close)="onClose()"
                      class="field-form">
              </form>`
})
export class FormItemPopover extends BaseFormModal implements OnInit
{
    public items  	   : any; /* ITEMS LOCAIS */
    public option 	   : any; /* OPTIONS DE FORMITEMS */
    public extraFields : Array<string>;
    public formItem    : any;

    _onSave(event:any)
    {
        if (this.formItem)
        {
            event.data.field.type = this.formItem.field.type
        }        

        this.data.populate(event.data)
        this.onSet({ data : this.data });
    }

    ngOnInit()
    {
        this.items       = new FormItemCollection();
        this.extraFields = [];

        this.form = new Form({
            name : "",
            type : FormType.SEGMENT_TYPE,
        });

        if (this.option && this.option.items.length > 0)
        {
            console.log("options", this.option.items);

            // PARA FAZER BIND NO SELECT
            for(const item of this.option.items)
            {
                item.value = item.id;
            }

            this.items.set({
                id    : "selector",
                row   : 0,
                col   : 0,
                field : new Field({
                    id          : "selector",
                    type        : FieldType.type("Select"),
                    name        : "selector",
                    label       : "FormItem",
                    placeholder : "FormItem",
                    required    : false,
                    clearable   : false,
                    option      : {
                        record : false,
                        items  : this.option.items
                    },
                    setting : {
                        bindValue  : "value",
                        bindLabel  : "_label",
                        searchable : true,
                    },
                    onInput : async (event:any) =>
                    {
                        if (event.data)
                        {                            
                            if (this.data)
                            {       
                                // this.data.populate(event.data);
                                // this.createFields();
                            }
                            else
                            {
                                event.data.id = this.core().util.randomString(20);

                                const data : any = new FormItem(event.data);
	
                                await data.on();

                                this.data = data;
                                this.createFields();
                            }

                            this.data.selector = this.getSelectorByType(this.data._field.type);
                            this.formItem      = event.data;
                        }                        
                    },
                })
            });

            if (this.data)
            {
                this.data.selector = this.getSelectorByType(this.data._field.type);
            }

            this.createFields();
        }
    }

    getSelectorByType(type:any)
    {
        for (const item of this.option.items)
        {
            if (item.field.type.value == type.value)
            {
                return item;
            }
        }
    }

    set data(value:any)
    {
        if (value)
        {
            this._data = value;
        }
    }

    get data():any
    {
        return this._data;
    }

    createFields()
    {
        /* EXTRA FIELDS */
        for (const key in this.extraFields)
        {
            this.items.del({
                id : this.extraFields[key]
            });
        }
		
        this.extraFields = [];
		
        if (this.data)
        {
            console.log("seleced field", this.data.field);

            this.items.set({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "field",
                row   : 1,
                col   : 0,
                field : new Field({
                    id      : "field",
                    type    : FieldType.type("Hidden"),
                    name    : "field",
                    initial : this.data.field.getData(),
                })
            });

            this.items.set({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_label",
                row   : 1,
                col   : 1,
                field : new Field({
                    id          : "_label",
                    type        : FieldType.type("Text"),
                    name        : "_label",
                    label       : "Label",
                    placeholder : "Label",
                    initial     : this.data.label,
                    required    : false,
                })
            });
			
            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_name",
                row   : 2,
                col   : 1,
                field : new Field({
                    id          : "_name",
                    type        : FieldType.type("Text"),
                    name        : "_name",
                    label       : "Nome",
                    placeholder : "Nome",
                    initial     : this.data.name,
                    required    : false,
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_path",
                row   : 3,
                col   : 1,
                field : new Field({
                    id          : "_path",
                    type        : FieldType.type("Text"),
                    name        : "_path",
                    label       : "Path",
                    placeholder : "Path",
                    initial     : this.data.name,
                    required    : false,
                })
            }));		
            
            const optionType : any = FieldType.optionType(this.data.field.type.value);
            const setting    : any = FieldType.setting(this.data.field.type.value);

            if (optionType)
            {
                this.extraFields.push("_option");

                this.items.set(new FormItem({
                    step : {
                        id    : 1,
                        label : "Geral"
                    },
                    id    : "_option",
                    row   : 4,
                    col   : 1,
                    items : (this.data ? this.data.items : []),
                    field : new Field({
                        id          : "_option",
                        type        : FieldType.type(optionType),
                        name        : "_option",
                        label       : "Opções",
                        required    : false,
                        viewOptions : true,
                        setting     : setting,
                    })
                }));
            }

            const hasGrid : any = FieldType.hasGrid(this.data.field.type.value);

            if (hasGrid)
            {
                this.extraFields.push("_grid");

                this.items.set(new FormItem({
                    step : {
                        id    : 1,
                        label : "Geral"
                    },
                    id    : "_grid",
                    row   : 5,
                    col   : 1,
                    field : new Field({
                        id       : "_grid",
                        type     : FieldType.type("GridSetting"),
                        name     : "_grid",
                        label    : "Grid",
                        required : false,
                    })
                }));
            }
			
            const hasFilter : any = FieldType.hasFilter(this.data.field.type.value);

            /* FILTERS */
            if (hasFilter)
            {
                this.extraFields.push("_filters");

                this.items.set(new FormItem({
                    id    : "_filters",					
                    row   : 6,
                    col   : 1,
                    field : new Field({
                        type        : FieldType.type("SubForm"),
                        name        : "_filters",
                        label       : "Filtros",
                        placeholder : "Filtros",
                        required    : false,                            
                    }),
                    setting : {
                        //showNoItems : false, 
                    },
                    grid : {
                        items : [
                            {
                                row   : 1,
                                col   : 1,
                                field : new Field({
                                    type        : FieldType.type("MultiSelect"),
                                    name        : "_groups",
                                    label       : "Grupo de Permissão",
                                    placeholder : "Grupo de Permissão",
                                    option      : {
                                        record : false,
                                        items  : this.core().optionPermission.items
                                    },										
                                })
                            },
                        ]
                    },
                    items : FieldType.getFilterItems()
                }));
            }

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_hasLabel",
                row   : 7,
                col   : 1,
                field : new Field({
                    id          : "_hasLabel",
                    type        : FieldType.type("Checkbox"),
                    name        : "_hasLabel",
                    label       : "Visualizar Label",
                    placeholder : "Visualizar Label",
                    required    : false,
                    initial     : true,
                })
            }));
			
            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_required",
                row   : 8,
                col   : 1,
                field : new Field({
                    id          : "_required",
                    type        : FieldType.type("Checkbox"),
                    name        : "_required",
                    label       : "Requerido",
                    placeholder : "Requerido",
                    required    : false,
                    initial     : true,
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_sort",
                row   : 9,
                col   : 1,
                field : new Field({
                    id          : "_sort",
                    type        : FieldType.type("Checkbox"),
                    name        : "_sort",
                    label       : "Ordenar",
                    placeholder : "Ordenar",
                    required    : false,
                    initial     : false,
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_editable",
                row   : 10,
                col   : 1,
                field : new Field({
                    id          : "_editable",
                    type        : FieldType.type("Checkbox"),
                    name        : "_editable",
                    label       : "Editável",
                    placeholder : "Editável",
                    required    : false,
                    initial     : true,
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_initial",
                row   : 11,
                col   : 1,
                field : new Field({
                    id          : "_initial",
                    type        : FieldType.type("Text"),
                    name        : "_initial",
                    label       : "Inicial",
                    placeholder : "Inicial",
                    required    : false,
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "__groups",
                row   : 12,
                col   : 1,
                field : new Field({
                    id          : "__groups",
                    type        : FieldType.type("MultiSelect"),
                    name        : "__groups",
                    label       : "Grupos",
                    placeholder : "Grupos",
                    required    : false,
                    option      : {
                        record : false,
                        items  : this.core().optionPermission.items
                    },
                })
            }));

            const slots : any = [
                SlotType.TIILE,
                SlotType.SUBTITLE,
                SlotType.BEFORE_CONTENT,
                SlotType.CONTENT,
                SlotType.AFTER_CONTENT,
                SlotType.FOOTER,
            ];

            slots[3].selected = true;

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_slot",
                row   : 13,
                col   : 1,
                field : new Field({
                    id          : "_slot",
                    type        : FieldType.type("Select"),
                    name        : "_slot",
                    label       : "Slot",
                    placeholder : "Slot",
                    required    : false,
                    option      : {
                        record : false,
                        items  : slots
                    }
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 1,
                    label : "Geral"
                },
                id    : "_setting",
                row   : 14,
                col   : 1,
                field : new Field({
                    id          : "_setting",
                    type        : FieldType.type("ObjectInput"),
                    name        : "_setting",
                    label       : "Configurações",
                    required    : false,
                    viewOptions : true,
                })
            }));
			
            this.items.set(new FormItem({
                step : {
                    id    : 2,
                    label : "Click"
                },
                row   : 15,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("Select"),
                    name        : "router",
                    label       : "Router",
                    placeholder : "Router",
                    required    : false,
                    setting     : {
                        optionID : "routes"
                    }
                })
            }));

            this.items.set(new FormItem({
                step : {
                    id    : 2,
                    label : "Click"
                },
                row   : 16,
                col   : 1,
                field : new Field({
                    type        : FieldType.type("ObjectInput"),
                    name        : "params",
                    label       : "Params",
                    placeholder : "Params",
                    required    : false,
                })
            }));
        }

        this.items.doSort();

        this.items.on(true).then(() =>
        {
            console.log("create items"); 

            //this.form._fullItems.updateItems(this.items);
            this.form.items.updateItems(this.items);
        });
    }
}
