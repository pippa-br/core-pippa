import { Component, OnInit } from '@angular/core';

/* PIPPA */
import { BaseModal }  from '../../base/modal/base.modal';

@Component({
    template : `<ion-list>
                    <ion-item class="button" *ngFor="let attachment of field.typeAttachments; trackBy: trackById"
                              (click)="onClick(attachment)">
                        <ion-icon name="{{attachment.icon}}"></ion-icon> &nbsp;{{attachment.label}}
                    </ion-item>
                </ion-list>`
})
export class AttachmentPopover extends BaseModal implements OnInit
{
    public field   : any;
    public onInput : any; /* POR CAUSA DO DAILY DO POPOVER */

    constructor(
    )
    {
        super();
    }

    ngOnInit()
    {
    }

    onClick(item:any)
    {
        this.onInput(item);
        //this.viewController.dismiss();
    }
}
