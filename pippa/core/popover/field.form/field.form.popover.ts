import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

/* PIPPA */
import { BaseFormModal }      from '../../base/form.modal/base.form.modal';
import { Field }              from '../../model/field/field';
import { FormItem }           from '../../model/form.item/form.item';
import { FormItemCollection } from '../../model/form.item/form.item.collection';
import { FieldType }          from '../../type/field/field.type';
import { FormType }           from '../../type/form/form.type';
import { AttachmentType }     from '../../type/attachment/attachment.type';
import { Form }               from '../../model/form/form';

@Component({
    selector    : 'field-form-popover',
    templateUrl : `field.form.popover.html`
    // template : `<form dynamic-form
    //                   [form]="form"
    //                   [data]="data"
    //                   [acl]="acl"
    //                   (add)="onAdd($event)"
    //                   (set)="onSet($event)"
    //                   (close)="onClose()"
    //                   class="field-form">
    //           </form>`
})
export class FieldFormPopover extends BaseFormModal implements OnInit
{
    @ViewChild('footer', { read: ElementRef }) public footer : ElementRef;

    public _field      : any;
    public _formItem   : any;
    public type        : any;
    public items       : any;
    public extraFields : Array<string>;

    onRendererComplete(event)
    {   
        const childElements = this.footer.nativeElement.childNodes;

        for(const child of childElements) 
        {
            this.footer.nativeElement.removeChild(child);
        }

        this.footer.nativeElement.appendChild(event.footerViewer.nativeElement)      
    }

    ngOnInit()
    {
        this.items       = new FormItemCollection();
        this.extraFields = [];

        this.form = new Form({
            name  : this.field.label,
            type  : FormType.FLAT_TYPE,
        });

        if(this.field && this.field.option && this.field.option.items)
        {
            if(this.field.option.items.length == 1)
            {
                this.type = this.field.option.items[0];

                this.items.set({
                    id    : 'type',
                    row   : 1,
                    col   : 0,
                    field : new Field({
                        id      : 'type',
                        type    : FieldType.type('Hidden'),
                        name    : 'type',
                        initial : this.type,
                        onInit  : () =>
                        {
                        }
                    })
                });

                this.createFields();
            }
            else if(this.field.option.items.length > 1)
            {
                this.items.set({
                    id    : 'type',
                    row   : 0,
                    col   : 0,
                    field : new Field({
                        id          : 'type',
                        type        : FieldType.type('Select'),
                        name        : 'type',
                        label       : 'Adicionar novo campo',
                        placeholder : 'Adicionar novo campo',
                        clearable   : false,
                        option      : this.field.option,
                        setting     : {
                            searchable : true
                        },
                        onInput : (event:any) =>
                        {
                            if(event.data)
                            {
                                /* ATUALIZA O DATA QUANDO HOUVER ALTERAÇÃO DO TYPE */
                                if(this.data)
                                {
                                    const data : any = new Field(this.data.parseData());
                                    this.data.type   = event.data;
                                    this.data  		 = data;
                                }

                                this.type = event.data;
                                this.createFields();
                            }
                        },
                    })
                });

                if(this.data)
                {
                    //this.data.on(true).then(() =>
                    //this.data.on().then(() =>
                    //{
                        this.type = this.data.type;
                        //this.form.doSetModeForm();
                        this.createFields();
                    //});
                }
                else
                {
                    this.createFields();
                }
            }
        }
    }

    set field(value)
    {
        if(value)
        {
            this._field = value;
        }
    }

    get field()
    {
        return this._field;
    }

    set data(value:any)
    {
        if(value)
        {
            this._data = value;
        }
    }

    get data():any
    {
        return this._data;
    }

    set formItem(value:any)
    {
        if(value)
        {
            this._formItem = value;
        }
    }

    get formItem():any
    {
        return this._formItem;
    }

    createFields()
    {
        /* EXTRA FIELDS */
        for(const key in this.extraFields)
        {
            this.items.del({
                id : this.extraFields[key]
            });
        }

        this.extraFields = [];

        if(this.type)
        {
            console.log('seleced type', this.type);

            this.items.set({
                id  : 'label',
                row : 1,
                col : 1,
                field : new Field({
                    id          : 'label',
                    type        : FieldType.type('TEXT'),
                    name        : 'label',
                    label       : 'Label',
                    placeholder : 'Label',
                })
            });

            this.items.set(new FormItem({
                id  : 'name',
                row : 2,
                col : 1,
                field : new Field({
                    id          : 'name',
                    type        : FieldType.type('ALNUM'),
                    name        : 'name',
                    label       : 'ID',
                    placeholder : 'ID',
                    initial     : this.core().util.randomString(20),
                })
            }));            							

			const optionType  : any = FieldType.optionType(this.type.value);
			const optionsType : any = FieldType.optionsType(this.type.value);
			  let setting     : any = FieldType.setting(this.type.value);

			if(optionType || optionsType)
			{
				const nameOptionType = optionType ? 'option' : 'options';

				this.extraFields.push(nameOptionType);

				// MERGER SETTING DO TYPE ADICIONADO NO CAMPO MASTER VIA BANCO
				if(this.type.setting)
				{
					setting = Object.assign({}, this.type.setting, setting);
				}

				this.items.set(new FormItem({
					id    : nameOptionType,
					row   : 3,
					col   : 1,
					items : (this.formItem ? this.formItem.items : []),
					field : new Field({
						id          : nameOptionType,
						type        : FieldType.type((optionType ? optionType : optionsType)),
						name        : nameOptionType,
						label       : 'Opções',
						required    : false,
						viewOptions : true,
						setting     : setting,
						//setting     : this.type.setting, /* PASSADO NA CONFIGURAÇÕO DOS CAMPOS NO MASTER */
					})
				}));
			}

			if(this.acl.hasKey('form_advanced_input'))
            {
				const hasGrid : any = FieldType.hasGrid(this.type.value);

				if(hasGrid)
				{
					this.extraFields.push('grid');

					this.items.set(new FormItem({
						id    : 'grid',
						row   : 4,
						col   : 1,
						field : new Field({
							id          : 'grid',
							type        : FieldType.type('GridSetting'),
							name        : 'grid',
							label       : 'Grid',
							required    : false,
						})
					}));
				}

				const hasFilter : any = FieldType.hasFilter(this.type.value);

				/* FILTERS */
				if(hasFilter)
				{
					this.extraFields.push('filters');

					this.items.set(new FormItem({
						id  : 'filters',
						row : 5,
						col : 1,
						field : new Field({
							type        : FieldType.type('SubForm'),
							name        : 'filters',
							label       : 'Filtros',
							placeholder : 'Filtros',
							required    : false,                            
						}),
						setting : {
							showNoItems : true,
						},
						grid : {
							items : [
								{
									row : 1,
									col : 1,
									field : new Field({
										type        : FieldType.type('Text'),
										name        : 'label',
										label       : 'Label',
										placeholder : 'Label',
									})
								},
								{
									row : 1,
									col : 1,
									field : new Field({
										type        : FieldType.type('MultiSelect'),
										name        : '_groups',
										label       : 'Grupo de Permissão',
										placeholder : 'Grupo de Permissão',
										option      : {
											record : false,
											items  : this.core().optionPermission.items
										},										
									})
								},
							]
						},
						items : FieldType.getFilterItems()
					}));
				}

				this.items.set(new FormItem({
					id  : 'setting',
					row : 6,
					col : 1,
					field : new Field({
						id          : 'setting',
						type        : FieldType.type('ObjectInput'),
						name        : 'setting',
						label       : 'Configurações',
						required    : false,
						viewOptions : true,
						setting     : this.type.setting, /* PASSADO NA CONFIGURAÇÕO DOS CAMPOS NO MASTER */
					})
				}));
				
				this.items.set(new FormItem({
					id  : 'typeAttachments',
					row : 7,
					col : 1,
					field : new Field({
						id          : 'typeAttachments',
						type        : FieldType.type('MultiSelect'),
						name        : 'typeAttachments',
						label       : 'Anexos',
						required    : false,
						option      : {
							record : false,
							items : [
								AttachmentType.COMMENT,
								AttachmentType.IMAGE,
								AttachmentType.GALLERY,
								AttachmentType.VIDEO,
								AttachmentType.AUDIO,
								AttachmentType.DOCUMENT,
								AttachmentType.BONUS,
							]
						}
					})
				}));
			}

            this.items.set(new FormItem({
                id  : 'description',
                row : 8,
                col : 1,
                field : new Field({
                    id          : 'description',
                    type        : FieldType.type('TEXTAREA'),
                    name        : 'description',
                    label       : 'Descrição',
                    placeholder : 'Descrição',
                    required    : false,
                })
            }));

            this.items.set(new FormItem({
                id  : 'information',
                row : 9,
                col : 1,
                field : new Field({
                    id          : 'information',
                    type        : FieldType.type('TEXT'),
                    name        : 'information',
                    label       : 'Informação',
                    placeholder : 'Informação',
                    required    : false,
                })
            }));

			this.items.set(new FormItem({
                id  : 'pplaceholder',
                row : 10,
                col : 1,
                field : new Field({
                    id          : 'placeholder',
                    type        : FieldType.type('TEXT'),
                    name        : 'placeholder',
                    label       : 'Placeholder',
                    placeholder : 'Placeholder',
                    required    : false,
                })
            }));

            this.items.set(new FormItem({
                id  : 'initial',
                row : 11,
                col : 1,
                field : new Field({
                    id          : 'initial',
                    type        : FieldType.type('TEXT'),
                    name        : 'initial',
                    label       : 'Valor Inicial',
                    placeholder : 'Valor Inicial',
					required    : false,
					setting     : {
						convert : true
					}
                })
            }));

            this.items.set(new FormItem({
                id  : 'required',
                row : 11,
                col : 2,
                field : new Field({
                    id          : 'required',
                    type        : FieldType.type('Checkbox'),
                    name        : 'required',
                    label       : 'Obrigatório',
                    placeholder : 'Obrigatório',
                    required    : false,
                    initial     : true,
                })
			}));						

            if(this.acl.hasKey('form_advanced_input'))
            {
				this.items.set(new FormItem({
					id  : 'row',
					row : 12,
					col : 1,
					field : new Field({
						id          : 'row',
						type        : FieldType.type('Integer'),
						name        : 'row',
						label       : 'Linha',
						placeholder : 'Linha',
						required    : false,
					})
				}));
	
				this.items.set(new FormItem({
					id  : 'col',
					row : 12,
					col : 2,
					field : new Field({
						id          : 'col',
						type        : FieldType.type('Integer'),
						name        : 'col',
						label       : 'Coluna',
						placeholder : 'Coluna',
						required    : false,
					})
				}));

                this.items.set(new FormItem({
                    id  : 'single',
                    row : 13,
                    col : 1,
                    field : new Field({
                        id          : 'single',
                        type        : FieldType.type('Checkbox'),
                        name        : 'single',
                        label       : 'Único',
                        placeholder : 'Único',
						required    : false,
						setting     : {
							controlField : 'singleFilters',
							'1'		     : 'singleFilters'
						}
                    })
                }));

				const hasSingleFilter : any = FieldType.hasSingleFilter(this.type.value);

				/* UNIQUE FILTERS */
				if(hasSingleFilter)
				{
					this.extraFields.push('singleFilters');

					this.items.set(new FormItem({
						id  : 'singleFilters',
						row : 14,
						col : 1,
						field : new Field({
							type        : FieldType.type('SubForm'),
							name        : 'singleFilters',
							label       : 'Filtros Único',
							placeholder : 'Filtros Único',
							required    : false,                            
						}),
						setting : {
							//showNoItems : false,
						},
						grid : {
							items : [
								{
									row : 1,
									col : 1,
									field : new Field({
										type        : FieldType.type('MultiSelect'),
										name        : '_groups',
										label       : 'Grupo de Permissão',
										placeholder : 'Grupo de Permissão',
										option      : {
											record : false,
											items  : this.core().optionPermission.items
										},										
									})
								},
							]
						},
						items : [
							{
								row : 1,
								col : 1,
								field : new Field({
									type        : FieldType.type('MultiSelect'),
									name        : '_groups',
									label       : 'Grupo de Permissão',
									placeholder : 'Grupo de Permissão',
									option      : {
										record : false,
										items  : this.core().optionPermission.items
									},										
								})
							},
							{
								row : 2,
								col : 1,
								field : new Field({
									type        : FieldType.type('Select'),
									name        : 'type',
									label       : 'Tipo',
									placeholder : 'Tipo',
									option : {
										items : [
											FieldType.type('Text'),
											FieldType.type('ReferenceSelect'),
											FieldType.type('ReferenceMultiSelect'),											
											FieldType.type('Integer'),
											FieldType.type('Number'),
											FieldType.type('Toggle'),
											FieldType.type('Date'),
										]
									},
								})
							},                        
							{
								row : 3,
								col : 1,
								field : new Field({
									type        : FieldType.type('Text'),
									name        : 'field',
									label       : 'Field',
									placeholder : 'Field',
								})
							},
							{
								row : 4,
								col : 1,
								field : new Field({
									type        : FieldType.type('Text'),
									name        : 'operator',
									label       : 'Operador',
									placeholder : 'Operador',
									initial     : '=='
								})
							},
							{
								row : 5,
								col : 1,
								field : new Field({
									type        : FieldType.type('Text'),
									name        : 'value',
									label       : 'Valor',
									placeholder : 'Valor',
									required    : false,
									setting : {
										convert : true
									}
								})
							},
							{
								row : 6,
								col : 1,
								field : new Field({
									type        : FieldType.type('Text'),
									name        : 'context',
									label       : 'Contexto',
									placeholder : 'Contexto',
									required    : false,
								})
							},							
							{
								row : 7,
								col : 1,
								field : new Field({
									type        : FieldType.type('Text'),
									name        : 'path',
									label       : 'Path',
									placeholder : 'Path',
									required    : false,
								})
							},
						]
					}));
				}

				this.items.set(new FormItem({
                    id  : 'hasSum',
                    row : 15,
                    col : 1,
                    field : new Field({
                        id          : 'hasSum',
                        type        : FieldType.type('Checkbox'),
                        name        : 'hasSum',
                        label       : 'Somar',
                        placeholder : 'Somar',
                        required    : false,
                    })
                }));

                this.items.set(new FormItem({
                    id  : 'fixed',
                    row : 16,
                    col : 1,
                    field : new Field({
                        id          : 'fixed',
                        type        : FieldType.type('Checkbox'),
                        name        : 'fixed',
                        label       : 'Fixo',
                        placeholder : 'Fixo',
                        required    : false,
                    })
                }));

                this.items.set(new FormItem({
                    id  : 'onlyAdd',
                    row : 17,
                    col : 1,
                    field : new Field({
                        id          : 'onlyAdd',
                        type        : FieldType.type('Checkbox'),
                        name        : 'onlyAdd',
                        label       : 'Somente Adicionar',
                        placeholder : 'Somente Adicionar',
                        required    : false,
                    })
                }));

                this.items.set(new FormItem({
                    id  : 'onlySet',
                    row : 18,
                    col : 1,
                    field : new Field({
                        id          : 'onlySet',
                        type        : FieldType.type('Checkbox'),
                        name        : 'onlySet',
                        label       : 'Somente Editar',
                        placeholder : 'Somente Editar',
                        required    : false,
                    })
                }));

                this.items.set(new FormItem({
                    id  : 'readonly',
                    row : 19,
                    col : 1,
                    field : new Field({
                        id          : 'readonly',
                        type        : FieldType.type('Checkbox'),
                        name        : 'readonly',
                        label       : 'Somente Leitura',
                        placeholder : 'Somente Leitura',
                        required    : false,
                        initial     : false,
                    })
				}));                

                this.items.set(new FormItem({
                    id  : 'viewColummn',
                    row : 20,
                    col : 1,
                    field : new Field({
                        id          : 'viewColummn',
                        type        : FieldType.type('Checkbox'),
                        name        : 'viewColummn',
                        label       : 'Visualizar Coluna',
                        placeholder : 'Visualizar Coluna',
                        required    : false,
                        initial     : true,
                    })
                }));

                this.items.set(new FormItem({
                    id  : 'hasLabel',
                    row : 21,
                    col : 1,
                    field : new Field({
                        id          : 'hasLabel',
                        type        : FieldType.type('Checkbox'),
                        name        : 'hasLabel',
                        label       : 'Visualizar Label',
                        placeholder : 'Visualizar Label',
                        required    : false,
                        initial     : true,
                    })
                }));

                /*this.items.set(new FormItem({
                    id  : 'display',
                    row : 20,
                    col : 1,
                    field : new Field({
                        id          : 'display',
                        type        : FieldType.type('Checkbox'),
                        name        : 'display',
                        label       : 'Visualizar',
                        placeholder : 'Visualizar',
                        required    : false,
                        initial     : true,
                    })
				}));*/
				
                this.items.set(new FormItem({
                    id  : 'editable',
                    row : 22,
                    col : 1,
                    field : new Field({
                        id          : 'editable',
                        type        : FieldType.type('Checkbox'),
                        name        : 'editable',
                        label       : 'Editável',
                        placeholder : 'Editável',
                        required    : false,
                        initial     : true,
                    })
				}));
				
				this.items.set(new FormItem({
                    id  : 'uppercase',
                    row : 23,
                    col : 1,
                    field : new Field({
                        id          : 'uppercase',
                        type        : FieldType.type('Checkbox'),
                        name        : 'uppercase',
                        label       : 'Uppercase',
                        placeholder : 'Uppercase',
                        required    : false,
                        initial     : false,
                    })
                }));

				this.items.set(new FormItem({
                    id  : '_groups',
                    row : 24,
                    col : 1,
                    field : new Field({
						type        : FieldType.type('MultiSelect'),
						name        : '_groups',
						label       : 'Grupos de Permissão',
						placeholder : 'Grupos de Permissão',
						required    : false,
						option      : {
							record : false,
							items  : this.core().optionPermission.items
						},	
                    })
                }));				
            }
            else
            {
                this.items.set(new FormItem({
                    id  : 'name',
                    row : 12,
                    col : 1,
                    field : new Field({
                        id          : 'name',
                        type        : FieldType.type('HIDDEN'),
                        name        : 'name',
                        label       : 'ID',
                        placeholder : 'ID',
                        initial     : this.core().util.randomString(20),
                    })
                }));
            }
        }

        this.items.doSort();

        this.items.on(true).then(() =>
        {
            console.log('createFields');

            //this.form._fullItems.updateItems(this.items);
            this.form.items.updateItems(this.items);
        });
    }
}
