import { Component, OnInit } from '@angular/core';

/* PIPPA */
import { BaseModal } 		   from '../../base/modal/base.modal';
import { Form }       		   from '../../model/form/form'; 
import { FieldType }  		   from '../../type/field/field.type';
import { EmailPlugin } 		   from '../../plugin/email/email.plugin';
import { Field }        	   from '../../model/field/field';
import { EmailTemplatePlugin } from '../../plugin/email.template/email.template.plugin';
@Component({
    template : `<div class="form-wrapper">
									<form dynamic-form
											#dynamicForm
											[form]="form"
											(add)="_onSend($event)"
											(close)="_onCancel()"
											class="login-form">
									</form>
								</div>`
})
export class EmailPopover extends BaseModal implements OnInit
{
	public onClose : any;

	constructor(
		public emailPlugin 		     : EmailPlugin,
		public emailTemplatePlugin : EmailTemplatePlugin,
  )
  {
        super();
	}
	
    async ngOnInit()
    {		
		const params = this.createParams({
			appid : this.data.appid,
			where : [{
				field 	 : 'form',
				operator : '==',
				value	 : this.data.form
			}]
		});

		const result = await this.emailTemplatePlugin.getData(params);

		const options = [];

		if(result.collection)
		{
			for(const item of result.collection)
			{
				options.push({
					label : item.mode.label + ' - ' + item.name,
					value : item,
				})
			}
		}

		const items = [];

        items.push({
			row   : 1,
			col   : 1,
			field : new Field({
				type        : FieldType.type('Select'),
				name        : 'template',
				label       : 'Template',
				placeholder : 'Template',
				option      : {
					record : false,
					items : options
				},				
			}),
		});		

        this.form = new Form({
            name    : 'E-mail',            
            iconAdd : 'send',
			items   : items,
			saveFixed : true,
            setting : {
                addText : 'Enviar',
            }
        });
    }
 
	async _onSend(event:any)
    {
		await this.emailPlugin.sendTransection(this.data, event.data.template.value, true, true);
		
		this.onClose(event);
	}

	_onCancel()
	{
		this.onClose(event);
	}
}
