import { Component, OnInit } from '@angular/core';

/* PIPPA */
import { BaseFormModal }      from '../../base/form.modal/base.form.modal';
import { Field }              from '../../model/field/field';
import { StepItem }           from '../../model/step.item/step.item';
import { FormItemCollection } from '../../model/form.item/form.item.collection';
import { FieldType }          from '../../type/field/field.type';
import { FormType }           from '../../type/form/form.type';
import { Form }               from '../../model/form/form';

@Component({
    selector : 'step-form-popover',
    template : `<form dynamic-form
                      [form]="form"
                      [data]="data"
                      [acl]="acl"
                      (add)="onAdd($event)"
                      (set)="onSet($event)"
                      (close)="onClose()"
                      class="field-form">
                </form>`
})
export class StepFormPopover extends BaseFormModal implements OnInit
{
    public _data : any;

    ngOnInit()
    {
        const items = new FormItemCollection();

        const form : any = new Form({
            name  : 'Step',
            type  : FormType.FLAT_TYPE,
        });

        items.set({
            id    : 'label',
            row   : 1,
            col   : 0,
            step  : new StepItem({
                id : 0,
            }),
            field : new Field({
                id       : 'label',
                type     : FieldType.type('Text'),
                name     : 'label',
                label    : 'Label',
                required : false
            })
		});		
		
		items.set({
            id    : 'name',
            row   : 2,
            col   : 0,
            step  : new StepItem({
                id : 0,
            }),
            field : new Field({
                id       : 'name',
                type     : FieldType.type('Text'),
                name     : 'name',
                label    : 'ID',
                required : false
            })
		});

		items.set({
            id    : 'stepOrder',
            row   : 3,
            col   : 0,
            step  : new StepItem({
                id : 0,
            }),
            field : new Field({
                id       : 'stepOrder',
                type     : FieldType.type('Integer'),
                name     : 'stepOrder',
                label    : 'Ordem',
                required : false
            })
		});
		
		items.set({
			id  : '_groups',
			row : 19,
			col : 1,
			step  : new StepItem({
                id : 0,
            }),
			field : new Field({
				type        : FieldType.type('MultiSelect'),
				name        : '_groups',
				label       : 'Grupos de Permissão',
				placeholder : 'Grupos de Permissão',
				required    : false,
				option      : {
					record : false,
					items  : this.core().optionPermission.items
				},	
			})
		});	

        items.set({
            id  : 'setting',
            row : 20,
            col : 1,
            field : new Field({
                id          : 'setting',
                type        : FieldType.type('ObjectInput'),
                name        : 'setting',
                label       : 'Configurações',
                required    : false,
                viewOptions : true,
            })
        });        

		items.set({
			id  : 'onlyAdd',
			row : 21,
			col : 1,
			field : new Field({
				id          : 'onlyAdd',
				type        : FieldType.type('Checkbox'),
				name        : 'onlyAdd',
				label       : 'Somente Adicionar',
				placeholder : 'Somente Adicionar',
				required    : false,
			})
		});

		items.set({
			id  : 'onlySet',
			row : 22,
			col : 1,
			field : new Field({
				id          : 'onlySet',
				type        : FieldType.type('Checkbox'),
				name        : 'onlySet',
				label       : 'Somente Editar',
				placeholder : 'Somente Editar',
				required    : false,
			})
		});

        form.items.setItems(items);

        if(this.data)
        {
            this.data.on(true).then(() =>
            {
                form.doSetModeForm();
            });
        }

        form.on(true).then(() =>
        {
            this.form = form;
        });
    }

    set data(value:any)
    {
        if(value)
        {
            this._data = value;
        }
    }

    get data():any
    {
        return this._data;
    }
}
