import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { NavParams, NavController } from '@ionic/angular';

@Component({
    template: `
        <ion-list>
            <ion-list-header>
                <ion-icon name="{{icon}}" item-start></ion-icon> {{title}}
            </ion-list-header>
            <ion-item class="button" *ngFor="let item of menu" (click)="onClick(item)">
                <ion-icon name="{{item['icon']}}" item-start></ion-icon> {{item['label']}}
            </ion-item>
        </ion-list>`
})
export class MenuPopover
{
    public title : string;
    public icon  : string;
    public menu  : Array<any> = [];

    constructor(
        public navParams      : NavParams,
        //public viewController : ViewController
    )
    {
        this.title = navParams.get('title');
        this.icon  = navParams.get('icon');
        this.menu  = navParams.get('menu');
    }

    onClick(item)
    {
        //this.viewController.dismiss(item);
    }
}
