import { Component } from '@angular/core';
import { Subscription } from 'rxjs';

/* PIPPA */
import { BaseModal }  from '../../base/modal/base.modal';
import { FormPlugin } from '../../plugin/form/form.plugin';

@Component({
    template : `<ion-list *ngIf="grid">
                    <ion-list-header>
                        <ion-label>
                            <ion-icon name="code-working-outline" item-start></ion-icon>
                            Visualizar Colunas
                        </ion-label>
                        <a class="close" (click)="onClose()">
                            <ion-icon name="close-outline" item-end></ion-icon>
                        </a>
                    </ion-list-header>
                    <div>
                        <ion-item class="button" *ngFor="let item of items">
                            <input id="{{item.id}}" type="checkbox"
                                   [checked]="!item.disabled"
                                   (change)="item.disabled = !item.disabled">
                            <label for="{{item.id}}">{{item.label}}</label>
                        </ion-item>
                    </div>
                    <ion-list-header class="footer">
                        <ion-button expand="full" (click)="onSave()">
                            Salvar
                        </ion-button>
                    </ion-list-header>
                </ion-list>`
})
export class ColumnOrderPopover extends BaseModal
{
    public bagItems : string;
    public items    : any;
    public subscriptions = new Subscription();

    protected _grid : any;

    constructor(
        public formPlugin : FormPlugin,
    )
    {
        super();        
    }

    get grid():any
    {
        return this._grid;
    }

    set grid(value:any)
    {
        value.on(true).then(() =>
        {
            this._grid = value;

            if(value)
            {
                this.items = value.items.copy();
            }
        })
    }

    onSave()
    {
        this.grid.items.updateItems(this.items).then(() =>
        {
            console.log('save', this.grid.parseData());
            this.grid.set(this.grid.parseData());
            this.onClose();
        });
    }
}
