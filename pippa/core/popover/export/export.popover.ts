import { Component, OnInit } from '@angular/core';

/* PIPPA */
import { BaseModal }  from '../../base/modal/base.modal';
import { Form }       from '../../model/form/form'; 
import { GridPlugin } from '../../plugin/grid/grid.plugin';
import { ExportType } from '../../type/export/export.type';
import { FieldType }  from '../../type/field/field.type';

@Component({
    template : `<div class="form-wrapper">
					<form dynamic-form
							#dynamicForm
							[form]="exportForm"
							[data]="data"
							(add)="_onExport($event)"
							(close)="_onCancel()"
							class="login-form">
					</form>
				</div>`
})
export class ExportPopover extends BaseModal implements OnInit
{
	public types      : any;
	public exportForm : any;
	public onExport   : any;
	public onCancel   : any;
	public gridPlugin : any;
	public grids 	  : any;

    ngOnInit()
    {		
		this.gridPlugin = this.core().injector.get(GridPlugin);

		/* LOAD GRID */
		const params = this.createParams({
			appid 	: this.app.code,
			orderBy : 'name',
		});				
		
		params.where.set({
			field    : 'isExport',
			operator : '==',
			value    : true,
		});

		this.gridPlugin.getData(params).then(async (result:any) =>
		{
			this.grids = [];

			/* GRID */
			if(result.collection.length > 0)
			{
				for(const item of result.collection)
				{
					this.grids.push({
						label : item.name,
						value : item.referencePath,
					});
				}
			}
			else
			{
				this.grids = [];
			}

			const items = [];

			items.push({
				row : 1,
				col : 1,
				field : {
					type        : FieldType.type('Select'),
					name        : 'documents',
					label       : 'Documentos',
					placeholder : 'Documentos',
					fixed 		: true,
					setting 	: {
						inline 		 : true,
						controlField : 'startDate,endDate',
						period 		 : 'startDate,endDate',
					},
					initial : ExportType.PERIOD_DOCUMENTS, 
					option : {
						items : 
						[							
							ExportType.PERIOD_DOCUMENTS,
							ExportType.ALL_DOCUMENTS,
						]
					}
				}
			});

			items.push({
				row : 2,
				col : 1,
				field : {
					type        : FieldType.type('Date'),
					name        : 'startDate',
					label       : 'Data Inicial',
					placeholder : 'Data Inicial',
					fixed 		: true,
				}
			});

			items.push({
				row : 2,
				col : 2,
				field : {
					type        : FieldType.type('Date'),
					name        : 'endDate',
					label       : 'Data Final',
					placeholder : 'Data Final',
					fixed 		: true,
				}
			});

			items.push({
				row : 3,
				col : 1,
				field : {
					type        : FieldType.type('Radio'),
					name        : 'type',
					label       : 'Tipo',
					placeholder : 'Tipo',
					fixed 		: true,
					setting 	: {
						inline : true
					},
					option : {
						items : 
						[
							ExportType.XLS_TYPE,
							ExportType.CSV_TYPE
						]
					}
				}
			});

			items.push({
				row : 4,
				col : 1,
				field : {
					type        : FieldType.type('Select'),
					name        : 'gridPath',
					label       : 'Grid',
					placeholder : 'Grid',
					fixed 		: true,
					setting 	: {
						inline : true
					},
					option : {
						items : this.grids
					}
				}
			});

			this.exportForm = new Form({
				name    : 'Exportar',            
				iconAdd : 'lock-open',
				items   : items,
				saveFixed : true,
				setting : {
					addText : 'Exportar',
				}
			});
		});						
    }

	_onExport(event:any)
    {
        this.onExport(event);
	}

	_onCancel()
	{
		this.onCancel(event);
	}
}
