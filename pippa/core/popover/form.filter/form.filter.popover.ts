import { Component, OnInit, } from '@angular/core';
import { PopoverController } from '@ionic/angular';

/* PIPPA */
import { BaseFilter }   from '../../base/filter/base.filter';
import { FilterPlugin } from '../../plugin/filter/filter.plugin';
import { Form }         from '../../model/form/form';

@Component({
    selector : '.form-filter-popover',
    template : `<form dynamic-form
                      [form]="form"
                      [data]="data"
                      (add)="setFilter($event)"
                      (set)="setFilter($event)"
                      (close)="onCancel($event)">
                </form>`,
})
export class FormFilterPopover extends BaseFilter implements OnInit
{
    public onFilter:any;

    constructor(
        public filterPlugin      : FilterPlugin,
        public popoverController : PopoverController,
    )
    {
        super();
    }

    setFilter(event:any)
    {
        event.filter = this.filter;

        this.onFilter(event);
        this.popoverController.dismiss();
    }

    onCancel(event:any)
    {
        this.onFilter(event);
        this.popoverController.dismiss();
    }

    ngOnInit()
    {
        this.loadFilter();
    }

    onClose(event:any)
    {
        event;
        this.popoverController.dismiss();
    }

    loadFilter()
    {
        return new Promise<void>((resolve) =>
        {
            /* LOAD FILTER */
            let params = this.createParams({
                appid : this.app.code,
            });

            this.filterPlugin.getData(params).then((result:any) =>
            {
                if(result.collection.length > 0)
                {
                    this.filter = result.collection[0];
                    this.form   = new Form({
						addText    : 'Filtrar',
						setText    : 'Filtrar',
                        cancelText : 'Limpar',
                        items      : result.collection[0].items
                    });
                }

                resolve();
            });
        });
    }
}
