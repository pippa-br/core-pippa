import { Injectable } from '@angular/core';

/* PIPPA */
import { DocumentPlugin } from '../document/document.plugin';
import { User }           from '../../model/user/user';
import { UserCollection } from '../../model/user/user.collection';
import { Params }         from '../../util/params/params';
import { Types }          from '../../type/types';

@Injectable({
    providedIn: 'root'
})
export class UserPlugin extends DocumentPlugin
{
	public onlyMaster : boolean = false;

    getMaps():any
    {
        return {
            model : User,
            list  : UserCollection,
        }
    }

    /*getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_AUTH_API,
			getEndPoint  : Types.GET_AUTH_API,
			addEndPoint  : Types.ADD_AUTH_API,
			setEndPoint  : Types.SET_AUTH_API,
			delEndPoint  : Types.DEL_AUTH_API,
        }
	}*/
}
