import { Injectable } from '@angular/core';

/* PIPPA */
import { Menu }           from '../../model/menu/menu';
import { MenuCollection } from '../../model/menu/menu.collection';
import { BasePlugin } 	  from '../base.plugin';
import { Params }         from '../../util/params/params';
import { Types } from '../../type/types';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MenuPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Menu,
            list  : MenuCollection,
        }
    }

    /*getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_MENU_API,
			getEndPoint  : Types.GET_MENU_API,
			addEndPoint  : Types.ADD_MENU_API,
			setEndPoint  : Types.SET_MENU_API,
			delEndPoint  : Types.DEL_MENU_API,
        }
	}*/

    prepareParams(params:Params)
    {
        params          = super.prepareParams(params);
        params.perPage  = params.perPage === undefined ? 999 	: params.perPage;
        params.orderBy  = params.orderBy === undefined ? 'order' : params.orderBy;
        params.asc      = params.asc 	=== undefined ? true    : params.asc;
        //params.map      = true;
        //params.mapItems = environment.menu.mapItems;

        return params;
    }

    loadMenu(appid:string)
    {
        return new Promise<any>((resolve:any) =>
        {
            const params = this.createParams({
                appid : appid,
                where : [{
                    field    : '_level',
                    operator : '==',
                    value    : 1,
                }]
            });

            this.getData(params).then((result:any) =>
            {
                resolve(result);
            });
        })
	}
	
	async afterAdd(app:any, data:any)
	{
		return;
	}
	
	async beforeSet(app:any, data:any, event:any)
	{
		return;
	}
}
