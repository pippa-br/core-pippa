import { Injectable } from '@angular/core';

/* PIPPA */
import { StepItem }           from '../../model/step.item/step.item';
import { StepItemCollection } from '../../model/step.item/step.item.collection';
import { BasePlugin }         from '../../../core/plugin/base.plugin';
import { Params }             from '../../../core/util/params/params';
import { Result }             from '../../../core/util/result/result';

@Injectable({
    providedIn: 'root'
})
export class StepPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : StepItem,
            list  : StepItemCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'steps';
	}    
}
