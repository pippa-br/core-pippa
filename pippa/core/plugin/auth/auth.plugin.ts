import { Injectable } from '@angular/core';

/* PIPPA */
import { Params }         from '../../util/params/params';
import { Result }         from '../../util/result/result';
import { BasePlugin }     from '../base.plugin';
import { UserCollection } from '../../model/user/user.collection';
import { EmailPlugin }    from '../../plugin/email/email.plugin';
import { StoragePlugin }  from '../../plugin/storage/storage.plugin';
import { EventPlugin }    from '../../plugin/event/event.plugin';
import { DocumentPlugin } from '../../plugin/document/document.plugin';
import { LogPlugin }      from '../log/log.plugin';
import { Types }          from '../../type/types';
import { User }           from '../../model/user/user';
import { Account }     	  from '../../model/account/account';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthPlugin extends BasePlugin
{
    constructor(
        public storagePlugin  : StoragePlugin,
        public documentPlugin : DocumentPlugin,
		public emailPlugin    : EmailPlugin,
        public logPlugin      : LogPlugin,
    )
    {
        super();
    }

    getMaps():any
    {
        return {
            model : User,
            list  : UserCollection,
        }
    }

	/*getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_AUTH_API,
			getEndPoint  : Types.GET_AUTH_API,
			addEndPoint  : Types.ADD_AUTH_API,
			setEndPoint  : Types.SET_AUTH_API,
			delEndPoint  : Types.DEL_AUTH_API,
        }
	}*/

    loginFull : (params:Params) => Promise<Result> = function (params:Params)
    {
        let promise : Promise<Result> = new Promise<Result>((resolve) =>
        {
            /* VERIFICA USUARIO EM SESSIOM */
            this.getLogged(params).then((result:any) =>
            {
                 if(result.status)
                 {
                     resolve(result);
                 }
                 else
                 {
                     /* ABRIR STORAGE */
                     this.storagePlugin.ready().then(() =>
                     {
                         /* VERIFICAR USUARIO STORAGE */
                         this.storagePlugin.getAuth().then((auth:any) =>
                         {
                             if(auth)
                             {
                                params.post = Object.assign({}, auth, params.post);

                                 /* FAZER LOGIN */
                                 this.login(params).then((result2:any) =>
                                 {
                                      resolve(result2);
                                 });
                             }
                             else
                             {
                                 let result    = this.createResult(params);
                                 result.status = false;

                                 resolve(result);
                             }
                         });
                     });
                 }
            });
        });

        return promise;
    }

	async getToken(params:Params)
    {
		params = this.prepareParams(params);
 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.GET_TOKEN_AUTH_API, 
		{
			platform : this.core().platform
			//maccid : params.maccid,
			//accid  : params.accid,
			//appid  : params.appid
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;        
    }

	async setUser(params:any)
    {
		params = this.prepareParams(params);
 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.SET_USER_API, 
		{
			platform : this.core().platform,
			accid    : params.accid,
			appid    : params.appid,
			colid    : params.colid,
			document : params.document,
			form 	 : params.form,
			data     : params.data,
			mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;        
    }

    async getLogged(params:Params)
    {
		params = this.prepareParams(params);
 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.GET_LOGGED_AUTH_API, 
		{
			platform : this.core().platform,
			map      : true,
        	mapItems : {
				referencePath : environment.defaultMapItems
			},
			accid : params.accid,
			appid : params.appid,
			colid : params.colid,
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;

        /*return new Promise(async (resolve) =>
        {
			const user = await this.core().fireAuth.currentUser;

			console.log('logged fireAuth.currentUser', user);

			if(user)
			{
				params       = this.prepareParams(params);
				params.where = [];

				params.where.push({
					field    : '_auth',
					operator : '==',
					value    : user.email
				},);

				const app = this.core().getApp(params.appid);

				/* DEFINE O COLID */
				/*if(app && app.colid != 'documents')
				{
					params.colid = app.colid;
				}

				/* PROCURA O USUARIO */
				/*this.api().getList(params).then((result:any) =>
				{
					/* CASO O USUARIO EM SESSION FOR DIFERENTE DO APPID */
					/*if(result.collection.length > 0)
					{
						//this.core().isLogged = true;
						//this.core().user     = result.collection[0];
						
						result.data   = result.collection[0];
						result.status = true;
						resolve(result);
					}
					else
					{
						let result = this.createResult(params);
						resolve(result);
					}
				});
			}
			else
			{
				//this.core().isLogged = false;
				//this.core().user     = null;

				let result = this.createResult(params);
				resolve(result);
			}
        });*/
    }

    async login(params:Params)
    {
				params = this.prepareParams(params);

				await this.core().loadingController.start();

				const result = await this.core().api.callable(Types.LOGIN_AUTH_API, 
				{
					maccid    	 	: params.maccid,
					accid     	 	: params.accid,
					appid     	 	: params.appid,
					colid     	 	: params.colid,
					groupCollection : params.groupCollection,
					loginPath 	 	: params.post.pathLogin,
					login 	  	 	: params.post.login,
					passwordPath 	: params.post.pathPassword,
					password     	: params.post.password,
					noEncrypt    	: params.post.noEncrypt !== undefined ? params.post.noEncrypt : true,
					platform 		: this.core().platform,
					mapItems : {
						referencePath : environment.defaultMapItems
					},
					token     	 	: params.token,
				}, this.getMaps(), true);

				this.core().loadingController.stop();

				return result;

		/*console.log('post', params.post);

		params       = this.prepareParams(params);
		params.where = [];

		const auth : any = {};

		params.post.login = params.post.login.toLowerCase();

		params.where.push({
			field    : params.post.pathLogin,
			operator : '==',
			value    : params.post.login
		});

		auth.login     = params.post.login;
		auth.pathLogin = params.post.pathPath;
		
		/* CASO NÃO FOR MD5 */
		/*if(params.post.encrypt && !this.core().util.isValidMD5(params.post.password)) 
		{
			const md5     = new Md5();
			const encrypt = md5.appendStr(params.post.password).end();

			params.post.password = encrypt;
		}

		/* MASTER PASSOWRD */
		/*let masterPassword = this.core().account.masterPassword;

		if(params.post.localPassword)
		{
			masterPassword = params.post.localPassword || masterPassword;
		}

		if(params.post.password && masterPassword != params.post.password)
		{
			params.where.push({
				field    : params.post.pathPassword,
				operator : '==',
				value    : params.post.password
			});

			auth.password = params.post.password;
		}

		if(params.extraFilters && params.extraFilters.length > 0)
		{
			for(const key in params.extraFilters)
			{
				const item = params.extraFilters[key];
				
				params.where.push({
					field    : item.field,
					operator : item.operator,
					value    : item.value,
				});
			}
		}

		console.log('login by', params.where, auth, masterPassword, params.appid);

		const app = this.core().getApp(params.appid);

		/* DEFINE O COLID */
		/*if(app.colid != 'documents')
		{
			params.colid = app.colid;
		}	

		/* PROCURA O USUARIO */
		/*this.api().getList(params).then(async (result:any) =>
		{
			console.log('login', result.collection.length > 0);

			if(result.collection.length > 0)
			{
				await this.storagePlugin.set('appid', params.appid); /* APPID DO AUTH */
				/*this.storagePlugin.setAuth(auth);

				/* DEPOIS VERIFICAR UPDATE DE PRODUTO
				https://tableless.com.br/auth-no-firebase/*/

				/* POR CAUSA DO AUTH COM PHONE, CPF, ETC UTLIZA ESSA FORMA */
				/*const user       = result.collection[0];
				const _auth      = (params.appid + '_' + user.id + '@pippa.com.br').toLowerCase();
				const lastAuth   = new Date();
				const uniqueAuth = lastAuth.getTime()

				await this.storagePlugin.set('uniqueAuth', uniqueAuth);

				const updateData : any = {
					_auth 	   : _auth,
					lastAuth   : lastAuth,
					uniqueAuth : uniqueAuth
				}

				if(this.core().notification)
				{
					updateData._notification = this.core().notification.reference;
				}

				/* ATUALIZA O USUARIO */
				/*user.set(updateData);

				console.log('sign in auth', _auth);

				/* VERIFICA SE TEM AUTH */
				/*this.core().fireAuth.fetchSignInMethodsForEmail(_auth).then((resultAuth:any) =>
				{
					if(resultAuth.length > 0)
					{
						/* TENTA FAZER O LOGIN */
						/*this.core().fireAuth.signInWithEmailAndPassword(_auth, _auth).then(() =>
						{
							result.data   = result.collection[0];
							result.status = true;

							/* LOG */
							/*this.logPlugin.add(LogType.LOGIN, params.appid, {}).then((dataLog:any) =>
							{
								console.log('log', dataLog);

								this.core().loadingController.stop();

								resolve(result);
							});
						})
						.catch((error:any) =>
						{
							result.message = error;
							resolve(result);
						});
					}
					else
					{
						this.core().fireAuth.createUserWithEmailAndPassword(_auth, _auth).then(() =>
						{
							result.data   = result.collection[0];
							result.status = true;

							/* LOG */
							/*this.logPlugin.add(LogType.LOGIN, params.appid, {}).then((dataLog:any) =>
							{
								console.log('log', dataLog);

								this.core().loadingController.stop();

								resolve(result);
							});
						})
						.catch((error:any) =>
						{
							result.message = error;

							this.core().loadingController.stop();

							resolve(result);
						});
					}
				});
			}
			else
			{
				result.status  = false;
				result.message = {
					description : "Login ou Senha Invalido!"
				};

				this.core().loadingController.stop();

				resolve(result);
			}
		});*/
	}
	
	/* DEPOIS DO CADASTRO FAZ O LOGIN */
    loginRegister(data:any)
    {
        const params = this.createParams({
            appid : data.appid,
            post  : {
				pathLogin    : 'email',
				pathPassword : 'password',
                login        : data.email,
                password     : data.password,
            },
			mapItems : {
				referencePath : environment.defaultMapItems
			},
        });

        this.login(params).then((result:any) =>
        {
            if(result.status)
            {
				this.core().loadingController.start().then(async () => 
				{
					this.core().user     = result.data;
					this.core().isLogged = true;
					
					if(result.data.appid == 'accounts')
					{
						this.core().account = new Account(result.data);
						await this.core().account.on();
						this.core().initAccount();
					}

					const group 	  = result.data.getGroup();
					const eventPlugin = this.core().injector.get(EventPlugin);
					
					/* FILTER MENU */
					if(this.core().menu)
					{
						this.core().menu.updateAsyncFilter().then(() =>
						{
							this.core().loadingController.stop();

							/* LOGIN */
							eventPlugin.dispatch(group + ':login', result);
							eventPlugin.dispatch('login', result);
			
							console.log('dispatch' + data.appid + ':login');							
						});
					}
					else
					{
						this.core().loadingController.stop();
						
						/* LOGIN */
						eventPlugin.dispatch(group + ':login', result);
						eventPlugin.dispatch('login', result);
		
						console.log('dispatch' + data.appid + ':login');						
					}      
				});                              
            }
            else
            {
                this.onError(result);
            }
        });
    }

    async logout(params:Params)
    {
		params = this.prepareParams(params);

		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.LOGOUT_AUTH_API, 
		{
			maccid   : params.maccid,
			accid    : params.accid,
			appid    : params.appid || 'admin',
			colid    : 'documents',
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		const appid = await this.storagePlugin.get('appid'); /* APPID DO AUTH */

		this.core().isLogged     = false;
		this.core().user         = null; 
		this.core().isInitialize = null;
		this.core().appidLogout  = appid;

		console.log('logout', appid);

		/* FILTER MENU, QUANDO FAZ LOGOUT PRECISA DESENHAR O MENU NOVAMENTE */
		if(this.core().menu)
		{
			this.core().menu.updateAsyncFilter();
		}

		/* CLEAR STORAGE */
		await this.storagePlugin.clear();

		this.core().loadingController.stop();
		
		return result;
    }

    async recoveryPassword(params:Params)
    {
		params = this.prepareParams(params);

		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.RECOVERY_PASSWORD_AUTH_API, 
		{
			maccid    : params.maccid,
			accid     : params.accid,
			appid     : params.appid,
			colid     : params.colid,
			loginPath : params.post.loginField,
			login 	  : params.post.login,
			mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		await this.core().loadingController.stop();

		return result;

		/*const result = this.createResult(params);

		params.where = [];

        params.where.push({
            field    : params.post.loginField || 'email',
            operator : '==',
            value    : params.post.login.toLowerCase()
        });

		const resultCollection = await this.documentPlugin.getData(params)
        
		if(resultCollection.collection.length > 0)
		{
			const user = resultCollection.collection[0];
			await user.on();
		
			const newPassword = this.core().util.randomString(10);

			/* UPDATE DATA */
			/*const md5     = new Md5();
			const encrypt = md5.appendStr(newPassword).end();

			user.set({
				password        : encrypt,
				confirmPassword : encrypt,
			});

			user.newPassword = newPassword;

			// LOG 
			const dataLog = await this.logPlugin.add(LogType.RECOVERY_PASSWORD, params.appid, { email : user.email });

			console.log('log', dataLog);

			/* ENVIA E-MAIL */
			/*const form = await user.getProperty('form');

			await this.emailPlugin.send(user, form, TransactionType.RECOVERY_PASSWORD_TYPE);

			result.message = 'Senha Enviada', 'Acesse o email, ' + params.post.login + ', e siga as instruções. Se você não receber o e-mail verifique as suas pastas de spam ou de lixo eletrônico. É possível que o e-mail que tentamos enviar a você tenha sido bloqueado por um filtro de spam ou de lixo eletrônico do seu sistema de e-mail.'
			result.status  = true;
		}
		else
		{
			result.error = 'E-mail não encontrado', 'Nenhuma conta foi encontrada com esse endereço de e-mail';
		}     

		return result;*/
    }
}
