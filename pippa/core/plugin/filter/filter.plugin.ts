import { Injectable } from '@angular/core';

/* PIPPA */
import { Filter }            from '../../model/filter/filter';
import { Where }             from '../../model/where/where';
import { WhereCollection }   from '../../model/where/where.collection';
import { FilterCollection }  from '../../model/filter/filter.collection';
import { BasePlugin }    	 from '../base.plugin';
import { Params }            from '../../util/params/params';
import { Result }            from '../../util/result/result';
import { StoragePlugin }     from '../../plugin/storage/storage.plugin';

@Injectable({
    providedIn: 'root'
})
export class FilterPlugin extends BasePlugin
{
    constructor(
        public storagePlugin : StoragePlugin,
    )
    {
        super();
    }

    getMaps()
    {
        return {
            model : Filter,
            list  : FilterCollection,
        }
	}

	collectionName(appid:string)
	{
		return 'filter';
	}  
    
    setFilter(app:any, whereCollection:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const path = app.accid + '_' + app.code + '_filter';
            const data = whereCollection.parseData();

            console.log('set filter', path, data);

            this.storagePlugin.set(path, data).then(() =>
            {
                resolve({
                    data : data
                });
            })
        });
    }

    getFilter(app:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const path = app.accid + '_' + app.code + '_filter';

            this.storagePlugin.get(path).then((data:any) =>
            {
                console.log('get filter', path, data);

                if(data)
                {
                    resolve({
                        data : new WhereCollection(data)
                    });
                }
                else
                {
                    resolve(null);
                }
            });
        });
    }

    delFilter(app:any):Promise<any>
    {
        return new Promise<void>((resolve) =>
        {
            const path = app.accid + '_' + app.code + '_filter';

            console.log('del filter', path);

            this.storagePlugin.del(path).then(() => 
            {
                resolve();
            })            
        });
    }
}
