import { Injectable } from '@angular/core';

/* PIPPA */
import { Notification }          	  from '../../model/notification/notification';
import { NotificationCollection } 	  from '../../model/notification/notification.collection';
import { NotificationTemplatePlugin } from '../../plugin/notification.template/notification.template.plugin';
import { BasePlugin }     		  	  from '../../plugin/base.plugin';
import { FieldType }           		  from '../../type/field/field.type';
import { Params }         		 	  from '../../util/params/params';
import { Form }       				  from '../../model/form/form';
import { FormPlugin } 				  from '../form/form.plugin';

@Injectable({
    providedIn: 'root'
})
export class NotificationPlugin extends BasePlugin
{
	constructor(
        public notificationTemplatePlugin : NotificationTemplatePlugin,
		public formPlugin 				  : FormPlugin,
    )
    {
        super();
	}
	
    getMaps():any
    {
        return {
            model : Notification,
            list  : NotificationCollection,
        }
	}
	
	appidName()
	{
		return '_notifications';
	}

    registration(formPath:string, notification:any)
    {
        return new Promise<any>(async (resolve) =>
        {   
			/* PEGA NOVAMENTE O OBJETO NOTIFICACTION */
            let params = this.createParams();
            params.id  = notification.token;
			
			const notificationResult = await this.get(params);
	
			/* PEGA O FORM NOTIFICATION */
			params   	= this.createParams();
			params.path = formPath;

			const formResult = await this.formPlugin.get(params);

			if(notificationResult.data.exists && notificationResult.data.form.id == formResult.data.id)
			{

				const data = await formResult.data.parseToData(notificationResult.data.getData());
				notificationResult.data.save(data);
			}
			else /* RESET O NOTIFICATION SE HOUVER TROCA DE FORM */
			{				
				const data = await formResult.data.parseToData({
					token  : notification.token,
					device : notification.device,
				});

				notificationResult.data.create(data);
			}

			await notificationResult.data.on();

			console.log('register token', notificationResult.data);
	
			resolve(notificationResult.data);               
        });
	}
	
	send(data:any, form:any, type:any):Promise<any>
    {
        return new Promise<void>((resolve) =>
        {
			if(this.core().account.cloudMessaging)
			{
				const params = this.createParams({
					appid : data.appid,
				});
				
				params.where = [];
	
				if(form)
				{
					params.where.push({
						field    : 'form',
						operator : '==', 
						value    : form.reference,
						type     : FieldType.type('ReferenceSelect')
					});
				}
				
				/* CARREGA TEMPLATE */
				this.notificationTemplatePlugin.getData(params).then(async (result:any) =>
				{
					if(result.status && result.collection.length > 0)
					{
						console.log('notification template', result.collection);

						for(const key in result.collection)
						{
							const notificationTemplate = result.collection[key];
							await notificationTemplate.on(true);

							const isValid = await this.validateTemplate(data, notificationTemplate, type);

							if(isValid)
                        	{
								const notificationCollection = await this.getNotificationCollection(data, notificationTemplate);

								let promises 	  = [];
								let notifications = [];
	
								const subject     = await this.core().util.parseVariables(data, notificationTemplate.subject);								
								const body        = await this.core().util.parseVariables(data, notificationTemplate.message);
								const description = await this.core().util.parseVariables(data, notificationTemplate.description);
	
								for(const key2 in notificationCollection)
								{
									let notification = notificationCollection[key2];
									let sendToMe     = true;

									if(this.core().user._notification && this.core().user._notification.id == notification.id)
									{
										sendToMe = false;
									}

									if(notification[data.appid] && sendToMe)
									{															
										const post = {
											notification : 
											{
												sound			  : 'default',
												title			  : subject, 
												body  	     	  : body,
												click_action      : "OPEN_ACTIVITY_1",
												priority          : "high",
												content_available : true,
											},
											data : {
												title  		: subject, /* POR CAUSA DO PERFORMED */
												body   		: body, /* POR CAUSA DO PERFORMED */
												description : description,
												router	    : notificationTemplate.router,
												params 		: notificationTemplate.params,
												action 		: notificationTemplate.action,	
											},
											to : notification.token,
										}

										const pushResult = await this.core().api.callable('cfmApi/push', post);

										promises.push(pushResult);
										notifications.push(notification);
									}
								}
		
								Promise.all(promises).then((values:any) => 
								{
									for(const key2 in values)
									{
										if(values[key2].failure)
										{
											console.error('notification failure', values[key2], notifications[key2].id);
											notifications[key2].del();	
										}
										else
										{
											notifications[key2].set({
												lastdate : new Date()
											});
											console.log('notification send', values[key2], notifications[key2].id);
										}
									}
	
									resolve();
								});
								
							}	
							else
							{
								resolve();
							}						
						}										
					}
					else
					{
						console.log('notification not found', data.appid, form);
						resolve();
					}
				});
			}
			else
			{
				resolve();
			}            
        });
	}

	async getNotificationCollection(data:any, notificationTemplate:any)
	{
		/* VERIFICAR toType PARA CADASTROS ANTIGOS  */
		if(notificationTemplate.toType && notificationTemplate.toType.value == 'app')
		{			
			const appid = notificationTemplate.app.code;

			const params2 = this.createParams({
				appid   : appid,
				perPage : 9999,
			});

			/* CARREGA NOTIFICATIONS */
			const result = await this.getData(params2);

			const notifications = new NotificationCollection();

			for(const key2 in result.collection)
			{
				let notification = result.collection[key2];

				/* POIS É REFERENCE DENTRO DO USER */
				if(notification._notification)
				{
					notification = new Notification(notification._notification);
					notifications.add(notification);	
				}
			}

			return await notifications.on();
		}
		else if(notificationTemplate.toType && notificationTemplate.toType.value == 'field')
		{
			const item 		    = await data.getProperty(notificationTemplate.field); 
			const notifications = new NotificationCollection();

			const notification = new Notification(item._notification);
			notifications.add(notification);			

			return await notifications.on();
		}
		else
		{
			let appid : string = '_notifications';

			const params2 = this.createParams({
				appid   : appid,
				perPage : 9999,
			});

			const result = await this.getData(params2);
			return result.collection;
		}
	}
	
	async validateTemplate(data:any, template:any, type:any)
	{
		let isSend : boolean = false;
		let key    : string  = '_send_notification_' + template.id;
		let count  : number = (data[key] ? data[key] : 0);		

		//if(count == 0 || !type.hasCount)
		//{
			if(template.type.value == type.value)
			{
				//type.value == EmailTemplate.SET_TYPE.value && PQ APENAS NO SET O ADD TAMBËM PRECISA
				if(template.validate)
				{
					const operators = {
						'==' : function(a, b) { return a == b },
						'>'  : function(a, b) { return a >  b },
						'<'  : function(a, b) { return a <  b },
						'>=' : function(a, b) { return a >= b },
						'<=' : function(a, b) { return a <= b },
						'!=' : function(a, b) { return a != b },
					};

					for(const key in template.validate)
					{
						const item  = template.validate[key];
						const value = await data.getProperty(item.field);
	
						if(operators[item.operator](value, item.value))
						{
							isSend = true;
						}
					}
				}
				else
				{
					isSend = true;
				}
			}
	
			if(isSend)
			{			
				const _data : any = {};
				_data[key] = count + 1;
	
				data.set(_data);
			}
		//}		

		console.log('validate notification', isSend, key, count);
		
		return isSend;
	}
}
