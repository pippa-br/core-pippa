import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }        from '../../../core/plugin/base.plugin';
import { File }              from '../../../core/model/file/file';
import { FileCollection }    from '../../../core/model/file/file.collection';
import { Types } from '../../type/types';

@Injectable({
    providedIn: 'root'
})
export class UploadPlugin extends BasePlugin
{
	getMaps():any
    {
        return {
            model : File,
            list  : FileCollection,
        }
	}

    public uploadPercent : number = 0;

    async uploadMultiple(inputs:any, resize?:boolean)
    {
		const promises : any = [];

		for(const key in inputs)
		{
			const item = inputs[key];

			if(typeof item === 'object')
			{
				promises.push(this.uploadFromInputTemp(item));
			}
		}

		const results : any = await Promise.all(promises);
		
		const files = new FileCollection();

		for(const result of results)
		{
			files.add(result.data);
		}

        console.log('uploadMultiple', files);
		
		return files;
    }

    async uploadFromInputTemp(file:any)
	{
		await this.core().loadingController.start();

        let formData : FormData = new FormData();
		formData.append('file', file, file.name);
		// formData.append('form', JSON.stringify({ 				
		// 	referencePath : form.reference.path, 
		// }));
		formData.append('maccid', 	 this.core().masterAccount.code);
		formData.append('accid', 	 this.core().account.code);
		formData.append('appid', 	 'apps');
		formData.append('platform',  this.core().platform);
		formData.append('colid', 	 'documents');
		//formData.append('parseJson', 'form');
		formData.append('fields',    'maccid,accid,appid,colid,platform');

		// CREATE FILE
		const result = await this.core().api.callFormData(Types.UPLOAD_TEMP_DOCUMENT_API, formData);		

		this.core().loadingController.stop();		

		return result
	}

	async uploadFromInput(input:any, folder='upload')
	{
		//https://www.ryanbethel.org/uploading-user-images-to-google-cloud-storage
		/*let formData : FormData = new FormData();
		formData.append('upload', input, input.name);
		formData.append('maccid', this.core().masterAccount.code);
		formData.append('accid', this.core().account.code);

		const result = await this.core().api.callFormData(Types.UPLOAD_DOCUMENT_API, formData);

		return result;*/

		await this.core().loadingController.start();

		// CREATE FILE
		const result = await this.core().api.callable(Types.UPLOAD2_DOCUMENT_API, 
		{
			maccid 		 : this.core().masterAccount.code,
			accid  		 : this.core().account.code,
			originalname : input.name,
            folder       : folder,
		});

		//let formData : FormData = new FormData();
		//formData.append('--upload-file', input, input.name);

		const headers = new Headers();
		headers.append('Content-Type', input.type);

		/* SAVE FILE */
		await this.core().api.http.put(result.data.endpoint, input, { headers }).toPromise();
		delete result.data.endpoint;

		// PUBLIC FILE
		await this.core().api.callable(Types.UPLOAD_MAKE_PUBLIC_DOCUMENT_API, 
		{
			maccid 	: this.core().masterAccount.code,
			accid  	: this.core().account.code,
			data 	: result.data,
		});

		this.core().loadingController.stop();		

		return result
	}

    /*uploadFromInput(input:any, resize?:boolean)
    {
        console.log('input', input);

        return new Promise<File>((resolve) =>
        {
            if(resize && input.type.match('image.*'))
            {
                this.core().loadingController.start().then(() =>
                {
                    this.resizeImage(input, 500).then(data_url =>
                    {
                        this.uploadFromString(data_url).then((file:any) =>
                        {
                            this.core().loadingController.stop().then(() => 
                            {
                                resolve(file)
                            });                            
                        });
                    });
                });
            }
            else
            {
                this.core().loadingController.start().then(() =>
                {
                    const filePath = new Date().getTime() + '_' + input.name.replace(/[^0-9a-zA-Z.]/g, "_");
                    const fileRef  = this.core().fireStorage.ref(filePath);
                    const task     = this.core().fireStorage.upload(filePath, input);

                    this.uploadPercent = 1;

                    task.percentageChanges().subscribe((value:any) =>
                    {
                        this.uploadPercent = Math.round(value);
                    });

                    task.snapshotChanges().pipe( finalize(() =>
                    {
                        fileRef.getDownloadURL().subscribe((url:string) =>
                        {
                            const file = new File({
                                name : input.name,
                                url  : url,
                            });

                            this.core().loadingController.stop().then(() =>
                            {
                                resolve(file);
                            });                            
                        });
                    })).subscribe();
                });
            }
        });
    }*/

    async uploadFromString(data_url:any)
    {
		await this.core().loadingController.start(true);

		const result = await this.core().api.callable(Types.BASE64_UPLOAD_DOCUMENT_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			base64   : data_url,
			platform : this.core().platform,
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		return result;

        /*return new Promise<File>((resolve) =>
        {
            this.core().loadingController.start().then(() =>
            {
                const filePath = new Date().getTime() + '.png';
                const fileRef  = this.core().fireStorage.ref(filePath);
                const task     = fileRef.putString(data_url, 'data_url');

                this.uploadPercent = 1;

                task.percentageChanges().subscribe((value:any) =>
                {
                    this.uploadPercent = Math.round(value);
                });

                task.snapshotChanges().pipe( finalize(() =>
                {
                    fileRef.getDownloadURL().subscribe((url:string) =>
                    {
                        const file = new File({
                            url : url,
                        });

                        this.core().loadingController.stop().then(() => 
                        {
                            resolve(file);
                        });                        
                    });
                })).subscribe();
            });
        });*/
    }

    /* DEPOIS VERIFICAR https://github.com/nodeca/pica */
    resizeImage(input:any, size:any)
    {
        return new Promise<any>((resolve) =>
        {
            var fileTracker = new FileReader;

            fileTracker.onload = function()
            {
                var image = new Image();
                image.onload = function()
                {
                    var canvas = document.createElement("canvas");

    				/*
    				if(image.height > size) {
    					image.width *= size / image.height;
    					image.height = size;
    				}
    				*/
    				if(image.width > size) {
    					image.height *= size / image.width;
    					image.width = size;
    				}

    				var ctx = canvas.getContext("2d");
    				ctx.clearRect(0, 0, canvas.width, canvas.height);
    				canvas.width  = image.width;
    				canvas.height = image.height;

    				ctx.drawImage(image, 0, 0, image.width, image.height);

                    resolve(canvas.toDataURL("image/png"));
    			};

                image.src = this.result as string;
    		}

            fileTracker.readAsDataURL(input);

    		fileTracker.onabort = function()
            {
    			console.error("The upload was aborted.");
    		}

    		fileTracker.onerror = function()
            {
    			console.error("An error occured while reading the file.");
    		}
        });
	};
}
