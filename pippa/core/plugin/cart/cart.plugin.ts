import { Injectable } from '@angular/core';

/* PIPPA */
import { CartCollection }  from '../../model/cart/cart.collection';
import { Cart }            from '../../model/cart/cart';
import { BasePlugin }  	   from '../../../core/plugin/base.plugin';
import { RequestPlugin }   from '../../../core/plugin/request/request.plugin';
import { FormPlugin }  	   from '../../../core/plugin/form/form.plugin';
import { Params }          from '../../../core/util/params/params';
import { Result }          from '../../../core/util/result/result';
import { LogType }         from '../../type/log/log.type';
import { TransactionType } from '../../type/transaction/transaction.type';

@Injectable({
    providedIn: 'root'
})
export class CartPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Cart,
            list  : CartCollection,
        }
	}    

	/* APENAS REMOVE O CARRIONHO DA SESSÃO */
	clearCart()
	{
		this.core().cartData = null;
	}
	
	async getCart()
	{
		if(this.core().user && !this.core().user.isGuest())
		{
			/* GET FORM CART */
			let params = this.createParams({
				appid : 'cart',
			});

			const formPlugin = this.core().injector.get(FormPlugin);
			let result 	     = await formPlugin.getData(params);
			let collection   = result.collection;

			this.core().cartForm = collection[0];
			await this.core().cartForm.on();

			/* GET DATA CART */
			params = this.createParams({
				appid : 'cart',
				where : [{
					field : 'owner',
					value : this.core().user.reference,
				}]
			});

			result 	   = await this.getData(params);
			collection = result.collection;

			if(collection.length == 0)
			{
				this.core().cartData = await this.add('cart', this.core().cartForm, { data : { }});
			}
			else
			{
				this.core().cartData = collection[0];
			}
		}		
	}

	async checkout(event)
	{
		/* GET FORM REQUEST */
		let params = this.createParams({
			appid : 'request',
		});

		const formPlugin = this.core().injector.get(FormPlugin);
		let result 	     = await formPlugin.getData(params);
		let collection   = result.collection;

		const form = collection[0];
		await form.on();

		const requestPlugin = this.core().injector.get(RequestPlugin);
		result         		= await requestPlugin.add('request', form, event);
		
		this.core().cartData.del();

		await this.getCart();

		return result;
	}
}
