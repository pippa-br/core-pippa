import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }         from '../../../core/plugin/base.plugin';
import { LogPlugin }          from '../log/log.plugin';
import { GridPlugin }         from '../../plugin/grid/grid.plugin';
import { Types }              from '../../../core/type/types';
import { Grid }               from '../../../core/model/grid/grid';
import { DocumentCollection } from '../../../core/model/document/document.collection';
import { GooglePlugin } 	  from '../../../core/plugin/google/google.plugin';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ImportPlugin extends BasePlugin
{
    public logPlugin  : LogPlugin;
    public gridPlugin : GridPlugin;

    constructor(
		public googlePlugin : GooglePlugin,
    )
    {
        super();

        this.logPlugin  = this.core().injector.get(LogPlugin);
        this.gridPlugin = this.core().injector.get(GridPlugin);
    }

	async import(form, files)
	{
		await this.core().loadingController.start();

		const file = files[0];

		let formData : FormData = new FormData();
		formData.append('spreadsheet', file, file.name);
		formData.append('form', 	   JSON.stringify({ 				
			referencePath : form.reference.path, 
		}));
		formData.append('mapItems', JSON.stringify({ 				
			referencePath : environment.defaultMapItems, 
		}));
		formData.append('maccid', 	 this.core().masterAccount.code);
		formData.append('accid', 	 this.core().account.code);
		formData.append('appid', 	 form.appid);
		formData.append('platform',  this.core().platform);
		formData.append('colid', 	 'documents');
		formData.append('parseJson', 'form,mapItems');
		formData.append('fields',    'maccid,accid,appid,colid,platform');
		
		const result = await this.core().api.callFormData(Types.IMPORT_DOCUMENT_API, formData);

		result.data 	  = new Grid(result.data);
		result.collection = new DocumentCollection(result.collection);

		return result;
	}
}
