import { Injectable } from '@angular/core';

/* PIPPA */
import { LogCollection } from '../../model/log/log.collection';
import { Log }           from '../../model/log/log';
import { BasePlugin }    from '../../../core/plugin/base.plugin';
import { Params }        from '../../../core/util/params/params';

@Injectable({
    providedIn: 'root'
})
export class LogPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Log,
            list  : LogCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'logs';
	}

    prepareParams(params:Params)
    {
        params         = super.prepareParams(params);
        params.orderBy = 'postdate';
        params.asc     = false;

        return params;
    }

    async add(type:any, appid:string, dataset?:any, document?:any)
    {
        let params = this.prepareParams(this.createParams({
			appid : appid,
		}));

		const addResult = await this.core().api.callable('logApi/add', 
		{
			maccid   : params.maccid,
			accid    : params.accid,
			appid    : params.appid,
			type     : type,
			dataset  : dataset,
			document : document,
		});

		return addResult;
    }
}
