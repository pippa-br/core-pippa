import { Injectable } from '@angular/core';

/* PIPPA */
import { FormCollection }  from '../../model/form/form.collection';
import { Form }            from '../../model/form/form';
import { BasePlugin }  	   from '../base.plugin';
import { Params }          from '../../../core/util/params/params';
import { Result }          from '../../../core/util/result/result';
import { LogType }         from '../../type/log/log.type';
import { TransactionType } from '../../type/transaction/transaction.type';

@Injectable({
    providedIn: 'root'
})
export class FormPlugin extends BasePlugin
{	
    public ADD_LOG_TYPE = LogType.ADD;
    public SET_LOG_TYPE = LogType.SET;
    public DEL_LOG_TYPE = LogType.DEL;

    public ADD_EMAIL_TEMPLATE_TYPE = TransactionType.ADD_FORM_TYPE;
    public SET_EMAIL_TEMPLATE_TYPE = TransactionType.SET_FORM_TYPE;

    getMaps():any
    {
        return {
            model : Form,
            list  : FormCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'forms';
	}

	getOne(params:Params)
    {
		return new Promise((resolve) => 
		{
			this.getData(params).then(result => 
			{
				for(const key in result.collection)
				{
					if(result.collection[key].reference.id != 'master')
					{
						result.data = result.collection[key];
						break;
					}
				}

				resolve(result.data);
			});
		});
	}    
		
	/*async addLog(app:any, data:any)
	{
		return;
	}

	async afterAdd(app:any, data:any)
	{
		return;
	}
	
	async beforeSet(app:any, data:any, event:any)
	{
		return;
	}*/
}
