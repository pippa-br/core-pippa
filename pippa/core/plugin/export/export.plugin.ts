import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

/* PIPPA */
import { FieldType }        from '../../type/field/field.type';
import { Types }       	    from '../../type/types';
import { BasePlugin }       from '../../../core/plugin/base.plugin';
import { LogPlugin }        from '../log/log.plugin';
import { LogType }          from '../../type/log/log.type';
import { ExportType }       from '../../type/export/export.type';
import { GridPlugin }       from '../../plugin/grid/grid.plugin';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ExportPlugin extends BasePlugin
{
    public logPlugin  : LogPlugin;
    public gridPlugin : GridPlugin;

    constructor(
    )
    {
        super();

        this.logPlugin  = this.core().injector.get(LogPlugin);
        this.gridPlugin = this.core().injector.get(GridPlugin);
    }

    export(appid:string, data:any, gridPath:any, bookType:any, grid?:any, apiUrl?:string, where?:any, orderBy?:string, asc?:boolean)
    {
        return new Promise<void>(async (resolve) =>
        {
			await this.core().loadingController.start();
			
			let result = null;
			const app  = this.core().getApp(appid);

			if(!apiUrl)
			{
				apiUrl = Types.EXPORT_DOCUMENT_API;
			}

			where = await where.parseFilter(); 

			console.log('export', appid, app._colid, data, grid, bookType, apiUrl, where, orderBy, asc);

			if(gridPath)
			{						
				result = await this.core().api.callable(apiUrl, {
					maccid   : this.core().masterAccount.code,
					accid     : this.core().account.code,
					appid     : appid, 
					colid     : app._colid,
					groupCollection : grid ? grid.groupCollection : false,
					itemsPath : gridPath.value,
					bookType  : bookType.value,
					where     : where,
					orderBy   : orderBy,
					asc       : asc,
					mapItems : {
						referencePath : environment.defaultMapItems
					},
					...(data || {})
				});

				this.saveFile(result, bookType)

				this.core().loadingController.stop();
				
				resolve();
			}
			else
			{
				result = await this.core().api.callable(apiUrl, 
				{
					maccid   : this.core().masterAccount.code,
					accid    : this.core().account.code,
					appid    : appid, 
					colid    : app._colid,
					groupCollection : grid ? grid.groupCollection : false,
					bookType : bookType.value,
					where    : where,
					orderBy  : orderBy,
					asc      : asc,
					mapItems : {
						referencePath : environment.defaultMapItems
					},
					...(data || {})
				});

				this.saveFile(result, bookType)		

				this.core().loadingController.stop();
				
				resolve();
			}

			// else if(columnsType.value == ExportType.FORM_COLUMNS.value)
			// {	
			// 	await data.form.on(true);
				
				
			// }
			// else if(columnsType.value == ExportType.GRID_COLUMNS.value)
			// {
			// 	result = await this.core().api.callable(apiUrl, {
			// 		maccid   : this.core().masterAccount.code,
			// 		accid     : this.core().account.code,
			// 		appid     : appid, 
			// 		colid     : app.colid,
			// 		groupCollection : grid ? grid.groupCollection : false,
			// 		itemsPath : grid.reference.path,
			// 		bookType  : bookType.value,
			// 		where     : where,
			// 		orderBy   : orderBy,
			// 		asc       : asc,
			// 		mapItems : {
			// 			referencePath : environment.defaultMapItems
			// 		}
			// 	});

			// 	this.saveFile(result, bookType)					

			// 	this.core().loadingController.stop();
			// }

			resolve();
        });
    }

	saveFile(result:any, bookType)
	{
		if(result.status)
		{
			FileSaver.saveAs(result.data._url);
		}
		else
		{
			this.showAlert('Atenção', result.error);
		}
	}   
}
