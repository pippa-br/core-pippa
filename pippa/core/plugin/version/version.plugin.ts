import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin } from '../../../core/plugin/base.plugin';

//https://blog.nodeswat.com/automagic-reload-for-clients-after-deploy-with-angular-4-8440c9fdd96c

@Injectable({
    providedIn: 'root'
})
export class VersionPlugin extends BasePlugin
{
    /**
     * Checks in every set frequency the version of frontend application
     * @param url
     * @param {number} frequency - in milliseconds, defaults to 30 minutes
     */
	async initVersionCheck(url, frequency = 1000 * 60 * 30) 
	{
		return await this.checkVersion(url);

		/*setInterval(() => 
		{
            this.checkVersion(url);
        }, frequency);*/
    }

	checkVersion(url) 
	{
		return new Promise((resolve:any) => 
		{
			// timestamp these requests to invalidate caches
			this.core().api.http.get(url + '?t=' + new Date().getTime()).first().subscribe((response: any) => 
			{
				const body 		  = JSON.parse(response._body);
				const hashChanged = body.hash != this.core().versionApp;
				
				console.error(body, hashChanged, this.core().versionApp);
				
				resolve(hashChanged);
			},
			(err) => 
			{
				console.error(err, 'Could not get version');
				resolve(true);
			});
		});        
    }
}
