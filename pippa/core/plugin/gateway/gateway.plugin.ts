import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }        from '../../plugin/base.plugin';
import { EmailPlugin }       from '../../plugin/email/email.plugin';
import { Payment }           from '../../model/payment/payment';
import { PaymentCollection } from '../../model/payment/payment.collection';
import { User } 			 from '../../model/user/user';
import { Document }  		 from '../../model/document/document';
import { TransactionType } 	 from '../../type/transaction/transaction.type';
import { Types } from '../../type/types';

@Injectable({
    providedIn: 'root'
})
export class GatewayPlugin extends BasePlugin
{
    constructor(		
		public emailPlugin : EmailPlugin
    )
    {
        super();
    }

	async createPayment(data:any)
	{
        await this.core().loadingController.start(true);

		const result = await this.core().api.callable(Types.ADD_PAYMENT_PAGARME_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : data.appid,
			colid    : this.collectionName(data.appid),
			document : data,
		}, this.getMaps(), true);

		this.core().loadingController.stop();	
		
		return result;
	}
}
