import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }    from '../base.plugin';
import { VideoEmbed }    from '../../util/video.embed/video.embed';

/*
To obtain the channel id you can view the source code of the channel page and
find either data-channel-external-id="UCjXfkj5iapKHJrhYfAF9ZGg" or "externalId":"UCjXfkj5iapKHJrhYfAF9ZGg".
*/

@Injectable({
  providedIn: 'root'
})
export class VideoPlugin extends BasePlugin
{
    public apiKey = 'AIzaSyDsW8kK8uVDbmCRQw64CZRwxfk8dBHc1Vg';

    getEmbed(url:string, options = [])
	{
        options = Object.assign({
            'thumbnailUrl' : '',
			'width'        : 560,
			'height'       : 315,
			'responsive'   : true,
			'autoPlay'     : true,
		}, options);
		
		if(this.isYoutube(url))
		{
			return this.getEmbedYoutube(url, options);
		}
		else if(this.isVimeo(url))
		{
			return this.getEmbedVimeo(url, options);
		}

		return this.getEmbedDefault(url, options);
	}

	getType(url)
	{
		if(this.isYoutube(url))
		{
			return 'youtube';
		}
		else if(this.isVimeo(url))
		{
			return 'vimeo';
		}
		else
		{
			return 'html5';
		}		
	}

	getEmbedDefault(url:string, options = [])
	{
        return new Promise<VideoEmbed>((resolve) =>
        {
            let embed : any;

    		if(options['responsive'])
    		{
				//poster="'+ options['thumburl'] + '">
				embed =  '<div class="embed-responsive embed-responsive-16by9">';
					embed += '<video controls style="width: 100%;">';
						embed += '<source src="' + url + '" type="video/mp4">';
					embed += '</video>';
    			embed += '</div>';
    		}
    		else
    		{
    			embed += '<video controls style="width: 100%;">';
					embed += '<source src="' + url + '" type="video/mp4">';
				embed += '</video>';
    		}

    		const info = {
                url         : url,
    			embed        : embed,
    			thumbnailUrl : options['thumburl'],
    			type         : VideoEmbed.FILE,
    			id           : 0,
    		};

    		resolve(new VideoEmbed(info));
        });
	}

	getEmbedYoutube(url:string, options = [])
	{
        return new Promise<VideoEmbed>((resolve) =>
        {
			const id = this.getYoutubeID(url);

			let baseUrl = "https://www.googleapis.com/youtube/v3/videos?id=" + id + "&key=" + this.apiKey + "&part=snippet";
			    baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);
					            
            const params = this.createParams({
                apiUrl : baseUrl,
				method : 'get',
				withCredentials : false,				
            });

            this.core().api.request(params).then((data:any) =>
            {
				if(data)
				{
					console.log('load youtube', id, data);
					/*let embed;
	
					if(options['responsive'])
					{
						embed  = '<div class="embed-responsive embed-responsive-16by9">';
						embed += '<iframe class="embed-responsive-item" id="video-' + id + '" width="' + options['width'] + '" height="' +
								   options['height'] + '" src="https://www.youtube.com/embed/' + id + '?autohide=1&rel=0&autoplay=' + (options['autoPlay'] ? '1' : '0') + '" frameborder="0" allowfullscreen></iframe>';
						embed += '</div>';
					}
					else
					{
						embed = '<iframe id="video-' + id + '" width="' + options['width'] + '" height="' + options['height'] + '" src="https://www.youtube.com/embed/' +
								 id + '?autohide=1&rel=0&autoplay=' + (options['autoPlay'] ? '1' : '0') + '" frameborder="0" allowfullscreen></iframe>';
					}*/
	
					/* DEFINE THUMB */
					let thumbnail    = options['thumburl'];
					const dimensions = ['standard', 'high','medium','default'];
	
					if(data.items[0])
					{
						const thumbnails = data.items[0].snippet.thumbnails;
	
						if(thumbnails)
						{
							for(const key in dimensions)
							{
								const key2  = dimensions[key];
	
								if(thumbnails[key2])
								{
									thumbnail = thumbnails[key2];
									thumbnail = thumbnail.url;
									break;
								}
							}
						}
					}
	
					const item = {
						url          : url,
						thumbnailUrl : thumbnail,
						type         : VideoEmbed.YOUTUBE,
						id           : id,
						//time  : (data.data.duration ? data.data.duration : 0)
					};
	
					resolve(new VideoEmbed(item));					
				}
				else
				{
					resolve(null);
				}                
            });
        });
	}

	getEmbedVimeo(url:string, options = [])
	{		
        return new Promise<VideoEmbed>((resolve) =>
        {
            const id = this.getVimeoID(url);
            const params = this.createParams({
                //apiUrl          : 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' + id, //* PQ? https://vimeo.com/207795760/627b95131b
                apiUrl          : 'https://vimeo.com/api/oembed.json?url=' + url,
                withCredentials : false,
                method          : 'get',
            });

            this.core().api.request(params).then((data:any) =>
            {
				if(data)
				{
					/*let embed;

					if(options['responsive'])
					{
						embed = '<div class="embed-responsive embed-responsive-16by9">';
						embed += '<iframe class="embed-responsive-item" id="video_' + id + '" width="' + options['width'] + '" height="' +
								options['height'] + '" src="https://player.vimeo.com/video/' + id + '?badge=0&server=vimeo.com&show_title=1&show_byline=1&autoplay=0&api=1&player_id=video_' +
								id + '" frameborder="0" allowfullscreen></iframe>';
						embed += '</div>';
					}
					else
					{
						embed = '<iframe id="video_' + id + '" width="' + options['width'] + '" height="' + options['height'] +
								'" src="https://player.vimeo.com/video/' + id + '?badge=0&server=vimeo.com&show_title=1&show_byline=1&autoplay=0&api=1&player_id=video_' +
								id + '" frameborder="0" allowfullscreen></iframe>';
					}*/

					const item = {
						url          : url,
						embed        : data.html,
						thumbnailUrl : options['thumbnailUrl'] != '' ? options['thumbnailUrl'] : (data.thumbnail_url ? data.thumbnail_url.replace('_295x166', '_600') : ''),
						type         : VideoEmbed.VIMEO,
						id           : id,
					};

					console.log('embed', item);

					item['embed'] = item['embed'].replace('&', '&amp;');

					resolve(new VideoEmbed(item));
				}
				else
				{
					console.error('url denied', url);
					resolve(null);
				}
            });
        });
	}

	isYoutube(url:string)
	{
		return url.match(/(http:|https:)?\/\/(www\.)?(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/);
	}

	getYoutubeID(url:string)
	{
		if(this.isYoutube(url))
		{
			let pattern = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
			let matches = url.match(pattern);

			if (matches.length)
			{
				return matches[1];
			}
		}

		return null;
	}

	isVimeo(url:string)
	{
		//return url.match(/^(http:\/\/|https:\/\/)(vimeo\.com)\/([\w\/]+)([\?].*)?$/i);

		var exp = new RegExp(/(vimeo\.com)/);
    	return exp.test(url);
	}

	isFile(url:any)
	{
		const pattern = '/^(?:[;\/?:@&=+,]|(?:[^\W_]|[-_.!~*\()\[\] ])|(?:%[\da-fA-F]{2}))*/';

		if(url.match(pattern) == 1)
		{
		   // url is valid
		   return false;
		}

		return true;
	}

	getVimeoID(url:string)
	{
		if(this.isVimeo(url))
		{
			let pattern = /\/\/(www\.)?vimeo.com\/(\d+)(|\/)/i;
			let matches = url.match(pattern);

			if (matches.length)
			{
				return matches[2];
			}
		}

		return '';
	}
}
