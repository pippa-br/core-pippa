import { Injectable } from '@angular/core';

/* PIPPA */
import { Grid }           from '../../model/grid/grid';
import { GridCollection } from '../../model/grid/grid.collection';
import { BasePlugin } 	  from '../base.plugin';

@Injectable({
    providedIn: 'root'
})
export class GridPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Grid,
            list  : GridCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'grids';
	}
}
