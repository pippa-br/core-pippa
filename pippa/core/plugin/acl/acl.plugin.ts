import { Injectable } from '@angular/core';

/* PIPPA */
import { Acl }           from '../../model/acl/acl';
import { AclCollection } from '../../model/acl/acl.collection';
import { BasePlugin }    from '../../../core/plugin/base.plugin';
import { Params }        from '../../../core/util/params/params';
import { Result }        from '../../../core/util/result/result';
import { Types } from '../../type/types';

@Injectable({
    providedIn: 'root'
})
export class AclPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Acl,
            list  : AclCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'acls';
	}

	getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_DOCUMENT_API,
			getEndPoint  : Types.GET_DOCUMENT_API,
			addEndPoint  : Types.ADD_ACL_API,
			setEndPoint  : Types.SET_DOCUMENT_API,
			delEndPoint  : Types.DEL_DOCUMENT_API,
        }
	}

    /*getData(params:Params):Promise<Result>
    {
        params = this.prepareParams(params);
        return this.api().getList(params);
    }

    getReference(params:Params):Promise<Result>
    {
        params = this.prepareParams(params);
        return this.api().getListReference(params);
    }*/

    async get(params:Params)
    {
        params 		 = this.prepareParams(params);
		const result = new Result(params);
		const acl	 = new Acl();	
		acl.exists   = false; // DEPOIS REMOVER POIS HOJE TODO MODEL TEM EXISTE TRUE	

		for(let group of params.groups)
			{
				if(typeof group == 'string')
				{
					group = {
						label : group,
						value : group
					}
				}

				const params2 = this.prepareParams(params);
				params2.id    = group.value;

				const resultData = await super.get(params2);

				if(resultData.data)
				{
						await resultData.data.reload();

						acl.merge(resultData.data);
						acl.reference = resultData.data.reference;		
				}							
		}

		if(acl.exists)
		{
			console.log('load acl', acl);			
		}
		else
		{
			console.error('acl not found', acl);
		}	
		
		result.data   = acl;
		result.status = true;

		return result;
    }
}
