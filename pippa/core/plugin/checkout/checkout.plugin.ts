import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }      from '../base.plugin';
import { DocumentPlugin }  from '../../plugin/document/document.plugin';
import { Form }            from '../../model/form/form';
import { OneCheckoutForm } from '../../form/one.checkout.form';
import { FormType }    	   from '../../type/form/form.type';
import { GatewayPlugin } from '../gateway/gateway.plugin';

@Injectable({
    providedIn: 'root'
})
export class CheckoutPlugin extends BasePlugin
{
    constructor(
        public documentPlugin : DocumentPlugin,
    )
    {
        super();
    }

    async oneCheckout(data, setting)
    {
		let formReference : any;

		if(setting.formPath)
		{
			formReference = new Form({
				referencePath : setting.formPath
			});
		}
		else if(setting.form)
		{
			formReference = setting.form;
		}

		if(formReference)
		{
			await formReference.on();
			
			if(formReference.exists)
			{
				if(data.prices && data.prices.length > 0)
				{							
					const checkoutForm : any = await OneCheckoutForm.create(data, setting);

					checkoutForm.complete = async (event:any) =>
					{									    
						const documentPlugin = this.core().injector.get(DocumentPlugin);

						/* PRODUTO A SER COMPRADO */
						if(setting.productField)
						{
							event.data[setting.productField] = data.reference;
						}
						else
						{
							event.data.product = data.reference;
						}

						/* COMPRADOR */
						if(setting.buyerField)
						{
							event.data[setting.buyerField] = this.core().user.reference;
						}
						else
						{
							event.data.buyer = this.core().user.reference;
						}

						event.data.freeInstallments = event.data.type.freeInstallments;
						event.data.paymentMethod    = event.data.type.paymentMethod;
						event.data.maxInstallments  = event.data.type.maxInstallments;
						event.data.interestRate     = event.data.type.interestRate || null;
						event.data.price            = event.data.coupon ? event.data.type.value : event.data.type.amount;

						const result = await documentPlugin.add(formReference.appid, formReference, event);					

						this.onAction(formReference, result.data, FormType.ADD_MODE);

						return result;
					};

					return checkoutForm;
				}
				else /* PRODUTO SEM PRECO */
				{
					const documentPlugin = this.core().injector.get(DocumentPlugin);

					const data2 : any = {}

					/* PRODUTO A SER COMPRADO */
					if(setting.productField)
					{
						data2[setting.productField] = data.reference;
					}
					else
					{
						data2.product = data.reference;
					}

					/* COMPRADOR */
					if(setting.buyerField)
					{
						data2[setting.buyerField] = this.core().user.reference;
					}
					else
					{
						data2.buyer = this.core().user.reference;
					}

					data2.price   = 0;
					data2.payment = 
					{						
						status : 'paid'
					}					

					const result = await documentPlugin.add(formReference.appid, formReference, { data : data2 });

					this.onAction(formReference, result.data, FormType.ADD_MODE);

					return result;

					/*const gateway  = await formReference.getProperty('gateway');					
					
					let redirectRouter : string = gateway.redirectRouter;
					let redirectParams : any    = gateway.redirectParams;

					redirectParams.documentPath = document.reference.path;
		
					this.push(redirectRouter, redirectParams);*/
				}						     											
			}
			else
			{
				console.error('form nor exists');
			}
		}
    } 

	/* DEPOIS JUNTAR COM O FORM DOCUMENT */
	async onAction(form:any, data:any, mode:any)
	{
		console.log('actions', form.actions);

		if(form.actions && form.actions.length > 0)
		{
			for(const key in form.actions)
			{
				const item = form.actions[key];

				if(item.mode.value == mode.value && this.core().user && this.core().user.hasGroups(item.groups))
				{
					if(item.action.value == FormType.ALERT_ACTION.value)
					{
						this.showAlert('', item.message);
					}
					else if(item.action.value == FormType.REDIRECT_ACTION.value)
					{
						item.params.documentPath = data.reference.path;
						this.push(item.router, item.params);
					}
					else if(item.action.value == FormType.PAYMENT_ACTION.value)
					{
						const gatewayPlugin = this.core().injector.get(GatewayPlugin);
						await gatewayPlugin.createPayment(data);

						item.params.documentPath = data.reference.path;
						this.push(item.router, item.params);
					}
				}
			}
		}
	}
}