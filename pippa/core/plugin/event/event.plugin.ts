import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { map } from 'rxjs/operators';

/* PIPPA */
import { BasePlugin } from '../base.plugin';

//@dynamic
@Injectable({
  	providedIn: 'root'
})
export class EventPlugin extends BasePlugin
{
    static _event: Subject<any>;

	static getEvent()
	{
		if(!EventPlugin._event)
        {
            EventPlugin._event = new Subject<any>();
		}
		
		return EventPlugin._event;
	}

    dispatch(key: any, data?: any)
    {
        console.log('event', key, data);
        EventPlugin.getEvent().next({key, data});
    }

    on(key: any)
    {
        return EventPlugin.getEvent().asObservable().pipe(filter((event:any) => event.key == key), map((event:any) => event.data));
    }
}
