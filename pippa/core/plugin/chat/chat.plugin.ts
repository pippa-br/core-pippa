import { Injectable } from '@angular/core';
import { serverTimestamp } from 'firebase/firestore';

/* PIPPA */
//import { OneSignalNative } from '../../../ionic/native/onesignal/onesignal.native';
import { Chat }            from '../../model/chat/chat';
import { ChatCollection }  from '../../model/chat/chat.collection';
import { BasePlugin }      from '../../../core/plugin/base.plugin';
import { Params }          from '../../../core/util/params/params';
import { Result }          from '../../../core/util/result/result';

@Injectable({
    providedIn: 'root'
})
export class ChatPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Chat,
            list  : ChatCollection,
        }
    }

    getData(params:Params)
    {
        params         = this.prepareParams(params);
        params.path    = params.path + params.sender.id + '/chats';
		params.orderBy = 'lastdate';
		params.asc     = false;

        return this.api().getList(params);
    }

    addChat(params:Params)
    {
        return new Promise<Result>(async (resolve) =>
        {
			// RECEIVER
			params       = this.prepareParams(params);
			params.path  = params.path + params.receiver.id + '/chats/' + params.sender.id + (params.target ? '-' + params.target.id : '');
			params.model = Chat;
			  let result = await this.core().api.getObject(params);
			  
			if(!result.data.exists())
			{
				await result.data.create({
					accid    : this.core().account.code,
					appid    : params.appid,
					sender   : params.receiver, /* AQUI INVERTE */
					receiver : params.sender, /* AQUI INVERTE */
					target   : params.target ? params.target : null,
					postdate : serverTimestamp()
				});
			}

            // SENDER
            params.path  = params.path + params.sender.id + '/chats/' + params.receiver.id + (params.target ? '-' + params.target.id : '');
            params.model = Chat;
			  	  result = await this.core().api.getObject(params);

			if(!result.data.exists())
			{
				await result.data.create({
					accid    : this.core().account.code,
					appid    : params.appid,
					sender   : params.sender,
					receiver : params.receiver,
					target   : params.target ? params.target : null,
					postdate : serverTimestamp()
				});
			}

			await result.data.on();

			resolve(result);
        });
    }

    setChat(params:Params)
    {
        return new Promise(async () =>
        {
            // SET SENDER
            await this.setSender(params);

            // RECEIVER USER
            /* IF PUBLIC UPDATE TODOS OS CHATS */
            if(params.receiver.public)
            {
                params         = this.prepareParams(params);
                params.path    = params.path + params.receiver.id + '/chats';
                params.orderBy = 'lastdate';

                this.api().getList(params).then(result =>
                {
                    params.sender = params.receiver;

                    /* NÃO PERMITIR REENVIOU */
                    const sends = [];

                    result.collection.on().then(async () => 
                    {
                        for(const key in result.collection)
                        {                            
                            const item = result.collection[key];
    
                            const params2    = params.copy();
                            params2.receiver = item.sender;

                            if(sends.indexOf(params2.receiver.id) == -1)
                            {
                                sends.push(params2.receiver.id);                        
                                await this.setReceiver(params2);    
                            }
                        }
                    })                    
                })
            }
            else
            {
                await this.setReceiver(params);
            }
        });
    }

    setSender(params:Params)
    {
        return new Promise<void>(async (resolve) =>
        {
			// SENDER
			params       = this.prepareParams(params);
            params.path  = params.path + params.sender.id + '/chats/' + params.receiver.id + (params.target ? '-' + params.target.id : '');
            params.model = Chat;
			const result = await this.core().api.getObject(params);

			if(result.data.exists())
			{
				const data : any = {};
				data.lastdate = serverTimestamp();

				if(result.data && result.data.count != undefined)
				{
					data.count = result.data.count + 1;
				}
				else
				{
					data.count = 1;
				}

				/* LAST MESSAGE */
				if(params.data)
				{
					data.lastMessage = params.data;
				}
				
				await result.data.set(data);

				resolve();
			}				
        });
    }

    setReceiver(params:Params)
    {
        return new Promise<void>(async (resolve) =>
        {			
			// RECEIVER
			params       = this.prepareParams(params);
			params.path  = params.path + params.receiver.id + '/chats/' + params.sender.id + (params.target ? '-' + params.target.id : '');
			params.model = Chat;
			  let result = await this.core().api.getObject(params);

			if(result.data.exists())
			{
				const data : any = {};
				data.lastdate = serverTimestamp();

				if(result.data && result.data.count != undefined)
				{
					data.count = result.data.count + 1;
				}
				else
				{
					data.count = 1;
				}

				/* LAST MESSAGE */
				if(params.data)
				{
					data.lastMessage = params.data;
				}

				await result.data.set(data);

				resolve();
			}
        });
    }
}
