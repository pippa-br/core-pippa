import { Injectable } from '@angular/core';

/* PIPPA */
import { NotificationTemplate }           from '../../model/notification.template/notification.template';
import { NotificationTemplateCollection } from '../../model/notification.template/notification.template.collection';
import { BasePlugin } 				  	  from '../../plugin/base.plugin';
import { Params }         		 		  from '../../util/params/params';
import { Form }       					  from '../../model/form/form';

@Injectable({
    providedIn: 'root'
})
export class NotificationTemplatePlugin extends BasePlugin 
{
    getMaps():any
    {
        return {
            model : NotificationTemplate,
            list  : NotificationTemplateCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'notification.template';
	}    
}
