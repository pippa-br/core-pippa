import { Injectable } from '@angular/core';

/* PIPPA */
import { Option }           from '../../model/option/option';
import { OptionCollection } from '../../model/option/option.collection';
import { BasePlugin }   	from '../base.plugin';
import { Params }           from '../../util/params/params';
import { Types } from '../../type/types';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class OptionPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Option,
            list  : OptionCollection,
        }
	}

	appidName()
	{
		return 'option';
	}

	async get(params:Params)
    {
		params = this.prepareParams(params);
 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.GET_DOCUMENT_API, 
		{
			maccid   : params.maccid,
			accid    : params.accid,
            appid    : params.appid,
			colid    : params.colid,
            code     : params.code,
			platform : params.platform,
			map      : true,
        	mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;        
    }

	/*getOne(params:Params)
    {
		return new Promise((resolve) => 
		{	
			this.getData(params).then(result => 
			{
				for(const key in result.collection)
				{
					result.data = result.collection[key];
					break;
				}

				resolve(result.data);
			});
		});
    }*/
}
