import { Injectable } from '@angular/core';

/* PIPPA */
import { Chart }           from '../../model/chart/chart';
import { ChartCollection } from '../../model/chart/chart.collection';
import { BasePlugin }  	   from '../../plugin/base.plugin';

@Injectable({
    providedIn: 'root'
})
export class ChartPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Chart,
            list  : ChartCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'chart';
	}

	async getDataChart(params:any)
	{
		await this.core().loadingController.start(true);

		params = this.prepareParams(params);

		// FEITO AQUI POIS O collectionName é do chatPlugin
		const app = this.core().getApp(params.appid);

		if(app && app._colid)
		{
				params.colid = app._colid;
		}
		else
		{
				params.colid = 'documents';
		}

		const result = await this.api().callable('chartApi/' + params.chart.type.value, 
		{
				maccid  : params.accid,
				accid   : params.accid,
				appid   : params.appid,
				colid   : params.colid,
				where   : await params.where.parseFilter(),
				chart   : params.chart,
				orderBy : params.orderBy,
				asc     : params.asc,
				perPage : params.perPage,
				paginationType : params.paginationType || null,
				noCache : true, 
		});

		await this.core().loadingController.stop();

		return result;
	}
}
