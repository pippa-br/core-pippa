import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

/* PIPPA */
import { Core } from '../../util/core/core';

@Injectable({
    providedIn: 'root'
})
export class StoragePlugin
{
    async get(key:string)
    {
		const result : any = await Storage.get({ key: key });

		try 
		{
			const value = JSON.parse(result.value);
			return value;
		} 
		catch (e) 
		{
			return result.value;
		}
	}

    async set(key:string, value:any)
    {	
		if(typeof value == 'object')
		{
			value = JSON.stringify(value)
		}	
		
		await Storage.set({
			key   : key,
			value : value,
		});
    }

    async del(key:string)
    {
		return await Storage.remove({ key: key });
    }

    async clear()
    {
		return await Storage.clear();
    }

    /* USER METHODS */
    async setUser(user:any)
    {
        return await this.set(this.core().account.code + '_logged_user', user);
    }

    async getUser()
    {
        return await this.get(this.core().account.code + '_logged_user');
    }

    async delUser()
    {
        return await this.del(this.core().account.code + '_logged_user');
    }
    /* ----- */

    /* AUTH METHODS */
    async setAuth(auth:any)
    {
        return await this.set(this.core().account.code + '_auth_user', auth);
    }

    async getAuth()
    {
        return await this.get(this.core().account.code + '_auth_user');
    }

    async delAuth()
    {
        return await this.del(this.core().account.code + '_auth_user');
    }
    /* ----- */

    core()
    {
        return Core.get()
    }
}
