/* PIPPA */
import { BasePlugin } from './base.plugin';

//@dynamic
export abstract class BaseGeneralPlugin extends BasePlugin
{
    goMenu(menu:any)
    {
        this.core().goMenu(menu);
    }

    goFirstMenu()
    {
        this.core().goFirstMenu();
    }
}
