import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }    from '../base.plugin';

/*
To obtain the channel id you can view the source code of the channel page and
find either data-channel-external-id="UCjXfkj5iapKHJrhYfAF9ZGg" or "externalId":"UCjXfkj5iapKHJrhYfAF9ZGg".
*/

@Injectable({
    providedIn: 'root'
})
export class YoutubePlugin extends BasePlugin
{
    //public apiKey = 'AIzaSyDsW8kK8uVDbmCRQw64CZRwxfk8dBHc1Vg';

    isValid(url:string)
	{
		return url.match(/(http:|https:)?\/\/(www\.)?(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/);
	}

    getPlaylistId(url:string)
    {
        const reg   = new RegExp("[&?]list=([a-z0-9_-]+)","i");
        const match = reg.exec(url);

        if (match && match[1].length > 0 && this.isValid(url))
        {
            return match[1];
        }
        else
        {
            return null;
        }
	}
	
	getYoutubeID(url:string)
	{
		if(this.isValid(url))
		{
			let pattern = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
			let matches = url.match(pattern);

			if (matches.length)
			{
				return matches[1];
			}
		}

		return null;
	}

    getPlaylist(channelId:string, pageToken?:string):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if(channelId)
            {
				this.core().loadingController.start().then(() =>
                {
					let baseUrl = 'https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=' + channelId + '&key=' + this.core().account.google.apiKey + '&maxResults=48&order=date';                    

					if(pageToken)
					{
						baseUrl += "&pageToken=" + pageToken;
					}

					//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

					const data = {
						url : baseUrl,
					};

					this.core().api.callable('utilApi/proxy', data).then((data:any) =>
					{
						this.core().loadingController.stop();

						resolve(data);
					});
				});
            }
            else
            {
                resolve(null);
            }
        });
    }

    getLive(channelId:string):Promise<any>
    {
        return new Promise((resolve) =>
        {
			this.core().loadingController.start().then(() =>
            {
				let baseUrl  = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=' + channelId + '&type=video&eventType=live&key=' + this.core().account.google.apiKey + '';
				//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);
	
				const data = {
					url : baseUrl,
				};
	
				this.core().api.callable('utilApi/proxy', data).then((data:any) =>
				{
					console.log('getLive', data, baseUrl, baseUrl);

					this.core().loadingController.stop();
	
					resolve(data);
				});	
			});
        });
    }

    getVideosByPlaylist(playlistId:string, pageToken?:string):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if(playlistId)
            {
				this.core().loadingController.start().then(() =>
                {
					let baseUrl = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=' + playlistId + '&key=' + this.core().account.google.apiKey + '&maxResults=48&order=published';                    

					if(pageToken)
					{
						baseUrl += "&pageToken=" + pageToken;
					}

					//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

					const data = {
						url : baseUrl,
					};

					this.core().api.callable('utilApi/proxy', data).then((data:any) =>
					{
						console.log('getVideosByPlaylist', data, baseUrl);

						this.core().loadingController.stop();

						resolve(data);
					});
				});
            }
            else
            {
                resolve(null);
            }
        });
    }

    getVideosByChannel(channelId:string, term:string, pageToken?:string, perPage?:number):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            if(channelId)
            {
				this.core().loadingController.start().then(() =>
            	{
					let baseUrl = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=' + channelId + '&key=' + this.core().account.google.apiKey + '&q=' + term + '&maxResults=' + (perPage ? perPage : 48) + '&order=date';

					if(pageToken)
					{
						baseUrl += "&pageToken=" + pageToken;
					}

					//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

					const data = {
						url : baseUrl,
					};

					this.core().api.callable('utilApi/proxy', data).then((data:any) =>
					{
						console.log('getVideosByChannel', data, baseUrl);

						this.core().loadingController.stop();

						resolve(data);
					});
				});
            }
            else
            {
                resolve(null);
            }
        });
    }
}
