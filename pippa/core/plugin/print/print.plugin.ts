import { Injectable } from '@angular/core';
import * as printJS from 'print-js'

/* PIPPA */
import { BasePlugin }       from '../../plugin/base.plugin';
import { TransformFactory } from '../../../core/factory/transform/transform.factory';
import { FieldType }        from '../../type/field/field.type';

@Injectable({
    providedIn: 'root'
})
export class PrintPlugin extends BasePlugin
{
    async printThermalViewer(viewer:any, data:any)
    {
		console.log('print');

		let html : string = '<table cellspacing="0">';
		
		/* HEADER */
		const header = viewer.getAttachments('imageHeader')

		if(header)
		{
			html += '<tr>';
				html += '<td colspan="2" style="text-align:center;font-size: 9pt;">';
					html += header.comment.replace(/(?:\r\n|\r|\n)/g, '<br>');
				html += '</td>';
			html += '</tr>';
		}

		html += '<tr>';
			html += '<td colspan="2" style="text-align:center;font-size: 9pt;">';
				html += await this.drawTable(viewer.items, [data]);
			html += '</td>';
		html += '</tr>';

		/* FOOTER */
		const footer = viewer.getAttachments('imageFooter')

		if(footer)
		{
			html += '<tr>';
				html += '<td colspan="2" style="text-align:center;font-size: 9pt;">';
					html += footer.comment.replace(/(?:\r\n|\r|\n)/g, '<br>');;
				html += '</td>';
			html += '</tr>';
		}
		
		html += '</table>';

		printJS({
			printable : html,
			type      : 'raw-html',
			style     : 'table {width:100%; border-spacing:0; border-collapse: collapse;font-size: 11pt; font-family: "Myriad Pro", Arial, Helvetica, Liberation Sans, sans-serif;} table td { border : solid 1px #000000; padding:5px;}',
		})
	}
	
	async drawTable(items:any, list:any)
	{
		const promises : any   = [];
		let html 	  : string = '<table cellspacing="0">';

		for(const data of list)
		{
			for(const formItem of items)
			{
				promises.push(TransformFactory.transform(formItem, data));
			}        
	
			const values = await Promise.all(promises);	        
	
			for(const key2 in items)
			{
				if(items[key2].field.type.value == FieldType.type('QRCode').value)
				{
					if(values[key2].value)
					{
						html += '<tr>';
							html += '<td colspan="2" style="text-align:center;">';
								html += '<img src="' + values[key2].format  + '" width="150"/>';
							html += '</td>';
						html += '</tr>';
					}                    
				}
				else if(items[key2].field.type.value == FieldType.type('SubForm').value)
				{
					html += '<tr>';
						html += '<td colspan="2">';
							html += values[key2].label;
						html += '</td>';
					html += '</tr>';

					html += '<tr>';
						html += '<td colspan="2">';
							html += await this.drawTable(values[key2].grid.items, values[key2].value);
						html += '</td>';
					html += '</tr>';
				}
				else
				{
					html += '<tr>';
						html += '<td>';
							html += values[key2].label;
						html += '</td>';
						html += '<td>';
							html += values[key2].format;
						html += '</td>';
					html += '</tr>';
				}
			}
		}        

		html += '</table>';

		return html;
	}
}
