import { Injectable } from '@angular/core';
import { serverTimestamp } from 'firebase/firestore';

/* PIPPA */
import { Message }           from '../../model/message/message';
import { MessageCollection } from '../../model/message/message.collection';
import { DocumentPlugin }  	 from '../../../core/plugin/document/document.plugin';
import { Params }            from '../../../core/util/params/params';
import { Result }            from '../../../core/util/result/result';
import { FieldType }         from '../../../core/type/field/field.type';

@Injectable({
    providedIn: 'root'
})
export class MessagePlugin extends DocumentPlugin
{
    getMaps():any
    {
        return {
            model : Message,
            list  : MessageCollection,
        }
    }

    /* MESSAGES */
    getData(params:Params):Promise<Result>
    {
        return new Promise<Result>(async (resolve) =>
        {
			// VERIFY LIST 1
			params 		 = this.prepareParams(params);
			params.path  = params.path + params.chat.sender.id   + '/messages/' + params.chat.receiver.id;
			params.model = Message;
			  let result = await this.core().api.getObject(params);
			  
			if(result.data.exists())
			{
				params         = this.prepareParams(params);
				params.path    = params.path + '/messages';
				params.orderBy = 'postdate';
				
				if(params.chat.target)
				{	
					params.where = [{
						field : 'target',
						value : params.chat.target.reference,
						type  : FieldType.type('ReferenceSelect'),
					}];
				}

				this.api().getList(params).then(result =>
				{
					resolve(result);
				});
			}
			else
			{
				// VERIFY LIST 2
				params.path  = params.accid + '/' + params.appid + '/documents/' + params.chat.receiver.id + '/messages/' + params.chat.sender.id;
				params.model = Message;
				let result   = await this.core().api.getObject(params);

				if(!result.data.exists())
				{
					await result.data.create({
						postdate : serverTimestamp()
					});					
				}

				params         = this.prepareParams(params);
				params.path    = params.path + '/messages';
				params.orderBy = 'postdate';
				
				if(params.chat.target)
				{	
					params.where = [{
						field : 'target',
						value : params.chat.target.reference,
						type  : FieldType.type('ReferenceSelect'),
					}];
				}

				this.api().getList(params).then(result =>
				{
					resolve(result);
				});
			}            
        });
    }
}
