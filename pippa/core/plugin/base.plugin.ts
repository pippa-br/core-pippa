import { Injectable } from '@angular/core';

/* APPP */
import { Core } from '../util/core/core';
import { AlertController } from '@ionic/angular';

/* PIPPA */
import { Params } from '../util/params/params';
import { Result } from '../util/result/result';
import { Types }  from '../type/types';
import { BaseList } from '../model/base.list';
import { Query } from '../util/query/query';
import { environment } from 'src/environments/environment';

//@dynamic
@Injectable({
	providedIn: 'root'
})
export abstract class BasePlugin
{
	public app 		   : any;
	public onlyMaster  : boolean = true; // DADOS QUE SÃO PUXADOS APENAS DO ACCOUNT MASTER

    getMaps():any
    {
        return {
        }
	}

	getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_DOCUMENT_API,
			getEndPoint  : Types.GET_DOCUMENT_API,
			addEndPoint  : Types.ADD_DOCUMENT_API,
			setEndPoint  : Types.SET_DOCUMENT_API,
			delEndPoint  : Types.DEL_DOCUMENT_API,
        }
	}

	collectionName(appid:string)
	{
		const app = this.core().getApp(appid);

		if(app && app._colid)
		{
			return app._colid;
		}
		else
		{
			return 'documents';
		}
	}

	appidName()
	{
		return '';
	}

    prepareParams(params:Params)
    {
				const maps = this.getMaps();  
				
				if(!params.model)
				{
					params.model = maps.model;
				}
				
				if(!params.collectionModel)
				{
					params.collectionModel = maps.list;
				}        

				if(!params.maccid)
				{
						params.maccid = this.core().masterAccount.code; 
				}

        if(!params.accid)
        {
            params.accid = this.core().account.code; 
        }

				if(!params.appid)
        {
            params.appid = this.appidName(); 
        }

				if(!params.colid) // SEMPRE PASSAR POIS TEM documents, grids, forms, etc
        {
            params.colid = this.collectionName(params.appid); 
        }

				if(params.onlyMaster === undefined)
				{
					params.onlyMaster = this.onlyMaster;
				}

				if(params.platform === undefined)
				{
					params.platform = this.core().platform;
				}		

				if(params.mapItems === undefined)
				{
					params.mapItems = {
						referencePath : environment.defaultMapItems
					};
				}		

        return params.copy();
    }

	createCollection(params:any)
	{
		//const reference : any = this.core().fireStore.collection(params.path); // PEGA APENAS A REFERENCIA
    
		//console.log('-------', this.core().fireStore.firestore.collection(params.path));

		//let unsubscribe = reference.query.onSnapshot(snapshot =>
		//{
		//unsubscribe();

		params 		   = this.prepareParams(params);		
		let collection = this.createResult(params);

		console.log('getList', params.accid, params.appid, params.where, params.page);

		if(params.collectionModel)
		{
			collection = new params.collectionModel();
		}
		else
		{
			collection = new BaseList();
		}
		
		//const counResult = await callable({path:params.path}).toPromise();		

		const apis = this.getApis();

		const query = new Query({
			//query   : reference.query,
			listEndPoint    : apis.listEndPoint,
			where    		: params.where,
			orderBy  		: params.orderBy,
			asc      		: params.asc,
			perPage  		: params.perPage,
			page     		: params.page,
			maccid   		: params.maccid,
			accid    		: params.accid,
			appid    		: params.appid,
			colid    		: params.colid,
			map		 		: params.map,
			mapItems 		: params.mapItems,
			groupCollection : params.groupCollection,
			onlyMaster 		: params.onlyMaster,
			loading 		: params.loading,
			platform 		: params.platform,
			searchVector : params.searchVector,
			//_count  : counResult.count,
		});

		//collection.accid 	  = params.accid;
		//collection.colid 	  = params.colid;
		collection.appid 	  = params.appid;
		collection.autoReload = params.autoReload;
		collection.onlyCount  = params.onlyCount;
		collection.setQuery(query);
		//result.collection.populate(snapshot.docs);
		//result.collection.reference = reference;

		/* RUN QUERY */
		/*let query = reference.query;

		if(params.where && params.where.length > 0)
		{
			for(let key in params.where)
			{
				let item = params.where[key];

				query = query.where(item['field'], item['operator'], item['value']);
			}
		}

		if(params.orderBy)
		{
			query = query.orderBy(params.orderBy, (params.asc ? 'asc' : 'desc'));
		}

		//query = query.orderBy('postdate', 'asc').startAt(5);
		query = query.limit(params.perPage);*/

		return collection;
	}

    async get(params:Params)
    {   
				const apis = this.getApis();
        params = this.prepareParams(params);   
		
				if(!params.path)
				{
					params.path = (this.onlyMaster ? params.maccid : params.accid) + '/' + params.appid + '/' + this.collectionName(params.appid) + '/' + params.id;
				}		

        console.log('get', params.path);

        await this.core().loadingController.start();

				const result = await this.core().api.callable(apis.getEndPoint, 
				{
					maccid   : this.core().masterAccount.code,
					accid    : this.core().account.code,
					appid    : params.appid,
					colid    : this.collectionName(params.appid),
					document : {
						referencePath : params.path
					},
					mapItems : params.mapItems
				}, this.getMaps());

				this.core().loadingController.stop();

				return result;
	}

	async add(appid:string, form:any, event:any, loading?:boolean, typeLog?:any)
    {		
		const apis = this.getApis();
		await this.core().loadingController.start(loading);

		const result = await this.core().api.callable(apis.addEndPoint, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : appid,
			colid    : this.collectionName(appid),			
			form     : form,
			data     : event.data,
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		// ATUALIZA OBJETOS DO CORE
		await this.updateCore(appid);
		
		return result;
	}

	async adds(appid:string, form:any, event:any, loading?:boolean, typeLog?:any)
    {		
		await this.core().loadingController.start(loading);

		const result = await this.core().api.callable(Types.ADDS_DOCUMENT_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : appid,
			colid    : this.collectionName(appid),			
			form     : form,
			data     : event.data,
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		// ATUALIZA OBJETOS DO CORE
		await this.updateCore(appid);
		
		return result;
	}

	async set(appid:string, data:any, form:any, event:any, loading?:boolean, typeLog?:any)
    {
		const apis = this.getApis();
		await this.core().loadingController.start(loading);

		const result = await this.core().api.callable(apis.setEndPoint, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : appid,
			colid    : this.collectionName(appid),	
			form     : form,
			document : data,
			data     : event.data,
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		// ATUALIZA OBJETOS DO CORE
		await this.updateCore(appid);

		return result;
	}

	async clone(data:any)
    {
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.CLONE_DOCUMENT_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : data.appid,
			colid    : this.collectionName(data.appid),
			document : data,
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		return result;
	}

    async del(data:any)
    {
		const apis = this.getApis();
		await this.core().loadingController.start();

		const result = await this.core().api.callable(apis.delEndPoint, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : data.appid,
			colid    : this.collectionName(data.appid),
			document : data,
			platform : this.core().platform,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();

		return result;
	}

	async delCache(appid:string, colid:string, path:string)
    {
		const result = await this.core().api.callable(Types.DEL_CACHE_DOCUMENT_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : appid,
			colid    : colid,
			keyCache : path,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		return result;
	}

	async updateCore(appid:string)
	{
		// QUANDO CRIAR UM APPS, ATUALIZA
		if(appid == 'apps')
		{
			await this.core().loadApps()
		}

		// QUANDO CRIAR UM MENU, ATUALIZA
		if(appid == 'menu')
		{
			await this.core().loadMenu()
		}

		// QUANDO CRIAR UM OPTION, ATUALIZA
		if(appid == 'option')
		{
			await this.core().loadOptionPermissions()
		}
	}

	async copy(params:Params)
    {
	}

	/*async doToast()
    {
        const toast = await this.toastController.create({
            message         : 'Formulário Enviado!',
            //showCloseButton : true,
            position        : 'top',
            duration        : 1000,
            //closeButtonText : 'X'
        });

        toast.present();
    }*/
	
	async count(params:Params):Promise<Result>
    {   
        params      = this.prepareParams(params);
		const where = await params.where.parseFilter();
		
		const counResult = await this.core().api.callable(Types.COUNT_DOCUMENT_API, 
		{
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid    : params.appid,
			perPage  : params.perPage,
			colid    : this.collectionName(params.appid),
			where    : where,
			mapItems : {
				referencePath : environment.defaultMapItems
			},
			searchVector  : params.searchVector,
		});		

        console.log('count', {path : params.path, where : where}, counResult);

        return counResult;
	}

	async indexes(params:Params):Promise<Result>
    {   
		params = this.prepareParams(params);
		
		const data : any = { 
			maccid   : this.core().masterAccount.code,
			accid    : this.core().account.code,
			appid  	   : params.appid,
			colid  	   : params.colid,
			groupCollection : false,//params.colid != 'documents' && params.colid != 'document' && params.colid != 'forms',
			perPage    : params.perPage,
			orderBy    : params.orderBy,
			count      : params.count,
			total      : params.total,
			page       : 0,
			startAfter : params.data, // AQUI PASSA A PAGINAÇÃO
			where      : params.where,
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		};

		const colid = this.collectionName(params.appid);

		if(colid != 'documents' && colid != 'forms') // COLECTIONS FORMS TAMBEM VERIFICA
		{
			data.colid = colid;
			delete data.path; // REMOVE O PATH
		}
		
		const indexesResult = await this.core().api.callable(Types.INDEXES_DOCUMENT_API, data);

        console.log('indexes', data, indexesResult);

        return indexesResult;
	}

    callable(url:string, data:any):Promise<Result>
    {   
        return this.api().callable(url, data);
	}
	
	getObject(params:Params):Promise<Result>
    {   
		const apis = this.getApis();
        params = this.prepareParams(params);
				params.getEndPoint = apis.getEndPoint;
        return this.api().getObject(params);
    }

    getData(params:Params):Promise<Result>
    {		
				params.collection = this.createCollection(params);
        return this.api().getList(params);
    }

    getReference(params:Params):Promise<Result>
    {
        params = this.prepareParams(params);
        return this.api().getListReference(params);
    }    

    createResult(params:Params)
    {
        return this.core().resultFactory.create(params);
    }

    createParams(args?:any)
    {
        return this.core().paramsFactory.create(args);
    }

    getGrid():any
    {
    }

    getViewer():any
    {
    }

    getApp(name:string)
    {
        return this.core().getApp(name);
    }

    getMenuByApp(name:string)
    {
        return this.core().getMenuByApp(name);
	}

	getMenuByName(name:string)
    {
        return this.core().getMenuByName(name);
	}	

    goRoot(app:any, args?:any, nav?:any)
    {
        this.core().goRoot(app, args, nav);
    }

    goInit()
    {
        this.core().goInit();
    }

    api()
    {
        return this.core().api;
	}
	
	showAlert(title:string, message:string, buttons?:any, inputs?:any)
    {
        const alertController = this.core().injector.get(AlertController);

        alertController.create({
            header  : title,
			message : message,
			inputs  : inputs || [],
            buttons : buttons || ['OK']
        })
        .then((alert:any) =>
        {
            alert.present();
        });
	}
	
	onError(result:Result)
    {
        let message : string = '';

        if(result.form && result.form.messages)
        {
            for(let key in result.form.messages)
            {
                message = this.getMessage(result.form.messages[key]);
            }

            const alertController = this.core().injector.get(AlertController);

            alertController.create({
                header  : 'Alerta',
                message : message,
                buttons : ['OK']
            })
            .then((alert:any) =>
            {
                alert.present();
            });
        }

        console.log('error', message);

        //this.errorEvent.emit(result);
	}

	getMessage(message:any)
    {
        if(typeof message == 'string')
        {
            return message;
        }
        else
        {
            for(let key in message)
            {
                let m = this.getMessage(message[key]);

                if(typeof m == 'string')
                {
                    return m;
                }
            }
        }
    }

    push(router:string, query?:any, routeParams?:any)
    {
        return this.core().push(router, query, routeParams);
    }

    core()
    {
        return Core.get()
    }
}
