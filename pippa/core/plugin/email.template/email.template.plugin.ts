import { Injectable } from '@angular/core';

/* PIPPA */
import { EmailTemplate }           from '../../model/email.template/email.template';
import { EmailTemplateCollection } from '../../model/email.template/email.template.collection';
import { BasePlugin }              from '../base.plugin';

@Injectable({
    providedIn: 'root'
})
export class EmailTemplatePlugin extends BasePlugin
{
    getMaps()
    {
        return {
            model : EmailTemplate,
            list  : EmailTemplateCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'email.template';
	}
}
