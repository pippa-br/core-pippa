import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }     from '../base.plugin';
import { DocumentPlugin } from '../document/document.plugin';

@Injectable({
    providedIn: 'root'
})
export class GooglePlugin extends BasePlugin
{
    getDistance(origins:any, destinations:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            let _destinations : string = '';

			for(const key in destinations)
			{
				if(destinations[key].lat && destinations[key].lng)
				{
					_destinations += destinations[key].lat + ',' + destinations[key].lng + '|';
				}				
			}

			let _origins : string = '';

			for(const key in origins)
			{
				if(origins[key].lat && origins[key].lng)
				{
					_origins += origins[key].lat + ',' + origins[key].lng + '|';
				}				
			}
			
			if(_destinations)
			{
				let baseUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + _origins + '&destinations=' + _destinations + '&key=' + this.core().masterAccount.google.apiKey;
				baseUrl 	= 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);
	
				const params = this.createParams({
					apiUrl : baseUrl,
					method : 'get',
					withCredentials : false,
					//loading : false,
				});
	
				this.core().api.request(params).then((data:any) =>
				{
					resolve(data);
				});
			}
			else
			{
				resolve(null);
			}	
        });
	} 
	
	getDirections(origin:any, destination:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            let _destination : string = destination.lat + ',' + destination.lng;
			let _origin  	 : string = origin.lat + ',' + origin.lng;
			
			if(_destination)
			{
				let baseUrl = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + _origin + '&destination=' + _destination + '&key=' + this.core().masterAccount.google.apiKey;
				baseUrl 	= 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);
	
				const params = this.createParams({
					apiUrl : baseUrl,
					method : 'get',
					withCredentials : false,
					loading : false,
				});
	
				this.core().api.request(params).then((data:any) =>
				{
					resolve(data);
				});
			}
			else
			{
				resolve(null);
			}	
        });
	} 

	/* TABELA DE FRETE */
	getDistancePrice(origin:any, destination:any):Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
			this.getDistance([origin], [destination]).then(async (datas) => 
			{
				if(datas)
				{
					const items = [];

					for(const key in datas.rows)
					{
						for(const key2 in datas.rows[key].elements)
						{
							const data : any = {};

							data.label 	     = datas.rows[key].elements[key2].distance.text;
							data.labelTime   = datas.rows[key].elements[key2].duration.text;
							data.value 	     = datas.rows[key].elements[key2].distance.value;
							data.time  	     = datas.rows[key].elements[key2].duration.value;								
							data.price	     = 0;
							data.origin      = origin;
							data.destination = destination;
							data.radius      = this.core().util.getDistanceFromLatLonInKm(origin.lat, origin.lng, destination.lat, destination.lng) * 1000;

							let params = this.createParams({
								appid   : 'distance',
								orderBy : 'distance',
								asc     : false,
								//loading : false,
							});

							/* GET COLLECTIO DISTANCES */
							const documentPlugin = this.core().injector.get(DocumentPlugin)
							const result 	     = await documentPlugin.getData(params);
							let   selected;

							for(const key3 in result.collection)
							{
								if(data.value >= result.collection[key3].distance * 1000)
								{
									selected = result.collection[key3];
									break;
								};
							}

							if(selected)
							{
								data.price 		= selected.value;
								data.labelPrice = 'R$ ' + selected.value.toLocaleString('pt-BR', { minimumFractionDigits : 2})								
								data.label 	    = data.labelPrice + ' (' + data.label + ' em ' + data.labelTime + ')';
							}
														
							data.valid = this.core().masterAccount.address.radius * 1000 >= data.radius;
							items.push(data);
						}
					}

					if(items.length > 0)
					{
						const item : any = items[0];
						resolve(item);
					}
					else
					{
						resolve(null);
					}
				}
				else
				{
					resolve(null);
				}
			});
        });
	} 
	
	getLocaleAddress(address:string)
    {
        return new Promise<any>((resolve) =>
        {
			let baseUrl = 'https://maps.google.com/maps/api/geocode/json?address=' + address + '&key=' + this.core().masterAccount.google.apiKey;
			//baseUrl 	= 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

			const params = this.createParams({
				apiUrl : baseUrl,
				method : 'get',
				withCredentials : false,
				loading : false,
			});

			this.core().api.request(params).then((data:any) =>
			{
				resolve(data);
			});

            /*this.zone.runOutsideAngular(() =>
            {
                params.apiUrl = 'https://maps.google.com/maps/api/geocode/json?address=' + params.post.address + '&key=' + this.core().masterAccount.google.apiKey;

                this.http.request(params.apiUrl)
                .map((res:any) => res.json())
                .catch((e:any) =>
                {
					resolve();
                    return this.handleError(e);
                })
                .subscribe((response:any) =>
                {
                    let result = this.createResult(params);
                    result.data = response;
                    resolve(result);
                });
            });*/
        });
    }
}
