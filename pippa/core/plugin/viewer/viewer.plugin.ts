import { Injectable } from '@angular/core';

/* PIPPA */
import { Viewer }           from '../../model/viewer/viewer';
import { ViewerCollection } from '../../model/viewer/viewer.collection';
import { BasePlugin }       from '../../../core/plugin/base.plugin';
import { Params }           from '../../../core/util/params/params';
import { Result }           from '../../../core/util/result/result';

@Injectable({
    providedIn: 'root'
})
export class ViewerPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Viewer,
            list  : ViewerCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'viewer';
	}

	getOne(params:Params)
    {
		return new Promise((resolve) => 
		{
			this.getData(params).then(result => 
			{
				for(const key in result.collection)
				{
					result.data = result.collection[key];
						break;
				}

				resolve(result.data);
			});
		});
	}  
}
