import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin }           from '../base.plugin';
import { Document }             from '../../model/document/document';
import { DocumentCollection }   from '../../model/document/document.collection';

@Injectable({
    providedIn: 'root'
})
export class DocumentPlugin extends BasePlugin
{
	public onlyMaster : boolean = false;

    getMaps():any
    {
        return {
            model : Document,
            list  : DocumentCollection,
        }
    }
}
