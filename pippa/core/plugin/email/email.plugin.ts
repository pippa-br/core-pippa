import { Injectable } from '@angular/core';

/* PIPPA */
import { BasePlugin } from '../../plugin/base.plugin';
import { Types } 	  from '../../type/types';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class EmailPlugin extends BasePlugin
{
    async sendTransection(data:any, template:any, noLimite:boolean, noValidate:boolean)
    {
        await this.core().loadingController.start(true);

		const result = await this.core().api.callable(Types.SEND_TRANSECTION_API, 
		{
			maccid     : this.core().masterAccount.code,
			accid      : this.core().account.code,
			appid      : data.appid,
			colid      : this.collectionName(data.appid),
			document   : data,
			template   : template,
			noLimite   : (noLimite == undefined ? false : noLimite),
			noValidate : (noValidate == undefined ? false : noValidate),
			mapItems : {
				referencePath : environment.defaultMapItems
			}
		}, this.getMaps(), true);

		this.core().loadingController.stop();
	}	
}
