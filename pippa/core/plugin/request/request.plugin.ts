import { Injectable } from '@angular/core';

/* PIPPA */
import { RequestCollection } from '../../model/request/request.collection';
import { Request }           from '../../model/request/request';
import { DocumentPlugin }  	 from '../../../core/plugin/document/document.plugin';
import { FormPlugin }  	     from '../../../core/plugin/form/form.plugin';
import { Params }            from '../../../core/util/params/params';
import { Result }            from '../../../core/util/result/result';
import { LogType }           from '../../type/log/log.type';
import { TransactionType }   from '../../type/transaction/transaction.type';

@Injectable({
    providedIn: 'root'
})
export class RequestPlugin extends DocumentPlugin
{
    getMaps():any
    {
        return {
            model : Request,
            list  : RequestCollection,
        }
	}
}
