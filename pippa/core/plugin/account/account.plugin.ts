import { Injectable } from '@angular/core';

/* PIPPA */
import { Account }           from '../../model/account/account';
import { AccountCollection } from '../../model/account/account.collection';
import { BasePlugin }        from '../base.plugin';
import { Params }            from '../../../core/util/params/params';
import { Result }            from '../../../core/util/result/result';
import { FieldType }         from '../../../core/type/field/field.type';
import { environment }       from '../../../../../environments/environment';
import { Types } from '../../type/types';

@Injectable({
  	providedIn : 'root'
})
export class AccountPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : Account,
            list  : AccountCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'documents';
	}

	appidName()
	{
		return 'accounts';
	}

    prepareParams(params:Params)
    {
        params = super.prepareParams(params);

		params.appid = 'accounts';
	
		if(params.orderBy == undefined)
		{
			params.orderBy = 'name';
		} 

		if(params.asc == undefined)
		{
			params.asc = true;
		}

        return params;
    }

    async get(params:Params)
    {
		params = this.prepareParams(params);
 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.GET_ACCOUNT_API, 
		{
			platform : this.core().platform,
			maccid   : params.maccid,
			accid    : params.accid,
            appid    : 'accounts',
            colid    : 'documents',
			map      : true,
            mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;        
    }
	
	async getTimezone(params:Params)
    { 
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.GET_TIMEZONE_API, 
		{
			platform : this.core().platform,
			maccid   : params.maccid,
			accid    : params.accid,
            appid    : 'accounts',
            colid    : 'documents',
			map      : true,
            mapItems : {
				referencePath : environment.defaultMapItems
			},
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;
    }

	async setTimezone(params:Params)
    {
		await this.core().loadingController.start();

		const result = await this.core().api.callable(Types.SET_TIMEZONE_API, 
		{
			platform : this.core().platform,
			maccid   : params.maccid,
			accid    : params.accid,
            appid    : 'accounts',
            colid    : 'documents',
			map      : true,
            mapItems : {
				referencePath : environment.defaultMapItems
			},
			timezone : params.data.timezone,
		}, this.getMaps(), true);

		this.core().loadingController.stop();
		
		return result;
    }
}
