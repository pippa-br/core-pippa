import { Injectable } from '@angular/core';

/* PIPPA */
import { App }           from '../../model/app/app';
import { AppCollection } from '../../model/app/app.collection';
import { BasePlugin }    from '../base.plugin';
import { Params }        from '../../util/params/params';
import { Types }         from '../../type/types';
import { environment }   from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppPlugin extends BasePlugin
{
    getMaps():any
    {
        return {
            model : App,
            list  : AppCollection,
        }
	}

    /*getApis():any
    {
        return {
			listEndPoint : Types.COLLECTION_APP_API,
			getEndPoint  : Types.GET_APP_API,
			addEndPoint  : Types.ADD_APP_API,
			setEndPoint  : Types.SET_APP_API,
			delEndPoint  : Types.DEL_APP_API,
        }
	}*/

	collectionName(appid:string)
	{
		return 'documents';
	}
	
	appidName()
	{
		return 'apps';
	}

    prepareParams(params:Params)
    {
        params          = super.prepareParams(params);
        params.orderBy  = 'name';
        params.asc      = true;
        //params.map      = true;
        
        return params;
    }
}
