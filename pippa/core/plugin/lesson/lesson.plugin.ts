import { Injectable } from '@angular/core';

/* PIPPA */
import { Lesson }           from '../../model/lesson/lesson';
import { LessonCollection } from '../../model/lesson/lesson.collection';
import { DocumentPlugin }   from '../../plugin/document/document.plugin';
import { Params }           from '../../util/params/params';
import { Result }           from '../../util/result/result';
import { SumFormTransform } from '../../component/transform/sum.form/sum.form.transform';
import { Types } 			from '../../type/types';

@Injectable({
    providedIn : 'root'
})
export class LessonPlugin extends DocumentPlugin
{
    getMaps()
    {
        return {
            model : Lesson,
            list  : LessonCollection,
        }
    }

    async getLessons(params:Params)
    {
		await this.core().loadingController.start();

		params = this.prepareParams(params);
		
		const data : any = { 
			maccid  : params.maccid,
			accid   : params.accid,
			appid   : params.appid,
			colid   : params.colid,
			course  : params.data.course,
			student : params.data.student
		};
 
		const result = await this.core().api.callable(Types.LESSONS_EAD_API, data, this.getMaps(), true, true);

		this.core().loadingController.stop();

        return result;

        /*return new Promise<Result>((resolve) =>
        {
            this.api().getList(this.prepareParams(params)).then((result:any) =>
            {
                const promises :any = [];
				
				// DEPOIS REVER
				/* SOMA DE RESULTADOS */
                /*for(const key in result.collection)
                {
                    const promise = SumFormTransform.transform(null, result.collection[key]);
                    promises.push(promise);
                }

                Promise.all(promises).then((values:any) =>
                {					
                    for(const key in result.collection)
                    {
                        result.collection[key].total      = values[key].total;
                        result.collection[key].count      = values[key].count;
						result.collection[key].percentage = values[key].total > 0 ? values[key].percentage : -1;
					}

                    resolve(result);
                });
            });
        });*/
    }
}
