import { Injectable } from '@angular/core';

/* PIPPA */
import { Field }           from '../../model/field/field';
import { FieldCollection } from '../../model/field/field.collection';
import { BasePlugin }      from '../../../core/plugin/base.plugin';
import { Params }          from '../../../core/util/params/params';
import { Result }          from '../../../core/util/result/result';

@Injectable({
    providedIn: 'root'
})
export class FieldPlugin extends BasePlugin
{
    getMaps()
    {
        return {
            model : Field,
            list  : FieldCollection,
        }
	}
	
	collectionName(appid:string)
	{
		return 'fields';
	}    
}
