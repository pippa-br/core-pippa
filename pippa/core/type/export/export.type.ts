export const ExportType =
{
	// COLUMNS
	NONE_COLUMNS : { id : 'noneColumns',  label : 'Sem',    value : 'noneColumns' }, // CHAMADAS DE API's EXTERNAS
	FORM_COLUMNS : { id : 'formColumns',  label : 'Form',   value : 'formColumns' },
    GRID_COLUMNS : { id : 'gridColumns',  label : 'Tabela', value : 'gridColumns' },

	// TYPE
    CSV_TYPE     : { id : 'csv',  label : 'CSV',   value : 'csv' },
    XLS_TYPE     : { id : 'xlsx', label : 'Excel', value : 'xlsx' },
    PDF_TYPE     : { id : 'pdf',  label : 'PDF',   value : 'pdf' },

	// DOCCUMENTS
    ALL_DOCUMENTS 	 : { id : 'all',    label : 'Todos', value : 'all' },
    PERIOD_DOCUMENTS : { id : 'period', label : 'Por Período', value : 'period' },
}
