/* PIPPA */
import { Core } from "../../util/core/core";

//@dynamic
export class FieldType extends Object
{
    static instance   : any;
    static isCreating  = false;
    static items      : any     = {};

    constructor()
    {
        super();

        if (!FieldType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if (FieldType.instance == null)
        {
            FieldType.isCreating = true;
            FieldType.instance   = new FieldType();
            FieldType.isCreating = false;
        }

        return FieldType.instance;
    }

    register(data:any)
    {
        FieldType.items[data.value.toUpperCase()] = data;
    }
	
    update(name:string, data:any)
    {
        FieldType.items[name.toUpperCase()] = Object.assign(FieldType.items[name.toUpperCase()], data);
    }

    static type(key:string)
    {
        try
        {
            return {
                label : FieldType.items[key.toUpperCase()].label,
                value : FieldType.items[key.toUpperCase()].value,	
            }
        }
        catch (e)
        {
            console.error("type not found", key);
        }
    }

    static input(key:string)
    {
        return FieldType.items[key.toUpperCase()].input;
    }

    static output(key:string)
    {
        const item = FieldType.items[key.toUpperCase()];

        if (item)
        {
            return item.output;
        }
        else
        {
            console.log("OUTPUT NOT FOUND", key.toUpperCase());
        }
    }

    static transform(key:string)
    {
        const item = FieldType.items[key.toUpperCase()];

        if (item)
        {
            return FieldType.items[key.toUpperCase()].transform;
        }
        else
        {
            console.log("TRANFORM NOT FOUND", key.toUpperCase());
        }        
    }

    static optionType(key:string)
    {
        return FieldType.items[key.toUpperCase()].optionType;
    }

    static optionsType(key:string)
    {
        return FieldType.items[key.toUpperCase()].optionsType;
    }

    static setting(key:string)
    {
        return FieldType.items[key.toUpperCase()].setting;
    }

    static hasGrid(key:string)
    {
        return FieldType.items[key.toUpperCase()].hasGrid;
    }

    static hasFilter(key:string)
    {
        return FieldType.items[key.toUpperCase()].hasFilter;
    }		

    static hasSingleFilter(key:string)
    {
        return FieldType.items[key.toUpperCase()].hasSingleFilter;
    }

    static getItems()
    {
        const items = [];

        for (const key in FieldType.items)
        {
            items.push(FieldType.type(key));
        }

        return items;
    }

    static getFieldsDefault()
    {
        const fields = [];

        /* COLUMN DEFAULT */
        fields.push({
            id    : "XLDVbXQGcR",
            name  : null,
            label : "Texto",
            type  : FieldType.type("Text"),
        });
		
        fields.push({
            id    : "87gsx9Kbo8",
            name  : null,
            label : "Select",
            type  : FieldType.type("Select"),
        });
		
        fields.push({
            id    : "kPOVxVerac",
            name  : null,
            label : "MultiSelect",
            type  : FieldType.type("MultiSelect"),
        });

        fields.push({
            id    : "PXT5Dcmp",
            name  : null,
            label : "ListMultiSelectInput",
            type  : FieldType.type("ListMultiSelectInput"),
        });		

        fields.push({
            id    : "EptUGVha",
            name  : null,
            label : "ListMultiRadioInput",
            type  : FieldType.type("ListMultiRadioInput"),
        });		

        fields.push({
            id    : "gqbjZXHvsj",
            name  : null,
            label : "ReferenceSelect",
            type  : FieldType.type("ReferenceSelect"),
        });
		
        fields.push({
            id    : "nRS7R2iZdo",
            name  : null,
            label : "ReferenceMultiSelect",
            type  : FieldType.type("ReferenceMultiSelect"),
        });

        fields.push({
            id    : "5RcjZ4DRtR",
            name  : "_sequence",
            label : "Sequence",
            type  : FieldType.type("Number"),
        });

        fields.push({
            id    : "7UKNTWISd9",
            name  : null,
            label : "Inteiro",
            type  : FieldType.type("Integer"),
        });

        fields.push({
            id    : "2N20HzsUds",
            name  : "indexes.orderSequence",
            label : "Order Sequence",
            type  : FieldType.type("Number"),
        });

        fields.push({
            id    : "bMqJLMq7PT",
            name  : "form.name",
            label : "Nome do Formulário",
            type  : FieldType.type("Text"),
        });

        fields.push({
            id    : "ZPhAEShvdi",
            name  : "form",
            label : "Somar Documento",
            type  : FieldType.type("SumForm"),
        });

        fields.push({
            id    : "rGYgHcGqqq",
            name  : "form.icon",
            label : "Icon do Formulário",
            type  : FieldType.type("SelectIcon"),
        });

        fields.push({
            id    : "m8II4wknAf",
            name  : "subForm",
            label : "SubForm",
            type  : FieldType.type("SubForm"),
        });

        fields.push({
            id    : "jfrNHHtM6b",
            name  : "owner",
            label : "Usuário Criador",
            type  : FieldType.type("ReferenceSelect"),
        });

        fields.push({
            id    : "aPrOTvVipA",
            name  : "user",
            label : "Usuário Editor",
            type  : FieldType.type("ReferenceSelect"),
        });

        fields.push({
            id    : "LCIVMvyfkN",
            name  : "order",
            label : "Ordem",
            type  : FieldType.type("Text"),
        });

        fields.push({
            id    : "irna3FTAIx",
            name  : "count",
            label : "CountDocuments",
            type  : FieldType.type("CountDocuments"),
        });

        fields.push({
            id    : "T8VZYU8zOd",
            name  : "_lock",
            label : "Lock",
            type  : FieldType.type("Lock"),
        });

        fields.push({
            id    : "PPaXjpXgLT",
            name  : "image",
            label : "Imagem",
            type  : FieldType.type("Image"),
        });

        fields.push({
            id    : "MImCVrNiVU",
            name  : "photo",
            label : "Avatar",
            type  : FieldType.type("Avatar"),
        });

        fields.push({
            id    : "KXflAgEiUp",
            name  : "name",
            label : "Título",
            type  : FieldType.type("Title"),
        });

        fields.push({
            id    : "LBRpCGGrCR",
            name  : "postdate",
            label : "Data de Criação",
            type  : FieldType.type("Date"),
        });

        fields.push({
            id      : "Y7eMrlakRi",
            name    : "lastdate",
            label   : "Data de Alteração",
            type    : FieldType.type("Date"),
            setting : {
                hasTime : true,
            }
        });

        fields.push({
            id      : "iYQgWDqdng",
            name    : "postdate",
            label   : "Data / Hora de Criação",
            type    : FieldType.type("Date"),
            setting : {
                hasTime : true,
            }
        });        

        fields.push({
            id    : "0hU2ZMfJ",
            name  : null,
            label : "Horário",
            type  : FieldType.type("Time"),            
        });

        fields.push({
            id    : "aLTsiecIgt",
            name  : null,
            label : "Função",
            type  : FieldType.type("Function"),
        });

        fields.push({
            id    : "lgbddljxSo",
            name  : null,
            label : "Botão",
            type  : FieldType.type("Button"),
        });
		
        fields.push({
            id    : "0Yy2xn4QX7",
            name  : null,
            label : "ShareButtons",
            type  : FieldType.type("ShareButtons"),
        });

        fields.push({
            id    : "uZQflTmpkL",
            name  : null,
            label : "Último Documento",
            type  : FieldType.type("LastDocument"),
        });

        fields.push({
            id    : "lsTwANAYlY",
            name  : null,
            label : "Centavos",
            type  : FieldType.type("Centavos"),
        });

        fields.push({
            id    : "8zpndWLBGZ",
            name  : null,
            label : "Moeda",
            type  : FieldType.type("Currency"),
        });

        fields.push({
            id    : "kUYmquhWeO",
            name  : "this",
            label : "Verificar Pagamento",
            type  : FieldType.type("VerifyPayment"),
        })
        
        fields.push({
            id    : "T2XlU5LG0z",
            name  : "payment",
            label : "Cobranças",
            type  : FieldType.type("ChargesPagarme"),
        });

        fields.push({
            id    : "Yeq0Ulr4J3",
            name  : "tracking",
            label : "Tracking Correios",
            type  : FieldType.type("TrackingCorreios"),
        });

        fields.push({
            id    : "FoNPCV05z8",
            name  : "reversalCode",
            label : "Tracking Reversal",
            type  : FieldType.type("TrackingReversal"),
        });

        fields.push({
            id    : "lMHnfGxTdw",
            name  : null,
            label : "Url",
            type  : FieldType.type("URL"),
        });

        fields.push({
            id    : "cGL6lw7E9i",
            name  : null,
            label : "Vídeo Url",
            type  : FieldType.type("VideoUrl"),
        });

        fields.push({
            id    : "ACOhSI34SM",
            name  : null,
            label : "Download",
            type  : FieldType.type("Download"),
        });

        fields.push({
            id    : "nXpcuvtRcX",
            name  : null,
            label : "Resultado Fieldset",
            type  : FieldType.type("SumFieldset"),
        });

        fields.push({
            id    : "6j5LGumitt",
            name  : "_archive",
            label : "Arquivados",
            type  : FieldType.type("Toggle"),
        });

        fields.push({
            id    : "APFrNncahi",
            name  : "_canceled",
            label : "Cancelado",
            type  : FieldType.type("Toggle"),
        });
		
        fields.push({
            id    : "6j5LGumit8",
            name  : "", 
            label : "Código de Barra",
            type  : FieldType.type("Barcode"),
        });
		
        fields.push({
            id    : "FnhRgYAJio",
            name  : "price",  
            label : "Preço",
            type  : FieldType.type("Currency"),
        });

        fields.push({
            id    : "0kXkKfU5Sk",
            name  : "percentage",  
            label : "Percentage",
            type  : FieldType.type("Percentage"),
        });
		
        fields.push({
            id    : "BDJHaISP3j",
            name  : "value", 
            label : "Número",
            type  : FieldType.type("Number"),
        });
		
        fields.push({
            id    : "NtTEZMJMwE",
            name  : "comments", 
            label : "Comentários",
            type  : FieldType.type("Comment"),
        });
		
        fields.push({
            id    : "B3sdBXmYdf",
            name  : "status", 
            label : "Toggle",
            type  : FieldType.type("Toggle"),
        });
		
        fields.push({
            id    : "k3XVv20j1P",
            name  : "ID",
            label : "StepItem",
            type  : FieldType.type("Step"),
        });

        fields.push({
            id    : "67KE4lnDmg",
            name  : "ID",
            label : "Matrix",
            type  : FieldType.type("Matrix"),
        });

        fields.push({
            id    : "50hARKyuyI",
            name  : "viewer",
            label : "Viewer",
            type  : FieldType.type("Viewer"),
        });

        fields.push({
            id    : "Q13puEwPOS",
            name  : "value",
            label : "Textarea",
            type  : FieldType.type("Textarea"),
        });

        fields.push({
            id    : "WUAqyWT27I",
            name  : "creditCard",
            label : "Cartão de Credito",
            type  : FieldType.type("CreditCard"),
        });

        fields.push({
            id    : "XdvmFkxqKU",
            name  : "qrcode",
            label : "QRCode",
            type  : FieldType.type("QRCode"),
        });


        fields.push({
            id    : "INVtnQXJ0V",
            name  : "address",
            label : "Endereço",
            type  : FieldType.type("Address"),
        });

        fields.push({
            id    : "rAB72YPzxp",
            name  : "items",
            label : "Stock de Produtos",
            type  : FieldType.type("StockProducts"),
        });

        fields.push({
            id    : "SOIdzxrZUC",
            name  : "items",
            label : "Preço de Produtos",
            type  : FieldType.type("PriceProducts"),
        });

        fields.push({
            id    : "8sZ9bgAY3X",
            name  : "stock",
            label : "Movimentação de Estoque",
            type  : FieldType.type("StockMoviment"),
        });

        fields.push({
            id    : "RgXMDzSoZR",
            name  : "payment",
            label : "Status do Pagamento",
            type  : FieldType.type("PaymentStatus"),
        });

        fields.push({
            id    : "hPvAqvO0iI0i",
            name  : "payment",
            label : "Reference Form",
            type  : FieldType.type("ReferenceForm"),
        });

        return fields;
    }
	
    static getFilterItems()
    {
        return [
            {
                row   : 0,
                col   : 1,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "label",
                    label       : "Label",
                    placeholder : "Label",
                }
            },			
            {
                row   : 0,
                col   : 1,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "name",
                    label       : "ID",
                    placeholder : "ID",
                }
            },
            {
                row   : 0,
                col   : 2,
                field : {
                    type        : FieldType.type("Integer"),
                    name        : "order",
                    label       : "Ordem",
                    placeholder : "Ordem",
                }
            },
            {
                row   : 0,
                col   : 3,
                field : {
                    type        : FieldType.type("MultiSelect"),
                    name        : "_groups",
                    label       : "Grupo de Permissão",
                    placeholder : "Grupo de Permissão",
                    option      : {
                        record : false,
                        items  : Core.get().optionPermission.items
                    },
                }
            },
            {
                row   : 1,
                col   : 1,
                field : {
                    type        : FieldType.type("Select"),
                    name        : "type",
                    label       : "Tipo",
                    placeholder : "Tipo",
                    option      : {
                        items : [
                            FieldType.type("Text"),
                            FieldType.type("Select"),
                            FieldType.type("Date"),
                            FieldType.type("MultiSelect"),
                            FieldType.type("ReferenceSelect"),
                            FieldType.type("ReferenceMultiSelect"),
                            FieldType.type("Integer"),
                            FieldType.type("Number"),
                            FieldType.type("Toggle"),
                        ]
                    },
                }
            },                        
            {
                row   : 1,
                col   : 2,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "field",
                    label       : "Field",
                    placeholder : "Field",
                }
            },
            {
                row   : 1,
                col   : 3,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "operator",
                    label       : "Operador",
                    placeholder : "Operador",
                    initial     : "=="
                }
            },
            {
                row   : 1,
                col   : 4,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "value",
                    label       : "Valor",
                    placeholder : "Valor",
                    required    : false,
                    setting     : {
                        convert : true
                    }
                }
            },
            {
                row   : 1,
                col   : 5,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "context",
                    label       : "Contexto",
                    placeholder : "Contexto",
                    required    : false,
                }
            },							
            {
                row   : 1,
                col   : 6,
                field : {
                    type        : FieldType.type("Text"),
                    name        : "path",
                    label       : "Path",
                    placeholder : "Path",
                    required    : false,
                }
            },
        ]
    }
}
