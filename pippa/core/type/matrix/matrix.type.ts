//@dynamic
export class MatrixType extends Object
{
	static COMBINE  = { value : 'combine',  label : 'Combinação', id : 0 };	
	static SEPARATE = { value : 'separate', label : 'Separação',  id : 1 };
	static FIRST    = { value : 'fisrt',    label : 'Primeiro',   id : 2 };
	static LAST     = { value : 'last',     label : 'Último',     id : 3 };
}