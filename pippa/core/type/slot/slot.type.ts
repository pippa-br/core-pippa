export const SlotType =
{
    TIILE          : { id : 'title',         label : 'Título',         value : 'title' },
    SUBTITLE       : { id : 'subtitle',      label : 'Subtítulo',      value : 'subtitle' },
    BEFORE_CONTENT : { id : 'beforeContent', label : 'Before Content', value : 'beforeContent' },
    AFTER_CONTENT  : { id : 'afterContent',  label : 'After Content',  value : 'afterContent' },
    CONTENT        : { id : 'content',       label : 'Content',        value : 'content' },
    FOOTER         : { id : 'footer',        label : 'Footer',         value : 'set' },
}
