//@dynamic
export class TransactionType extends Object
{
	static EMAIL_MODE     = { value : 'email',    label : 'E-mail'};
	static PUSH_MODE      = { value : 'push',     label : 'Push'};
	static TWILIO_MODE    = { value : 'twilio',   label : 'Twilio' };
	static PLUG_ZAPI_MODE = { value : 'plugZapi', label : 'Whatsapp' };
	static APP_MODE       = { value : 'app',      label : 'App' };

  static NONE_TYPE              	   = { value : 'none',             label : 'Nenhum', 				      hasLimite : true};
	static ADD_TYPE              	   = { value : 'add',              label : 'Novo', 				     	  hasLimite : true};
    static SET_TYPE              	   = { value : 'set',              label : 'Editar', 			  	      hasLimite : true};
    static ADD_FORM_TYPE         	   = { value : 'addForm',          label : 'Novo Form', 			 	  hasLimite : true};
    static SET_FORM_TYPE         	   = { value : 'setForm',          label : 'Form Editar', 		     	  hasLimite : true};
	static LOGIN_TYPE            	   = { value : 'Login',            label : 'Login', 		  	 	  	  hasLimite : false};
    static RECOVERY_PASSWORD_TYPE	   = { value : 'recoveryPassword', label : 'Recuperar Senha', 	 	      hasLimite : false};
    static NEW_PAYMENT_TYPE      	   = { value : 'newPayment',       label : 'Novo Pagamento', 	 		  hasLimite : true};
	static WAITING_PAYMENT_TYPE  	   = { value : 'waitingPayment',   label : 'Aguardando Pagamento', 	      hasLimite : true};
	static PAID_PAYMENT_TYPE     	   = { value : 'paidPayment',      label : 'Pagamento Confirmado', 	      hasLimite : true};
	static BILLET_PAYMENT_TYPE   	   = { value : 'billetPayment',    label : 'Pagamento em Boleto', 	      hasLimite : true};
	static PIX_PAYMENT_TYPE      	   = { value : 'pixPayment',       label : 'Pagamento em PIX',  		  hasLimite : true};
	static REFUSED_PAYMENT_TYPE  	   = { value : 'refusedPayment',   label : 'Pagamento Recusado',     	  hasLimite : true};	
	static SCHEDULE_TYPE   		  	   = { value : 'schedule',   	   label : 'Agendamento',   			  hasLimite : true};
	static GET_TOKEN 	  		  	   = { value : 'getToken',   	   label : 'Gerar Token',   		 	  hasLimite : false};
	static NF_AUTORIZADA_TRANSACTION   = { value : 'nfAutorizada',     label : 'NF Autorizada',   	  	      hasLimite : true};
	static PLP_TRANSACTION   	 	   = { value : 'plp',   	  	   label : 'PLP Gerada',   	 			  hasLimite : true};	
	static REVERSAL_CODE_TRANSACTION   = { value : 'reversalCode',     label : 'Código Reverso',   	 		  hasLimite : true};	
	static APPROVED_ORDER_TRANSACTION  = { value : 'approvedOrder',    label : 'Pedido Aprovado',      	 	  hasLimite : true};
    static CANCELED_ORDER_TRANSACTION  = { value : 'canceledOrder',    label : 'Pedido Cancelado',     	      hasLimite : true};
	static SPLIT_ORDER_TRANSACTION     = { value : 'splitOrder',       label : 'Pedido Segmentado',      	  hasLimite : true};
	static AVAIABLE_ORDER_TRANSACTION  = { value : 'availableOrder',   label : 'Pedido Pronto para Retirada', hasLimite : true};
	static PAID_ORDER_TRANSACTION      = { value : 'paidOrder',    	   label : 'Pedido Pago', 			  	  hasLimite : true};
	static REMINDER_BOLETO_TRANSACTION  = { value : 'reminderBoleto',   label : 'Lembrete do Boleto',          hasLimite : true};	
	static REMINDER_CART_TRANSACTION    = { value : 'reminderCart',     label : 'Lembrete de Carrinho',        hasLimite : true};
	static MOVEMENT_STATUS_TRANSACTION  = { value : 'movementStatus',     label : 'Movimento de Entrega',        hasLimite : false};
	static DELIVERED_STATUS_TRANSACTION = { value : 'deliveredStatus',     label : 'Status de Entrega',        hasLimite : true};
	static SCHEDULE_ACTION   		   = { value : 'schedule',     	   label : 'Agendamento',        		  hasLimite : true};

	static SEND_GRID_PROVIDER  = { value : 'sendgrid', label : 'Send Grid' };
	static APP_PROVIDER        = { value : 'app',      label : 'App' };
	static PLUG_ZAPI__PROVIDER = { value : 'plugZapi', label : 'PlugZapi' }

	static getModes()
	{
		return [
			TransactionType.EMAIL_MODE,
			TransactionType.PUSH_MODE,
			TransactionType.PLUG_ZAPI_MODE,
			TransactionType.TWILIO_MODE,	
			TransactionType.APP_MODE,
		];
	}

	static getProvides()
	{
		return [
			TransactionType.SEND_GRID_PROVIDER,
			TransactionType.APP_PROVIDER,
			TransactionType.PLUG_ZAPI__PROVIDER,
		];
	}

	static getTypes()
	{
		return [
			TransactionType.NONE_TYPE,
			TransactionType.ADD_TYPE,
			TransactionType.SET_TYPE,
			TransactionType.RECOVERY_PASSWORD_TYPE,
			TransactionType.LOGIN_TYPE,
			TransactionType.NEW_PAYMENT_TYPE,
			TransactionType.WAITING_PAYMENT_TYPE,
			TransactionType.BILLET_PAYMENT_TYPE,
			TransactionType.PIX_PAYMENT_TYPE,
			TransactionType.PAID_PAYMENT_TYPE,
			TransactionType.REFUSED_PAYMENT_TYPE,
			TransactionType.GET_TOKEN,
			TransactionType.SCHEDULE_ACTION,
			TransactionType.NF_AUTORIZADA_TRANSACTION,
			TransactionType.PLP_TRANSACTION,
			TransactionType.REVERSAL_CODE_TRANSACTION,
			TransactionType.APPROVED_ORDER_TRANSACTION,
			TransactionType.PAID_ORDER_TRANSACTION,
			TransactionType.CANCELED_ORDER_TRANSACTION,
			TransactionType.AVAIABLE_ORDER_TRANSACTION,
			TransactionType.SPLIT_ORDER_TRANSACTION,			
			TransactionType.REMINDER_BOLETO_TRANSACTION,
			TransactionType.REMINDER_CART_TRANSACTION,		
			TransactionType.MOVEMENT_STATUS_TRANSACTION,			
			TransactionType.DELIVERED_STATUS_TRANSACTION,			
		];
	}
}
