//@dynamic
export class Types extends Object
{
    // DOCUMENT API
    static COLLECTION_DOCUMENT_API	= "documentV2/collection";
    static GET_DOCUMENT_API 	 	= "documentV2/get";
    static ADD_DOCUMENT_API 	 	= "documentV2/add";
    static ADDS_DOCUMENT_API 	 	= "documentV2/adds";
    static SET_DOCUMENT_API 	 	= "documentV2/set";
    static DEL_DOCUMENT_API 	 	= "documentV2/del";
    static CLONE_DOCUMENT_API 	 	= "documentV2/clone";
    static DEL_CACHE_DOCUMENT_API 	= "documentV2/delCache";
    static COUNT_DOCUMENT_API 	 	= "documentV2/count";
    static INDEXES_DOCUMENT_API  	= "documentV2/indexes";
    static EXPORT_DOCUMENT_API   	= "documentV2/export";
    static IMPORT_DOCUMENT_API   	= "documentV2/import";
    static CLEAR_CACHE_DOCUMENT_API = "documentV2/clearCache";

    // AUTH API
    //static COLLECTION_AUTH_API	           = 'authV2/collection';
    //static GET_AUTH_API	           		   = 'authV2/get';
    //static ADD_AUTH_API	           		   = 'authV2/add';
    //static SET_AUTH_API	           		   = 'authV2/set';
    //static DEL_AUTH_API	           		   = 'authV2/del';
    static LOGIN_AUTH_API     	 	       = "authV2/login";
    static LOGOUT_AUTH_API     	  	       = "authV2/logout";
    static RECOVERY_PASSWORD_AUTH_API      = "authV2/recoveryPassword";
    static GET_LOGGED_AUTH_API     	       = "authV2/getLogged";
    static GET_TOKEN_AUTH_API     	       = "authV2/getToken";
    static SET_USER_API     	       	   = "authV2/setUser";

    /* AGGREGATE */
    static COUNT_AGGREGATE = { value : "Count" }
    static SUM_AGGREGATE   = { value : "Sum" }

    // CORREIOS
    static TRACK_CORREIOS_API   = "correiosApi/track";
    static REVERAL_CORREIOS_API = "correiosApi/reversal-status";

    // APP API
    /*static COLLECTION_APP_API = 'appApi/collection';
	static GET_APP_API	      = 'appApi/get';
	static ADD_APP_API	      = 'appApi/add';
	static SET_APP_API	      = 'appApi/set';
	static DEL_APP_API	      = 'appApi/del';*/

    // MENU API
    /*static COLLECTION_MENU_API = 'menuApi/collection'
	static GET_MENU_API	       = 'menuApi/get'
	static ADD_MENU_API	       = 'menuApi/add';
	static SET_MENU_API	       = 'menuApi/set';
	static DEL_MENU_API	       = 'menuApi/del';*/
	
    static UPLOAD_DOCUMENT_API   	       = "storageV2/upload";	
    static UPLOAD_VIDEO_API   	           = "storageV2/uploadVideo";
    static UPLOAD2_DOCUMENT_API   	       = "storageV2/upload2";	
    static UPLOAD_TEMP_DOCUMENT_API   	   = "storageV2/uploadTemp";
    static UPLOAD_MAKE_PUBLIC_DOCUMENT_API = "storageV2/uploadMakePublic";	
    static BASE64_UPLOAD_DOCUMENT_API  	   = "storageV2/base64Upload";	
    static GET_ACCOUNT_API     	       	   = "accountV2/get";
    static ADD_ACL_API     	       		   = "utilApi/addAcl";
    static PROXY_API     	       		   = "utilApi/proxy";
    static GET_TIMEZONE_API     	       = "utilApi/getTimezone";
    static SET_TIMEZONE_API     	       = "utilApi/setTimezone";
    //static GET_BY_CODE_OPTION_API     	   = 'optionApi/getByCode';	
    static CERTIFICATE_IMAGE_AUTH_API      = "imageApi/certificate";
    static SEND_TRANSECTION_API 		   = "emailApi/sendTransection";
    static ADD_PAYMENT_PAGARME_API 		   = "pagarmeApi/addPayment";
    static LESSONS_EAD_API 	 	   	       = "eadApi/lessons";

    // CALL
    static BEFORE_ADD_CALL = { value : "beforeAdd", label : "Antes de Adicionar" };
    static AFTER_ADD_CALL  = { value : "afterAdd",  label : "Depois de Adicionar" };	
    static BEFORE_SET_CALL = { value : "beforeSet", label : "Antes de Editar" };
    static AFTER_SET_CALL  = { value : "afterSet",  label : "Depois de Editar" };
    static BEFORE_DEL_CALL = { value : "beforeDel", label : "Antes de Remover" };	
    static AFTER_DEL_CALL  = { value : "afterDel",  label : "Depois de Remover" };	

    // SEARCH TYPES
    static STRING_TYPE       = { value : "string",   label : "string" };
    static INT32_TYPE        = { value : "int32",    label : "int32" };
    static INT64_TYPE        = { value : "int64",    label : "int64" };
    static FLOAT_TYPE        = { value : "float",    label : "float" };
    static DOUBLE_TYPE       = { value : "double",   label : "double" };
    static BOOL_TYPE         = { value : "bool",     label : "bool" };
    static GEOPOINT_TYPE     = { value : "geopoint", label : "geopoint" };
    static ARRAY_STRING_TYPE = { value : "string[]", label : "string[]" };
    static ARRAY_INT32_TYPE  = { value : "int32[]",  label : "int32[]" };
    static ARRAY_INT64_TYPE  = { value : "int64[]",  label : "int64[]" };
    static ARRAY_FLOAT_TYPE  = { value : "float[]",  label : "float[]" };
    static ARRAY_DOUBLE_TYPE = { value : "double[]", label : "double[]" };
    static ARRAY_BOOL_TYPE   = { value : "bool[]",   label : "bool[]" };

    // DOCUMENTS TYPE CARDS
    static CPF_DOCUMENT_TYPE 	  = { value : "cpf", 	  label : "CPF", 		id : "cpf", 	 type : "individual"  };
    static CNPJ_DOCUMENT_TYPE  	  = { value : "cnpj",     label : "CNPJ", 		id : "cnpj", 	 type : "corporation" };	
    static PASSPORT_DOCUMENT_TYPE = { value : "passport", label : "Passaporte", id : "passport", type : "individual"  };
}
