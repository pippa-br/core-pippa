//@dynamic
export class BlockType extends Object
{
    static instance   : any;
    static isCreating : Boolean = false;

    constructor()
    {
        super();

        if(!BlockType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if(BlockType.instance == null)
        {
            BlockType.isCreating = true;
            BlockType.instance   = new BlockType();
            BlockType.isCreating = false;
        }

        return BlockType.instance;
    }

    register(data:any)
    {
        BlockType.instance[data.value.toUpperCase()] = data;
    }

    static type(key:string)
    {
        return {
            label : BlockType.instance[key.toUpperCase()].label,
            value : BlockType.instance[key.toUpperCase()].value,
        }
    }

    static block(key:string)
    {
        return BlockType.instance[key.toUpperCase()].block;
    }

    static getItems()
    {
        const items = [];

        for(const key in BlockType.instance)
        {
            items.push(BlockType.type(key));
        }

        return items;
    }
}
