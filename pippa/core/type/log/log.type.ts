export const LogType =
{
    ADD               : { id : 'add',              label : 'Novo',            value : 'add' },
    SET               : { id : 'set',              label : 'Alterado',        value : 'set' },
    DEL               : { id : 'del',              label : 'Removido',        value : 'del' },
    OFFLINE           : { id : 'offline',          label : 'Offline',         value : 'offline' },
    ARCHIVE           : { id : 'archive',          label : 'Arquivar',        value : 'archive' },
    UNARCHIVE         : { id : 'unarchive',        label : 'Desarquivar',     value : 'unarchive' },
    CANCEL            : { id : 'cancel',           label : 'Fechar',          value : 'cancel' },
	UNCANCEL          : { id : 'uncancel',         label : 'Abrir',           value : 'uncancel' },
    LOGIN             : { id : 'login',            label : 'Login',           value : 'login' },
    LOGOUT            : { id : 'logout',           label : 'Logout',          value : 'logout' },
    EXPORT        	  : { id : 'export',       	   label : 'Export',      	  value : 'export' },
    RECOVERY_PASSWORD : { id : 'recoveryPassword', label : 'Recuperar Senha', value : 'recoveryPassword' },
    ADD_FORM          : { id : 'addForm',          label : 'Novo Form',       value : 'addForm' },
    SET_FORM          : { id : 'setForm',          label : 'Form Alterado',   value : 'setForm' },
    DEL_FORM          : { id : 'delForm',          label : 'Form Removido',   value : 'delForm' },
}
