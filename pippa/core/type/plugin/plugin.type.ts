import { Core } from '../../util/core/core';

//@dynamic
export class PluginType extends Object
{
	/* COLLECTION */
	static ACCOUNT_PLUGIN    : string = 'ACCOUNT_PLUGIN';
    static APP_PLUGIN        : string = 'APP_PLUGIN';
    static USER_PLUGIN       : string = 'USER_PLUGIN';
    static DOCUMMENT_PLUGIN  : string = 'DOCUMENT_PLUGIN';
    static NAVIGATION_PLUGIN : string = 'NAVIGATION_PLUGIN';

    /* OBJECT */
	static SETTING_PLUGIN : string = 'SETTING_PLUGIN';
	
    static instance   : any;
    static isCreating : Boolean = false;

    constructor()
    {
        super();

        if(!PluginType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if(PluginType.instance == null)
        {
            PluginType.isCreating = true;
            PluginType.instance   = new PluginType();
            PluginType.isCreating = false;
        }

        return PluginType.instance;
    }

    register(data:any)
    {
        PluginType.instance[data.value.toUpperCase()] = data;
    }

    getInstance(key:string)
    {
        return Core.get().injector.get(PluginType.instance[key.toUpperCase()].plugin);
    }

    getClass(key:string)
    {
        return PluginType.instance[key.toUpperCase()].plugin;
    }
}
