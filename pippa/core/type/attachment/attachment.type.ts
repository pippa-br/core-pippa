export const AttachmentType =
{
    COMMENT  : { label : 'Comentátio', value : 'comment',  icon  : 'chatboxes' },
    IMAGE    : { label : 'Imagem',     value : 'image',    icon  : 'image',    accept : 'image/*' },
    DOCUMENT : { label : 'Documento',  value : 'document', icon  : 'document', accept : '.pdf,.doc,.docx,.xls,.xlsx' },
    AUDIO    : { label : 'Audio',      value : 'audio',    icon  : 'mic',      accept : 'audio/*' },
    VIDEO    : { label : 'Vídeo',      value : 'video',    icon  : 'videocam', accept : 'video/*' },
    GALLERY  : { label : 'Galeria',    value : 'gallery',  icon  : 'images',   accept : 'image/*' },
    BONUS    : { label : 'Bônus',      value : 'bonus',    icon  : 'gift' },
}
