//@dynamic
export class PartialType extends Object
{
    static instance   : any;
    static isCreating : Boolean = false;

    constructor()
    {
        super();

        if(!PartialType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if(PartialType.instance == null)
        {
            PartialType.isCreating = true;
            PartialType.instance   = new PartialType();
            PartialType.isCreating = false;
        }

        return PartialType.instance;
    }

    register(data:any)
    {
        PartialType.instance[data.value.toUpperCase()] = data;
    }

    static type(key:string)
    {
        return {
            label : PartialType.instance[key.toUpperCase()].label,
            value : PartialType.instance[key.toUpperCase()].value,
        }
    }

    static partial(key:string)
    {
        return PartialType.instance[key.toUpperCase()].partial;
    }
}
