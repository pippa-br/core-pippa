import { environment } from "src/environments/environment";
import { Core }         from "../../util/core/core";

//@dynamic
export class FunctionsType extends Object
{
    static instance   : any;
    static isCreating  = false;

    constructor()
    {
        super();

        if (!FunctionsType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if (FunctionsType.instance == null)
        {
            FunctionsType.isCreating = true;
            FunctionsType.instance   = new FunctionsType();
            FunctionsType.isCreating = false;
        }

        FunctionsType.instance.initialize();

        return FunctionsType.instance;
    }

    initialize()
    {
        /* APP USER FILTERV*/
        this.register("MENU_FILTER", async (menu:any) =>
        {
            /* VERIFICA PERMISSÕES PARA OS FILHOS */
            /* NAO ADICIONAR: menu._children.length > 0, pois depois de filtrar pode ter filho e ficar 0 */
            if (menu._children)
            {
                await menu._children.filterAsync(FunctionsType.get().getFunction("MENU_FILTER"));
            }

            /* FIND RULES FILTER */
            let ruleFilter : any;
            let pathGroup  : string;

            for (const key in menu.params)
            {
                if (menu.params[key].label == "ruleFilter")
                {
                    ruleFilter = menu.params[key].value;						
                }
                else if (menu.params[key].label == "pathGroup")
                {
                    pathGroup = menu.params[key].value;
                }
            }

            let groups = [ { value : "guest" } ];

            if (Core.get().user)
            {
                if (pathGroup)
                {
                    const group = await Core.get().user.getPathValue(pathGroup);

                    if (group)
                    {
                        groups = [ group ];
                    }						
                }
                else
                {
                    groups = Core.get().user.getGroups();
                }                    
            }

            //console.error(groups, Core.get().user);

            /* HAS RULE */
            if (ruleFilter)
            {
                const value = await this.getFunction(ruleFilter)(menu); 				
                return value;
            }
            else if (groups && menu.groups && menu.groups.length > 0)
            {
                let has = false;

                for (const key2 in groups)
                {
                    const userGroup = groups[key2].value;

                    for (const key3 in menu.groups)
                    {
                        const appGroup = menu.groups[key3].value;

                        if (userGroup.toLowerCase() == appGroup.toLowerCase())
                        {
                            has = true;                                
                        }
                    }
                }

                return has;
            }                
            else
            {
                return false;
            }
        });
 
        /* CALC */
        this.register("calc", (model:any, params?:any, setting?:any) =>
        {
            return new Promise((resolve:any) =>
            {
                /* PORQUE A FUNCAO PODE NÃO ESTÁ COMPLETA */
                try
                {
                    let precision = 2;

                    if (setting.precision)
                    {
                        precision = setting.precision;
                    }

                    Core.get().util.parseVariables(model, params).then((value:any) =>
                    {
                        let total = 0;

                        try
                        {
                            total = eval(value);
                        }
                        catch (e)
                        {
                            //
                        }

                        console.log("eval", value, total);

                        resolve(total.toFixed(precision));
                    });
                }
                catch (err)
                {
                    //
                }
            });
        });
		
        /* SUM */
        this.register("sum", (model:any, params?:any, setting?:any) =>
        {
            return new Promise((resolve:any) =>
            {
                /* PORQUE A FUNCAO PODE NÃO ESTÁ COMPLETA */
                try
                {
                    Core.get().util.parseVariables(model, params, true).then((values:any) =>
                    {
                        let total = 0;

                        for (const key in values)
                        {
                            for (const key2 in values[key])
                            {
                                total += parseFloat(values[key][key2]);	
                            }							
                        }

                        console.log("sum", values, total, model);

                        resolve(total);
                    });
                }
                catch (err)
                {
                    //
                }
            });
        });
		
        /* AVERAGE2 - NÃO CONSIDERA VALORES 0 */
        this.register("average2", (model:any, params?:any, setting?:any) =>
        {
            return new Promise((resolve:any) =>
            {
                /* PORQUE A FUNCAO PODE NÃO ESTÁ COMPLETA */
                try
                {
                    Core.get().util.parseVariables(model, params, true).then((values:any) =>
                    {
                        let total = 0;
                        let count = 0;

                        for (const key in values)
                        {
                            for (const key2 in values[key])
                            {
                                const value = parseFloat(values[key][key2]);
								
                                if (value)
                                {
                                    total += value;	
                                    count++;	
                                }
                            }
                        }

                        if (count > 0)
                        {
                            total = total / count;
                        }						

                        console.log("average2", values, total, model);

                        resolve(total);
                    });
                }
                catch (err)
                {
                    //
                }
            });
        });

        /* CONCAT */
        this.register("concat", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                Core.get().util.parseVariables(model, params).then((value:any) =>
                {
                    resolve(value);
                });

            });
        });

        /* getPathValue  */
        this.register("getPathValue", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getPathValue(params).then((value:any) =>
                {
                    resolve(value);
                });
            });
        });

        /* getProperty  */
        this.register("getProperty", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    resolve(value);
                });
            });
        });

        /* getSku  */
        this.register("getSku", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value instanceof Array)
                    {
                        let sku = "";

                        for (const item of value)
                        {
                            if (item.value)
                            {
                                sku += item.value + "-"
                            }                            
                        }                        

                        resolve(sku.slice(0, -1));
                    }

                    resolve(value);
                });
            });
        });

        /* getImageByVariant  */
        this.register("getImageByVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value instanceof Array && value.length == 2)
                    {
                        let sku = "";

                        if (value[1].length > 0)
                        {
                            for (const item of value[1])
                            {
                                if (item.value)
                                {
                                    sku += item.value + "-"
                                }

                                break;
                            }

                            sku = sku.slice(0, -1);
                        }
                        else
                        {
                            sku = "_default";
                        }      
                        
                        if (value[0] && value[0].data && value[0].data[sku])
                        {                            
                            resolve(value[0].data[sku].images);
                        }                                                
                    }

                    resolve(null);
                });                
            });
        });

        /* getIdVariant  */
        this.register("getIdVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 2)
                    {
                        if (value[1] instanceof Array)
                        {
                            let code = value[0].code + "-"; 

                            if (value[1].length > 0)
                            {
                                for (const item of value[1])
                                {
                                    code += item.value + "-"
                                }                                
                            }
                            else
                            {
                                code += "_default";
                            }

                            code = code.slice(0, -1);

                            resolve(code);
                        }
                    }                                        

                    resolve(null);
                });                
            });
        });

        /* getCodeVariant  */
        this.register("getCodeVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value instanceof Array)
                    {
                        let code = ""; 

                        if (value.length > 0)
                        {
                            for (const item of value)
                            {
                                code += item.value + "-"
                            }

                            code = code.slice(0, -1);
                        }
                        else
                        {
                            code = "_default";
                        }

                        resolve(code);
                    }

                    resolve(null);
                });                
            });
        });

        /* sum total  */
        this.register("sumTotal", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value instanceof Array)
                    {
                        let total = 0;

                        for (const item of value)
                        {
                            total += parseFloat(item.total);	  
                        }                        

                        resolve(total);
                    }

                    resolve(0);
                });
            });
        });

        /* get total By quantity, PRICE OR PROMOTION PRICE  */
        this.register("getTotalByQuantity", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value instanceof Array && value.length == 3)
                    {
                        // price > promotionalPrice
                        if (value[2] > 0 && value[1] > value[2])
                        {
                            resolve(value[0] * value[2]);
                        }
                        else
                        {
                            resolve(value[0] * value[1]);
                        }
                    }

                    resolve(0);
                });
            });
        });

        /* get total quantity  */
        this.register("getTotalQuantity", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    let count = 0;

                    for (const item of value)
                    {
                        count += item.quantity;
                    }

                    resolve(count);
                });
            });
        });

        /* caculateShipping  */
        this.register("caculateShipping", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 3 && value[1].zipcode)
                    {
                        let totalQuantity = 0;
                        let totalPrice    = 0;
                        let totalWeight   = 0;
                        let discountFixed = 0;

                        for (const item of value[0])
                        {
                            totalQuantity += item.quantity;
                            totalPrice    += item.total;
                            totalWeight   += item.weight || 300;                      
                        }

                        if (value[2])
                        {
                            discountFixed = value[2];
                        }

                        const data = {
                            accid       : Core.get().account.code,
                            appid       : "shippings",
                            colid       : "documentsShippings", 
                            platform    : Core.get().platform,
                            destination : value[1].zipcode,
                            weight     	: totalWeight,
                            totalItems  : totalPrice - discountFixed,
                        }
    
                        Core.get().api.callable("shippingApi/calculateShippings", data).then(async (result) => 
                        {
                            resolve(result.collection); 
                        });
                    }
                });
            });
        });

        /* getInstallments  */
        this.register("getInstallments", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 2)
                    {
                        const data = {
                            accid         : Core.get().account.code,
                            appid         : "gateway",
                            colid         : "documents",
                            subTotal      : value[0],
                            totalShipping : value[1],
                            document      : {
                                referencePath : setting.gatewayPath
                            }
                        }
     
                        Core.get().api.callable("pagarmeApi/getInstallments", data).then(async (result) => 
                        {
                            resolve(result.collection); 
                        });   
                    }                                     
                });
            });
        });

        /* getQuantityByVariant  */
        this.register("getQuantityStockByVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 2 && value[0].reference)
                    {
                        const data = {
                            accid    : Core.get().account.code,
                            appid    : "product",
                            colid    : "documents",
                            platform : Core.get().platform,
                            mapItems : {
                                referencePath : environment.defaultMapItems
                            },
                            document : {
                                referencePath : value[0].reference.path,
                            },
                            data      : value[1],
                            stockName : {
                                referencePath : setting.stockNamePath
                            }
                        }
     
                        Core.get().api.callable("productApi/getQuantityStock", data).then(async (result) => 
                        {
                            resolve(result.total); 
                        });   
                    }                                     
                });
            });
        });

        /* getPriceByVariant  */
        this.register("getPriceByVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 2 && value[0].reference)
                    {
                        const data = {
                            accid    : Core.get().account.code,
                            appid    : "product",
                            colid    : "documents",
                            platform : Core.get().platform,
                            mapItems : {
                                referencePath : environment.defaultMapItems
                            },
                            document : {
                                referencePath : value[0].reference.path,
                            },
                            data      : value[1],
                            priceName : {
                                referencePath : setting.priceNamePath
                            }
                        }
     
                        Core.get().api.callable("productApi/getPrice", data).then(async (result) => 
                        {
                            resolve(result.total); 
                        });
                    }                                     
                });
            });
        });

        /* getCostPriceByVariant  */
        this.register("getCostPriceByVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {
                    if (value && value.length == 2 && value[0].reference)
                    {
                        const data = {
                            accid    : Core.get().account.code,
                            appid    : "product",
                            colid    : "documents",
                            platform : Core.get().platform,
                            mapItems : {
                                referencePath : environment.defaultMapItems
                            },
                            document : {
                                referencePath : value[0].reference.path,
                            },
                            data      : value[1],
                            priceName : {
                                referencePath : setting.priceNamePath
                            }
                        }
     
                        Core.get().api.callable("productApi/getCostPrice", data).then(async (result) => 
                        {
                            resolve(result.total); 
                        });
                    }                                     
                });
            });
        });

        /* getCodeByVariant  */
        this.register("getCodeByVariant", (model:any, params:any, setting:any) =>
        {
            return new Promise((resolve:any) =>
            {
                model.getProperty(params).then((value:any) =>
                {                    
                    if (value && value.length == 2 && value[0].reference)
                    {
                        const data = {
                            accid    : Core.get().account.code,
                            appid    : "product",
                            colid    : "documents",
                            platform : Core.get().platform,
                            mapItems : {
                                referencePath : environment.defaultMapItems
                            },
                            document : {
                                referencePath : value[0].reference.path,
                            },
                            data : value[1]
                        }
     
                        Core.get().api.callable("productApi/getCode", data).then(async (result) => 
                        {
                            resolve(result.data); 
                        });
                    }                                     
                });
            });
        });
    }

    register(key:string, fc:any)
    {
        FunctionsType.instance[key.toUpperCase()] = fc;
    }

    getFunction(key:string)
    {
        if (key)
        {
            return FunctionsType.instance[key.toUpperCase()];
        }
        else
        {
            return (data) => 
            {
                return new Promise((resolve:any) =>
                {
                    resolve(null)
                });
            };
        }
    }
}
