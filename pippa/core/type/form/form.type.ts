//@dynamic
export class FormType extends Object
{
	static FLAT_TYPE      : any = { id : "flat", 	  label  : "Flat",      value : "flat" };
    static SEGMENT_TYPE   : any = { id : "segment",   label  : "Segment",   value : "segment" };
    static WIZARD_TYPE    : any = { id : "wizard",    label  : "Wizard",    value : "wizard" };
    static ACCORDION_TYPE : any = { id : "accordion", label  : "Accordion", value : "accordion" };
	static EXPAND_TYPE    : any = { id : "expand",    label  : "Expand",    value : "expand" };
    static STEPS_TYPE     : any = { id : "steps",     label  : "Steps",     value : "steps" };
	
	static ROUTER_OPEN_MODE : any = { id : 'Router', label : 'Router',  value : 'Router'};
	static MODAL_OPEN_MODE  : any = { id : 'modal',  label : 'Modal',   value : 'modal'};
    static SIDE_OPEN_MODE   : any = { id : 'side',   label : 'Lateral', value : 'side'};    

	static ADD_MODE : any = { id : 'add', label : 'Adicionar', value : 'add'};
	static SET_MODE : any = { id : 'set', label : 'Editar',    value : 'set'};
    static DEL_MODE : any = { id : 'del', label : 'Remover',   value : 'del'};    

	static ALERT_ACTION    : any = { id : 'alert',    label : 'Alert',     value : 'alert'};
	static REDIRECT_ACTION : any = { id : 'redirect', label : 'Redirect',  value : 'redirect'};
	static PAYMENT_ACTION  : any = { id : 'payment',  label : 'Pagamento', value : 'payment'};

	static CLEAR_CALLBACK : any = { id : 'clear', label : 'Limpar', value : 'clear'};
	static CLOSE_CALLBACK : any = { id : 'close', label : 'Fechar', value : 'close'};
	static BACK_CALLBACK  : any = { id : 'back',  label : 'Voltar', value : 'back'};

    static instance   : any;
    static isCreating : Boolean = false;

    constructor()
    {
        super();

        if(!FormType.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if(FormType.instance == null)
        {
            FormType.isCreating = true;
            FormType.instance   = new FormType();
            FormType.isCreating = false;
        }

        return FormType.instance;
	}
	
	update(name:string, data:any)
    {
        FormType[name.toUpperCase()] = Object.assign(FormType[name.toUpperCase()], data);
    }

    register(data:any)
    {
        FormType.instance[data.value.toUpperCase()] = data;
    }

    static container(key:string)
    {
        return FormType.instance[key.toUpperCase()].container;
    }
}
