export const PaginationType =
{
		NUMBER     : { id : 'number',    label : 'Número',   	   value : 'number'    },
		DAY        : { id : 'day',       label : 'Dia', 	  	   value : 'day'       },
		WEEK       : { id : 'week',      label : 'Semanal',  	   value : 'week'      },
		DAY7D      : { id : 'day7D',     label : '+7 Dias', 	   value : 'day7D'     },
		DAY7       : { id : 'day7',      label : '7 Dias', 	     value : 'day7'      },
		DAY15      : { id : 'day15',     label : '15 Dias', 	   value : 'day15'     },
		MONTH      : { id : 'month',     label : 'Mês',   		   value : 'month'     },
		MONTH_YEAR : { id : 'monthYear', label : 'Ano',   	     value : 'monthYear' },
		YEAR       : { id : 'year',      label : 'Anual',        value : 'year'      },
		INFINITE   : { id : 'infinite',  label : 'Infinite',     value : 'infinite'  },
}
