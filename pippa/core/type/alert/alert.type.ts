//@dynamic
export class AlertType extends Object
{
	static CHAT    = { value : 'chat',    label : 'Chat' };
	static REQUEST = { value : 'request', label : 'Pedido' };
}
