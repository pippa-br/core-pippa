//@dynamic
export class TimezoneType extends Object
{
	static AMERICA_SAO_PAULO_TIMEZONE = { value : '-03:00', label : 'America/Sao_Paulo', id : 'America/Sao_Paulo' };
    static UTC_TIMEZONE = { value : '+00:00', label : 'Etc/UTC', id : 'Etc/UTC' };
}
