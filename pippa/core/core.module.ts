import { HammerModule } from "@angular/platform-browser";
import { NgModule, Injectable } from "@angular/core";
import { CommonModule } from "@angular/common";
//import { CurrencyPipe } from '@angular/common';
import { provideEnvironmentNgxMask, IConfig, NgxMaskDirective, NgxMaskPipe } from "ngx-mask"
import { IonicModule } from "@ionic/angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MixedCdkDragDropModule } from "angular-mixed-cdk-drag-drop";
//import { CurrencyMaskModule } from "ng2-currency-mask";
import { NgSelectModule } from "@ng-select/ng-select";
import { FileUploadModule } from "ng2-file-upload";
import { ImageCropperModule } from "ngx-image-cropper";
import { HttpClientModule } from "@angular/common/http";
//import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { ColorPickerModule } from "ngx-color-picker";
import { MbscModule, setOptions, localePtBR } from "@mobiscroll/angular";
import { MatStepperModule } from "@angular/material/stepper"
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTabsModule } from "@angular/material/tabs"
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatNativeDateModule } from "@angular/material/core";
import { MatCardModule } from "@angular/material/card";
import { MatPaginatorModule, MatPaginatorIntl } from "@angular/material/paginator";
import { AgmCoreModule } from "@agm/core";
//import { IonContent } from '@ionic/angular';
import { NgChartsModule } from "ng2-charts";
import { MonacoEditorModule } from "ngx-monaco-editor";
import { LazyMapsAPILoaderConfigLiteral, LAZY_MAPS_API_CONFIG } from "@agm/core";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { PlyrModule } from "ngx-plyr";
import { ClickOutsideModule } from "ng-click-outside";
import { ShareModule } from "ngx-sharebuttons";
import { A11yModule } from "@angular/cdk/a11y"
import { NgxPrettyCheckboxModule } from "ngx-pretty-checkbox";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from "ng-recaptcha-custom"

/* NATIVE */
import { BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { SocialSharing }  from "@ionic-native/social-sharing/ngx";

import { Core } from "./util/core/core";
import { environment } from "../../../environments/environment";

setOptions({
    theme  : "ios",
    locale : localePtBR,
    //themeVariant: 'dark'
});

export class MapsConfig implements LazyMapsAPILoaderConfigLiteral 
{
    public apiKey: string
    public libraries: string[]

    constructor() 
    {
        //console.log('--------', Core.get());
    }
}

/* MODEL */
//export { Field } from './model/field/field';

/* PAGE */
import { FieldType }   from "./type/field/field.type";
import { PartialType } from "./type/partial/partial.type";
import { PluginType }  from "./type/plugin/plugin.type";
import { BlockType }   from "./type/block/block.type";
import { FormType }    from "./type/form/form.type";

/* BASE */
/*import { BaseRoot }      from './base/root/base.root';
import { BaseComponent } from './base/base.component';
import { BaseForm }      from './base/form/base.form';
import { BaseFilter }    from './base/filter/base.filter';
import { BasePage }      from './base/page/base.page';
import { BaseModal }     from './base/modal/base.modal';
import { BaseGrid }      from './base/grid/base.grid';
import { BaseViewer }    from './base/viewer/base.viewer';
import { BaseTabs }      from './base/tabs/base.tabs';
import { BaseApp }       from './base/app/base.app';*/

/* POPOVER */
import { MenuPopover } 		  from "./popover/menu/menu.popover";
import { AttachmentPopover }  from "./popover/attachment/attachment.popover";
import { ListPopover } 		  from "./popover/list/list.popover";
import { ColumnOrderPopover } from "./popover/column.order/column.order.popover";
import { FieldFormPopover }   from "./popover/field.form/field.form.popover";
import { GridPopover } 		  from "./popover/grid/grid.popover";
import { FormItemPopover }    from "./popover/form.item/form.item.popover";
import { FormBlockPopover }   from "./popover/form.block/form.block.popover";
import { FormFilterPopover }  from "./popover/form.filter/form.filter.popover";
import { StepFormPopover }    from "./popover/step.form/step.form.popover";
import { FormFormPopover }    from "./popover/form.form/form.form.popover";
import { ExportPopover }      from "./popover/export/export.popover";
import { EmailPopover }       from "./popover/email/email.popover";

/* MODAL */
import { EmbedModal } 		from "./modal/embed/embed.modal";
import { IframeModal } 		from "./modal/iframe/iframe.modal";
import { AvatarModal } 		from "./modal/avatar/avatar.modal";
import { PDFModal } 		from "./modal/pdf/pdf.modal";
import { ComentImageModal } from "./modal/coment.image/coment.image.modal";
import { GalleryModal } 	from "./modal/gallery/gallery.modal";
import { ViewerModal } 		from "./modal/viewer/viewer.modal";
import { AgreeModal } 		from "./modal/agree/agree.modal";

/* MODEL */
import { BaseModel } from "./model/base.model";
import { Field }     from "./model/field/field";

/* FACTORY */
import { ComponentFactory } from "./factory/component/component.factory";

/* CONTROLLER */
import { NavController } from "./controller/nav/nav.controller";
import { ModalController } from "./controller/modal/modal.controller";

/* DIRECTIVE */
import { LastElementDirective } from "./directive/last.element/last.element.directive";
import { DirectionsMapDirective } from "./directive/agm.map/agm.map.directions";
import { BackgroundSliderDirective } from "./directive/background.slider/background.slider.directive";
import { AutosizeDirective } from "./directive/autosize/autosize.directive";
import { AclDirective } from "./directive/acl/acl.directive";
import { FocusDirective } from "./directive/focus/focus.directive";
import { AutoPlayDirective } from "./directive/auto.play/auto.play.directive";
import { ShadowCssDirective } from "./directive/shadow.css/shadow.css.directive";
import { AdjustKeyboardDirective } from "./directive/adjust.keyboard/adjust.keyboard.directive";
import { HiddenDirective } from "./directive/hidden/hidden.directive";
import { StickyDirective } from "./directive/sticky/sticky.directive";
import { SourceCodeDirective } from "./directive/source.code/source.code.directive";
import { CurrencyMaskDirective } from "./directive/currencyMask/currencyMask";

/* PIPE */
import { SafePipe } 			 from "./pipe/safe/safe.pipe";
import { ReversePipe } 			 from "./pipe/reverse/reverse.pipe";
import { BreakLinePipe } 		 from "./pipe/break.line/break.line.pipe";
import { DateFormatPipe } 		 from "./pipe/date.format/date.format.pipe";
import { CurrencyFormatPipe } 	 from "./pipe/currency.format/currency.format.pipe";
import { IntFormatPipe } 	     from "./pipe/int.format/int.format.pipe";
import { NotificationCountPipe } from "./pipe/notification.count/notification.count.pipe";
import { FilePipe } 			 from "./pipe/file/file.pipe";
import { FilesPipe } 			 from "./pipe/files/files.pipe";
import { ResumePipe } 			 from "./pipe/resume/resume.pipe";
import { TranslatePipe } 		 from "./pipe/translate/translate.pipe";
import { CardMaskPipe }			 from "./pipe/card.mask/card.mask.pipe";
import { ArrayPipe }			 from "./pipe/array/array.pipe";
import { AgePipe }				 from "./pipe/age/age.pipe";
import { PropertyPipe }			 from "./pipe/property/property.pipe";
import { AddressFormatPipe }	 from "./pipe/address.format/address.format.pipe";
import { TimeFormatPipe }	 	 from "./pipe/time.format/time.format.pipe";
import { UniquePipe }	 	 	 from "./pipe/unique/unique.pipe";
import { NbspRemovePipe }	 	 from "./pipe/nbsp.remove/nbsp.remove.pipe";
import { BaseListPipe }	 	 	 from "./pipe/base.list/base.list.pipe";
import { AvatarPipe }	 	 	 from "./pipe/avatar/avatar.pipe";

/* PLUGIN */
import { DocumentPlugin } from "./plugin/document/document.plugin";
import { EventPlugin } from "./plugin/event/event.plugin";
import { VideoPlugin } from "./plugin/video/video.plugin";
import { AclPlugin } from "./plugin/acl/acl.plugin";
import { FormPlugin } from "./plugin/form/form.plugin";
import { AppPlugin } from "./plugin/app/app.plugin";
import { GridPlugin } from "./plugin/grid/grid.plugin";
import { ViewerPlugin } from "./plugin/viewer/viewer.plugin";
import { AccountPlugin } from "./plugin/account/account.plugin";
import { EmailTemplatePlugin } from "./plugin/email.template/email.template.plugin";
import { NotificationTemplatePlugin } from "./plugin/notification.template/notification.template.plugin";
import { EmailPlugin } from "./plugin/email/email.plugin";
import { GatewayPlugin } from "./plugin/gateway/gateway.plugin";
import { FilterPlugin } from "./plugin/filter/filter.plugin";
import { FieldPlugin } from "./plugin/field/field.plugin";
import { UploadPlugin } from "./plugin/upload/upload.plugin";
import { ExportPlugin } from "./plugin/export/export.plugin";
import { UserPlugin } from "./plugin/user/user.plugin";
import { AuthPlugin } from "./plugin/auth/auth.plugin";
import { MessagePlugin } from "./plugin/message/message.plugin";
import { ChatPlugin } from "./plugin/chat/chat.plugin";
import { LessonPlugin } from "./plugin/lesson/lesson.plugin";
import { MenuPlugin } from "./plugin/menu/menu.plugin";
import { OptionPlugin } from "./plugin/option/option.plugin";
import { CheckoutPlugin } from "./plugin/checkout/checkout.plugin";
import { GooglePlugin } from "./plugin/google/google.plugin";
import { CartPlugin } from "./plugin/cart/cart.plugin";
import { RequestPlugin } from "./plugin/request/request.plugin";

/* BLOCK */
import { FileViewerBlock }   from "./component/block/file.viewer/file.viewer.block";
import { AudioPlayerBlock }  from "./component/block/audio.player/audio.player.block";
import { RadioPlayerBlock }  from "./component/block/radio.player/radio.player.block";
import { UserProfileBlock }  from "./component/block/user.profile/user.profile.block";
import { MenuNavBlock }      from "./component/block/menu.nav/menu.nav.block";
import { MenuFooterBlock }   from "./component/block/menu.footer/menu.footer.block";
import { MenuTileBlock }     from "./component/block/menu.tile/menu.tile.block";
import { SlideBlock }        from "./component/block/slide/slide.block";
import { BibleBlock } 	     from "./component/block/bible/bible.block";
import { FilterHeaderBlock } from "./component/block/filter.header/filter.header.block";
import { FilterChartBlock }  from "./component/block/filter.chart/filter.chart.block";
import { AnalyticsViewerBlock } from "./component/block/analytics.viewer/analytics.viewer.block";

/* PAGINATION */
import { NextPagination } from "./component/pagination/next/next.pagination";
import { NumberPagination } from "./component/pagination/number/number.pagination";
import { OnlyNextPagination } from "./component/pagination/only.next/only.next.pagination";

// SERVICE 
import { PwaUpdateService } from "./service/pwa.update.service/PwaUpdateService";

/* INPUT */
import { AlnumInput } from "./component/input/alnum/alnum.input";
import { AvatarInput } from "./component/input/avatar/avatar.input";
import { CNPJInput } from "./component/input/cnpj/cnpj.input";
import { TextInput } from "./component/input/text/text.input";
import { NumberInput } from "./component/input/number/number.input";
import { DateInput } from "./component/input/date/date.input";
import { EmailInput } from "./component/input/email/email.input";
import { AddressInput } from "./component/input/address/address.input";
import { CreditCardInput } from "./component/input/credit.card/credit.card.input";
import { CPFInput } from "./component/input/cpf/cpf.input";
import { StarInput } from "./component/input/star/star.input";
import { ItemStarInput } from "./component/input/star/item.star.input";
import { ZipcodeBRInput } from "./component/input/zipcode.br/zipcode.br.input";
import { SelectInput } from "./component/input/select/select.input";
import { SelectIconInput } from "./component/input/select.icon/select.icon.input";
import { CurrencyInput } from "./component/input/currency/currency.input";
import { PercentageInput } from "./component/input/percentage/percentage.input";
import { RadioInput } from "./component/input/radio/radio.input";
import { HiddenInput } from "./component/input/hidden/hidden.input";
import { IPInput } from "./component/input/ip/ip.input";
import { HiddenLabelInput } from "./component/input/hidden.label/hidden.label.input";
import { PhoneInput } from "./component/input/phone/phone.input";
import { TextareaInput } from "./component/input/textarea/textarea.input";
import { PasswordInput } from "./component/input/password/password.input";
import { CVVInput } from "./component/input/cvv/cvv.input";
import { ConfirmPasswordInput } from "./component/input/confirm.password/confirm.password.input";
import { MonthYearInput } from "./component/input/month.year/month.year.input";
import { YearInput } from "./component/input/year/year.input";
import { DayMonthInput } from "./component/input/day.month/day.month.input";
import { CardNumberInput } from "./component/input/card.number/card.number.input";
import { MultiUploadJsonInput } from "./component/input/multi.upload.json/multi.upload.json.input";
import { MultiCheckboxInput } from "./component/input/multi.checkbox/multi.checkbox.input";
import { UploadJsonInput } from "./component/input/upload.json/upload.json.input";
import { VideoUploadInput } from "./component/input/video.upload/video.upload.input";
import { ListInput } from "./component/input/list/list.input";
import { FieldsetInput } from "./component/input/fieldset/fieldset.input";
import { BankInput } from "./component/input/bank/bank.input"; 
import { IntegerInput } from "./component/input/integer/integer.input";
import { ColumnGridInput } from "./component/input/column.grid/column.grid.input";
import { ReferenceMultiSelectInput } from "./component/input/reference.multi.select/reference.multi.select.input";
import { ProductMultiSelectInput } from "./component/input/product.multi.select/product.multi.select.input";
import { ReferenceSelectInput } from "./component/input/reference.select/reference.select.input";
import { ReferenceRadioInput } from "./component/input/reference.radio/reference.radio.input";
import { ReferenceTextInput } from "./component/input/reference.text/reference.text.input";
import { ReferenceCouponInput } from "./component/input/reference.coupon/reference.coupon.input";
import { ReferenceAppsInput } from "./component/input/reference.apps/reference.apps.input";
import { ArrangeInput } from "./component/input/arrange/arrange.input";
import { CronJobInput } from "./component/input/cron.job/cron.job.input";
import { CheckboxInput } from "./component/input/checkbox/checkbox.input";
import { CheckboxObjectInput } from "./component/input/checkbox.object/checkbox.object.input";
import { CPFCNPJInput } from "./component/input/cpf.cnpj/cpf.cnpj.input";
import { GeneratePasswordInput } from "./component/input/generate.password/generate.password.input";
import { ScanButtonInput } from "./component/input/scan.button/scan.button.input";
import { Table3x3Input } from "./component/input/table.3x3/table.3x3.input";
import { TableInput } from "./component/input/table/table.input";
import { DiscInput } from "./component/input/disc/disc.input";
import { DNAInput } from "./component/input/dna/dna.input";
import { ScaleInput } from "./component/input/scale/scale.input";
import { ReferenceFieldsInput } from "./component/input/reference.fields/reference.fields.input";
import { FieldsInput } from "./component/input/fields/fields.input";
import { ReferenceOptionsInput } from "./component/input/reference.options/reference.options.input";
import { ReferenceMatchInput } from "./component/input/reference.match/reference.match.input";
import { ReferenceArrangeInput } from "./component/input/reference.arrange/reference.arrange.input";
import { QuestionInput } from "./component/input/question/question.input";
import { ReferenceFormInput } from "./component/input/reference.form/reference.form.input";
import { HeaderInput } from "./component/input/header/header.input";
import { ErrorInput } from "./component/input/error/error.input";
import { IconInput } from "./component/input/icon/icon.input";
import { AttachmentInput } from "./component/input/attachment/attachment.input";
import { RichTextInput } from "./component/input/rich.text/rich.text.input";
import { SourceCodeInput } from "./component/input/source.code/source.code.input";
import { ToggleInput } from "./component/input/toggle/toggle.input";
import { UrlInput } from "./component/input/url/url.input";
import { EmbedInput } from "./component/input/embed/embed.input";
import { CheckboxTitleInput } from "./component/input/checkbox.title/checkbox.title.input";
import { AgreeInput } from "./component/input/agree/agree.input";
import { SubFormInput } from "./component/input/sub.form/sub.form.input";
import { DataFormInput } from "./component/input/data.form/data.form.input";
import { MultiSelectInput } from "./component/input/multi.select/multi.select.input";
import { MatrixInput } from "./component/input/matrix/matrix.input";
import { LabelInput } from "./component/input/label/label.input";
import { IframeInput } from "./component/input/iframe/iframe.input";
import { BirthdayInput } from "./component/input/birthday/birthday.input";
import { NoteInput } from "./component/input/note/note.input";
import { StepItemInput } from "./component/input/step.item/step.item.input";
import { StepBlockInput } from "./component/input/step.block/step.block.input";
import { ReferenceStepInput } from "./component/input/reference.step/reference.step.input";
import { TimeInput } from "./component/input/time/time.input";
import { TimeSecondsInput } from "./component/input/time.seconds/time.seconds.input";
import { AutoFillInput } from "./component/input/auto.fill/auto.fill.input";
import { ReferenceAutoFillInput } from "./component/input/reference.auto.fill/reference.auto.fill.input";
import { ReferenceMultiCheckboxInput } from "./component/input/reference.multi.checkbox/reference.multi.checkbox.input";
import { ReferenceCheckboxInput } from "./component/input/reference.checkbox/reference.checkbox.input";
import { SignatureInput } from "./component/input/signature/signature.input";
import { ColorPickerInput } from "./component/input/color.picker/color.picker.input";
import { ObjectInput } from "./component/input/object/object.input";
import { GeolocationInput } from "./component/input/geolocation/geolocation.input";
import { JsonFirebaseInput } from "./component/input/json.firebase/json.firebase.component";
import { PaymentStatusInput } from "./component/input/payment.status/payment.status.input";
import { DistanceInput } from "./component/input/distance/distance.input";
import { CommentInput } from "./component/input/comment/comment.input";
import { CorreiosInput } from "./component/input/correios/correios.input";
import { ListMultiSelectInput } from "./component/input/list.multi.select/list.multi.select.input";
import { ListMultiRadioInput } from "./component/input/list.multi.radio/list.multi.radio.input";
import { ViewerInput } from "./component/input/viewer/viewer.input";

/* SETTING */
import { ItemsSetting } from "./component/setting/items/items.setting";
import { Table3x3Setting } from "./component/setting/table.3x3/table.3x3.setting";
import { DiscSetting } from "./component/setting/disc/disc.setting";
import { QuestionSetting } from "./component/setting/question/question.setting";
import { DNASetting } from "./component/setting/dna/dna.setting";
import { DNA2Setting } from "./component/setting/dna2/dna2.setting";
import { AppSetting } from "./component/setting/app/app.setting";
import { GridSetting } from "./component/setting/grid/grid.setting";
import { FieldTypeSetting } from "./component/setting/field.type/field.type.setting";

/* OUTPUT */
import { Table3x3Output } from "./component/output/table.3x3/table.3x3.output";
import { TableOutput } from "./component/output/table/table.output";
import { TextOutput } from "./component/output/text/text.output";
import { ObjectOutput } from "./component/output/object/object.output";
import { RichTextOutput } from "./component/output/rich.text/rich.text.output";
import { SourceCodeOutput } from "./component/output/source.code/source.code.output";
import { LockOutput } from "./component/output/lock/lock.output";
import { ImageOutput } from "./component/output/image/image.output";
import { AudioOutput } from "./component/output/audio/audio.output";
import { PDFOutput } from "./component/output/pdf/pdf.output";
import { UploadJsonOutput } from "./component/output/upload.json/upload.json.output";
import { DownloadOutput } from "./component/output/download/download.output";
import { OptionOutput } from "./component/output/option/option.output";
import { AddressOutput } from "./component/output/address/address.output";
import { DiscOutput } from "./component/output/disc/disc.output";
import { DNAOutput } from "./component/output/dna/dna.output";
import { DNA2Output } from "./component/output/dna2/dna2.output";
import { StepItemOutput } from "./component/output/step.item/step.item.output";
import { SumFormOutput } from "./component/output/sum.form/sum.form.output";
import { SumDocumentsOutput } from "./component/output/sum.documents/sum.documents.output";
import { ReferenceListOutput } from "./component/output/reference.list/reference.list.output";
import { VideoUrlOutput } from "./component/output/video.url/video.url.output";
import { VideoPlaylistOutput } from "./component/output/video.playlist/video.playlist.output";
import { QuestionOutput } from "./component/output/question/question.output";
import { ReferenceFormOutput } from "./component/output/reference.form/reference.form.output";
import { SelectIconOutput } from "./component/output/select.icon/select.icon.output";
import { ReferenceSelectOutput } from "./component/output/reference.select/reference.select.output";
import { ViewerOutput } from "./component/output/viewer/viewer.output";
import { ToggleOutput } from "./component/output/toggle/toggle.output";
import { EmbedOutput } from "./component/output/embed/embed.output";
import { TitleOutput } from "./component/output/title/title.output";
import { ButtonOutput } from "./component/output/button/button.output";
import { ShareButtonsOutput } from "./component/output/share.buttons/share.buttons.output";
import { TextareaOutput } from "./component/output/textarea/textarea.output";
import { DateOutput } from "./component/output/date/date.output";
import { PhoneOutput } from "./component/output/phone/phone.output";
import { CurrencyOutput } from "./component/output/currency/currency.output";
import { FunctionOutput } from "./component/output/function/function.output";
import { BirthdayOutput } from "./component/output/birthday/birthday.output";
import { HasDocumentOutput } from "./component/output/has.document/has.document.output";
import { LastDocumentOutput } from "./component/output/last.document/last.document.output";
import { TimeOutput } from "./component/output/time/time.output";
import { IframeOutput } from "./component/output/iframe/iframe.output";
import { PageUrlOutput } from "./component/output/page.url/page.url.output";
import { FieldsetOutput } from "./component/output/fieldset/fieldset.output";
import { SubFormOutput } from "./component/output/sub.form/sub.form.output";
import { StockProductsOutput } from "./component/output/stock.products/stock.products.output";
import { StockMovimentOutput } from "./component/output/stock.moviment/stock.moviment.output";
import { PriceProductsOutput } from "./component/output/price.products/price.products.output";
import { DataFormOutput } from "./component/output/data.form/data.form.output";
import { MultiOptionOutput } from "./component/output/multi.option/multi.option.output";
import { UrlOutput } from "./component/output/url/url.output";
import { BankOutput } from "./component/output/bank/bank.output";
import { VerifyPaymentOutput } from "./component/output/verify.payment/verify.payment.output";
import { ChargesPagarmeOutput } from "./component/output/charges.pagarme/charges.pagarme.output";
import { TrackingCorreiosOutput } from "./component/output/tracking.correios/tracking.correios.output";
import { TrackingReversalOutput } from "./component/output/tracking.reversal/tracking.reversal.output";
import { TimeSecondsOutput } from "./component/output/time.seconds/time.seconds.output";
import { AvatarOutput } from "./component/output/avatar/avatar.output";
import { PercentageOutput } from "./component/output/percentage/percentage.output";
import { QRCodeOutput } from "./component/output/qrcode/qrcode.output";
import { PasswordOutput } from "./component/output/password/password.output";
import { YoutubeLiveOutput } from "./component/output/youtube.live/youtube.live.output";
import { BarcodeOutput } from "./component/output/barcode/barcode.output";
import { MenuOutput } from "./component/output/menu/menu.output";
import { CommentOutput } from "./component/output/comment/comment.output";
import { DistanceOutput } from "./component/output/distance/distance.output";
import { CreditCardOutput } from "./component/output/credit.card/credit.card.output";
import { IntegerOutput } from "./component/output/integer/integer.output";
import { StarOutput } from "./component/output/star/star.output";
import { MatrixOutput } from "./component/output/matrix/matrix.output";
import { CronJobOutput } from "./component/output/cron.job/cron.job.output";
import { AgreeOutput } from "./component/output/agree/agree.output";
import { ListMultiSelectOutput } from "./component/output/list.multi.select/list.multi.select.output";

/* TRANSFORMS */
import { TextTransform } from "./component/transform/text/text.transform";
import { Table3x3Transform } from "./component/transform/table.3x3/table.3x3.transform";
import { OptionTransform } from "./component/transform/option/option.transform";
import { HiddenTransform } from "./component/transform/hidden/hidden.transform";
import { AddressTransform } from "./component/transform/address/address.transform";
import { DiscTransform } from "./component/transform/disc/disc.transform";
import { ReferenceListTransform } from "./component/transform/reference.list/reference.list.transform";
import { CommentTransform } from "./component/transform/comment/comment.transform";
import { VideoUrlTransform } from "./component/transform/video.url/video.url.transform";
import { VideoPlaylistTransform } from "./component/transform/video.playlist/video.playlist.transform";
import { QuestionTransform } from "./component/transform/question/question.transform";
import { ReferenceFormTransform } from "./component/transform/reference.form/reference.form.transform";
import { LabelTransform } from "./component/transform/label/label.transform";
import { SelectIconTransform } from "./component/transform/select.icon/select.icon.transform";
import { ReferenceTransform } from "./component/transform/reference/reference.transform";
import { ReferenceCouponTransform } from "./component/transform/reference.coupon/reference.coupon.transform";
import { SumFormTransform } from "./component/transform/sum.form/sum.form.transform";
import { SumDocumentsTransform } from "./component/transform/sum.documents/sum.documents.transform";
import { CountDocumentsTransform } from "./component/transform/count.documents/count.documents.transform";
import { UploadJsonTransform } from "./component/transform/upload.json/upload.json.transform";
import { DNATransform } from "./component/transform/dna/dna.transform";
import { DNA2Transform } from "./component/transform/dna2/dna2.transform";
import { DateTransform } from "./component/transform/date/date.transform";
import { FunctionTransform } from "./component/transform/function/function.transform";
import { CheckboxTransform } from "./component/transform/checkbox/checkbox.transform";
import { HasDocumentTransform } from "./component/transform/has.document/has.document.transform";
import { LastDocumentTransform } from "./component/transform/last.document/last.document.transform";
import { FieldsetTransform } from "./component/transform/fieldset/fieldset.transform";
import { SubFormTransform } from "./component/transform/sub.form/sub.form.transform";
import { StockProductsTransform } from "./component/transform/stock.products/stock.products.transform";
import { StockMovimentTransform } from "./component/transform/stock.moviment/stock.moviment.transform";
import { PriceProductsTransform } from "./component/transform/price.products/price.products.transform";
import { DataFormTransform } from "./component/transform/data.form/data.form.transform";
import { UrlTransform } from "./component/transform/url/url.transform";
import { PageUrlTransform } from "./component/transform/page.url/page.url.transform";
import { BankTransform } from "./component/transform/bank/bank.transform";
import { MultiCheckboxTransform } from "./component/transform/multi.checkbox/multi.checkbox.transform";
import { TimeSecondsTransform } from "./component/transform/time.seconds/time.seconds.transform";
import { CentavosTransform } from "./component/transform/centavos/centavos.transform";
import { VerifyPaymentTransform } from "./component/transform/verify.payment/verify.payment.transform";
import { PaymentStatusTransform } from "./component/transform/payment.status/payment.status.transform";
import { CurrencyTransform } from "./component/transform/currency/currency.transform";
import { SumFieldsetTransform } from "./component/transform/sum.fieldset/sum.fieldset.transform";
import { QRCodeTransform } from "./component/transform/qrcode/qrcode.transform";
import { CreditCardTransform } from "./component/transform/credit.card/credit.card.transform";
import { NumberTransform } from "./component/transform/number/number.transform";
import { StepItemTransform } from "./component/transform/step.item/step.item.transform";
import { AgreeTransform } from "./component/transform/agree/agree.transform";

/* PARTIAL */
//import { BasePartial }       from './component/partial/base.partial';
import { CardPartial } from "./component/partial/card/card.partial";
import { TrPartial } from "./component/partial/tr/tr.partial";
import { TdPartial } from "./component/partial/td/td.partial";
import { ThPartial } from "./component/partial/th/th.partial";
import { DivPartial } from "./component/partial/div/div.partial";
import { TbodyPartial } from "./component/partial/tbody/tbody.partial";
import { RowPartial } from "./component/partial/row/row.partial";
import { ColPartial } from "./component/partial/col/col.partial";
import { LabelPartial } from "./component/partial/label/label.partial";
import { ButtonPartial } from "./component/partial/button/button.partial";
import { SelectPartial } from "./component/partial/select/select.partial";
import { SumFormPartial } from "./component/partial/sum.form/sum.form.partial";
import { TextPartial } from "./component/partial/text/text.partial";
import { TheadPartial } from "./component/partial/thead/thead.partial";
import { TablePartial } from "./component/partial/table/table.partial";
import { SlidePartial } from "./component/partial/slide/slide.partial";
import { SlidesPartial } from "./component/partial/slides/slides.partial";
import { TabPartial } from "./component/partial/tab/tab.partial";
import { SegmentButtonPartial } from "./component/partial/segment.button/segment.button.partial";
import { AttachmentsPartial } from "./component/partial/attachments/attachments.partial";

/* DYNAMIC */
import { DynamicViewer } from "./dynamic/viewer/dynamic.viewer";
import { DynamicChart } from "./dynamic/chart/dynamic.chart";
import { DynamicList } from "./dynamic/list/dynamic.list";
import { DynamicForm } from "./dynamic/form/dynamic.form";

/* CONTAINER CHART */
import { PieChartContainer } from "./dynamic/chart/container/pie/pie.chart.container";
import { BarChartContainer } from "./dynamic/chart/container/bar/bar.chart.container";
import { LineChartContainer } from "./dynamic/chart/container/line/line.chart.container";
import { CardChartContainer } from "./dynamic/chart/container/card/card.chart.container";

/* CONTAINER FORM */
import { FlatFormContainer } 	  from "./dynamic/form/container/flat/flat.form.container";
import { SegmentFormContainer }   from "./dynamic/form/container/segment/segment.form.container";
import { AccordionFormContainer } from "./dynamic/form/container/accordion/accordion.form.container";
import { WizardFormContainer }    from "./dynamic/form/container/wizard/wizard.form.container";
import { ExpandFormContainer } 	  from "./dynamic/form/container/expand/expand.form.container";
import { StepsFormContainer } 	  from "./dynamic/form/container/steps/steps.form.container";

/* CONTAINER VIEWER */
import { FlatViewerContainer } from "./dynamic/viewer/container/flat/flat.viewer.container";
import { SegmentViewerContainer } from "./dynamic/viewer/container/segment/segment.viewer.container";
import { AccordionViewerContainer } from "./dynamic/viewer/container/accordion/accordion.viewer.container";
import { SlideViewerContainer } from "./dynamic/viewer/container/slide/slide.viewer.container";
import { WizardViewerContainer } from "./dynamic/viewer/container/wizard/wizard.viewer.container";

/* CONTAINER LIST */
import { TableListContainer }	 from "./dynamic/list/container/table/table.list.container";
import { TileListContainer } 	 from "./dynamic/list/container/tile/tile.list.container";
import { CardListContainer }	 from "./dynamic/list/container/card/card.list.container";
import { CalendarListContainer } from "./dynamic/list/container/calendar/calendar.list.container";
import { SlideListContainer } 	 from "./dynamic/list/container/slide/slide.list.container";
import { TabListContainer } 	 from "./dynamic/list/container/tab/tab.list.container";
import { MapListContainer }		 from "./dynamic/list/container/map/map.list.container";

/* VALIDATE */
import { InputValidate } from "./validate/input.validate";

/* ROUTER */
import { PippaUrlSerializer } from "./router/pippa.url.serializer";

@Injectable({
    providedIn : "root"
})
export class MatPaginatorIntlCro extends MatPaginatorIntl 
{
    itemsPerPageLabel = "";
    nextPageLabel = "Próxima Página";
    previousPageLabel = "Página Anterior";
    firstPageLabel = "Primeira Página";
    lastPageLabel = "Última Página";

    getRangeLabel = function (page, pageSize, length) 
    {
        if (length === 0 || pageSize === 0) 
        { 
            return "0 de " + length;
        }

        length           = Math.max(length, 0);
        const startIndex = page * pageSize;
        // If the start index exceeds the list length, do not try and fix the end index to the end.
        const endIndex = startIndex < length ?
            Math.min(startIndex + pageSize, length) :
            startIndex + pageSize;
        return startIndex + 1 + " - " + endIndex + " de " + length;
    };
}

const maskConfig: Partial<IConfig> = {
    dropSpecialCharacters : false
};

@NgModule({
    schemas : [ CUSTOM_ELEMENTS_SCHEMA ],
    imports : [		
        RecaptchaV3Module,
        HammerModule,
        MbscModule,
        MatStepperModule,
        MatExpansionModule,
        MatToolbarModule,
        MatTabsModule,
        MatTooltipModule,
        MatCardModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatPaginatorModule,
        DragDropModule,
        HttpClientModule,
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        //CurrencyMaskModule,
        NgSelectModule,
        FileUploadModule,
        NgxExtendedPdfViewerModule,
        MonacoEditorModule.forRoot(),
        NgChartsModule,		
        AgmCoreModule.forRoot(),
        MixedCdkDragDropModule.forRoot({ autoScrollStep : 4 }),
        //DragulaModule,
        CKEditorModule,
        ColorPickerModule,
        ImageCropperModule,
        PlyrModule,
        ClickOutsideModule,
        ShareModule,
        A11yModule,
        NgxPrettyCheckboxModule,
        NgxMaskDirective, 
        NgxMaskPipe
    ],	
    declarations : [
        //BaseModel,
        //Field,
        /* PIPE */
        SafePipe,
        ReversePipe,
        BreakLinePipe,
        NotificationCountPipe,
        DateFormatPipe,
        CurrencyFormatPipe,
        IntFormatPipe,
        //BaseComponent,
        //BaseFilter,
        //BasePage,
        //BaseRoot,
        //BaseForm,
        //.BaseTabs,
        //BaseApp,
        //BaseModal,
        //BaseGrid,
        //BaseViewer,
        EmbedModal,
        IframeModal,
        AvatarModal,
        PDFModal,
        ComentImageModal,
        GalleryModal,
        ViewerModal,
        AgreeModal,
        /* POPOVER */
        MenuPopover,
        AttachmentPopover,
        ListPopover,
        FieldFormPopover,
        GridPopover,
        FormItemPopover,
        FormBlockPopover,
        FormFilterPopover,
        StepFormPopover,
        FormFormPopover,
        ExportPopover,
        EmailPopover,
        ColumnOrderPopover,
        /* DIRECTIVE */
        LastElementDirective,
        DirectionsMapDirective,
        BackgroundSliderDirective,
        AutosizeDirective,
        FilePipe,
        FilesPipe,
        ResumePipe,
        AclDirective,
        FocusDirective,
        AutoPlayDirective,
        ShadowCssDirective,
        AdjustKeyboardDirective,
        HiddenDirective,
        StickyDirective,
        SourceCodeDirective,
        CurrencyMaskDirective,
        TranslatePipe,
        CardMaskPipe,
        ArrayPipe, 
        AgePipe,
        PropertyPipe,
        AddressFormatPipe,
        TimeFormatPipe,
        UniquePipe,
        NbspRemovePipe,
        BaseListPipe,
        AvatarPipe,
        /* VIEWER */
        FileViewerBlock,
        AudioPlayerBlock,
        RadioPlayerBlock,
        UserProfileBlock,
        MenuNavBlock,
        MenuFooterBlock,
        MenuTileBlock,
        SlideBlock,
        BibleBlock,
        FilterHeaderBlock,
        FilterChartBlock,
        AnalyticsViewerBlock,
        /* PAGINATION */
        NextPagination,
        OnlyNextPagination,
        NumberPagination,
        /* FILTER */
        //SelectFilter,
        /* INPUT */
        //BaseInput,
        //BaseContainer,
        FieldsetInput,
        TextInput,
        IframeInput,
        NumberInput,
        AlnumInput,
        AvatarInput,
        EmailInput,
        DateInput,
        CPFInput,
        StarInput,
        ItemStarInput,
        ZipcodeBRInput,
        SelectInput,
        RadioInput,
        HiddenInput,
        IPInput,
        HiddenLabelInput,
        AddressInput,
        CreditCardInput,
        PasswordInput,
        ConfirmPasswordInput,
        PhoneInput,
        TextareaInput,
        CVVInput,
        MonthYearInput,
        YearInput,
        DayMonthInput,
        CardNumberInput,
        MultiUploadJsonInput,
        MultiSelectInput,
        ListMultiSelectInput,
        ListMultiRadioInput,
        ViewerInput,
        MatrixInput,
        MultiCheckboxInput,
        UploadJsonInput,
        VideoUploadInput,
        ListInput,
        BankInput,
        IntegerInput,
        CurrencyInput,
        PercentageInput,
        ColumnGridInput,
        ReferenceMultiSelectInput,
        ProductMultiSelectInput,
        ReferenceSelectInput,
        ReferenceRadioInput,
        ReferenceTextInput,
        ReferenceCouponInput,
        ReferenceAppsInput,
        ArrangeInput,
        CronJobInput,
        CheckboxInput,
        CheckboxObjectInput,
        PaymentStatusInput,
        DistanceInput,
        CommentInput,
        CorreiosInput,
        GeolocationInput,
        CheckboxTitleInput,
        NoteInput,
        SubFormInput,
        DataFormInput,
        AgreeInput,
        CPFCNPJInput,
        CNPJInput,
        GeneratePasswordInput,
        ScanButtonInput,
        Table3x3Input,
        TableInput,
        SelectIconInput,
        DiscInput,
        DNAInput,
        ScaleInput,
        ReferenceFieldsInput,
        FieldsInput,
        ReferenceOptionsInput,
        ReferenceMatchInput,
        ReferenceArrangeInput,
        QuestionInput,
        ReferenceFormInput,
        RichTextInput,
        SourceCodeInput,
        ToggleInput,
        UrlInput,
        EmbedInput,
        HeaderInput,
        ErrorInput,
        IconInput,
        AttachmentInput,
        /* OPTIONS */
        //FieldOptions,
        ItemsSetting,
        DiscSetting,
        DNASetting,
        DNA2Setting,
        AppSetting,
        Table3x3Setting,
        ObjectInput,
        GeolocationInput,
        JsonFirebaseInput,
        QuestionSetting,
        GridSetting,
        FieldTypeSetting,
        MultiSelectInput,
        ListMultiSelectInput,
        ListMultiRadioInput,
        ViewerInput,
        MatrixInput,
        LabelInput,
        BirthdayInput,
        StepItemInput,
        StepBlockInput,
        ReferenceStepInput,
        TimeInput,
        TimeSecondsInput,
        AutoFillInput,
        ReferenceAutoFillInput,
        ReferenceMultiCheckboxInput,
        ReferenceCheckboxInput,
        SignatureInput,
        ColorPickerInput,
        GeolocationInput,
        /* OUTPUT */
        MenuOutput,
        CommentOutput,
        DistanceOutput,
        CreditCardOutput,
        IntegerOutput,
        Table3x3Output,
        TextOutput,
        TableOutput,
        ObjectOutput,
        RichTextOutput,
        SourceCodeOutput,
        LockOutput,
        IframeOutput,
        PageUrlOutput,
        ImageOutput,
        AudioOutput,
        PDFOutput,
        UploadJsonOutput,
        DownloadOutput,
        OptionOutput,
        AddressOutput,
        DiscOutput,
        DiscOutput,
        SumFormOutput,
        SumDocumentsOutput,
        DNAOutput,
        DNA2Output,
        StepItemOutput,
        ReferenceListOutput,
        VideoUrlOutput,
        VideoPlaylistOutput,
        QuestionOutput,
        ReferenceFormOutput,
        SelectIconOutput,
        ReferenceSelectOutput,
        ViewerOutput,
        ReferenceTextInput,
        ReferenceCouponInput,
        ToggleOutput,
        EmbedOutput,
        TitleOutput,
        DateOutput,
        PhoneOutput,
        CurrencyOutput,
        FunctionOutput,
        ButtonOutput,
        ShareButtonsOutput,
        TextareaOutput,
        StarOutput,
        MatrixOutput,
        CronJobOutput,
        AgreeOutput,
        ListMultiSelectOutput,
        BirthdayOutput,
        HasDocumentOutput,
        LastDocumentOutput,
        TimeOutput,
        FieldsetOutput,
        SubFormOutput,
        StockProductsOutput,
        StockMovimentOutput,
        PriceProductsOutput,
        DataFormOutput,
        MultiOptionOutput,
        UrlOutput,
        BankOutput,
        VerifyPaymentOutput,
        ChargesPagarmeOutput,
        TrackingCorreiosOutput,
        TrackingReversalOutput,
        TimeSecondsOutput,
        TrPartial,
        CardPartial,
        TdPartial,
        ThPartial,
        TbodyPartial,
        DivPartial,
        RowPartial,
        ColPartial,
        AvatarOutput,
        PercentageOutput,
        QRCodeOutput,
        BarcodeOutput,
        PasswordOutput,
        YoutubeLiveOutput,
        //BasePartial,
        LabelPartial,
        ButtonPartial,
        SelectPartial,
        SumFormPartial,
        TextPartial,
        TheadPartial,
        TablePartial,
        SlidePartial,
        SlidesPartial,
        TabPartial,
        SegmentButtonPartial,
        AttachmentsPartial,
        /* DYNAMIC */
        DynamicViewer,
        DynamicChart,
        DynamicList,
        DynamicForm,
        //DynamicFilter,
        /* CONTAINER CHART */
        PieChartContainer,
        BarChartContainer,
        LineChartContainer,
        CardChartContainer,
        /* CONTAINER FORM */
        WizardFormContainer,
        ExpandFormContainer,
        StepsFormContainer,
        SegmentFormContainer,
        AccordionFormContainer,
        FlatFormContainer,
        AccordionFormContainer,
        /* CONTAINER VIEWER */
        FlatViewerContainer,
        SegmentViewerContainer,
        AccordionViewerContainer,
        SlideViewerContainer,
        WizardViewerContainer,
        /* CONTAINER LIST */
        TableListContainer,
        TileListContainer,
        CardListContainer,
        CalendarListContainer,
        SlideListContainer,
        TabListContainer,
        MapListContainer,
        /* PAGE */
    ],
    exports : [ 
        /* PIPE */
        SafePipe,
        ReversePipe,
        BreakLinePipe,
        NotificationCountPipe,
        DateFormatPipe,
        CurrencyFormatPipe,
        IntFormatPipe,
        TranslatePipe,
        CardMaskPipe,
        NgxExtendedPdfViewerModule,
        MatStepperModule,
        MatExpansionModule,
        DragDropModule,
        MixedCdkDragDropModule,
        /* DIRECTIVE */
        LastElementDirective,
        DirectionsMapDirective,
        BackgroundSliderDirective,
        AutosizeDirective,
        FilePipe,
        FilesPipe,
        ResumePipe,
        AclDirective,
        FocusDirective,
        AutoPlayDirective,
        ShadowCssDirective,
        AdjustKeyboardDirective,
        HiddenDirective,
        StickyDirective,
        SourceCodeDirective,
        CurrencyMaskDirective,
        AgePipe,
        PropertyPipe,
        AddressFormatPipe,
        TimeFormatPipe,
        UniquePipe,
        NbspRemovePipe,
        BaseListPipe,
        AvatarPipe,
        /* DYNAMIC */
        DynamicViewer,
        DynamicChart,
        DynamicList,
        DynamicForm,
        //DynamicFilter,
        /* VIEWER */
        FileViewerBlock,
        UserProfileBlock,
        MenuNavBlock,
        RadioPlayerBlock,
        MenuFooterBlock,
        MenuTileBlock,
        SlideBlock,
        BibleBlock,
        FilterHeaderBlock,
        FilterChartBlock,
        AnalyticsViewerBlock,
        ErrorInput,
        /* PAGINATION */
        NextPagination,
        OnlyNextPagination,
        NumberPagination,
        /* FILTER */
        //SelectFilter,
        /* MODULES */
        ReactiveFormsModule,
        FormsModule,
        CKEditorModule,
        HammerModule,
        NgSelectModule,
        IonicModule,
        CommonModule,
        NgChartsModule,
        AgmCoreModule,
        MatTabsModule,
        ImageCropperModule,
        MatTooltipModule,
        MatCardModule,
        MbscModule,
        //CurrencyMaskModule,
        PlyrModule,
        ShareModule,
        ClickOutsideModule,
        NgxPrettyCheckboxModule,
    ],
    // entryComponents: [
    // 	//BaseComponent,
    // 	//BaseApp, 
    // 	EmbedModal,
    // 	IframeModal,
    // 	AvatarModal,
    // 	PDFModal,
    // 	ComentImageModal,
    // 	GalleryModal,
    // 	ViewerModal,
    // 	AgreeModal,
    // 	JsonFirebaseInput,
    // 	/* POPOVER */
    // 	MenuPopover,
    // 	AttachmentPopover,
    // 	ListPopover,
    // 	FieldFormPopover,
    // 	GridPopover,
    // 	FormItemPopover,
    // 	FormBlockPopover,
    // 	FormFilterPopover,
    // 	StepFormPopover,
    // 	FormFormPopover,
    // 	ExportPopover,
    // 	EmailPopover,
    // 	ColumnOrderPopover,
    // 	/* VIEWER */
    // 	FileViewerBlock,
    // 	AudioPlayerBlock,
    // 	RadioPlayerBlock,
    // 	UserProfileBlock,
    // 	MenuNavBlock,
    // 	MenuFooterBlock,
    // 	MenuTileBlock,
    // 	SlideBlock,
    // 	BibleBlock,
    // 	FilterHeaderBlock,
    // 	/* PAGINATION */
    // 	NextPagination,
    // 	OnlyNextPagination,
    // 	/* FILTER */
    // 	//SelectFilter,
    // 	/* INPUT */
    // 	FieldsetInput,
    // 	PhoneInput,
    // 	DateInput,
    // 	TextInput,
    // 	IframeInput,
    // 	NumberInput,
    // 	AlnumInput,
    // 	AvatarInput,
    // 	HiddenInput,
    // 	IPInput,
    // 	HiddenLabelInput,
    // 	TextareaInput,
    // 	PasswordInput,
    // 	ConfirmPasswordInput,
    // 	EmailInput,
    // 	AddressInput,
    // 	CreditCardInput,
    // 	CPFInput,
    // 	DiscInput,
    // 	DNAInput,
    // 	StarInput,
    // 	ZipcodeBRInput,
    // 	SelectInput,
    // 	RadioInput,
    // 	CVVInput,
    // 	MonthYearInput,
    // 	YearInput,
    // 	DayMonthInput,
    // 	CardNumberInput,
    // 	MultiUploadJsonInput,
    // 	MultiSelectInput,
    // 	ListMultiSelectInput,
    // 	ListMultiRadioInput,
    // 	ViewerInput,
    // 	MatrixInput,
    // 	MultiCheckboxInput,
    // 	UploadJsonInput,
    // 	VideoUploadInput,
    // 	ListInput,
    // 	FlatFormContainer,
    // 	AccordionFormContainer,
    // 	BankInput,
    // 	IntegerInput,
    // 	CurrencyInput,
    // 	PercentageInput,
    // 	ColumnGridInput,
    // 	ReferenceMultiSelectInput,
    // 	ProductMultiSelectInput,
    // 	ReferenceSelectInput,
    // 	ReferenceRadioInput,
    // 	ReferenceTextInput,
    // 	ReferenceCouponInput,
    // 	ReferenceAppsInput,
    // 	ArrangeInput,
    // 	CronJobInput,
    // 	CheckboxInput,
    // 	CheckboxObjectInput,
    // 	PaymentStatusInput,
    // 	DistanceInput,
    // 	CommentInput,
    // 	CorreiosInput,
    // 	CheckboxTitleInput,
    // 	NoteInput,
    // 	SubFormInput,
    // 	DataFormInput,
    // 	AgreeInput,
    // 	CPFCNPJInput,
    // 	CNPJInput,
    // 	GeneratePasswordInput,
    // 	ScanButtonInput,
    // 	Table3x3Input,
    // 	TableInput,
    // 	SelectIconInput,
    // 	ScaleInput,
    // 	ReferenceFieldsInput,
    // 	FieldsInput,
    // 	ReferenceOptionsInput,
    // 	ReferenceMatchInput,
    // 	ReferenceArrangeInput,
    // 	QuestionInput,
    // 	ReferenceFormInput,
    // 	RichTextInput,
    // 	SourceCodeInput,
    // 	ToggleInput,
    // 	UrlInput,
    // 	EmbedInput,
    // 	HeaderInput,
    // 	ErrorInput,
    // 	IconInput,
    // 	AttachmentInput,
    // 	/* OPTIONS */
    // 	//FieldOptions,
    // 	ItemsSetting,
    // 	DiscSetting,
    // 	DNASetting,
    // 	DNA2Setting,
    // 	AppSetting,
    // 	Table3x3Setting,
    // 	ObjectInput,
    // 	QuestionSetting,
    // 	GridSetting,
    // 	FieldTypeSetting,
    // 	MultiSelectInput,
    // 	ListMultiSelectInput,
    // 	ListMultiRadioInput,
    // 	ViewerInput,
    // 	MatrixInput,
    // 	LabelInput,
    // 	BirthdayInput,
    // 	StepItemInput,
    // 	StepBlockInput,
    // 	ReferenceStepInput,
    // 	TimeInput,
    // 	TimeSecondsInput,
    // 	AutoFillInput,
    // 	ReferenceAutoFillInput,
    // 	ReferenceMultiCheckboxInput,
    // 	ReferenceCheckboxInput,
    // 	SignatureInput,
    // 	ColorPickerInput,
    // 	GeolocationInput,
    // 	/* OUTPUT */
    // 	MenuOutput,
    // 	CommentOutput,
    // 	DistanceOutput,
    // 	CreditCardOutput,
    // 	IntegerOutput,
    // 	Table3x3Output,
    // 	TextOutput,
    // 	TableOutput,
    // 	ObjectOutput,
    // 	RichTextOutput,
    // 	SourceCodeOutput,
    // 	LockOutput,
    // 	IframeOutput,
    // 	PageUrlOutput,
    // 	ButtonOutput,
    // 	ShareButtonsOutput,
    // 	TextareaOutput,
    // 	StarOutput,
    // 	MatrixOutput,
    // 	CronJobOutput,
    // 	AgreeOutput,
    // 	ListMultiSelectOutput,
    // 	ImageOutput,
    // 	AudioOutput,
    // 	PDFOutput,
    // 	UploadJsonOutput,
    // 	DownloadOutput,
    // 	OptionOutput,
    // 	AddressOutput,
    // 	DiscOutput,
    // 	DiscOutput,
    // 	SumFormOutput,
    // 	SumDocumentsOutput,
    // 	DNAOutput,
    // 	DNA2Output,
    // 	StepItemOutput,
    // 	ReferenceListOutput,
    // 	VideoUrlOutput,
    // 	VideoPlaylistOutput,
    // 	QuestionOutput,
    // 	ReferenceFormOutput,
    // 	SelectIconOutput,
    // 	ReferenceSelectOutput,
    // 	ViewerOutput,
    // 	ToggleOutput,
    // 	EmbedOutput,
    // 	TitleOutput,
    // 	DateOutput,
    // PhoneOutput,
    // 	CurrencyOutput,
    // 	FunctionOutput,
    // 	BirthdayOutput,
    // 	HasDocumentOutput,
    // 	LastDocumentOutput,
    // 	TimeOutput,
    // 	FieldsetOutput,
    // 	SubFormOutput,
    // 	StockProductsOutput,
    // 	StockMovimentOutput,
    // 	PriceProductsOutput,
    // 	DataFormOutput,
    // 	MultiOptionOutput,
    // 	UrlOutput,
    // 	BankOutput,
    // 	VerifyPaymentOutput,
    // 	ChargesPagarmeOutput,
    // 	TrackingCorreiosOutput,
    // 	TimeSecondsOutput,
    // 	TrPartial,
    // 	CardPartial,
    // 	TdPartial,
    // 	ThPartial,
    // 	TbodyPartial,
    // 	DivPartial,
    // 	RowPartial,
    // 	ColPartial,
    // 	AvatarOutput,
    // 	PercentageOutput,
    // 	QRCodeOutput,
    // 	BarcodeOutput,
    // 	PasswordOutput,
    // 	YoutubeLiveOutput,
    // 	//BasePartial,
    // 	LabelPartial,
    // 	ButtonPartial,
    // 	SelectPartial,
    // 	SumFormPartial,
    // 	TextPartial,
    // 	TheadPartial,
    // 	TablePartial,
    // 	SlidePartial,
    // 	SlidesPartial,
    // 	TabPartial,
    // 	SegmentButtonPartial,
    // 	AttachmentsPartial,
    // 	/* DYNAMIC */
    // 	DynamicViewer,
    // 	DynamicChart,
    // 	DynamicList,
    // 	DynamicForm,
    // 	//DynamicFilter,
    // 	/* CONTAINER CHART */
    // 	PieChartContainer,
    // 	BarChartContainer,
    // 	/* CONTAINER FORM */
    // 	WizardFormContainer,
    // 	ExpandFormContainer,
    // 	StepsFormContainer,
    // 	SegmentFormContainer,
    // 	AccordionFormContainer,
    // 	FlatFormContainer,
    // 	AccordionFormContainer,
    // 	/* CONTAINER VIEWER */
    // 	FlatViewerContainer,
    // 	SegmentViewerContainer,
    // 	AccordionViewerContainer,
    // 	SlideViewerContainer,
    // 	WizardViewerContainer,
    // 	/* CONTAINER LIST */
    // 	TableListContainer,
    // 	TileListContainer,
    // 	CardListContainer,
    // 	CalendarListContainer,
    // 	SlideListContainer,
    // 	TabListContainer,
    // 	MapListContainer,
    // ],
    providers : [
        NavController,
        ModalController,
        provideEnvironmentNgxMask(maskConfig),
        { provide : RECAPTCHA_V3_SITE_KEY, useValue : environment.recaptchaKey },
        /* FACTORY */
        ComponentFactory,
        //IonContent,
        //ParamsFactory,
        //ResultFactory,
        //InputFactory,
        //OutputFactory,
        //PartialFactory,
        //TransformFactory,
        /* PLUGIN */
        VideoPlugin,
        AclPlugin,
        EventPlugin,
        //CurrencyPipe,
        DocumentPlugin,		
        FormPlugin,
        AppPlugin,
        GridPlugin,
        ViewerPlugin,
        AccountPlugin,
        EmailTemplatePlugin,
        NotificationTemplatePlugin,
        EmailPlugin,
        GatewayPlugin,
        FilterPlugin,
        FieldPlugin,
        UploadPlugin,
        ExportPlugin,		
        UserPlugin,
        AuthPlugin,
        MessagePlugin,
        ChatPlugin,
        LessonPlugin,
        CheckoutPlugin,
        GooglePlugin,
        CartPlugin,
        RequestPlugin,
        PwaUpdateService,
        BarcodeScanner,
        SocialSharing,
        /* VALIDATE */
        InputValidate,
        /* ROUTER */
        PippaUrlSerializer,
        [ { provide : MatPaginatorIntl, useClass : MatPaginatorIntlCro } ],
        /*{
            provide: LAZY_MAPS_API_CONFIG,
            useClass: MapsConfig,
        }*/
    ]
})
export class CoreModule 
{
    static initialize() 
    {
        /* PLUGIN */
        PluginType.get().register({ label : "App",      value : "APP_PLUGIN",      plugin : AppPlugin });
        PluginType.get().register({ label : "Usuário",  value : "USER_PLUGIN",     plugin : UserPlugin });
        PluginType.get().register({ label : "Document", value : "DOCUMENT_PLUGIN", plugin : DocumentPlugin });
        PluginType.get().register({ label : "Chat",     value : "CHAT_PLUGIN",     plugin : ChatPlugin });
        PluginType.get().register({ label : "Message",  value : "MESSAGE_PLUGIN",  plugin : MessagePlugin });
        PluginType.get().register({ label : "Lesson",   value : "LESSON_PLUGIN",   plugin : LessonPlugin });
        PluginType.get().register({ label : "Menu",     value : "MENU_PLUGIN",     plugin : MenuPlugin });
        PluginType.get().register({ label : "Option",   value : "OPTION_PLUGIN",   plugin : OptionPlugin });
        PluginType.get().register({ label : "Account",  value : "ACCOUNT_PLUGIN",  plugin : AccountPlugin });
        PluginType.get().register({ label : "Account",  value : "REQUEST_PLUGIN",  plugin : RequestPlugin });

        /* TYPE */
        FieldType.get().register({ label : "Alnum", value : "Alnum", input : AlnumInput });
        FieldType.get().register({ label : "Text", value : "Text", input : TextInput, output : TextOutput, transform : TextTransform });
        FieldType.get().register({ label : "Iframe", value : "Iframe", input : IframeInput, output : IframeOutput, transform : UrlTransform });
        FieldType.get().register({ label : "PageUrl", value : "PageUrl", input : UrlInput, output : PageUrlOutput, transform : PageUrlTransform });
        FieldType.get().register({ label : "Lock", value : "lock", output : LockOutput });
        FieldType.get().register({ label : "Fieldset", value : "Fieldset", input : FieldsetInput, output : FieldsetOutput, transform : FieldsetTransform });
        FieldType.get().register({ label : "Email", value : "Email", input : EmailInput });
        FieldType.get().register({ label : "Phone", value : "Phone", input : PhoneInput, output : PhoneOutput });
        FieldType.get().register({ label : "Date", value : "Date", input : DateInput, output : DateOutput, transform : DateTransform });
        FieldType.get().register({ label : "CNPJ", value : "CNPJ", input : CNPJInput, hasSingleFilter : true });
        FieldType.get().register({ label : "CPF", value : "CPF", input : CPFInput, hasSingleFilter : true });
        FieldType.get().register({ label : "Zipcode", value : "Zipcode", input : ZipcodeBRInput });
        FieldType.get().register({ label : "Select", value : "Select", input : SelectInput, output : OptionOutput, transform : OptionTransform, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "Radio", value : "Radio", input : RadioInput, output : OptionOutput, transform : OptionTransform, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "Hidden", value : "Hidden", input : HiddenInput, transform : HiddenTransform, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "HiddenLabel", value : "HiddenLabel", input : HiddenLabelInput });
        FieldType.get().register({ label : "IP", value : "IP", input : IPInput });
        FieldType.get().register({ label : "Address", value : "Address", input : AddressInput, output : AddressOutput, transform : AddressTransform });
        FieldType.get().register({ label : "CreditCard", value : "CreditCard", input : CreditCardInput, transform : CreditCardTransform, output : CreditCardOutput });
        FieldType.get().register({ label : "CardNumber", value : "CardNumber", input : CardNumberInput });
        FieldType.get().register({ label : "Password", value : "Password", input : PasswordInput, output : PasswordOutput });
        FieldType.get().register({ label : "ConfirmPassword", value : "ConfirmPassword", input : ConfirmPasswordInput, output : PasswordOutput });
        FieldType.get().register({ label : "Button", value : "Button", input : ToggleInput, output : ButtonOutput, optionType : "SelectIcon" });
        FieldType.get().register({ label : "ShareButtons", value : "ShareButtons", output : ShareButtonsOutput });
        FieldType.get().register({ label : "Textarea", value : "Textarea", input : TextareaInput, output : TextareaOutput });
        FieldType.get().register({ label : "Star", value : "Star", input : StarInput, output : StarOutput });
        FieldType.get().register({ label : "Integer", value : "Integer", input : IntegerInput, output : IntegerOutput, transform : NumberTransform });
        FieldType.get().register({ label : "Percentage", value : "Percentage", input : PercentageInput, output : PercentageOutput, transform : NumberTransform });
        FieldType.get().register({ label : "CVV", value : "CVV", input : CVVInput });
        FieldType.get().register({ label : "MonthYear", value : "MonthYear", input : MonthYearInput });
        FieldType.get().register({ label : "Year", value : "Year", input : YearInput });
        FieldType.get().register({ label : "DayMonth", value : "DayMonth", input : DayMonthInput });
        FieldType.get().register({ label : "Centavos", value : "Centavos", input : NumberInput, transform : CentavosTransform, output : CurrencyOutput });
        FieldType.get().register({ label : "MultiSelect", value : "MultiSelect", input : MultiSelectInput, output : MultiOptionOutput, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "ListMultiSelectInput", value : "ListMultiSelectInput", input : ListMultiSelectInput, output : ListMultiSelectOutput, optionsType : "ReferenceOptions", setting : { multiple : true } });
        FieldType.get().register({ label : "ListMultiRadioInput", value : "ListMultiRadioInput", input : ListMultiRadioInput, output : MultiOptionOutput, optionsType : "ReferenceOptions", setting : { multiple : true } });		
        FieldType.get().register({ label : "ViewerInput", value : "ViewerInput", input : ViewerInput, output : ViewerOutput });				
        FieldType.get().register({ label : "Matrix", value : "Matrix", input : MatrixInput, output : MatrixOutput, optionsType : "ReferenceOptions", setting : { multiple : true } });		
        FieldType.get().register({ label : "Download", value : "Download", input : UploadJsonInput, output : DownloadOutput, transform : UploadJsonTransform, optionType : "SelectIcon" });
        FieldType.get().register({ label : "UploadJson", value : "UploadJson", input : UploadJsonInput, output : UploadJsonOutput, transform : UploadJsonTransform });
        FieldType.get().register({ label : "MultiUploadJson", value : "MultiUploadJson", input : MultiUploadJsonInput });
        FieldType.get().register({ label : "List", value : "List", input : ListInput });
        FieldType.get().register({ label : "Bank", value : "Bank", input : BankInput, output : BankOutput, transform : BankTransform });
        FieldType.get().register({ label : "Currency", value : "Currency", input : CurrencyInput, output : CurrencyOutput, transform : CurrencyTransform });
        FieldType.get().register({ label : "MultiCheckbox", value : "MultiCheckbox", input : MultiCheckboxInput, output : MultiOptionOutput, transform : MultiCheckboxTransform, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "ColumnGrid", value : "ColumnGrid", input : ColumnGridInput });
        FieldType.get().register({ label : "Referência Apps", value : "ReferenceApps", input : ReferenceAppsInput });
        FieldType.get().register({ label : "Arrange", value : "Arrange", input : ArrangeInput });
        FieldType.get().register({ label : "SelectIcon", value : "SelectIcon", input : SelectIconInput, output : SelectIconOutput, transform : SelectIconTransform });
        FieldType.get().register({ label : "CronJob", value : "CronJob", input : CronJobInput, output : CronJobOutput });
        FieldType.get().register({ label : "Checkbox", value : "Checkbox", input : CheckboxInput, transform : CheckboxTransform });
        FieldType.get().register({ label : "CheckboxObject", value : "CheckboxObject", input : CheckboxObjectInput });
        FieldType.get().register({ label : "PaymentStatus", value : "PaymentStatus", input : PaymentStatusInput, transform : PaymentStatusTransform, output : VerifyPaymentOutput });
        FieldType.get().register({ label : "ChargesPagarme", value : "ChargesPagarme", output : ChargesPagarmeOutput });
        FieldType.get().register({ label : "TrackingCorreios", value : "TrackingCorreios", transform : SubFormTransform, output : TrackingCorreiosOutput });	
        FieldType.get().register({ label : "TrackingReversal", value : "TrackingReversal", transform : SubFormTransform, output : TrackingReversalOutput });
        FieldType.get().register({ label : "Distância", value : "distance", output : DistanceOutput, input : DistanceInput });
        FieldType.get().register({ label : "Geolocation", value : "Geolocation", input : GeolocationInput });
        FieldType.get().register({ label : "CPFCNPJ", value : "CPFCNPJ", input : CPFCNPJInput });
        FieldType.get().register({ label : "GeneratePassword", value : "GeneratePassword", input : GeneratePasswordInput });
        FieldType.get().register({ label : "ScanButton", value : "ScanButton", input : ScanButtonInput });
        FieldType.get().register({ label : "Table3x3", value : "Table3x3", input : Table3x3Input, output : Table3x3Output, transform : Table3x3Transform, optionType : "Table3x3Setting" });
        FieldType.get().register({ label : "Table", value : "Table", input : TableInput, output : TableOutput });
        FieldType.get().register({ label : "Disc", value : "Disc", input : DiscInput, output : DiscOutput, transform : DiscTransform, optionType : "DiscSetting" });
        FieldType.get().register({ label : "Signature", value : "Signature", input : SignatureInput, output : ImageOutput, transform : UploadJsonTransform });
        FieldType.get().register({ label : "Scale", value : "Scale", input : ScaleInput, output : OptionOutput, transform : OptionTransform, optionType : "ReferenceOptions" });
        FieldType.get().register({ label : "ReferenceSelect", value : "ReferenceSelect", input : ReferenceSelectInput, output : ReferenceSelectOutput, transform : ReferenceTransform, optionType : "AppSetting", hasFilter : true, hasSingleFilter : true });
        FieldType.get().register({ label : "Viewer", value : "Viewer", input : ReferenceSelectInput, output : ViewerOutput, transform : ReferenceTransform, optionType : "AppSetting", hasFilter : true, hasSingleFilter : true });
        FieldType.get().register({ label : "ReferenceRadio", value : "ReferenceRadio", input : ReferenceRadioInput, output : ReferenceSelectOutput, transform : ReferenceTransform, optionType : "AppSetting", hasFilter : true, hasSingleFilter : true });		
        FieldType.get().register({ label : "ReferenceText", value : "ReferenceText", input : ReferenceTextInput, output : ReferenceSelectOutput, transform : ReferenceTransform, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceCoupon", value : "ReferenceCoupon", input : ReferenceCouponInput, output : ReferenceSelectOutput, transform : ReferenceCouponTransform, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceMultiSelect", value : "ReferenceMultiSelect", input : ReferenceMultiSelectInput, output : ReferenceListOutput, transform : ReferenceListTransform, optionType : "AppSetting", hasGrid : true, hasFilter : true });
        FieldType.get().register({ label : "ProductMultiSelect", value : "ProductMultiSelect", input : ProductMultiSelectInput, output : ReferenceListOutput, transform : ReferenceListTransform, optionType : "AppSetting", hasGrid : true, hasFilter : true });
        FieldType.get().register({ label : "Menu", value : "Menu", input : ReferenceMultiSelectInput, transform : ReferenceListTransform, output : MenuOutput });
        FieldType.get().register({ label : "Comentário", value : "Comment", transform : CommentTransform, output : CommentOutput, input : CommentInput });
        FieldType.get().register({ label : "Correios", value : "Correios", input : CorreiosInput, output : OptionOutput, transform : OptionTransform });
        FieldType.get().register({ label : "ReferenceFields", value : "ReferenceFields", input : ReferenceFieldsInput, optionType : "FieldTypeSetting" });
        FieldType.get().register({ label : "Fields", value : "Fields", input : FieldsInput, optionType : "FieldTypeSetting" });
        FieldType.get().register({ label : "ReferenceMatch", value : "ReferenceMatch", input : ReferenceMatchInput, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceArrange", value : "ReferenceArrange", input : ReferenceArrangeInput, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceAutoFill", value : "ReferenceAutoFill", input : ReferenceAutoFillInput, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceMultiCheckbox", value : "ReferenceMultiCheckbox", input : ReferenceMultiCheckboxInput, output : ReferenceListOutput, transform : ReferenceListTransform, optionType : "AppSetting", hasFilter : true });
        FieldType.get().register({ label : "ReferenceCheckbox", value : "ReferenceCheckbox", input : ReferenceCheckboxInput, output : ReferenceSelectOutput, transform : ReferenceTransform, optionType : "AppSetting" });
        FieldType.get().register({ label : "ReferenceForm", value : "ReferenceForm", input : ReferenceFormInput, output : ReferenceFormOutput, transform : ReferenceFormTransform, optionType : "AppSetting", hasFilter : true });
        FieldType.get().register({ label : "VideoUrl", value : "VideoUrl", input : UrlInput, output : VideoUrlOutput, transform : VideoUrlTransform });
        FieldType.get().register({ label : "VideoPlaylist", value : "VideoPlaylist", input : UrlInput, output : VideoPlaylistOutput, transform : VideoPlaylistTransform });
        FieldType.get().register({ label : "Question", value : "Question", input : QuestionInput, output : QuestionOutput, transform : QuestionTransform, optionType : "QuestionSetting" });
        FieldType.get().register({ label : "Label", value : "Label", input : LabelInput, transform : LabelTransform });
        FieldType.get().register({ label : "RichText", value : "RichText", input : RichTextInput, output : RichTextOutput });
        FieldType.get().register({ label : "SumForm", value : "SumForm", input : TextInput, output : SumFormOutput, transform : SumFormTransform });
        FieldType.get().register({ label : "SumFieldset", value : "SumFieldset", input : TextInput, transform : SumFieldsetTransform });
        FieldType.get().register({ label : "Image", value : "Image", input : UploadJsonInput, output : ImageOutput, transform : UploadJsonTransform });
        FieldType.get().register({ label : "Audio", value : "Audio", input : UploadJsonInput, output : AudioOutput, transform : UploadJsonTransform });
        FieldType.get().register({ label : "Video Upload", value : "Video", input : VideoUploadInput, output : VideoUrlOutput, transform : VideoUrlTransform });
        FieldType.get().register({ label : "PDF", value : "PDF", input : UploadJsonInput, output : PDFOutput, transform : UploadJsonTransform });
        FieldType.get().register({ label : "DNA", value : "DNA", input : DNAInput, output : DNAOutput, transform : DNATransform, optionType : "DNASetting" });
        FieldType.get().register({ label : "DNA2", value : "DNA2", input : DNAInput, output : DNA2Output, transform : DNA2Transform, optionType : "DNA2Setting" });
        FieldType.get().register({ label : "StepItem", value : "Step", input : StepItemInput, transform : StepItemTransform, output : StepItemOutput });
        FieldType.get().register({ label : "StepBlock", value : "StepBlock", input : StepBlockInput });
        FieldType.get().register({ label : "Toggle", value : "Toggle", input : ToggleInput, output : ToggleOutput });
        FieldType.get().register({ label : "Embed", value : "Embed", input : EmbedInput, output : EmbedOutput });
        FieldType.get().register({ label : "CheckboxTitle", value : "CheckboxTitle", input : CheckboxTitleInput });
        FieldType.get().register({ label : "Agree", value : "Agree", input : AgreeInput, output : AgreeOutput, transform : AgreeTransform });
        FieldType.get().register({ label : "Title", value : "Title", output : TitleOutput });
        FieldType.get().register({ label : "VerifyPayment", value : "VerifyPayment", input : TextInput, output : VerifyPaymentOutput, transform : VerifyPaymentTransform });
        FieldType.get().register({ label : "SumDocuments", value : "SumDocuments", input : TextInput, output : SumDocumentsOutput, transform : SumDocumentsTransform });
        FieldType.get().register({ label : "CountDocuments", value : "CountDocuments", input : TextInput, output : TextOutput, transform : CountDocumentsTransform, optionType : "AppSetting", hasFilter : true });
        FieldType.get().register({ label : "SubForm", value : "SubForm", input : SubFormInput, output : SubFormOutput, transform : SubFormTransform, hasGrid : true });
        FieldType.get().register({ label : "StockProducts", value : "StockProducts", output : StockProductsOutput, transform : StockProductsTransform });
        FieldType.get().register({ label : "StockMoviment", value : "StockMoviment", output : StockMovimentOutput, transform : StockMovimentTransform });		
        FieldType.get().register({ label : "PriceProducts", value : "PriceProducts", output : PriceProductsOutput, transform : PriceProductsTransform });
        FieldType.get().register({ label : "DataForm", value : "DataForm", input : DataFormInput, output : DataFormOutput, transform : DataFormTransform });
        FieldType.get().register({ label : "Url", value : "Url", input : UrlInput, output : UrlOutput, transform : UrlTransform, optionType : "SelectIcon" });
        FieldType.get().register({ label : "Birthday", value : "Birthday", input : BirthdayInput, output : BirthdayOutput, transform : DateTransform });
        FieldType.get().register({ label : "Note", value : "Note", input : NoteInput });
        FieldType.get().register({ label : "HasDocument", value : "HasDocument", input : TextInput, output : HasDocumentOutput, transform : HasDocumentTransform });
        FieldType.get().register({ label : "LastDocument", value : "LastDocument", input : TextInput, output : LastDocumentOutput, transform : LastDocumentTransform, hasFilter : true });
        FieldType.get().register({ label : "Function", value : "Function", input : TextInput, output : FunctionOutput, transform : FunctionTransform }); /* USADO NA COLUNA */
        FieldType.get().register({ label : "ReferenceStep", value : "ReferenceStep", input : ReferenceStepInput });
        FieldType.get().register({ label : "Time", value : "Time", input : TimeInput, output : TimeOutput });
        FieldType.get().register({ label : "Avatar", value : "Avatar", input : AvatarInput, output : AvatarOutput });
        FieldType.get().register({ label : "TimeSeconds", value : "TimeSeconds", input : TimeSecondsInput, output : TimeSecondsOutput, transform : TimeSecondsTransform });
        FieldType.get().register({ label : "AutoFill", value : "AutoFill", input : AutoFillInput });
        FieldType.get().register({ label : "QRCode", value : "QRCode", input : TextInput, output : QRCodeOutput, transform : QRCodeTransform });
        FieldType.get().register({ label : "Barcode", value : "Barcode", input : TextInput, output : BarcodeOutput, transform : TextTransform });
        FieldType.get().register({ label : "ColorPicker", value : "ColorPicker", input : ColorPickerInput });
        FieldType.get().register({ label : "Number", value : "Number", input : NumberInput, transform : NumberTransform });
        FieldType.get().register({ label : "ObjectInput", value : "ObjectInput", input : ObjectInput, output : ObjectOutput });
        FieldType.get().register({ label : "thumbnailUrl", value : "thumbnailUrl", output : ImageOutput });
        FieldType.get().register({ label : "JsonFirebaseInput", value : "JsonFirebaseInput", input : JsonFirebaseInput });
        FieldType.get().register({ label : "YoutubeLive", value : "YoutubeLive", input : TextInput, output : YoutubeLiveOutput });
        FieldType.get().register({ label : "SourceCode", value : "SourceCode", input : SourceCodeInput, output : SourceCodeOutput });

        /* SETTING */
        FieldType.get().register({ label : "ReferenceOptions", value : "ReferenceOptions", input : ReferenceOptionsInput });
        FieldType.get().register({ label : "FieldTypeSetting", value : "FieldTypeSetting", input : FieldTypeSetting });
        FieldType.get().register({ label : "DiscSetting", value : "DiscSetting", input : DiscSetting });
        FieldType.get().register({ label : "QuestionSetting", value : "QuestionSetting", input : QuestionSetting });
        FieldType.get().register({ label : "Table3x3Setting", value : "Table3x3Setting", input : Table3x3Setting });
        FieldType.get().register({ label : "DNASetting", value : "DNASetting", input : DNASetting });
        FieldType.get().register({ label : "DNA2Setting", value : "DNA2Setting", input : DNA2Setting });
        FieldType.get().register({ label : "ItemsSetting", value : "ItemsSetting", input : ItemsSetting, output : MultiOptionOutput }); /* SAVA COMO ARRAY */
        FieldType.get().register({ label : "AppSetting", value : "AppSetting", input : AppSetting });
        FieldType.get().register({ label : "GridSetting", value : "GridSetting", input : GridSetting });
        //FIELD_SETTING            : { label : 'FieldSetting',           value : 'FieldOSetting' });

        /* PARTIAL */
        PartialType.get().register({ label : "Row",	 		 value : "Row", partial : RowPartial });
        PartialType.get().register({ label : "Col", 	 		 value : "Col", partial : ColPartial });
        PartialType.get().register({ label : "Button",		 value : "Button", partial : ButtonPartial });
        PartialType.get().register({ label : "Select",		 value : "Select", partial : SelectPartial });
        PartialType.get().register({ label : "Label", 		 value : "Label", partial : LabelPartial });
        PartialType.get().register({ label : "Tr",	 		 value : "Tr", partial : TrPartial });
        PartialType.get().register({ label : "Card",	 		 value : "Card", partial : CardPartial });
        PartialType.get().register({ label : "Td",	 		 value : "Td", partial : TdPartial });
        PartialType.get().register({ label : "Th",	 		 value : "Th", partial : ThPartial });
        PartialType.get().register({ label : "Thead", 		 value : "Thead", partial : TheadPartial });
        PartialType.get().register({ label : "Table", 		 value : "Table", partial : TablePartial });
        PartialType.get().register({ label : "Slide", 		 value : "Slide", partial : SlidePartial });
        PartialType.get().register({ label : "Slides",		 value : "Slides", partial : SlidesPartial });
        PartialType.get().register({ label : "Tab",	 		 value : "Tab", partial : TabPartial });
        PartialType.get().register({ label : "SegmentButton", value : "SegmentButton", partial : SegmentButtonPartial });
        PartialType.get().register({ label : "Attachments",   value : "Attachments", partial : AttachmentsPartial });
        PartialType.get().register({ label : "Tbody", 		 value : "Tbody", partial : TbodyPartial });
        PartialType.get().register({ label : "Text",		     value : "Text", partial : TextPartial });
        PartialType.get().register({ label : "Div",			 value : "Div", partial : DivPartial });

        /* BLOCK */
        BlockType.get().register({ label : "MenuTile", value : "MenuTile", block : MenuTileBlock });

        /* FORM */
        FormType.get().register({ label : "Flat", 	  value : "flat",      container : FlatFormContainer });
        FormType.get().register({ label : "Segment",   value : "segment",   container : SegmentFormContainer });
        FormType.get().register({ label : "Wizard",    value : "wizard",    container : WizardFormContainer });
        FormType.get().register({ label : "Expand",    value : "expand",    container : ExpandFormContainer });		
        FormType.get().register({ label : "Accordion", value : "accordion", container : AccordionFormContainer });
        FormType.get().register({ label : "Steps", 	  value : "steps", 	  container : StepsFormContainer });		

        return {
            ngModule : CoreModule,
        }
    }
}