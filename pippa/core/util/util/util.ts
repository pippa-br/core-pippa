import { Browser } from '@capacitor/browser';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DocumentReference, Timestamp } from 'firebase/firestore'; 

/* PIPPA */
import { Core }       		from '../core/core';
import { FieldType }  		from '../../type/field/field.type';
import { TransformFactory } from '../../factory/transform/transform.factory';
import { FunctionsType } 	from '../../type/functions/functions.type';
import { AlertController } from '@ionic/angular';
import { TDate } from '../tdate/tdate';
export class Util
{
	async copyForm(model:any)
	{
		if(model)
		{
            const data = this.core().util.parseApiData(await model.getData());

			const inputs = [
				{
					name		: 'code',
					placeholder : 'Código',
					type		: 'textarea',
					value       : JSON.stringify(data),
				}
			];
	
			this.showAlert('Copiar Dados', '', null, inputs);
		}
		else
		{
			this.showAlert('Copiar Dados', 'dados não pode ser copiada');
		}
	}

	pastForm(model:any)
	{
		return new Promise(resolve => 
		{
			if(model)
			{
				const inputs = [
					{
						name		: 'code',
						placeholder : 'Código',
						type		: 'textarea',
					}
				];
	
				const buttons = [{
					text: 'Colar',
					handler: (data) => 
					{		
						const dataJson = JSON.parse(data.code);
						
						const fieldsRemove = ["reference","appid","accid","colid","form","owner","user","postdate","lastdate","order","id","attachments","indexes","orderFormItem","projectID","_logs"];
	
						for(const item of fieldsRemove)
						{
							delete dataJson[item];
						}
	
						model.populate(dataJson);
	
						resolve(model);
					}
				}];
		
				this.showAlert('Colar Dados', '', buttons, inputs);
			}
			else
			{
				this.showAlert('Copiar Dados', 'dados não pode ser copiada');
			}
		});		
	}

	parseApiData(data:any)
	{	
		for(const key in data)
		{
			if(data[key])
			{
				if(data[key] instanceof DocumentReference)
				{
					data[key] = {
						referencePath : data[key].path
					}
				}
				else if (data[key] instanceof Timestamp)
				{
						data[key] = data[key].toDate();
				}
				else if(data[key].reference)
				{
					data[key] = {
						referencePath : data[key].reference.path
					}
				}
				else if(typeof data[key] == 'object')
				{
					data[key] = this.parseApiData(data[key]);
				}
			}			
		}

		return data;
	}

	openWaze(label:string, geocoords:string)
	{
		var label = encodeURI(label); // encode the label!
		window.open('https://waze.com/ul?q=' + label + '&ll=' + geocoords +'&navigate=yes', '_system');
	}

	openGoogleMaps(label:string, geocoords:string)
	{
		if(this.core().isIOS)
		{
			window.open('maps://?q=' + geocoords, '_system');
		}
		else 
		{
			var label = encodeURI(label); // encode the label!
			window.open('geo:0,0?q=' + geocoords + '(' + label + ')', '_system');
		}
	}

	isJson(value:string) 
	{
		try 
		{
			JSON.parse(value);
		} 
		catch (e) 
		{
			return false;
		}

		return true;
	}	

	productName(...args:any) 
	{
		let name = args[0][0].name;
		let code = args[0][0].code;

		if(args[0][0].codeTable)
		{
			for(let key in args[0][0].codeTable.data)
			{
				code = args[0][0].codeTable.data[key].code + '...';
				break;
			}			
		}

		return code + ' - ' + name;
	};

	s2ab(s) 
	{
		var buf = new ArrayBuffer(s.length);
		var view = new Uint8Array(buf);
		for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
		return buf;
	}

	binaryToBase64(data:any, type:string) 
	{
		let binary  = '';
        const bytes = new Uint8Array(data);
        const len   = bytes.byteLength;

        for (let i = 0; i < len; i++) 
		{
          	binary += String.fromCharCode(bytes[i]);
        }

        const file = window.btoa(binary);

		const mimType = type === 'pdf' ? 'application/pdf' : type === 'xlsx' ? 'application/xlsx' :
		type === 'pptx' ? 'application/pptx' : type === 'csv' ? 'application/csv' : type === 'docx' ? 'application/docx' :
		type === 'jpg' ? 'application/jpg' : type === 'png' ? 'application/png' : '';
        
		return `data:${mimType};base64,` + file;
	}

	/* address format  */
	addressFormat(args:any)
	{
		if(args && args.length > 0)
		{
			return args[0].street + ', ' + args[0].housenumber + ' ' + args[0].complement
			+ ', ' + args[0].district + ' - ' + args[0].city + ' / ' + args[0].state + ' - '
			+ args[0].zipcode;
		}
		
		return "";
	}; 

	makeClickHtml(elementRef)
	{
		const elements = elementRef.nativeElement.querySelectorAll('[push], [open], [href]');

		for(const key in elements)
		{
			const element = elements[key];

			if(typeof element == "object")
			{				
				let href = element.getAttribute('href');

				if(href)
				{
					// TROCA HREF POR PUSH SE MOBILE://
					if(href.indexOf('app://') > -1)
					{
						href 		 = href.replace('app://', '');
						const router = href.split('?')[0].split(';')[0];						
						const query  = this.queryStringToJSON(href);
						
						element.removeAttribute('href');
						element.setAttribute('push',   router);
						element.setAttribute('params', JSON.stringify(query));
					}
					else if(href.indexOf('http://') > -1 || href.indexOf('https://') > -1)
					{
						element.removeAttribute('href');
						element.setAttribute('open', href);
					}
					else if(href.indexOf('function:') > -1)
					{						
						href = href.replace('function:', '');
						element.removeAttribute('href');
						element.setAttribute('function', href);
					}
				}

				element.addEventListener('click', async () =>
				{
					const router   = element.getAttribute('push');
					const open     = element.getAttribute('open');
					const funktion = element.getAttribute('function');
					
					console.log('makeClickHtml:', router, open);

					if(router)
					{
						let params = element.getAttribute('params');

						if(params)
						{
							params = JSON.parse(params.replace(/\'/g, '"'));
						}

						this.core().push(router, params,
						{
							time : new Date().getTime()
						});
					}  
					else if(open)
					{
						await this.openUrl(open);
					}
					else if(funktion)
					{
						const call = FunctionsType.get().getFunction(funktion);
						await call();
					}
				})
			}
		}
	}

	async openUrl(url:string)
	{
		// CASO FOR E-MAIL
		if(this.core().isMobile() && this.core().util.isEmail(url))
		{			
			await this.core().loadingController.start();

			// Share via email
			try
			{
				const socialSharing	= this.core().injector.get(SocialSharing)

				await socialSharing.shareViaEmail('', '', [url.split('?')[0]]);
			}			
			catch(e:any)
			{
				this.showAlert('Atenção', "Aplicativo de e-mail não localizado ou sem permissão");
			}

			this.core().loadingController.stop();
		}
		else
		{
			await this.core().loadingController.start();

			await Browser.open({url : url});

			this.core().loadingController.stop();

			/*Browser.addListener("browserFinished", () => 
			{
				this.core().back();
			});*/	
		}	
	}

	showAlert(title:string, message:string, buttons?:any, inputs?:any)
    {
		const alertController = this.core().injector.get(AlertController);

        alertController.create({
            header  : title,
			message : message,
			inputs  : inputs || [],
            buttons : buttons || ['OK']
        })
        .then((alert:any) =>
        {
            alert.present();
        });
	}

    randomString = function(length:number)
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
	}

	//https://gist.github.com/CawaKharkov/1c477d44fcfdf67aea3f
	convertImgToBase64URL(url)
	{
		return new Promise(function(resolve, reject) 
        {
            const img 		= new Image();
			img.crossOrigin = 'Anonymous';
			img.onload 		= () =>
			{
				let canvas : any = document.createElement('CANVAS');
				const ctx 	  	 = canvas.getContext('2d');
				canvas.height 	 = img.height;
				canvas.width  	 = img.width;

				ctx.drawImage(img, 0, 0);
				
				const dataURL = canvas.toDataURL('image/png');
				resolve(dataURL);

				canvas = null; 
			};
			img.src = url;
        });		
	}

	isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); } 

	isEmail(text) 
	{ 
		var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
		return re.test(text);
	}

	removeAccents(str:string) 
	{
		//str = str.toLowerCase(); NAO PODE FAZER ISSO POR CONTA DE BUSCA FULL
		const accents : any = 'ÀÁÂÃÄÅĄĀāàáâãäåąßÒÓÔÕÕÖØŐòóôőõöøĎďDŽdžÈÉÊËĘèéêëęðÇçČčĆćÐÌÍÎÏĪìíîïīÙÚÛÜŰùűúûüĽĹŁľĺłÑŇŃňñńŔŕŠŚŞšśşŤťŸÝÿýŽŻŹžżźđĢĞģğ';
		const accents_out   = "AAAAAAAAaaaaaaaasOOOOOOOOoooooooDdDZdzEEEEEeeeeeeCcCcCcDIIIIIiiiiiUUUUUuuuuuLLLlllNNNnnnRrSSSsssTtYYyyZZZzzzdGGgg";
		const accents_map   = new Map();

		for(const i in accents)
		{
			accents_map.set(accents.charCodeAt(i), accents_out.charCodeAt(parseInt(i)))
		}			
			
		const nstr = new Array(str.length);
		let x, i;
		for (i = 0; i < nstr.length; i++)
			nstr[i] = accents_map.get(x = str.charCodeAt(i)) || x;

		return String.fromCharCode.apply(null, nstr);
	}

    /* DOWNLOAD */
    /*download(files:any)
    {
        var zip = new JSZip();

        for(const key in files)
        {
            const file = files;
            zip.file(file[key].name, this.urlToPromise(file[key]._url), {binary:true});
        }

        Core.get().loadingController.start().then(() => 
        {
            zip.generateAsync({type:"blob"}).then((content:any) =>
            {                
                FileSaver.saveAs(content, "download.zip");

                Core.get().loadingController.stop();
            });
        });
	}*/
	
	rad(x) 
	{
		return x * Math.PI / 180;
	};

	getDistance(p1, p2) 
	{
		var R 	  = 6378137;
		var dLat  = this.core().util.rad(p2.lat() - p1.lat());
		var dLong = this.core().util.rad(p2.lng() - p1.lng());
		var a 	  = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.core().util.rad(p1.lat())) * Math.cos(this.core().util.rad(p2.lat())) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c 	  = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d	  = R * c;
		
		return d;
	};

	concat(...args:any)
	{
		let value     = '';
		let separator = ' - ';

		for(const key in args[0])
		{
			if(args[0][key])
			{
				value += args[0][key] + (parseInt(key) < args[0].length - 1 && args[0][key] ? separator : '');
			}
		}

		return value;
	};

    /*urlToPromise(url:string):any 
    {
        return new Promise(function(resolve, reject) 
        {
            JSZipUtils.getBinaryContent(url, function (err, data) 
            {
                if(err) 
                {
                    reject(err);
                } 
                else 
                {
                    resolve(data);
                }
            });
        });
    }*/

    /* PIPE */
    qrcodeFormat(value:string)
    {
        return new Promise<any>(resolve => 
        {
            if(value)
            {                
                /*QRCode.toDataURL(value).then((url) => 
                {
                    resolve(url);
                })*/
                
                resolve('<img src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=' + value + '"/>');
            }
            else
            {
                resolve(null);
            }
        })
    }

    /* PIPE */
    dateFormat(date:any, format:string = 'dd/MM/yyyy')
    {
        return new Promise(resolve => 
        {
            if(typeof date.toDate == 'function')
            {
                date = date.toDate();
            }
    
            resolve(new TDate({ value : date}).format(format));
        })       
    }

    /* PIPE */
    currencyFormat(value:any)
    {
        return new Promise(resolve => 
        {
			resolve(new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(value));
            //resolve('R$ ' + value.toLocaleString('pt-BR', { minimumFractionDigits : 2}))
        })
    }

    isValidMD5(value:string)
    {
        return (/[a-fA-F0-9]{32}/).test(value);
    }

    secondsToHours(value:number)
    {
        const hours   : any = value / 3600;
        const minutes : any = value % 3600 / 60;

        return {
            hours   : parseInt(hours),
            minutes : parseInt(minutes),
            format  : parseInt(hours) + 'h' + parseInt(minutes) + '',
        }
        //const seconds = parseInt(value % 60);
    }

    getMonthDateRange(year, month)
    {
        // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
        // array is 'year', 'month', 'day', etc
        var startDate = new TDate({ value : new Date(year, month - 1, 1)});

        // Clone the value before .endOf()
        var endDate = new TDate({ value : startDate}).endOfMonth().toDate();

        // make sure to call toDate() for plain JavaScript date type
        return { start: startDate, end: endDate };
    }

    getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2)
    {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2-lon1);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km

        return d;
    }

    queryStringToJSON(queryString, wildcard="?", separator="&"):any
    {
        if(queryString.indexOf(wildcard) > -1)
        {
            queryString = queryString.split(wildcard)[1];
        }

        var pairs  = queryString.split(separator);
        var result = {};

        pairs.forEach(function(pair) {
            pair = pair.split('=');
            result[pair[0]] = decodeURIComponent(pair[1] || '');
        });

        return result;
	}
	
	routeParamsToJSON(queryString, wildcard="?", separator=";"):any
    {
        if(queryString.indexOf(wildcard) > -1)
        {
            queryString = queryString.split(wildcard)[0];
        }

        var pairs  = queryString.split(separator);
        var result = {};

		pairs.forEach(function(pair) 
		{
			if(pair[1])
			{
				pair = pair.split('=');

				if(pair[1])
				{
					result[pair[0]] = decodeURIComponent(pair[1]);	
				}				
			}
        });

        return result;
    }

    deg2rad(deg)
    {
        return deg * (Math.PI/180)
    }

    calc(value)
    {
        return eval(value);
	}
	
	cardMask(value)
    {
        const r = /\b(?:\d{4}[ -]?){3}(?=\d{4}\b)/gm;
		const s = `**** **** **** `;
		return value.replace(r, s);
    }

    sumFloat(value1:number, value2:number):number
    {
        let decimalCount1 = this.decimalCount(value1);
        let decimalCount2 = this.decimalCount(value2);

        return parseFloat((parseFloat('' + value1) + parseFloat('' + value2)).toFixed(Math.max(decimalCount1, decimalCount2)));
    }

    subFloat(value1:number, value2:number):number
    {
        let decimalCount1 = this.decimalCount(value1);
        let decimalCount2 = this.decimalCount(value2);

        return parseFloat((parseFloat('' + value1) - parseFloat('' + value2)).toFixed(Math.max(decimalCount1, decimalCount2)));
    }

    decimalCount(num):number
    {
        var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) { return 0; }
        return Math.max(
            0,
            // Number of digits right of decimal point.
            (match[1] ? match[1].length : 0)
            // Adjust for scientific notation.
            - (match[2] ? +match[2] : 0)
        );
    }

    parseFloat(value)
    {
        return parseFloat(value);
    }

    parseInt(value)
    {
        return parseInt(value);
    }

    parseVariables(data, message, returnVariables?:any)
    {
        return new Promise<any>((resolve) =>
        {
			let variables = message.match(/\${([^{][^}]+)}/g);
			const funcs   = message.match(/([a-zA-Z_{1}][a-zA-Z0-9_]+)(?=\()/g);
			const isCalc  = /(?!^-)[+*\/-](\s?-)?/.test(message);

            /* DEPOIS REFAZER USANDO O PROPRIO getPathValue que foi alterado*/
            if(variables)
            {
                //console.log('parseVariables', variables);

                const paths = [];
                const pipes = [];

                for(const key in variables)
                {
                    const value  = variables[key].replace("${","").replace("}","").replace(/\s+/g, '');
                    const values = value.split('|');

                    paths[key] = values[0];
                    pipes[key] = (values[1] ? values[1] : null);
                }

                const promises = [];

                //console.log('parseVariables path', paths, pipes);

                for(const key in paths)
                {
                    if(paths[key] == 'fieldsTable')
                    {
                        promises.push(this.core().util.fieldsTable(data));
                    }
                    else if(paths[key].indexOf('loggedUser') > -1)
                    {
                        paths[key] = paths[key].replace('loggedUser.', '');
                        promises.push(Core.get().user.getPathValue(paths[key]));
                    }
                    else
                    {
                        promises.push(data.getPathValue(paths[key]));
                    }
                }
                   
                /* UPDATE VALUES */
                Promise.all(promises).then((values) =>
                {
                    //console.log('parseVariables values', values);

                    const promisePipes = []

					if(funcs)
					{
						for(const key in funcs)
						{
							values = this.core().util[funcs[key]](values)
						}						

						resolve(values);
					}
					else
					{
						for(const key in values)
						{
							if(values[key] == undefined)
							{
								values[key] = '';
							}
	
							if(pipes[key])
							{
								promisePipes.push(this.core().util[pipes[key]](values[key]));
							}
							else
							{
								promisePipes.push(new Promise((resolve) => 
								{
									resolve(values[key]);
								}))
							}                        
						}
					}                                    

                    /* UPDATE WITH PIPES */
                    Promise.all(promisePipes).then((values2) => 
                    {
						if(returnVariables)
						{
							resolve(values2);
						}
						else
						{
							for(const key in values2)
							{
								if(isCalc && values2[key] == '')
								{
									values2[key] = 0;
								}

								message = message.replace(variables[key], values2[key]);     
							}

							resolve(message);
						}
                    });
                });
            }
            else
            {
                resolve(message);
            }
        });
    }

    fieldsTable(data:any)
    {
        return new Promise<string>((resolve) =>
        {
            data.form.on(true).then((form2:any) =>
            {
				const items = [];
				
                for(let key in form2.items)
                {
                    const formItem = form2.items[key];

                    if(formItem.field.isDisplay())
                    {
                        items.push(formItem);
                    }
                }

                /* GERAR OUTPUTS */
                const promises = [];

                for(const key in items)
                {
                    promises.push(TransformFactory.transform(items[key], data));
                }

                Promise.all(promises).then(outputs =>
                {
                    console.log('outputs', outputs);

                    /* GERAR TABLE */
                    let html = '<table>';

                    for(const key in outputs)
                    {
                        const output = outputs[key];

                        if(!output.value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;"></td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('LABEL').value)
                        {
                            html += '<tr>';
                                html += '<td colspan="2" style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2; text-align:center;">' + output.label + '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('SELECT').value ||
                                output.formItem.field.type.value == FieldType.type('RADIO').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value.label + '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('ReferenceSelect').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value.name + '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('ADDRESS').value)
                        {
                            html += '<tr>';
                                html += '<td colspan="2" style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                            html += '</tr>';

                            for(const key in output.value)
                            {
                                html += '<tr>';
                                    html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.value[key].label + '</td>';
                                    html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value[key].value + '</td>';
                                html += '</tr>';
                            }
                        }
                        else if(output.formItem.field.type.value == FieldType.type('MultiCheckbox').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">';

                                    for(const key in output.value)
                                    {
                                        html += output.value[key].label + '<br>';
                                    }

                                html += '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('DATE').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value.format('dd/MM/yyyy') + '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('Birthday').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value.format('dd/MM/yyyy') + ' (' + new TDate().diff(output.value, 'years') + ')</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('UploadJson').value ||
                                output.formItem.field.type.value == FieldType.type('AVATAR').value ||
                                output.formItem.field.type.value == FieldType.type('DOWNLOAD').value)
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">';
                                    html += '<a href="' + output.value.url + '">' + output.value.url + '</a>';
                                html += '</td>';
                            html += '</tr>';
                        }
                        else if(output.formItem.field.type.value == FieldType.type('HIDDEN').value)
                        {
                        }
                        else
                        {
                            html += '<tr>';
                                html += '<td style="border-bottom:solid 1px #d2d2d2;padding: 5px 10px; background:#f2f2f2;">' + output.label + '</td>';
                                html += '<td style="border-bottom:solid 1px #f2f2f2;padding: 5px 10px; background:#f9f9f9;">' + output.value + '</td>';
                            html += '</tr>';
                        }
                    }

                    html += '</table>';

                    resolve(html);
                });
            });
        });
	}

	getCountries()
	{
		//https://gist.github.com/ssskip/5a94bfcd2835bf1dea52
		const data = {
			"AF": "Afghanistan",
			"AX": "Aland Islands",
			"AL": "Albania",
			"DZ": "Algeria",
			"AS": "American Samoa",
			"AD": "Andorra",
			"AO": "Angola",
			"AI": "Anguilla",
			"AQ": "Antarctica",
			"AG": "Antigua And Barbuda",
			"AR": "Argentina",
			"AM": "Armenia",
			"AW": "Aruba",
			"AU": "Australia",
			"AT": "Austria",
			"AZ": "Azerbaijan",
			"BS": "Bahamas",
			"BH": "Bahrain",
			"BD": "Bangladesh",
			"BB": "Barbados",
			"BY": "Belarus",
			"BE": "Belgium",
			"BZ": "Belize",
			"BJ": "Benin",
			"BM": "Bermuda",
			"BT": "Bhutan",
			"BO": "Bolivia",
			"BA": "Bosnia And Herzegovina",
			"BW": "Botswana",
			"BV": "Bouvet Island",
			"BR": "Brasil",
			"IO": "British Indian Ocean Territory",
			"BN": "Brunei Darussalam",
			"BG": "Bulgaria",
			"BF": "Burkina Faso",
			"BI": "Burundi",
			"KH": "Cambodia",
			"CM": "Cameroon",
			"CA": "Canada",
			"CV": "Cape Verde",
			"KY": "Cayman Islands",
			"CF": "Central African Republic",
			"TD": "Chad",
			"CL": "Chile",
			"CN": "China",
			"CX": "Christmas Island",
			"CC": "Cocos (Keeling) Islands",
			"CO": "Colombia",
			"KM": "Comoros",
			"CG": "Congo",
			"CD": "Congo, Democratic Republic",
			"CK": "Cook Islands",
			"CR": "Costa Rica",
			"CI": "Cote D\"Ivoire",
			"HR": "Croatia",
			"CU": "Cuba",
			"CY": "Cyprus",
			"CZ": "Czech Republic",
			"DK": "Denmark",
			"DJ": "Djibouti",
			"DM": "Dominica",
			"DO": "Dominican Republic",
			"EC": "Ecuador",
			"EG": "Egypt",
			"SV": "El Salvador",
			"GQ": "Equatorial Guinea",
			"ER": "Eritrea",
			"EE": "Estonia",
			"ET": "Ethiopia",
			"FK": "Falkland Islands (Malvinas)",
			"FO": "Faroe Islands",
			"FJ": "Fiji",
			"FI": "Finland",
			"FR": "France",
			"GF": "French Guiana",
			"PF": "French Polynesia",
			"TF": "French Southern Territories",
			"GA": "Gabon",
			"GM": "Gambia",
			"GE": "Georgia",
			"DE": "Germany",
			"GH": "Ghana",
			"GI": "Gibraltar",
			"GR": "Greece",
			"GL": "Greenland",
			"GD": "Grenada",
			"GP": "Guadeloupe",
			"GU": "Guam",
			"GT": "Guatemala",
			"GG": "Guernsey",
			"GN": "Guinea",
			"GW": "Guinea-Bissau",
			"GY": "Guyana",
			"HT": "Haiti",
			"HM": "Heard Island & Mcdonald Islands",
			"VA": "Holy See (Vatican City State)",
			"HN": "Honduras",
			"HK": "Hong Kong",
			"HU": "Hungary",
			"IS": "Iceland",
			"IN": "India",
			"ID": "Indonesia",
			"IR": "Iran, Islamic Republic Of",
			"IQ": "Iraq",
			"IE": "Ireland",
			"IM": "Isle Of Man",
			"IL": "Israel",
			"IT": "Italy",
			"JM": "Jamaica",
			"JP": "Japan",
			"JE": "Jersey",
			"JO": "Jordan",
			"KZ": "Kazakhstan",
			"KE": "Kenya",
			"KI": "Kiribati",
			"KR": "Korea",
			"KW": "Kuwait",
			"KG": "Kyrgyzstan",
			"LA": "Lao People\"s Democratic Republic",
			"LV": "Latvia",
			"LB": "Lebanon",
			"LS": "Lesotho",
			"LR": "Liberia",
			"LY": "Libyan Arab Jamahiriya",
			"LI": "Liechtenstein",
			"LT": "Lithuania",
			"LU": "Luxembourg",
			"MO": "Macao",
			"MK": "Macedonia",
			"MG": "Madagascar",
			"MW": "Malawi",
			"MY": "Malaysia",
			"MV": "Maldives",
			"ML": "Mali",
			"MT": "Malta",
			"MH": "Marshall Islands",
			"MQ": "Martinique",
			"MR": "Mauritania",
			"MU": "Mauritius",
			"YT": "Mayotte",
			"MX": "Mexico",
			"FM": "Micronesia, Federated States Of",
			"MD": "Moldova",
			"MC": "Monaco",
			"MN": "Mongolia",
			"ME": "Montenegro",
			"MS": "Montserrat",
			"MA": "Morocco",
			"MZ": "Mozambique",
			"MM": "Myanmar",
			"NA": "Namibia",
			"NR": "Nauru",
			"NP": "Nepal",
			"NL": "Netherlands",
			"AN": "Netherlands Antilles",
			"NC": "New Caledonia",
			"NZ": "New Zealand",
			"NI": "Nicaragua",
			"NE": "Niger",
			"NG": "Nigeria",
			"NU": "Niue",
			"NF": "Norfolk Island",
			"MP": "Northern Mariana Islands",
			"NO": "Norway",
			"OM": "Oman",
			"PK": "Pakistan",
			"PW": "Palau",
			"PS": "Palestinian Territory, Occupied",
			"PA": "Panama",
			"PG": "Papua New Guinea",
			"PY": "Paraguay",
			"PE": "Peru",
			"PH": "Philippines",
			"PN": "Pitcairn",
			"PL": "Poland",
			"PT": "Portugal",
			"PR": "Puerto Rico",
			"QA": "Qatar",
			"RE": "Reunion",
			"RO": "Romania",
			"RU": "Russian Federation",
			"RW": "Rwanda",
			"BL": "Saint Barthelemy",
			"SH": "Saint Helena",
			"KN": "Saint Kitts And Nevis",
			"LC": "Saint Lucia",
			"MF": "Saint Martin",
			"PM": "Saint Pierre And Miquelon",
			"VC": "Saint Vincent And Grenadines",
			"WS": "Samoa",
			"SM": "San Marino",
			"ST": "Sao Tome And Principe",
			"SA": "Saudi Arabia",
			"SN": "Senegal",
			"RS": "Serbia",
			"SC": "Seychelles",
			"SL": "Sierra Leone",
			"SG": "Singapore",
			"SK": "Slovakia",
			"SI": "Slovenia",
			"SB": "Solomon Islands",
			"SO": "Somalia",
			"ZA": "South Africa",
			"GS": "South Georgia And Sandwich Isl.",
			"ES": "Spain",
			"LK": "Sri Lanka",
			"SD": "Sudan",
			"SR": "Suriname",
			"SJ": "Svalbard And Jan Mayen",
			"SZ": "Swaziland",
			"SE": "Sweden",
			"CH": "Switzerland",
			"SY": "Syrian Arab Republic",
			"TW": "Taiwan",
			"TJ": "Tajikistan",
			"TZ": "Tanzania",
			"TH": "Thailand",
			"TL": "Timor-Leste",
			"TG": "Togo",
			"TK": "Tokelau",
			"TO": "Tonga",
			"TT": "Trinidad And Tobago",
			"TN": "Tunisia",
			"TR": "Turkey",
			"TM": "Turkmenistan",
			"TC": "Turks And Caicos Islands",
			"TV": "Tuvalu",
			"UG": "Uganda",
			"UA": "Ukraine",
			"AE": "United Arab Emirates",
			"GB": "United Kingdom",
			"US": "United States",
			"UM": "United States Outlying Islands",
			"UY": "Uruguay",
			"UZ": "Uzbekistan",
			"VU": "Vanuatu",
			"VE": "Venezuela",
			"VN": "Vietnam",
			"VG": "Virgin Islands, British",
			"VI": "Virgin Islands, U.S.",
			"WF": "Wallis And Futuna",
			"EH": "Western Sahara",
			"YE": "Yemen",
			"ZM": "Zambia",
			"ZW": "Zimbabwe"
		};

		const items = [];
		//let lll = '';

		for(const key in data)
		{
			items.push({
				id       : key.toLocaleLowerCase(),
				label    : data[key],
				value    : key.toLocaleLowerCase(),
				selected : key.toLocaleLowerCase() == 'br',
			});

			//lll += data[key] + '|' + key.toLocaleLowerCase() + '\n';	
		}

		//console.log(lll);

		return items;
	}

	getCountryByLabel(label:string)
	{
		const items = this.getCountries();

		for(const key in items)
		{
			if(label.toLowerCase() == items[key].label.toLowerCase())
			{
				return items[key];
			}
		}
	}

	core()
    {
        return Core.get();
    }
}
