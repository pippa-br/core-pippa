import { WhereCollection } from '../../../core/model/where/where.collection';

export class Params extends Object
{
    static ADD_MODE_FORM    : string = 'add';
    static SET_MODE_FORM    : string = 'set';
    static VIEWER_MODE_FORM : string = 'viewer';
    static DESKTOP_PLATFORM : any = { id: 'desktop', label : 'Desktop', value : 'desktop'};
    static MOBILE_PLATFORM  : any = { id: 'mobile',  label : 'Mobile',  value : 'mobile'};

    public ref             : any;
    public id              : string;
    public appid           : string;
	public maccid          : string;
	public accid           : string;
	public colid           : string;
    public page            : number;
    public perPage         : number;
    public method          : string;
    public context         : string; /* FIELD CONTEXT */
	public model           : any;
	public acl             : any;
    public collectionModel : any;
    public accs            : Array<number>;
    public apps            : Array<number>;
    public replaces        : Array<any>;
    public user            : any;
	public count           : number;
	public total           : number;
    public query           : any;
    public to              : any;
    public orderBy         : string;
    public asc             : boolean;
    public post            : any;
    public data            : any;
    public apiUrl          : string;
    public loadingContent  : string;
    public collection      : Array<any>;
    public where           : any;
    public path            : string; /* CAMINHO DO FIREBASE */
    public name            : string;
    public sender          : any;
    public receiver        : any;
    public sort            : any;
    public form            : any;
    public type            : any;
    public groups          : Array<any>;
	public chat            : any;
	public target          : any;
    public withCredentials : boolean;
	public index           : any;
	public key 			   : string;
	public headers         : any;
	public parse           : boolean;
	public loading         : boolean;
	public autoReload      : boolean;
	public onlyCount       : boolean;
	public extraFilters    : any;
	public groupCollection : boolean;
	public onlyMaster 	   : boolean;
    public code            : string;
    public listEndPoint    : string;
    public getEndPoint     : string;
	public map		 	   : boolean;
    public includeForm     : boolean;
	public mapItems        : any;
    public platform        : string;
    public token           : string;
	public searchVector    : string;

    constructor(args)
    {
		super();
		
		this.parse 			 = true;
		this.withCredentials = true;
		this.groupCollection = false;
		//this.onlyMaster 	 = false; NÃO ADICIONAR
		this.autoReload      = false;
		this.onlyCount       = false;
		this.loading 		 = true;
		this.map 			 = false;
		this.method 		 = 'Post';
        this.loadingContent  = '';
        this.page            = 1;
        this.perPage         = 24;
		this.where           = new WhereCollection();

        if(args)
        {
            for(let key in args)
            {
				if(key == 'where')
				{
					this.where.setItems(args[key]);
				}
				else
				{
					this[key] = args[key];
				}								
            }

            if(this.asc == undefined)
            {
                this.asc = true;
            }
        }
    }

    parseQuery : () => any = function()
    {
        let urlSearchParams = new URLSearchParams();

        for(var key in this.query)
        {
            urlSearchParams.append(key, this.query[key]);
        };

        return urlSearchParams;
    }

    getPost : () => any = function()
    {
        if(!this.post)
        {
            this.post = {};
        }

        if(this.page)
        {
            this.post.page = this.page;
        }

        if(this.perPage)
        {
            this.post.perPage = this.perPage;
        }

        if(this.app)
        {
            this.post.accid = this.app.accid;
            this.post.appid = this.app.code;
        }

        if(this.id)
        {
            this.post.id = this.app.id;
        }

        if(this.accs)
        {
            this.post.accs = this.accs;
        }

        if(this.apps)
        {
            this.post.apps = this.apps;
        }

        if(this.user)
        {
            this.post.user = this.user;
        }

		if(this.parse)
		{
			return this.objectToParams(this.post);	
		}        
		else
		{
			return this.post;
		}
    }

    copy()
    {
        return new Params(this);
    }

    isJsObject : (o) => any = function(o)
    {
        return o !== null && (typeof o == 'function' || typeof o == 'object');
    }

    objectToParams : (object) => string = function(object)
    {
        return Object.keys(object).map((key) => this.isJsObject(object[key]) ?
            this.subObjectToParams(encodeURIComponent(key), object[key]) :
            `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`
        ).join('&');
    }

    subObjectToParams : (key, object) => string = function(key, object)
    {
        return Object.keys(object).map((childKey) => this.isJsObject(object[childKey]) ?
                this.subObjectToParams(`${key}[${encodeURIComponent(childKey)}]`, object[childKey]) :
                `${key}[${encodeURIComponent(childKey)}]=${encodeURIComponent(object[childKey])}`
        ).join('&');
    }
}
