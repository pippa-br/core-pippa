export class VideoEmbed extends Object
{
    static VIMEO   : string = 'vimeo';
	static YOUTUBE : string = 'youtube';
	static FILE    : string = 'file';

    public id           : string;
    public title        : string;
    public url          : string;
	public thumbnailUrl : string;;
	public type         : string;
	public time         : string;
    public embed        : string; /* PRECISA POIS TEM VIDEO QUE OS DADOS VEM VIA API */

    constructor(args:any)
    {
        super();

        args = Object.assign({
            'thumbnailUrl' : '',
        }, args);

        for(let key in args)
        {
            this[key] = args[key];
        }
    }

    getEmbed(autoplay = true, width = 500, height = 315)
    {
        if(this.embed)
        {
            return '<div class="embed-responsive embed-responsive-16by9">' + this.embed + '</div>';
        }

        let embed : string = '';

        if(this.type == VideoEmbed.YOUTUBE)
        {
            embed  = '<div class="embed-responsive embed-responsive-16by9">'
                          + '<iframe class="embed-responsive-item" id="video-' + this.id + '" width="' + width + '" height="'
                          + height + '" src="https://www.youtube.com/embed/' + this.id + '?autohide=1&rel=0&autoplay=' + ( autoplay ? '1' : '0') + '" frameborder="0" allowfullscreen></iframe>';
                          + '</div>';
        }
        else if(this.type == VideoEmbed.VIMEO)
        {
            embed = '<div class="embed-responsive embed-responsive-16by9">'
                         + '<iframe class="embed-responsive-item" id="video_' + this.id + '" width="' + width + '" height="' +
                         + height + '" src="https://player.vimeo.com/video/' + this.id + '?badge=0&server=vimeo.com&show_title=1&show_byline=1&autoplay=0&api=1&player_id=video_' +
                         + this.id + '" frameborder="0" allowfullscreen></iframe>';
                         + '</div>';
        }

    	if(!autoplay)
    	{
    		embed = embed.replace('autoplay=1', 'autoplay=0');
    	}

    	embed = embed.replace('width="560"',  'width="'  + width  + '"');
    	embed = embed.replace('height="315"', 'height="' + height + '"');

    	return embed;
    }

    parseData()
    {
        return {
            id           : this.id,
            url          : this.url,
            thumbnailUrl : this.thumbnailUrl,
            type         : this.type,
            embed        : (this.embed ? this.embed : null),
            //time     : this.time,
        }
    }

    /* POR CAUSA DO COLLECTION */
    getPathValue(path:string)
    {
        return new Promise((resolve) =>
        {
            resolve(this[path]);
        })
    }

    isOwner()
    {
        return true;
    }
}
