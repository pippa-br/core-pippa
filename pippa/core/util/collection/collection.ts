/* CORE */
import { Core }    from '../../util/core/core';
import { Subject } from 'rxjs';

export class Collection extends Array
{
	_onUpdate : any
    params : any;
    plugin : any;
    method : any;
    page         = 1;
    pageCount    = 0;
    stop         = false;
    _sort : any;
    public isInfinite : boolean = false;
    public _instance  : any;

    constructor(items?:any)
    {
        super();

        Object.defineProperty(this, "params",       {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "plugin",       {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "method",       {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "page",         {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "pageCount",    {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "stop",         {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "activeFilter", {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "setPlugin",    {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "setMethod",    {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "setFilter",    {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "add",          {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "set",          {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "del",          {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "findPosition", {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "addItems",     {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "setItems",     {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "getItems",     {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "refresh",      {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "call",         {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "clearFilter",  {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_sort",        {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "setSort",      {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "has",          {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_onUpdate",    {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "isInfinite",   {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_instance",    {writable: true, enumerable: false, configurable: true});

		this.initialize(items);        
	}

	/* METODO UTILIZADO PARA INICIALIZAR VARIABLES */
    initialize(items:Array<any>)
    {
		this._onUpdate = new Subject();

		if(items)
        {
            this.populate(items);
        }
	}
	
	isEmpty()
	{
		return this.length == 0;
	}

    populate(items:any)
    {
        for(const key in items)
        {
            this[key] = items[key];
        }
    }

    setPlugin : (plugin) => void = function(plugin)
    {
        this.plugin = plugin;
    }

    setMethod : (method) => void = function(method)
    {
        this.method = method;
    }

    setSort : (s) => void = function(s) : void
    {
        this._sort = s;

        this.sort(this._sort);
    }

    onUpdate()
    {
        return this._onUpdate.asObservable();
    }

    dispatchUpdate()
    {
        this._onUpdate.next(this);
    }

    activeFilter = false;
    _filterCallbackfn;
    _filterItems;
    setFilter : (callbackfn: (value: any, index: number, array: any) => boolean, thisArg?: any) => any = function(callbackfn: (value: any, index: number, array: any) => boolean, thisArg?: any) : any
    {
        if(!this.activeFilter)
        {
            this._filterItems = this.slice();
            this.activeFilter = true;
        }

        let items = this._filterItems.filter(callbackfn, thisArg);
        this.setItems(items);

        this._filterCallbackfn = callbackfn;

        return items;
    }

    clearFilter : () => void = function() : void
    {
        this.setItems(this._filterItems);
        this._filterItems = undefined;
        this.activeFilter = false;
    }

    clear(force:boolean = false)
    {
        /* FORCE POR CAUSA DA BUSCA E INFINE SCROLL */
        if(!this.isInfinite || force)
        {
            this.page      = 1;
            this.pageCount = 0;
            this.stop      = false;

            this.splice(0, this.length);
        }
    }

    add : (item) => void = function(item) : void
    {
        this.set(item);
    }

    set : (item) => void = function(item)
    {
        let position = this.findPosition(item);

        if(position > -1)
        {
            this[position] = item;
        }
        else
        {
            this.push(item);
        }
    }

    del : (item) => void = function(item)
    {
        let position = this.findPosition(item);

        if(position > -1)
        {
            this.splice(position, 1);
        }
    }

    has : (item) => boolean = function(item)
    {
        let position = this.findPosition(item);

        return position > -1;
    }

    findPosition : (item:any) => number = function(item:any)
    {
        let i = 0;

        for(let key in this)
        {
            let item2 = this[key];

            if(item.id == item2.id)
            {
                return i;
            }

            i++;
        }

        return -1;
    }

    addItems : (items) => void = function(items)
    {
        for(let key in items)
        {
            let item = items[key];

            if(!this.has(item))
            {
                this.push(item);
            }
        }
    }

    setItems : (items) => void = function(items)
    {
        this.splice(0, this.length);

        for(let key in items)
        {
            let item = items[key];
            this.set(item);
        }
    }

    getItems : () => any = function()
    {
        let items = [];

        for(let key in this)
        {
            items.push(this[key]);
        }

        console.log('items', items);

        return items;
    }

    refresh : (scroll?:any) => void = function(scroll?:any)
    {
        this.clear();
        this.call(scroll);
    }

    load(scroll?:any)
    {
        this.page++;

        if(this.page >= this.pageCount)
        {
            this.stop = true;
        }

        if(this.stop)
        {
            if(scroll)
            {
                scroll.complete();
            }

            return;
        }

        this.call(scroll);
    }

    call : (scroll?:any) => void = function(scroll?:any)
    {
        this.params.page = this.page;

        this.plugin[this.method](this.params).then((result:any) =>
        {
            let items = result.collection;

            /* FILTER */
            if(this._filterCallbackfn)
            {
                items = items.filter(this._filterCallbackfn);
            }

            /* SORT */
            if(this._sort)
            {
                items = items.sort(this._sort);
            }

            this.addItems(items);

            this.pageCount = result.pageCount;

            if(scroll)
            {
                scroll.complete();
            }
        });
    }

    core()
    {
        return Core.get()
    }
}
