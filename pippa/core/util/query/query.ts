import validator from 'validator';

/* PIPPA */
import { WhereCollection } from '../../../core/model/where/where.collection';
import { FieldType }       from '../../../core/type/field/field.type';

export class Query extends Object
{
    public __query    : any; /* ORIGINAL QUERY */
    public where      : WhereCollection; /* USANDO ISSO POR CAUSA DO CIRCULAR DEPENDENCIA, POIS LIST TEM QUERY */
    public orderBy    : any;
    public perPage    : number;
    public page       : number;
    public asc        : boolean;
    public startAfter : any;
    public snapshots  : Array<any>;
    public _total     : number;
    public startAt    : any;
	public endAt      : any;
	public listEndPoint : string;
    public getEndPoint  : string;
    public term       : string;
	public maccid     : string;
	public accid      : string;
	public appid      : string;
	public colid      : string;
	public groupCollection : boolean; 
	public onlyMaster : boolean;
	public loading    : boolean;
	public map 		  : boolean;
	public mapItems   : any;
    public platform   : string;
    public searchVector : string;
	
    constructor(args:any)
    {
        super();

        this.reset();

        this.__query = args.query;
        delete args.query;

        for(let key in args)
        {
            this[key] = args[key];
        }        
    }

    reset()
    {
        this.snapshots  = [];
        this.page       = 1;
        this._total     = 0;
		this.startAfter = null;
		this.term       = null;
    }

    reload()
    {
        return new Promise(async (resolve:any) =>
        {
            //if(this.__query)
            //{
                /* RUN QUERY */
                let query : any = {
					total  			: this._total,
					listEndPoint 	: this.listEndPoint,
                    maccid 			: this.maccid,
					accid  			: this.accid,
					appid  			: this.appid,
					colid  			: this.colid,
					map  			: this.map,
					mapItems  		: this.mapItems,
					groupCollection : this.groupCollection,
					onlyMaster 		: this.onlyMaster,
					loading 		: this.loading,
                    platform        : this.platform,
                    searchVector    : this.searchVector,
				};

				//let noOrder = false; /* order by clause cannot contain a field with an equality filter class */
				let _where  = [];

                //&& !noOrder
                if(this.orderBy)
                {
					query.orderBy = this.orderBy;
					query.asc 	  = this.asc;

					if(this.startAfter)
                    {
                        query.startAfter = this.startAfter;
                    }

                    if(this.page >= 0)
                    {
                        query.page = this.page;
                    }
				}

                if(this.perPage)
                {
                    query.perPage = this.perPage;
                }

                if(this.asc)
                {
                    if(this.startAt)
                    {
                        query.startAt = this.startAt.path;
                    }

                    if(this.endAt)
                    {
                        query.endAt = this.endAt.path;
                    }
                }
                else
                {
                    if(this.startAt)
                    {
                        query.startAt = this.startAt.path;
                    }

                    if(this.endAt)
                    {
                        query.endAt = this.endAt.path;
                    }
				}
				
				/* TERM */
				if(this.term)
				{
					query.term = this.term;

					_where.push({
						field    : 'search', 
						operator : 'array-contains', 
						value    : this.term,
					});

                    this.searchVector = this.term;
				}

                if(this.where && this.where.length > 0)
				{
					if(!(this.where instanceof WhereCollection))
					{
						this.where = new WhereCollection(this.where);
					}

					/* PARA CHAMADA VIA API */
					const parse = await this.where.parseFilter();
					_where      = _where.concat(parse); 

					query._where = _where;

					console.log('query', query, this);
					
					resolve(query);
				}
				else
				{
					/* PARA CHAMADA VIA API */
					query._where = _where; 

					console.log('query', query, this);					

					resolve(query);
				}
            /*}
            else
            {
                resolve({
					_where : []
				});
            }*/
        });
	}
	
	/*reloadSearch()
	{
		return new Promise((resolve:any) =>
        {
			const query : any = {};
			query.hitsPerPage = this.perPage;
			query.page 		  = this.page - 1;
			query.term        = this.term;
	
			if(this.where && this.where.length > 0)
			{
				if(!(this.where instanceof WhereCollection))
				{
					this.where = new WhereCollection(this.where);
				}
	
				this.where.on().then(async () =>
				{
					const filters = await this.where.parseSearch();

					if(filters)
					{
						query.filters = filters
					}

					resolve(query);
				});
			}
			else
			{
				resolve(query);
			}
		});		
	}*/

    /*async onSnapshot()
    {
        let items;
        await this.reload().onSnapshot(items => items);

        setTimeout(() =>
        {
            return items;
        });
    }*/

    nextPage(page:number)
    {
        if(page >= 0)
        {
            this.page = page;
            
            console.log('nextPageOffset', this.page, Math.ceil(this._total / this.perPage));    
        }
    }

    nextPageReference(referencePath:any)
    {
        if(referencePath)
        {
            this.startAfter = {
                referencePath : referencePath
            };
            this.snapshots.push(this.startAfter);
            this.page++;
            
            console.log('nextPage', this.page, Math.ceil(this._total / this.perPage));    
        }
    }

    prevPageReference()
    {
        this.snapshots.pop();

        if(this.snapshots.length == 0)
        {
            this.startAfter = null;
        }
        else
        {
            this.startAfter = this.snapshots[this.snapshots.length - 1];
        }

        this.page--;
    }

    hasPrevPage()
    {
        return this.page > 1;
    }

    hasNextPage()
    {
        //console.error('hasNextPage', this._total, this.perPage, this.page, Math.ceil(this._total / this.perPage) > this.page);

        return Math.ceil(this._total / this.perPage) > this.page;
    }
}
