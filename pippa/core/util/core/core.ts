import { Howl } from "howler";
import * as _ from "lodash" 
//const openpgp = require('openpgp');

/* PIPPA */
import { IApp }               from "../../interface/app/i.app";
import { AppCollection }      from "../../model/app/app.collection";
import { User }               from "../../model/user/user";
import { Account }            from "../../model/account/account";
import { ParamsFactory }      from "../../factory/params/params.factory";
import { ResultFactory }      from "../../factory/result/result.factory";
import { ComponentFactory }   from "../../factory/component/component.factory";
import { AccountPlugin }      from "../../plugin/account/account.plugin";
import { AppPlugin }          from "../../plugin/app/app.plugin";
import { StoragePlugin }      from "../../plugin/storage/storage.plugin";
import { AuthPlugin }         from "../../plugin/auth/auth.plugin";
import { EventPlugin }        from "../../plugin/event/event.plugin";
import { MenuPlugin }         from "../../plugin/menu/menu.plugin";
import { DocumentPlugin }     from "../../plugin/document/document.plugin";
import { FunctionsType }      from "../../type/functions/functions.type";
import { NotificationPlugin } from "../../plugin/notification/notification.plugin";
import { OptionPlugin }  	  from "../../plugin/option/option.plugin";
import { Option }   		  from "../../model/option/option";
import { Util }   		  	  from "../../util/util/util";
import { doc, getDoc } 		  from "firebase/firestore";

//@dynamic
export class Core extends Object
{
    static instance   : any;
    static isCreating  = false;

    public versionApp 		  = "{{APP_BUILD_VERSION}}";
    public _injector         : any;
    public navController     : any;
    public baseUrl            = "/";
    public apiUrl             = "";
    public apiDevUrl          = "/api";	
    public angularFirestore  : any
    public isModal           : any;
    public notification      : any;    
    public platform          : any = {
        value : "desktop"
    };
    public optionPermission  : any;
    public fireStore         : any;
    public solved            : any = { // NÃO ALTERAR AQUI
        name : "Solved by Pippa",
        url  : "https://pippa.com.br"
    };

    /* API */
    public api : any;

    public mode = "add";

    /* NAVEGATION */
    public apps            : AppCollection;
    public currentApp      : IApp;
    public currentLanguage : any;
    public queryParams     : any;
    public currentRouter   : string;
    public currentArgs     : any = {};
    public loadQuery       : any;
    public currentContent  : any;
    public iframe           = false;
    public access           = false;
    public _isMobile        = false;
    public alertSound      : any;
    public timezone        : any = { label : "" };

    /* FACTORYS */
    public paramsFactory    : ParamsFactory;
    public resultFactory    : ResultFactory;
    public componentFactory : ComponentFactory;
    public util 			: Util;

    public viewers    : Array<string>;
    public outputs    : Array<string> = [];
    public transforms : Array<string> = [];

    // ACCOUNTS
    public userAccount     = false; /* QUANDO O LOGIN FOI FEITO POR UMA CONTA */
    public accountProfile  = false; /* QUANDO HOUVER UMA MUDANÇA DE CONTA NA MESMA SECAO */
    public backAccount    : any;
    public masterAccount  : any;
    public _account       : any;

    public isLoading            = false;
    public isApp                = false;
    public isOnline             = false;
    public device              : any;
    public isPause              = true;
    public currentPosition     : any     = {};
    public isLocationAvailable  = false;
    public isLogged             = false;
    public _user               : any;
    public options             : any;
    public userProfile          = false; /* USADO PARA "LOGAR" COMO OUTRO USUARIO NA MESMA SECAO */
    public isInitialize         = false;
    public menu                : any;
    public fullMenu            : any;
    public cloudMessaging      : any;
    public languageMaps        : any;
    public userSubscribe       : any;
    public storagePlugin       : any;
    public accountPlugin       : any;
    public authPlugin	       : any;
    public eventPlugin	       : any;
    public optionPlugin        : any;
    public jwtToken	 	       : string;
    public currentMenu		   : any;
    public token			   : string;

    public oneSignal : any = {};

    public browser : any = {}

    constructor()
    {
        super();

        if (!Core.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if (Core.instance == null)
        {
            Core.isCreating = true;
            Core.instance   = new Core();
            Core.isCreating = false;
        }

        return Core.instance;
    }

    /* INJECTOR */
    set injector(data:any)
    {
        this._injector = data;

        /* FACTORYS */
        this.componentFactory = this.injector.get(ComponentFactory);
        this.paramsFactory    = new ParamsFactory();
        this.resultFactory    = new ResultFactory();
        this.util             = new Util();
        this.storagePlugin    = this.injector.get(StoragePlugin);
        this.accountPlugin    = this.injector.get(AccountPlugin);
        this.authPlugin       = this.injector.get(AuthPlugin);
        this.eventPlugin   	  = this.injector.get(EventPlugin);
        this.optionPlugin     = this.injector.get(OptionPlugin);
    }

    get isIOS()
    {
        return this.device && this.device.platform == "ios"
    }

    get isAndroid()
    {
        return this.device && this.device.platform == "android"
    }

    get isWeb()
    {
        return this.device && this.device.platform == "web"
    }

    get isElectron()
    {
        return this.device && this.device.platform == "electron"
    }

    isMobile()
    {
        return this._isMobile
    }

    createParams(args:any)
    {
        return this.paramsFactory.create(args);
    }

    createResult(params:any)
    {
        return this.paramsFactory.create(params);
    }

    get injector():any
    {
        return this._injector;
    }

    /* ACCOUNT */
    set account(data:any)
    {
        this._account = data;

        /* QUANDO ALTERAR A CONTA ALTERAR O ACCID */
        /*if(this.apps)
        {
            for(let key in this.apps)
            {
                let app   = this.apps[key];
                app.accid = data.accid;
            }
        }*/
    }

    get account():any
    {
        return this._account;
    }

    getContext(value:string)
    {
        return this[value];
    }

    public set user(data:User)
    {
        if (data)
        {
            if (!(data instanceof User))
            {
                data = new User(data);
            }

            if (this.userSubscribe)
            {
                this.userSubscribe();
            }

            /* PARA EVITAR DOS LOGINS SIMULTANEOS */
            // if(this.masterAccount && this.masterAccount.uniqueAuth && data.reference)
            // {
            // 	this.userSubscribe = docRx(data.reference).subscribe(async (doc:any) =>
            // 	{
            // 		const data2 = doc.data();
		
            // 		const uniqueAuth = await this.storagePlugin.get('uniqueAuth');

            // 		if(uniqueAuth && data2 && data2.uniqueAuth != uniqueAuth)
            // 		{
            // 			const alertController = this.injector.get(AlertController);

            // 			const alert = await alertController.create({
            // 				header  : 'Atenção',
            // 				message : 'Login Realizado em Outro Dispositivo!',
            // 				buttons : ['OK']
            // 			});

            // 			await alert.present();

            // 			/* LOGOUT */
            // 			const params = this.createParams({
            // 				appid : data2.appid 
            // 			})

            // 			this.authPlugin.logout(params);
            // 		}
            // 	});	
            // }
        }

        this._user = data;
    }

    public get user():User
    {
        return this._user;
    }

    setOptions(options:any):any
    {
        this.options = options;
    }

    initialize():Promise<boolean>
    {		
        return new Promise<boolean>(async (resolve:any) =>
        {
            if (this.isInitialize)
            {
                resolve(false);
            }
            else
            {			
                console.log("initialize", this.options);
				
                //const versionPlugin = this.injector.get(VersionPlugin);

                //const check = await versionPlugin.initVersionCheck('./version.json');

                if (navigator.serviceWorker)
                {
                    navigator.serviceWorker.getRegistrations().then((registrations)  => 
                    {				
                        for (const registration of registrations) 
                        { 				
                            registration.unregister().then(() =>
                            {			
                                const _self : any = self;
                                return _self.clients ? _self.clients.matchAll() : [];
                            })
							.then(function(clients) 
							{			
							    clients.forEach(client => 
							    {			
							        if (client.url && "navigate" in client)
							        {			
							            client.navigate(client.url);
							        }			
							    });
							})					
                        }
                    });
                }				

                /*if(!check)
				{
					self.addEventListener('activate', function(event:any) 
					{
						event.waitUntil(
							caches.keys().then(function(cacheNames) {
							return Promise.all(
								cacheNames.filter(function(cacheName) {
								}).map(function(cacheName) {
									return caches.delete(cacheName);
								})
							);
							})
						);
					});

					document.location.reload(true);
				}
				else
				{*/

                /* LOGIN BY APPID */					
                /* ACCOUNT */
                this.account = this.options["account"];

                /* CONTA MAS?TER */
                this.masterAccount = this.options["masterAccount"];

                /* OPTIONS */
                for (const key in this.options)
                {
                    if (key != "account" && key != "masterAccount")
                    {
                        this[key] = this.options[key];
                    }
                }

                console.log("init account", this.masterAccount, this.account);

                console.log("initialize core", this.options);
				
                //await this.getToken();

                let promises = [];				
                promises.push(this.getTimezone());
                promises.push(this.accountMasterLoad());
                promises.push(this.accountLoad());
                promises.push(this.loadOptionPermissions());
				
                await Promise.all(promises);

                await this.authLoad();

                promises = [];
                //promises.push(this.loadNotification());
                promises.push(this.loadApps());				
                //promises.push(this.loadLanguage());				
                promises.push(this.loadMenu());
		
                await Promise.all(promises);
				
                this.loadFacebook();
                //this.loadGTM();
                //this.loadGA();
                //this.loadAlertSound();

                this.isInitialize = true;  

                resolve(true);
            }
        });
    }

    getApp(value:string) : any
    {
        if (value)
        {
            for (const key in this.apps)
            {
                const app : any = this.apps[key];

                if (app.id.toLowerCase() == value.toLowerCase() ||
                   (!app.onlyViewer && app.code && app.code.toLowerCase() == value.toLowerCase()))
                {
                    return this.apps[key];
                }
            }    
        }

        console.log("APP NOT FOUND: " + value);

        return;
    }

    getMenuByApp(value:string)
    {
        for (const key in this.fullMenu)
        {
            if (this.fullMenu[key]._children && this.fullMenu[key]._children.length > 0)
            {
                for (const key2 in this.fullMenu[key]._children)
                {
                    if (this.fullMenu[key]._children[key2].app && this.fullMenu[key]._children[key2].app.code == value)
                    {
                        return this.fullMenu[key]._children[key2];
                    }
                }
            }
            else if (this.fullMenu[key].app && this.fullMenu[key].app.code == value)
            {
                return this.fullMenu[key];
            }
        }

        console.log("MENU NOT FOUND: " + value);

        return;
    }
	
    getMenuByName(name:string)
    {
        for (const key in this.fullMenu)
        {
            if (this.fullMenu[key]._children && this.fullMenu[key]._children.length > 0)
            {
                for (const key2 in this.fullMenu[key]._children)
                {
                    if (this.fullMenu[key]._children[key2].name == name)
                    {
                        return this.fullMenu[key]._children[key2];
                    }
                }
            }
            else if (this.fullMenu[key].name == name)
            {
                return this.fullMenu[key];
            }
        }

        console.log("MENU NOT FOUND: " + name);

        return;
    }

    getMenuById(id:string)
    {
        for (const key in this.fullMenu)
        {
            if (this.fullMenu[key]._children && this.fullMenu[key]._children.length > 0)
            {
                for (const key2 in this.fullMenu[key]._children)
                {
                    if (this.fullMenu[key]._children[key2].id == id)
                    {
                        return this.fullMenu[key]._children[key2];
                    }
                }
            }
            else if (this.fullMenu[key].id == id)
            {
                return this.fullMenu[key];
            }
        }

        console.log("MENU NOT FOUND: " + name);

        return;
    }
	
    goMenuByName(name:string)
    {
        this.navController.goMenu(this.getMenuByName(name));
    }

    goInit()
    {
        this.navController.goInit();
    }

    push(router:string, queryParams?:any, routeParams?:any)
    {
        this.navController.push(router, queryParams, routeParams);
    }

    goMenu(menu:any)
    {
        this.navController.goMenu(menu);
    }

    goFirstMenu()
    {
        this.navController.goFirstMenu();
    }

    back()
    {
        this.navController.back();
    }

    hasBack()
    {
        return this.navController && this.navController.hasBack();
    }

    isFirstMenu()
    {
        return this.navController && this.navController.isFirstMenu();
    }

    async getTimezone()
    {
        let accid = await this.storagePlugin.get("accid");

        if (!accid)
        {
            accid = this.account.code;
        }

        /* LOAD ACCOUNT */ 
        const params = this.paramsFactory.create({
            accid : accid,
        });

        const result = await this.accountPlugin.getTimezone(params);

        if (result.status)
        {
            this.timezone = result.data;
        }
        else
        {
            console.error("timezone não definido");
        }
    }

    async accountMasterLoad()
    {
        /* LOAD ACCOUNT */
		 const params = this.paramsFactory.create({
			 accid : this.masterAccount.code,
		 });

		 const result = await this.accountPlugin.get(params);

		 if (result.status)
		 {
            this.masterAccount = result.data;
		 }
		 else
		 {
            console.error("Conta master não existe");
		 }
    }

    async accountLoad()
    {
        this.accountProfile = await this.storagePlugin.get("accountProfile");
        let accid           = await this.storagePlugin.get("accid");

        if (!accid)
        {
            accid = this.account.code;
        }

        /* LOAD ACCOUNT */ 
        const params = this.paramsFactory.create({
            accid : accid,
        });

        const result = await this.accountPlugin.get(params);

        if (result.status)
        {
            console.log("accountLoad", accid);
            this.account = result.data;

            this.initAccount();
        }
        else
        {
            await this.storagePlugin.del("accid")
            console.error("Conta não existe", accid);
        }
    }
	
    initAccount()
    {
        console.log("init account load", this.account);		

        /* SOLVED */
        if (this.account.solved)
        {
            this.solved = this.account.solved;
        }                    

        const eventPlugin = this.injector.get(EventPlugin);
        eventPlugin.dispatch("account:load");
    }

    async loadApps()
    {
        const appPlugin = this.injector.get(AppPlugin);

        const params = this.paramsFactory.create({
            perPage : 999,                
        });

        const result = await appPlugin.getData(params);

        // CORRIGIR _COLID
        // for(const item of result.collection)
        // {
        // 	if(item.code == 'shippings')
        // 	{
        // 		item._colid = 'documentsShippings';
        // 	}
        // }

        this.apps            = result.collection;
        this.apps.isInfinite = true; // PARA QUANDO TIVER ALTERAÇÃO EM ALGUM APP APENAS ATUALIZAR NÃO CRIAR UM NOVO

        console.log("apps", this.apps);
    }

    async loadMenu()
    {
        let appid = await this.storagePlugin.get("menu_appid_" + this.platform.value)
		
        if (!appid)
        {
            appid = this.masterAccount.getMenuAppid();					
        }

        if (!appid)
        {
            console.error("account menu appid not defined", appid);
        }
        else
        {
            const menuPlugin = this.injector.get(MenuPlugin);
            const result     = await menuPlugin.loadMenu(appid);

            await result.collection.on(true);

            this.fullMenu = result.collection.copy();

            console.log("fullMenu", appid, this.fullMenu);

            await this.fullMenu.on(true);

            await result.collection.filterAsync(FunctionsType.get().getFunction("MENU_FILTER"));

            console.log("account menu appid", appid);

            this.menu = result.collection;

            console.log("menu", appid, this.menu);
        }
    }

    async getToken()
    {
        return new Promise<boolean>(async (resolve:any) =>
        {
            const reference  = doc(this.fireStore, this.account.code + "/authorization/documents/" + this.token);
            const docSnap    = await getDoc(reference);
            const data : any = docSnap.data();

            resolve(true);

            // docRx(reference).subscribe(async (doc:any) =>
            // {				
            // 	const data = doc.data();

            // 	//console.error(data);

            // 	//this.publicKey = await openpgp.readKey({ armoredKey : data.publicKey });

            // 	/*this.privateKey = await openpgp.decryptKey({
            // 		privateKey : await openpgp.readPrivateKey({ armoredKey : data.privateKey }),
            // 		passphrase : this.token
            // 	});*/							

            // 	resolve(true)
            // });			
        });
    }

    async authLoad()
    {
        /* LOGIN BY APPID */
        const appid = await this.storagePlugin.get("appid");
		
        console.log("verify auth", appid);

        const params = this.paramsFactory.create({
            appid : appid
        });

        const result = await this.authPlugin.getLogged(params);

        if (result.status)
        {
            const group = result.data.getGroup();

            // NÃO PERMITIR USUARIO SEM GRUPO
            if (group)
            {
                this.user     = result.data;
                this.isLogged = true;
				
                /* QUANDO O USUARIO É A CONTA */
                if (this.user["appid"] == "accounts")
                {
                    //result.data['accid'] = result.data.id; //ACCOUNTS POR ID
                    //result.data['appid'] = 'accounts'; //QUANDO ACCOUNTS NÃO TEM APPID
	
                    this.userAccount = true;	
                    this.account     = new Account(result.data.getData());
                    await this.account.on();
                }
            }
            else
            {
                /* LOGOUT */
                const params = this.createParams({
                    appid : result.data.appid 
                })

                this.authPlugin.logout(params);
            }			
        }
        else
        {
            if (this.hasGuestUser())
            {
                this.user = new User({
                    name  : "Login",
                    group : "guest"
                });

                await this.user.on();
	
                this.isLogged = true;	
            }

            if (!this.isAccountMaster)
            {
                await this.storagePlugin.del("accid");
                this.account = this.masterAccount;
            }
        }
    }

    get isAccountMaster()
    {
        return this.masterAccount && this.account && this.masterAccount.code == this.account.code;
    }
	
    async loadOptionPermissions()
    {
        const params = this.createParams({
            accid : this.masterAccount.code,	
            appid : "option",
            colid : "documents",
            code  : "_groups"
        });
		
        const result = await this.optionPlugin.get(params);

        if (result.data)
        {
            this.optionPermission = result.data;
        }
        else
        {
            this.optionPermission = new Option();
        }

        this.optionPermission.items.unshift({
            id    : "all",
            label : "Todos",
            value : "all",
        });

        console.log("optionPermission", this.optionPermission);
    }

    async loadLanguage()
    {
        /* NA SELECAO DE IDIOMAS */
        this.currentLanguage = await this.storagePlugin.get("language_" + this.platform.value)
		
        if (!this.currentLanguage)
        {
            this.currentLanguage = "pt";
        }

        const params = this.createParams({
            accid   : this.masterAccount.code,	
            appid   : "language",
            perPage : 9999,
        });

        const documentPlugin = this.injector.get(DocumentPlugin);
        const result         = await documentPlugin.getData(params);
        this.languageMaps    = {}

        if (result.collection.length > 0)
        {
            for (const key in result.collection)
            {
                const item                    = result.collection[key];
                this.languageMaps[item["pt"]] = item[this.currentLanguage];
            }	
        }
		
        console.log("load language", this.languageMaps);  		     
    }

    async loadNotification()
    {
        /* POR CAUSA DA SELECAO DE IDIOMAS */
        let formPath = await this.storagePlugin.get("notification_form_path_" + this.platform.value)
		
        if (!formPath)
        {
            formPath = this.account.getNotificationFormPath();
        }

        if (!formPath)
        {
            console.error("notification form path not defined", formPath);
        }
        else
        {
            console.log("notification form path", formPath);
        }

        if (formPath && this.notification)
        {
            const notificationPlugin = this.injector.get(NotificationPlugin);
            const data               = await notificationPlugin.registration(formPath, this.notification);
            this.notification        = data;
        }
    }

    loadFacebook() 
    {
        if (this.account.facebook)
        {
            (function (f: any, b, e, v, n, t, s) 
            {
                if (f.fbq) return;

                n = f.fbq = function () 
                {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };

                if (!f._fbq) f._fbq = n;
                n.push = n; n.loaded = !0; n.version = "2.0"; n.queue = []; t = b.createElement(e); t.async = !0;
                t.src   = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
            })(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");

            (window as any).fbq.disablePushState = true; //not recommended, but can be done
            (window as any).fbq("init",  this.account.facebook.pixel);
        }
    }

    loadGA() 
    {
        if (this.account.ga)
        {
            (function(i, s, o, g, r, a, m)
            {
                i["GoogleAnalyticsObject"] = r;i[r] = i[r] || function()
                {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * (new Date().getTime());a = s.createElement(o),
			  m = s.getElementsByTagName(o)[0];a.async = 1;a.src = g;m.parentNode.insertBefore(a, m)
            })(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga");

            (window as any).ga("create", this.account.analytics.id, "auto"); 
        }       
    }

    loadGTM() 
    {
        if (this.account.gtm)
        {
            (function(w, d, s, l, i)
            {
                w[l]    = w[l] || [];w[l].push({ "gtm.start" :
			new Date().getTime(), event       : "gtm.js" });const f = d.getElementsByTagName(s)[0],
                    j:any = d.createElement(s), dl = l != "dataLayer" ? "&l=" + l : "";
                j.async = true;
                j.src   = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;f.parentNode.insertBefore(j, f);
            })(window, document, "script", "dataLayer", this.account.gtm.key);					
        }       
    }

    gtag(...args:any)
    {
        (window as any).dataLayer.push(arguments);
    }
	
    loadAlertSound()
    {
        if (this.account.hasAlertSound && (this.isWeb || this.isElectron))
        {
            this.account.reference.onSnapshot((doc:any) => 
            {
                const data = doc.data();

                if (data.alerts)
                {
                    if (data.alerts.length > 0)
                    {
                        if (!this.alertSound || !this.alertSound.playing())
                        {
                            this.alertSound = new Howl({
                                src     		 	: [ "./assets/sound/" + data.alerts[0].type.value + ".mp3" ],
                                autoplay   	: false,
                                loop     	 	: true,
                                volume    		: 1,
                                autoSuspend : false,
                            });

                            setTimeout(() => 
                            {
                                this.alertSound.play();
                            });						
                        }										
                    }
                    else if (this.alertSound)
                    {
                        this.alertSound.stop();
                    }					

                    /* ATUALIZA LOCALMENTE */
                    this.account.alerts = data.alerts;
                }									
            })
        }
    }

    trackFacebook(track:string, value?:any)
    {
        if (this.account.facebook)
        {
            const _track = this.account.facebook[track];

            if (_track)
            {
                const tracks = _track.split(",");

                for (const name of tracks)
                {
                    if (value)
                    {
                        (window as any).fbq("track", name, value);
                    }            
                    else
                    {
                        (window as any).fbq("track", name);
                    }
	
                    console.error("track facebook", name);
                }
            }	
        }
    }

    // send, pageview, { eventCategory: eventCategory, eventLabel: eventLabel, eventAction: eventAction, eventValue: eventValue }
    trackGA(track:string, value?:any)	
    {
        if (this.account.ga)
        {
            if (this.account.ga[track])
            {
                const _track = this.account.ga[track];

                if (_track.value)
                {
                    value = _.merge({}, _track.value, value);
                }

                if (value)
                {
                    (window as any).ga(_track.type, _track.name, value);
                }
                else
                {
                    (window as any).ga(_track.type, _track.name);			
                }

                console.error("track analitics", _track);
            }					
        }
    }

    // send, pageview, { eventCategory: eventCategory, eventLabel: eventLabel, eventAction: eventAction, eventValue: eventValue }
    trackGTM(track:string, value?:any)	
    {
        if (this.account.gtm)
        {
            if (this.account.gtm[track])
            {
                const _track = this.account.gtm[track];

                if (_track.value)
                {
                    value = _.merge({}, _track.value, value);
                }

                if (value)
                {
                    this.gtag(_track.type, _track.name, value);
                }
                else
                {
                    (window as any).dataLayer.push(_track.type, _track.name);			
                }

                console.error("track GTM", _track);
            }					
        }
    }

    hasGuestUser()
    {
        const hasGuestUser = "hasGuestUser_" + this.platform.value;
        return this.account && this.account[hasGuestUser]
    }
}
