import { Subject } from 'rxjs';

/* APP */
import { Collection }    from '../collection/collection';
import { VideoEmbed }    from '../../util/video.embed/video.embed';
import { YoutubePlugin } from '../../plugin/youtube/youtube.plugin';

export class YoutubeCollection extends Collection
{
    public _channel      : string;
    public _playlist     : string;
    public youtubePlugin : any;
    public nextPageToken : any;
    public prevPageToken : any;
    public _onNextPage   : any;
    public _onPrevPage   : any;
    public _onSearch     : any;
    public term          : string = '';
	public perPage       : number = 48;

    constructor(args?:any)
    {
        super(args);

        this.youtubePlugin = this.core().injector.get(YoutubePlugin);

        Object.defineProperty(this, "_channel",      {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_playlist",     {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "youtubePlugin", {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "nextPageToken", {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "prevPageToken", {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_onNextPage",   {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_onPrevPage",   {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "_onSearch",     {writable: true, enumerable: false, configurable: true});
        Object.defineProperty(this, "term",          {writable: true, enumerable: false, configurable: true});
		Object.defineProperty(this, "perPage",       {writable: true, enumerable: false, configurable: true});

        this._onNextPage = new Subject();
        this._onPrevPage = new Subject();
        this._onSearch   = new Subject();
		this.isInfinite  = true;
    }

    load(pageToken?:string)
    {
		this.youtubePlugin.getVideosByPlaylist(this.playlist, pageToken).then((result:any) =>
		{
			if(result.status)
			{
				this.populate(result.data.items);

				/* BUG: POIS MESMO VAZIO VEM NEXT TOKEN*/
				if(result.data.items.length > 0)
				{
					this.nextPageToken = result.data.nextPageToken;
				}
				else
				{
					this.nextPageToken = null;
				}

				this.prevPageToken = result.data.prevPageToken;
			}
		});
	}
	
	isEmpty()
	{
		return this.length == 0 && this._playlist == '' && this._channel == '';
	}

    set playlist(id:string)
    {
        this._playlist = id;
        //this.loadPlaylist();
    }

    get playlist()
    {
        return this._playlist;
    }

    set channel(id:string)
    {
        this._channel = id;
        //this.loadChannel();
    }

    get channel()
    {
        return this._channel;
    }

    populate(items:any)
    {
        this.clear();

        const dimensions = ['standard', 'high','medium','default'];

        for(const key in items)
        {
			if(items[key].snippet.thumbnails)
			{
				let thumbnail:any;

				for(const key2 in dimensions)
				{
					const key3 = dimensions[key2];
	
					if(items[key].snippet.thumbnails[key3])
					{
						thumbnail = items[key].snippet.thumbnails[key3].url;
						break;
					}
				}
	
				const item = {
					id           : items[key].snippet.resourceId.videoId,
					title        : items[key].snippet.title,
					thumbnailUrl : thumbnail,
					type         : VideoEmbed.YOUTUBE,
					postdate     : items[key].snippet.publishedAt,
				};
	
				this.add(new VideoEmbed(item));
			}            
        }

        this.dispatchUpdate();
    }

    search(term:string)
    {
        this._onSearch.next();

        this.term = term;
        this.clear(true);
        this.load();
    }

    on()
    {
        return new Promise<void>((resolve) =>
        {
            resolve();
        })
    }

    nextPage()
    {
        if(this.hasNextPage())
        {
            this._onNextPage.next();
            this.load(this.nextPageToken);
        }
    }

    prevPage()
    {
        if(this.hasPrevPage())
        {
            this._onPrevPage.next();
            this.load(this.prevPageToken);
        }
    }

    onSearch()
    {
        return this._onSearch.asObservable();
    }

    hasNextPage()
    {
        return (this.nextPageToken ? true : false);
    }

    hasPrevPage()
    {
        return (this.prevPageToken ? true : false);
    }

    onNextPage()
    {
        return this._onNextPage.asObservable();
    }

    onPrevPage()
    {
        return this._onPrevPage.asObservable();
    }
}
