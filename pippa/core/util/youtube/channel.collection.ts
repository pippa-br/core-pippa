/* APP */
import { YoutubeCollection } from './youtube.collection';
import { VideoEmbed }        from '../../util/video.embed/video.embed';
import { YoutubePlugin }     from '../../plugin/youtube/youtube.plugin';

export class ChannelCollection extends YoutubeCollection
{
    constructor(args?:any)
    {
        super(args);

        this.youtubePlugin = this.core().injector.get(YoutubePlugin);
		this.isInfinite    = true; 
    }

    load(pageToken?:string)
    {
        this.youtubePlugin.getVideosByChannel(this.channel, this.term, pageToken, this.perPage).then((result:any) =>
		{
			if(result.status)
			{
				this.populate(result.data.items);

				/* BUG: POIS MESMO VAZIO VEM NEXT TOKEN*/
				if(result.data.items.length > 0)
				{
					this.nextPageToken = result.data.nextPageToken;
				}
				else
				{
					this.nextPageToken = null;
				}

				this.prevPageToken = result.data.prevPageToken;
			}
		});
	}

    populate(items:any)
    {
        this.clear();

        const dimensions = ['standard', 'high','medium','default'];

        for(const key in items)
        {
			/* INFO CHANNEL DA CHANNEL VEM NA API ITEM 0 */
			if(items[key].id.videoId)
			{
				let thumbnail:any;

				for(const key2 in dimensions)
				{
					const key3 = dimensions[key2];
	
					if(items[key].snippet.thumbnails[key3])
					{
						thumbnail = items[key].snippet.thumbnails[key3].url;
						break;
					}
				}
	
				const item = {
					id           : items[key].id.videoId,
					title        : items[key].snippet.title,
					thumbnailUrl : thumbnail,
					type         : VideoEmbed.YOUTUBE,
				};
	
				this.add(new VideoEmbed(item));
			}            
        }

        this.dispatchUpdate();
    }
}
