import { Params } from '../params/params';

export class Result extends Object
{
    public app:any;
    public params:Params;
    public collection:any;
    public count:number;
    public pageCount:number;
    public message:any;
	public error:any;
    public form:any;
    public data:any;
    public status:boolean;
    public total:number;

    constructor(params?:Params)
    {
        super();

        this.params = params;
        this.status = false;
    }
}
