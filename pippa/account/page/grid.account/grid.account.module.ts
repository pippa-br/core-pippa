import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

/* PIPPA */
import { CoreModule }      from "../../../core";
import { GridAccount }     from "../../component/grid.account/grid.account";
import { GridAccountPage } from "./grid.account.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : GridAccountPage } ])
    ],
    declarations : [
        GridAccountPage,
        GridAccount,
    ],
})
export class GridAccountModule 
{}
