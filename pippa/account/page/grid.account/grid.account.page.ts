import { Component } from "@angular/core";
import { PopoverController } from "@ionic/angular";

/* PIPPA */
import { BasePage }         from "../../../core/base/page/base.page";
import { ListPopover }      from "../../../core/popover/list/list.popover";

@Component({
    selector : "grid-account-page",
    template : `<ion-header>
                    <ion-toolbar>
                        <button ion-button icon-only menuToggle start>
                            <ion-icon name="menu-outline"></ion-icon>
                        </button>
                        <ion-title>
                            <ion-icon [name]="app?.icon?.value"></ion-icon>
                            {{app.name}}
                        </ion-title>
                        <ion-buttons end>
                            <button ion-button icon-only (click)="goAddForm($event)">
                                <ion-icon name="add-outline"></ion-icon>
                            </button>
                            <button ion-button icon-only (click)="goFormGrid()">
                                <ion-icon name="albums-outline"></ion-icon>
                            </button>
                        </ion-buttons>
                    </ion-toolbar>
                </ion-header>
                <ion-content>
                    <div account-grid
                         (apps)="onApps($event)"
                         (set)="onSet($event)"></div>
                </ion-content>`,
})
export class GridAccountPage extends BasePage
{
    constructor(
        public popoverController : PopoverController,
    )
    {
        super();
    }

    onClose()
    {
        this.back();
    }

    goAddForm(event:any)
    {
        this.popoverController.create(
            {
                component : ListPopover,
                componentProps :
            {
                app      : this.app,
                onSelect : ((form:any) =>
                {
                    if (form)
                    {
                        this.push("form-document",
                            {
                                appid    : this.app.code,
                                formPath : form.reference.path
                            });
                    }
                })
            },
                showBackdrop : false,
            })
        .then(popover =>
        {
            popover.present();
        })
    }

    goFormGrid()
    {
        this.push("grid-form",
            {
                appid : this.app.code
            });
    }

    onSet(event:any)
    {
        this.push("form-document",
            {
                appid    : this.app.code,
                dataPath : event.data.reference.path,
            });
    }

    onApps(event:any)
    {
        const app = this.getApp("apps").copy();
        app.accid = event.data.accid;

        //this.goApp(app);

        /*/this.goApp(new App({
            name         : event.data.name,
            accid        : event.data.accid,
            appid        : 'apps',
            plugin       : { value : 'APP_PLUGIN',         label : 'APP_PLUGIN'},
            viewer       : { value : 'DOCUMENT_GRID_PAGE', label : 'DOCUMENT_GRID_PAGE'},
            active       : true,
            icon         : {
                label : 'apps-outline',
                value : 'apps-outline',
            },
            menu         : false,
            isCollection : () =>
            {
                return true;
            }
        }));*/
    }
}
