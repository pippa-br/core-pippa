import { Component } from "@angular/core";

/* PIPPA */
import { BasePage }      from "../../../core/base/page/base.page";
import { StoragePlugin } from "../../../core/plugin/storage/storage.plugin";


@Component({
    selector : "logout-account",
    template : "<ng-template></ng-template>",
})
export class LogoutAccountPage extends BasePage
{
    constructor(
        public storagePlugin : StoragePlugin,
    )
    {
        super();
    }

    async initialize()
    {
        console.log("logout account");

        const accid = await this.storagePlugin.get("accid");		
        //this.core().isInitialize = false;

        if (accid)
        {
            await this.storagePlugin.del("accid");
            await this.storagePlugin.del("accountProfile");
			
            this.core().accountProfile = false;	
            this.core().account        = this.core().backAccount;
            this.core().backAccount    = null;
        }

        // ATUALIZA O MENU
        if (this.core().menu)
        {
            await this.core().menu.updateAsyncFilter();			
        }

        this.goFirstMenu();
		
        //this.goInit();	
    }
}
