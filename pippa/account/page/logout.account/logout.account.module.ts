import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }        from "../../../core";
import { LogoutAccountPage } from "./logout.account.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : LogoutAccountPage } ])
    ],
    declarations : [
        LogoutAccountPage,
    ],
})
export class LogoutAccountModule 
{}
