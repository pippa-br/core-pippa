import { Component } from "@angular/core";

/* PIPPA */
import { BasePage }         from "../../../core/base/page/base.page";
import { StoragePlugin }    from "../../../core/plugin/storage/storage.plugin";

@Component({
    selector : ".change-account",
    template : "<ng-template></ng-template>",
})
export class ChangeAccountPage extends BasePage
{
    constructor(
        public storagePlugin : StoragePlugin,
    )
    {
        super();
    }

    async initialize()
    {
        console.log("change account", this.document);

        //const accid = await this.storagePlugin.get('accid');
		
        //console.log('change accid', accid);

        //this.core().isInitialize = false;

        await this.storagePlugin.set("accid", 		   this.document.code);
        await this.storagePlugin.set("accountProfile", true); // PARA INDENTIFICAR QUE HOUVE MUDANÇA DE CONTA NA MESMA SECAO		

        this.core().backAccount    = this.core().account;
        this.core().account        = this.document;
        this.core().accountProfile = true;

        // ATUALIZA O MENU
        if (this.core().menu)
        {
            await this.core().menu.updateAsyncFilter();			
        }

        this.goFirstMenu();

        //this.goInit();

        /*if(!accid)
		{
			
		}
		else
		{
			this.goInit();
		}*/
    }
}
