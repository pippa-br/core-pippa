import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

import { CoreModule }        from "../../../core";
import { ChangeAccountPage } from "./change.account.page";

@NgModule({
    imports : [
        CoreModule,
        RouterModule.forChild([ { path : "", component : ChangeAccountPage } ])
    ],
    declarations : [
        ChangeAccountPage,
    ],
})
export class ChangeAccountModule 
{}
