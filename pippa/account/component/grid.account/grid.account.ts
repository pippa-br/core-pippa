import { Component, Output, OnInit, EventEmitter } from "@angular/core";

/* PIPPA */
import { BaseGrid }       from "../../../core/base/grid/base.grid";
import { FieldType }      from "../../../core/type/field/field.type";
import { FormCollection } from "../../../core/model/form/form.collection";
import { AccountPlugin }  from "../../../core/plugin/account/account.plugin";
import { Grid }           from "../../../core/model/grid/grid";

@Component({
    selector : "[grid-account]",
    template : `<div dynamic-list
                      [grid]="grid"
                      [collection]="collection"
                      (add)="onAdd($event)"
                      (set)="onSet($event)"
                      (del)="onDel($event)"
                      (viewer)="onViewer($event)">
                </div>`,
})
export class GridAccount extends BaseGrid implements OnInit
{
    @Output("apps") appsEvent : EventEmitter<any> = new EventEmitter();

    constructor(
        public accountPlugin : AccountPlugin,
    )
    {
        super();

        this.grid = new Grid({
            items : [
                {
                    label : "ID",
                    value : "accid",
                    type  : FieldType.type("Text"),
                },
                {
                    label : "Nome",
                    value : "name",
                    type  : FieldType.type("Text"),
                },
            ],
            buttons : [
                {
                    icon      : "apps",
                    isEnabled : (() =>
                    {
                        return true;
                    }),
                    click : ((event) =>
                    {
                        this.appsEvent.emit(event);
                    })
                },
            ]
        });
    }

    doDel(item)
    {
        /*if(item.hasParent())
        {
            item.parent.fields.del(item);

            let parent = item.rootParent();
            let fields = parent.fields.parseData();

            parent.set({
                fields : fields
            });
        }
        else
        {*/
        item.del();
        //}
    }

    ngOnInit()
    {
        const params = this.createParams({
            app   : this.app,
            order : "order",
        });

        this.accountPlugin.getData(params).then((result:any) =>
        {
            this.collection = result.collection;

            //this.flatCollection = result.collection.getFlat();

            //this.collection.change = () =>
            //{
            //console.log('change flatCollection');
            //this.flatCollection = this.collection.getFlat();
            //};
        });
    }
}
